<?php
/******************* CouponController.php *******************
 *

 *
 ******************** CouponController.php ******************/

/**
 * Define Namespace
 */
namespace mcms5xx;

//https://www.techiediaries.com/php-jwt-authentication-tutorial/

/**
 * Include view page class
 */
require_once '../../m/classes/viewpage.class.php';
require_once '../../m/classes/APIJsonResponse.php';
require_once "../../m/classes/vendor/autoload.php";
use \Firebase\JWT\JWT;

class CouponController extends \mcms5xx\classes\ViewPage
{
    public $langs;
    public $permalinks = '';
    public $perma_type = '';
    public $inside_lang = '';
    public $index_lang = '';

	public $operation = '';
    public $isSuccess = false;
	public $code = 0;
	public $data = array();
    public $errors = array();
    public $result = array();

	public $site_url = '';
	public $secret_key = '';
	
    public function __construct()
    {
	    parent::__construct();
		$this->site_url = $this->fromConfig('site_url');
		$this->secret_key = "45478d6870bf6ae031212152364f0e84";
		
		//$this->utils->require_auth();
    }

    public function onLoad()
    {

		$action = $this->utils->Get('action');
        if (@$_SERVER['REQUEST_METHOD'] == 'POST') {
			
			$this->utils->RequestHeaderPost();
            switch ($action) {
                case 'news':
					$this->news();
				break;
                case 'categories':
					$this->categories();
				break;
            }
		} 

        if (@$_SERVER['REQUEST_METHOD'] == 'GET') {
			$this->utils->RequestHeaderGet();
            switch ($action) {
                case 'none':
				break;
            }
		} 

		$res = new \mcms5xx\classes\APIJsonResponse($this->isSuccess, $this->errors, $this->data);
    }



	private function news(){
		$catid = 0; $PageCount = 1;
		$newsLimit = 10;
		$data = json_decode(file_get_contents("php://input"));
		
        $monthArr = $this->fromLangIndex('monthArr');
		$catid = $data->catid;
		$PageCount = $data->PageCount;
		$limitTo = $PageCount * $newsLimit; 
		$limitFrom = $limitTo - $newsLimit;
		//$neSql = ($cat_row['event'] == 1) ? ' && (N.newsdate>=' . (time() - $events_days_limit) . ') ' : ' && (N.newsdate<=' . time() . ') ';
		$catSql = ($catid > 0) ? ' && (NC.catid=' . $catid . ') ' : ' ';
		
		$sql = 'SELECT N.*, NL.name, NL.header, NL.comment, NL.slug, (select C.name from ' . $this->db->prefix . "newscategorylocalizations C WHERE C.catid=N.catid && C.lang='".$this->lang."') as catname FROM
			" . $this->db->prefix . 'news N
			INNER JOIN ' . $this->db->prefix . 'newslocalizations NL ON NL.newsid = N.newsid
			INNER JOIN ' . $this->db->prefix . "newscategories NC ON NC.catid = N.catid
			WHERE 
				(N.active='1')
			 && (NL.name!='')
			 && (NL.lang='" . $this->lang . "')
			" . $catSql . '
			ORDER BY N.newsdate ASC
			' . $this->db->get_limit($limitFrom, $limitTo);
			

		$rows = $this->db->fetch_all($sql);
		$news = array();
		foreach($rows as $row) {
			$name = $row['name'];
			$newsid = $row['newsid'];
			$catname = $row['catname'];
			$comment = $row['comment'];
			$imgid = (int)$row['header'];
			
			$monthNum = date('n', $row['newsdate']) - 1;

            $duration = $this->utils->time_elapsed_string($row['newsdate']);
            $img_file = $this->site_url . $this->site->getFile($imgid);

			$new = array();
			$new['id'] = $newsid;
			$new['name'] = $name;
			$new['duration'] = $duration;
			$new['iddia']['count'] = rand(1,999);
			$new['iddia']['yes'] = rand(1,999);
			$new['iddia']['no'] = rand(1,999);
			$new['newsdate']['DD'] = date('d', $row['newsdate']);
			$new['newsdate']['MM'] = date('m', $row['newsdate']);
			$new['newsdate']['MONTH'] = date('M', $row['newsdate']);
			$new['newsdate']['MONTH_LANG'] = $monthArr[$monthNum];
			$new['newsdate']['YYYY'] = date('Y', $row['newsdate']);
			$new['newsdate']['HM'] = date('H:i', $row['newsdate']);
			$new['catname'] = $catname;
			$new['comment'] = $comment;
			$new['img'] = $img_file;

			$news[] = $new;
		}
		
		if(count($news) > 0) $this->isSuccess = true;
		$this->data['list'] = $news;
	
	}
	



	private function categories(){
		$data = json_decode(file_get_contents("php://input"));
		
		
		$sql = 'SELECT N.*, NL.name FROM
			' . $this->db->prefix . 'newscategories N
			INNER JOIN ' . $this->db->prefix . "newscategorylocalizations NL ON NL.catid = N.catid
			WHERE 
				(N.active='1')
			 && (NL.name!='')
			 && (NL.lang='" . $this->lang . "')
			ORDER BY N.position ASC";
			

		$rows = $this->db->fetch_all($sql);
		$categories = array();
		foreach($rows as $row) {
			$catid = $row['catid'];
			$name = $row['name'];

			$cat = array();
			$cat['id'] = $catid;
			$cat['name'] = $name;
			
			$categories[] = $cat;
		}
		
		if(count($categories) > 0) $this->isSuccess = true;
		$this->data['list'] = $categories;
	
	}
	




}
$CouponController = new CouponController();

include $CouponController->lg_folder . '/index.lang.php';
$CouponController->onLoad();


/******************* api.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** api.php ******************/;

?>