<?php
/******************* UserController.php *******************
 *

 *
 ******************** UserController.php ******************/

/**
 * Define Namespace
 */
namespace mcms5xx;

//https://www.techiediaries.com/php-jwt-authentication-tutorial/

/**
 * Include view page class
 */
require_once '../../m/classes/viewpage.class.php';
require_once '../../m/classes/APIJsonResponse.php';
require_once "../../m/classes/vendor/autoload.php";
use \Firebase\JWT\JWT;

class UserController extends \mcms5xx\classes\ViewPage
{
    public $langs;
    public $permalinks = '';
    public $perma_type = '';
    public $inside_lang = '';
    public $index_lang = '';

	public $operation = '';
    public $isSuccess = false;
	public $code = 0;
	public $data = array();
    public $errors = array();
    public $result = array();

	public $site_url = '';
	public $secret_key = '';
	
    public function __construct()
    {
	    parent::__construct();
		$this->site_url = $this->fromConfig('site_url');
		$this->secret_key = "45478d6870bf6ae031212152364f0e84";
		
		//$this->utils->require_auth();
    }

    public function onLoad()
    {

		$action = $this->utils->Get('action');
        if (@$_SERVER['REQUEST_METHOD'] == 'POST') {
			
			$this->utils->RequestHeaderPost();
            switch ($action) {
                case 'login':
					$this->login();
				break;
                case 'registerStep1':				
					$this->registerSendCode();
				break;
                case 'registerStep2':				
					$this->registerVerifyCode();
				break;
                case 'registerStep3':
					$this->register();
				break;
            }
		} 

        if (@$_SERVER['REQUEST_METHOD'] == 'GET') {
			
			$this->utils->RequestHeaderGet();
            switch ($action) {
                case 'protect':
					$this->protect();
				break;
            }
		} 
		if($this->isSuccess)
			$this->isSuccess = 'ok';
		$res = new \mcms5xx\classes\APIJsonResponse($this->isSuccess, $this->errors, $this->data);
		
    }



	private function protect(){
		$jwt = null;
		
		$data = json_decode(file_get_contents("php://input"));

		
		$jwt = $this->utils->getBearerToken();
		if($jwt){

			try {
				$decoded = JWT::decode($jwt, $this->secret_key, array('HS256'));
				// Access is granted. Add code of the operation here 
				$this->data['jwt'] = $jwt;			
				$this->isSuccess = true;
				http_response_code(200);

			} catch (\Firebase\JWT\ExpiredException $e){

				http_response_code(401);

				$err = array();
				$err['code'] = 'LOGIN002';
				$err['message'] = $e->getMessage();
				$this->errors[] = $err;		
				
				$this->isSuccess = false;
			}
		}
	
	}
	
	private function login() {

		$username = ''; $password = '';
		
		$data = json_decode(file_get_contents("php://input"));
		$username = $data->username;
		$password = $data->password;
		
		/* check user */
		$queryCheck = 'SELECT 
				M.*
			FROM ' .$this->db->prefix."members M
			WHERE LOWER(M.m_mail)='".trim(strtolower($username))."' AND M.m_pass='".SHA1(trim($password))."'";

		$resultCheck = $this->db->query($queryCheck);
		if ($rowCheck = $this->db->fetch($resultCheck)) {
			
			$id = $rowCheck['member_id'];
			$firstname = $rowCheck['m_name'];
			$lastname = $rowCheck['m_name'];
			$email = $rowCheck['m_mail'];

			$issuer_claim = $this->site_url; // this can be the servername
			$audience_claim = "THE_AUDIENCE";
			$issuedat_claim = time(); // issued at
			$notbefore_claim = $issuedat_claim + 10; //not before in seconds
			$expire_claim = $issuedat_claim + 60; // expire time in seconds
			$token = array(
				"iss" => $issuer_claim,
				"aud" => $audience_claim,
				"iat" => $issuedat_claim,
				"nbf" => $notbefore_claim,
				"exp" => $expire_claim,
				"data" => array(
					"id" => $id,
					"firstname" => $firstname,
					"lastname" => $lastname,
					"email" => $email
			));	

			$jwt = JWT::encode($token, $this->secret_key);	
			$this->data['jwt'] = $jwt;			
			$this->data['expireAt'] = $expire_claim;	
			$this->isSuccess = true;
		}
		else {
			$this->isSuccess = false;
			$err = array();
			$err['code'] = 'LOGIN001';
			$err['message'] = 'User does not exist';
			$this->errors[] = $err;			
			
		}

	}
	
	private function registerVerifyCode() {

		$email = ''; $code = '';
		
		$data = json_decode(file_get_contents("php://input"));
		$email = $data->email;
		$code = $data->code;
		
		/* check user */
		$queryCheck = 'SELECT 
				M.*
			FROM ' .$this->db->prefix."members_register_code M
			WHERE LOWER(M.m_mail)='".trim(strtolower($email))."' AND M.code='".$code."' AND M.active=2 ORDER BY M.code_id LIMIT 1";
		

		$resultCheck = $this->db->query($queryCheck);
		if ($rowCheck = $this->db->fetch($resultCheck)) {
			
			$id = $rowCheck['code_id'];
			$this->changeRegisterCodeStatus($id, 1);

			$this->isSuccess = true;
		}
		else {
			$this->isSuccess = false;
			$err = array();
			$err['code'] = 'LOGIN001';
			$err['message'] = 'code not correct';
			$this->errors[] = $err;			
			
		}

	}

	private function changeRegisterCodeStatus($id, $active) {

		$dataUpdate = array();
		$dataUpdate['active'] = $active;
		
		$this->db->update($this->db->prefix.'members_register_code' , $dataUpdate, " M.code_id='".$id."'");
	}
	
	
	
	private function registerSendCode() {

		$email = ''; 
		
		$data = json_decode(file_get_contents("php://input"));
		$email = $data->email;
		
		/* check user */
		$queryCheck = 'SELECT 
				M.*
			FROM ' .$this->db->prefix."members M
			WHERE LOWER(M.m_mail)='".trim(strtolower($email))."'";

		$resultCheck = $this->db->query($queryCheck);
		if ($rowCheck = $this->db->fetch($resultCheck)) {
			
			$this->isSuccess = false;
			$err = array();
			$err['code'] = 'REG001';
			$err['message'] = 'User is registered';
			$this->errors[] = $err;	
		}
		else {
				
			$dataInsert = array();
			$dataInsert['code'] = $this->generateRegisterCode();
			$dataInsert['m_mail'] = trim($email);
			$dataInsert['active'] = 2;
			$dataInsert['created_date'] = time();
			
			$inserted_id = $this->db->insert($this->db->prefix.'members_register_code' , $dataInsert);
			$this->isSuccess = true;		
			
		}

	}

	private function generateRegisterCode() {
		$randnum = rand(111111,999999);
		//return $randnum;
		return '111111';
	}
	
	
	private function register() {

		$name = ''; $surname=''; $password = ''; $password2=''; $username = '';
		
		$data = json_decode(file_get_contents("php://input"));
		$name = $data->name;
		$surname = $data->surname;
		$username = $data->username;
		$password = $data->password;
		$password2 = $data->password2;
		
		/* check user */
		$queryCheck = 'SELECT 
				M.*
			FROM ' .$this->db->prefix."members M
			WHERE LOWER(M.m_mail)='".trim(strtolower($username))."'";

		$resultCheck = $this->db->query($queryCheck);
		if ($rowCheck = $this->db->fetch($resultCheck)) {
			
			$this->isSuccess = false;
			$err = array();
			$err['code'] = 'REG001';
			$err['message'] = 'User exist';
			$this->errors[] = $err;	
		}
		else {
			$dataInsert = array();
			$dataInsert['m_mail'] = trim($username);
			$dataInsert['m_user'] = trim($username);
			$dataInsert['m_name'] = trim($name);
			$dataInsert['m_surname'] = trim($surname);
            $dataInsert['m_phone'] = '';
            $dataInsert['m_birthdate'] = '';
            $dataInsert['activate_info'] = '';
            $dataInsert['last_visit_date'] = time();
            
			$dataInsert['m_pass'] = trim(SHA1($password));
			$dataInsert['reg_date'] = time();
			$dataInsert['active'] = 1;
			
			$inserted_id = $this->db->insert($this->db->prefix.'members' , $dataInsert);
			$this->isSuccess = true;		
			
		}

	}





}
$UserController = new UserController();

include $UserController->lg_folder . '/index.lang.php';
$UserController->onLoad();


/******************* api.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** api.php ******************/;

?>