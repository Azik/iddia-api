<?php
/******************* mcms.cnfg.php *******************
 *
 * @version 5.2.0
 *
 * Configuration file
 *
 * @author MicroPHP <info@microphp.com>
 * @web www.microphp.com
 * @copyright 2004-2019 MicroPHP
 *
 ******************** mcms.cnfg.php ******************/

/** 
 * Define Namespace 
 */
namespace mcms5xx;

/**
 * Current timezone.
 */
date_default_timezone_set('Asia/Baku');

/**
 * Set error reporting
 */
@ini_set('error_reporting', E_ALL);
@ini_set('display_errors', 1);

/** 
 * Block pages from loading when they detect reflected XSS attacks 
 */
header("X-XSS-Protection: 1; mode=block");

/*
 * Start Session
 */
session_start();

/*
 * Gzip Compression
 */
if (substr_count(@$_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip')) {
    ob_start('ob_gzhandler');
} else {
    ob_start();
}

/**
 * include admin configuration file.
 */
require 'mcms.dmn.php';

class mcmsConfig extends \mcms5xx\mcmsAdminConfig
{
    /**
     * Don't change order, it used on preg_match.
     *
     * @var array URL's for pages
     */
    public $permalinks = array(
        'page_slug' => array(
            'index' => array('0' => '', '1' => '[lang]/'),
            'module_homepage' => array('0' => '', '1' => '[lang]/'),
            'index_html' => array('0' => 'index.html', '1' => '[lang]/index.html'),

            'news' => array('0' => 'news/[year]/[slug]', '1' => '[lang]/news/[year]/[slug]'),
            'news_cat' => array('0' => 'news/[catid]', '1' => '[lang]/news/[catid]'),
            'news_cat_ns' => array('0' => 'news/[catid]/', '1' => '[lang]/news/[catid]/'),
            'news_cat_page_ns' => array('0' => 'news/c[catid]pg[page]', '1' => '[lang]/news/c[catid]pg[page]'),
            'news_cat_page' => array('0' => 'news/c[catid]pg[page]/', '1' => '[lang]/news/c[catid]pg[page]/'),

            'module_braincategory' => array('0' => 'category/[catid]', '1' => '[lang]/category/[catid]'),
            'module_braintest' => array('0' => 'test/[testid]', '1' => '[lang]/test/[testid]'),

            'module_news' => array('0' => 'news', '1' => '[lang]/news'),
            'module_news_ns' => array('0' => 'news/', '1' => '[lang]/news/'),
            'module_news_paging' => array('0' => 'news/pg[page]', '1' => '[lang]/news/pg[page]'),

            'module_contacts' => array('0' => 'contacts', '1' => '[lang]/contacts'),
            'contacts' => array('0' => 'contacts', '1' => '[lang]/contacts'),
            'contacts_ok' => array('0' => 'contacts-ok', '1' => '[lang]/contactsok'),
            'contacts_error' => array('0' => 'contacts-error', '1' => '[lang]/contactserror'),

            'unsubscribe' => array('unsubscribe/[slug]', '[lang]/unsubscribe/[slug]'),
            'module_subscribe' => array('0' => 'subscribe', '1' => '[lang]/subscribe'),
            'module_subscribe_ok' => array('0' => 'subscribe-ok', '1' => '[lang]/subscribeok'),
            'module_subscribe_error' => array('0' => 'subscribe-error', '1' => '[lang]/subscribeerror'),

            'search' => array('0' => 'search', '1' => '[lang]/search'),
            'error404' => array('0' => '404', '1' => '[lang]/404'),
            'inside' => array('0' => '[slug]', '1' => '[lang]/[slug]'),
        ),
    );

    /**
     * @var string
     */
    public $perma_type = 'page_slug';

    /**
     *  Module request name
     *  Request method _GET.
     *
     * @var
     */
    public $module_view_qs = 'module';

    /**
     * @var string
     */
    public $protocolsArr = array('http://', 'https://', 'ftp://', 'mailto:');

    /**
     * @var string
     */
    public $lang_folder = 't/lang/';

    /**
     * @var string
     */
    public $dateformat = 'd.m.Y'; //"d F Y";

    /**
     * @var array
     */
    public $date_arr = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');

    /**
     * Template folder
     *
     * @var string
     */
    public $template_folder = 't/';

    /**
     * Site view template
     * Default templates: openhouse, healthcare, zentro, blank
     * Default value: blank
     *
     * @var string
     */
    public $view_template = "v1";

    /**
     * @var array Templates for view page
     */
    public $templates_view = array();

    /**
     * @var array Default view pages
     */
    public $templates_views = array();

    /**
     * @var array Category levels
     */
    public $levels = array();

    /**
     * @var string
     */
    public $upload_folder = 't/u/'; 

	/**
     * @var string
     */
    public $site_url = 'http://localhost/iddia/public_html/';
	public $secret_key = "45478d6870bf6ae031212152364f0e84";
	/**
     * @var string
     */
    public $apiUrl = 'http://liminal.abcvyz.com:4015/lifeTest/otp';

    /**
     * @var int Size limit in bytes for thumbnail image
     */
    public $thumb_limit = 9876543210000; //512000;

    /**
     * Gallery configurations.
     *
     * @var string
     * @var string $gallery_sub
     * @var string $gallery_code
     */
    public $gallery_folder = 'gallery';
    public $gallery_sub = 'gallery_[id]';
    public $gallery_code = '{GALLERY_[id]}';

    /* News module */
    /**
     * @var string
     */
    public $news_foot_limit = 3;

    /**
     * @var string
     */
    public $news_index_limit = 5;
    
    /**
     * @var string
     */
    public $news_slide_limit = 5;

    /**
     * @var string
     */
    public $news_page_limit = 3;

    /**
     * @var string
     */
    public $news_block_seperator = 3;

    /**
     * @var string
     */
    public $news_top_page = 3;

    /**
     * @var string
     */
    public $news_topin_page = 4;

    /**
     * @var string
     */
    public $news_date_format = 'd.m.Y';
	
    /**
     * @var string
     */
    public $news_cat_page = 10;
	
    /**
     * @var string
     */
    public $news_view_in_page = 3;
	
    /**
     * @var array
     */
    public $newsPagingArr = array(1, 5, 10, 20);

    /* Events module */
    /**
     * @var int How hays show event
     */
    public $events_days_limit = 7;

    /* Subscribe module */
    /**
     * @var int mail sends limit
     */
    public $subscribe_mail_sends = 1;

    /**
	 * Images
	 */
    public $imgResize = 700;
    public $imgSizes = array('400x400', '430x330', '227x330', '325x160', '325x340', '325x155', '328x247');
    public $ajaxPageLimit = 120;

    public $apiAuth = array(
        'mobile' => array(
            'user' => 'demo',
            'pass' => 'demo',
        ),
    );
    public function __construct()
    {
        parent::__construct();

        /**
         * Site module template files
         */
        $this->templates_view['index.header'] = 'index.header.tpl';
        $this->templates_view['index.footer'] = 'index.footer.tpl';
        $this->templates_view['main'] = 'main.tpl';
        $this->templates_view['inside'] = 'inside.tpl';
        $this->templates_view['news_more'] = 'news_more.tpl';
        $this->templates_view['service'] = 'service.tpl';
        $this->templates_view['news'] = 'news.tpl';
        $this->templates_view['gallery'] = 'gallery.tpl';
        $this->templates_view['contacts'] = 'contacts.tpl';
        $this->templates_view["subscribe"] = "subscribe.tpl";
        $this->templates_view["mcategory"] = "mcategory.tpl";
        $this->templates_view["mquestion"] = "mquestion.tpl";
        $this->templates_view["mtest"] = "mtest.tpl";
        $this->templates_view['404'] = '404.tpl';

        /**
         * View template types
         */
        $this->templates_views = array(
            1 => 'main',
            2 => 'inside',
            3 => 'gallery',
            4 => 'news_more',
            5 => 'service',
        );
		
		
        /**
         * levels types
         */
        $this->levels = array(
            100 => 'free',
            200 => 'low',
            300 => 'high',
        );





    }
}

/**
 * Check for Attacks
 */
define('mCMScheck', '1');

/**
 * Define folder path for include files
 */
if (!defined('iFolded')) {
    define('iFolded', '../../');
}

/**
 * if not install process include Database configuration file.
 */

if (is_file(iFolded . 'mcms.db.php')) {
    require iFolded . 'mcms.db.php';
}

/******************* mcms.cnfg.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** mcms.cnfg.php ******************/
