<?php
/******************* viewpage.class.php *******************
 *
 * @author MicroPHP <info@microphp.com>
 * @web www.microphp.com
 * @copyright 2004-2019 MicroPHP
 *
 ******************** viewpage.class.php ******************/

/** 
 * Define Namespace 
 */
namespace mcms5xx\classes;

/**
 * Include configuration file.
 */
require_once '../../mcms.cnfg.php';

if (!defined('mCMSdb')) {
    die ('mCMS not installed. Please use <a href="install.php">install.php</a> ');
}

/*
 * Checking if class included normally
 */
if (!defined('mCMScheck')) {
    die('mCMS by MicroPHP');
}

/**
 * Include Base class.
 */
require_once iFolded . 'm/classes/base.class.php';


/**
 * View class
 */
class ViewPage extends Base
{
    /**
     * Site default module.
     *
     * @var string
     */
    public $def_module = 'main';

    /**
     * Current module.
     *
     * @var string
     */
    public $module;

    /**
     * Modules folder.
     */
    public $module_folder = 'm/modules/';

    /**
     *  Module request name
     *  Request method _GET.
     *
     * @var
     */
    public $module_qs = '';

    /**
     * Page is parallax or not (0, 1)
     */
    public $parallax = 0;

    public $langs;
    public $default_lang;
    public $lang;
    public $lg_folder;
    public $lang_qs = 'lang';
    public $curr_lang = 0;

	public $session_logged = "member_logged";
	public $session_email = "member_email";
	public $session_userid = "member_id";
	public $session_usertype = "member_usertype";

    /**
     * @var string Templates class
     */
    public $template;
    public $temp_view;

    /**
     * News Date format.
     */
    public $news_date_format = '';

    /**
     * @var string Site script current folder
     *
     * @example http://example.com/folder/file.php $curr_folder = /folder/
     */
    public $curr_folder;

    /**
     * current domain
     */
    public $curr_domain;

    /**
     * current module
     */
    public $curr_module;
    public $idArr = array();

    /**
     * View Class constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->module_qs = $this->fromConfig('module_view_qs');
        $this->on_load();
        $this->parallax = $this->getKey('parallax', '0');
        if ($this->parallax == 1) {
            $this->parallaxMain();
        }
    }

    /**
     * defineLang Method
     * Defines language.
     */
    public function defineLang()
    {
        $lang = $this->utils->UserGet($this->lang_qs);
        $lang = strtolower($lang);

        if (!in_array($lang, $this->langs)) {
            $lang = $this->default_lang;
        }
        $this->lang = $lang;
        $this->lg_folder = $this->fromConfig('lang_folder');
        $this->lg_folder .= $this->lang;
        $this->lg_folder = '../../' . $this->lg_folder;
    }
	
    /**
     * @method createTemplate(string $rootdir) Creating templates
     *
     * @param string $rootdir
     */
    public function createTemplate($rootdir = './')
    {
        $template = $this->fromConfig('view_template');
        $dir = $this->fromConfig('template_folder');
        $temp_view = '{ROOT}{DIR}{TEMPLATE}';
        $temp_view = str_replace('{ROOT}', $rootdir, $temp_view);
        $temp_view = str_replace('{DIR}', $dir, $temp_view);
        $temp_view = str_replace('{TEMPLATE}', $template, $temp_view);

        $temp_view_folder = '/{DIR}{TEMPLATE}';
        $temp_view_folder = str_replace('{DIR}', $dir, $temp_view_folder);
        $temp_view_folder = str_replace('{TEMPLATE}', $template, $temp_view_folder);

        $temp_site_folder = $this->curr_folder . '{DIR}{TEMPLATE}';
        $temp_site_folder = str_replace('{DIR}', $dir, $temp_site_folder);
        $temp_site_folder = str_replace('{TEMPLATE}', $template, $temp_site_folder);
        $this->curr_domain = 'https://' . $_SERVER['HTTP_HOST'];

        $this->template = new Template($temp_view);
        $template_files = $this->fromConfig('templates_view');

        $this->template->set_filenames($template_files);
        $this->template->assign_var('ROOT', $temp_view . '/');
        $this->template->assign_var('ROOT_FOLDER', $temp_view_folder . '/');
        $this->template->assign_var('SITE_FOLDER', $temp_site_folder . '/');
        $this->template->assign_var('CURR_FOLDER', $this->curr_folder);

        $this->temp_view = $temp_view;
        $this->temp_view_folder = $temp_view_folder;

        // Admin configs
        $template_admin = $this->fromConfig('admin_template');
        $dir_admin = $this->fromConfig('template_folder');
        $temp_admin = $this->curr_folder . '{DIR_ADMIN}{TEMPLATE_ADMIN}';
        $temp_admin = str_replace('{DIR_ADMIN}', $dir_admin, $temp_admin);
        $temp_admin = str_replace('{TEMPLATE_ADMIN}', $template_admin, $temp_admin);
        $this->template->assign_var('ROOT_ADMIN', $temp_admin . '/');

        $this->template->assign_var('LANG', $this->lang);
        $this->template->assign_var('LANG_UP', $this->nameup($this->lang));
    }

    public function grabSlug()
    {
        if (array_key_exists('mfd', $_GET)) {
            $m = $this->utils->UserGet('mfd');
            $mdl = $this->utils->UserGet($this->module_qs);
            if (strlen($mdl) == 0) {
                $matches = array();
                $module_tp = '';
                foreach ($this->permalinks[$this->perma_type] as $module_tp => $ruleArr) {
                    $ruleArr['0'] = str_replace('/', '\/', $ruleArr['0']);
                    $ruleArr['0'] = str_replace('.', '\.', $ruleArr['0']);
                    $ruleArr['0'] = str_replace('[year]', '[0-9]{4}', $ruleArr['0']);
                    $ruleArr['0'] = str_replace('[lang]', '[a-z]{2}', $ruleArr['0']);
                    $ruleArr['0'] = str_replace('[page]', '[0-9]{1,}', $ruleArr['0']);
                    $ruleArr['0'] = str_replace('[id]', '[0-9]{1,}', $ruleArr['0']);
                    $ruleArr['0'] = str_replace('[subid]', '[0-9]{1,}', $ruleArr['0']);
                    $ruleArr['0'] = str_replace('[catid]', '[0-9]{1,}', $ruleArr['0']);
                    $ruleArr['0'] = str_replace('[testid]', '[0-9]{1,}', $ruleArr['0']);
                    $ruleArr['0'] = str_replace('[slug]', '(.*)', $ruleArr['0']);
                    $ruleArr['0'] = str_replace('html', '(html)', $ruleArr['0']);
                    $ruleArr['0'] = str_replace('index', '(index)', $ruleArr['0']);
                    //$ruleArr['0'] = '#^' . $ruleArr['0'] . '$#i';
                    $ruleArr['0'] = '@^' . $ruleArr['0'] . '$@i';

                    $ruleArr['1'] = str_replace('/', '\/', $ruleArr['1']);
                    $ruleArr['1'] = str_replace('.', '\.', $ruleArr['1']);
                    $ruleArr['1'] = str_replace('[year]', '[0-9]{4}', $ruleArr['1']);
                    $ruleArr['1'] = str_replace('[lang]', '[a-z]{2}', $ruleArr['1']);
                    $ruleArr['1'] = str_replace('[page]', '[0-9]{1,}', $ruleArr['1']);
                    $ruleArr['1'] = str_replace('[id]', '[0-9]{1,}', $ruleArr['1']);
                    $ruleArr['1'] = str_replace('[subid]', '[0-9]{1,}', $ruleArr['1']);
                    $ruleArr['1'] = str_replace('[catid]', '[0-9]{1,}', $ruleArr['1']);
                    $ruleArr['1'] = str_replace('[testid]', '[0-9]{1,}', $ruleArr['1']);
                    $ruleArr['1'] = str_replace('[slug]', '(.*)', $ruleArr['1']);
                    $ruleArr['1'] = str_replace('html', '(html)', $ruleArr['1']);
                    $ruleArr['1'] = str_replace('index', '(index)', $ruleArr['1']);
                    //$ruleArr['1'] = '#^' . $ruleArr['1'] . '$#i';
                    $ruleArr['1'] = '@^' . $ruleArr['1'] . '$@i';
                    /* echo $ruleArr['1'].' == '.$m.'<br>'; */
                    if (preg_match($ruleArr['0'], $m, $matches)) {
                        break;
                    }
                    if (preg_match($ruleArr['1'], $m, $matches)) {
                        break;
                    }
                }
                if (count(@$matches) > 0) {
                    /* print_r($matches); */
                    $lang = explode('/', @$matches['0']);
                    $slug = (strlen($lang[count($lang) - 1]) == 0) ? $lang[count($lang) - 2] : $lang[count($lang) - 1];
                    $_GET['lang'] = $this->default_lang;
                    if (in_array($lang['0'], $this->langs)) {
                        $_GET['lang'] = $lang['0'];
                    }
                    /* print_r($lang);
                    echo "<br/>AAA".$slug."<br/>"; */
                    $slug = str_replace('.html', '', $slug);
                    $_GET[$this->module_qs] = $this->def_module;
			
                    /* echo $module_tp; */
                    switch ($module_tp) {
                        case 'inside': {
                            $_GET[$this->module_qs] = 'inside';
                            $_GET['id'] = -1;
                            $slug_query = 'SELECT mid as id FROM `' . $this->db->prefix . "menulocalizations` WHERE (`lang`='" . $this->utils->UserGet('lang') . "') && (`slug`='" . $this->utils->url_filter($slug) . "') ";
                            $slug_result = $this->db->query($slug_query);
                            if ($slug_row = $this->db->fetch($slug_result)) {
                                $_GET['id'] = $slug_row['id'];
                                $this->curr_module = 'inside';
                                $this->getNavID($slug_row['id']);
                            } else {
                                $_GET['module_info'] = 'Inside page not found';
                                $_GET[$this->module_qs] = '404';
                            }
                            break;
                        }
                        case 'error404': {
                            $_GET['module_info'] = '404 page';
                            $_GET[$this->module_qs] = '404';
                            break;
                        }
                        case 'index_html':
                        case 'module_homepage': {
                            $_GET[$this->module_qs] = 'main';
                            break;
                        }
                        case 'module_sitemap': {
                            $_GET[$this->module_qs] = 'sitemap';
                            break;
                        }
						case 'module_braincategory': {
                            $_GET[$this->module_qs] = 'mcategory';
                            $repArr = array('cat', 'c', '_');
                            $_GET['catid'] = (int)str_replace($repArr, '', $slug);
                            $_GET['id'] = -1;
                            break;
                        }	
						case 'module_braintest': {
                            $_GET[$this->module_qs] = 'mtest';
                            $repArr = array('cat', 'c', '_');
                            $_GET['testid'] = (int)str_replace($repArr, '', $slug);
                            $_GET['id'] = -1;
                            break;
                        }
                        case 'news': {
                            $_GET[$this->module_qs] = 'news_more';
                            $_GET['id'] = -1;
                            $slug_query = 'SELECT newsid as id FROM `' . $this->db->prefix . "newslocalizations` WHERE (`lang`='" . $this->utils->UserGet('lang') . "') && (`slug`='" . $this->utils->url_filter($slug) . "') ";
                            $slug_result = $this->db->query($slug_query);
                            if ($slug_row = $this->db->fetch($slug_result)) {
                                $_GET['id'] = $slug_row['id'];
                                $this->curr_module = 'news';
                            } else {
                                $_GET['module_info'] = 'News not found';
                                $_GET[$this->module_qs] = '404'; //$this->def_module;
                            }
                            break;
                        }
                        case 'module_news_ns':
                        case 'module_news': {
                            $_GET[$this->module_qs] = 'news';
                            $this->curr_module = 'news';
                            $_GET['page'] = 1;
                            $_GET['id'] = -1;
                            break;
                        }
                        case 'module_news_paging': {
                            $_GET[$this->module_qs] = 'news';
                            $page = $this->utils->filterInt(str_replace("pg", '', $slug));
                            $_GET['page'] = ($page > 0) ? $page : 1;
                            $_GET['id'] = -1;
                            break;
                        }
                        case 'news_cat_ns':
                        case 'news_cat': {
                            $_GET[$this->module_qs] = 'news';
                            $repArr = array('cat', 'c', '_');
                            $_GET['catid'] = (int)str_replace($repArr, '', $slug);
                            $_GET['id'] = -1;
                            break;
                        }                      

                        case 'news_cat_page_ns':
                        case 'news_cat_page': {
                            $_GET[$this->module_qs] = 'news';
                            //echo $slug;
                            $repArr = array('cat', 'c', '_');
                            $slugArr = explode('pg', $slug);
                            $_GET['catid'] = (int)str_replace($repArr, '', $slugArr['0']);
                            $_GET['page'] = (int)str_replace($repArr, '', $slugArr['1']);
                            $_GET['id'] = -1;
                            break;
                        }
                        case 'module_contacts':
                        case 'contacts': {
                            $_GET[$this->module_qs] = 'contacts';
                            $_GET['tp'] = '';
                            $_GET['id'] = -1;
                            break;
                        }
                        case 'contacts_ok': {
                            $_GET[$this->module_qs] = 'contacts';
                            $_GET['tp'] = 'ok';
                            $_GET['id'] = -1;
                            break;
                        }
                        case 'contacts_error': {
                            $_GET[$this->module_qs] = 'contacts';
                            $_GET['tp'] = 'error';
                            $_GET['id'] = -1;
                            break;
                        }
                        case 'module_subscribe': {
                            $_GET[$this->module_qs] = 'subscribe';
                            $_GET['tp'] = '';
                            break;
                        }
                        case 'module_subscribe_ok': {
                            $_GET[$this->module_qs] = 'subscribe';
                            $_GET['tp'] = 'ok';
                            break;
                        }
                        case 'module_subscribe_error': {
                            $_GET[$this->module_qs] = 'subscribe';
                            $_GET['tp'] = 'error';
                            break;
                        }
                        default: {
                            $_GET[$this->module_qs] = $module_tp;
                            break;
                        }
                    }
                } else {
                    $_GET['module_info'] = 'Not found matches';
                    $_GET[$this->module_qs] = '404'; //$this->def_module;
                }
            }
        } else {
            $_GET[$this->module_qs] = $this->def_module;
            $_GET['lang'] = $this->default_lang;
        }
        $this->curr_module = (strlen($this->curr_module) > 0) ? $this->curr_module : $_GET[$this->module_qs];
    }

    /**
     * @method loadModule() Loading module. If module does not exists then load default module
     */
    public function loadModule()
    {
        $mod = $this->utils->UserGet($this->module_qs);

        $module_file = $this->module_folder . $mod . '.view.php';
        if (($mod != '') && strpos($mod, '/') === false && file_exists($module_file)) {
            $this->module = $mod;
        } else {
            $this->module = $this->def_module;
        }
    }

    /**
     * @method insertModule() Including module
     */
    public function insertModule()
    {
        if ($this->module == '') {
            return;
        }
        $module_file = $this->module_folder . $this->module . '.view.php';
        if (file_exists($module_file)) {
            require_once $module_file;
        }
    }

    /**
     * Build Parallax main infrmations.
     */
    public function parallaxMain()
    {
        $toYear = '&mdash;' . date('Y');
        $this->template->assign_vars(array(
            'TITLE' => $this->getKeyLang('site_name', $this->lang, ''),
            'TITLE_UP' => $this->nameup($this->getKeyLang('site_name', $this->lang, '')),
            'SLOGAN' => $this->getKeyLang('site_title', $this->lang, ''),
            'YEAR' => '2012' . $toYear,
            'COPYRIGHT' => '&copy; ' . $this->getKeyLang('meta_author', $this->lang, ''),
            'COPYRIGHT_INFO' => $this->fromLangIndex('copyright_info'),
            'SITE_COPYRIGHT' => str_replace('[YEAR]', date('Y'), $this->getKeyLang('site_copyright', $this->lang, '[YEAR]')),
            'CHARSET' => $this->getKey('charset', 'utf-8'),
            'AUTHOR' => $this->getKeyLang('meta_author', $this->lang, ''),
            'KEYWORDS' => $this->getKeyLang('meta_keywords', $this->lang, ''),
            'DESCRIPTION' => $this->getKeyLang('meta_description', $this->lang, ''),
            'META_EMAIL' => $this->getKeyLang('meta_email', $this->lang, ''),
            'LANGUAGE' => $this->fromLangIndex('language'),
            'LANG_QS' => $this->lang_qs,
            'MODULE_QS' => $this->module_qs,
            'CURR_LANG' => $this->lang,
            'CURR_LANG_UP' => $this->nameup($this->lang),
            'BODY_CLASS' => ($this->module != 'main') ? ' class="grey"' : ' class="home"',
        ));
    }

    /**
     * Build Menu from section manager.
     */
    public function buildMenu()
    {
        $this->buildDBMenu('bottom_menu');
        $this->buildDBMenu('top_menu');
        $this->buildDBMenu('middle_menu');
        $this->buildDBMenu('foot_menu');
        $this->buildLangs();

        $this->loadModuleCls();

        $news_index_limit = $this->getKey('news_index_limit', 5);
        //$this->buildCatNews('indexnews', ' && (N.index = 1) ', ' && (N.index = 1) ', $news_index_limit, ' ORDER BY N.newsdate DESC, N.newsid DESC ', ' ORDER BY N.newsdate ASC, N.newsid DESC ');
        $this->buildNews('indexnews', ' && (N.`index` = 1) && (NC.`event` = 0) ', $news_index_limit, ' ORDER BY N.`newsdate` DESC, N.`newsid` DESC ');
        $this->buildNews('indexevents', ' && (N.`index` = 1) && (NC.`event` = 1) ', $news_index_limit, ' ORDER BY N.`newsdate` ASC, N.`newsid` DESC ', 'events');
        //buildCatNews($template_name = "news", $cat_sql = "", $news_sql = "", $news_limit = 3, $news_order = " ORDER BY N.newsdate DESC, N.newsid DESC ", $events_order = " ORDER BY N.newsdate ASC, N.newsid DESC ")
        //buildNews($template_name = "news", $where_sql = "", $news_limit = 3, $news_order = " ORDER BY N.newsdate DESC, N.newsid DESC ")

		$this->highlight();
        $this->listBlocks();
        $this->buildContactsText();
        if($this->module != 'main') $this->buildCats();
		$this->isLogged();
        $this->buildSubscribe();
        $search_text = $this->utils->UserSearchPost('search_text');

        $this->template->assign_vars(array(
            'SEARCH_TEXT' => $search_text,
            'CURR_YEAR' => date('Y'),
            'INDEX_URL' => $this->curr_folder . str_replace('[lang]', $this->lang, $this->permalinks[$this->perma_type]['index'][$this->curr_lang]),
            'CONTACTS_URL' => $this->curr_folder . str_replace('[lang]', $this->lang, $this->permalinks[$this->perma_type]['module_contacts'][$this->curr_lang]),
            'NEWS_URL' => $this->curr_folder . str_replace('[lang]', $this->lang, $this->permalinks[$this->perma_type]['module_news'][$this->curr_lang]),
            'SEARCH_URL' => $this->curr_folder . str_replace('[lang]', $this->lang, $this->permalinks[$this->perma_type]['search'][$this->curr_lang]),
        ));
    }



    public function isLogged() {

        $this->prf_logged = $this->utils->filterInt($this->utils->GetSession($this->session_logged));
        $this->prf_logged_id = $this->utils->filterInt($this->utils->GetSession($this->session_userid));
        $this->prf_usertype = $this->utils->GetSession($this->session_usertype);

        if ($this->prf_logged != 9) {

            $this->template->assign_block_vars('member_not_logged', array(
                'TITLE' => $this->fromLangIndex('profile'),
            ));

            /*$this->template->assign_block_vars('member_not_logged.links', array(
                'TEACHER_URL' => 'teacher.'.$this->curr_folder,
            ));
			*/

        }
        else {

            $this->template->assign_block_vars('member_logged', array(
                'TITLE' => $this->fromLangIndex('profile'),
            ));

			/*
				$this->template->assign_block_vars('member_logged.links', array(
					'LOGOUT_URL' => $this->curr_folder . str_replace('[lang]', $this->lang, $this->permalinks[$this->perma_type]['module_member_logout'][$this->curr_lang]),
					'PROFILE_URL' => $this->curr_folder . str_replace('[lang]', $this->lang, $this->permalinks[$this->perma_type]['module_teacher_dashboard'][$this->curr_lang]),
				));
			*/
        }
    }


    public function logout() {

        /* B: OK */
        $this->utils->RemoveSession($this->session_logged);
        $this->utils->RemoveSession($this->session_email);
        $this->utils->RemoveSession($this->session_userid);
        $this->utils->RemoveSession($this->session_usertype);
        $this->utils->RemoveSession('otpcode');
        $this->utils->RemoveSession('phone');
        $this->utils->RemoveSession('usertype');
        $this->utils->RemoveSession('rid');
        $this->utils->RemoveSession('extraDetail');
      
    }



    /**
     * Load module config classes.
     */
    public function loadModuleCls()
    {
        $sql = 'SELECT ML.*
        FROM `' . $this->db->prefix . 'modules` ML
        WHERE
            (ML.`have_view` = 1)
        ';
        $result = $this->db->query($sql);
        while ($row = $this->db->fetch($result)) {
            $md_name = $row['name'];
            //$this->$md_name = '';

            $have_cnfg = $row['have_cnfg'];
            if ($have_cnfg == 1) {
                //$this->$md_name = $module_form_builder;
                @require iFolded . 'm/modules/' . $md_name . '.cls.php';
            }
        }
    }

    /**
     * buildDBMenu Method
     * Show menu.
     *
     *
     * @param string $menu_value
     */
    public function buildDBMenu($menu_value = 'top_menu')
    {
        $sql = 'SELECT
					M.mid
			FROM `' . $this->db->prefix . "menu` M
			WHERE
				( M.`parentid` = 0 )
			AND ( M.`visible`  = 1 )
			AND ( M.`menutype` = '" . $menu_value . "' )
			ORDER BY M.`position`
		";
        $result = $this->db->query($sql);
        if ($row = $this->db->fetch_assoc($result)) {
            /** parent menu id */
            $parentid = $row['mid'];

            /** from get ID */
            $id = $this->utils->UserGetInt('id');

            /** Menu index  */
            $indx = 0;

            $sub_query = 'SELECT
					M.*,
					ML.name,
					ML.comment,
					ML.text,
					ML.header,
					ML.slug,
					ML.link as lang_link,
					COUNT(MS.mid) AS sub_counts
				FROM `' . $this->db->prefix . 'menu` M
				LEFT JOIN `' . $this->db->prefix . 'menulocalizations` ML ON M.mid = ML.mid
				LEFT JOIN `' . $this->db->prefix . "menu` MS ON (M.visible='1') AND (MS.parentid = M.mid)
				WHERE
					( M.`visible` = 1 )
				AND ( M.`parentid`= " . $parentid . " )
				AND ( ML.`lang`	 = '" . $this->lang . "' )
				AND ( ML.`name`	!='' )
				GROUP BY M.`mid`
				ORDER BY M.`position`
			";
            $sub_result = $this->db->query($sub_query);
            $num_rows = $this->db->num_result_rows($sub_result);
            while ($sub_row = $this->db->fetch_assoc($sub_result)) {
                ++$indx;
                $mid = $sub_row['mid'];

                $this->blockRow($menu_value, $sub_row, $indx, $num_rows, $id);

                /** If have sub menus */
                $sub_counts = $sub_row['sub_counts'];
                if ($sub_counts > 0) {
                    $this->buildSubMenu($menu_value, $mid, 1, $id);
                }
            }
        }
    }

    /** Section manager: inside, menu */
    /**
     * buildSubMenu Method
     * Show Sub menu.
     *
     *
     * @param string $menu_value
     * @param int $parentid
     * @param int $level
     * @param int $id
     */
    public function buildSubMenu($menu_value = 'top_menu', $parentid = 0, $level = 1, $id = 0)
    {
        $levelBlock = ($level == 1) ? '' : $level;
        $levelIn = ++$level;
        $si = 0;
        $sub_query = 'SELECT
				M.*,
				ML.name,
				ML.comment,
				ML.text,
				ML.header,
				ML.slug,
				ML.link as lang_link,
				COUNT(MS.mid) AS sub_counts
			FROM `' . $this->db->prefix . 'menu` M
			LEFT JOIN `' . $this->db->prefix . 'menulocalizations` ML ON ML.mid = M.mid
			LEFT JOIN `' . $this->db->prefix . "menu` MS ON (M.visible='1') AND (MS.parentid = M.mid)
			WHERE
				( M.`visible` = 1 )
			AND ( M.`parentid`= " . $parentid . " )
			AND ( ML.`lang`	  = '" . $this->lang . "' )
			AND ( ML.`name`	  != '' )
			GROUP BY M.`mid`
			ORDER BY M.`position`
		";
        $sub_result = $this->db->query($sub_query);
        $num_rows = $this->db->num_result_rows($sub_result);
        while ($sub_row = $this->db->fetch($sub_result)) {
            ++$si;
            $mid = $sub_row['mid'];
            if ($si == 1) {
                $this->template->assign_block_vars($menu_value . '.sub' . $levelBlock, array());
            }

            if ($sub_row['menutype'] == 'nolink') {
                $this->blockRow($menu_value . '.sub' . $levelBlock . '.nolink' . $levelBlock, $sub_row, $si, $num_rows, $id);
            } else {
                $this->blockRow($menu_value . '.sub' . $levelBlock . '.menu' . $levelBlock, $sub_row, $si, $num_rows, $id);
                $this->blockRow($menu_value . '.sub_menu' . $levelBlock, $sub_row, $si, $num_rows, $id);

                /** If have sub menus */
                $sub_counts = $sub_row['sub_counts'];
                if ($sub_counts > 0) {
                    $this->buildSubMenu($menu_value . '.sub' . $levelBlock . '.menu' . $levelBlock, $mid, $levelIn, $id);
                }
            }
        }
    }

    /**
     * Menu show.
     *
     * @param string $blockname
     * @param array $row (mid
     *                           parentid
     *                           name
     *                           comment
     *                           text
     *                           newwindow (0, 1)
     *                           menutype
     *                           link
     *                           lang_link)
     * @param int $curr_index
     * @param int $num_rows
     * @param int $id
     */
    public function blockRow($blockname = 'top_menu', $row = array('mid' => 0, 'parentid' => 0, 'name' => '', 'comment' => '', 'text' => '', 'newwindow' => 0, 'menutype' => 'content', 'link' => '', 'lang_link' => ''), $curr_index = 1, $num_rows = 0, $id = 0)
    {
        /** ID. From menu table */
        $mid = $row['mid'];

        /** newwindow for open link in new window. From menu table */
        $blank = ($row['newwindow'] == 1) ? ' onclick="window.open(this.href,\'_blank\');return false;"' : '';

        /** Build URL */
        $url = $this->buildSectionUrl($row);

        $this->template->assign_block_vars($blockname, array(
            'ID' => $mid,
            'SLUG' => $row['slug'],
            'NAME' => $row['name'],
            'NAME_UP' => $this->nameup($row['name']),
            'TITLE' => $row['name'],
            'COMMENT' => $row['comment'],
            'TEXT' => $row['text'],
            'BLANK' => $blank,
            'URL' => $url,
        ));

        /* if menu id is current id */
        $curr_chck = ($mid == $id);
        /* if id is 0 and index is 1 */
        $curr_chck = (($curr_chck) || (($id == 0) && ($curr_index == 1) && ($this->curr_module == 'main')));
        /* if id is parentid */
        //$curr_chck = (($curr_chck) || (false /* Developer mode  */));
        $curr_chck = (($curr_chck) || (in_array($mid, $this->idArr)));
        /* if curr module */
        $curr_chck = (($curr_chck) || (($row['menutype'] == 'module') && ($this->curr_module == $row['link'])));
        if ($curr_chck) {
            $this->template->assign_block_vars($blockname . '.curr_class', array());
        } else {
            $this->template->assign_block_vars($blockname . '.norm_class', array());
        }

        /* If Menu item is first */
        if ($curr_index == 1) {
            $this->template->assign_block_vars($blockname . '.first_class', array());
        }
        /* If Menu item is middle */
        if (($curr_index != $num_rows) && ($curr_index != 1)) {
            $this->template->assign_block_vars($blockname . '.middle_class', array());
        }

        /* If Menu item is last */
        if ($curr_index == $num_rows) {
            $this->template->assign_block_vars($blockname . '.last_class', array());
        }
    }

    public function buildSectionUrl($row = array('mid' => 0, 'parentid' => 0, 'name' => '', 'comment' => '', 'text' => '', 'newwindow' => 0, 'menutype' => 'content', 'link' => '', 'lang_link' => '')) {

        /** ID. From menu table */
        $mid = $row['mid'];

        /** parentID. From menu table */
        //$parentid = $row['parentid'];

        /** Language */
        $lang_ext = $this->lang;

        /** Protocol operations */
        /** link from menu table */
        $protocolsLnk = false;
        $siteLink = $row['link'];
        /** link from menulocalizations table */
        $protocolsLng = false;
        $siteLangLink = $row['lang_link'];
        foreach ($this->protocolsArr as $protocolsURL) {
            if (strpos($siteLangLink, $protocolsURL) !== false) {
                $protocolsLng = true;
                break;
            }
        }

        /** menutype. From menu table */
        $menutype = $row['menutype'];

        $url = $this->curr_folder;

        switch ($menutype) {
            case 'content': {
                /*
                 * If menu is content
                 */
                $url .= str_replace('[slug]', $this->get_slug($mid, $lang_ext), str_replace('[name]', $this->utils->url_filter($row['name']), str_replace('[id]', $mid, str_replace('[lang]', $lang_ext, $this->permalinks[$this->perma_type]['inside'][$this->curr_lang]))));
                break;
            }
            case 'link': {
                /*
                 * if menu type is link
                 */
                foreach ($this->protocolsArr as $protocolsURL) {
                    if (strpos($siteLink, $protocolsURL) !== false) {
                        $protocolsLnk = true;
                        break;
                    }
                }
                $siteLinkInt = (int)$siteLink;
                $url = ($protocolsLnk) ? str_replace('{LANG}', $this->lang, $siteLink) : $this->curr_folder . str_replace('{LANG}', $this->lang, $siteLink);
                if (($siteLinkInt > 0) && ($siteLinkInt != $mid)) {
                    $sub_query = 'SELECT
                            M.*,
                            ML.name,
                            ML.comment,
                            ML.text,
                            ML.header,
                            ML.slug,
                            ML.link as lang_link,
                            COUNT(MS.mid) AS sub_counts
                        FROM `' . $this->db->prefix . 'menu` M
                        LEFT JOIN `' . $this->db->prefix . 'menulocalizations` ML ON ML.mid = M.mid
                        LEFT JOIN `' . $this->db->prefix . "menu` MS ON (M.visible='1') AND (MS.parentid = M.mid)
                        WHERE
                            ( M.`visible` = 1 )
                        AND ( M.`mid`= " . $siteLinkInt . " )
                        AND ( ML.`lang`	  = '" . $this->lang . "' )
                        AND ( ML.`name`	  != '' )
                        GROUP BY M.`mid`
                    ";
                    $sub_result = $this->db->query($sub_query);
                    if ($sub_row = $this->db->fetch($sub_result)) {
                        $url = $this->buildSectionUrl($sub_row);
                    }
                }

                break;
            }
            case 'module': {
                /**
                 * if menu type is module.
                 */
                $md_name = $row['link'];
                $module_name = 'module_' . $md_name;
                if (array_key_exists($module_name, $this->permalinks[$this->perma_type])) {
                    $url .= str_replace('[lang]', $this->lang, $this->permalinks[$this->perma_type][$module_name][$this->curr_lang]);
                } else {
                    //print_r($this->$md_name->permalinks[$this->perma_type]);
                    if (array_key_exists($module_name, $this->$md_name->permalinks[$this->perma_type])) {
                        $url .= str_replace('[lang]', $this->lang, $this->$md_name->permalinks[$this->perma_type][$module_name][$this->curr_lang]);
                    }
                }
                break;
            }
        }

        $url = (($protocolsLng) && (!$protocolsLnk) && (strlen($siteLangLink) > 11)) ? $siteLangLink : ((!$protocolsLng) ? $this->curr_folder . $this->lang . '/' . $siteLangLink : $url);

        return $url;
    }

    /** News manager */

    /**
     *  Show news by category
     *
     * @param string $template_name
     * @param string $cat_sql
     * @param string $news_sql
     * @param integer $news_limit
     * @param string $news_order
     * @param string $events_order
     */
    public function buildCatNews($template_name = 'news', $cat_sql = '', $news_sql = '', $news_limit = 3, $news_order = ' ORDER BY N.newsdate DESC, N.newsid DESC ', $events_order = ' ORDER BY N.newsdate ASC, N.newsid DESC ')
    {
        $this->news_date_format = $this->getKey('news_date_format', 'd.m.Y');
        $events_days_limit = $this->fromConfig('events_days_limit');
        $this->template->assign_block_vars($template_name, array());
        $cat_query = 'SELECT
          N.*,
          NL.name AS cat_name,
          NL.header AS cat_header
        FROM ' . $this->db->prefix . 'newscategories N
		INNER JOIN ' . $this->db->prefix . "newscategorylocalizations NL ON NL.catid = N.catid
		WHERE
		    (N.active = 1)
		 && (NL.lang='" . $this->lang . "')
		 && (NL.name!='')
		    " . $cat_sql . '
		ORDER BY N.position ASC
		';
        $cat_idx = 0;
        $num_cats = $this->db->num_rows($cat_query);
        $cat_result = $this->db->query($cat_query);
        while ($cat_row = $this->db->fetch($cat_result)) {
            $catid = $cat_row['catid'];
            ++$cat_idx;
            $this->buildNewsCatInfo($template_name . '.blocks', $cat_row, $cat_idx, $num_cats);

            $orderby = ($cat_row['event'] == 1) ? $events_order : $news_order;
            $neSql = ($cat_row['event'] == 1) ? ' && (N.newsdate>=' . (time() - $events_days_limit) . ') ' : ' && (N.newsdate<=' . time() . ') ';
            $news_query = 'SELECT
              N.*,
              NL.name,
              NL.header,
              NL.comment,
              NL.slug
            FROM ' . $this->db->prefix . 'news N
            INNER JOIN ' . $this->db->prefix . "newslocalizations NL ON NL.newsid = N.newsid
            WHERE
                (N.active='1')
             && (N.catid = " . $catid . ")
             && (NL.lang='" . $this->lang . "')
             && (NL.name!='')
		     && (N.newsdate <= " . time() . ')
             ' . $neSql . '
             ' . $news_sql . '
            ' . $orderby . $this->db->get_limit(0, $news_limit);
            $news_idx = 0;
            $num_news = $this->db->num_rows($news_query);
            $news_result = $this->db->query($news_query);
            while ($news_row = $this->db->fetch_assoc($news_result)) {
                ++$news_idx;
                $this->buildNewsInfo($template_name . '.blocks.items', $news_row, $news_idx, $num_news);
                $this->buildNewsInfo($template_name . '.items', $news_row, $news_idx, $num_news);
            }
        }
    }

    /**
     *  Show news
     *
     * @param string $template_name
     * @param string $where_sql
     * @param integer $news_limit
     * @param string $news_order
     */
    public function buildNews($template_name = 'news', $where_sql = '', $news_limit = 3, $news_order = ' ORDER BY N.newsdate DESC, N.newsid DESC ', $n_type = "news")
    {
        $this->news_date_format = $this->getKey('news_date_format', 'd.m.Y');
        $this->template->assign_block_vars($template_name, array());
        $ndate = ($n_type == "news") ? " && (N.newsdate <= " . time() . ") " : " && (N.newsdate >= " . time() . ") ";
        $news_query = "SELECT
          N.*,
          NL.name,
          NL.header,
          NL.comment,
          NL.slug,
          NCL.name as cat_name,
          NCL.header AS cat_header
        FROM " . $this->db->prefix . "news N
		INNER JOIN " . $this->db->prefix . "newslocalizations NL ON NL.newsid = N.newsid
		INNER JOIN " . $this->db->prefix . "newscategories NC ON NC.catid = N.catid
		INNER JOIN " . $this->db->prefix . "newscategorylocalizations NCL ON (NCL.catid = NC.catid) && (NCL.lang='" . $this->lang . "')
		WHERE
		    (N.active='1')
		 && (NL.lang='" . $this->lang . "')
		 && (NL.name!='')
		 " . $ndate . "
		 " . $where_sql . "
		 " . $news_order . $this->db->get_limit(0, $news_limit);
        $news_idx = 0;
        $num_news = $this->db->num_rows($news_query);
        $news_result = $this->db->query($news_query);
        $catArr = array();
        while ($news_row = $this->db->fetch_assoc($news_result)) {
            $catid = $news_row['catid'];
            ++$news_idx;
            if (!in_array($catid, $catArr)) {
                $catArr[] = $catid;
                $this->buildNewsCatInfo($template_name . '.blocks', $news_row, count($catArr), -1);
            }
            $this->buildNewsInfo($template_name . '.blocks.items', $news_row, $news_idx, $num_news);
            $this->buildNewsInfo($template_name . '.items', $news_row, $news_idx, $num_news);
        }
    }

    /**
     * Build categry news
     *
     * @param string $template_name
     * @param $cat_row
     * @param $cat_idx
     * @param $num_cats
     */
    public function buildNewsCatInfo($template_name = 'news', $cat_row = array('catid' => 0, 'cat_name' => '', 'cat_header' => ''), $cat_idx = 1, $num_cats = 0)
    {
        $catid = $cat_row['catid'];
        $this->template->assign_block_vars($template_name, array(
            'INDX' => $cat_idx,
            'CATID' => $catid,
            'CAT_NAME' => $cat_row['cat_name'],
            'CAT_NAME_UP' => $this->nameup($cat_row['cat_name']),
            'CAT_TEXT' => $cat_row['cat_header'],
            'CAT_URL' => $this->curr_folder . str_replace('[lang]', $this->lang, str_replace('[catid]', $catid, $this->permalinks[$this->perma_type]['news_cat'][$this->curr_lang])),
        ));

        /* If Cat item is first */
        if ($cat_idx == 1) {
            $this->template->assign_block_vars($template_name . '.first_class', array());
        }
        /* If Cat item is middle */
        if (($cat_idx != $num_cats) && ($cat_idx != 1)) {
            $this->template->assign_block_vars($template_name . '.middle_class', array());
        }
        /* If Cat item is last */
        if ($cat_idx == $num_cats) {
            $this->template->assign_block_vars($template_name . '.last_class', array());
        }
    }

    /**
     * build News info
     *
     * @param string $template_name
     * @param $news_row
     * @param $news_idx
     * @param $num_news
     */
    public function buildNewsInfo($template_name = 'news', $news_row = array('newsid' => 0, 'name' => '', 'header' => '', 'comment' => '', 'slug' => ''), $news_idx = 1, $num_news = 0)
    {
        $newsid = $news_row['newsid'];
        $comment = $this->site->f_tags($news_row['comment']);
        $comment = $this->site->replaceImageTemplate($comment, $this->curr_folder);
        $comment = $this->contentUsibility($comment);
        $monthArr = $this->fromLangIndex('monthArr');
        $monthNum = date('n', $news_row['newsdate']) - 1;
        $imgid = (int)$news_row['header'];
        $this->template->assign_block_vars($template_name, array(
            'INDX' => $news_idx,
            'NEWSID' => $newsid,
            'IMGID' => $imgid,
            'DATE' => date($this->news_date_format, $news_row['newsdate']),
            'DATE_DD' => date('d', $news_row['newsdate']),
            'DATE_MM' => date('m', $news_row['newsdate']),
            'DATE_MONTH' => date('M', $news_row['newsdate']),
            'DATE_MONTH_LANG' => $monthArr[$monthNum],
            'DATE_YYYY' => date('Y', $news_row['newsdate']),
            'DATE_HM' => date('H:i', $news_row['newsdate']),
            'NAME' => $news_row['name'],
            'NAME_UP' => $this->nameup($news_row['name']),
            'TITLE' => $news_row['name'],
            'COMMENT' => $comment,
            'HEADER' => $news_row['header'],
            'URL' => $this->curr_folder . str_replace('[slug]', $this->utils->url_filter($news_row['slug']), str_replace('[year]', date('Y', $news_row['newsdate']), str_replace('[name]', $this->utils->url_filter($news_row['name']), str_replace('[id]', $newsid, str_replace('[lang]', $this->lang, $this->permalinks[$this->perma_type]['news'][$this->curr_lang]))))),
            'NEWS_URL' => $this->curr_folder . str_replace('[slug]', $this->utils->url_filter($news_row['slug']), str_replace('[year]', date('Y', $news_row['newsdate']), str_replace('[name]', $this->utils->url_filter($news_row['name']), str_replace('[id]', $newsid, str_replace('[lang]', $this->lang, $this->permalinks[$this->perma_type]['news'][$this->curr_lang]))))),
        ));
        if ($imgid > 0) {
            $img_file = $this->curr_folder . $this->site->getFile($imgid);
            $this->template->assign_block_vars($template_name . '.img', array(
                'IMGID' => $imgid,
                'IMG_FILE' => $img_file,
            ));
        }
        /* If news item is first */
        if ($news_idx == 1) {
            $this->template->assign_block_vars($template_name . '.first_class', array());
        }
        /* If newsitem is middle */
        if (($news_idx != $num_news) && ($news_idx != 1)) {
            $this->template->assign_block_vars($template_name . '.middle_class', array());
        }
        /* If news item is last */
        if ($news_idx == $num_news) {
            $this->template->assign_block_vars($template_name . '.last_class', array());
        }
    }

    /**
     * name Upercase
     *
     * @param string $name
     */
    public function nameup($name)
    {
        if ($this->lang == 'az') {
            $orgArr = array('ə', 'i', 'ı', 'ğ', 'ç', 'ş');
            $repArr = array('Ə', 'İ', 'I', 'Ğ', 'Ç', 'Ş');
            $name = str_replace($orgArr, $repArr, $name);
        }
        $name = strtoupper($name);
        //$name = mb_convert_case($name, MB_CASE_UPPER, 'UTF-8');

        return $name;
    }

    /**
     * name First Alphabet Upercase
     *
     * @param string $name
     */
    public function namefirstup($name)
    {
        $name = trim($name);
        $fc = mb_strtoupper(mb_substr($name, 0, 1));
        return $fc . mb_substr($name, 1);
        //$name = ucfirst($name);
    }
	
    /**
     * Set index language settings for module {_LANG_VALUE_}
     */
    public function buildLangs()
    {
        foreach ($GLOBALS['language']['index']['view'] as $key => $txt) {
            $this->template->assign_var('_' . strtoupper($key) . '_', $txt);
        }
    }

    public function generateSearchToken()
    {
        $stoken = (array_key_exists('stoken', $_SESSION)) ? $this->utils->dataFullFilter($_SESSION['stoken']) : '';
        if ((@$_SERVER['REQUEST_METHOD'] != 'POST') && (strlen($stoken) < 5)) {
            //echo "<br/>aaaaaa<br/>";
            $stoken = sha1(uniqid(rand(787856, time()), true));
            $_SESSION['stoken'] = $stoken;
            $this->template->assign_vars(array(
                'STOKEN' => $stoken,
            ));
        } else {
            $this->template->assign_vars(array(
                'STOKEN' => $stoken,
            ));
        }
    }

    public function generateToken()
    {
        if (@$_SERVER['REQUEST_METHOD'] != 'POST') {
            $token = (array_key_exists('token', $_SESSION)) ? $this->utils->dataFullFilter($_SESSION['token']) : '';
            $token = (strlen($token) > 5) ? $token : sha1(uniqid(rand(962374, time()), true));
            $_SESSION['token'] = $token;
            $this->template->assign_vars(array(
                'TOKEN' => $token,
            ));
        } else {
            $token = $this->utils->dataFullFilter($_SESSION['token']);
            $this->template->assign_vars(array(
                'TOKEN' => $token,
            ));
        }
    }

    public function contentUsibility($text, $tm = '')
    {
        $gallery_code = $this->fromConfig('gallery_code');
        $pattern = '/' . str_replace('[id]', "(\d+)", $gallery_code) . '/i';
        preg_match_all($pattern, $text, $matches, PREG_SET_ORDER);
        foreach ($matches as $id) {
            foreach ($id as $id1) {
                $this->getGallery($id1, $tm);
            }
        }
        $text = preg_replace_callback($pattern, function () {
            return '';
        }, $text);

        return $text;
    }

    /**
     * get Gallery
     *
     * @param $gal_id
     * @param $tm
     */
    public function getGallery($gal_id, $tm = '')
    {
        $gal_id = (int)$gal_id;
        $div_txt = '';
        if ($gal_id > 0) {
            $query = "SELECT 
              GL.* 
            FROM `" . $this->db->prefix . "gallery` G
			INNER JOIN `" . $this->db->prefix . "gallerylocalizations` GL ON GL.gid=G.gid
			WHERE
				(GL.`lang` = '" . $this->lang . "')
			 && (G.`gid` = " . $gal_id . ")
			 && (G.`active` = 1)
			";
            $result = $this->db->query($query);
            while ($row = $this->db->fetch($result)) {
                $gallery_query = 'SELECT * FROM ' . $this->db->prefix . 'gallery_images WHERE gid=' . $gal_id . ' ORDER BY `imgid`';
                $gallery_nums = $this->db->num_rows($gallery_query);
                $this->template->assign_block_vars($tm . 'gallery', array(
                    'NAME' => $row['galleryname'],
                    'NAME_UP' => $this->nameup($row['galleryname']),
                    'DESC' => $row['gallery_desc'],
                    'DESC_NOTAG' => strip_tags($row['gallery_desc']),
                    'GID' => $row['gid'],
                    'X' => $gallery_nums,
                ));
                $ndx = 1;
                $indx = 1;
                $gallery_result = $this->db->query($gallery_query);
                while ($gallery_row = $this->db->fetch($gallery_result)) {
                    $id = $gallery_row['imgid'];

                    $folder_x612 = $this->io->dateFolder('img_gallery/img_x612/', $gallery_row['img_date']);
                    $img_x612 = $folder_x612 . '/' . $id . '.jpg';
                    $check_x612 = (!@is_file($img_x612)) ? '<img src="' . $this->curr_folder . 'gallery_img.php?i=' . $id . '&h=612" style="width:0px;height:0px;">' : '';

                    $folder_150x150 = $this->io->dateFolder('img_gallery/img_150x150/', $gallery_row['img_date']);
                    $img_150x150 = $folder_150x150 . '/' . $id . '.jpg';
                    $check_150x150 = (!@is_file($img_150x150)) ? '<img src="' . $this->curr_folder . 'gallery_img.php?i=' . $id . '&w=150&h=150" style="width:0px;height:0px;">' : '';

                    $folder_612x = $this->io->dateFolder('img_gallery/img_612x/', $gallery_row['img_date']);
                    $img_612x = $folder_612x . '/' . $id . '.jpg';
                    $check_612x = (!@is_file($img_612x)) ? '<img src="' . $this->curr_folder . 'gallery_img.php?i=' . $id . '&w=612" style="width:0px;height:0px;">' : '';

                    $img_110x54 = $this->curr_folder . 'gallery_img.php?i=' . $id . '&w=110&h=54';
                    $img_710x350 = $this->curr_folder . 'gallery_img.php?i=' . $id . '&w=710&h=350';

                    //$check_x612 = $check_150x150 = '';
                    $ndx12 = rand(1, 2);
                    $ndx12 = (((($indx - 1) % 4) == 0) || (($indx % 4) == 0)) ? 1 : $ndx12;
                    $indx += $ndx12;
                    $this->template->assign_block_vars($tm . 'gallery.list', array(
                        'ID' => $id,
                        'IMGID' => $id,
                        'NDX' => ($ndx++),
                        'NDX12' => $ndx12,
                        'CHECK_X612' => $check_x612,
                        'IMG_X612' => $this->curr_folder . $img_x612,
                        'CHECK_612X' => $check_612x,
                        'IMG_612X' => $this->curr_folder . $img_612x,
                        'CHECK_150X150' => $check_150x150,
                        'IMG_150X150' => $this->curr_folder . $img_150x150,
                        'IMG_110X54' => $this->curr_folder . $img_110x54,
                        'IMG_710X350' => $this->curr_folder . $img_710x350,
                    ));
                }
            }
        }

        return $div_txt;
    }

    /**
     * list Blocks
     */
    public function listBlocks()
    {
        $cat_query = 'SELECT
          B.*,
          BL.`name` AS cat_name,
          BL.`more_txt`,
          BL.`imgid`,
          BL.`info`,
          BL.`embed_code`,
          BL.`url`
        FROM ' . $this->db->prefix . 'blockscategories B
		INNER JOIN ' . $this->db->prefix . "blockscategorylocalizations BL ON BL.catid = B.catid
		WHERE
		    (B.active = 1)
		 && (BL.lang='" . $this->lang . "')
		ORDER BY B.position ASC
		";
        $cat_idx = 0;
        $num_cats = $this->db->num_rows($cat_query);
        $cat_result = $this->db->query($cat_query);
        while ($cat_row = $this->db->fetch($cat_result)) {
            ++$cat_idx;
            $cat_id = $cat_row['catid'];
            $this->buildBlockCatInfo('blocks', $cat_row, $cat_idx, $num_cats);
            $this->buildBlockCatInfo('blocks_' . $cat_id, $cat_row, $cat_idx, $num_cats);

            $item_query = 'SELECT
              B.*,
              BL.name,
              BL.link_name,
              BL.text,
              BL.img,
              BL.img02,
              BL.img_url,
              BL.`url`
            FROM ' . $this->db->prefix . 'blocks B
			INNER JOIN ' . $this->db->prefix . "blockslocalizations BL ON BL.blocksid = B.blocksid
			WHERE
			    (B.active='1')
			 && (BL.lang='" . $this->lang . "')
			 && (B.catid=" . $cat_id . ')
			ORDER BY B.position ASC
			';
            $item_idx = 0;
            $num_items = $this->db->num_rows($item_query);
            $item_result = $this->db->query($item_query);
            while ($item_row = $this->db->fetch($item_result)) {
                if ($item_idx % 2 == 0) {
                    $this->buildBlockCatInfo('blocks.row_2', $cat_row, $cat_idx, $num_cats);
                    $this->buildBlockCatInfo('blocks_' . $cat_id . '.row_2', $cat_row, $cat_idx, $num_cats);
                }
                if ($item_idx % 3 == 0) {
                    $this->buildBlockCatInfo('blocks.row_3', $cat_row, $cat_idx, $num_cats);
                    $this->buildBlockCatInfo('blocks_' . $cat_id . '.row_3', $cat_row, $cat_idx, $num_cats);
                }
                if ($item_idx % 4 == 0) {
                    $this->buildBlockCatInfo('blocks.row_4', $cat_row, $cat_idx, $num_cats);
                    $this->buildBlockCatInfo('blocks_' . $cat_id . '.row_4', $cat_row, $cat_idx, $num_cats);
                }
                if ($item_idx % 5 == 0) {
                    $this->buildBlockCatInfo('blocks.row_5', $cat_row, $cat_idx, $num_cats);
                    $this->buildBlockCatInfo('blocks_' . $cat_id . '.row_5', $cat_row, $cat_idx, $num_cats);
                }
                ++$item_idx;
                $this->buildBlockInfo('blocks.items', $item_row, $item_idx, $num_items);
                $this->buildBlockInfo('blocks.row_2.items', $item_row, $item_idx, $num_items);
                $this->buildBlockInfo('blocks.row_3.items', $item_row, $item_idx, $num_items);
                $this->buildBlockInfo('blocks.row_4.items', $item_row, $item_idx, $num_items);
                $this->buildBlockInfo('blocks.row_5.items', $item_row, $item_idx, $num_items);
                $this->buildBlockInfo('blocks_' . $cat_id . '.items', $item_row, $item_idx, $num_items);
                $this->buildBlockInfo('blocks_' . $cat_id . '.row_2.items', $item_row, $item_idx, $num_items);
                $this->buildBlockInfo('blocks_' . $cat_id . '.row_3.items', $item_row, $item_idx, $num_items);
                $this->buildBlockInfo('blocks_' . $cat_id . '.row_4.items', $item_row, $item_idx, $num_items);
                $this->buildBlockInfo('blocks_' . $cat_id . '.row_5.items', $item_row, $item_idx, $num_items);
            }
        }
    }
	
	
    /**
     * build Cats
     */
    public function buildCats()
    {	
        $cat_query = 'SELECT
          B.*,
          BL.`name` AS cat_name
        FROM ' . $this->db->prefix . 'questionscategories B
		INNER JOIN ' . $this->db->prefix . "questionscategorylocalizations BL ON BL.catid = B.catid
		WHERE
		    (B.active = 1) && (B.index = 1)
		 && (BL.lang='" . $this->lang . "')
		ORDER BY B.position ASC
		";

        $cat_idx = 0;
        $num_cats = $this->db->num_rows($cat_query);
        $cat_result = $this->db->query($cat_query);
		$catid = $this->utils->UserGetInt('catid');
        while ($cat_row = $this->db->fetch($cat_result)) {

			if ($cat_idx == 0) {
                $this->template->assign_block_vars('cats', array());
            }
			++$cat_idx;
            $cat_id = $cat_row['catid'];
			$act_class = '';
			
			if($this->module == 'mcategory' && $catid == $cat_id)
				$act_class='active';
            $name = $cat_row['cat_name'];
			$url = $this->curr_folder . str_replace('[lang]', $this->lang, str_replace('[catid]', $cat_id, $this->permalinks[$this->perma_type]['module_braincategory'][$this->curr_lang]));

            $this->template->assign_block_vars('cats.items', array(
				'ID' => $cat_id,
				'NDX' => $cat_idx,
				'URL' => $url,
				'ACT_CLASS' => $act_class,
				'NAME' => $name,
				'NAME_UP' => $this->nameup($name),
		    ));


		}
    }

    /**
     * build Blacks category info
     *
     * @param string $template_name
     * @param $cat_row
     * @param $cat_idx
     * @param $num_cats
     */
    public function buildBlockCatInfo($template_name = 'blocks', $cat_row = array('catid' => 0, 'cat_name' => '', 'imgid' => 0, 'info' => ''), $cat_idx = 1, $num_cats = 0)
    {
        $catid = $cat_row['catid'];

        $img_full = ($cat_row['imgid'] > 0) ? $this->curr_folder . $this->site->getFile($cat_row['imgid']) : '';
        $href = (strlen($cat_row['url']) > 5) ? 'href="' . $cat_row['url'] . '"' : '';
        $this->template->assign_block_vars($template_name, array(
            'ID' => $catid,
            'INDX' => $cat_idx,
            'NAME' => $cat_row['cat_name'],
            'NAME_UP' => $this->nameup($cat_row['cat_name']),
            'IMGID' => $cat_row['imgid'],
            'IMG_FULL' => $img_full,
            'INFO' => $cat_row['info'],
            'INFO_NOTAG' => strip_tags($cat_row['info']),
            'MORE_TXT' => $cat_row['more_txt'],
            'MORE_TXT_UP' => $this->nameup($cat_row['more_txt']),
            'EMBED_CODE' => $cat_row['embed_code'],
            'URL' => $cat_row['url'],
            'HREF' => $href,
        ));

        /* If Cat item is first */
        if ($cat_idx == 1) {
            $this->template->assign_block_vars($template_name . '.first_class', array());
        }
        /* If Cat item is middle */
        if (($cat_idx != $num_cats) && ($cat_idx != 1)) {
            $this->template->assign_block_vars($template_name . '.middle_class', array());
        }
        /* If Cat item is last */
        if ($cat_idx == $num_cats) {
            $this->template->assign_block_vars($template_name . '.last_class', array());
        }
    }

    public function buildBlockInfo($template_name = 'blocks', $item_row = array('name' => ''), $item_idx = 1, $num_items = 0)
    {
        $name = $item_row['name'];
        $link_name = $item_row['link_name'];
        $img_id = $item_row['img'];
        $img02_id = (int)$item_row['img02'];
        $text = $item_row['text'];
        $url = $item_row['url'];
        $img_full = ($img_id > 0) ? $this->curr_folder . $this->site->getFile($img_id) : '';
        $img_url = (strlen($item_row['img_url']) > 5) ? '<a href="' . $item_row['img_url'] . '">' : '';
        $img_url_e = (strlen($item_row['img_url']) > 5) ? '</a>' : '';
        $text = str_replace('{IMG}', $img_full, $text);
        $ad_code = '';
        if (strlen($img_full) > 5) {
            $img_ext = $this->utils->GetFileExtension($img_full);
            switch ($img_ext) {
                case 'swf': {
                    $ad_code = '
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash4/cabs/swflash.cab#version=4,0,0,0" height="200" width="120"><param name="movie" value="' . $img_full . '" /><param name="quality" value="high" /><param name="wmode" value="window" /><param name="allowScriptAccess" value="always" /><embed allowScriptAccess="always" height="200" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash" quality="high" src="' . $img_full . '" type="application/x-shockwave-flash" width="120" wmode="window"></embed></object>';
                    break;
                }
                default: {
                    $ad_code = '<img src="' . $img_full . '" alt="' . $name . '" />';
                    break;
                }
            }
        }
        $href = (strlen($url) > 5) ? 'href="' . $url . '"' : '';
        $class = ($item_idx == 1) ? ' class="active"' : '';
        $in_class = ($item_idx == 1) ? ' in active' : '';

        $mcmsBlockArray = array(
            'IDX0' => ($item_idx - 1),
            'IDX' => $item_idx,
            'CLASS' => $class,
            'IN_CLASS' => $in_class,
            'NAME' => $name,
            'NAME_UP' => $this->nameup($name),
            'LINK_NAME' => $link_name,
            'LINK_NAME_UP' => $this->nameup($link_name),
            'TITLE' => $name,
            'IMG_FULL' => $img_full,
            'IMG_ID' => $img_id,
            'IMGID' => $img_id,
            'IMG_URL' => $img_url,
            'IMG_URL_E' => $img_url_e,
            'AD_CODE' => $ad_code,
            'TEXT' => $text,
            'TEXT_NOTAG' => strip_tags($text),
            'URL' => $url,
            'HREF' => $href,
        );

        $this->template->assign_block_vars($template_name . "_" . $item_idx, $mcmsBlockArray);
        $this->template->assign_block_vars($template_name, $mcmsBlockArray);

        if ($img_id > 0) {
            $this->template->assign_block_vars($template_name . '.img01', array(
                'IMG_ID' => $img_id,
                'IMGID' => $img_id,
                'IMG_FULL' => $img_full,
            ));
        }
        if ($img02_id > 0) {
            $img02_full = $this->curr_folder . $this->site->getFile($img02_id);
            $this->template->assign_block_vars($template_name . '.img02', array(
                'IMG_ID' => $img02_id,
                'IMGID' => $img02_id,
                'IMG_FULL' => $img02_full,
            ));
        }
    }

    public function buildContactsText()
    {
        $query = 'SELECT M.mid, M.parentid, M.htaccessname, ML.*
				FROM `' . $this->db->prefix . 'menu` M
				LEFT OUTER JOIN `' . $this->db->prefix . "menulocalizations` ML ON M.mid = ML.mid
				WHERE ML.lang='" . $this->lang . "' AND M.htaccessname='contacts'";

        $name = $text = '';

        $result = $this->db->query($query);
        if ($row = $this->db->fetch($result)) {
            $name = $row['name'];
            $text = $this->site->f_tags($row['text']);
            $text = $this->site->replaceImageTemplate($text, $this->curr_folder);
            $text = str_replace('[alt]', $name, $text);
        }

        $this->template->assign_var('CONTACT_NAME', $name);
        $this->template->assign_var('CONTACT_NAME_UP', $this->nameup($name));
        $this->template->assign_var('CONTACT_TEXT', $text);
    }

    public function buildSubscribe()
    {
        $this->template->assign_vars(array(
            'SUBSCRIBE_MODULE_FORM_URL' => $this->curr_folder . str_replace('[lang]', $this->lang, $this->permalinks[$this->perma_type]['module_subscribe'][$this->curr_lang]),
        ));
        $this->template->assign_block_vars('subscribe_form', array());
    }

		

    private function highlight()
    {
        $sql = 'SELECT 
            S.*, SL.name, SL.header 
        FROM '.$this->db->prefix.'questionscategories S
		INNER JOIN ' .$this->db->prefix."questionscategorylocalizations SL ON SL.catid = S.catid
		WHERE SL.lang='" .$this->lang."' && S.active = 1 && S.index = 1
		ORDER BY rand() LIMIT 3
		";
        /* echo($sql);	 */
        $result = $this->db->query($sql);
        $ndx = 0;
        $slide_nums = $this->db->num_rows($sql);
        while ($row = $this->db->fetch($result)) {
            ++$ndx;
            $id = $row['catid'];
            $name = $row['name'];
            $icon = $row['icon'];
            $more_txt = $row['header'];
            $fileid = $row['image'];
            $img_file = $this->curr_folder.$this->site->getFile($fileid);
            $img = '<img src="/'.$img_file.'" alt="" />';
			$url = $this->curr_folder . str_replace('[lang]', $this->lang, str_replace('[catid]', $id, $this->permalinks[$this->perma_type]['module_braincategory'][$this->curr_lang]));
			
            if ($ndx == 1) {
                $this->template->assign_block_vars('highlight', array(
					'TITLE' => $this->fromLangIndex('highlight_title')
				));
            }
			

            $this->template->assign_block_vars('highlight.items', array(
				'ID' => $id,
				'NDX' => $ndx,
				'NAME' => $name,
				'NAME_UP' => $this->nameup($name),
				'MORE_TXT' => $this->nameup($more_txt),
				'MORE_TXT_UP' => $this->nameup($more_txt),
				'TITLE' => $name,
				'IMG' => $img,
				'IMGID' => $fileid,
				'IMG_FILE' => $img_file,
				'TEXT' => $more_txt,
				'TEXT_NOTAG' => strip_tags($more_txt),
				'ICON' => $icon,
			    'URL' => $url,
				'FILEID' => $fileid,
            ));
            
        }
    }
	
	
	
	
    /*
     * Generate password string for selected table
     * @val $length
     * @val $table_name = 'm_user'
     * @val $table_column = 'secret_code'
     */
    public function generatePass($length, $table_name = 'm_user', $table_column = 'secret_code')
    {
        $pass = $this->utils->generateString($length);
        $sel = 'SELECT `' . $table_column . '` FROM `' . $table_name . '` WHERE `' . $table_column . "`='" . $pass . "'";
        $query = $this->db->query($sel);
        if ($row = $this->db->fetch_assoc($query)) {
            return $this->generatePass($length, $table_name, $table_column);
        } else {
            return $pass;
        }
    }

    /*
     * Build titles for Meta title, SEO, languages
     */
    public function buildTitle()
    {

        /* echo "<br/><br/>AAA==== ".@$_GET[$this->module_qs].'<br/><pre>';
        print_r($_GET);
        echo '</pre>'; */
        //echo $_SERVER['REQUEST_URI'];
        //echo "<br/><br/>AAA:".$this->module."<br/><br/>";
        switch ($this->module) {
            case'main': {
                /*
                 * Main page title
                 */
                $this->template->assign_vars(array(
                    'TITLE_INSIDE2' => ' - ' . $this->getKeyLang('site_title', $this->lang, ''),
                    'META_DESCRIPTION' => $this->getKeyLang('site_title', $this->lang, ''),
                ));
                $this->template->assign_block_vars('title_main', array());

                $this->template->assign_block_vars('langs', array());
                $ldx = 0;
                $lang_list = $this->fromLangIndex('lang_list');
                foreach ($this->langs as $key => $value) {
                    ++$ldx;
                    $link_lang = ($this->default_lang == $value) ? '0' : '1';
                    $cur_class = ($this->lang == $value) ? ' class="current"' : '';
                    $act_class = ($this->lang == $value) ? ' class="active"' : '';
                    $act_class_nm = ($this->lang == $value) ? ' active' : '';
                    $lang_act = ($this->lang == $value) ? ' lang_act' : '';
                    $this->template->assign_block_vars('langs.list', array(
                        'NAME_FULL' => @$lang_list[$value],
                        'NAME_FULL_UP' => $this->nameup(@$lang_list[$value]),
                        'NAME' => $value,
                        'UP_NAME' => strtoupper($value),
                        'LANG_ACT' => $lang_act,
                        'CUR_CLASS' => $cur_class,
                        'ACT_CLASS' => $act_class,
                        'ACT_CLASS_NM' => $act_class_nm,
                        'URL' => $this->curr_folder . str_replace('[lang]', $value, $this->permalinks[$this->perma_type]['index'][$link_lang]),
                        'IMG' => 'lang_' . $value . '.png',
                    ));
                    if ($this->lang == $value) {
                        $this->template->assign_block_vars('langs.list.current', array());
                    }
                    if ($ldx != count($this->langs)) {
                        $this->template->assign_block_vars('langs.list.sep', array());
                    }
                }
                $this->template->assign_block_vars('langs.current', array(
                    'NAME_FULL' => @$lang_list[$this->lang],
                    'NAME_FULL_UP' => $this->nameup(@$lang_list[$this->lang]),
                    'NAME' => $this->lang,
                    'UP_NAME' => strtoupper($this->lang),
                    'URL' => $this->curr_folder . str_replace('[lang]', $this->lang, $this->permalinks[$this->perma_type]['index'][$link_lang]),
                    'IMG' => 'lang_' . $this->lang . '.png',
                ));
                break;
            }
			
           case 'mcategory': {
                /*
                 * Category page title
                 */

				$catid = $this->utils->UserGetInt('catid');
				$sql = 'SELECT
                      M.*, ML.name
					FROM ' . $this->db->prefix . 'questionscategories M
        			INNER JOIN ' . $this->db->prefix . "questionscategorylocalizations ML ON ML.`catid` = M.`catid`
					WHERE
						(M.`catid` = '" . $catid . "')
					AND (ML.`lang` = '" . $this->lang . "')
					";

				$catname = '';
                $result = $this->db->query($sql);
                if ($row = $this->db->fetch($result)) {
					$catname = $row['name'];
				}



                $this->template->assign_block_vars('langs', array());
                $ldx = 0;
                $lang_list = $this->fromLangIndex('lang_list');
                foreach ($this->langs as $key => $value) {
                    ++$ldx;
                    $link_lang = ($this->default_lang == $value) ? '0' : '1';
                    $cur_class = ($this->lang == $value) ? ' class="current"' : '';
                    $act_class = ($this->lang == $value) ? ' class="active"' : '';
                    $act_class_nm = ($this->lang == $value) ? ' active' : '';
                    $lang_act = ($this->lang == $value) ? ' lang_act' : '';
                    $this->template->assign_block_vars('langs.list', array(
                        'NAME_FULL' => @$lang_list[$value],
                        'NAME_FULL_UP' => $this->nameup(@$lang_list[$value]),
                        'NAME' => $value,
                        'UP_NAME' => strtoupper($value),
                        'LANG_ACT' => $lang_act,
                        'CUR_CLASS' => $cur_class,
                        'ACT_CLASS' => $act_class,
                        'ACT_CLASS_NM' => $act_class_nm,
                        'URL' => $this->curr_folder . str_replace('[lang]', $value, $this->permalinks[$this->perma_type]['index'][$link_lang]),
                        'IMG' => 'lang_' . $value . '.png',
                    ));
                    if ($this->lang == $value) {
                        $this->template->assign_block_vars('langs.list.current', array());
                    }
                    if ($ldx != count($this->langs)) {
                        $this->template->assign_block_vars('langs.list.sep', array());
                    }
                }
                $this->template->assign_block_vars('langs.current', array(
                    'NAME_FULL' => @$lang_list[$this->lang],
                    'NAME_FULL_UP' => $this->nameup(@$lang_list[$this->lang]),
                    'NAME' => $this->lang,
                    'UP_NAME' => strtoupper($this->lang),
                    'URL' => $this->curr_folder . str_replace('[lang]', $this->lang, $this->permalinks[$this->perma_type]['index'][$link_lang]),
                    'IMG' => 'lang_' . $this->lang . '.png',
                ));


                $this->template->assign_vars(array(
                    'TITLE_INSIDE2' => ' - '.$this->fromLangIndex('NAV_category') .' - '.$catname,
                    'META_DESCRIPTION' => $this->fromLangIndex('NAV_category'),
                ));
                $this->template->assign_block_vars('title_main', array());

                break;
            }
			
           case 'mtest': {
                /*
                 * Test page title
                 */

				$testid = $this->utils->UserGetInt('testid');
				$sql = 'SELECT
                      M.*, ML.name
					FROM ' . $this->db->prefix . 'tests M
        			INNER JOIN ' . $this->db->prefix . "testslocalizations ML ON ML.`testid` = M.`testid`
					WHERE
						(M.`testid` = '" . $testid . "')
					AND (ML.`lang` = '" . $this->lang . "')
					";

				$testname = '';
                $result = $this->db->query($sql);
                if ($row = $this->db->fetch($result)) {
					$testname = $row['name'];
				}



                $this->template->assign_block_vars('langs', array());
                $ldx = 0;
                $lang_list = $this->fromLangIndex('lang_list');
                foreach ($this->langs as $key => $value) {
                    ++$ldx;
                    $link_lang = ($this->default_lang == $value) ? '0' : '1';
                    $cur_class = ($this->lang == $value) ? ' class="current"' : '';
                    $act_class = ($this->lang == $value) ? ' class="active"' : '';
                    $act_class_nm = ($this->lang == $value) ? ' active' : '';
                    $lang_act = ($this->lang == $value) ? ' lang_act' : '';
                    $this->template->assign_block_vars('langs.list', array(
                        'NAME_FULL' => @$lang_list[$value],
                        'NAME_FULL_UP' => $this->nameup(@$lang_list[$value]),
                        'NAME' => $value,
                        'UP_NAME' => strtoupper($value),
                        'LANG_ACT' => $lang_act,
                        'CUR_CLASS' => $cur_class,
                        'ACT_CLASS' => $act_class,
                        'ACT_CLASS_NM' => $act_class_nm,
                        'URL' => $this->curr_folder . str_replace('[lang]', $value, $this->permalinks[$this->perma_type]['index'][$link_lang]),
                        'IMG' => 'lang_' . $value . '.png',
                    ));
                    if ($this->lang == $value) {
                        $this->template->assign_block_vars('langs.list.current', array());
                    }
                    if ($ldx != count($this->langs)) {
                        $this->template->assign_block_vars('langs.list.sep', array());
                    }
                }
                $this->template->assign_block_vars('langs.current', array(
                    'NAME_FULL' => @$lang_list[$this->lang],
                    'NAME_FULL_UP' => $this->nameup(@$lang_list[$this->lang]),
                    'NAME' => $this->lang,
                    'UP_NAME' => strtoupper($this->lang),
                    'URL' => $this->curr_folder . str_replace('[lang]', $this->lang, $this->permalinks[$this->perma_type]['index'][$link_lang]),
                    'IMG' => 'lang_' . $this->lang . '.png',
                ));


                $this->template->assign_vars(array(
                    'TITLE_INSIDE2' => ' - '.$this->fromLangIndex('NAV_test') .' - '.$testname,
                    'META_DESCRIPTION' => $this->fromLangIndex('NAV_category'),
                ));
                $this->template->assign_block_vars('title_main', array());

                break;
            }

            default: {
				
				$id = $this->utils->UserGetInt('id');
				$catid = $this->utils->UserGetInt('catid');
				$check_id = ($id > 0) ? $id : $catid;
                $module_sql = 'SELECT
                      M.*,
                      ML.`seo_title`
					FROM ' . $this->db->prefix . 'modules M
        			INNER JOIN ' . $this->db->prefix . "modulelocalizations ML ON ML.`moduleid` = M.`moduleid`
					WHERE
						(M.`name` = '" . $this->module . "')
					AND (ML.`lang` = '" . $this->lang . "')
					";
                //echo("<pre>".$module_sql."</pre>");
                //echo "<pre>".print_r($this->permalinks[$this->perma_type], true)."</pre>";
                $module_result = $this->db->query($module_sql);
                if ($module_row = $this->db->fetch($module_result)) {
                    /**
                     * If found module, then load module title.
                     */
                    $field_id = $module_row['table_id'];
                    $field_header = $module_row['title_header'];
                    $seo_title = $module_row['seo_title'];
                    $field_description = $module_row['title_description'];
                    $field_keywords = $module_row['title_keywords'];
                    $seo_table = $module_row['seo_table'];
                    $seo_sql = 'SELECT
						ML.`' . $field_id . '`,
						ML.`' . $field_header . '`,
						ML.`' . $field_description . '`,
						ML.`' . $field_keywords . '`
					FROM `' . $this->db->prefix . $seo_table . "` ML
					WHERE
						(ML.`lang` = '" . $this->lang . "')
					AND (ML.`" . $field_id . "` = '" . $check_id . "')
				 ";

                    $check_seo_row = (
                        (strlen($field_id) > 0)
                        && (strlen($field_header) > 0)
                        && (strlen($field_description) > 0)
                        && (strlen($field_keywords) > 0)
                        && (strlen($seo_table) > 0)
                    );

                    $seo_row[$field_header] = $seo_row[$field_description] = $seo_row[$field_keywords] = '';
                    if ($check_seo_row) {
                        $seo_result = $this->db->query($seo_sql);
                        $check_seo_row = ($seo_row = $this->db->fetch_assoc($seo_result));
                        if ($check_seo_row) {
                            $check_seo_row = (strlen($seo_row[$field_header]) > 0);
                        }
                    }
                    //echo"<pre>".$seo_sql."</pre>";
                    if ($check_seo_row) {
                        /**
                         * If have information from table.
                         */
                        $title_header = $seo_row[$field_header];
                        $title_description = $seo_row[$field_description];
                        $title_keywords = $seo_row[$field_keywords];
                        $this->template->assign_vars(array(
                            'TITLE_HEADER' => $title_header,
                            'TITLE_DESCRIPTION' => $title_description,
                            'TITLE_KEYWORD' => $title_keywords,
                            'TITLE_SEPERATOR' => ' - ',
                        ));
                        $this->template->assign_block_vars('head_title_inside', array());
                        $this->template->assign_block_vars('title_' . $this->module, array());

                        $module_lang = (array_key_exists($this->module, $this->permalinks[$this->perma_type])) ? $this->module : 'module_' . $this->module;
                        if (array_key_exists($module_lang, $this->permalinks[$this->perma_type])) {
                            $this->template->assign_block_vars('langs', array());
                            $ldx = 0;
                            $lang_list = $this->fromLangIndex('lang_list');
                            foreach ($this->langs as $key => $value) {
                                ++$ldx;
                                $link_lang = ($this->default_lang == $value) ? '0' : '1';
                                $cur_class = ($this->lang == $value) ? ' class="current"' : '';
                                $act_class = ($this->lang == $value) ? ' class="active"' : '';
                                $act_class_nm = ($this->lang == $value) ? ' active' : '';
                                $lang_act = ($this->lang == $value) ? ' lang_act' : '';
                                $this->template->assign_block_vars('langs.list', array(
                                    'NAME_FULL' => @$lang_list[$value],
                                    'NAME_FULL_UP' => $this->nameup(@$lang_list[$value]),
                                    'NAME' => $value,
                                    'UP_NAME' => strtoupper($value),
                                    'LANG_ACT' => $lang_act,
                                    'CUR_CLASS' => $cur_class,
                                    'ACT_CLASS' => $act_class,
                                    'ACT_CLASS_NM' => $act_class_nm,
                                    'URL' => $this->curr_folder . str_replace('[slug]', $this->get_slug($check_id, $value), str_replace('[id]', $check_id, str_replace('[lang]', $value, $this->permalinks[$this->perma_type][$module_lang][$link_lang]))),
                                    'IMG' => 'lang_' . $value . '.png',
                                ));
                                if ($this->lang == $value) {
                                    $this->template->assign_block_vars('langs.list.current', array());
                                }
                                if ($ldx != count($this->langs)) {
                                    $this->template->assign_block_vars('langs.list.sep', array());
                                }
                            }
                            $this->template->assign_block_vars('langs.current', array(
                                'NAME_FULL' => @$lang_list[$this->lang],
                                'NAME_FULL_UP' => $this->nameup(@$lang_list[$this->lang]),
                                'NAME' => $this->lang,
                                'UP_NAME' => strtoupper($this->lang),
                                'URL' => $this->curr_folder . str_replace('[lang]', $this->lang, $this->permalinks[$this->perma_type]['index'][$link_lang]),
                                'IMG' => 'lang_' . $this->lang . '.png',
                            ));
                        }
                    } else {
                        /**
                         * Load info from language file.
                         */
                        $seo_title = (strlen($seo_title) > 0) ? $seo_title : $this->fromLangIndex($this->module);
                        $this->template->assign_vars(array(
                            'TITLE_HEADER' => $seo_title,
                            'TITLE_DESCRIPTION' => $seo_title,
                            'TITLE_KEYWORD' => $seo_title,
                            'TITLE_SEPERATOR' => ' - ',
                        ));
                        $this->template->assign_block_vars('head_title_inside', array());
                        $this->template->assign_block_vars('title_' . $this->module, array());

                        $module_lang = (array_key_exists($this->module, $this->permalinks[$this->perma_type])) ? $this->module : 'module_' . $this->module;
                        if (array_key_exists($module_lang, $this->permalinks[$this->perma_type])) {
                            $this->template->assign_block_vars('langs', array());
                            $ldx = 0;
                            $lang_list = $this->fromLangIndex('lang_list');
                            switch ($module_lang) {
                                case 'news': {
                                    $module_lang = 'module_news';
                                    break;
                                }
                            }
                            foreach ($this->langs as $key => $value) {
                                ++$ldx;
                                $link_lang = ($this->default_lang == $value) ? '0' : '1';
                                $cur_class = ($this->lang == $value) ? ' class="current"' : '';
                                $act_class = ($this->lang == $value) ? ' class="active"' : '';
                                $act_class_nm = ($this->lang == $value) ? ' active' : '';
                                $lang_act = ($this->lang == $value) ? ' lang_act' : '';
                                $this->template->assign_block_vars('langs.list', array(
                                    'NAME_FULL' => @$lang_list[$value],
                                    'NAME_FULL_UP' => $this->nameup(@$lang_list[$value]),
                                    'NAME' => $value,
                                    'UP_NAME' => strtoupper($value),
                                    'LANG_ACT' => $lang_act,
                                    'CUR_CLASS' => $cur_class,
                                    'ACT_CLASS' => $act_class,
                                    'ACT_CLASS_NM' => $act_class_nm,
                                    'URL' => $this->curr_folder . str_replace('[slug]', $this->get_slug($check_id, $value), str_replace('[lang]', $value, $this->permalinks[$this->perma_type][$module_lang][$link_lang])),
                                    'IMG' => 'lang_' . $value . '.png',
                                ));
                                if ($this->lang == $value) {
                                    $this->template->assign_block_vars('langs.list.current', array());
                                }
                                if ($ldx != count($this->langs)) {
                                    $this->template->assign_block_vars('langs.list.sep', array());
                                }
                            }
                            $this->template->assign_block_vars('langs.current', array(
                                'NAME_FULL' => @$lang_list[$this->lang],
                                'NAME_FULL_UP' => $this->nameup(@$lang_list[$this->lang]),
                                'NAME' => $this->lang,
                                'UP_NAME' => strtoupper($this->lang),
                                'URL' => $this->curr_folder . str_replace('[lang]', $this->lang, $this->permalinks[$this->perma_type]['index'][$link_lang]),
                                'IMG' => 'lang_' . $this->lang . '.png',
                            ));
                        }
                    }
                } else {
                    /*
                     * Load main page title
                     */
                    $this->template->assign_vars(array(
                        'TITLE_INSIDE2' => ' - ' . $this->getKeyLang('site_title', $this->lang, ''),
                        'META_DESCRIPTION' => $this->getKeyLang('site_title', $this->lang, ''),
                    ));
                    $this->template->assign_block_vars('title_main', array());

                    $module_lang = (array_key_exists($this->module, $this->permalinks[$this->perma_type])) ? $this->module : 'module_' . $this->module;
                    if (array_key_exists($module_lang, $this->permalinks[$this->perma_type])) {
                        $this->template->assign_block_vars('langs', array());
                        $ldx = 0;
                        $lang_list = $this->fromLangIndex('lang_list');
                        foreach ($this->langs as $key => $value) {
                            ++$ldx;
                            $link_lang = ($this->default_lang == $value) ? '0' : '1';
                            $cur_class = ($this->lang == $value) ? ' class="current"' : '';
                            $act_class = ($this->lang == $value) ? ' class="active"' : '';
                            $act_class_nm = ($this->lang == $value) ? ' active' : '';
                            $lang_act = ($this->lang == $value) ? ' lang_act' : '';
                            $this->template->assign_block_vars('langs.list', array(
                                'NAME_FULL' => @$lang_list[$value],
                                'NAME_FULL_UP' => $this->nameup(@$lang_list[$value]),
                                'NAME' => $value,
                                'UP_NAME' => strtoupper($value),
                                'LANG_ACT' => $lang_act,
                                'CUR_CLASS' => $cur_class,
                                'ACT_CLASS' => $act_class,
                                'ACT_CLASS_NM' => $act_class_nm,
                                'URL' => $this->curr_folder . str_replace('[slug]', $this->get_slug($check_id, $value), str_replace('[lang]', $value, $this->permalinks[$this->perma_type][$module_lang][$link_lang])),
                                'IMG' => 'lang_' . $value . '.png',
                            ));
                            if ($this->lang == $value) {
                                $this->template->assign_block_vars('langs.list.current', array());
                            }
                            if ($ldx != count($this->langs)) {
                                $this->template->assign_block_vars('langs.list.sep', array());
                            }
                        }
                        $this->template->assign_block_vars('langs.current', array(
                            'NAME_FULL' => @$lang_list[$this->lang],
                            'NAME_FULL_UP' => $this->nameup(@$lang_list[$this->lang]),
                            'NAME' => $this->lang,
                            'UP_NAME' => strtoupper($this->lang),
                            'URL' => $this->curr_folder . str_replace('[lang]', $this->lang, $this->permalinks[$this->perma_type]['index'][$link_lang]),
                            'IMG' => 'lang_' . $this->lang . '.png',
                        ));
                    }
                }
                break;
            }
        }
    }

    public function get_nav($curr_id, $ndx = 0, $page_type = 'inside', $all_link = 0)
    {
        if ($ndx == 0) {
            $this->template->assign_block_vars('where', array());
            $this->template->assign_block_vars('where.for', array(
                'NAME' => $this->fromLangIndex('title_home'),
                'NAME_UP' => $this->nameup($this->fromLangIndex('title_home')),
                'SPACE' => $this->fromLangIndex('where_space'),
                'URL' => $this->curr_folder . str_replace('[lang]', $this->lang, $this->permalinks[$this->perma_type]['index'][$this->curr_lang]),
            ));
        }

        $query = 'SELECT M.*, ML.name, ML.header, ML.slug, ML.link as lang_link
				FROM `' . $this->db->prefix . 'menu` M
				LEFT OUTER JOIN `' . $this->db->prefix . "menulocalizations` ML ON M.mid = ML.mid
				WHERE ML.lang='" . $this->lang . "' AND M.mid='" . $curr_id . "'";
        $result = $this->db->query($query);
        if ($row = $this->db->fetch($result)) {
            ++$ndx;
            $parentid = $row['parentid'];
            $name = $row['name'];
            if ($parentid != 0) {
                $this->get_nav($parentid, $ndx, $page_type, $all_link);
                $tpl_nm = (($ndx == 1) && ($all_link == 0)) ? 'where.end' : 'where.for';
                $cur_type = $row['menutype'];
                $curr_url = $this->curr_folder;
                $id = $row['mid'];
                $curr_lang_ext = $this->lang;

                switch ($cur_type) {
                    case 'content': {
                        $curr_url .= str_replace('[slug]', $this->get_slug($id, $curr_lang_ext), str_replace('[name]', $this->utils->url_filter($row['name']), str_replace('[id]', $id, str_replace('[lang]', $this->lang, $this->permalinks[$this->perma_type]['inside'][$this->curr_lang]))));
                        break;
                    }
                    case 'link':
                        $curr_url = str_replace('{LANG}', $this->lang, $row['link']);
                        break;
                    case 'module': {
                        $module_name = 'module_' . $row['link'];
                        $curr_url .= str_replace('[lang]', $this->lang, $this->permalinks[$this->perma_type][$module_name][$this->curr_lang]);
                        break;
                    }
                }

                $curr_url = (($row['lang_link'] != 'http://') && (strlen($row['lang_link']) > 3)) ? $this->curr_folder . $row['lang_link'] : $curr_url;
                if (($row['link'] != 'homepage') || ($ndx == 1)) {
                    $this->template->assign_block_vars($tpl_nm, array(
                        'NAME' => $name,
                        'NAME_UP' => $this->nameup($name),
                        'SPACE' => $this->fromLangIndex('where_space'),
                        'URL' => $curr_url,
                    ));
                }
            }
        }
    }

    public function getNavID($curr_id)
    {
        $query = 'SELECT M.*
        FROM `' . $this->db->prefix . 'menu` M
		WHERE
		    (M.`mid` = ' . $curr_id . ')
        ';
        $result = $this->db->query($query);
        if ($row = $this->db->fetch_assoc($result)) {
            $parentid = $row['parentid'];
            if ($parentid != 0) {
                $this->idArr[] = $parentid;
                $this->getNavID($parentid);
            }
        }
    }

    public function get_slug($curr_id, $lang, $ndx = 0, $page_type = 'inside')
    {
        $link = '';
        $query = 'SELECT M.mid, M.parentid, M.htaccessname, ML.*
				FROM `' . $this->db->prefix . 'menu` M
				LEFT OUTER JOIN `' . $this->db->prefix . "menulocalizations` ML ON M.mid = ML.mid
				WHERE ML.lang='" . $lang . "' AND M.mid = '" . $curr_id . "'";
        $result = $this->db->query($query);
        if ($row = $this->db->fetch($result)) {
            ++$ndx;
            $parentid = $row['parentid'];
            if ($parentid != 0) {
                $slug = $row['slug'];
                $link .= $this->get_slug($parentid, $lang, $ndx, $page_type);
                $link .= $this->utils->url_filter($slug);
                $link .= ($ndx > 1) ? '/' : '';
            }
        }

        return $link;
    }

    /**
     * Define language value from index.lang.php file.
     */
    public function fromLangIndex($key, $sub1 = '', $sub2 = '')
    {
        return $this->utils->GetFromLangCommon('index', 'view', $key, $sub1, $sub2);
    }

    /**
     * Define language value from module language file.
     */
    public function fromLang($key, $sub1 = '', $sub2 = '')
    {
        return $this->utils->GetFromLangCommon($this->module, 'view', $key, $sub1, $sub2);
    }

    /**
     * Parse youtube ID from URL
     */
    public function youtubeFromUrl($url = "")
    {
        $url = str_replace("https://www.youtube.com/watch?v=", "", $url);
        $urlArr = explode("&", $url);
        if (count($urlArr) > 0) {
            $url = $urlArr['0'];
        }
        return $url;
    }

    /**
     * on_load Method
     * Set links
     * Defines language
     * Load module.
     */
    private function on_load()
    {
        $this->curr_folder = $this->utils->findFolder($_SERVER['SCRIPT_NAME']);
        $this->perma_type = $this->fromConfig('perma_type');
        $this->permalinks = $this->fromConfig('permalinks');
        $this->langs = $this->site->getShowLangs();
        $this->default_lang = $this->site->getDefaultLang();
        $this->error404_lang = 'error404_lang';
        if ($this->perma_type == 'page_slug') {
            $this->grabSlug();
        }

        $this->defineLang();
        $this->curr_lang = ($this->default_lang == $this->lang) ? '0' : '1';
        $this->loadModule();
    }
}

/******************* viewpage.class.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** viewpage.class.php ******************/;
