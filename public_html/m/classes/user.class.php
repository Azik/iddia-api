<?php
/******************* user.class.php *******************
 *
 * User class
 *
 * @author MicroPHP <info@microphp.com>
 * @web www.microphp.com
 * @copyright 2004-2019 MicroPHP
 *
 ******************** user.class.php ******************/

/** 
 * Define Namespace 
 */
namespace mcms5xx\classes;

/**
 * Checking if class included normally.
 */
if (!defined('mCMScheck')) {
    die('mCMS by MicroPHP');
}

/**
 * User class
 */
class User
{
    /**
     * Utilites class
     */
    public $utils;

    /**
     * Database class
     */
    public $db;

    /**
     * parrent
     */
    public $parent;

    /**
     * Default module
     */
    public $def_module;

    /**
     * User type string
     */
    public $usertype_str = '';

    /**
     * Permission string (length = 5)
     * full: 0,1;
     * del: 0,1;
     * edit: 0,1;
     * add: 0,1;
     * view: 0,1;
     */
    public $perm_string = '00000';

    /**
     * Class constructor.
     *
     * @param $parent
     * @param $utils - Utilites class
     * @param $db   - Database class
     */
    public function __construct($parent, $utils, $db)
    {
        $this->parent = $parent;
        $this->utils = $utils;
        $this->db = $db;
        $this->OnLoad();
    }

    /**
     * Configure default options for class.
     */
    private function OnLoad()
    {
        $this->def_module = $this->utils->getFromConfig('admin_def_module');
    }

    /**
     * Check if logged
     */
    public function IsLogin()
    {
        $login = false;
        if ($this->utils->GetSession('username') != '') {
            $login = true;
        }

        return $login;
    }

    /**
     * Login user
     *
     * @param string $username
     * @param string $password
     * @param string $teiken
     * @param string $stoken
     */
    public function Login($username, $password, $teiken = '', $stoken = '')
    {
        $is_login = false;

        // check it the user exist
        $query = 'SELECT * FROM '.$this->db->prefix."users WHERE `username`='".$username."' AND `password`='".hash('sha256', $password)."'";
        $result = $this->db->query($query);
        //if ( ($row = $this->db->fetch($result)) && (strlen($teiken)>5) && ($teiken==$stoken) )
        if ($row = $this->db->fetch($result)) {
            $key_val_array = array();
            $key_val_array['username'] = $username;
            $key_val_array['UID'] = (int) $row['userid'];
            $key_val_array['usertype'] = $row['usertype'];
            $this->utils->SetSessionArray($key_val_array);

            $is_login = true;
        }

        return $is_login;
    }

    /**
     * Logout user
     */
    public function Logout()
    {
        $this->utils->RemoveSession('username');
        $this->utils->RemoveSession('UID');
        $this->utils->RemoveSession('usertype');
        $this->utils->Redirect('index.php');
    }

    /**
     * get user type by index id
     *
     * @param $index
     */
    public function GetUserTypeOptions($index = 3)
    {
        if ($this->usertype_str == '') {
            $this->usertype_str = '1:'.$this->fromLangIndex('usertype_superadmin').';2:'.$this->fromLangIndex('usertype_admin').';3:'.$this->fromLangIndex('usertype_manager');
        }

        $options = '';

        $types = explode(';', $this->usertype_str);
        for ($i = 0; $i < count($types); ++$i) {
            $parts = explode(':', $types[$i]);
            $id = $parts[0];
            if (!$this->IsInType(1) && ($id == 1 || $id == 2)) {
                continue;
            }
            if ($id == $index) {
                $options .= '<option value="'.$id.'" selected>'.$parts[1].'</option>';
            } else {
                $options .= '<option value="'.$id.'">'.$parts[1].'</option>';
            }
        }

        return $options;
    }

    /**
     * Define language value from index.lang.php file.
     *
     * @param string $key
     * @param string $sub1
     * @param string $sub2
     */
    public function fromLangIndex($key, $sub1 = '', $sub2 = '')
    {
        return $this->utils->GetFromLangCommon('index', 'admin', $key, $sub1, $sub2);
    }

    /**
     * is in Type
     *
     * @param $index
     */
    public function IsInType($index)
    {
        $type = $this->GetUserType();

        return $type == $index;
    }

    /**
     * get use type from session
     */
    public function GetUserType()
    {
        return $this->utils->GetSession('usertype');
    }

    /**
     * GetUserTypeOptions_old
     *
     * @param $index
     */
    public function GetUserTypeOptions_old($index = 0)
    {
        if ($this->usertype_str == '') {
            $this->usertype_str = '1:'.$this->fromLangIndex('usertype_superadmin').';2:'.$this->fromLangIndex('usertype_admin').';3:'.$this->fromLangIndex('usertype_manager');
        }

        $options = '';

        $types = explode(';', $this->usertype_str);
        for ($i = 0; $i < count($types); ++$i) {
            $parts = explode(':', $types[$i]);
            $id = $parts[0];
            if ($id == $index || ($index == 0 && $i == 0)) {
                $options .= '<option value="'.$id.'" selected>'.$parts[1].'</option>';
            } else {
                $options .= '<option value="'.$id.'">'.$parts[1].'</option>';
            }
        }

        return $options;
    }

    /**
     * get current user type
     */
    public function GetCurrentUserTypeText()
    {
        $index = $this->GetUserType();

        if ($index != 0) {
            return $this->GetUserTypeText($index);
        } else {
            return $this->fromLangIndex('guest');
        }
    }

    /**
     * get user type
     *
     * @param $index
     */
    public function GetUserTypeText($index)
    {
        if ($this->usertype_str == '') {
            $this->usertype_str = '1:'.$this->fromLangIndex('usertype_superadmin').';2:'.$this->fromLangIndex('usertype_admin').';3:'.$this->fromLangIndex('usertype_manager');
        }

        $usertype = '';
        $types = explode(';', $this->usertype_str);
        for ($i = 0; $i < count($types); ++$i) {
            $parts = explode(':', $types[$i]);
            if ($parts[0] == $index) {
                $usertype = $parts[1];
                break;
            }
        }

        return $usertype;
    }

    /**
     * get user info
     *
     * @param integer $userid
     */
    public function GetUser($userid)
    {
        $query = 'SELECT * FROM `'.$this->db->prefix."users` WHERE `userid`='".$userid."'";
        $result = $this->db->query($query);
        if ($row = $this->db->fetch($result)) {
            return $row;
        }

        return null;
    }

    /**
     * get user name
     *
     * @param integer $userid
     */
    public function GetUserName($userid = 0)
    {
        if ($userid === 0) {
            return $this->utils->GetSession('username');
        } elseif ($userid == '') {
            return '';
        } else {
            $query = 'SELECT * FROM `'.$this->db->prefix."users` WHERE `userid` = '".$userid."'";
            $result = $this->db->query($query);
            if ($row = $this->db->fetch($result)) {
                return $row['username'];
            }

            return '';
        }
    }

    /**
     * get user permission
     *
     * @param integer $userid
     */
    public function GetUserPermissionsString($userid)
    {
        $perm_str = '0';
        $query = 'SELECT * FROM `'.$this->db->prefix."users` WHERE `userid` = '".$userid."'";
        $result = $this->db->query($query);
        if ($row = $this->db->fetch($result)) {
            $perm_str = $row['permissions'];
        }

        return $perm_str;
    }

    /**
     * get user options
     *
     * @param integer $userid
     */
    public function GetUserOptions($userid = 0)
    {
        $options = '';

        $query = 'SELECT * FROM `'.$this->db->prefix.'users` ';
        $result = $this->db->query($query);
        $i = 1;
        while ($row = $this->db->fetch($result)) {
            $id = $row['userid'];
            if ($id == $userid || ($userid == 0 && $i == 1)) {
                $options .= '<option value="'.$id.'" selected>'.$row['username'].'</option>';
            } else {
                $options .= '<option value="'.$id.'">'.$row['username'].'</option>';
            }
            ++$i;
        }

        return $options;
    }

    /**
     * check it the user exist
     *
     * @param $username
     */
    public function ExistUser($username)
    {
        $exist = false;

        // check it the user exist
        $query = 'SELECT * FROM `'.$this->db->prefix."users` WHERE `username`='".$username."'";
        $result = $this->db->query($query);
        if ($row = $this->db->fetch($result)) {
            $exist = true;
        }

        return $exist;
    }

    /**
     * get user ID
     *
     * @param $username
     */
    public function GetUserIdByUserName($username)
    {
        $userid = '';

        // check it the user exist
        $query = 'Select * from `'.$this->db->prefix."users` where `username`='".$username."'";
        $result = $this->db->query($query);
        if ($row = $this->db->fetch($result)) {
            $userid = $row['userid'];
        }

        return $userid;
    }

    /**
     * current user hase permission for current module or not
     *
     * @param $module
     */
    public function hasPermission($module)
    {
        $is_right = false;
        $result = $this->db->query('SELECT * FROM `'.$this->db->prefix."modules` WHERE (`have_admin`=1) && (`name`='".$module."') ");
        //$mdl_nums = $this->db->num_rows("SELECT * FROM " . $this->db->prefix . "modules WHERE (`name`='" . $module . "') ");
        $mdl_nums = $this->db->num_rows('SELECT * FROM `'.$this->db->prefix."modules` WHERE (`have_admin`=1) && (`name`='".$module."') ");
        if ($row = $this->db->fetch($result)) {
            $moduleid = $row['moduleid'];
            $type = $row['type'];
            $active = $row['active'];
            if ($active == 1) {
                switch ($type) {
                    case 1: {
                        if ($this->IsInType(1)) {
                            $is_right = true;
                            $this->perm_string = '11111';
                        }
                        break;
                    }
                    case 2: {
                        if ($this->IsInTypes('1,2')) {
                            $is_right = true;
                            $this->perm_string = '11111';
                        }
                        break;
                    }
                    default: {
                        $userid = $this->GetUserId();
                        $perms = $this->GetUserPermissions($userid);
                        //if ($this->IsInTypes("1,2") || array_key_exists($moduleid, $perms))
                        if ($this->IsInTypes('1,2') || (array_key_exists($moduleid, $perms) && ($perms[$moduleid] > 0))) {
                            $is_right = true;
                            $this->perm_string = @$perms[$moduleid];
                            $this->perm_string = ($this->IsInTypes('1,2')) ? '11111' : @$perms[$moduleid];
                            $this->perm_string = (strlen(@$this->perm_string) == 5) ? @$this->perm_string : '00001';
                        }
                        break;
                    }
                }
            }
        } elseif (($mdl_nums > 0) || ($module == $this->def_module) || ($module == 'login') || ($module == 'change_status') || ($module == 'action') || ($module == 'help')) {
            $is_right = true;
        }

        return $is_right;
    }

    /**
     * is in type
     *
     * @param $indexes
     */
    public function IsInTypes($indexes)
    {
        $isin = false;
        $arr = explode(',', $indexes);
        $type = $this->GetUserType();
        for ($i = 0; $i < count($arr); ++$i) {
            if ($type == $arr[$i]) {
                $isin = true;
                break;
            }
        }

        return $isin;
    }

    /**
     * get current user ID from session
     */
    public function GetUserId()
    {
        return $this->utils->filterInt($this->utils->GetSession('UID'));
    }

    /**
     * get user permissions
     *
     * @param $userid
     */
    public function GetUserPermissions($userid)
    {
        $arr = array();
        $arr[] = 0;

        if ($userid == '') {
            return $arr;
        }
        $usertype = 3;
        $perm_str = '';
        $username = '';
        $query = 'SELECT * FROM '.$this->db->prefix."users WHERE userid='".$userid."'";
        $result = $this->db->query($query);
        if ($row = $this->db->fetch($result)) {
            $perm_str = $row['permissions'];
            $username = $row['username'];
            $usertype = $row['usertype'];
        }
        if ($perm_str != '') {
            $perm_arr = explode(';', $perm_str);
            $arr = array();
            foreach ($perm_arr as $perm_txt) {
                $perm_txtArr = explode('::', $perm_txt);
                @$perm_txtArr['1'] = ($usertype == 3) ? @$perm_txtArr['1'] : '11111';
                @$perm_txtArr['1'] = (strlen(@$perm_txtArr['1']) == 5) ? @$perm_txtArr['1'] : '00001';
                $arr[$perm_txtArr['0']] = @$perm_txtArr['1'];
            }
        }
        $arr['username'] = $username;

        return $arr;
    }

    /**
     * Log User operations
     *
     * @param integer $user_id
     * @param string $module_name
     * @param integer $moduleid
     * @param string $operation_mode
     */
    public function logOperation($user_id = 0, $module_name = '', $moduleid = 0, $operation_mode = '')
    {
        $ip = $this->utils->GetIP();
        $comp = $this->utils->GetComp();
        $page = $this->utils->txt_request_filter_user($_SERVER['QUERY_STRING']);
        $query = 'INSERT INTO `'.$this->db->prefix.'logs`(`logsid`, `user_id`, `ip`, `comp`, `date_int`, `module_name`, `moduleid`, `operation_mode`, `page`)
					VALUES(NULL, ' .$user_id.", '".$ip."', '".$comp."', ".time().", '".$module_name."', ".$moduleid.", '".$operation_mode."', '".$page."')";
        $this->db->query($query);
    }
}

/******************* user.class.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** user.class.php ******************/;
