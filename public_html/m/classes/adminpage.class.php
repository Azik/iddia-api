<?php
/******************* adminpage.class.php *******************
 *
 * Admin class
 *
 * @author MicroPHP <info@microphp.com>
 * @web www.microphp.com
 * @copyright 2004-2019 MicroPHP
 *
 ******************** adminpage.class.php ******************/

/** 
 * Define Namespace 
 */
namespace mcms5xx\classes;

/**
 * Include configuration file.
 */
@require_once iFolded.'mcms.cnfg.php';

/*
 * Checking if class included normally
 */
if (!defined('mCMScheck')) {
    die('mCMS by MicroPHP');
}

/**
 * Include Base class file.
 */
require_once iFolded.'m/classes/base.class.php';

/**
 * Include Template admin class file.
 */
require_once iFolded.'m/classes/template.class.admin.php';

/**
 * Include User class file.
 */
require_once iFolded.'m/classes/user.class.php';

/**
 * Admin class
 */
class AdminPage extends Base
{
    /**
     * User class
     */
    public $user;

    /**
     * Default module
     */
    public $def_module;

    /**
     * Loaded module
     */
    public $module = '';

    /**
     * Admin module files folder
     */
    public $module_folder = 'modules/';

    /**
     * Admin module query string
     */
    public $module_qs = '';

    /**
     * Current language
     */
    public $lang;

    /**
     * Language files folder
     */
    public $lang_folder;

    /**
     * Current Language files folder
     */
    public $lg_folder;

    /**
     * Admin template files folder
     */
    public $template;

    /**
     * Site languages array
     */
    public $langs;

    /**
     * Default admin panguage
     */
    public $default_lang;

    /**
     * @var string Site script current folder
     *
     * @example http://example.com/folder/file.php $curr_folder = /folder/
     */
    public $curr_folder;
	
	/**
     * current domain
     */
    public $curr_domain;

    /**
     * Current module
     */
    public $curr_module = 'main';
	
    /**
     * Admin folder
     */
    public $admin_folder;

    /**
     * Admin Class constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->def_module = $this->fromConfig('admin_def_module');
        $this->module_qs = $this->fromConfig('module_admin_qs');
        $this->user = new User($this, $this->utils, $this->db);

        /**
         * Create template
         */
        $this->createTemplate();
        $this->on_load();

        $this->template->assign_var('DATE_A', date('Y-m-d'));
    }

    /**
     * Create template
     */
    public function createTemplate()
    {
        $rootdir = '../';
        $template = $this->fromConfig('admin_template');
        $dir = $this->fromConfig('template_folder');
        $temp_admin = '{ROOT}{DIR}{TEMPLATE}';
        $temp_admin = str_replace('{ROOT}', $rootdir, $temp_admin);
        $temp_admin = str_replace('{DIR}', $dir, $temp_admin);
        $temp_admin = str_replace('{TEMPLATE}', $template, $temp_admin);

        $this->template = new Template($temp_admin);

        $template_files = $this->fromConfig('templates_admin');

        $this->template->set_filenames($template_files);

        $this->admin_folder = $temp_admin.'/';
        $this->template->assign_var('ROOT', $this->admin_folder);
    }

    /**
     * Load default configurations
     */
    public function on_load()
    {
		$this->curr_folder = $this->utils->findFolder($_SERVER['SCRIPT_NAME'], 1);
		$this->curr_domain = 'https://' . $_SERVER['HTTP_HOST'];
        $this->langs = $this->site->getLangs();
        $this->default_lang = $this->site->getDefaultLang();
        $admin_lang = $this->fromConfig('admin_lang');
        $this->lang = $this->getKey('lang_adminmainlang', $admin_lang);
        $this->lang_folder = '../'.$this->fromConfig('lang_folder');
        $this->lg_folder = $this->lang_folder.$this->lang;
        $this->lg_folder = $this->lg_folder;

        $this->template->assign_vars(array(
            'MODULE_QS' => $this->module_qs,
            'YEAR' => date('Y'),
            'LOGOUT' => $this->fromLangIndex('logout'),
        ));
        $this->loadModule();
        $this->defineModuleByUser();
    }

    /**
     * Generate CSRF token
     */
    public function generateToken()
    {
        if (@$_SERVER['REQUEST_METHOD'] != 'POST') {
            $token = sha1(uniqid(rand(98456, time()), true));
            $_SESSION['token'] = $token;
            $this->template->assign_vars(array(
                'TOKEN' => $token,
            ));
        }
    }

    /**
     * Generate SEO slug
     *
     * @param string $name
     * @param string $lang - Language
     * @param string $tbl - Table for check name
     */
    public function generateSlug($name, $lang, $tbl = 'menulocalizations')
    {
        $name = (strlen($name) == 0) ? 'name' : $name;
        $slug = $this->utils->url_filter($name);
        $slug = $this->utils->slugFilter($slug);
        $slug = ($this->checkSlugList($slug)) ? 'name' : $slug;
        $slug_id = $this->checkSlug($slug, $lang, $tbl);
        $slug = ($slug_id == 0) ? $slug : $slug.$slug_id;

        return $slug;
    }

    /**
     * Check SEO slug for black list
     *
     * @param string $slug
     */
    public function checkSlugList($slug)
    {
        $blackList = array('admin', 'a', 'm', 't', 'news', 'img', 'classes', 'gallery', 'img_gallery', 'modules', 'contacts', 'search');

        return in_array($slug, $blackList);
    }

    /**
     * Check SEO slug for dublicate
     *
     * @param string $slug
     * @param string $lang - Language
     * @param string $tbl - Table for check name
     */
    public function checkSlug($slug, $lang, $tbl = 'menulocalizations')
    {
        $id = 0;

        $sql = 'SELECT ML.name FROM `'.$this->db->prefix."modules` ML
			WHERE 
				(ML.`name`='" .$slug."') ";
        while ($this->db->num_rows($sql) > 0) {
            ++$id;
            $slug_name = $slug.$id;
            $sql = 'SELECT ML.slug FROM `'.$this->db->prefix."modules` ML
			WHERE 
				(ML.`name`='" .$slug_name."') ";
        }

        $sql = 'SELECT ML.slug FROM `'.$this->db->prefix.$tbl."` ML
			WHERE 
				(ML.lang='" .$lang."')
			 && (ML.slug='" .$slug."') ";
        while ($this->db->num_rows($sql) > 0) {
            ++$id;
            $slug_name = $slug.$id;
            $sql = 'SELECT ML.slug FROM `'.$this->db->prefix.$tbl."` ML
			WHERE 
				(ML.lang='" .$lang."')
			 && (ML.slug='" .$slug_name."') ";
        }

        return $id;
    }

    /**
     * Build Admin menu
     */
    public function buildMenu()
    {
        if (!$this->user->IsLogin()) {
            return;
        }

        $user_type = $this->user->GetCurrentUserTypeText();
        $this->curr_module = $this->module;

        $this->template->assign_vars(array(
            'USER_RIGHT' => $this->fromLangIndex('user_right', $user_type),
            'USER_NAME' => $this->user->GetUserName($this->user->GetUserId()),
            'HOME' => $this->fromLangIndex('home'),
            'DASHBOARD' => $this->fromLangIndex('dashboard'),
            'VISIT_SITE' => $this->fromLangIndex('visit_site'),
            'SITEOPTIONS' => $this->fromLangIndex('siteoptions'),
            'PROFILE' => $this->fromLangIndex('profile'),
            'PASSWORD' => $this->fromLangIndex('password'),
        ));
        $class = ($this->module == 'main') ? ' class="current"' : '';
        $active_class = ($this->curr_module == 'main') ? ' class="active"' : '';
        $active_class_nm = ($this->curr_module == 'main') ? ' active' : '';
        $selected_class = ($this->curr_module == 'main') ? '<span class="selected"></span>' : '';
        $this->template->assign_block_vars('menu', array());
        $this->template->assign_block_vars('menu.items', array(
            'URL' => 'index.php?'.$this->module_qs.'=main',
            'TEXT' => $this->fromLangIndex('dashboard'),
            'CLASS' => $class,
            'ACTIVE_CLASS' => $active_class,
            'ACTIVE_CLASS_NM' => $active_class_nm,
            'SELECTED_CLASS' => $selected_class,
            'ICON' => 'fa fa-home',
        ));

        // Modules
        $this->template->assign_block_vars('menu', array());
        //$result = $this->db->query("SELECT * FROM " . $this->db->prefix . "modules WHERE (`have_admin`=1) && (`active`=1) ORDER BY position");
        $sql = "SELECT
          M.*,
          ML.`title`
		FROM " .$this->db->prefix."modules M
		INNER JOIN " .$this->db->prefix."modulelocalizations ML ON ML.`moduleid` = M.`moduleid` AND ML.lang='".$this->lang."'
		WHERE
		    (M.`have_admin`=1)
		 && (M.`active`=1)
		 && (M.`type`=3)
		 && (M.`name` != 'options_profile')
		 && (M.`name` != 'options_site')
		 ORDER BY M.`position` ";
        $result = $this->db->query($sql);
        $this->template->assign_block_vars('menu.heading', array(
            'NAME' => $this->fromLangIndex('heading_modules'),
        ));

        while ($row = $this->db->fetch($result)) {
            $this->template->assign_block_vars('menu', array());
            $id = $row['moduleid'];
            $name = $row['name'];
            $type = $row['type'];
            $title = $row['title'];
            $description = $row['description'];

            /* $result_loc = $this->db->query("SELECT * FROM " . $this->db->prefix . "modulelocalizations WHERE moduleid='" . $id . "' AND lang='" . $this->lang . "'");
            if ($row_loc = $this->db->fetch($result_loc)) {
                $title = $row_loc['title'];
            } */
            $is_right = false;
            switch ($type) {
                case 1:
                    if ($this->user->IsInType(1)) {
                        $is_right = true;
                    }
                    break;
                case 2:
                    if ($this->user->IsInTypes('1,2')) {
                        $is_right = true;
                    }
                    break;
                default:
                    $userid = $this->user->GetUserId();
                    $perms = $this->user->GetUserPermissions($userid);
                    if ($this->user->IsInTypes('1,2') || (array_key_exists($id, $perms) && ($perms[$id] > 0))) {
                        $is_right = true;
                    }
                    break;
            }

            if ($is_right) {
                $menu_url = 'index.php?'.$this->module_qs.'='.$name;
                $class = ($this->module == $name) ? ' class="current"' : '';
                $active_class = ($this->curr_module == $name) ? ' class="active"' : '';
                $active_class_nm = ($this->curr_module == $name) ? ' active' : '';
                $selected_class = ($this->curr_module == $name) ? '<span class="selected"></span>' : '';

                $this->template->assign_block_vars('menu.items', array(
                    'URL' => $menu_url,
                    'TEXT' => $title,
                    'CLASS' => $class,
                    'ACTIVE_CLASS' => $active_class,
                    'ACTIVE_CLASS_NM' => $active_class_nm,
                    'SELECTED_CLASS' => $selected_class,
                    'ICON' => $row['icon'],
                ));
            }
        }

        // Features
        $this->template->assign_block_vars('menu', array());
        $this->template->assign_block_vars('menu.heading', array(
            'NAME' => $this->fromLangIndex('heading_features'),
        ));
        $sql = "SELECT
          M.*,
          ML.`title`
		FROM " .$this->db->prefix."modules M
		INNER JOIN " .$this->db->prefix."modulelocalizations ML ON ML.`moduleid` = M.`moduleid` AND ML.lang='".$this->lang."'
		WHERE
		    (M.`have_admin` = 1)
		 && (M.`active` = 1)
		 && ( (M.`type` != 3) || ( (M.`name` = 'options_profile') || (M.`name` = 'options_site') ) )
		 ORDER BY M.`position` ";
        $result = $this->db->query($sql);
        while ($row = $this->db->fetch($result)) {
            $this->template->assign_block_vars('menu', array());
            $id = $row['moduleid'];
            $name = $row['name'];
            $type = $row['type'];
            $title = $row['title'];
            $description = $row['description'];

            /* $result_loc = $this->db->query("SELECT * FROM " . $this->db->prefix . "modulelocalizations WHERE moduleid='" . $id . "' AND lang='" . $this->lang . "'");
            if ($row_loc = $this->db->fetch($result_loc)) {
                $title = $row_loc['title'];
            } */
            $is_right = false;
            switch ($type) {
                case 1:
                    if ($this->user->IsInType(1)) {
                        $is_right = true;
                    }
                    break;
                case 2:
                    if ($this->user->IsInTypes('1,2')) {
                        $is_right = true;
                    }
                    break;
                default:
                    $userid = $this->user->GetUserId();
                    $perms = $this->user->GetUserPermissions($userid);
                    if ($this->user->IsInTypes('1,2') || (array_key_exists($id, $perms) && ($perms[$id] > 0))) {
                        $is_right = true;
                    }
                    break;
            }

            if ($is_right) {
                $menu_url = 'index.php?'.$this->module_qs.'='.$name;
                $class = ($this->module == $name) ? ' class="current"' : '';
                $active_class = ($this->curr_module == $name) ? ' class="active"' : '';
                $active_class_nm = ($this->curr_module == $name) ? ' active' : '';
                $selected_class = ($this->curr_module == $name) ? '<span class="selected"></span>' : '';
                switch ($name) {
                    case 'options_profile':{
                        $this->template->assign_block_vars('header_options_profile', array());
                        $selected_class .= '<span class="arrow "></span>';
                        //$selected_class .= '<span class="fa fa-caret-down arrow"></span>';
                        //$selected_class .= '<span class="fa fa-caret-left"></span>';
                        $menu_url = 'javascript:;';
                        break;
                    }
                    case "options_site":{
                        $this->template->assign_block_vars('header_options_site', array());
                        break;
                    }
                }
                $this->template->assign_block_vars('menu.items', array(
                    'URL' => $menu_url,
                    'TEXT' => $title,
                    'CLASS' => $class,
                    'ACTIVE_CLASS' => $active_class,
                    'ACTIVE_CLASS_NM' => $active_class_nm,
                    'SELECTED_CLASS' => $selected_class,
                    'ICON' => $row['icon'],
                ));

                if ($name == 'options_profile') {
                    $this->template->assign_block_vars('menu.items.sub', array());
                    $this->template->assign_block_vars('menu.items.sub.link', array(
                        'URL' => 'index.php?'.$this->module_qs.'=options_profile&mode=profile',
                        'TEXT' => $this->fromLangIndex('profile'),
                    ));
                    $this->template->assign_block_vars('menu.items.sub.link', array(
                        'URL' => 'index.php?'.$this->module_qs.'=options_profile&mode=password',
                        'TEXT' => $this->fromLangIndex('password'),
                    ));
                }
            }
        }

        // Help
        $active_class = ($this->curr_module == 'help') ? ' class="active"' : '';
        $active_class_nm = ($this->curr_module == 'help') ? ' active' : '';
        $this->template->assign_block_vars('menu.items', array(
            'URL' => 'help.php',
            'ACTIVE_CLASS' => $active_class,
            'ACTIVE_CLASS_NM' => $active_class_nm,
            'TEXT' => $this->fromLangIndex('help'),
            'ICON' => 'fa fa-question',
        ));

        //Log out
        $this->template->assign_block_vars('menu.items', array(
            'URL' => 'index.php?'.$this->module_qs.'=action&act=logout',
            'TEXT' => $this->fromLangIndex('logout'),
            'ICON' => 'fa fa-power-off',
        ));
    }

    /**
     * Define module by User permission
     */
    public function defineModuleByUser()
    {	
        if (!$this->user->IsLogin() && $this->module != 'login' && $this->module != 'change_status') {
			//echo "MM:".$this->module;exit();
            $this->utils->Redirect('index.php?'.$this->module_qs.'=login');
            //$this->utils->Redirect('index.php?'.$this->module_qs.'=action&act=logout');
            return;
        } elseif (!$this->user->hasPermission($this->module)) {
            $this->utils->Redirect('?'.$this->module_qs.'='.$this->def_module);
        }
    }

    /**
     * Include module
     */
    public function insertModule()
    {
        if ($this->module == '') {
            return;
        }
        require_once $this->module_folder . $this->module.'.admin.php';
    }

    /**
     * Load modue
     */
    public function loadModule()
    {
        $mod = $this->utils->Get($this->module_qs);
        if ($mod != '' && strpos($mod, '/') === false && file_exists($this->module_folder.$mod.'.admin.php')) {
            $this->module = $mod;
        } else {
            $this->module = $this->def_module;
        }
    }

    /**
     * Load data from index language file
     *
     * @param string $opt_key
     * @param string $sub1
     * @param string $sub2
     */
    public function fromLangIndex($opt_key, $sub1 = '', $sub2 = '')
    {
        return $this->utils->GetFromLangCommon('index', 'admin', $opt_key, $sub1, $sub2);
    }

    /**
     * Load data from module language file
     *
     * @param string $opt_key
     * @param string $sub1
     * @param string $sub2
     */
    public function fromLang($opt_key, $sub1 = '', $sub2 = '')
    {
        return $this->utils->GetFromLangCommon($this->module, 'admin', $opt_key, $sub1, $sub2);
    }

    /**
     * Set index language settings for module {_LANG_VALUE_}
     */
    public function buildLangs()
    {
        foreach ($GLOBALS['language']['index']['admin'] as $key => $txt) {
            $this->template->assign_var('_' . strtoupper($key) . '_', $txt);
        }
    }


    public function getFileTypesArray()
    {
        $arr = array();
        $file_types = $this->fromConfig('file_types');

        foreach ($file_types as $key => $value) {
            for ($i = 0; $i < count($value); ++$i) {
                $arr[] = $value[$i];
            }
        }

        return $arr;
    }


    public function getFileCat($ext)
    {
        $cat = '';
        $file_types = $this->fromConfig('file_types');

        foreach ($file_types as $key => $value) {
            for ($i = 0; $i < count($value); ++$i) {
                if ($ext == $value[$i]) {
                    return $key;
                }
            }
        }

        return $cat;
    }

    /**
     * Filter data for show
     *
     * @param string $txt
     */
    public function ekranFilter($txt)
    {
        $txt = addslashes($txt);
        $txt = stripslashes($txt);
        $txt = str_replace('"', '&quot;', $txt);

        return $txt;
    }

    /**
     * Order Section
     *
     * @param integer $parentid
     */
    public function orderSection($parentid = 0)
    {
        $ndx = 1;
        $result = $this->db->query('SELECT mid, position, parentid
			FROM ' .$this->db->prefix."menu
			WHERE 
				parentid='" .$parentid."'
			ORDER BY position ASC");
        while ($row = $this->db->fetch($result)) {
            $this->db->query('UPDATE '.$this->db->prefix."menu
				SET 
					position='" .($ndx++)."'
				WHERE 
					 (`parentid`='" .$parentid."')
				 && (`mid`=" .$row['mid'].')');
        }
    }

    /**
     * Order news category
     */
    public function orderNewsCat()
    {
        $ndx = 1;
        $result = $this->db->query('SELECT gid, position
			FROM ' .$this->db->prefix.'gallery
			ORDER BY position ASC');
        while ($row = $this->db->fetch($result)) {
            $this->db->query('UPDATE '.$this->db->prefix."gallery
				SET 
					position='" .($ndx++)."'
				WHERE 
					 (`gid`=" .$row['gid'].')
				');
        }
    }

    /**
     * Order Gallery
     */
    public function orderGallery()
    {
        $ndx = 1;
        $result = $this->db->query('SELECT catid, position
			FROM ' .$this->db->prefix.'newscategories
			ORDER BY position ASC');
        while ($row = $this->db->fetch($result)) {
            $this->db->query('UPDATE '.$this->db->prefix."newscategories
				SET 
					position='" .($ndx++)."'
				WHERE 
					 (`catid`=" .$row['catid'].')
				');
        }
    }

    /**
     * Order Slider
     */
    public function orderSlider()
    {
        $ndx = 1;
        $result = $this->db->query('SELECT sid, position
			FROM ' .$this->db->prefix.'slider
			ORDER BY position ASC');
        while ($row = $this->db->fetch($result)) {
            $this->db->query('UPDATE '.$this->db->prefix."slider
				SET 
					position='" .($ndx++)."'
				WHERE 
					 (`sid`=" .$row['sid'].')
				');
        }
    }

    /**
     * Order Blocks
     *
     * @param integer $catid - Category ID
     */
    public function orderBlocks($catid = 0)
    {
        $ndx = 1;
        $result = $this->db->query('SELECT blocksid, position
			FROM ' .$this->db->prefix.'blocks
			WHERE 
				`catid`=' .$catid.'
			ORDER BY position ASC');
        while ($row = $this->db->fetch($result)) {
            $this->db->query('UPDATE '.$this->db->prefix."blocks
				SET 
					position='" .$ndx."'
				WHERE 
					 (`blocksid`=" .$row['blocksid'].')
				');
            ++$ndx;
        }
    }

    /**
     * Order Modules
     */
    public function orderModules()
    {
        $ndx = 1;
        $result = $this->db->query('SELECT moduleid, position
			FROM ' .$this->db->prefix.'modules
			ORDER BY position ASC');
        while ($row = $this->db->fetch($result)) {
            $this->db->query('UPDATE '.$this->db->prefix."modules
				SET 
					position='" .$ndx."'
				WHERE 
					 (`moduleid`=" .$row['moduleid'].')
				');
            ++$ndx;
        }
    }
}

/******************* adminpage.class.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** adminpage.class.php ******************/;
