<?php
/******************* base.class.php *******************
 *
 * Base class
 *
 * @author MicroPHP <info@microphp.com>
 * @web www.microphp.com
 * @copyright 2004-2019 MicroPHP
 *
 ******************** base.class.php ******************/

/** 
 * Define Namespace 
 */
namespace mcms5xx\classes;

/**
 * Checking if class included normally.
 */
if (!defined('mCMScheck')) {
    die('mCMS by MicroPHP');
}

/*
 * Checking if iFolded defined
 */
if (!defined('iFolded')) {
    define('iFolded', './');
}

/**
 * Include utils class file.
 */
require_once iFolded.'m/classes/utils.class.php';

/**
 * Include site class file.
 */
require_once iFolded.'m/classes/site.class.php';

/**
 * Include Input output class file.
 */
require_once iFolded.'m/classes/io.class.php';

/**
 * Multiple inheritance
 */
class multipleInheritance
{
    /**
     * Ca                                                                                                                                                                                                                   ll Classes
     *
     * @param $class_to_call
     */
    public function callClass($class_to_call)
    {
        return new $class_to_call();
    }
}

/**
 * Base class
 */
class Base extends multipleInheritance
{
    /**
     * Utils class
     */
    public $utils;

    /**
     * Database Configuration class
     */
    public $db_cfg;

    /**
     * Database class
     */
    public $db;

    /**
     * Site class
     */
    public $site;

    /**
     * Input Output class
     */
    public $io;

    /**
     * Page load time.
     *
     * @var int
     */
    public $begTime;

    /**
     * @var array protocols from configuration
     */
    public $protocolsArr = array();

    /**
     *  Module request name
     *  Request method _GET.
     *
     * @var
     */
    public $module_view_qs = '';

    /**
     * Base Class constructor.
     */
    public function __construct($load_module = true)
    {
        /**
         * Database configuration
         */
        $this->db_cfg = new \mcms5xx\dbConfig();

        /**
         * Set Begin time for detect script runtime
         */
        $this->begTime = microtime(true);

        /**
         * Input Output class
         */
        $this->io = new IO();

        /**
         * Include database class file.
         */
        require_once iFolded . 'm/classes/database.class.' . $this->db_cfg->db_connection . '.php';
        
        /**
         * Database class
         */
        $this->db = new Database($this->db_cfg->db_name, $this->db_cfg->db_user, $this->db_cfg->db_pass, $this->db_cfg->db_host, $this->db_cfg->db_prefix);

        /**
         * Utils class
         */
        $this->utils = new \mcms5xx\classes\Utils($this->io, $this->db);

        /**
         * Site class
         */
        $this->site = new Site($this->utils, $this->db);
		
		/**
		 * Load configuration data.
		 */
		$this->loadConfData();
		
		/** 
		 * Load module config files. 
		 */
		if ($load_module) {	
			$this->loadModuleConf();
		}
		
    }

    /**
     * Load configuration data.
     */
    public function loadConfData()
    {
        $this->protocolsArr = $this->fromConfig('protocolsArr');
        $this->module_view_qs = $this->fromConfig('module_view_qs');
    }

    /**
     * Load module config files.
     */
    public function loadModuleConf()
    {
        $sql = 'SELECT ML.*
        FROM `' .$this->db->prefix.'modules` ML
        WHERE
            (ML.`have_view` = 1)
        ';
        $result = $this->db->query($sql);
        while ($row = $this->db->fetch($result)) {
            $md_name = $row['name'];
            $this->$md_name = '';

            $have_cnfg = $row['have_cnfg'];
            if ($have_cnfg == 1) {
                //$this->$md_name = $module_form_builder;
                include_once iFolded.'m/modules/'.$md_name.'.cnfg.php';
                $mdl_str = $md_name.'_cls';
                if (class_exists($mdl_str)) {
                    $this->$md_name = new $mdl_str();
                }
            }
        }
    }

    /**
     * pageLoad Method
     * Return page load time.
     *
     * @param string $pg - page name
     * @param boolean $show
     */
    public function pageLoad($pg = '', $show = false)
    {
        $endTime = microtime(true);
        $tTime = ($endTime - $this->begTime);
        echo '<!-- Load time: '.$pg.':'.$tTime.'//-->'."\n";
        if ($show) {
            echo 'Load time: '.$pg.':'.$tTime."\n<br/>";
        }
        $this->template->assign_vars(array(
            'PAGE_LOAD' => $tTime,
        ));
    }

    /**
     * Default values from config
     *
     * @param string $opt_key - get from configuration file
     */
    public function fromConfig($opt_key)
    {
        return $this->utils->getFromConfig($opt_key);
    }

    /**
     * Send mail
     *
     * @param string $mail_to
     * @param string $mail_subject
     * @param string $mail_txt
     */
    public function sendMailQueue($mail_to = '', $mail_subject = '', $mail_txt = '')
    {
        $create_date = time();
        $sql = 'INSERT INTO '.$this->db->prefix.'message_sends (`msgid`, `create_date`, `send_datetime`, `mail_to`, `mail_subject`, `mail_txt`, `sended`)
			VALUES(NULL, ' .$create_date.", 0, '".$mail_to."', '".$mail_subject."', '".$this->db->quote($mail_txt)."', 0)";
        $this->db->query($sql);
    }

    /**
     * Add notification (log)
     *
     * @param integer $to_member_id
     * @param string $n_subject
     * @param string $n_text
     * @param integer $from_member_id
     * @param integer $send_mail
     * @param string $mail_to = ''
     */
    public function addNotification($to_member_id, $n_subject, $n_text = '', $from_member_id = 0, $send_mail = 0, $mail_to = '')
    {
        $n_date = time();
        $sql = 'INSERT INTO `'.$this->db->prefix.'notifications` (`nid`, `to_member_id`, `from_member_id`, `n_subject`, `n_text`, `n_date`, `read_date`, `send_mail`, `active`)
			VALUES(NULL, ' .$to_member_id.', '.$from_member_id.", '".$n_subject."', '".$n_text."', ".$n_date.', 0, '.$send_mail.', 1)';
        $this->db->query($sql);
        if (($send_mail == 1) && (strlen($mail_to) > 0)) {
            $this->sendMailQueue($mail_to, $n_subject, $n_text);
        }
    }

    /**
     * Add Key to options table
     *
     * @param string $opt_key get from configuration file
     * @param [mixed] $value add value stored on options table
     */
    public function addKey($opt_key, $value)
    {
        $sql = 'SELECT id FROM '.$this->db->prefix."options WHERE `opt_key`='".$opt_key."'";
        if ($this->db->num_rows($sql) > 0) {
            $sql = 'UPDATE '.$this->db->prefix."options SET value='".$value."' WHERE `opt_key`='".$opt_key."'";
            $this->db->query($sql);
        } else {
            $sql = 'INSERT INTO '.$this->db->prefix."options (`opt_key`, `value`) VALUES('".$opt_key."','".$value."')";
            $this->db->query($sql);
        }
    }

    /**
     * Add Key to options multilanguage table
     *
     * @param string $opt_key get from configuration file
     * @param string $lang
     * @param $value add value stored on optionslocalizations table
     */
    public function addKeyLang($opt_key, $lang, $value)
    {
        $sql = 'SELECT `opt_key` FROM '.$this->db->prefix."optionslocalizations WHERE (`lang`='".$lang."') && (`opt_key`='".$opt_key."')";
        if ($this->db->num_rows($sql) > 0) {
            $sql = 'UPDATE '.$this->db->prefix."optionslocalizations SET value='".$value."' WHERE (`lang`='".$lang."') && (`opt_key`='".$opt_key."')";
            $this->db->query($sql);
        } else {
            $sql = 'INSERT INTO '.$this->db->prefix."optionslocalizations (`lang`, `opt_key`, `value`) VALUES('".$lang."', '".$opt_key."', '".$value."')";
            $this->db->query($sql);
        }
    }

    /**
     * Get value from options table
     *
     * @param string $opt_key get from configuration file
     * @param $def_val get value from options table
     */
    public function getKey($opt_key, $def_val)
    {
        $value = $def_val;
        $result = $this->db->query('SELECT * FROM '.$this->db->prefix."options WHERE `opt_key`='".$opt_key."'");
        if ($row = $this->db->fetch($result)) {
            $value = $row['value'];
        }

        return $value;
    }

    /**
     * Get value from multilang
     *
     * @param string $opt_key get from configuration file
     * @param string $lang
     * @param $def_val get value from optionslocalizations table
     */
    public function getKeyLang($opt_key, $lang, $def_val)
    {
        $value = $def_val;
        $result = $this->db->query('SELECT * FROM '.$this->db->prefix."optionslocalizations WHERE (`lang`='".$lang."') && (`opt_key`='".$opt_key."')");
        if ($row = $this->db->fetch($result)) {
            $value = $row['value'];
        }

        return $value;
    }
}

/******************* base.class.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** base.class.php ******************/;
