<?php
/******************* APIJsonResponse.php *******************
 *
 * Json Api response class
 *
 * @author MicroPHP <info@microphp.com>
 * @web www.microphp.com
 * @copyright 2004-2019 MicroPHP
 *
 ******************** APIJsonResponse.php ******************/

/** 
 * Define Namespace 
 */
namespace mcms5xx\classes;

/**
 * Checking if class included normally.
 */
if (!defined('mCMScheck')) {
    die('mCMS by MicroPHP');
}

/**
 * Api response generaate
 */
class APIJsonResponse
{
   public $status;
    public $data = [];
    public $errors = [];
    public $statusCode;
    public $result;
    
    public function __construct($status, array $errors = [],  array $data = [])
    {
        $this->status = $status;
        $this->errors = $errors;
        $this->data = $data;
		

        $this->result = array(
          'status' => $this->status
        );
        
        echo $this->response();
    }
    
    /**
     * Format user message with HTTP status and status code
     *
     * @return string, json object
     */
    public function response()
    {
        $statusCode = 200;
        //set the HTTP response code
		if($this->status)
			$this->status = 'ok';

        switch ($this->status)
        {
            case "unauthorized": {
				$statusCode = 401;
                break;
			}
            case "exception": {
                $statusCode = 500;
                break;
			}
        }
		//echo "DD:".$statusCode;exit();
        
        //set the response header
        //header("Content-Type", "application/json");
		header('Content-Type: application/json');
		//echo "BB";exit();
        header(sprintf('HTTP/1.1 %s %s', $statusCode, $this->status), true, $statusCode);
        
        if ( count($this->errors) > 0)
        {
            $this->result['errors'] = $this->errors;
        }
        
        if ( count($this->data) > 0 ) {
            $this->result['data'] = $this->data;
        }

        if ( $this->result['status'] == 'ok' ) {
            $this->result['status'] = true;
        }
        
        return json_encode($this->result);
    }
}

/******************* APIJsonResponse.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** APIJsonResponse.php ******************/;
