<?php
/******************* site.class.php *******************
 *
 * Site class
 *
 * @author MicroPHP <info@microphp.com>
 * @web www.microphp.com
 * @copyright 2004-2019 MicroPHP
 *
 ******************** site.class.php ******************/

/** 
 * Define Namespace 
 */
namespace mcms5xx\classes;

/**
 * Checking if class included normally.
 */
if (!defined('mCMScheck')) {
    die('mCMS by MicroPHP');
}

/**
 * Site class
 */
class Site
{
    /**
     * Utils class
     */
    public $utils;

    /**
     * Database class
     */
    public $db;

    /**
     * Start class
     *
     * @param $utils
     * @param $db
     */
    public function __construct($utils, $db)
    {
        $this->utils = $utils;
        $this->db = $db;
    }

    /**
     * Get languages
     */
    public function getLangs()
    {
        $langs = $this->getKey('lang_langs', 'en');
        $langs = explode(',', $langs);
        return $langs;
    }

    /**
     * Get default language
     */
    public function getDefaultLang()
    {
        $lang = $this->getKey('lang_mainlang', 'en');

        return $lang;
    }

    /**
     * Show langauges
     */
    public function getShowLangs()
    {
        $langs = $this->getKey('lang_showlangs', 'en');
        $langs = explode(',', $langs);

        return $langs;
    }

    /**
     * get key from options table
     *
     * @param $opt_key
     * @param $def_val
     */
    public function getKey($opt_key, $def_val)
    {
        $value = $def_val;
        $result = $this->db->query('SELECT * FROM `'.$this->db->prefix."options` WHERE `opt_key`='".$opt_key."'");
        if ($row = $this->db->fetch($result)) {
            $value = $row['value'];
        }

        return $value;
    }

    /**
     * filter text tags
     *
     * @param $text
     */
    public function f_tags($text)
    {
        $from_tag = array('<P>', '</P>', '<STRONG>', '</STRONG>', '<B>', '</B>', '<A', '</A>', '<BR>', '<DIV', '</DIV>', '<UL', '</UL>', '<LI', '</LI>');
        $to_tag = array('<p>', '</p>', '<strong>', '</strong>', '<b>', '</b>', '<a', '</a>', '<br />', '<div', '</div>', '<ul', '</ul>', '<li', '</li>');
        $text = str_replace($from_tag, $to_tag, $text);

        return $text;
    }

    /**
     * Replace image in text
     *
     * @param string $text
     * @param string $curr_folder
     */
    public function replaceImageTemplate($text, $curr_folder = '/')
    {
        $new_text = $this->uploadLink($text);

        $imgArr = preg_split('/({IMAGE_+)/i', $new_text, -1, PREG_SPLIT_DELIM_CAPTURE);
        //print_r($imgArr);
        for ($i = 0; $i < count($imgArr); ++$i) {
            if ($imgArr[$i] == '{IMAGE_') {
                $j = $i + 1;
                $imgArrTxt = explode('}', $imgArr[$j]);
                $idArr = explode('_', $imgArrTxt['0']);
                if (count($idArr) == 2) {
                    $id = (int) $idArr['0'];
                    $img_sz = (int) str_replace('XX', '', $idArr['1']);
                    if ($img_sz > 0) {
                        $new_text = str_replace('{IMAGE_'.$id.'_XX'.$img_sz.'}', '<img src="'.$GLOBALS['index']->curr_folder.'file_img.php?i='.$id.'&w=0&h='.$img_sz.'" alt="" >', $new_text);
                        $new_text = str_replace('{IMAGE_'.$id.'_'.$img_sz.'XX}', '<img src="'.$GLOBALS['index']->curr_folder.'file_img.php?i='.$id.'&w='.$img_sz.'&h=0" alt="" >', $new_text);
                    }
                    $imgSzX = str_replace('S', '', $idArr['1']);
                    $imgSzArr = explode('X', $imgSzX);
                    if (count($imgSzArr) == 2) {
                        $new_text = str_replace('{IMAGE_'.$id.'_S'.$imgSzArr['0'].'X'.$imgSzArr['1'].'}', '<img src="'.$GLOBALS['index']->curr_folder.'file_img.php?i='.$id.'&w='.$imgSzArr['0'].'&h='.$imgSzArr['1'].'" alt="" >', $new_text);
                    }
                }
            }
        }

        $imgArr = preg_split('/({IMAGELEFT_+)/i', $new_text, -1, PREG_SPLIT_DELIM_CAPTURE);
        //print_r($imgArr);
        for ($i = 0; $i < count($imgArr); ++$i) {
            if ($imgArr[$i] == '{IMAGELEFT_') {
                $j = $i + 1;
                $imgArrTxt = explode('}', $imgArr[$j]);
                $idArr = explode('_', $imgArrTxt['0']);
                if (count($idArr) == 2) {
                    $id = (int) $idArr['0'];
                    $img_sz = (int) str_replace('XX', '', $idArr['1']);
                    if ($img_sz > 0) {
                        $new_text = str_replace('{IMAGELEFT_'.$id.'_XX'.$img_sz.'}', '<img src="'.$GLOBALS['index']->curr_folder.'file_img.php?i='.$id.'&w=0&h='.$img_sz.'" alt="" style="float:left;margin: 0 10px 10px 0;" >', $new_text);
                        $new_text = str_replace('{IMAGELEFT_'.$id.'_'.$img_sz.'XX}', '<img src="'.$GLOBALS['index']->curr_folder.'file_img.php?i='.$id.'&w='.$img_sz.'&h=0" alt="" style="float:left;margin: 0 10px 10px 0;" >', $new_text);
                    }
                    $imgSzX = str_replace('S', '', $idArr['1']);
                    $imgSzArr = explode('X', $imgSzX);
                    if (count($imgSzArr) == 2) {
                        $new_text = str_replace('{IMAGELEFT_'.$id.'_S'.$imgSzArr['0'].'X'.$imgSzArr['1'].'}', '<img src="'.$GLOBALS['index']->curr_folder.'file_img.php?i='.$id.'&w='.$imgSzArr['0'].'&h='.$imgSzArr['1'].'" alt="" style="float:left;margin: 0 10px 10px 0;" >', $new_text);
                    }
                }
            }
        }

        $imgArr = preg_split('/({IMAGERIGHT_+)/i', $new_text, -1, PREG_SPLIT_DELIM_CAPTURE);
        //print_r($imgArr);
        for ($i = 0; $i < count($imgArr); ++$i) {
            if ($imgArr[$i] == '{IMAGERIGHT_') {
                $j = $i + 1;
                $imgArrTxt = explode('}', $imgArr[$j]);
                $idArr = explode('_', $imgArrTxt['0']);
                if (count($idArr) == 2) {
                    $id = (int) $idArr['0'];
                    $img_sz = (int) str_replace('XX', '', $idArr['1']);
                    if ($img_sz > 0) {
                        $new_text = str_replace('{IMAGERIGHT_'.$id.'_XX'.$img_sz.'}', '<img src="'.$GLOBALS['index']->curr_folder.'file_img.php?i='.$id.'&w=0&h='.$img_sz.'" alt="" style="float:right;margin: 0 0 10px 10px;" >', $new_text);
                        $new_text = str_replace('{IMAGERIGHT_'.$id.'_'.$img_sz.'XX}', '<img src="'.$GLOBALS['index']->curr_folder.'file_img.php?i='.$id.'&w='.$img_sz.'&h=0" alt="" style="float:right;margin: 0 0 10px 10px;" >', $new_text);
                    }
                    $imgSzX = str_replace('S', '', $idArr['1']);
                    $imgSzArr = explode('X', $imgSzX);
                    if (count($imgSzArr) == 2) {
                        $new_text = str_replace('{IMAGERIGHT_'.$id.'_S'.$imgSzArr['0'].'X'.$imgSzArr['1'].'}', '<img src="'.$GLOBALS['index']->curr_folder.'file_img.php?i='.$id.'&w='.$imgSzArr['0'].'&h='.$imgSzArr['1'].'" alt="" style="float:right;margin: 0 0 10px 10px;" >', $new_text);
                    }
                }
            }
        }

        $pattern = "/{IMAGEFULL_(\d+)}/i";
        $new_text = preg_replace_callback($pattern, function ($matches) {
            return $this->getFullImg($matches['1'], $GLOBALS['index']->curr_folder);
        }, $new_text);

        $pattern = "/{IMAGETHUMB_(\d+)}/i";
        $new_text = preg_replace_callback($pattern, function ($matches) {
            return $this->getThumbnailImg($matches['1']);
        }, $new_text);

        $pattern = "/{IMAGESMALL_(\d+)}/i";
        $new_text = preg_replace_callback($pattern, function ($matches) {
            return $this->getSmallImg($matches['1']);
        }, $new_text);

        $pattern = "/{IMAGELEFT_(\d+)}/i";
        $new_text = preg_replace_callback($pattern, function ($matches) {
            return $this->getAlignImg($matches['1'], 'left');
        }, $new_text);

        $pattern = "/{IMAGERIGHT_(\d+)}/i";
        $new_text = preg_replace_callback($pattern, function ($matches) {
            return $this->getAlignImg($matches['1'], 'right');
        }, $new_text);

        $pattern = "/{IMAGECENTER_(\d+)}/i";
        $new_text = preg_replace_callback($pattern, function ($matches) {
            return $this->getAlignImg($matches['1'], 'center');
        }, $new_text);

        $pattern = "/{IMAGESIZE_(\d+)}/i";
        $new_text = preg_replace_callback($pattern, function ($matches) {
            return $this->getSmImg($matches['1']);
        }, $new_text);

        $pattern = "/{IMAGESIZELEFT_(\d+)}/i";
        $new_text = preg_replace_callback($pattern, function ($matches) {
            return $this->getSmImgAlign($matches['1'], 'left');
        }, $new_text);

        $pattern = "/{IMAGESIZERIGHT_(\d+)}/i";
        $new_text = preg_replace_callback($pattern, function ($matches) {
            return $this->getSmImgAlign($matches['1'], 'right');
        }, $new_text);

        $pattern = "/{IMAGE_LEFT_(\d+)}/i";
        $new_text = preg_replace_callback($pattern, function ($matches) {
            return $this->getSmallImgAlign($matches['1'], 'left');
        }, $new_text);

        $pattern = "/{IMAGE_RIGHT_(\d+)}/i";
        $new_text = preg_replace_callback($pattern, function ($matches) {
            return $this->getSmallImgAlign($matches['1'], 'right');
        }, $new_text);

        $pattern = "/{pdf_(\d+)}/i";
        $new_text = preg_replace_callback($pattern, function ($matches) {
            return $this->getPdf($matches['1']);
        }, $new_text);

        $pattern = "/{IMAGE_(\d+)}/i";
        $new_text = preg_replace_callback($pattern, function ($matches) {
            return $this->getImg($matches['1']);
        }, $new_text);

        $pattern = "/{VIDEO_(\d+)}/i";
        $new_text = preg_replace_callback($pattern, function ($matches) {
            return $this->getVideo($matches['1']);
        }, $new_text);

        $pattern = "/{MYVIDEO_(\d+)}/i";
        $new_text = preg_replace_callback($pattern, function ($matches) {
            return $this->getMyVideo($matches['1']);
        }, $new_text);

        $pattern = "/{SLIDE_(\d+)}/i";
        $new_text = preg_replace_callback($pattern, function ($matches) {
            return $this->getSlide($matches['1']);
        }, $new_text);

        $pattern = "/{DOC_(\d+)}/i";
        $new_text = preg_replace_callback($pattern, function ($matches) {
            return $this->getDoc($matches['1']);
        }, $new_text);

        return $new_text;
    }

    /**
     * Uplaod link
     *
     * @param $txt
     */
    public function uploadLink($txt)
    {
        $dir = $this->utils->getFromConfig('upload_folder');
        $txt = str_replace('../'.$dir, $GLOBALS['index']->curr_folder.$dir, $txt);
        $txt = str_replace('../uploads/', $GLOBALS['index']->curr_folder.'uploads/', $txt);
        $txt = str_replace('<pre>', '<p>', $txt);
        $txt = str_replace('</pre>', '</p>', $txt);
        $txt = str_replace('href="../', 'href="./', $txt);

        return $txt;
    }

    /**
     * get file by ID
     *
     * @param $fileid
     * @param $cat
     */
    public function getFile($fileid, $cat = '')
    {
        $image = '';
        $query = 'SELECT * FROM `'.$this->db->prefix."files` WHERE `fileid`='".$fileid."'";
        if ($cat != '') {
            $query .= " AND category='".$cat."' ";
        }
        $result = $this->db->query($query);
        if ($row = $this->db->fetch($result)) {
            $filename = $row['filename'];
            $dir = $this->utils->getFromConfig('upload_folder');
            $dir .= 'image/';
            $dir = $this->utils->io->dateFolder($dir, $row['add_time']).'/';
            $image = $dir.$filename;
        }

        return $image;
    }

    /**
     * get PDF file by id
     *
     * @param $fileid
     */
    public function getPdfFileName($fileid)
    {
        $filename = '';

        $query = 'SELECT * FROM '.$this->db->prefix."pdf WHERE `pdf_id`='".$fileid."'";
        $result = $this->db->query($query);
        if ($row = $this->db->fetch($result)) {
            $filename = 'file_'.$row['pdf_id'].'.pdf';
        }

        return $filename;
    }

    /**
     * get PDF images
     *
     * @param $fileid
     */
    public function getPdfImages($fileid)
    {
        $imageArr = array();
        $filename = $this->getPdfFileName($fileid);
        if (strlen($filename) > 5) {
            $query = 'SELECT * FROM '.$this->db->prefix."pdf_images WHERE `pdf_id`='".$fileid."' ORDER BY `position` ASC";
            $result = $this->db->query($query);
            while ($row = $this->db->fetch($result)) {
                $thumb_img = '/pdf_img/x605/'.$row['img_id'].'.jpg';
                $imageArr[] = array($row['img_id'], $row['img_w'], $row['img_h']);
            }
        }

        return $imageArr;
    }

    /**
     * set PDF images
     *
     * @param $fileid
     */
    public function setPdfImages($fileid)
    {
        $filename = $this->getPdfFileName($fileid);
        if (strlen($filename) > 5) {
            $query = 'SELECT * FROM '.$this->db->prefix."pdf_images WHERE `pdf_id`='".$fileid."' ORDER BY `position` ASC";
            $result = $this->db->query($query);
            while ($row = $this->db->fetch($result)) {
                $thumb_img = '../pdf_img/x605/'.$row['img_id'].'.jpg';
                if (!file_exists($thumb_img)) {
                    echo '<img src="/img_x605.php?id='.$row['img_id'].'" alt="" style="width:0px;height:0px;" />';
                }
            }
        }
    }

    /**
     * get download file by id
     *
     * @param $fileid
     */
    public function getDownloadFile($fileid)
    {
        $filedown = '';

        $query = 'SELECT * FROM '.$this->db->prefix."downloads WHERE downloadsid='".$fileid."'";

        $downloads_category = $this->utils->getFromConfig('downloads_category');

        $result = $this->db->query($query);
        if ($row = $this->db->fetch($result)) {
            $filename = $row['filename'];
            $dir = $this->utils->getFromConfig('downloads_folder');
            $category = $row['category'];
            $dir .= $downloads_category[$category].'/';
            $filedown = $dir.$filename;
        }

        return $filedown;
    }

    /**
     * get file extension
     *
     * @param string $file_nm
     */
    public function getFileExt($file_nm)
    {
        $file_nmArr = explode('.', $file_nm);
        $file_nmID = count($file_nmArr) - 1;

        return strtolower($file_nmArr[$file_nmID]);
    }

    /**
     * get File information
     *
     * @param $fileid
     * @param $cat
     */
    public function getFileInfo($fileid, $cat = '')
    {
        $image = '';

        $query = 'SELECT * FROM '.$this->db->prefix."files WHERE fileid='".$fileid."'";
        if ($cat != '') {
            $query .= " AND category='".$cat."' ";
        }
        $result = $this->db->query($query);
        $title = '';
        if ($row = $this->db->fetch($result)) {
            $title = $row['title'];
        }

        return $title;
    }

    /**
     * get URL
     *
     * @param $fileid
     * @param $cat
     */
    public function getURL($fileid, $cat = '')
    {
        $image = '';

        $query = 'SELECT * FROM '.$this->db->prefix."files WHERE fileid='".$fileid."'";
        if ($cat != '') {
            $query .= " AND category='".$cat."' ";
        }
        $result = $this->db->query($query);
        $title = '';
        if ($row = $this->db->fetch($result)) {
            $title = $row['title'];
        }
        $pos = strpos($title, 'http://');
        if ($pos === false) {
            $title = 'http://'.$title;
        }

        return $title;
    }

    /**
     * get Video file by id
     *
     * @param $fileid
     * @param $cat
     */
    public function getVideoFile($fileid, $cat = '')
    {
        $image = '';

        $query = 'SELECT * FROM '.$this->db->prefix."files WHERE fileid='".$fileid."'";
        if ($cat != '') {
            $query .= " AND category='".$cat."' ";
        }
        $result = $this->db->query($query);
        if ($row = $this->db->fetch($result)) {
            $filename = $row['filename'];
            $dir = $this->utils->getFromConfig('upload_folder');
            $dir .= 'multimedia/';
            $image = $dir.$filename;
        }

        return $image;
    }

    /**
     * get DOC file by id
     *
     * @param $fileid
     */
    public function getDocFile($fileid)
    {
        $image = '';

        $query = 'SELECT * FROM '.$this->db->prefix."files WHERE fileid='".$fileid."'";

        $result = $this->db->query($query);
        if ($row = $this->db->fetch($result)) {
            $filename = $row['filename'];
            $dir = $this->utils->getFromConfig('upload_folder');
            $dir .= 'document/';
            $image = $dir.$filename;
        }

        return $image;
    }

    /**
     * get formatetd date
     *
     * @param integer $date
     */
    public function getFormattedDate($date)
    {
        if ($date < 1) {
            return '';
        }
        $formated_date = date('d.m.Y H:i', $date);

        return $formated_date;
    }


    /**
     * clear templates
     *
     * @param $text
     */
    public function clearTemplates($text)
    {
        $new_text = $text;

        $pattern = "/{(\w+)_(\d+)}/ie";

        $new_text = preg_replace($pattern, '', $new_text);

        return $new_text;
    }

    /**
     * URL Filter
     *
     * @param string $url
     */
    public function url_filter($url)
    {
        $url = strtolower($url);

        for ($i = 0; $i < strlen($url); ++$i) {
            if (!preg_match('/^[a-z_~-]+$/i', $url[$i])) {
                $url[$i] = str_replace($url[$i], '-', $url[$i]);
            }
        }

        return $url;
    }

    /**
     * Random image
     *
     * @param $images
     */
    public function rand_image($images)
    {
        srand($this->make_seed());
        $txt_img = '';
        $images_arr = explode(';', $images);
        if (count($images_arr) > 0) {
            $r_id = rand(1, count($images_arr)) - 1;
            $txt_img = '<img src="'.$this->getFile($images_arr[$r_id]).'" alt="[alt]" title="[alt]" /> ';
        }

        return $txt_img;
    }

    /**
     * Find image
     *
     * @param $images
     */
    public function find_image($images)
    {
        srand($this->make_seed());
        $txt_img = '';
        $images_arr = explode(';', $images);
        if (count($images_arr) > 0) {
            $r_id = rand(1, count($images_arr)) - 1;
            $txt_img = $this->getFile($images_arr[$r_id]);
        }

        return $txt_img;
    }

    /**
     * make seed
     */
    public function make_seed()
    {
        list($usec, $sec) = explode(' ', microtime());

        return (float) $sec + ((float) $usec * 100000);
    }

    /**
     * Get Image by id
     *
     * @param $fileid
     */
    public function getImg($fileid)
    {
        return $this->getFile($fileid, 'image');
    }

    /**
     * get PDF file by id
     *
     * @param $fileid
     */
    public function getPdf($fileid)
    {
        $pdf_txt = '';
        $filename = $this->getPdfFileName($fileid);
        if (strlen($filename) > 5) {
            $imagesArr = $this->getPdfImages($fileid);
            $pdf_txt = '<a href="/pdf_flipp/index.php?id='.$fileid.'" onclick="window.open(this.href, \'_blank\');return false;"><img src="/img_150x.php?id='.$imagesArr['0']['0'].'" alt="" /></a>';
        }

        return $pdf_txt;
    }

    /**
     * get Video file by id
     *
     * @param $fileid
     */
    public function getVideo($fileid)
    {
        return '&nbsp;<a href="'.$this->getVideoFile($fileid).'" target="blank"><img src="./system/templates/default/view/img/video.jpg" border="0" /></a>&nbsp;';
    }

    /**
     * get Myvideo by id
     *
     * @param $video
     */
    public function getMyVideo($video)
    {
        return '<iframe width="560" height="315" src="http://embed.myvideo.az/flv_player/player.php?video_id='.$video.'.mp4" frameborder="0"></iframe>';
    }

    /**
     * get Slide by id
     *
     * @param $id
     */
    public function getSlide($id)
    {
        return '<iframe src="http://www.slideshare.net/slideshow/embed_code/'.$id.'" width="476" height="400" frameborder="0" marginwidth="0" marginheight="0" scrolling="no"></iframe>';
    }

    /**
     * get DOC file by id
     *
     * @param $fileid
     */
    public function getDoc($fileid)
    {
        return '&nbsp;<a href="'.$this->getDocFile($fileid).'" target="blank">Doc</a>&nbsp;';
    }

    /**
     * get Youtube
     *
     * @param $file_url
     */
    public function getYoutube($file_url)
    {
        $txt = '';
        $file_urlArr = explode('http://www.youtube.com/watch?', $file_url);
        $file_urlArr2 = @explode('&amp;', $file_urlArr[count($file_urlArr) - 1]);
        if (count($file_urlArr2) == 1) {
            $file_urlArr2 = @explode('&', $file_urlArr2['0']);
        }
        $yurl = '';
        foreach ($file_urlArr2 as $val) {
            if (strpos($val, 'v=') !== false) {
                $yurl = str_replace('v=', '', $val);
            }
        }
        if (strlen($yurl) > 2) {
            $txt = '
				<iframe width="480" height="390" src="http://www.youtube.com/embed/' .$yurl.'" frameborder="0" allowfullscreen></iframe>
				';
        }

        return $txt;
    }

    /**
     * get Small image by id
     *
     * @param $fileid
     */
    public function getSmallImg($fileid)
    {
        return '<img src="'.$this->getFile($fileid, 'image').'" border="0" />';
    }

    /**
     * get aligned image
     *
     * @param $fileid
     * @param $alignmnemt
     */
    public function getAlignImg($fileid, $alignmnemt)
    {
        return '<img src="'.$this->getFile($fileid, 'image').'" border="0" align="'.$alignmnemt.'" hspace="0" />';
    }

    /**
     * get small image
     *
     * @param $fileid
     */
    public function getSmImg($fileid)
    {
        $PhNM = $this->getFile($fileid, 'image');
        //$PhNM = str_replace(" ", "%20", "$PhNM");
        $size = @getimagesize("$PhNM");
        $sz1 = $size[0];
        $sz2 = $size[1];

        return "<img class=\"PhotoHover\" alt=\"Click to Enlarge\" onclick=\"ShowProccess('".$this->getFile($fileid, 'image')."', ".$sz1.', '.$sz2.", '', this)\"
							src=\"./show_thumbs.php?f=" .base64_encode($this->getFile($fileid, 'image')).'" border="1" alt="">
					';
    }

    /**
     * get Thumbnail image
     *
     * @param $fileid
     * @param string $align
     */
    public function getThumbnailImg($fileid, $align = 'center')
    {
        $PhNM = $this->getFile($fileid, 'image');
        $size = @getimagesize("$PhNM");
        $sz1 = $size[0];
        $sz2 = $size[1];
        $big_img = '/img/big/'.$fileid.'.jpg';

        return '
				<img src="/img_big.php?f=' .$fileid.'" alt="" style="width:0px;" />
				<a class="fancybox-effects" href="' .$big_img.'" title="[alt]" >
					<img src="/img_thumb.php?f=' .$fileid.'" alt="[alt]" title="[alt]" align="'.$align.'" />
				</a>';
    }

    /**
     * get Small image
     *
     * @param $fileid
     * @param string $alignmnemt
     */
    public function getSmImgAlign($fileid, $alignmnemt)
    {
        $PhNM = $this->getFile($fileid, 'image');
        //$PhNM = str_replace(" ", "%20", "$PhNM");
        $size = @getimagesize("$PhNM");
        $sz1 = $size[0];
        $sz2 = $size[1];

        return "<img class=\"PhotoHover\" alt=\"Click to Enlarge\" onclick=\"ShowProccess('".$this->getFile($fileid, 'image')."', ".$sz1.', '.$sz2.", '', this)\"
							src=\"./show_thumbs.php?f=" .base64_encode($this->getFile($fileid, 'image')).'" border="1" alt="" align="'.$alignmnemt.'" hspace="10" />
					';
    }

    /**
     * get Small image
     *
     * @param $fileid
     * @param string $alignmnemt
     */
    public function getSmallImgAlign($fileid, $alignmnemt)
    {
        $PhNM = $this->getFile($fileid, 'image');
        $br = '';
        switch ($alignmnemt) {
            case 'right': {
                $img_align = ' style="float:right; margin: 0px 10px 0px 10px;" ';
                break;
            }
            case 'left': {
                $img_align = ' style="float:left; margin: 0px 10px 0px 10px;" ';
                $br = '<br style=" width: 100%; clear: both !important;" />';
                break;
            }

        }

        return '<img src="/show_small.php?f='.base64_encode($this->getFile($fileid, 'image')).'" alt="[alt]" title="[alt]" '.$img_align.' />'.$br;
    }

    /**
     * get Full image
     *
     * @param $fileid
     * @param $curr_folder
     */
    public function getFullImg($fileid, $curr_folder = '/')
    {
        return '<img src="'.$curr_folder.$this->getFile($fileid, 'image').'" border="0" />';
    }
}

/******************* site.class.php *******************
*
* Copyright : (C) 2004 - 2019. All Rights Reserved
*
******************** site.class.php ******************/;
