<?php
/******************* paging.class.php *******************
 *
 * Pagination class
 *
 * @author MicroPHP <info@microphp.com>
 * @web www.microphp.com
 * @copyright 2004-2019 MicroPHP
 *
 * EXAMPLE:
 *
 * include($_SERVER["DOCUMENT_ROOT"] . "/classes/paging.class.php");
 * $paging = new PagedResults();
 * $paging->TotalResults = 500; //This is how many results to page through.
 *                                //$this->db->num_rows($query) would just fine!
 * print_p($paging->InfoArray());
 *
 ******************** paging.class.php ******************/

/** 
 * Define Namespace 
 */
namespace mcms5xx\classes;

/**
 * Checking if class included normally.
 */
if (!defined('mCMScheck')) {
    die('mCMS by MicroPHP');
}

/**
 * Pagination class
 */
class PagedResults
{
    /* These are defaults */
    public $TotalResults;
    public $CurrentPage = 1;
    public $PageVarName = 'page';
    public $ResultsPerPage = 20;
    public $LinksPerPage = 10;
    public $UrlPrefix = '?';
    public $Prefix;
    public $TotalPages;

    public function InfoArray()
    {
        $this->TotalPages = $this->getTotalPages();
        $this->CurrentPage = $this->getCurrentPage();
        $this->Prefix = $this->UrlPrefix.$this->PageVarName.'=';
        $this->ResultArray = array(
            'PREV_PAGE' => $this->getPrevPage(),
            'NEXT_PAGE' => $this->getNextPage(),
            'CURRENT_PAGE' => $this->CurrentPage,
            'TOTAL_PAGES' => $this->TotalPages,
            'TOTAL_RESULTS' => $this->TotalResults,
            'PAGE_NUMBERS' => $this->getNumbers(),
            'MYSQL_LIMIT1' => $this->getStartOffset(),
            'MYSQL_LIMIT2' => $this->ResultsPerPage,
            'START_OFFSET' => $this->getStartOffset(),
            'END_OFFSET' => $this->getEndOffset(),
            'RESULTS_PER_PAGE' => $this->ResultsPerPage,
            'NEXT_LINK_PAGE' => $this->getNextLinkPage(),
            'PREV_LINK_PAGE' => $this->getPrevLinkPage(),
        );

        return $this->ResultArray;
    }

    /**
     * Start information functions
     */
    public function getTotalPages()
    {
        /* Make sure we don't devide by zero */
        if ($this->TotalResults != 0 && $this->ResultsPerPage != 0) {
            $result = ceil($this->TotalResults / $this->ResultsPerPage);
        }
        /* If 0, make it 1 page */
        if (isset($result) && $result == 0) {
            return 1;
        } else {
            return @$result;
        }
    }

    public function getStartOffset()
    {
        $offset = $this->ResultsPerPage * ($this->CurrentPage - 1);
        if ($offset != 0) {
            ++$offset;
        }

        return $offset;
    }

    public function getEndOffset()
    {
        if ($this->getStartOffset() > ($this->TotalResults - $this->ResultsPerPage)) {
            $offset = $this->TotalResults;
        } elseif ($this->getStartOffset() != 0) {
            $offset = $this->getStartOffset() + $this->ResultsPerPage - 1;
        } else {
            $offset = $this->ResultsPerPage;
        }

        return $offset;
    }

    public function getCurrentPage()
    {
        if (isset($_GET[$this->PageVarName])) {
            return (int) @$_GET[$this->PageVarName];
        } elseif (isset($_POST[$this->PageVarName])) {
            return (int) @$_POST[$this->PageVarName];
        } else {
            return $this->CurrentPage;
        }
    }

    public function getPrevPage()
    {
        if ($this->CurrentPage > 1) {
            return $this->CurrentPage - 1;
        } else {
            return false;
        }
    }

    public function getNextPage()
    {
        if ($this->CurrentPage < $this->TotalPages) {
            return $this->CurrentPage + 1;
        } else {
            return false;
        }
    }

    public function getNextLinkPage_old()
    {
        if (($this->TotalPages - $this->CurrentPage) >= $this->LinksPerPage) {
            $next_link_page = floor($this->CurrentPage / $this->LinksPerPage) * $this->LinksPerPage + $this->LinksPerPage + 1;

            return $next_link_page;
        } else {
            return false;
        }
    }

    public function getNextLinkPage()
    {
        /*$c = $this->CurrentPage;
        $l = $this->LinksPerPage;
        $t = $this->TotalPages;
        $e=(ceil($c / $l)*$l);
        $r = (ceil($t / $l)*$l);*/
        //if( $r>$e )
        if ($this->CurrentPage < $this->TotalPages) {
            /*$mod = $c % $l;
            if($mod == 0)
            $c = $c-1;
            $next_link_page = (floor($c / $l)*$l) + $l + 1;*/
            $next_link_page = $this->CurrentPage + 1;

            return (int) $next_link_page;
        } else {
            return 1;
        }
    }

    public function getPrevLinkPage()
    {
        if ($this->CurrentPage > 1) {
            //$prev_link_page = floor($this->CurrentPage / $this->LinksPerPage)*$this->LinksPerPage - $this->LinksPerPage + 1;
            $prev_link_page = $this->CurrentPage - 1;

            return (int) $prev_link_page;
        } else {
            return 1;
        }
    }

    public function getStartNumber()
    {
        $start_no = floor(($this->CurrentPage - 1) / $this->LinksPerPage) * $this->LinksPerPage + 1;

        return $start_no;
        $links_per_page_half = $this->LinksPerPage / 2;
        /* See if curpage is less than half links per page */
        if ($this->CurrentPage <= $links_per_page_half || $this->TotalPages <= $this->LinksPerPage) {
            return 1;
            /* See if curpage is greater than TotalPages minus Half links per page */
        } elseif ($this->CurrentPage >= ($this->TotalPages - $links_per_page_half)) {
            return $this->TotalPages - $this->LinksPerPage + 1;
        } else {
            return $this->CurrentPage - $links_per_page_half;
        }
    }

    public function getEndNumber()
    {
        if ($this->TotalPages < $this->LinksPerPage) {
            return $this->TotalPages;
        } else {
            $end_no = $this->getStartNumber() - 1 + $this->LinksPerPage;
            if ($end_no > $this->TotalPages) {
                $end_no = $this->TotalPages;
            }

            return $end_no;
        }
    }

    public function getNumbers()
    {
        for ($i = $this->getStartNumber(); $i <= $this->getEndNumber(); ++$i) {
            $numbers[] = $i;
        }

        return @$numbers;
    }

    public function GetPaging()
    {
        $paging = '';
        /* Get our array of valuable paging information! */
        $InfoArray = $this->InfoArray();

        /* Print out our prev link page*/
        if ($InfoArray['PREV_LINK_PAGE']) {
            $paging .= "<a href='".$this->Prefix.$InfoArray['PREV_LINK_PAGE']."'>(Prev Link Page)</a> ";
        } else {
            $paging .= '(Prev Link Page) ';
        }

        /* Print our first link */
        if ($InfoArray['CURRENT_PAGE'] != 1) {
            $paging .= "<a href='".$this->Prefix."1'>First</a> ";
        } else {
            $paging .= 'First ';
        }

        /* Print out our prev link */
        if ($InfoArray['PREV_PAGE']) {
            $paging .= "<a href='".$this->Prefix.$InfoArray['PREV_PAGE']."'>Previous</a> | ";
        } else {
            $paging .= 'Previous | ';
        }

        /* Example of how to print our number links! */
        for ($i = 0; $i < count($InfoArray['PAGE_NUMBERS']); ++$i) {
            if ($InfoArray['CURRENT_PAGE'] == $InfoArray['PAGE_NUMBERS'][$i]) {
                $paging .= $InfoArray['PAGE_NUMBERS'][$i].' | ';
            } else {
                $paging .= "<a href='".$this->Prefix.$InfoArray['PAGE_NUMBERS'][$i]."'>".$InfoArray['PAGE_NUMBERS'][$i].'</a> | ';
            }
        }

        /* Print out our next link */
        if ($InfoArray['NEXT_PAGE']) {
            $paging .= " <a href='".$this->Prefix.$InfoArray['NEXT_PAGE']."'>Next</a>";
        } else {
            $paging .= ' Next';
        }

        /* Print our last link */
        if ($InfoArray['CURRENT_PAGE'] != $InfoArray['TOTAL_PAGES']) {
            $paging .= " <a href='".$this->Prefix.$InfoArray['TOTAL_PAGES']."'>Last</a>";
        } else {
            $paging .= ' Last';
        }

        /* Print out our next link */
        if ($InfoArray['NEXT_LINK_PAGE']) {
            $paging .= " <a href='".$this->Prefix.$InfoArray['NEXT_LINK_PAGE']."'>(Next Link Page)</a>";
        } else {
            $paging .= ' (Next Link Page)';
        }

        return $paging;
    }

    public function GetCustomPaging()
    {
        $paging = '';
        /* Get our array of valuable paging information! */
        $InfoArray = $this->InfoArray();

        /* Print out our prev link page*/
        if ($InfoArray['PREV_LINK_PAGE']) {
            $paging .= "<a href='".$this->Prefix.$InfoArray['PREV_LINK_PAGE']."'>&lt;&lt;</a> ";
        } else {
            $paging .= '&lt;&lt; ';
        }

        /* Example of how to print our number links! */
        for ($i = 0; $i < count($InfoArray['PAGE_NUMBERS']); ++$i) {
            if ($InfoArray['CURRENT_PAGE'] == $InfoArray['PAGE_NUMBERS'][$i]) {
                $paging .= $InfoArray['PAGE_NUMBERS'][$i].' | ';
            } else {
                $paging .= "<a href='".$this->Prefix.$InfoArray['PAGE_NUMBERS'][$i]."'>".$InfoArray['PAGE_NUMBERS'][$i].'</a> | ';
            }
        }

        /* Print out our next link */
        if ($InfoArray['NEXT_LINK_PAGE']) {
            $paging .= " <a href='".$this->Prefix.$InfoArray['NEXT_LINK_PAGE']."'>&gt;&gt;</a>";
        } else {
            $paging .= ' &gt;&gt;';
        }

        return $paging;
    }

    public function GetEventsAdminPaging()
    {
        $paging = '';
        /* Get our array of valuable paging information! */
        $InfoArray = $this->InfoArray();

        /* Print out our prev link page*/
        if ($InfoArray['PREV_LINK_PAGE']) {
            $paging .= '<td  align="center" valign="middle"><a href="'.$this->Prefix.$InfoArray['PREV_LINK_PAGE'].'"><img src="images/events/DoubleconLeft.gif" align="middle"></a></td>';
        } else {
            $paging .= '<td  align="center" valign="middle"><img src="images/events/DoubleconLeft.gif" align="middle"></td>';
        }

        $paging .= '<td width="2"><table width="2"><tr><td></td></tr></table></td>';

        /* Print out our prev link */
        if ($InfoArray['PREV_PAGE']) {
            $paging .= '<td align="center" valign="middle"><a href="'.$this->Prefix.$InfoArray['PREV_PAGE'].'"><img src="images/events/OneconLeft.gif" align="middle"></a></td>';
        } else {
            $paging .= '<td align="center" valign="middle"><img src="images/events/OneconLeft.gif" align="middle"></td>';
        }

        $paging .= '<td width="10"><table width="10"><tr><td></td></tr></table></td>';

        $paging .= '<td class="Text" align="center">';
        /* Example of how to print our number links! */
        $count = count($InfoArray['PAGE_NUMBERS']);
        for ($i = 0; $i < $count; ++$i) {
            if ($InfoArray['CURRENT_PAGE'] == $InfoArray['PAGE_NUMBERS'][$i]) {
                $paging .= $InfoArray['PAGE_NUMBERS'][$i];
                if ($i < ($count - 1)) {
                    $paging .= ', ';
                }
            } else {
                $paging .= '<a href="'.$this->Prefix.$InfoArray['PAGE_NUMBERS'][$i].'">'.$InfoArray['PAGE_NUMBERS'][$i].'</a>';
                if ($i < ($count - 1)) {
                    $paging .= ', ';
                }
            }
        }

        $paging .= '</td>';

        $paging .= '<td width="10"><table width="10"><tr><td></td></tr></table></td>';

        /* Print out our next link */
        if ($InfoArray['NEXT_PAGE']) {
            $paging .= '<td align="center" valign="middle"><a href="'.$this->Prefix.$InfoArray['NEXT_PAGE'].'"><img src="images/events/OneconRig.gif" align="middle"></a></td>';
        } else {
            $paging .= '<td align="center" valign="middle"><img src="images/events/OneconRig.gif" align="middle"></td>';
        }

        $paging .= '<td width="2"><table width="2"><tr><td></td></tr></table></td>';

        /* Print out our next link */
        if ($InfoArray['NEXT_LINK_PAGE']) {
            $paging .= '<td align="center" valign="middle"><a href="'.$this->Prefix.$InfoArray['NEXT_LINK_PAGE'].'"><img src="images/events/DoubleconRig.gif" align="middle"></a></td>';
        } else {
            $paging .= '<td align="center" valign="middle"><img src="images/events/DoubleconRig.gif" align="middle"></td>';
        }

        return $paging;
    }
}

/******************* paging.class.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** paging.class.php ******************/;
