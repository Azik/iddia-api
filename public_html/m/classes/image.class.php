                                                                                                                                                                                                                                                                            <?php
/******************* image.class.php *******************
 *
 * @author MicroPHP <info@microphp.com>
 * @web www.microphp.com
 * @copyright 2004-2019 MicroPHP
 *
 ******************** image.class.php ******************/

/** 
 * Define Namespace 
 */
namespace mcms5xx\classes;

/**
 * Checking if class included normally.
 */
if (!defined('mCMScheck')) {
    die('mCMS by MicroPHP');
}

/**
 * Image Class
 */
class Image
{
    public $FileName;
    public $NewName;
    public $ThumbPrefix;
    public $File;
    public $NewWidth = 600;
    public $NewHeight = 600;
    public $TWidth = 100;
    public $THeight = 100;
    public $SavePath;
    public $ThumbPath;
    public $OverWrite;
    public $NameCase;
    public $method = 1;
    private $Image;
    private $width;
    private $height;
    private $Error;

    /**
     * Image::__construct()
     *
     * @return
     */
    public function __construct()
    {
        $this->FileName = 'imagename.jpg';
        $this->OverWrite = true;
        $this->NameCase = '';
        $this->Error = '';
        $this->NewName = '';
        $this->ThumbPrefix = '';
        $this->randName = '';
    }


    /**
     * Image::UploadFile()
     *
     * @return
     */
    public function UploadFile()
    {
        if (is_array($this->File['name'])) {
            $this->_ArrayUpload();
        } else {
            $this->_NormalUpload();
        }

        return $this->Error;
    }

    /**
     * Upload::_ArrayUpload()
     *
     * @return
     */
    function _ArrayUpload()
    {
        for ($i = 0; $i < count($this->File['name']); $i++) {

            if (!empty($this->File['name'][$i]) and $this->_FileExist($this->NewName[$i], $this->File['name'][$i]) == false) {
                $this->_UploadImage($this->File['name'][$i], $this->File['tmp_name'][$i], $this->File['size'][$i], $this->File['type'][$i], $this->NewName[$i]);

                if (!empty($this->ThumbPath)) {
                    $this->_ThumbUpload($this->File['name'][$i], $this->File['tmp_name'][$i], $this->File['size'][$i], $this->File['type'][$i], $this->ThumbPrefix . $this->NewName[$i]);
                }
            }
        }
    }

    /**
     * Upload::_NormalUpload()
     *
     * @return
     */
    function _NormalUpload()
    {
        $_FileName = $this->File['name'];
        $_NewName = $this->NewName;
        $_ThumbPrefix = $this->ThumbPrefix;

        if (!empty($this->File['name']) and $this->_FileExist($_NewName, $_FileName) == false) {
            $this->_UploadImage($this->File['name'], $this->File['tmp_name'], $this->File['size'], $this->File['type'], $this->NewName);

            if (!empty($this->ThumbPath)) {
                $this->_ThumbUpload($this->File['name'], $this->File['tmp_name'], $this->File['size'], $this->File['type'], $this->ThumbPrefix . $this->NewName);
            }
        }
    }

    /**
     * Upload::_UploadImage()
     *
     * @param mixed $FileName
     * @param mixed $TmpName
     * @param mixed $Size
     * @param mixed $Type
     * @param mixed $NewName
     * @return
     */
    function _UploadImage($FileName, $TmpName, $Size, $Type, $NewName)
    {
        list($width, $height) = getimagesize($TmpName);

        $this->image = new Image($FileName);
        $this->image->newWidth = $this->NewWidth;
        $this->image->newHeight = $this->NewHeight;
        $this->image->PicDir = $this->SavePath;
        $this->image->TmpName = $TmpName;
        $this->image->FileSize = $Size;
        $this->image->FileType = $Type;

        $this->image->FileName = $this->_CheckName($NewName, $FileName);

        if ($width < $this->NewWidth and $height < $this->NewHeight) {
            $this->image->Save();
        } else {
            $this->image->Save();
            //              $this->image->Resize($this->method);
        }
    }

    /**
     * Upload::_ThumbUpload()
     *
     * @param mixed $FileName
     * @param mixed $TmpName
     * @param mixed $Size
     * @param mixed $Type
     * @param mixed $NewName
     * @return
     */
    function _ThumbUpload($FileName, $TmpName, $Size, $Type, $NewName)
    {
        list($width, $height) = getimagesize($TmpName);

        $this->Timage = new Image($FileName);
        $this->Timage->newWidth = $this->TWidth;
        $this->Timage->newHeight = $this->THeight;
        $this->Timage->PicDir = $this->ThumbPath;
        $this->Timage->TmpName = $TmpName;
        $this->Timage->FileSize = $Size;
        $this->Timage->FileType = $Type;

        $this->Timage->FileName = $this->_CheckName($NewName, $FileName);

        if ($width < $this->TWidth and $height < $this->THeight) {
            $this->Timage->Save();
        } else {
            $this->Timage->Resize($this->method);
        }
    }

    /**
     * Upload::_CheckName()
     *
     * @param mixed $NewName
     * @param mixed $UpFile
     * @return
     */
    function _CheckName($NewName, $UpFile)
    {
        if (empty($NewName)) {
            return $this->_ChangeCase($UpFile);
        } else {
            $Ext = explode(".", $UpFile);
            $Ext = end($Ext);
            $Ext = strtolower($Ext);

            $NewName = $this->_ChangeCase($NewName . "." . $Ext);
            return $NewName;
        }
    }

    /**
     * Upload::_ChangeCase()
     *
     * @param mixed $FileName
     * @return
     */
    function _ChangeCase($FileName)
    {
        if ($this->NameCase == 'lower') {
            return strtolower($FileName);
        } elseif ($this->NameCase == 'upper') {
            return strtoupper($FileName);
        } else {
            return $FileName;
        }
    }

    /**
     * Upload::_FileExist()
     *
     * @param mixed $_NewName
     * @param mixed $_FileName
     * @return
     */
    function _FileExist($_NewName, $_FileName)
    {
        if ($this->OverWrite == true) {
            if (file_exists($this->SavePath . $this->_CheckName($_NewName, $_FileName))) {
                if (!unlink($this->SavePath . $this->_CheckName($_NewName, $_FileName))) {
                    $this->Error[] = "File: " . $this->_CheckName($_NewName, $_FileName) . " Cannot verwrite.";
                } else {
                    if (file_exists($this->ThumbPath . $this->_CheckName($_NewName, $_FileName))) {
                        unlink($this->ThumbPath . $this->_CheckName($_NewName, $_FileName));
                    }
                }
            }
        } else {
            if (file_exists($this->_CheckName($_NewName, $_FileName))) {
                $this->Error[] = "File: " . $this->_CheckName($_NewName, $_FileName) . " aready exist";
                return true;
            }
        }
    }






    /**
     * Sekil upload ile bagli burda bezi propersional ishler gedib. skeilin uzunluq enine uygun duzgun ve seliqeli upload ucun. cox saytin problemidi bu.
     *
     * seliqeye salmaq lazimdi kodu
     */

    private function fileUpload($file, $_sid, $folder_temp)
    {

//				$this->sortedImages('75BABA46-3201-40AA-91AD-F334C345DFD2');

        $filename = $this->utils->filename_filter($file['name']);
        $originalname = strtolower($filename);
        if (trim($filename) != '') {
            $ext = $this->utils->GetFileExtension($filename);
            if (in_array($ext, $this->getFileTypesArray())) {
                $cat = $this->getFileCat($ext);
                if ($cat == 'image') {
                    $file_dir = $folder_temp . '/' . $_sid . '/';
                    $file_dir2 = $folder_temp . '/' . $_sid . '/org/';
                    $file_dirdef = $folder_temp . '/' . $_sid . '/def/';
                    $file_dir_thumbs = $folder_temp . '/' . $_sid . '/thumbs/';
                    $add_time = time();
                    $filename = $this->utils->GetFileName($file_dir, $filename);
                    $titles = $this->utils->txt_request_filter(str_replace("." . $ext, "", $originalname));

                    $img_file = $file_dir . $filename;
                    $img_file2 = $file_dir2 . $filename;
                    $img_file_def = $file_dirdef . $filename;
                    $img_file_thumbs = $file_dir_thumbs . $filename;

                    if (move_uploaded_file($file['tmp_name'], $img_file) && chmod($img_file, 0777)) {

                        $image_thumb_width = 230;//$this->fromConfig('image_thumb_width_ann');
                        $thumb_dir = $this->fromConfig('upload_ann_temp_folder');
                        list($original_width, $original_height, $src_t, $src_a) = getimagesize($img_file);
                        $image_thumb_width = $original_width;
                        $image_thumb_height = $original_height;


                        if ($original_width > $original_height && $original_width >= 1280) {
                            $image_thumb_width = 1280;
                            $calcP = array($original_width, $image_thumb_width);
                            $calc_p = $this->utils->calc_perc($calcP['1'], $calcP['0']);
                            $image_thumb_height = round(($original_height / $calc_p) * 100, 4);
                            $src_w = round(($image_thumb_width / 100) * $calc_p, 4);
                            $src_h = round(($image_thumb_height / 100) * $calc_p, 4);
                        } else if ($original_height > $original_width && $original_height > 1280) {
                            $image_thumb_height = 1280;
                            $calcP = array($original_height, $image_thumb_height);
                            $calc_p = $this->utils->calc_perc($calcP['1'], $calcP['0']);
                            $image_thumb_width = round(($original_width / $calc_p) * 100, 4);
                            $src_w = round(($image_thumb_width / 100) * $calc_p, 4);
                            $src_h = round(($image_thumb_height / 100) * $calc_p, 4);
                        } else {

                            $calcP = array($original_width, $image_thumb_width);
                            $calc_p = $this->utils->calc_perc($calcP['1'], $calcP['0']);
                            $image_thumb_height = round(($original_height / $calc_p) * 100, 4);
                            $src_w = round(($image_thumb_width / 100) * $calc_p, 4);
                            $src_h = round(($image_thumb_height / 100) * $calc_p, 4);

                        }


                        /*if($original_width > $original_height && $original_width < 1280){
                            $image_thumb_width = 230;
                        }
                        */


                        switch ($ext) {
                            case 'jpg':
                            case 'jpeg': {
                                $image = imagecreatefromjpeg($img_file);
                                break;
                            }
                            case 'gif': {
                                $image = imagecreatefromgif($img_file);
                                break;
                            }
                            case 'png': {
                                $image = imagecreatefrompng($img_file);
                                break;
                            }
                            default: {
                                $image = imagecreatefromjpeg($img_file);
                                break;
                            }
                        }

                        $image_p1 = imagecreatetruecolor($image_thumb_width, $image_thumb_height);
                        $watermark_x = 10;
                        $watermark_y = 10;

                        imagecopyresampled($image_p1, $image, 0, 0, 0, 0, $image_thumb_width, $image_thumb_height, $src_w, $src_h);
                        switch ($ext) {
                            case 'jpg':
                            case 'jpeg': {
                                @imagejpeg($image_p1, $img_file_def, 100);
                                break;
                            }
                            case 'gif': {
                                @imagegif($image_p1, $img_file_def);
                                break;
                            }
                            case 'png': {
                                @imagepng($image_p1, $img_file_def, 9);
                                break;
                            }
                            default: {
                                @imagejpeg($image_p1, $img_file_def, 100);
                                break;
                            }
                        }


                        @chmod($img_file_def, 0644);
                        // @imagedestroy($image_p);
                        if (move_uploaded_file($file['tmp_name'], $img_file_def)) {
                        } else {
                        }


                        imagecopyresampled($image_p1, $image, 0, 0, 0, 0, $image_thumb_width, $image_thumb_height, $src_w, $src_h);
                        switch ($ext) {
                            case 'jpg':
                            case 'jpeg': {
                                @imagejpeg($image_p1, $img_file2, 100);
                                break;
                            }
                            case 'gif': {
                                @imagegif($image_p1, $img_file2);
                                break;
                            }
                            case 'png': {
                                @imagepng($image_p1, $img_file2, 9);
                                break;
                            }
                            default: {
                                @imagejpeg($image_p1, $img_file2, 100);
                                break;
                            }
                        }


                        $wtrmrk_file = "./img/watermark.png";
                        $this->ak_img_watermark($img_file2, $wtrmrk_file, $img_file2);
                        @chmod($img_file2, 0644);
                        // @imagedestroy($image_p);
                        if (move_uploaded_file($file['tmp_name'], $img_file2)) {
                        } else {
                        }


                        $image_thumb_width = 230;
                        $image_thumb_height = 170;
                        $calcP = array($original_width, $image_thumb_width);
                        $calc_p = $this->utils->calc_perc($calcP['1'], $calcP['0']);
                        $src_w = round(($image_thumb_width / 100) * $calc_p, 4);
                        $src_h = round(($image_thumb_height / 100) * $calc_p, 4);

                        $image_p = imagecreatetruecolor($image_thumb_width, $image_thumb_height);

                        imagecopyresampled($image_p, $image, 0, 0, 0, 0, $image_thumb_width, $image_thumb_height, $src_w, $src_h);
                        switch ($ext) {
                            case 'jpg':
                            case 'jpeg': {

                                @imagejpeg($this->cropAlign($image_p1, 231, 170, 'center', 'center'), $img_file_thumbs, 100);
                                break;
                            }
                            case 'gif': {
                                @imagegif($this->cropAlign($image_p1, 231, 170, 'center', 'center'), $img_file_thumbs);
                                break;
                            }
                            case 'png': {
                                @imagepng($this->cropAlign($image_p1, 231, 170, 'center', 'center'), $img_file_thumbs, 9);
                                break;
                            }
                            default: {
                                @imagejpeg($this->cropAlign($image_p1, 231, 170, 'center', 'center'), $img_file_thumbs, 100);
                                break;
                            }
                        }
                        @chmod($img_file2, 0644);
                        @imagedestroy($image_p);
                        if (move_uploaded_file($file['tmp_name'], $img_file_thumbs)) {
                        } else {
                        }

                        //$this->crop_center($original_width, $original_height, 230, 171);


                        unlink($img_file);


                        $dataInsert = array();
                        $dataInsert['sid'] = $_sid;
                        $dataInsert['originalname'] = $originalname;
                        //emlak.az-satilir-4-otaqli-80-msup2sup-obyekt-bakixanov-785c03d67e1856c.jpg
                        $dataInsert['filename'] = $filename;
                        $dataInsert['extension'] = $ext;
                        $dataInsert['ip'] = $_SERVER['REMOTE_ADDR'];
                        $dataInsert['istemp'] = 'y';
                        $dataInsert['add_time'] = time();
                        $insertid = $this->db->insert($this->db->prefix . "ann_file_uploads_temp", $dataInsert);

                        return $img_file2;
                        exit;
                        //// for insert block

                    }
                } else {
                    echo 'invalid file';
                    exit;
                }
            } else {
                echo 'invalid file';
                exit;
            }

        }
    }


    public function cropAlign($image, $cropWidth, $cropHeight, $horizontalAlign = 'center', $verticalAlign = 'middle')
    {
        $width = imagesx($image);
        $height = imagesy($image);
        $horizontalAlignPixels = $this->calculatePixelsForAlign($width, $cropWidth, $horizontalAlign);
        $verticalAlignPixels = $this->calculatePixelsForAlign($height, $cropHeight, $verticalAlign);
        return imageCrop($image, [
            'x' => $horizontalAlignPixels[0],
            'y' => $verticalAlignPixels[0],
            'width' => $horizontalAlignPixels[1],
            'height' => $verticalAlignPixels[1]
        ]);
    }

    private function doActionRotate($inaction)
    {
        $res = '';
        $action = $this->utils->UserPost("action");
        $isSuccess = true;
        if (($action == $inaction) && ($_SERVER['REQUEST_METHOD'] == 'POST')) {
            $folder_temp = $this->fromConfig('upload_ann_temp_folder');
            $folder = $this->fromConfig('upload_ann_temp_folder');

            $_sid = $this->utils->UserPost("sid");
            $_atype = $this->utils->UserPost("atype");
            $degrees = 0;

            if ($_atype == 'left') $degrees = 90;
            if ($_atype == 'right') $degrees = -90;
            //$folder_temp = $this->io->dateFolder($folder_temp."/", time()) . '/';

            $_thumbImage = $this->utils->UserPost("thumbImage");
            $_thumbImage = explode('/', $_thumbImage);
            $dateFolder = $_thumbImage['4'] . "/" . $_thumbImage['5'];
            $_thumbImage = end($_thumbImage);
            //$_thumbImage = str_replace('http://www.demo.ipoteka.az/'.$folder_temp.'//'.$_sid.'/org/','', $_thumbImage);


            if (trim($_thumbImage) != '')
                $ext = $this->utils->GetFileExtension($_thumbImage);

            if (in_array($ext, $this->getFileTypesArray())) {
                $cat = $this->getFileCat($ext);
                if ($cat == 'image') {
                    $file_dirRoot = $folder_temp . '/' . $dateFolder . '/' . $_sid . '/thumbs/';
                    $file_dir = $folder_temp . '/' . $dateFolder . '/' . $_sid . '/org/';
                    $file_dirDef = $folder_temp . '/' . $dateFolder . '/' . $_sid . '/def/';
                    $img_fileRoot = $file_dirRoot . $_thumbImage;
                    $img_file = $file_dir . $_thumbImage;
                    $img_fileDef = $file_dirDef . $_thumbImage;

                    // Load

                    $fi = @copy($img_fileDef, $img_file);

                    switch ($ext) {
                        case 'jpg':
                        case 'jpeg': {
                            $sourceRoot = imagecreatefromjpeg($img_fileRoot);
                            $source = imagecreatefromjpeg($img_file);
                            $sourceDef = imagecreatefromjpeg($img_fileDef);
                            break;
                        }
                        case 'gif': {
                            $sourceRoot = imagecreatefromgif($img_fileRoot);
                            $source = imagecreatefromgif($img_file);
                            $sourceDef = imagecreatefromgif($img_fileDef);
                            break;
                        }
                        case 'png': {
                            $sourceRoot = imagecreatefrompng($img_fileRoot);
                            $source = imagecreatefrompng($img_file);
                            $sourceDef = imagecreatefrompng($img_fileDef);
                            break;
                        }
                        default: {
                            $sourceRoot = imagecreatefromjpeg($img_fileRoot);
                            $source = imagecreatefromjpeg($img_file);
                            $sourceDef = imagecreatefromjpeg($img_fileDef);
                            break;
                        }
                    }
                    // Rotate
                    $rotateRoot = imagerotate($sourceRoot, $degrees, 0);
                    $rotate = imagerotate($source, $degrees, 0);
                    $rotateDef = imagerotate($sourceDef, $degrees, 0);


                    // Output
                    switch ($ext) {
                        case 'jpg':
                        case 'jpeg': {

                            @imagejpeg($rotateRoot, $img_fileRoot);
                            @imagejpeg($rotate, $img_file);
                            @imagejpeg($rotateDef, $img_fileDef);
                            break;
                        }
                        case 'gif': {
                            @imagegif($rotateRoot, $img_fileRoot);
                            @imagegif($rotate, $img_file);
                            @imagegif($rotateDef, $img_fileDef);
                            break;
                        }
                        case 'png': {
                            @imagepng($rotateRoot, $img_fileRoot);
                            @imagepng($rotate, $img_file);
                            @imagepng($rotateDef, $img_fileDef);
                            break;
                        }
                        default: {
                            @imagejpeg($rotateRoot, $img_fileRoot);
                            @imagejpeg($rotate, $img_file);
                            @imagejpeg($rotateDef, $img_fileDef);
                            break;
                        }
                    }
                    $wtrmrk_file = "./img/watermark.png";
                    $this->ak_img_watermark($img_fileDef, $wtrmrk_file, $img_file);

                    // Free the memory
                    imagedestroy($sourceRoot);
                    imagedestroy($rotateRoot);
                    imagedestroy($source);
                    imagedestroy($rotate);
                    imagedestroy($sourceDef);
                    imagedestroy($rotateDef);
                }


                $res = "rotated";
            }

        }

        return $res;
    }


    private function ak_img_watermark($target, $wtrmrk_file, $newcopy)
    {
        $watermark = imagecreatefrompng($wtrmrk_file);
        imagealphablending($watermark, false);
        imagesavealpha($watermark, true);

        $ext = $this->utils->GetFileExtension($target);

        switch ($ext) {
            case 'jpg':
            case 'jpeg': {
                $img = imagecreatefromjpeg($target);
                break;
            }
            case 'gif': {
                $img = imagecreatefromgif($target);
                break;
            }
            case 'png': {
                $img = imagecreatefrompng($target);
                break;
            }
            default: {
                $img = imagecreatefromjpeg($target);
                break;
            }
        }

        $img_w = imagesx($img);
        $img_h = imagesy($img);
        $wtrmrk_w = imagesx($watermark);
        $wtrmrk_h = imagesy($watermark);


        #if we need to add a watermark
        #find out what type of image the watermark is
        $info = getimagesize($wtrmrk_file);
        $imgtype = image_type_to_mime_type($info[2]);

        #assuming the mime type is correct
        switch ($imgtype) {
            case 'image/jpeg':
                $watermark = imagecreatefromjpeg($wtrmrk_file);
                break;
            case 'image/gif':
                $watermark = imagecreatefromgif($wtrmrk_file);
                break;
            case 'image/png':
                $watermark = imagecreatefrompng($wtrmrk_file);
                break;
            default:
                die('Invalid watermark type.');
        }

        #if we're adding a watermark, figure out the size of the watermark
        #and then place the watermark image on the bottom right of the image
        $wm_w = imagesx($watermark);
        $wm_h = imagesy($watermark);


        $image_thumb_width = $img_w;
        $calcP = array(1280, $image_thumb_width);
        $calc_p = $this->utils->calc_perc($calcP['1'], $calcP['0']);
        $image_thumb_height = round((500 / $calc_p) * 100, 0);
        //image_thumb_height
        $wm_w = $image_thumb_height;
        $aaa = '';
        $aaa .= "wm_w=" . $wm_w;

        $calcP = array(imagesx($watermark), $wm_w);
        $calc_p = $this->utils->calc_perc($calcP['1'], $calcP['0']);
        $image_thumb_height = round(($wm_h / $calc_p) * 100, 0);
        $wm_h = $image_thumb_height;
        $aaa .= "wm_h=" . $image_thumb_height;


        $thumb = imagecreatetruecolor($wm_w, $wm_h);
        //keeps the transparency of the picture
        imagealphablending($thumb, false);
        imagesavealpha($thumb, true);
        imagecopyresampled($thumb, $watermark, 0, 0, 0, 0, $wm_w, $wm_h, imagesx($watermark), imagesy($watermark));
        //imagecopyresized($thumb, $watermark, 0, 0, 0, 0, $wm_w, $wm_h, imagesx($watermark), imagesy($watermark));
        $dst_x = ($img_w / 2) - ($wm_w / 2); // For centering the watermark on any image
        $dst_y = ($img_h / 2) - ($wm_h / 2); // For centering the watermark on any image
        imagecopy($img, $thumb, $dst_x, $dst_y, 0, 0, $wm_w, $wm_h);


//		imagecopy($img, $watermark, $dst_x, $dst_y, 0, 0, $wtrmrk_w, $wtrmrk_h); 

        switch ($ext) {
            case 'jpg':
            case 'jpeg': {
                @imagejpeg($img, $newcopy, 100);
                break;
            }
            case 'gif': {
                @imagegif($img, $newcopy);
                break;
            }
            case 'png': {
                @imagepng($img, $newcopy, 9);
                break;
            }
            default: {
                @imagejpeg($img, $newcopy, 100);
                break;
            }
        }


        imagedestroy($img);
        imagedestroy($watermark);
    }




/*1) Open base.class.php and add this code*/

    /**
     * Include image resize class file.
     */
/*require_once iFolded.'m/classes/imageResize.class.php';*/

    /**
     * Include image upload class file.
     */
/*require_once iFolded.'m/classes/imageUpload.class.php';


----------------------------------------------

Example:

$newName = $filename;
$name = $newName.".".strtolower($ext);

$als = new Upload();
$als->File = $file;
$als->method = 1;
$als->SavePath = $file_dir;
$als->NewWidth = $this->Org_Width;
$als->NewHeight = $this->Org_Height;
$als->NewName = $name;
$als->OverWrite = true;
$err = $als->UploadFile();*/


// Resize

/*    public $FileName;
    public $FileSize;
    public $FileType;
    public $newWidth = 100;
    public $newHeight = 100;
    public $TmpName;
    public $PicDir;
    private $MaxFileSize = 26214400;
    private $AllowedExtentions = array("image/png", "image/gif", "image/jpeg", "image/pjpeg", "image/jpg", "image/x-png");
    private $ImageQuality = 85;
    private $ImageQualityPng = 3;*/

    /**
     * Image::Image()
     *
     * @param mixed $FileName
     * @return
     */
    /*function __construct($FileName)
    {
        $this->FileName = $FileName;
    }*/

    /**
     * Image::GetFileExtention()
     *
     * @param mixed $FileName
     * @return
     */
    function GetFileExtention($FileName)
    {
        if ($this->AllowedExtentions) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Image::ExistFile()
     *
     * @return
     */
    function ExistFile()
    {
        $fileexist = $_SERVER['DOCUMENT_ROOT'] . dirname($_SERVER['PHP_SELF']) . '/' . $this->PicDir . $this->FileName;
        if (file_exists($fileexist)) {
            return true;
        }
    }

    /**
     * Image::GetError()
     *
     * @param mixed $error
     * @return
     */
    function GetError($error)
    {
        switch ($error) {
            case 0:
                echo "Error: Invalid file type <strong>$this->FileType</strong>! Allowed type: .jpg, .jpeg, .gif, .png <strong>$this->FileName</strong><br />";
                break;

            case 1:
                echo "Error: File <strong>$this->FileSize</strong> is too large!<br />";
                break;

            case 2:
                echo "Error: Please, select a file for uploading!<br>";
                break;

            case 3:
                echo "Error: File <strong>$this->FileName</strong> already exist!<br />";
                break;
        }
    }

    /**
     * Image::Resize()
     *
     * @return
     */
    function Resize($method)
    {
        if (empty($this->TmpName)) {
            echo $this->GetError(2);
        } elseif ($this->FileSize > $this->MaxFileSize) {
            echo $this->GetError(1);
        } elseif ($this->GetFileExtention($this->FileName) == false) {
            echo $this->GetError(0);
        } elseif ($this->ExistFile()) {
            echo $this->GetError(3);
        } else {
            $ext = explode(".", $this->FileName);
            $ext = end($ext);
            $ext = strtolower($ext);

            // Get new sizes
            list($width_orig, $height_orig) = getimagesize($this->TmpName);
            $ratio_orig = $width_orig / $height_orig;

            if ($method == 1) {
                if ($this->newWidth && !$this->newHeight) {
                    $this->newHeight = floor($height_orig * ($this->newWidth / $width_orig));
                } elseif ($this->newHeight && !$this->newWidth)
                    $this->newWidth = floor($width_orig * ($this->newHeight / $height_orig));
            } else {
                if ($this->newWidth / $this->newHeight > $ratio_orig) {
                    $this->newWidth = $this->newHeight * $ratio_orig;
                } else
                    $this->newHeight = $this->newWidth / $ratio_orig;
            }

            $normal = imagecreatetruecolor($this->newWidth, $this->newHeight);
            switch ($ext) {
                case "jpg":
                    $source = imagecreatefromjpeg($this->TmpName);
                    break;
                case "gif":
                    $source = imagecreatefromgif($this->TmpName);
                    break;
                case "png":
                    $source = imagecreatefrompng($this->TmpName);
                    break;
            }

            $white = imagecolorallocate($normal, 255, 255, 255);
            imagefill($normal, 0, 0, $white);

            if ($method == 1) {
                $origin_x = 0;
                $origin_y = 0;

                $src_x = $src_y = 0;
                $src_w = $width_orig;
                $src_h = $height_orig;

                $cmp_x = $width_orig / $this->newWidth;
                $cmp_y = $height_orig / $this->newHeight;

                if ($cmp_x > $cmp_y) {
                    $src_w = round($width_orig / $cmp_x * $cmp_y);
                    $src_x = round(($width_orig - ($width_orig / $cmp_x * $cmp_y)) / 2);
                } elseif ($cmp_y > $cmp_x) {
                    $src_h = round($height_orig / $cmp_y * $cmp_x);
                    $src_y = round(($height_orig - ($height_orig / $cmp_y * $cmp_x)) / 2);
                }

                imagecopyresampled($normal, $source, $origin_x, $origin_y, $src_x, $src_y, $this->newWidth, $this->newHeight, $src_w, $src_h);
            } else {
                imagecopyresampled($normal, $source, 0, 0, 0, 0, $this->newWidth, $this->newHeight, $width_orig, $height_orig);
            }
            imagecolortransparent($normal, $white);

            switch ($ext) {
                case "jpg":
                    imagejpeg($normal, "$this->PicDir/$this->FileName", "$this->ImageQuality");
                    break;
                case "gif":
                    imagegif($normal, "$this->PicDir/$this->FileName", "$this->ImageQuality");
                    break;
                case "png":
                    imagepng($normal, "$this->PicDir/$this->FileName", "$this->ImageQualityPng");
                    break;
            }

            imagedestroy($source);
        }
    }

    /**
     * Image::Save()
     *
     * @return
     */
    function Save()
    {
        if (empty($this->TmpName)) {
            echo $this->GetError(2);
        } elseif ($this->FileSize > $this->MaxFileSize) {
            echo $this->GetError(1);
        } elseif ($this->GetFileExtention($this->FileName) == false) {
            echo $this->GetError(0);
        } elseif ($this->ExistFile()) {
            echo $this->GetError(3);
        } else {
            copy($this->TmpName, $this->PicDir . $this->FileName);
        }
    }



}


/******************* image.class.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** image.class.php ******************/;
