<?php
/******************* database.class.mysqli.php *******************
 *
 * MySQLi Database class
 *
 * @author MicroPHP <info@microphp.com>
 * @web www.microphp.com
 * @copyright 2004-2019 MicroPHP
 *
 ******************** database.class.mysqli.php ******************/

/** 
 * Define Namespace 
 */
namespace mcms5xx\classes;

/**
 * Checking if class included normally.
 */
if (!defined('mCMScheck')) {
    die('mCMS by MicroPHP');
}

/**
 * MySQL Database class with MySQLi extension
 */
class Database
{
    /**
     * @var string Database name
     */
    public $db_name;

    /**
     * @var string Database user
     */
    public $db_user;

    /**
     * @var string Database password
     */
    public $db_pass;

    /**
     * @var string Database host
     */
    public $db_host;

    /**
     * @var database prefix from configuration
     */
    public $prefix;
    public $db_prefix;
	
    /**
     * @var database connection status
     */
    public $link;

    /**
     * Database Class constructor.
     */
    public function __construct($db_name = "mcms_db", $db_user = "root", $db_pass = "p@ssw0rd", $db_host = "localhost", $db_prefix = "m_", $test_mode = false)
    {
        /**
         * @var string Database name
         */
        $this->db_name = $db_name;

        /**
         * @var string Database user
         */
        $this->db_user = $db_user;

        /**
         * @var string Database password
         */
        $this->db_pass = $db_pass;

        /**
         * @var string Database host
         */
        $this->db_host = $db_host;

        /*
         * Set database prefix
         */
        $this->prefix = $this->db_prefix = $db_prefix;

        /*
         * Connect to database
         */
        $this->link = @mysqli_connect($this->db_host, $this->db_user, $this->db_pass, $this->db_name);
        if ( (!$this->link) && (!$test_mode) ) {
            die('Could not connect: ' . mysqli_error($this->link));
        }

        if ($this->link) {
            /*
             * Set UTF8
             */
            //$this->query("SET NAMES 'utf8';");
			mysqli_set_charset($this->link, 'utf8');
        }
    }

    /**
     * MySQL Query.
     *
     * @param string $sql
     */
    public function query($sql)
    {
        $result = mysqli_query($this->link, $sql);

        return $result;
    }
    
    /**
     * MySQL Multi Query.
     *
     * @param string $sql
     */
    public function multi_query($sql)
    {
        $result = mysqli_multi_query($this->link, $sql);

        return $result;
    }

    /**
     * MySQL num rows from result.
     *
     * @param $result
     */
    public function num_result_rows($result)
    {
        $numrows = mysqli_num_rows($result);

        return $numrows;
    }

    /**
     * MySQL num rows.
     *
     * @param string $sql
     */
    public function num_rows($sql)
    {
        $result = $this->query($sql);
        $numrows = $this->num_result_rows($result);

        return $numrows;
    }

    /**
     * Escapes special characters in a string for use in an MySQL statement
     *
     * @param string $txt
     */
    public function quote($txt)
    {
        $txt = mysqli_real_escape_string($this->link, $txt);

        return $txt;
    }

    /**
     * MySQL Find next Auto_increment ID.
     *
     * @param string $table - Table name
     */
    public function find_id($table)
    {
        $statusSQL = $this->query('SHOW TABLE STATUS FROM ' . $this->db_name . " LIKE '" . $table . "' ");
        $statusResult = $this->fetch($statusSQL);

        return $statusResult['Auto_increment'];
    }

    /**
     * MySQL Find inserted Auto_increment ID.
     */
    public function insert_id()
    {
        return (int)mysqli_insert_id($this->link);
    }

    /**
     * MySQL fetch array.
     *
     * @param $result
     */
    public function fetch($result)
    {
        return mysqli_fetch_array($result);
    }

    /**
     * MySQL fetch assoc.
     *
     * @param $result
     */
    public function fetch_assoc($result)
    {
        return @mysqli_fetch_assoc($result);
    }

    /**
     * Database::fetch_all()
     * Returns all the results
     * @param mixed $sql
     * @return assoc array
     *
     * $sql = "SELECT * from ".$this->db->prefix."table WHERE id='".$_id."' ";
     * $row = $this->db->fetch_all($sql);
     */
    public function fetch_all($sql)
    {
        $query_id = $this->query($sql);
        $record = array();
        while ($row = $this->fetch($query_id, $sql)) {
            $record[] = $row;
        }

        return $record;
    }

    /**
     * MySQL limit.
     *
     * @param integer $start
     * @param integer $end
     */
    public function get_limit($start, $end)
    {
        return ' LIMIT ' . $start . ',' . $end . ' ';
    }

    /**
     * Database::insert()
     * Insert query with an array
     * @param mixed $table
     * @param mixed $data
     * @return id of inserted record, false if error
     *
     * Sample:
     * $dataInsert = array();
     * $dataInsert['price'] = 1;
     * $dataInsert['name'] = 'test';
     * $dataInsert['template'] = 'test.tpl';
     * $dataInsert['isdefault'] = 1;
     * $dataInsert['active'] = 1;
     * $insert_id = $this->db->insert($this->db->prefix.'currency' , $dataInsert);
     */
    public function insert($table = null, $data)
    {

        global $core;
        if ($table === null or empty($data) or !is_array($data)) {
            die("Invalid array for table: <b>".$table."</b>.");
            return false;
        }
        $q = "INSERT INTO `" . $table . "` ";
        $v = '';
        $k = '';

        foreach ($data as $key => $val) :
            $k .= "`$key`, ";
            if (strtolower($val) == 'null')
                $v .= "NULL, ";
            elseif (strtolower($val) == 'now()')
                $v .= "NOW(), ";
            elseif (strtolower($val) == 'tzdate')
                $v .= "DATE_ADD(NOW(),INTERVAL " . date_default_timezone_set('GMT') . " HOUR), ";
            else
                $v .= "'" . $this->escape($val) . "', ";
        endforeach;

        $q .= "(" . rtrim($k, ', ') . ") VALUES (" . rtrim($v, ', ') . ");";
		
        if ($this->query($q)) {
            return $this->insert_id();
        } else
            return false;
    }

    /**
     * Database::update()
     * Update query with an array
     * @param mixed $table
     * @param mixed $data
     * @param string $where
     * @return query_id
     *
     * Sample:
     * $dataUpdate = array();
     * $dataUpdate['price'] = 100;
     * $dataUpdate['active'] = 0;
     * $this->db->update($this->db->prefix.'currency' , $dataUpdate, " cid=".$insert_id."");
     */
    public function update($table = null, $data, $where = '1')
    {
        global $core;
        if ($table === null or empty($data) or !is_array($data)) {
            die("Invalid array for table: <b>" . $table . "</b>.");
            return false;
        }

        $q = "UPDATE `" . $table . "` SET ";
        foreach ($data as $key => $val) :
            if (strtolower($val) == 'null')
                $q .= "`$key` = NULL, ";
            elseif (strtolower($val) == 'now()')
                $q .= "`$key` = NOW(), ";
            elseif (strtolower($val) == 'tzdate')
                $q .= "`$key` = DATE_ADD(NOW(),INTERVAL " . date_default_timezone_set('GMT') . " HOUR), ";
            elseif (strtolower($val) == 'default()')
                $q .= "`$key` = DEFAULT($val), ";
            elseif(preg_match("/^inc\((\-?\d+)\)$/i",$val,$m))
                $q.= "`$key` = `$key` + $m[1], ";
            else
                $q .= "`$key`='" . $this->escape($val) . "', ";
        endforeach;
        $q = rtrim($q, ', ') . ' WHERE ' . $where . ';';
        return $this->query($q);
    }


    /**
     * Database::escape()
     * @param mixed $string
     * @return
     */
    public function escape($string)
    {
        if (is_array($string)) {
            foreach ($string as $key => $value) :
                $string[$key] = $this->quote($value);
            endforeach;
        } else
            $string = $this->quote($string);

        return $string;
    }

    /**
     * Database::delete()
     * Delete records
     * @param mixed $table
     * @param string $where
	 * $this->db->delete($this->db->prefix . 'questionscategorylocalizations', " catid='".$id."'");
     * @return
     */
    public function delete($table, $where = '')
    {
        $q = !$where ? 'DELETE FROM ' . $table : 'DELETE FROM ' . $table . ' WHERE ' . $where;
        return $this->query($q);
    }
}

/******************* database.class.mysqli.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** database.class.mysqli.php ******************/;
