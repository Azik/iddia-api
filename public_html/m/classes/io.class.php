<?php
/******************* io.class.php *******************
 *
 * Input Output class
 *
 * @author MicroPHP <info@microphp.com>
 * @web www.microphp.com
 * @copyright 2004-2019 MicroPHP
 *
 ******************** io.class.php ******************/

/** 
 * Define Namespace 
 */
namespace mcms5xx\classes;

/**
 * Checking if class included normally.
 */
if (!defined('mCMScheck')) {
    die('mCMS by MicroPHP');
}

/**
 * Input Output class
 */
class IO
{
    /**
     * Start
     */
    public function __construct()
    {
    }

    /**
     * return sub folders in array format
     *
     * @param string $dir
     */
    public function GetSubFolders($dir)
    {
        $folders = array();

        if (is_dir($dir)) {
            if ($handle = opendir($dir)) {
                while (($file = readdir($handle)) !== false) {
                    if ($file != '.' && $file != '..') {
                        if (is_dir($dir.'/'.$file)) {
                            $folders[] = $file;
                        }
                    }
                }
                closedir($handle);
            }
        }

        return $folders;
    }

    /**
     * get list of files
     *
     * @param string $dir
     */
    public function GetFiles($dir)
    {
        $files = array();

        if (is_dir($dir)) {
            if ($handle = opendir($dir)) {
                while (($file = readdir($handle)) !== false) {
                    if (is_file($dir.'/'.$file)) {
                        $files[] = $file;
                    }
                }
                closedir($handle);
            }
        }

        return $files;
    }

    /**
     * Get contents of a file into a string.
     *
     * @param string $file
     */
    public function GetFileContent($file)
    {
        $content = '';
        $handle = @fopen($file, 'r');
        if ($handle) {
            if (@filesize($file) > 0) {
                $content = fread($handle, filesize($file));
                fclose($handle);
            }
        }

        if (strlen($content) == 0) {
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $file);
            curl_setopt($curl, CURLOPT_FAILONERROR, 1);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
            $content = curl_exec($curl);
            curl_close($curl);
        }

        return $content;
    }

    /**
     * Save content to file.
     *
     * @param string $file
     * @param string $content
     */
    public function SaveToFile($file, $content)
    {
        if (is_writable($file)) {
            if ($handle = @fopen($file, 'w')) {
                $success = fwrite($handle, $content);
                fclose($handle);

                return $success;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Create file and append content to file.
     *
     * @param string $file
     * @param string $content
     */
    public function createFile($file, $content)
    {
        if ($handle = @fopen($file, 'w')) {
            $success = fwrite($handle, $content);
            fclose($handle);

            return $success;
        } else {
            return false;
        }
    }

    /**
     * Save content to end of file.
     *
     * @param string $file
     * @param string $content
     */
    public function AppendToFile($file, $content)
    {
        if (is_writable($file)) {
            if ($handle = @fopen($file, 'a')) {
                $success = fwrite($handle, $content);
                fclose($handle);

                return $success;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Check if folder exists.
     *
     * @param string $dir
     */
    public function dir_exists($dir)
    {
        $exists = false;

        if (is_dir($dir)) {
            $exists = true;
        }

        return $exists;
    }

    /**
     * Check if file exists.
     *
     * @param $file_nm
     */
    public function file_exists($file_nm)
    {
        $exists = false;

        if ((@is_file($file_nm)) && (@filesize($file_nm) > 5)) {
            $exists = true;
        }

        return $exists;
    }

    /**
     * Create sub directories.
     *
     * @param string $path
     */
    public function CreateDirTree($path)
    {
        $success = true;
        if (!$this->dir_exists($path)) {
            $dir = '';
            $parts = explode('/', $path);
            for ($i = 0; $i < count($parts); ++$i) {
                if ($i == 0) {
                    $dir = $parts[$i];
                } else {
                    $dir .= '/'.$parts[$i];
                }
                if (!$this->dir_exists($dir)) {
                    $sc = @mkdir($dir, 0777);
                    if (!$sc) {
                        $success = false;
                        break;
                    }
                }
            }
        }

        return $success;
    }

    /**
     * Create sub directories for given date (integer format) YYYY/MM/DD.
     *
     * @param string $folder
     * @param integer $date - datetime in unixtime format
     */
    public function dateFolder($folder, $date = 0)
    {
        $date = ($date > 0) ? $date : time();
        if (!is_dir($folder)) {
            @mkdir($folder);
            @chmod($folder, 0777);
        }
        $folder .= date('Y', $date);
        if (!is_dir($folder)) {
            @mkdir($folder);
            @chmod($folder, 0777);
        }

        $folder .= '/'.date('m', $date);
        if (!is_dir($folder)) {
            @mkdir($folder);
            @chmod($folder, 0777);
        }

        $folder .= '/'.date('d', $date);
        if (!is_dir($folder)) {
            @mkdir($folder);
            @chmod($folder, 0777);
        }

        return $folder;
    }

    /**
     * Copy files from $source folder to $dest folder.
     *
     * @param string $source
     * @param string $dest
     * @param boolean $overwrite
     */
    public function CopyFolder($source, $dest, $overwrite)
    {
        $success = true;
        if (!$this->dir_exists($source)) {
            return false;
        }
        $sc = $this->CreateDirTree($dest);
        if (!$sc) {
            return false;
        }

        $files = $this->GetFiles($source);
        foreach ($files as $file) {
            if (file_exists($dest.'/'.$file) && !$overwrite) {
                continue;
            }

            if (file_exists($dest.'/'.$file)) {
                $sc = @unlink($dest.'/'.$file);
                if (!$sc) {
                    return false;
                }
            }

            $this->copyFile($source.'/'.$file, $dest.'/'.$file);
        }

        $folders = $this->GetSubFolders($source);
        foreach ($folders as $folder) {
            $sc = $this->CopyFolder($source.'/'.$folder, $dest.'/'.$folder, $overwrite);
            if (!$sc) {
                return false;
            }
        }

        return $success;
    }

    /**
     * Copy file from $source folder to $dest.
     *
     * @param string $source
     * @param string $dest
     */
    public function copyFile($source, $dest)
    {
        $sc = @copy($source, $dest);
        if ($sc) {
            @chmod($dest, 0777);
        }

        return $sc;
    }

    /**
     * Delete files from $path folder, if $delete_self then delete self folder also.
     *
     * @param string $path
     * @param boolean $delete_self
     */
    public function DeleteDirTree($path, $delete_self)
    {
        $success = true;
        if ($this->dir_exists($path)) {
            $files = $this->GetFiles($path);
            foreach ($files as $file) {
                $sc = $this->deleteFile($path.'/'.$file);
                if (!$sc) {
                    return false;
                }
            }

            $folders = $this->GetSubFolders($path);
            foreach ($folders as $folder) {
                $sc = $this->DeleteDirTree($path.'/'.$folder, true);
                if (!$sc) {
                    return false;
                }
            }

            if ($delete_self) {
                $sc = rmdir($path);
                if (!$sc) {
                    return false;
                }
            }
        }

        return $success;
    }

    /**
     * Delete file
     *
     * @param sting $file_nm - Filename for delete
     */
    public function deleteFile($file_nm)
    {
        return @unlink($file_nm);
    }

    /**
     * Copy entire contents of a directory to another
     *
     * @param string $src
     * @param $dst
     */
    public function recurseCopy($src, $dst)
    {
        $dir = opendir($src);
        @mkdir($dst);
        while (false !== ($file = readdir($dir))) {
            if (($file != '.') && ($file != '..')) {
                if (is_dir($src.'/'.$file)) {
                    recurse_copy($src.'/'.$file, $dst.'/'.$file);
                } else {
                    copy($src.'/'.$file, $dst.'/'.$file);
                }
            }
        }
        closedir($dir);
    }

    /**
     * Rename File or Folder
     * @param $oldName
     * @param $nameName
     * @param $type - file, folder
     */
    public function renameFileFolder($oldName, $newName, $type="folder") {
        if ($type=="file") {
            rename($oldName, $newName);
        } else {
            rename($oldName, $newName);
        }
    }

}

/******************* io.class.php *******************
*
* Copyright : (C) 2004 - 2019. All Rights Reserved
*
******************** io.class.php ******************/;
