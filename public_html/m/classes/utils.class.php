<?php
/******************* utils.class.php *******************
 *
 * Image Resize and Upload class
 *
 * @author MicroPHP <info@microphp.com>
 * @web www.microphp.com
 * @copyright 2004-2019 MicroPHP
 *
 ******************** utils.class.php ******************/

/** 
 * Define Namespace 
 */
namespace mcms5xx\classes;

/**
 * Checking if class included normally.
 */
if (!defined('mCMScheck')) {
    die('mCMS by MicroPHP');
}

/**
 * Utilites class
 */
class Utils
{
    /**
     * Input Output class
     */
    public $io;

    /**
     * Database class
     */
    private $db;

    /**
     * @var Configuration class
     */
    public $cfg;

    /**
     * Utils Class constructor.
     *
     * @param $io - Input Output class
     * @param $db - Database class
     */
    public function __construct($io, $db)
    {
        $this->io = $io;
        $this->db = $db;
        $this->cfg = new \mcms5xx\mcmsConfig();
    }

    /**
     * POST Request operations
     *
     * @param $name
     */
    public function simplePost($name)
    {
        //$txt = array_key_exists($name_field, $_POST) ? $_POST[$name_field] : "";
        $txt = array_key_exists($name, $GLOBALS['_POST']) ? $GLOBALS['_POST'][$name] : '';

        return $txt;
    }

    /**
     * POST Request operations
     *
     * @param $name
     */
    public function Post($name)
    {
        $txt = array_key_exists($name, $GLOBALS['_POST']) ? $GLOBALS['_POST'][$name] : null;

        return $this->txt_request_filter($txt);
    }

    /**
     * user POST Request operations
     *
     * @param $name
     */
    public function UserPost($name)
    {
        $txt = array_key_exists($name, $GLOBALS['_POST']) ? $GLOBALS['_POST'][$name] : null;

        return $this->txt_request_filter_user($txt);
    }
	
    /**
     * user POST Request operations with CKeditor
     *
     * @param $name
     */
    public function UserPostCKeditor($name)
    {
        $txt = array_key_exists($name, $GLOBALS['_POST']) ? $GLOBALS['_POST'][$name] : null;

        return $this->txt_request_filter_user($txt, '<p><h1><h2><h3><h4><strong><i><a><ul><ol><li><blockquote><figure><table><tbody><tr><td><oembed>');
    }

	public function RequestHeaderPost() {
		
		header("Access-Control-Allow-Origin: *");
		header("Content-Type: application/json; charset=UTF-8");
		header("Access-Control-Allow-Methods: POST");
		header("Access-Control-Max-Age: 3600");
		header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
		
	}

	public function RequestHeaderGet() {
		
		header("Access-Control-Allow-Origin: *");
		header("Content-Type: application/json; charset=UTF-8");
		header("Access-Control-Allow-Methods: POST");
		header("Access-Control-Max-Age: 3600");
		header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
		
	}

	
	public function getAuthorizationHeader(){
        $headers = null;
        if (isset($_SERVER['Authorization'])) {
            $headers = trim($_SERVER["Authorization"]);
        }
        else if (isset($_SERVER['HTTP_AUTHORIZATION'])) { //Nginx or fast CGI
            $headers = trim($_SERVER["HTTP_AUTHORIZATION"]);
        } elseif (function_exists('apache_request_headers')) {
            $requestHeaders = apache_request_headers();
            // Server-side fix for bug in old Android versions (a nice side-effect of this fix means we don't care about capitalization for Authorization)
            $requestHeaders = array_combine(array_map('ucwords', array_keys($requestHeaders)), array_values($requestHeaders));
            //print_r($requestHeaders);
            if (isset($requestHeaders['Authorization'])) {
                $headers = trim($requestHeaders['Authorization']);
            }
        }
        return $headers;
    }
	
	/**
	 * get access token from header
	 * */
	public function getBearerToken() {
		$headers = $this->getAuthorizationHeader();
		// HEADER: Get the access token from the header
		if (!empty($headers)) {
			if (preg_match('/Bearer\s(\S+)/', $headers, $matches)) {
				return $matches[1];
			}
		}
		return null;
	}


    public function time_elapsed_string($datetime, $full = false) {
        $now = new \DateTime;
        $ago = new \DateTime(date("m/d/Y h:i:s",$datetime));
        $diff = $now->diff($ago);
    
        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;
    
        $string = array(
            'y' => 'il',
            'm' => 'ay',
            'w' => 'həftə',
            'd' => 'gün',
            'h' => 'saat',
            'i' => 'dəqiqə',
            's' => 'saniyə',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? '' : '');
            } else {
                unset($string[$k]);
            }
        }
    
        if (!$full) $string = array_slice($string, 0, 1);
        return $string ? implode(', ', $string) . ' əvvəl' : 'indi';
    }
	
    public function require_auth() {
        $authConfig = $this->getFromConfig('apiAuth')['mobile'];
        $AUTH_USER = $authConfig['user'];
        $AUTH_PASS = $authConfig['pass'];
        header('Cache-Control: no-cache, must-revalidate, max-age=0');
        $has_supplied_credentials = !(empty($_SERVER['PHP_AUTH_USER']) && empty($_SERVER['PHP_AUTH_PW']));
        $is_not_authenticated = (
            !$has_supplied_credentials ||
            $_SERVER['PHP_AUTH_USER'] != $AUTH_USER ||
            $_SERVER['PHP_AUTH_PW']   != $AUTH_PASS
        );
        if ($is_not_authenticated) {
            header('HTTP/1.1 401 Authorization Required');
            header('WWW-Authenticate: Basic realm="Access denied"');
            exit;
        }
    }

    /**
     * user POST Registration operations
     *
     * @param $name
     */
    public function userPostReg($name)
    {
        $txt = array_key_exists($name, $GLOBALS['_POST']) ? $GLOBALS['_POST'][$name] : null;
        $txt = $this->txt_request_filter_user($txt);
        $txt = $this->txt_request_filter_reg($txt);

        return $txt;
    }

    /**
     * POST request filter for admin panel
     *
     * @param $name
     */
    public function UserTextPost($name)
    {
        $txt = array_key_exists($name, $GLOBALS['_POST']) ? $GLOBALS['_POST'][$name] : null;

        return $this->text_editor_request_filter($txt);
    }

    /**
     * POST request Array integer
     *
     * @param $name
     * @param $coma_sep
     */
    public function UserPostIntArr($name, $coma_sep = ',')
    {
        $txtTmpArr = array_key_exists($name, $GLOBALS['_POST']) ? (is_array($GLOBALS['_POST'][$name]) ? $GLOBALS['_POST'][$name] : array()) : array();
        $txtArr = array();
        for ($i = 0; $i < count($txtTmpArr); ++$i) {
            $txtTmpArr[$i] = $this->filterInt($txtTmpArr[$i]);
            if ($txtTmpArr[$i] > 0) {
                $txtArr[] = $txtTmpArr[$i];
            }
        }

        return $txtArr;
    }

    /**
     * POST request Array
     *
     * @param $name
     * @param $coma_sep
     */
    public function UserPostArr($name, $coma_sep = ',')
    {
        $txtTmpArr = array_key_exists($name, $GLOBALS['_POST']) ? (is_array($GLOBALS['_POST'][$name]) ? $GLOBALS['_POST'][$name] : array()) : array();
        $txtArr = array();
        for ($i = 0; $i < count($txtTmpArr); ++$i) {
            $txtTmpArr[$i] = $this->txt_request_filter_user($txtTmpArr[$i]);
            if (strlen($txtTmpArr[$i]) > 0) {
                $txtArr[] = $txtTmpArr[$i];
            }
        }

        return $txtArr;
    }

    /**
     * POST search filter for site
     *
     * @param $name
     */
    public function UserSearchPost($name)
    {
        $txt = array_key_exists($name, $GLOBALS['_POST']) ? $GLOBALS['_POST'][$name] : null;
        $txt = $this->txt_request_filter_user($txt);
        $txt = $this->txt_search_filter_user($txt);

        return $txt;
    }

    /**
     * UserPostInt Method
     * POST request
     * Return numeric result or zero.
     *
     * @param $name
     */
    public function UserPostInt($name = '')
    {
        return (array_key_exists($name, $GLOBALS['_POST'])) ? $this->filterInt($GLOBALS['_POST'][$name]) : 0;
    }

    /**
     * GET Request operations
     *
     * @param $name
     */
    public function Get($name)
    {
        $txt = array_key_exists($name, $GLOBALS['_GET']) ? $GLOBALS['_GET'][$name] : null;

        return $this->txt_request_filter($txt);
    }

    /**
     * GET Request user operations
     *
     * @param $name
     */
    public function UserGet($name = '')
    {
        $txt = array_key_exists($name, $GLOBALS['_GET']) ? $GLOBALS['_GET'][$name] : null;

        return $this->txt_request_filter_user($txt);
    }

    /**
     * UserGetInt Method
     * GET request
     * Return numeric result or zero.
     *
     * @param $name - GET Field name
     */
    public function UserGetInt($name = '')
    {
        return (array_key_exists($name, $GLOBALS['_GET'])) ? $this->filterInt($GLOBALS['_GET'][$name]) : 0;
    }

    /**
     * Filter full
     *
     * @param string $txt
     */
    public function dataFullFilter($txt)
    {
        $txt = $this->txt_request_filter_user($txt);
        $txt = $this->txt_search_filter_user($txt);

        return $txt;
    }

    /**
     * filterInt Method
     * Filter field to numeric format.
     *
     * @param $name - Field name
     */
    public function filterInt($name)
    {
        if (!is_array($name)) {
            $fld = (int)$name;
            $fld = $fld * 1;
            $fld = intval($fld);
        } else {
            $fld = 0;
        }
        return round($fld, 0);
    }

    /**
     * Find folder
     *
     * @param $script_name
     */
    public function findFolder($script_name, $from_admin=0)
    {
        $scriptArr = explode('/', $script_name);
        $fldr = '/';
		$minus = ($from_admin==1) ? 2 : 1;
        for ($i = 1; $i < count($scriptArr) - $minus; ++$i) {
            $fldr .= $scriptArr[$i] . '/';
        }

        return $fldr;
    }

    /**
     * filter for dispplay
     *
     * @param $txt
     */
    public function ekranFilter($txt)
    {
        $txt = addslashes($txt);
        $txt = stripslashes($txt);
        $txt = str_replace('<span style="font-size: small;">', '', $txt);
        $txt = str_replace('</span>', '', $txt);
        $txt = str_replace('<p>', '', $txt);
        $txt = str_replace('</p>', '', $txt);
        $txt = str_replace('"', '&quot;', $txt);

        return $txt;
    }

    /**
     * calculate percent
     *
     * @param $found_num
     * @param $give_num
     */
    public function calc_perc($found_num, $give_num)
    {
        return round(($give_num / ($found_num / 100)), 4);
    }

    /**
     * File operations
     *
     * @param $name
     * @param $type
     */
    public function File($name, $type)
    {
        global $_FILES;

        return @$_FILES[$name][$type];
    }

    /**
     * get File name
     *
     * @param sting $dir
     * @param sting $filename
     */
    public function GetFileName($dir, $filename)
    {
        $filename = $this->filename_filter($filename);
        $fullname = $dir . $filename;
        $m = 0;
        while (file_exists($fullname)) {
            ++$m;

            if ($m > 1) {
                $filename = substr_replace($filename, $m, 0, strlen($m));
            } else {
                $filename = $m . '_' . $filename;
            }

            $fullname = $dir . $filename;
        }

        return strtolower($filename);
    }

    /**
     * Filename filter
     *
     * @param string $url
     */
    public function filename_filter($url)
    {
        $url = $this->txt_request_filter_user($url);
        $url = $this->requestNormalize($url);

        return $url;
    }

    /**
     * get file Extension
     *
     * @param $filename
     */
    public function GetFileExtension($filename)
    {
        $ext = strrchr($filename, '.');
        $ext = substr($ext, 1);
        $ext = strtolower($ext);

        return $ext;
    }

    /**
     * get file Size
     *
     * @param $file
     */
    public function GetFileSize($file)
    {
        $sz = filesize($file);
        $sz = ceil($sz / 1024);

        return $sz;
    }

    public function Download($file)
    {
        $success = true;
        if (file_exists($file)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octetstream');
            header('Content-Disposition: attachment; filename=' . basename($file));
            readfile($file);
        } else {
            $success = false;
        }

        return $success;
    }

    /**
     * array_random Method
     * Randomise array elements
     * return array.
     *
     * @param $arr
     */
    public function array_random($arr)
    {
        shuffle($arr);

        $r = array();
        for ($i = 0; $i < count($arr); ++$i) {
            $r[] = $arr[$i];
        }

        return $r;
    }

    public function GetFromLangCommon($module, $admin_or_view, $key, $sub1 = '', $sub2 = '')
    {
        $value = '';
        if ($key != '') {
            $value = @$GLOBALS['language'][$module][$admin_or_view][$key];
        }

        if ($sub1 != '') {
            $value = str_replace('{0}', $sub1, $value);
        }
        if ($sub2 != '') {
            $value = str_replace('{1}', $sub2, $value);
        }

        return $value;
    }

    public function getFromConfig($key)
    {
        $value = null;
        if (property_exists($this->cfg, $key)) {
            $value = $this->cfg->$key;
        }

        return $value;
    }

    public function Redirect($url = '')
    {
        if ($url != '') {
            @header('location: ' . $url);
            $script_url = '<script type="text/javascript"> window.location.href="' . $url . '"; </script>';
            echo $script_url;
        }
        exit();
    }

    /**
     * COOKIE Request operations
     *
     * @param $key
     * @param $value
     */
    public function setCookie($key, $value)
    {
        setcookie($key, $value);
    }

    public function getCookie($key)
    {
        global $_COOKIE;

        return @$_COOKIE[$key];
    }

    /** SESSION Request operations */
    public function GetSession($key)
    {
        @session_start();

        global $_SESSION;
        $value = '';
        if (array_key_exists($key, $_SESSION)) {
            $value = $_SESSION[$key];
        }

        return $value;
    }

    public function GetSessionInt($key)
    {
        @session_start();

        global $_SESSION;
        $value = 0;
        if (array_key_exists($key, $_SESSION)) {
            $value = $_SESSION[$key];
        }

        return $this->filterInt($value);
    }

    public function RemoveSession($key)
    {
        @session_start();
        global $_SESSION;
        unset($_SESSION[$key]);
    }

    public function SetSession($key, $value)
    {
        @session_start();
        global $_SESSION;
        $_SESSION[$key] = $value;
    }

    public function SetSessionArray($key_val_array)
    {
        @session_start();
        global $_SESSION;
        foreach ($key_val_array as $key => $value) {
            $_SESSION[$key] = $value;
        }
    }

    /**
     * String operations
     *
     * @param integer $length - Length of generated string
     */
    public function generateString($length)
    {
        $string = '';
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        for ($i = 0; $i < $length; ++$i) {
            $string .= substr($chars, mt_rand(0, strlen($chars) - 1), 1);
        }

        return $string;
    }

    /**
     * Generate random numeric number
     *
     * @param integer $length - Length of generated string
     */
    public function generateNumeric($length)
    {
        $chars = '123456789';
        $string = substr($chars, mt_rand(0, strlen($chars) - 1), 1);
        $chars = '0123456789';
        for ($i = 1; $i < $length; $i++) {
            $string .= substr($chars, mt_rand(0, strlen($chars) - 1), 1);
        }
        return $string;
    }

    /**
     * uppercase text
     *
     * @param $text
     */
    public function ToUpper($text)
    {
        $text = strip_tags($text);
        $Uppers = array('Q', 'Ü', 'E', 'R', 'T', 'Y', 'U', 'İ', 'O', 'P', 'Ö', 'Ğ', 'A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', 'I', 'Ə', 'Z', 'X', 'C', 'V', 'B', 'N', 'M', 'Ç', 'Ş', 'W', 'Й', 'Ц', 'У', 'К', 'Е', 'Н', 'Г', 'Ш', 'Щ', 'З', 'Х', 'Ъ', 'Ф', 'Ы', 'В', 'А', 'П', 'Р', 'О', 'Л', 'Д', 'Ж', 'Э', 'Я', 'Ч', 'С', 'М', 'И', 'Т', 'Ь', 'Б', 'Ю', 'Ё');
        $Lowers = array('q', 'ü', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', 'ö', 'ğ', 'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'ı', 'ə', 'z', 'x', 'c', 'v', 'b', 'n', 'm', 'ç', 'ş', 'w', 'й', 'ц', 'у', 'к', 'е', 'н', 'г', 'ш', 'щ', 'з', 'х', 'ъ', 'ф', 'ы', 'в', 'а', 'п', 'р', 'о', 'л', 'д', 'ж', 'э', 'я', 'ч', 'с', 'м', 'и', 'т', 'ь', 'б', 'ю', 'ё');
        $text = str_replace($Lowers, $Uppers, $text);

        return $text;
    }


    /**
     * Lowercase text
     *
     * @param $text
     */
    public function ToLower($text)
    {
        $text = strip_tags($text);
        $Uppers = array('Q', 'Ü', 'E', 'R', 'T', 'Y', 'U', 'İ', 'O', 'P', 'Ö', 'Ğ', 'A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', 'I', 'Ə', 'Z', 'X', 'C', 'V', 'B', 'N', 'M', 'Ç', 'Ş', 'W', 'Й', 'Ц', 'У', 'К', 'Е', 'Н', 'Г', 'Ш', 'Щ', 'З', 'Х', 'Ъ', 'Ф', 'Ы', 'В', 'А', 'П', 'Р', 'О', 'Л', 'Д', 'Ж', 'Э', 'Я', 'Ч', 'С', 'М', 'И', 'Т', 'Ь', 'Б', 'Ю', 'Ё');
        $Lowers = array('q', 'ü', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', 'ö', 'ğ', 'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'ı', 'ə', 'z', 'x', 'c', 'v', 'b', 'n', 'm', 'ç', 'ş', 'w', 'й', 'ц', 'у', 'к', 'е', 'н', 'г', 'ш', 'щ', 'з', 'х', 'ъ', 'ф', 'ы', 'в', 'а', 'п', 'р', 'о', 'л', 'д', 'ж', 'э', 'я', 'ч', 'с', 'м', 'и', 'т', 'ь', 'б', 'ю', 'ё');
        $text = str_replace($Uppers, $Lowers, $text);

        return $text;
    }

    /**
     * Filter text
     *
     * @param string $text
     */
    public function txt_filter($text)
    {
        $text = strip_tags($text);
        $text = stripslashes($text);
        $text = addslashes($text);
        $text = trim($text);

        return $text;
    }

    public function txt_request_filter($text)
    {
        if (!is_array($text)) {
            if (@strlen($text) > 0) {
                //$text = stripslashes($text);
                //$text = addslashes($text);
                $text = trim($text);
                $text = $this->db->quote($text);
            }

            return $text;
        } else {
            return '';
        }
    }

    /**
     * User filter text
     *
     * @param $text
     */
    public function txt_request_filter_user($text, $disable_tags='')
    {
        if (!is_array($text)) {
            if (strlen($text) > 0) {
                $text = (strlen($disable_tags)>0) ? strip_tags($text, $disable_tags) : strip_tags($text);
                $text = stripslashes($text);
                $text = addslashes($text);
                $text = trim($text);
                //$rep_arr = array('select', 'union', 'drop', 'delete', 'update', 'fread');
                /* $rep_arr = array('–', 'union', 'drop', 'delete', 'update', 'fread');
                $text = str_replace($rep_arr, '', $text); */
                $rep_arr = array('–');
                $text = str_replace($rep_arr, '', $text);

                return $text;
            } else {
                return '';
            }
        } else {
            return '';
        }
    }

    /**
     * User registration text filter
     *
     * @param $text
     */
    public function txt_request_filter_reg($text)
    {
        $text = preg_replace('/[[:^print:]]/', '', $text);

        return $text;
    }

    /**
     * User editor text filter
     *
     * @param $text
     */
    public function text_editor_request_filter($text)
    {
        if (!is_array($text)) {
            if (strlen($text) > 0) {
                $text = stripslashes($text);
                $text = addslashes($text);
                $text = trim($text);
                /* $rep_arr = array('select', 'union', 'drop', 'delete', 'update', 'fread');
                $text = str_replace($rep_arr, '', $text); */
                return $text;
            } else {
                return '';
            }
        } else {
            return '';
        }
    }

    /**
     * Search text filter
     *
     * @param $text
     */
    public function txt_search_filter_user($text)
    {
        $rep_arr = array('?', '!', ',', '(', ')', '/', '\\', '', '	', "\n", "\r", "\t", '~', '$', '♛', '✦', '&', '#', ';', '@', '%', '`', '^', '*', '+', '=', '[', ']', '{', '}', '<', '>', '&quot;', '“', '”', ':', "'", '"', '’', '‘');
        $text = str_replace($rep_arr, '', $text);
        $text = str_replace('  ', ' ', $text);
        $text = trim($text);

        return $text;
    }

    /**
     * Request normalize
     *
     * @param string $url
     */
    public function requestNormalize($url)
    {
        $rep_arr = array('"', "'", '’', '‘');
        $url = str_replace($rep_arr, '', $url);
        $url = str_replace(' ', '_', $url);
        $url = str_replace('---', '-', $url);
        $url = str_replace('--', '-', $url);
        $url = str_replace('"', '&quot;', $url);
        $url = str_replace('#', '', $url);
        $url = str_replace('%', '', $url);
        $url = str_replace('İ', 'I', $url);
        $url = str_replace('ə', 'e', $url);
        $url = str_replace('ü', 'u', $url);
        $url = str_replace('ö', 'o', $url);
        $url = str_replace('ğ', 'g', $url);
        $url = str_replace('ç', 'c', $url);
        $url = str_replace('ş', 's', $url);
        $url = str_replace('ı', 'i', $url);
        $url = str_replace('Ə', 'E', $url);
        $url = str_replace('Ü', 'U', $url);
        $url = str_replace('Ö', 'O', $url);
        $url = str_replace('Ğ', 'G', $url);
        $url = str_replace('Ç', 'C', $url);
        $url = str_replace('Ş', 'S', $url);
        $url = str_replace('"', '', $url);
        $url = str_replace("'", '', $url);
        $url = str_replace(':', '', $url);
        $url = str_replace('“', '', $url);
        $url = str_replace('”', '', $url);
        $url = str_replace('&quot;', '', $url);
        $url = str_replace('?', '', $url);
        $url = str_replace('!', '', $url);

        $url = str_replace('а', 'a', $url);
        $url = str_replace('б', 'b', $url);
        $url = str_replace('в', 'v', $url);
        $url = str_replace('г', 'g', $url);
        $url = str_replace('д', 'd', $url);
        $url = str_replace('е', 'e', $url);
        $url = str_replace('ё', 'jo', $url);
        $url = str_replace('ж', 'zh', $url);
        $url = str_replace('з', 'z', $url);
        $url = str_replace('и', 'i', $url);
        $url = str_replace('й', 'j', $url);
        $url = str_replace('к', 'k', $url);
        $url = str_replace('л', 'l', $url);
        $url = str_replace('м', 'm', $url);
        $url = str_replace('н', 'n', $url);
        $url = str_replace('о', 'o', $url);
        $url = str_replace('п', 'p', $url);
        $url = str_replace('р', 'r', $url);
        $url = str_replace('с', 's', $url);
        $url = str_replace('т', 't', $url);
        $url = str_replace('у', 'u', $url);
        $url = str_replace('ф', 'f', $url);
        $url = str_replace('х', 'x', $url);
        $url = str_replace('ц', 'c', $url);
        $url = str_replace('ч', 'ch', $url);
        $url = str_replace('ш', 'sh', $url);
        $url = str_replace('щ', 'w', $url);
        $url = str_replace('ъ', '', $url);
        $url = str_replace('ы', 'y', $url);
        $url = str_replace('ь', '', $url);
        $url = str_replace('э', 'je', $url);
        $url = str_replace('ю', 'ju', $url);
        $url = str_replace('я', 'ya', $url);

        $url = str_replace('А', 'A', $url);
        $url = str_replace('Б', 'B', $url);
        $url = str_replace('В', 'V', $url);
        $url = str_replace('Г', 'G', $url);
        $url = str_replace('Д', 'D', $url);
        $url = str_replace('Е', 'E', $url);
        $url = str_replace('Ё', 'Jo', $url);
        $url = str_replace('Ж', 'Zh', $url);
        $url = str_replace('З', 'Z', $url);
        $url = str_replace('И', 'I', $url);
        $url = str_replace('Й', 'J', $url);
        $url = str_replace('К', 'K', $url);
        $url = str_replace('Л', 'L', $url);
        $url = str_replace('М', 'M', $url);
        $url = str_replace('Н', 'N', $url);
        $url = str_replace('О', 'O', $url);
        $url = str_replace('П', 'P', $url);
        $url = str_replace('Р', 'R', $url);
        $url = str_replace('С', 'S', $url);
        $url = str_replace('Т', 'T', $url);
        $url = str_replace('У', 'U', $url);
        $url = str_replace('Ф', 'F', $url);
        $url = str_replace('Х', 'H', $url);
        $url = str_replace('Ц', 'C', $url);
        $url = str_replace('Ч', 'Ch', $url);
        $url = str_replace('Ш', 'Sh', $url);
        $url = str_replace('Щ', 'W', $url);
        $url = str_replace('Ъ', '', $url);
        $url = str_replace('Ы', 'Y', $url);
        $url = str_replace('Ь', '', $url);
        $url = str_replace('Э', 'Je', $url);
        $url = str_replace('Ю', 'Ju', $url);
        $url = str_replace('Я', 'Ja', $url);

        $url = str_replace('Ñ', 'N', $url);
        $url = str_replace('ñ', 'n', $url);
        $url = str_replace('è', 'e', $url);
        $url = str_replace('é', 'e', $url);
        $url = str_replace('ê', 'e', $url);
        $url = str_replace('ë', 'e', $url);
        $url = str_replace('È', 'E', $url);
        $url = str_replace('É', 'E', $url);
        $url = str_replace('Ê', 'E', $url);
        $url = str_replace('Ë', 'E', $url);

        $rep_arr = array('?', '!', ',', '(', ')', '/', '', '	', "\n", "\r", "\t", '~', '$', '♛', '✦', '&', '#', ';', '@', '%', '`', '^', '*', '+', '=', '[', ']', '{', '}', '<', '>', '’', '‘', '€');
        $url = str_replace($rep_arr, '', $url);
        $url = trim($url);
        $url = str_replace(' ', '-', $url);
        $url = str_replace('----', '-', $url);
        $url = str_replace('---', '-', $url);
        $url = str_replace('--', '-', $url);

        return $url;
    }

    /**
     * Request filter
     *
     * @param string $text
     */
    public function txt_request_filter_without_trim($text)
    {
        $text = stripslashes($text);
        $text = addslashes($text);

        return $text;
    }

    /**
     * Search filter
     *
     * @param string $text
     */
    public function txt_search_filter($text)
    {
        $text = strip_tags($text);
        $text = stripslashes($text);
        $text = addslashes($text);
        $text = trim($text);

        return $text;
    }

    /**
     * Display filter
     *
     * @param string $txt
     */
    public function txt_ekranFilter($txt)
    {
        $txt = strip_tags($txt);
        $txt = addslashes($txt);
        $txt = stripslashes($txt);
        $txt = str_replace('"', '&quot;', $txt);

        return $txt;
    }

    /**
     * Display filter BR
     *
     * @param string $txt
     */
    public function txt_ekranFilterBr($txt)
    {
        $txt = str_replace('<br>', "\n", $txt);
        $txt = str_replace('<br/>', "\n", $txt);
        $txt = str_replace('<br />', "\n", $txt);
        $txt = strip_tags($txt);
        $txt = addslashes($txt);
        $txt = stripslashes($txt);
        $txt = str_replace('"', '&quot;', $txt);
        $txt = str_replace("\n", '<br/>', $txt);

        return $txt;
    }

    public function q_txt($text)
    {
        $short_h_txt = '';
        if (@strlen($text) > 0) {
            $text = strip_tags($text);
            $text = stripslashes($text);
            $text = addslashes($text);
            $gallery_code = $this->getFromConfig('gallery_code');
            $pattern = '/' . str_replace('[id]', "(\d+)", $gallery_code) . '/i';
            preg_match($pattern, $text, $matches);
            $text = preg_replace($pattern, '', $text);
            $short_h = explode(' ', $text);
            $ii = (count($short_h) > 30) ? 30 : count($short_h);
            for ($i = 0; $i < $ii; ++$i) {
                $short_h_txt .= $short_h[$i] . ' ';
            }
        }

        return stripslashes($short_h_txt);
    }

    public function GetPartOfString($source, $symbolCount)
    {
        $source = trim(strip_tags($source));
        $part = '';

        if (strlen($source) > $symbolCount) {
            $part = substr($source, 0, $symbolCount);
            $part .= '...';
        } else {
            $part = $source;
        }

        return $part;
    }

    public function GetPartOfText($source, $symbolCount)
    {
        $source = trim(strip_tags($source));
        $part = '';

        if (strlen($source) > $symbolCount) {
            $sourceArr = explode(' ', $source);
            $si = 0;
            /*while (strlen($part) < $symbolCount) {
                $part .= $sourceArr[$si].' ';
                ++$si;
            }
            */
            do {
                $part .= $sourceArr[$si].' ';
                ++$si;
            } while (strlen($part.$sourceArr[$si]."...") < $symbolCount);
            $part .= '...';
        } else {
            $part = $source;
        }

        return $part;
    }

    public function seperateTextParts($source, $partCount)
    {
        $source = trim(strip_tags($source));
        $part = '';
        $sourceArr = explode(' ', $source);
        if (count($sourceArr) > $partCount) {
            $si = 0;
            while ($si < $partCount) {
                $part .= $sourceArr[$si] . ' ';
                ++$si;
            }
            $part .= '...';
        } else {
            $part = $source;
        }

        return $part;
    }

    /**
     * get Requested ID
     */
    public function GetIP()
    {
        $det_ip = (getenv('HTTP_X_FORWARDED_FOR')) ? getenv('HTTP_X_FORWARDED_FOR') : getenv('REMOTE_ADDR');

        return $this->txt_request_filter_user($det_ip);
    }

    /**
     * get Requested User Agent
     */
    public function GetComp()
    {
        $comp = $this->txt_request_filter_user(@$_SERVER['HTTP_USER_AGENT']);

        return $comp;
    }

    public function removeQueryString($url, $qs)
    {
        return preg_replace('/(\&|\?)' . $qs . '=\d+/i', '', $url);
    }

    public function formattedDate($date)
    {
        if ($date < 1) {
            return '';
        }
        //$formated_date = date("d.m.Y H:i",$date);
        //$formated_date = date("F, d, Y",$date);
        //$formated_date = date("d F Y",$date);
        $formated_date = date('F d, Y H:i', $date);

        return $formated_date;
    }

    public function url_filter($url)
    {
        $url = $this->txt_request_filter_user($url);
        $url = $this->requestNormalize($url);
		$url = str_replace(' ', '', $url);
        $url = str_replace('.', '', $url);
        $url = str_replace('_', '-', $url);
        $url = str_replace('----', '-', $url);
        $url = str_replace('---', '-', $url);
        $url = str_replace('--', '-', $url);

        return strtolower($url);
    }

    /**
     * Slug filter
     */
    public function slugFilter($url)
    {
        $url = $this->txt_request_filter_user($url);
        $url = $this->requestNormalize($url);
        $url = str_replace('.', '', $url);
        $url = str_replace('_', '-', $url);
        $url = str_replace('----', '-', $url);
        $url = str_replace('---', '-', $url);
        $url = str_replace('--', '-', $url);
        $url = str_replace('\\', '', $url);

        return strtolower($url);
    }

    /**
     * form Data
     *
     * @param $txt
     */
    public function formData($txt)
    {
        $txt = htmlspecialchars($txt);

        return $txt;
    }

    /**
     * sanitize()
     *
     * @param mixed $string
     * @param bool $trim
     * @param bool $int
     * @param bool $str
     * @return
     *
     * Sample $this->utils->sanitize($row['address'],15);
     */
    public function sanitize($string, $trim = false, $int = false, $str = false)
    {
        $string = filter_var($string, FILTER_SANITIZE_STRING);
        $string = trim($string);
        $string = stripslashes($string);
        $string = strip_tags($string);
        $string = str_replace(array('‘', '’', '“', '”'), array("'", "'", '"', '"'), $string);

        if ($trim)
            $string = substr($string, 0, $trim);

        if ($int)
            $string = preg_replace("/[^0-9\s]/", "", $string);

        if ($str)
            $string = preg_replace("/[^a-zA-Z\s]/", "", $string);
        return $string;
    }

	
	public function retriveData($service_url, $customrequest = "GET")
    {
        $curl = curl_init($service_url);
        $headr = array();
        $headr[] = 'Content-Length: 0';
        $headr[] = 'Content-Type: application/json';
        //$headr[] = 'X-Authorization: ' . $this->api_key;
        curl_setopt($curl, CURLOPT_URL, $service_url);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $customrequest);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headr);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            die('error occured during curl exec. Additioanl info: ' . var_export($info));
            curl_close($curl);
        }
        curl_close($curl);

        $curl_responseArr = json_decode($curl_response, true);
        return $curl_responseArr;
    }
	
}

/******************* utils.class.class.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** utils.class.class.php ******************/;
