<?php
/******************* news_more.view.php *******************
 *
 * News Inside view module
 *
 * @author MicroPHP <info@microphp.com>
 * @web www.microphp.com
 * @copyright 2004-2019 MicroPHP
 *
 ******************** news_more.view.php ******************/

/** 
 * Define Namespace 
 */
namespace mcms5xx\modules;

/**
 * Checking if class included normally.
 */
if (!defined('mCMScheck')) {
    die('mCMS by MicroPHP');
}

class News extends \mcms5xx\classes\ViewPage
{
    public function __construct()
    {
        parent::__construct();
        $this->onLoad();
    }

    private function onLoad()
    {
        $this->buildPage();
        $this->newsCats();
    }

    private function buildPage()
    {
        $this->buildMenu();
        $this->get_nav(0);
        $id = $this->utils->UserGetInt('id');

        $this->template->assign_block_vars('where.for', array(
            'NAME' => $this->fromLangIndex('news_archive'),
            'SPACE' => $this->fromLangIndex('where_space'),
            'URL' => $this->curr_folder.str_replace('[lang]', $this->lang, $this->permalinks[$this->perma_type]['module_news'][$this->curr_lang]),
        ));
        $query = 'SELECT N.*, NL.name, NL.header, NL.text, NL.lang, CL.name as cat_name, NL.slug FROM
		' .$this->db->prefix.'news N
		INNER JOIN ' .$this->db->prefix.'newslocalizations NL ON NL.newsid = N.newsid
		INNER JOIN ' .$this->db->prefix."newscategorylocalizations CL on (CL.catid = N.catid) && (CL.lang='".$this->lang."')
		WHERE (N.active='1') && (NL.lang='" .$this->lang."') && (N.newsid = '".$id."')
		";
        $result = $this->db->query($query);
        $row = $this->db->fetch($result);
        if (strlen($row['name']) > 2) {
            $name = $row['name'];
            $cat_name = $row['cat_name'];
            $cat_id = $row['catid'];
            $img_id = $this->utils->filterInt($row['header']);
            $this->template->assign_var('HEADER', $name);
            $news_id = $row['newsid'];
            $this->template->assign_block_vars('where.for', array(
                'NAME' => $cat_name,
                'SPACE' => $this->fromLangIndex('where_space'),
                'URL' => $this->curr_folder.str_replace('[lang]', $this->lang, str_replace('[catid]', $cat_id, $this->permalinks[$this->perma_type]['news_cat'][$this->curr_lang])),
            ));

            $news_url = $this->curr_folder.str_replace('[slug]', $this->utils->url_filter($row['slug']), str_replace('[year]', date('Y', $row['newsdate']), str_replace('[name]', $this->utils->url_filter($row['name']), str_replace('[id]', $news_id, str_replace('[lang]', $this->lang, $this->permalinks[$this->perma_type]['news'][$this->curr_lang])))));
            $this->template->assign_block_vars('where.end', array(
                'NAME' => $name,
                'URL' => $news_url,
            ));
            $text = $this->site->f_tags($row['text']);
            $text = $this->site->replaceImageTemplate($text, $this->curr_folder);
            $text = $this->contentUsibility($text);
            $text = str_replace('[alt]', $name, $text);

            $name = $row['name'];

            $this->template->assign_var('TEXT', $text);
            $this->template->assign_var('NAME', $name);
            $this->template->assign_var('TITLE', $name);

            $news_date_format = $this->fromConfig('news_date_format');
            $full_url = $this->curr_domain.$news_url;
            $this->template->assign_vars(array(
                'IMGID' => $img_id,
                'DATE' => date($news_date_format, $row['newsdate']),
                'DATE_DD' => date('d', $row['newsdate']),
                'DATE_MM' => date('m', $row['newsdate']),
                'DATE_MONTH' => date('M', $row['newsdate']),
                'DATE_YYYY' => date('Y', $row['newsdate']),
                'FULL_URL' => $full_url,
                'SOCIAL_URL' => urlencode($full_url),
            ));

            if ($img_id > 0) {
                $this->template->assign_block_vars('news_img', array(
                    'IMGID' => $img_id,
                ));
            }

            $this->up_view($row['newsid']);

            $site_url = $this->fromConfig('site_url');
            //$original_url = $this->utils->url_filter($site_url.'n'.$row['newsid'].'/'.$name);
            $original_url = $this->curr_folder.str_replace('[slug]', $this->utils->url_filter($row['slug']), str_replace('[year]', date('Y', $row['newsdate']), str_replace('[name]', $this->utils->url_filter($row['name']), str_replace('[id]', $news_id, str_replace('[lang]', $this->lang, $this->permalinks[$this->perma_type]['news'][$this->curr_lang])))));
            $short_url = $original_url;
            $this->template->assign_var('SHARE_URL', $short_url);
            $this->template->assign_var('ORIGINAL_URL', urlencode($short_url));
        } else {
            $this->template->assign_var('HEADER', $this->fromLangIndex('error'));
            $this->template->assign_var('TEXT', $this->fromLangIndex('error_news'));
        }
    }

    private function newsCats()
    {
        $news_block_seperator = $this->fromConfig('news_block_seperator');
        $cat_query = 'SELECT N.*, NL.name FROM '.$this->db->prefix.'newscategories N
		INNER JOIN ' .$this->db->prefix."newscategorylocalizations NL ON NL.catid = N.catid
		WHERE (N.active = 1) && (NL.lang='" .$this->lang."')
		ORDER BY N.position ASC
		";
        $cat_idx = 0;
        $cat_nums = $this->db->num_rows($cat_query);
        $cat_result = $this->db->query($cat_query);
        while ($cat_row = $this->db->fetch($cat_result)) {
            ++$cat_idx;
            $cat_id = $cat_row['catid'];
            $class = ($cat_idx == $cat_nums) ? ' class="nb"' : '';
            $this->template->assign_block_vars('news_cats', array(
                'IDX' => $cat_idx,
                'CAT_ID' => $cat_id,
                'NAME' => $cat_row['name'],
                'CAT_URL' => $this->curr_folder.str_replace('[lang]', $this->lang, str_replace('[catid]', $cat_id, $this->permalinks[$this->perma_type]['news_cat'][$this->curr_lang])),
                'CLASS' => $class,
            ));
        }
    }

    private function up_view($news_id)
    {
        $this->template->assign_var('TOTAL_VIEWS', $this->fromLang('total_views'));

        $view_query = 'SELECT N.* FROM '.$this->db->prefix."news_read N
			WHERE (N.newsid = '" .$news_id."') ";
        if ($this->db->num_rows($view_query) == 0) {
            $vadd_query = 'INSERT INTO '.$this->db->prefix."news_read(`newsid`, `read_date` ) VALUES('".$news_id."','".time()."' )";
            $this->db->query($vadd_query);
            $this->template->assign_var('TOTAL_VIEWS_COUNT', 1);
        } else {
            $view_result = $this->db->query($view_query);
            $view_row = $this->db->fetch($view_result);

            $this->template->assign_var('TOTAL_VIEWS_COUNT', $view_row['total_views']);

            if ($view_row['read_date'] != time()) {
                $CurrMonth = strtotime('01 '.date('F').' '.date('Y').'');
                $month_views = ((date('n', $view_row['read_date']) != date('n')) || (date('Y', $view_row['read_date']) != date('Y'))) ? " `month_views` = '1'" : ' `month_views` = `month_views`+1';

                $vup_query = 'UPDATE '.$this->db->prefix."news_read SET `read_date`='".time()."', `total_views`=`total_views`+1, ".$month_views." WHERE `newsid`='".$news_id."'";
                $this->db->query($vup_query);
            }
        }
    }
}

$news = new News();
$news->template->pparse('news_more');

/******************* news_more.view.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** news_more.view.php ******************/;
