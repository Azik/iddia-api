<?php
/******************* unsubscribe.cnfg.php *******************
 *
 * @author MicroPHP <info@microphp.com>
 * @web www.microphp.com
 * @copyright 2004-2019 MicroPHP
 *
 ******************** unsubscribe.cnfg.php ******************/

/** 
 * Define Namespace 
 */
namespace mcms5xx\modules;

class unsubscribe_cls extends \mcms5xx\classes\Base
{
    public $permalinks = array(
        'page_slug' => array(
            'unsubscribe' => array('unsubscribe/[slug]', '[lang]/unsubscribe/[slug]'),
            'unsubscribe_error' => array('unsubscribe_sehv', '[lang]/unsubscribe_error'),
            'unsubscribe_ok' => array('unsubscribe_ughur', '[lang]/unsubscribe_ok'),
        ),
    );

    public function __construct()
    {
        parent::__construct(false);
		
        if (array_key_exists('mfd', $_GET)) {
            //print_r($this);exit();
            //print_r($this->utils);exit();
            $m = $this->utils->UserGet('mfd');
            $matches = array();
            $module_tp = '';
            foreach ($this->permalinks['page_slug'] as $module_tp => $ruleArr) {
                $ruleArr['0'] = str_replace('/', '\/', $ruleArr['0']);
                $ruleArr['0'] = str_replace('.', '\.', $ruleArr['0']);
                $ruleArr['0'] = str_replace('[year]', '[0-9]{4}', $ruleArr['0']);
                $ruleArr['0'] = str_replace('[lang]', '[a-z]{2}', $ruleArr['0']);
                $ruleArr['0'] = str_replace('[page]', '[0-9]{1,}', $ruleArr['0']);
                $ruleArr['0'] = str_replace('[id]', '[0-9]{1,}', $ruleArr['0']);
                $ruleArr['0'] = str_replace('[subid]', '[0-9]{1,}', $ruleArr['0']);
                $ruleArr['0'] = str_replace('[catid]', '[0-9]{1,}', $ruleArr['0']);
                $ruleArr['0'] = str_replace('[slug]', '(.*)', $ruleArr['0']);
                $ruleArr['0'] = str_replace('html', '(html)', $ruleArr['0']);
                $ruleArr['0'] = str_replace('index', '(index)', $ruleArr['0']);
                $ruleArr['0'] = '#^'.$ruleArr['0'].'$#i';

                $ruleArr['1'] = str_replace('/', '\/', $ruleArr['1']);
                $ruleArr['1'] = str_replace('.', '\.', $ruleArr['1']);
                $ruleArr['1'] = str_replace('[year]', '[0-9]{4}', $ruleArr['1']);
                $ruleArr['1'] = str_replace('[lang]', '[a-z]{2}', $ruleArr['1']);
                $ruleArr['1'] = str_replace('[page]', '[0-9]{1,}', $ruleArr['1']);
                $ruleArr['1'] = str_replace('[id]', '[0-9]{1,}', $ruleArr['1']);
                $ruleArr['1'] = str_replace('[subid]', '[0-9]{1,}', $ruleArr['1']);
                $ruleArr['1'] = str_replace('[catid]', '[0-9]{1,}', $ruleArr['1']);
                $ruleArr['1'] = str_replace('[slug]', '(.*)', $ruleArr['1']);
                $ruleArr['1'] = str_replace('html', '(html)', $ruleArr['1']);
                $ruleArr['1'] = str_replace('index', '(index)', $ruleArr['1']);
                $ruleArr['1'] = '#^'.$ruleArr['1'].'$#i';
                /* echo $ruleArr['1'].' == '.$m.'<br>'; */
                if (preg_match($ruleArr['0'], $m, $matches)) {
                    break;
                }
                if (preg_match($ruleArr['1'], $m, $matches)) {
                    break;
                }
            }
            if (count(@$matches) > 0) {
                $lang = explode('/', @$matches['0']);
                $slug = (strlen($lang[count($lang) - 1]) == 0) ? $lang[count($lang) - 2] : $lang[count($lang) - 1];
                $this->langs = $this->site->getShowLangs();
                $_GET['lang'] = $this->site->getDefaultLang();
                if (in_array($lang['0'], $this->langs)) {
                    $_GET['lang'] = $lang['0'];
                }
                switch ($module_tp) {
                    case 'unsubscribe': {
                        $_GET[$this->module_view_qs] = 'unsubscribe';
                        $_GET['tp'] = '';
                        $_GET['slug'] = '';
                        $slugArr = array();
                        for ($l = 0; $l < count($lang); ++$l) {
                            if (strlen($lang[$l]) > 0) {
                                if (!in_array($lang[$l], $this->langs)) {
                                    $slugArr[] = $lang[$l];
                                }
                            }
                        }
                        //print_r($slugArr);exit();
                        if (count($slugArr) == 2) {
                            $cat_zero_query = 'SELECT * FROM `'.$this->db->prefix."subscribe` WHERE (SHA1(concat(`sid`, `add_date`, `email`)) = '".$this->utils->url_filter($slugArr['1'])."') ";
                            $cat_zero_result = $this->db->query($cat_zero_query);
                            if ($cat_zero_row = $this->db->fetch($cat_zero_result)) {
                                //print_r($slugArr);exit();
                                $_GET['slug'] = $slugArr['1'];
                            } else {
                                $_GET['tp'] = 'error';
                            }
                        } else {
                            $_GET['tp'] = 'error';
                        }
                        break;
                    }
                    case 'unsubscribe_error': {
                        $_GET[$this->module_view_qs] = 'unsubscribe';
                        $_GET['tp'] = 'error';
                        break;
                    }
                    case 'unsubscribe_ok': {
                        $_GET[$this->module_view_qs] = 'unsubscribe';
                        $_GET['tp'] = 'ok';
                        break;
                    }
                }
            }
        }
    }
}

/******************* unsubscribe.cnfg.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** unsubscribe.cnfg.php ******************/;
