<?php
/******************* 404.view.php *******************
 *
 * 404 page view module
 *
 * @author MicroPHP <info@microphp.com>
 * @web www.microphp.com
 * @copyright 2004-2019 MicroPHP
 *
 ******************** 404.view.php ******************/

/** 
 * Define Namespace 
 */
namespace mcms5xx\modules;

/**
 * Checking if module included normally.
 */
if (!defined('mCMScheck')) {
    die('mCMS by MicroPHP');
}

class Main404 extends \mcms5xx\classes\ViewPage
{
    public function __construct()
    {
        parent::__construct();
        $this->buildMenu();
        $this->build404Title();
    }

    private function build404Title()
    {
        $toYear = '&mdash;' . date('Y');
        $this->template->assign_vars(array(
            'TITLE' => $this->getKeyLang('site_name', $this->lang, ''),
            'TITLE_UP' => $this->nameup($this->getKeyLang('site_name', $this->lang, '')),
            'TITLE_INSIDE2' => ' - ' . $this->getKeyLang('site_title', $this->lang, ''),
            'SLOGAN' => $this->getKeyLang('site_title', $this->lang, ''),
            'YEAR' => $toYear,
            'CURR_YEAR' => date('Y'),
            'COPYRIGHT' => '&copy; ' . $this->getKeyLang('meta_author', $this->lang, ''),
            'COPYRIGHT_INFO' => $this->fromLangIndex('copyright_info'),
            'SITE_COPYRIGHT' => str_replace('[YEAR]', date('Y'), $this->getKeyLang('site_copyright', $this->lang, '[YEAR]')),
            'CHARSET' => $this->getKey('charset', 'utf-8'),
            'AUTHOR' => $this->getKeyLang('meta_author', $this->lang, ''),
            'KEYWORDS' => $this->getKeyLang('meta_keywords', $this->lang, ''),
            'DESCRIPTION' => $this->getKeyLang('meta_description', $this->lang, ''),
            'META_EMAIL' => $this->getKeyLang('meta_email', $this->lang, ''),
            'LANG_QS' => $this->lang_qs,
            'MODULE_QS' => $this->module_qs,
            'CURR_LANG' => $this->lang,
        ));
        $title_description = '<meta name="description" content="' . $this->fromLangIndex('title_404') . '" />';
        $meta_description = $this->fromLangIndex('title_404');
        $this->template->assign_vars(array(
            'TITLE_INSIDE' => $this->fromLangIndex('title_404') . ' - ',
            'TITLE_KEYWORD' => $this->fromLangIndex('title_404') . ', ',
            'TITLE_DESCRIPTION' => $title_description,
            'META_DESCRIPTION' => $meta_description,
        ));
        $this->template->assign_block_vars('langs', array());
        foreach ($this->langs as $key => $value) {
            $link_lang = ($this->default_lang == $value) ? '0' : '1';
            $cur_class = ($this->lang == $value) ? ' class="current"' : '';
            $act_class = ($this->lang == $value) ? ' class="active"' : '';
            $act_class_nm = ($this->lang == $value) ? ' active' : '';
            $lang_act = ($this->lang == $value) ? ' lang_act' : '';
            $this->template->assign_block_vars('langs.list', array(
                'NAME' => $value,
                'UP_NAME' => strtoupper($value),
                'LANG_ACT' => $lang_act,
                'CUR_CLASS' => $cur_class,
                'ACT_CLASS' => $act_class,
                'ACT_CLASS_NM' => $act_class_nm,
                'URL' => $this->curr_folder . str_replace('[lang]', $value, $this->permalinks[$this->perma_type]['index'][$link_lang]),
                'IMG' => 'lang_' . $value . '.png',
            ));
        }
    }
}

$main = new Main404();
$main->template->pparse('404');

/******************* 404.view.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** 404.view.php ******************/;
