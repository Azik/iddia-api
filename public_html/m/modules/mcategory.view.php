<?php
/******************* mcategory.view.php *******************
 *
 * index, mcategory page view module
 *
 * @author MicroPHP <info@microphp.com>
 * @web www.microphp.com
 * @copyright 2004-2019 MicroPHP
 *
 ******************** mcategory.view.php ******************/

/** 
 * Define Namespace 
 */
namespace mcms5xx\modules;

/**
 * Checking if module included normally.
 */
if (!defined('mCMScheck')) {
    die('mCMS by MicroPHP');
}

class mcategoryView extends \mcms5xx\classes\ViewPage
{
    public function __construct()
    {
        parent::__construct();
        $this->onLoad();
    }

    private function onLoad()
    {
        $this->buildPage();
    }

    private function buildPage()
    {
		$catid = $this->utils->UserGetInt('catid');
		$level = $this->utils->GetSession('member_usertype');
		$subQuery = "";
		if($level == "") {
            /*$index_url = $this->curr_folder.str_replace('[lang]', $this->lang, $this->permalinks[$this->perma_type]['index'][$this->curr_lang]);
			@header('location: '.$index_url);
            $this->utils->Redirect($index_url);
			exit; */
		}
		else {
			$subQuery = " AND S.level='".$level."'";
		}
		
		$sql = 'SELECT 
            S.*, SL.name
        FROM '.$this->db->prefix.'questionscategories S
		INNER JOIN ' .$this->db->prefix."questionscategorylocalizations SL ON SL.catid = S.catid
		WHERE SL.lang='" .$this->lang."' && S.active = 1 && S.index = 1 ".$subQuery."
		&& S.catid='".(int)$catid."'";
        $result = $this->db->query($sql);
        if($row = $this->db->fetch($result)) {
			$this->buildPageInfo($row);
			$this->buildTests($catid);
			
		}
		
		

        $this->buildTitle();
        
        $this->buildMenu();
    }

    private function buildPageInfo($row)
    {
		$fileid = $row['image'];
		$img_file = $this->curr_folder.$this->site->getFile($fileid);
        $this->template->assign_vars(array(
            'IMG_FILE' => $img_file,
        ));
        
		//$this->categories();
    }



    private function buildTests($id)
    {
		$level = $this->utils->GetSession('member_usertype');
        $sql = 'SELECT 
            S.*, SL.name
        FROM '.$this->db->prefix.'tests S
		INNER JOIN ' .$this->db->prefix."testslocalizations SL ON SL.testid = S.testid
		WHERE SL.lang='" .$this->lang."' && S.active = 1 && S.index = 1
		&& S.catid='".(int)$id."'
		ORDER BY S.testdate ASC
		";
		
        /* echo($sql);	 */
        $result = $this->db->query($sql);
        $ndx = 0;
        $slide_nums = $this->db->num_rows($sql);
        while ($row = $this->db->fetch($result)) {
            
            $id = $row['testid'];
            $name = $row['name'];
            $fileid = $row['image'];
            $img_file = $this->curr_folder.$this->site->getFile($fileid);
            $img = '<img src="/'.$img_file.'" alt="" />';
			$url = $this->curr_folder . str_replace('[lang]', $this->lang, str_replace('[testid]', $id, $this->permalinks[$this->perma_type]['module_braintest'][$this->curr_lang]));
			if($level == "")
				$url = "";
			
            if ($ndx == 0) {
                $this->template->assign_block_vars('tests', array());
            }
			
			if ($ndx % 8 == 0) {
				$this->template->assign_block_vars('tests.row_8', array());
			}
			
			
			$this->template->assign_block_vars('tests.row_8.items', array(
					'ID' => $id,
					'NDX' => $ndx,
					'NAME' => $name,
					'NAME_UP' => $this->nameup($name),
					'TITLE' => $name,
					'IMG' => $img,
					'IMGID' => $fileid,
					'IMG_FILE' => $img_file,
					'URL' => $url,
					'FILEID' => $fileid,
				));
				
				
            $this->template->assign_block_vars('tests.items', array(
				'ID' => $id,
				'NDX' => $ndx,
				'NAME' => $name,
				'NAME_UP' => $this->nameup($name),
				'TITLE' => $name,
				'IMG' => $img,
				'IMGID' => $fileid,
				'IMG_FILE' => $img_file,
			    'URL' => $url,
				'FILEID' => $fileid,
            ));
            ++$ndx;
        }
    }
	
}

$mcategory = new mcategoryView();
$mcategory->template->pparse('mcategory');

/******************* mcategory.view.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** mcategory.view.php ******************/;
