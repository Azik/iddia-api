<?php
/******************* inside.view.php *******************
 *
 * Inside view module
 *
 * @author MicroPHP <info@microphp.com>
 * @web www.microphp.com
 * @copyright 2004-2019 MicroPHP
 *
 ******************** inside.view.php ******************/

/** 
 * Define Namespace 
 */
namespace mcms5xx\modules;

/**
 * Checking if module included normally.
 */
if (!defined('mCMScheck')) {
    die('mCMS by MicroPHP');
}

class insideView extends \mcms5xx\classes\ViewPage
{
    public $page_template = 'inside';

    public function __construct()
    {
        parent::__construct();
        $this->onLoad();
    }

    private function onLoad()
    {
        $this->buildPage();
    }

    private function buildPage()
    {
        $this->buildMenu();

        $id = $this->utils->UserGetInt('id');
        $query = "SELECT 
              M.*, 
              ML.`name`, 
              ML.`comment`,
              ML.`text`,
              ML.`header`,
              ML.`slug`,
              ML.`link` as lang_link
        FROM `" . $this->db->prefix . "menu` M
        LEFT OUTER JOIN `" . $this->db->prefix . "menulocalizations` ML ON M.`mid` = ML.`mid`
        WHERE 
              (M.`mid` = '" . $id . "') 
          AND (ML.`lang` = '" . $this->lang . "')
        ";

        $header = $comment = '';
        $curr_url = $this->curr_folder;
        $name = $this->fromLangIndex('error');
        $text = $this->fromLangIndex('error_info');
        $result = $this->db->query($query);
        if ($row = $this->db->fetch($result)) {
            $templates_views = $this->fromConfig('templates_views');
            $id = $row['mid'];
            $template = $row['template'];
            $this->page_template = (array_key_exists($template, $templates_views)) ? $templates_views[$template] : $this->page_template;
            $this->get_nav($id);
            $name = $row['name'];
            $header = (int)$row['header'];
            $curr_htaccessname = $row['htaccessname'];
            $cur_type = $row['menutype'];
            $curr_lang_ext = $this->lang;
            switch ($cur_type) {
                case 'content': {
                    switch ($curr_htaccessname) {
                        case 'index': {
                            $curr_url .= str_replace('[lang]', $curr_lang_ext, $this->permalinks[$this->perma_type]['index'][$this->curr_lang]);
                            break;
                        }
                        case 'contact': {
                            $curr_url .= str_replace('[lang]', $curr_lang_ext, $this->permalinks[$this->perma_type]['contact'][$this->curr_lang]);
                            break;
                        }
                        default: {
                            $curr_url .= str_replace('[slug]', $this->get_slug($id, $curr_lang_ext), str_replace('[name]', $this->utils->url_filter($row['name']), str_replace('[id]', $id, str_replace('[lang]', $this->lang, $this->permalinks[$this->perma_type]['inside'][$this->curr_lang]))));
                            break;
                        }
                    }
                    break;
                }
                case 'link':
                    $curr_url = str_replace('{LANG}', $this->lang, $row['link']);
                    break;
                case 'module': {
                    $module_name = 'module_' . $row['link'];
                    $curr_url .= str_replace('[lang]', $this->lang, $this->permalinks[$this->perma_type][$module_name][$this->curr_lang]);
                    break;
                }
            }
            $comment = $this->site->f_tags($row['comment']);
            $comment = $this->site->replaceImageTemplate($comment, $this->curr_folder);
            $comment = $this->contentUsibility($comment);
            $comment = str_replace('[alt]', $name, $comment);

            $text = $this->site->f_tags($row['text']);
            $text = $this->site->replaceImageTemplate($text, $this->curr_folder);
            $text = $this->contentUsibility($text);
            $text = str_replace('[alt]', $name, $text);
        }

        $this->template->assign_var('NAME', $name);
        $this->template->assign_var('NAME_UP', $this->nameup($name));
        $header_img = ($header > 0) ? '<img src="' . $this->site->getFile($header, 'image') . '" style="border:0px;" />' : '';
        $header_img_file = ($header > 0) ? $this->site->getFile($header, 'image') : '';

        if (strlen($text) > 60) {
            $this->template->assign_block_vars('have_content', array());
        }
        $this->template->assign_var('TEXT', $text);
        $this->template->assign_var('TEXT_NOTAG', strip_tags($text));
        $this->template->assign_var('CURR_URL', $curr_url);
        $this->template->assign_var('COMMENT', $comment);
        $this->template->assign_var('HEADER_IMG', $header_img);
        $this->template->assign_var('HEADER_IMG_FILE', $header_img_file);
        if ($header > 0) {
            $this->template->assign_block_vars('have_image', array());
        }

        /* B: SUB descriptions */
        $sub_desc_query = 'SELECT M.*, ML.name, ML.header, ML.text, ML.slug, ML.link as lang_link
			FROM `' . $this->db->prefix . 'menu` M
			LEFT OUTER JOIN `' . $this->db->prefix . "menulocalizations` ML ON M.mid = ML.mid
			WHERE ML.lang='" . $this->lang . "' AND M.parentid='" . $id . "' AND M.visible='0' ORDER BY M.position";
        $sub_desc_result = $this->db->query($sub_desc_query);
        $this->template->assign_block_vars('sub_descriptions', array());
        $ndx = 1;
        $cur_class = ' class="current"';
        $act_class = ' class="active"';
        $this->template->assign_block_vars('sub_descriptions.list', array(
            'NDX' => $ndx,
            'ID' => $id,
            'ACT_CLASS' => $act_class,
            'CUR_CLASS' => $cur_class,
            'NAME' => $name,
            'TITLE' => $name,
            'TEXT' => $text,
        ));
        $this->template->assign_block_vars('sub_descriptions.list_ids', array(
            'NDX' => $ndx,
            'ID' => $id,
            'ACT_CLASS' => $act_class,
            'CUR_CLASS' => $cur_class,
            'NAME' => $name,
            'TITLE' => $name,
        ));

        while ($sub_desc_row = $this->db->fetch($sub_desc_result)) {
            $act_class = $cur_class = '';
            ++$ndx;
            $list_id = $sub_desc_row['mid'];
            $list_name = $sub_desc_row['name'];
            $list_text = $this->site->f_tags($sub_desc_row['text']);
            $list_text = $this->site->replaceImageTemplate($list_text, $this->curr_folder);
            $list_text = $this->contentUsibility($list_text);
            $list_text = str_replace('[alt]', $list_name, $list_text);
            $this->template->assign_block_vars('sub_descriptions.list', array(
                'NDX' => $ndx,
                'ID' => $list_id,
                'ACT_CLASS' => $act_class,
                'CUR_CLASS' => $cur_class,
                'NAME' => $list_name,
                'TITLE' => $list_name,
                'TEXT' => $list_text,
            ));
            $this->template->assign_block_vars('sub_descriptions.list_ids', array(
                'NDX' => $ndx,
                'ID' => $list_id,
                'ACT_CLASS' => $act_class,
                'CUR_CLASS' => $cur_class,
                'NAME' => $list_name,
                'TITLE' => $list_name,
            ));
        }
        /* E: SUB descriptions */

        /* B: SUB Query */
        $sub2_query = 'SELECT M.*, ML.name, ML.header, ML.comment, ML.text, ML.slug, ML.link as lang_link
			FROM `' . $this->db->prefix . 'menu` M
			LEFT OUTER JOIN `' . $this->db->prefix . "menulocalizations` ML ON M.mid = ML.mid
			WHERE ML.lang='" . $this->lang . "' AND M.parentid='" . $id . "' AND M.visible='1' ORDER BY M.position";
        if ($this->db->num_rows($sub2_query) > 0) {
            $this->template->assign_block_vars('yes_sub', array());
            $this->template->assign_block_vars('sub_menu', array());
            if ($header > 0) {
                $this->template->assign_block_vars('yes_sub.have_image', array());
            }
        } else {
            $this->template->assign_block_vars('no_sub', array());
            if ($header > 0) {
                $this->template->assign_block_vars('no_sub.have_image', array());
            }
        }

        $sub2_result = $this->db->query($sub2_query);
        while ($sub2_row = $this->db->fetch($sub2_result)) {
            $sub2_id = $sub2_row['mid'];
            $sub2_name = $sub2_row['name'];
            $sub2_comment = $sub2_row['comment'];
            $sub2_text = $sub2_row['text'];
            $sub2_header = $sub2_row['header'];
            $sub2_type = $sub2_row['menutype'];
            $sub2_htaccessname = $sub2_row['htaccessname'];
            $sub2_url = $this->curr_folder;
            $sub2_lang_ext = $this->lang;

            $sub2_blank = ($sub2_row['newwindow'] == 1) ? ' onclick="window.open(this.href,\'_blank\');return false;"' : '';

            if (strlen($sub2_name) > 3) {
                switch ($sub2_type) {
                    case 'content': {
                        switch ($sub2_htaccessname) {
                            case 'index': {
                                $sub2_url .= str_replace('[lang]', $sub2_lang_ext, $this->permalinks[$this->perma_type]['index'][$this->curr_lang]);
                                break;
                            }
                            case 'contact': {
                                $sub2_url .= str_replace('[lang]', $sub2_lang_ext, $this->permalinks[$this->perma_type]['contacts'][$this->curr_lang]);
                                break;
                            }
                            default: {
                                //$sub2_url .= str_replace('[slug]', $this->utils->url_filter($sub2_row['slug']), str_replace('[name]', $this->utils->url_filter($sub2_row['name']), str_replace('[id]', $sub2_id, str_replace('[lang]', $sub2_lang_ext, $this->permalinks[$this->perma_type]['inside'][$this->curr_lang]))));
                                $sub2_url .= str_replace('[slug]', $this->get_slug($sub2_id, $sub2_lang_ext), str_replace('[name]', $this->utils->url_filter($sub2_row['name']), str_replace('[id]', $sub2_id, str_replace('[lang]', $sub2_lang_ext, $this->permalinks[$this->perma_type]['inside'][$this->curr_lang]))));
                                break;
                            }
                        }
                        break;
                    }
                    case 'link':
                        $sub2_url = str_replace('{LANG}', $this->lang, $this->site->uploadLink($sub2_row['link']));
                        break;
                    case 'module': {
                        $module_name = 'module_' . $sub2_row['link'];
                        $sub2_url .= str_replace('[lang]', $this->lang, $this->permalinks[$this->perma_type][$module_name][$this->curr_lang]);
                        break;
                    }
                }
                $sub2_header = (int)$sub2_header;
                $header2_img = (is_numeric($sub2_header)) ? '<img src="' . $this->curr_folder . $this->site->getFile($sub2_header, 'image') . '" style="border:0px;" />' : '';
                $header2_img_file = (is_numeric($sub2_header)) ? $this->curr_folder . $this->site->getFile($sub2_header, 'image') : '';
                $sub2_name = ($sub2_row['mid'] == $id) ? '<strong><u>' . $sub2_name . '</u></strong>' : $sub2_name;
                $this->template->assign_block_vars('yes_sub.sub', array(
                    'URL' => $sub2_url,
                    'NAME' => $sub2_name,
                    'NAME_UP' => $this->nameup($sub2_name),
                    'TITLE' => $sub2_name,
                    'COMMENT' => $sub2_comment,
                    'TEXT' => $sub2_text,
                    'TEXT_NOTAG' => strip_tags($sub2_text),
                    'IMG' => $header2_img,
                    'IMG_FILE' => $header2_img_file,
                    'BLANK' => $sub2_blank,
                ));
                $this->template->assign_block_vars('sub', array(
                    'URL' => $sub2_url,
                    'NAME' => $sub2_name,
                    'NAME_UP' => $this->nameup($sub2_name),
                    'TITLE' => $sub2_name,
                    'COMMENT' => $sub2_comment,
                    'TEXT' => $sub2_text,
                    'TEXT_NOTAG' => strip_tags($sub2_text),
                    'IMG' => $header2_img,
                    'IMG_FILE' => $header2_img_file,
                    'BLANK' => $sub2_blank,
                ));
                $this->template->assign_block_vars('sub_menu.list', array(
                    'URL' => $sub2_url,
                    'NAME' => $sub2_name,
                    'TITLE' => $sub2_name,
                    'COMMENT' => $sub2_comment,
                    'IMG' => $header2_img,
                    'IMG_FILE' => $header2_img_file,
                    'BLANK' => $sub2_blank,
                ));
                if ($sub2_header > 0) {
                    $this->template->assign_block_vars('sub.br', array());
                }
            }
        }
        /* E: SUB Query */
    }
}

$inside = new insideView();
$inside->template->pparse($inside->page_template);

/******************* inside.view.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** inside.view.php ******************/;
