<?php
/******************* news.view.php *******************
 *
 * News view module
 *
 * @author MicroPHP <info@microphp.com>
 * @web www.microphp.com
 * @copyright 2004-2019 MicroPHP
 *
 ******************** news.view.php ******************/

/** 
 * Define Namespace 
 */
namespace mcms5xx\modules;

/**
 * Checking if module included normally.
 */
if (!defined('mCMScheck')) {
    die('mCMS by MicroPHP');
}

class News extends \mcms5xx\classes\ViewPage
{
    public $news_template = 'news';
    protected $catid = 0;

    public function __construct()
    {
        parent::__construct();
        $this->onLoad();
    }

    private function onLoad()
    {
        $this->catid = $this->utils->UserGetInt('catid');
        $this->buildPage();
        $this->builNewsNavigation();
        $this->builNewsCat();
        $this->buildNewsSlide();
        $this->buildNewsItems();
    }

    private function buildPage()
    {
        $this->buildMenu();
        $cat_sql = ($this->catid > 0) ? ' && (N.catid = ' . $this->catid . ') ' : '';
        $news_page_limit = $this->fromConfig('news_page_limit');
        $news_date_format = $this->fromConfig('news_date_format');
        $events_days_limit = $this->fromConfig('events_days_limit') * 86400;
        $news_block_seperator = $this->fromConfig('news_block_seperator');
        $cat_query = 'SELECT
          N.*,
          NL.`name`,
          NL.`header`
        FROM ' . $this->db->prefix . 'newscategories N
		INNER JOIN ' . $this->db->prefix . "newscategorylocalizations NL ON NL.catid = N.catid
		WHERE 
		   (N.active = 1) 
		&& (NL.lang='" . $this->lang . "')
		" . $cat_sql . '
		ORDER BY N.position ASC
		';
        $cat_idx = 0;
        $cat_nums = $this->db->num_rows($cat_query);
        $cat_result = $this->db->query($cat_query);
        while ($cat_row = $this->db->fetch($cat_result)) {
            ++$cat_idx;
            $cat_id = $cat_row['catid'];
            $class = ($cat_idx == $cat_nums) ? ' class="nb"' : '';
            $this->template->assign_block_vars('news', array(
                'IDX' => $cat_idx,
                'CAT_ID' => $cat_id,
                'CAT_NAME' => $cat_row['name'],
                'CAT_NAME_UP' => $this->nameup($cat_row['name']),
                'CAT_DESC' => $cat_row['header'],
                'CAT_URL' => $this->curr_folder . str_replace('[lang]', $this->lang, str_replace('[catid]', $cat_id, $this->permalinks[$this->perma_type]['news_cat'][$this->curr_lang])),
                'CLASS' => $class,
            ));

            $neSql = ($cat_row['event'] == 1) ? ' && (N.newsdate>=' . (time() - $events_days_limit) . ') ' : ' && (N.newsdate<=' . time() . ') ';
            $neOrder = ($cat_row['event'] == 1) ? ' N.newsdate ASC, N.newsid DESC ' : ' N.newsdate DESC, N.newsid DESC ';
            $item_query = 'SELECT N.*, NL.name, NL.header, NL.comment, NL.slug FROM
			' . $this->db->prefix . 'news N
			INNER JOIN ' . $this->db->prefix . 'newslocalizations NL ON NL.newsid = N.newsid
			INNER JOIN ' . $this->db->prefix . "newscategories NC ON NC.catid = N.catid
			WHERE 
				(N.active='1')
			 && (NL.name!='')
			 && (NL.lang='" . $this->lang . "')
			 && (NC.catid=" . $cat_id . ')
			' . $neSql . '
			ORDER BY ' . $neOrder . '
			' . $this->db->get_limit(0, $news_page_limit);
            $item_idx = 0;
            $item_result = $this->db->query($item_query);
            $item_nums = $this->db->num_rows($item_query);
            while ($item_row = $this->db->fetch($item_result)) {
                if (($item_idx % $news_block_seperator) == 0) {
                    $this->template->assign_block_vars('news.block', array());
                }
                ++$item_idx;
                $last_ckeck = ($item_nums == $item_idx);
                $first_ckeck = (1 == $item_idx);
                $first_class_nm = ($first_ckeck) ? ' first' : '';
                $first_class = ($first_ckeck) ? ' class="first"' : '';
                $last_class_nm = ($last_ckeck) ? ' last' : '';
                $last_class = ($last_ckeck) ? ' class="last"' : '';

                $name = $item_row['name'];
                $comment = $item_row['comment'];
                $news_id = $item_row['newsid'];
                $news_url = $this->curr_folder . str_replace('[slug]', $this->utils->url_filter($item_row['slug']), str_replace('[year]', date('Y', $item_row['newsdate']), str_replace('[name]', $this->utils->url_filter($item_row['name']), str_replace('[id]', $news_id, str_replace('[lang]', $this->lang, $this->permalinks[$this->perma_type]['news'][$this->curr_lang])))));

                /*$this->template->assign_block_vars('news.block.items', array(
                    'IDX' => $item_idx,
                    'ID' => $news_id,
                    'DATE' => DATE($news_date_format, $item_row['newsdate']),
                    'NAME' => $name,
                    'TITLE' => $name,
                    'COMMENT' => $comment,
                    'URL' => $news_url,
                    'FIRST_CLASS_NM' => $first_class_nm,
                    'FIRST_CLASS' => $first_class,
                    'LAST_CLASS_NM' => $last_class_nm,
                    'LAST_CLASS' => $last_class,
                ));*/
                /*$this->template->assign_block_vars('news.items', array(
                    'IDX' => $item_idx,
                    'ID' => $news_id,
                    'DATE' => DATE($news_date_format, $item_row['newsdate']),
                    'DATE_DD' => date('d', $item_row['newsdate']),
                    'DATE_MONTH' => date('M', $item_row['newsdate']),
                    'DATE_YYYY' => date('Y', $item_row['newsdate']),
                    'NAME' => $name,
                    'TITLE' => $name,
                    'COMMENT' => $comment,
                    'URL' => $news_url,
                    'FIRST_CLASS_NM' => $first_class_nm,
                    'FIRST_CLASS' => $first_class,
                    'LAST_CLASS_NM' => $last_class_nm,
                    'LAST_CLASS' => $last_class,
                ));*/
                $this->buildNewsInfo('news.block.items', $item_row, $item_idx, $item_nums);
                $this->buildNewsInfo('news.items', $item_row, $item_idx, $item_nums);
            }
        }
    }

    private function builNewsNavigation()
    {
        /* B: Navigation bar */
        $this->get_nav(0);
        if ($this->catid > 0) {
            $cat_query = 'SELECT C.*, CL.name as cat_name FROM
			' . $this->db->prefix . 'newscategories C
			INNER JOIN ' . $this->db->prefix . "newscategorylocalizations CL on (CL.catid = C.catid)
			WHERE (C.active='1') && (CL.lang='" . $this->lang . "') && (C.catid = '" . $this->catid . "')
			";
            $cat_result = $this->db->query($cat_query);
            if ($cat_row = $this->db->fetch($cat_result)) {
                $cat_name = $cat_row['cat_name'];
                $cat_id = $cat_row['catid'];
                $this->template->assign_block_vars('where.for', array(
                    'NAME' => $this->fromLangIndex('news_archive'),
                    'SPACE' => $this->fromLangIndex('where_space'),
                    'URL' => $this->curr_folder . str_replace('[lang]', $this->lang, $this->permalinks[$this->perma_type]['module_news'][$this->curr_lang]),
                ));
                $this->template->assign_block_vars('where.end', array(
                    'NAME' => $cat_name,
                    'URL' => $this->curr_folder . str_replace('[lang]', $this->lang, str_replace('[catid]', $cat_id, $this->permalinks[$this->perma_type]['news_cat'][$this->curr_lang])),
                ));
            } else {
                $this->template->assign_block_vars('where.end', array(
                    'NAME' => $this->fromLangIndex('news_archive'),
                    'URL' => $this->curr_folder . str_replace('[lang]', $this->lang, $this->permalinks[$this->perma_type]['module_news'][$this->curr_lang]),
                ));
            }
        } else {
            $this->template->assign_block_vars('where.end', array(
                'NAME' => $this->fromLangIndex('news_archive'),
                'URL' => $this->curr_folder . str_replace('[lang]', $this->lang, $this->permalinks[$this->perma_type]['module_news'][$this->curr_lang]),
            ));
        }
        /* E: Navigation bar */
        $this->template->assign_vars(array(
            'NAME' => $this->fromLangIndex('news_archive'),
        ));
    }

    private function builNewsCat()
    {
        $news_page_limit = $this->fromConfig('news_page_limit');
        $news_date_format = $this->fromConfig('news_date_format');
        $events_days_limit = $this->fromConfig('events_days_limit') * 86400;
        $news_block_seperator = $this->fromConfig('news_block_seperator');
        $catSql = ($this->catid > 0) ? ' && (N.catid=' . $this->catid . ')' : '';
        $cat_query = 'SELECT N.*, NL.name 
        FROM `' . $this->db->prefix . 'newscategories` N
		INNER JOIN ' . $this->db->prefix . "newscategorylocalizations NL ON NL.`catid` = N.`catid`
		WHERE 
		    (N.`active` = 1) 
	  	 && (NL.`lang` = '" . $this->lang . "') 
		     " . $catSql . '
		ORDER BY N.`position` ASC
		';
        $cat_idx = 0;
        $cat_nums = $this->db->num_rows($cat_query);
        $cat_result = $this->db->query($cat_query);
        while ($cat_row = $this->db->fetch($cat_result)) {
            ++$cat_idx;
            $cat_id = $cat_row['catid'];
            $class = ($cat_idx == $cat_nums) ? ' class="nb"' : '';
            $newsCatArr = array(
                'IDX' => $cat_idx,
                'CAT_ID' => $cat_id,
                'CAT_NAME' => $cat_row['name'],
                'CAT_URL' => $this->curr_folder . str_replace('[lang]', $this->lang, str_replace('[catid]', $cat_id, $this->permalinks[$this->perma_type]['news_cat'][$this->curr_lang])),
                'CLASS' => $class,
            );
            $this->template->assign_block_vars('news_cat', $newsCatArr);
            $this->template->assign_block_vars('news' . $cat_id, $newsCatArr);
            $this->template->assign_block_vars('news_' . $cat_id, $newsCatArr);

            $neSql = ($cat_row['event'] == 1) ? ' && (N.newsdate>=' . (time() - $events_days_limit) . ') ' : ' && (N.newsdate<=' . time() . ') ';
            $neOrder = ($cat_row['event'] == 1) ? ' N.newsdate ASC, N.newsid DESC ' : ' N.newsdate DESC, N.newsid DESC ';
            $item_query = 'SELECT N.*, NL.name, NL.header, NL.comment, NL.slug FROM
			' . $this->db->prefix . 'news N
			INNER JOIN ' . $this->db->prefix . 'newslocalizations NL ON NL.newsid = N.newsid
			INNER JOIN ' . $this->db->prefix . "newscategories NC ON NC.catid = N.catid
			WHERE 
				(N.active='1')
			 && (NL.name!='')
			 && (NL.lang='" . $this->lang . "')
			 && (NC.catid=" . $cat_id . ')
			' . $neSql . '
			ORDER BY ' . $neOrder . '
			';
            $item_idx = 0;
            /* B: Page limits */
            $total_items = $this->db->num_rows($item_query);

            $newsPagingArr = $this->fromConfig('newsPagingArr');
            $limitnum = $this->utils->UserGetInt('limitnum');
            $limitnum = (($limitnum <= 0) || ($limitnum > count($newsPagingArr))) ? 1 : $limitnum;
            $limitnum_sql = $limitnum - 1;
            //$page_limit = $newsPagingArr[$limitnum_sql];
            $page_limit = $this->getKey('news_page_limit', 10);
            $all_pages = ceil($total_items / $page_limit);

            $page = $this->utils->UserGetInt('page');
            $page = (($page <= 0) || ($page > $all_pages)) ? 1 : $page;
            $page_val = ((($page + 1) <= 0) || (($page + 1) > $all_pages)) ? 1 : ($page + 1);
            //$news_start_page = ($page - 1) * $newsPagingArr[$limitnum_sql];
            $news_start_page = ($page - 1) * $page_limit;

            $item_query .= $this->db->get_limit($news_start_page, $page_limit);
            /* echo($item_query); */
            /* E: Page limits */
            $item_result = $this->db->query($item_query);
            $item_nums = $this->db->num_rows($item_query);
            while ($item_row = $this->db->fetch($item_result)) {
                ++$item_idx;
                $last_ckeck = ($item_nums == $item_idx);
                $first_ckeck = (1 == $item_idx);
                $first_class_nm = ($first_ckeck) ? ' first' : '';
                $first_class = ($first_ckeck) ? ' class="first"' : '';
                $last_class_nm = ($last_ckeck) ? ' last' : '';
                $last_class = ($last_ckeck) ? ' class="last"' : '';

                $name = $item_row['name'];
                $comment = $item_row['comment'];
                $news_id = $item_row['newsid'];
                $news_url = $this->curr_folder . str_replace('[slug]', $this->utils->url_filter($item_row['slug']), str_replace('[year]', date('Y', $item_row['newsdate']), str_replace('[name]', $this->utils->url_filter($item_row['name']), str_replace('[id]', $news_id, str_replace('[lang]', $this->lang, $this->permalinks[$this->perma_type]['news'][$this->curr_lang])))));
                $itemArr = array(
                    'IDX' => $item_idx,
                    'ID' => $news_id,
                    'IMGID' => (int)$item_row['header'],
                    'DATE' => date($news_date_format, $item_row['newsdate']),
                    'DATE_DD' => date('d', $item_row['newsdate']),
                    'DATE_MONTH' => date('M', $item_row['newsdate']),
                    'DATE_YYYY' => date('Y', $item_row['newsdate']),
                    'NAME' => $name,
                    'TITLE' => $name,
                    'COMMENT' => $comment,
                    'URL' => $news_url,
                    'FIRST_CLASS_NM' => $first_class_nm,
                    'FIRST_CLASS' => $first_class,
                    'LAST_CLASS_NM' => $last_class_nm,
                    'LAST_CLASS' => $last_class,
                );
                //$this->template->assign_block_vars('news_cat.items', $itemArr);
				$this->buildNewsInfo('news_cat.items', $item_row, $item_idx, $item_nums);
                $this->template->assign_block_vars('news' . $cat_id . '.items', $itemArr);
                $this->template->assign_block_vars('news_' . $cat_id . '.items', $itemArr);
            }

            if ($all_pages > 1) {
                $this->template->assign_block_vars('news_cat.pages', array());
                $this->template->assign_block_vars('news' . $cat_id . '.pages', array());
                $this->template->assign_block_vars('news_' . $cat_id . '.pages', array());
                for ($p = 1; $p <= $all_pages; ++$p) {
                    $pg_url = $this->curr_folder . str_replace('[lang]', $this->lang, str_replace('[page]', $p, str_replace('[catid]', $cat_id, $this->permalinks[$this->perma_type]['news_cat_page'][$this->curr_lang])));
                    $pg_href = ($p == $page) ? '' : ' href="' . $pg_url . '"';
                    $pgArr = array(
                        'NUM' => $p,
                        'URL' => $pg_url,
                        'HREF' => $pg_href,
                    );
                    $this->template->assign_block_vars('news_cat.pages.pg', $pgArr);
                    $this->template->assign_block_vars('news' . $cat_id . '.pages.pg', $pgArr);
                    $this->template->assign_block_vars('news_' . $cat_id . '.pages.pg', $pgArr);
                }
            }
        }
    }

    private function buildNewsSlide()
    {
        $news_slide_limit = $this->fromConfig('news_slide_limit');
        $news_date_format = $this->fromConfig('news_date_format');
        $catSql = ($this->catid > 0) ? ' && (NC.catid=' . $this->catid . ')' : '';
        $item_query = 'SELECT N.*, NL.name, NL.header, NL.comment, NL.slug FROM
		' . $this->db->prefix . 'news N
		INNER JOIN ' . $this->db->prefix . 'newslocalizations NL ON NL.newsid = N.newsid
		INNER JOIN ' . $this->db->prefix . "newscategories NC ON NC.catid = N.catid
		WHERE (N.active='1') && (NL.lang='" . $this->lang . "') && (NL.name!='') " . $catSql . '
		ORDER BY N.newsdate DESC, N.newsid DESC
		' . $this->db->get_limit(0, $news_slide_limit);
        if ($this->db->num_rows($item_query) > 0) {
            $this->template->assign_block_vars('news_slide', array());
        }
        $item_idx = 0;
        $item_result = $this->db->query($item_query);
        while ($item_row = $this->db->fetch($item_result)) {
            ++$item_idx;
            $name = $item_row['name'];
            $comment = $item_row['comment'];
            $news_id = $item_row['newsid'];
            $img_id = $this->utils->filterInt($item_row['header']);
            $news_url = $this->curr_folder . str_replace('[slug]', $this->utils->url_filter($item_row['slug']), str_replace('[year]', date('Y', $item_row['newsdate']), str_replace('[name]', $this->utils->url_filter($item_row['name']), str_replace('[id]', $news_id, str_replace('[lang]', $this->lang, $this->permalinks[$this->perma_type]['news'][$this->curr_lang])))));
            $img_390x255 = $this->curr_folder . 'img_390x255.php?i=' . $img_id;
            if ($item_idx == 1) {
                $this->template->assign_block_vars('news_slide.first_item', array(
                    'IDX' => $item_idx,
                    'ID' => $news_id,
                    'IMG_390X255' => $img_390x255,
                    'DATE' => date($news_date_format, $item_row['newsdate']),
                    'DATE_DD' => date('d', $item_row['newsdate']),
                    'DATE_MM' => date('m', $item_row['newsdate']),
                    'DATE_MONTH' => date('M', $item_row['newsdate']),
                    'DATE_YYYY' => date('Y', $item_row['newsdate']),
                    'NAME' => $name,
                    'TITLE' => $name,
                    'COMMENT' => $comment,
                    'URL' => $news_url,
                ));
            }
            $active_class = ($item_idx == 1) ? ' class="active"' : '';
            $this->template->assign_block_vars('news_slide.items', array(
                'IDX' => $item_idx,
                'ID' => $news_id,
                'ACTIVE_CLASS' => $active_class,
                'IMG_390X255' => $img_390x255,
                'DATE' => date($news_date_format, $item_row['newsdate']),
                'DATE_DD' => date('d', $item_row['newsdate']),
                'DATE_MM' => date('m', $item_row['newsdate']),
                'DATE_MONTH' => date('M', $item_row['newsdate']),
                'DATE_YYYY' => date('Y', $item_row['newsdate']),
                'NAME' => $name,
                'TITLE' => $name,
                'COMMENT' => $comment,
                'URL' => $news_url,
            ));
        }
    }

    private function buildNewsItems()
    {
        $news_page_limit = $this->fromConfig('news_page_limit');
        $news_date_format = $this->fromConfig('news_date_format');
        $news_block_seperator = $this->fromConfig('news_block_seperator');
        $catSql = ($this->catid > 0) ? ' && (NC.catid=' . $this->catid . ')' : '';
        $item_query = 'SELECT N.*, NL.name, NL.header, NL.comment, NL.slug FROM
		' . $this->db->prefix . 'news N
		INNER JOIN ' . $this->db->prefix . 'newslocalizations NL ON NL.newsid = N.newsid
		INNER JOIN ' . $this->db->prefix . "newscategories NC ON NC.catid = N.catid
		WHERE (N.active='1') && (NL.lang='" . $this->lang . "') && (NL.name!='') " . $catSql . '
		ORDER BY N.newsdate DESC, N.newsid DESC
		' . $this->db->get_limit(0, $news_page_limit);
        $item_idx = 0;
        $item_result = $this->db->query($item_query);
        while ($item_row = $this->db->fetch($item_result)) {
            if (($item_idx % $news_block_seperator) == 0) {
                $this->template->assign_block_vars('news_block', array());
            }
            ++$item_idx;
            $name = $item_row['name'];
            $comment = $item_row['comment'];
            $news_id = $item_row['newsid'];
            $img_id = $this->utils->filterInt($item_row['header']);
            $news_url = $this->curr_folder . str_replace('[slug]', $this->utils->url_filter($item_row['slug']), str_replace('[year]', date('Y', $item_row['newsdate']), str_replace('[name]', $this->utils->url_filter($item_row['name']), str_replace('[id]', $news_id, str_replace('[lang]', $this->lang, $this->permalinks[$this->perma_type]['news'][$this->curr_lang])))));
            $class = (($item_idx % $news_block_seperator) == 0) ? 'fl-right' : 'fl-left';
            $img_115x75 = $this->curr_folder . 'img_115x75.php?i=' . $img_id;
            $this->template->assign_block_vars('news_block.items', array(
                'IDX' => $item_idx,
                'ID' => $news_id,
                'CLASS' => $class,
                'IMG_115x75' => $img_115x75,
                'DATE' => date($news_date_format, $item_row['newsdate']),
                'DATE_DD' => date('d', $item_row['newsdate']),
                'DATE_MONTH' => date('M', $item_row['newsdate']),
                'DATE_YYYY' => date('Y', $item_row['newsdate']),
                'NAME' => $name,
                'TITLE' => $name,
                'COMMENT' => $comment,
                'URL' => $news_url,
            ));
        }
    }
}

$news = new News();
$news->template->pparse($news->news_template);

/******************* news.view.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** news.view.php ******************/;
