<?php
/******************* main.view.php *******************
 *
 * index, main page view module
 *
 * @author MicroPHP <info@microphp.com>
 * @web www.microphp.com
 * @copyright 2004-2019 MicroPHP
 *
 ******************** main.view.php ******************/

/** 
 * Define Namespace 
 */
namespace mcms5xx\modules;

/**
 * Checking if module included normally.
 */
if (!defined('mCMScheck')) {
    die('mCMS by MicroPHP');
}

class mainView extends \mcms5xx\classes\ViewPage
{
    public function __construct()
    {
        parent::__construct();
        $this->onLoad();
    }

    private function onLoad()
    {
        $this->buildPage();
    }

    private function buildPage()
    {
		/*
		echo '<pre>';
		print_r($_SESSION);
		exit; */
        $this->buildMain();

        $this->buildTitle();
        
        $this->buildMenu();
    }

    private function buildMain()
    {
        $this->template->assign_vars(array(
            'MAIN_INDEX' => $this->fromLangIndex('main_index'),
        ));
        
		$this->categories();
    }



    private function categories()
    {
		$level = $this->utils->GetSession('member_usertype');
		$subQuery = "";
		if($level != "") {
			$subQuery = " AND S.level='".$level."'";
		}

        $sql = 'SELECT 
            S.*, SL.name, SL.header 
        FROM '.$this->db->prefix.'questionscategories S
		INNER JOIN ' .$this->db->prefix."questionscategorylocalizations SL ON SL.catid = S.catid
		WHERE SL.lang='" .$this->lang."' && S.active = 1 && S.index = 1 ".$subQuery."
		ORDER BY S.position ASC
		";
        /* echo($sql);	 */
        $result = $this->db->query($sql);
        $ndx = 0;
        $slide_nums = $this->db->num_rows($sql);
        while ($row = $this->db->fetch($result)) {
            ++$ndx;
            $id = $row['catid'];
            $name = $row['name'];
            $icon = $row['icon'];
            $more_txt = $row['header'];
            $fileid = $row['image'];
            $img_file = $this->curr_folder.$this->site->getFile($fileid);
            $img = '<img src="/'.$img_file.'" alt="" />';
			$url = $this->curr_folder . str_replace('[lang]', $this->lang, str_replace('[catid]', $id, $this->permalinks[$this->perma_type]['module_braincategory'][$this->curr_lang]));
			
            if ($ndx == 1) {
                $this->template->assign_block_vars('categories', array());
            }
			

            $this->template->assign_block_vars('categories.items', array(
				'ID' => $id,
				'NDX' => $ndx,
				'NAME' => $name,
				'NAME_UP' => $this->nameup($name),
				'MORE_TXT' => $this->nameup($more_txt),
				'MORE_TXT_UP' => $this->nameup($more_txt),
				'TITLE' => $name,
				'IMG' => $img,
				'IMGID' => $fileid,
				'IMG_FILE' => $img_file,
				'TEXT' => $more_txt,
				'TEXT_NOTAG' => strip_tags($more_txt),
				'ICON' => $icon,
			    'URL' => $url,
			   // 'HREF' => (strlen($url) > 0) ? ' href="'.$url.'"' : '',
				'FILEID' => $fileid,
            ));
            
        }
    }

    private function slider()
    {
        $sql = 'SELECT 
            S.*, SL.name, SL.more_txt, SL.text, SL.embed_code, SL.url 
        FROM '.$this->db->prefix.'slider S
		INNER JOIN ' .$this->db->prefix."sliderlocalizations SL ON SL.sid = S.sid
		WHERE SL.lang='" .$this->lang."' && S.active = 1
		ORDER BY S.position ASC
		";
        /* echo($sql);	 */
        $result = $this->db->query($sql);
        $ndx = 0;
        $slide_nums = $this->db->num_rows($sql);
        while ($row = $this->db->fetch($result)) {
            ++$ndx;
            $id = $row['sid'];
            $name = $row['name'];
            $more_txt = $row['more_txt'];
            $text = $row['text'];
            $embed_code = $row['embed_code'];
            $url = $row['url'];
            $fileid = $row['slide_img'];
            $img_file = $this->curr_folder.$this->site->getFile($fileid);
            $img = '<img src="/'.$img_file.'" alt="" />';

            if ($ndx == 1) {
                $this->template->assign_block_vars('slider', array(
                    'SLIDE_NUMS' => $slide_nums,
                    'ID' => $id,
                    'NDX' => $ndx,
                    'NAME' => $name,
                    'NAME_UP' => $this->nameup($name),
                    'MORE_TXT' => $this->nameup($more_txt),
                    'MORE_TXT_UP' => $this->nameup($more_txt),
                    'TITLE' => $name,
                    'IMG' => $img,
                    'IMGID' => $fileid,
                    'IMG_FILE' => $img_file,
                    'TEXT' => $text,
                    'TEXT_NOTAG' => strip_tags($text),
                    'EMBED_CODE' => $embed_code,
                    'URL' => $url,
                    'HREF' => (strlen($url) > 0) ? ' href="'.$url.'"' : '',
                    'FILEID' => $fileid,
                ));
            }
            $class_active = ($ndx == 1) ? ' class="active"' : '';
            $class_val_active = ($ndx == 1) ? ' active' : '';

            $this->template->assign_block_vars('slider.nums', array(
                'ID' => $id,
                'NDX' => $ndx,
                'IMGID' => $fileid,
            ));
            $this->template->assign_block_vars('slider.items', array(
                'ID' => $id,
                'NDX' => $ndx,
                'NDX_0' => ($ndx - 1),
                'NDX0' => ($ndx - 1),
                'NAME' => $name,
                'NAME_UP' => $this->nameup($name),
                'MORE_TXT' => $this->nameup($more_txt),
                'MORE_TXT_UP' => $this->nameup($more_txt),
                'TITLE' => $name,
                'CLASS_ACTIVE' => $class_active,
                'CLASS_VAL_ACTIVE' => $class_val_active,
                'IMG' => $img,
                'IMGID' => $fileid,
                'IMG_FILE' => $img_file,
                'TEXT' => $text,
                'TEXT_NOTAG' => strip_tags($text),
                'EMBED_CODE' => $embed_code,
                'URL' => $url,
                'HREF' => (strlen($url) > 0) ? ' href="'.$url.'"' : '',
                'FILEID' => $fileid,
            ));
            if (strlen($url) > 0) {
                $this->template->assign_block_vars('slider.items.more', array());
            }
            if (strlen($name) > 5) {
                $this->template->assign_block_vars('slider.items.panel', array(
                    'ID' => $id,
                    'NDX' => $ndx,
                    'NAME' => $name,
                    'NAME_UP' => $this->nameup($name),
                    'TITLE' => $name,
                    'IMG' => $img,
                    'IMGID' => $fileid,
                    'IMG_FILE' => $img_file,
                    'TEXT' => $text,
                    'TEXT_NOTAG' => strip_tags($text),
                    'EMBED_CODE' => $embed_code,
                    'URL' => $url,
                    'HREF' => (strlen($url) > 0) ? ' href="'.$url.'"' : '',
                    'FILEID' => $fileid,
                ));
            }
            if (strlen($text) > 0) {
                $this->template->assign_block_vars('slider.items.desc', array());
            }
            if (strlen($embed_code) > 0) {
                $this->template->assign_block_vars('slider.items.embed_code', array());
            } else {
                $this->template->assign_block_vars('slider.items.no_embed_code', array());
            }
        }
        if ($ndx == 0) {
            $this->template->assign_block_vars('slider', array());
        }
    }
}

$main = new mainView();
$main->template->pparse('main');

/******************* main.view.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** main.view.php ******************/;
