<?php
/******************* mtest.view.php *******************
 *
 * index, mtest page view module
 *
 * @author MicroPHP <info@microphp.com>
 * @web www.microphp.com
 * @copyright 2004-2019 MicroPHP
 *
 ******************** mtest.view.php ******************/

/** 
 * Define Namespace 
 */
namespace mcms5xx\modules;

/**
 * Checking if module included normally.
 */
if (!defined('mCMScheck')) {
    die('mCMS by MicroPHP');
}

class mtestView extends \mcms5xx\classes\ViewPage
{
    public function __construct()
    {
        parent::__construct();
        $this->onLoad();
    }

    private function onLoad()
    {
		$level = $this->utils->GetSession('member_usertype');
		if($level == "") {
            $index_url = $this->curr_folder.str_replace('[lang]', $this->lang, $this->permalinks[$this->perma_type]['index'][$this->curr_lang]);
			@header('location: '.$index_url);
            $this->utils->Redirect($index_url);
			exit;
		}
        $this->buildPage();
    }

    private function buildPage()
    {
		$testid = $this->utils->UserGetInt('testid');
		
		$sql = 'SELECT 
            S.*, SL.name
        FROM '.$this->db->prefix.'tests S
		INNER JOIN ' .$this->db->prefix."testslocalizations SL ON SL.testid = S.testid
		WHERE SL.lang='" .$this->lang."' && S.active = 1 && S.testid='".(int)$testid."'";
		
        $result = $this->db->query($sql);
        if($row = $this->db->fetch($result)) {
			
			$this->buildPageInfo($row);
			//$this->buildQuestions($testid);
			
		}
		
		

        $this->buildTitle();
        
        $this->buildMenu();
    }

    private function buildPageInfo($row)
    {
		$subQuery = " AND S.level='".$level."'";
		$name = $row['name'];
		$testid = $row['testid'];
		$caticon = '';
		
		/*CAT*/
		$sqlcat = 'SELECT 
            S.*, SL.name
        FROM '.$this->db->prefix.'questionscategories S
		INNER JOIN ' .$this->db->prefix."questionscategorylocalizations SL ON SL.catid = S.catid
		WHERE SL.lang='" .$this->lang."' && S.active = 1 && S.catid='".(int)$row['catid']."' ".$subQuery."";
		
        $resultcat = $this->db->query($sqlcat);
        if($rowcat = $this->db->fetch($resultcat)) {
			$caticon = $rowcat['icon'];
		}
		else {
			$index_url = $this->curr_folder.str_replace('[lang]', $this->lang, $this->permalinks[$this->perma_type]['index'][$this->curr_lang]);
			@header('location: '.$index_url);
            $this->utils->Redirect($index_url);
			exit;

		}
		
		
		
        $this->template->assign_vars(array(
            'TESTID' => $testid,
            'NAME' => $name,
            'ICON' => $caticon,
        ));
        
		//$this->categories();
    }



    private function buildQuestions($testid)
    {
        $sql = 'SELECT 
            S.*, SL.name
        FROM '.$this->db->prefix.'questions S
		INNER JOIN ' .$this->db->prefix."questionslocalizations SL ON SL.questionid = S.questionid
		WHERE SL.lang='" .$this->lang."' && S.active = 1 && S.testid='".(int)$testid."'
		ORDER BY S.questionsdate DESC
		";

        /* echo($sql);	 */
        $result = $this->db->query($sql);
        $ndx = 0;
        $slide_nums = $this->db->num_rows($sql);
        while ($row = $this->db->fetch($result)) {
            
            $id = $row['questionid'];
            $name = $row['name'];
           
            if ($ndx == 0) {
                $this->template->assign_block_vars('questions', array());
            }
			
			
            ++$ndx;	
            $this->template->assign_block_vars('questions.items', array(
				'ID' => $id,
				'NDX' => $ndx,
				'NAME' => $name,
				'NAME_UP' => $this->nameup($name),
				'TITLE' => $name,
		    ));
			
			$this->template->assign_block_vars('questions.items.answers', array());
			$this->buildAnswers($id, 'questions.items.answers');
        }
    }
	

    private function buildAnswers($questionid, $template)
    {
        $sql = 'SELECT 
            S.*, SL.name
        FROM '.$this->db->prefix.'questionanswers S
		INNER JOIN ' .$this->db->prefix."questionanswerslocalizations SL ON SL.answerid = S.answerid
		WHERE SL.lang='" .$this->lang."' && S.active = 1 && S.questionid='".(int)$questionid."'
		ORDER BY S.answersdate ASC
		";
	   /* echo($sql);	 */
        $result = $this->db->query($sql);
        $ndx = 0;
        $slide_nums = $this->db->num_rows($sql);
        while ($row = $this->db->fetch($result)) {
            
			$checked = '';
            $id = $row['answerid'];
            $name = $row['name'];
           
			if($ndx == 0)
				$checked = ' checked';

            ++$ndx;	
            $this->template->assign_block_vars($template.'.aitems', array(
				'ID' => $id,
				'NDX' => $ndx,
				'CHECKED' => $checked,
				'NAME' => $name,
				'NAME_UP' => $this->nameup($name),
				'TITLE' => $name,
		    ));
			
			
        }
    }
	
}

$mtest = new mtestView();
$mtest->template->pparse('mtest');

/******************* mtest.view.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** mtest.view.php ******************/;
