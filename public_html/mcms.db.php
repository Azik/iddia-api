<?php
/******************* mcms.db.php *******************
 *
 * Database Configuration file
 *
 * @author MicroPHP <info@microphp.com>
 * @web www.microphp.com
 * @copyright 2004-2019 MicroPHP
 *
 ******************** mcms.db.php ******************/

/**
 * Define Namespace
 */
namespace mcms5xx;
/**
 * Checking if class included normally.
 */
if (!defined('mCMScheck')) {
    die('mCMS by MicroPHP');
}

/**
 * Database configuration Class
 */
class dbConfig
{
    /**
     * @var string Database name
     */
    public $db_name = 'mcms_iddia';

    /**
     * @var string Database user
     */
    public $db_user = 'root';

    /**
     * @var string Database password
     */
    public $db_pass = '';

    /**
     * @var string Database host
     */
    public $db_host = 'localhost';

    /**
     * @var string tables prefix
     */
    public $db_prefix = 's_';

    /**
     * @var string Database connection type
     *             mysqli, pdo
     */
    public $db_connection = 'mysqli';

    /**
     * @var string $site_host current site host address
     */
    public $site_host = 'http://mcms.ws';

    public function __construct()
    {
        $this->site_host = (($_SERVER['REMOTE_ADDR'] == '127.0.0.1') || ($_SERVER['REMOTE_ADDR'] == '::1')) ? 'http://localhost' : 'http://narportal';
    }
}

/**
 * Set for DB config
 */
define('mCMSdb', '1');

/******************* mcms.db.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** mcms.db.php ******************/;
