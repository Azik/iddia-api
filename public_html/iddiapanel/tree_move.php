<?php
/******************* tree_move.php *******************
 *
 *
 * @author MicroPHP <info@microphp.com>
 * @web www.microphp.com
 * @copyright 2004-2019 MicroPHP
 *
 ******************** tree_move.php ******************/

/** 
 * Define Namespace 
 */
namespace mcms5xx\admin;

define('iFolded', '../');
require_once iFolded.'m/classes/adminpage.class.php';
@$_GET['module'] = 'section';

class Index extends \mcms5xx\classes\AdminPage
{
    public function __construct()
    {
        parent::__construct();
    }

    public function onLoad()
    {
        /* $p = '';
        foreach ($_GET as $key=>$index) {
            $p .= $key ."=>". $index ."\n";
        }
        $handle = @fopen("file.txt", "a+");
        @fwrite($handle, "GET:".$p);
        @fclose($handle);
         */

        /* $p = '';
        foreach ($_POST as $key=>$index) {
            $p .= $key ."=>". $index ."\n";
        }
        $handle = @fopen("file.txt", "a+");
        @fwrite($handle, "\n\n"."POST:".$p);
        @fclose($handle); */

        $this->buildPage();
    }

    public function buildPage()
    {
        $user_type = $this->user->GetCurrentUserTypeText();

        if (!$this->user->IsLogin()) {
            return;
        }

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $move_id = $this->utils->UserPostInt('move_id');
            $old_parent = $this->utils->UserPostInt('old_parent');
            $position = $this->utils->UserPostInt('position');
            $old_position = $this->utils->UserPostInt('old_position');
            $curr_id = $this->utils->UserPostInt('curr_id');
            $hitmode = $this->utils->UserPost('hitmode');
            $check_move = (($move_id == 0) && ($old_parent == 0));

            //if ($move_id > 0)
            //if ( ( !$check_move ) && ($move_id > 0) )
            if (($move_id > 0) && ($old_parent > 0)) {

                /* $handle = @fopen("file.txt", "a+");
                @fwrite($handle, "\n\n"."PPPPP");
                @fclose($handle); */

                switch ($hitmode) {
                    case 'over': {
                        /* B: Move over  */
                        /* $max_position = $this->db->query("SELECT MAX(position) AS position FROM " . $this->db->prefix . "menu WHERE parentid='" . $move_id . "'");
                        $max_position = $this->db->fetch($max_position);
                        $max_position = $max_position['position'];

                        $this->db->query("UPDATE " . $this->db->prefix . "menu SET position='" . (++$max_position) . "', parentid=" . $move_id . " WHERE mid=" . $curr_id); */
                        if (@$this->user->perm_string['2'] == 1) {
                            //Perm for Edit
                            $this->db->query('UPDATE '.$this->db->prefix."menu
							SET 
								position='0', 
								parentid=" .$move_id.' 
							WHERE 
								mid=' .$curr_id);
                            $this->orderSection($move_id);
                        }
                        break;
                        /* E: Move over  */
                    }

                    case 'before': {
                        /* B: Move before section */
                        if (@$this->user->perm_string['2'] == 1) {
                            //Perm for Edit
                            $this->db->query('UPDATE '.$this->db->prefix.'menu
							SET 
								`position` = `position`+1
							WHERE 
								(`parentid` = ' .$move_id.')
							AND (`position` > ' .$position.')
							');
                            $this->db->query('UPDATE '.$this->db->prefix."menu
							SET 
								`position` = '" .($position + 1)."'
							WHERE 
								 (`mid` = " .$curr_id.')
							');
                        }
                        /* $result = $this->db->query("SELECT mid, position, parentid
                            FROM " . $this->db->prefix . "menu
                            WHERE
                                parentid='" . $move_id . "'
                            ORDER BY position DESC");
                        while ($row = $this->db->fetch($result)) {
                        } */
                        /* $move_position = $this->db->query("SELECT MAX(`position`) AS `position`, `parentid` FROM " . $this->db->prefix . "menu WHERE mid='" . $move_id . "'");
                        $move_position = $this->db->fetch($move_position);
                        $move_parentid = $move_position['parentid'];
                        $ndx = $move_position['position'];

                        $result = $this->db->query("SELECT mid, position, parentid
                            FROM " . $this->db->prefix . "menu
                            WHERE
                                parentid='" . $move_parentid . "'
                            ORDER BY position DESC");
                        while ($row = $this->db->fetch($result)) {
                            $this->db->query("UPDATE " . $this->db->prefix . "menu
                                SET
                                    position='" . ($ndx--) . "'
                                WHERE
                                     (`parentid`='" . $move_parentid . "')
                                 && (`mid`=" . $row['mid'] . ")");
                            if ($row['mid'] == $move_id) {
                                $this->db->query("UPDATE " . $this->db->prefix . "menu
                                    SET
                                        `parentid`='" . $move_parentid . "',
                                        `position`='" . ($ndx--) . "'
                                    WHERE
                                        (`mid`=" . $curr_id . ")");
                            }
                        } */
                        if (@$this->user->perm_string['2'] == 1) {
                            //Perm for Edit
                            $this->orderSection($move_id);
                        }
                        break;
                        /* E: Move before section */
                    }

                    case 'after': {
                        /* B: Move after section */
                        if (@$this->user->perm_string['2'] == 1) {
                            //Perm for Edit
                            $up_query = 'UPDATE '.$this->db->prefix.'menu
							SET 
								`position` = `position` - 2
							WHERE 
								(`parentid` = ' .$move_id.')
							AND (`position` <= ' .($position + 1).')
						    ';
                            $this->db->query($up_query);
                        }

                            /* $handle = @fopen("file.txt", "a+");
                            @fwrite($handle, "\n\n"."POST:".$up_query);
                            @fclose($handle); */

                        if (@$this->user->perm_string['2'] == 1) {
                            //Perm for Edit
                            $this->db->query('UPDATE '.$this->db->prefix."menu
							SET 
								`position` = '" .$position."'
							WHERE 
								 (`mid` = " .$curr_id.')
							');
                        }
                            /* $handle = @fopen("file.txt", "a+");
                            @fwrite($handle, "\n\n"."POST:"."UPDATE " . $this->db->prefix . "menu
                            SET
                                `position` = '" . ($position-1) . "'
                            WHERE
                                 (`mid` = " . $curr_id . ")
                            ");
                            @fclose($handle); */

                        /* $move_position = $this->db->query("SELECT `position`, `parentid` FROM " . $this->db->prefix . "menu WHERE mid='" . $move_id . "'");
                        $move_position = $this->db->fetch($move_position);
                        $move_parentid = $move_position['parentid'];

                        $ndx = 1;
                        $result = $this->db->query("SELECT mid, position, parentid
                            FROM " . $this->db->prefix . "menu
                            WHERE
                                parentid='" . $move_parentid . "'
                            ORDER BY position ASC");
                        while ($row = $this->db->fetch($result)) {
                            $this->db->query("UPDATE " . $this->db->prefix . "menu
                                SET
                                    position='" . ($ndx++) . "'
                                WHERE
                                     (`parentid`='" . $move_parentid . "')
                                 && (`mid`=" . $row['mid'] . ")");
                            if ($row['mid'] == $move_id) {
                                $this->db->query("UPDATE " . $this->db->prefix . "menu
                                    SET
                                        `parentid`='" . $move_parentid . "',
                                        `position`='" . ($ndx++) . "'
                                    WHERE
                                        (`mid`=" . $curr_id . ")");
                            }
                        } */
                        if (@$this->user->perm_string['2'] == 1) {
                            //Perm for Edit
                            $this->orderSection($move_id);
                        }
                        break;
                        /* E: Move after section */
                    }
                }
            }
            echo time();
            /* $p = '';
            foreach ($_POST as $key=>$index) {
                $p .= $key ."=>". $index ."\n";
            }
            echo $p; */
        }
    }
}

$index = new Index();
include $index->lg_folder.'/index.lang.php';

$index->onLoad();

/******************* tree_move.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** tree_move.php ******************/;
