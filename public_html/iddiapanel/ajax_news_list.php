<?php
/******************* ajax_news_list.php *******************
 *
 *
 * @author MicroPHP <info@microphp.com>
 * @web www.microphp.com
 * @copyright 2004-2019 MicroPHP
 *
 ******************** ajax_news_list.php ******************/

/** 
 * Define Namespace 
 */
namespace mcms5xx\admin;

define('iFolded', '../');
require_once iFolded.'m/classes/adminpage.class.php';
@$_GET['module'] = 'news';

class Index extends \mcms5xx\classes\AdminPage
{
    public function __construct()
    {
        parent::__construct();
    }

    public function onLoad()
    {
        $this->buildPage();
    }

    public function buildPage()
    {
        $user_type = $this->user->GetCurrentUserTypeText();

        if (!$this->user->IsLogin()) {
            return;
        }

        $this->listNewsAjax();
    }
	
	private function listNewsAjax()
	{
        /*$dataArr['data'] = array();
        $dataArr['data'][] = array(
            0,
            1,
            2,
            3,
            4
        );*/

        /*$p = '';
        foreach ($_GET as $key=>$index) {
            $p .= $key ."=>". $index ."\n";
        }
        $handle = @fopen("file.txt", "a+");
        @fwrite($handle, "\n\n"."POST:".$p);
        @fclose($handle);*/

        $dataArr['data'] = array();
        $nums = 0; $catid = $this->utils->UserGetInt('catid');
        $cat_sql = ($catid>0) ? " WHERE  (N.catid='" . $catid . "') " : "";
        $list_sql = "SELECT
          N.*,
          IF ((NL.`name`!=''), NL.`name`, NLIN.`name`) as `name`,
          IF ((NL.`name`!=''), NL.`text`, NLIN.`text`) as `text`,
          IF ((NL.`slug`!=''), NL.`slug`, NLIN.`slug`) as `slug`,
          NR.read_date,
          NR.total_views,
          NR.month_views
        FROM " . $this->db->prefix . 'news N
        LEFT JOIN ' . $this->db->prefix . 'news_read NR ON (NR.newsid = N.newsid)
        LEFT JOIN ' . $this->db->prefix . "newslocalizations NL ON (NL.newsid = N.newsid) AND (NL.lang='" . $this->default_lang . "')
        LEFT JOIN " . $this->db->prefix . "newslocalizations NLIN ON ( NLIN.newsid = N.newsid ) AND (NLIN.name != '')
        ".$cat_sql."
        GROUP BY N.newsid
        ";

        $start = $this->utils->UserGetInt("start");
        $length = $this->utils->UserGetInt("length");
        $length = ($length > 0) ? $length : 10;

        //echo("<pre>\n\n\n\n\n".$list_sql."</pre>");
        //$this->newsCount = $this->db->num_rows($list_sql);
        $w_sql = "";
        $s_value = (strlen(@$_GET['search']['value'])>0) ? $this->utils->txt_request_filter_user(@$_GET['search']['value']) : "";
        if (strlen($s_value)>2) {
            $w_sql = " WHERE
					(S.name LIKE '%".$s_value."%')
				 OR (S.newsid LIKE '%".$s_value."%')
				 OR (S.text LIKE '%".$s_value."%')
				 OR (S.slug LIKE '%".$s_value."%')
				";
        }
        $sql = "SELECT S.* FROM ( ".$list_sql." ) S ".$w_sql." ORDER BY S.newsdate DESC, S.newsid DESC ";
        $nums_sql = "SELECT COUNT(`newsid`) as `total` FROM ( ".$list_sql." ) S ".$w_sql;
        $nums_result = $this->db->query($nums_sql);
        $recordsTotal = 0;
        if ($nums_row = $this->db->fetch($nums_result)) {
            $recordsTotal = $nums_row['total'];
        }
        $this->newsCount = $recordsTotal;
        $sql .= " LIMIT ".$start.", ".$length." ";
        //echo ($sql);
        $result = $this->db->query($sql);
        $edit_url_lang = '<a href="[EDIT_URL]" title="'.$this->fromLang('news_edit').'" class="btn yellow"> '.$this->fromLang('news_edit').'&nbsp; <i class="fa fa-pencil"></i></a>';
        $del_url_lang = '<a href="JavaScript:doNewsDelete([ID])" title="'.$this->fromLang('news_delete').'" class="btn red ask" title="'.$this->fromLang('news_delete').'"> '.$this->fromLang('news_delete').'&nbsp; <i class="fa fa-trash"></i></a>';
        $active_url_lang = '<a href="[ACTIVE_URL]" class="btn btn-success">'.$this->fromLang('news_active').'&nbsp; <i class="fa fa-unlock"></i></a>';
        $inactive_url_lang = '<a href="[INACTIVE_URL]" class="btn btn-danger">'.$this->fromLang('news_inactive').'&nbsp; <i class="fa fa-lock"></i></a>';
        while ($row = $this->db->fetch($result)) {
            $id = $row['newsid'];
            $date = $row['newsdate'];
            $name = $row['name'];
            $comment = $row['text'];
            $read_date = (int)$row['read_date'];
            $total_views = (int)$row['total_views'];
            $month_views = (int)$row['month_views'];
            $active = ($row['active'] == 1) ? $this->fromLang('news_yes') : $this->fromLang('news_no');
            $index = ($row['index'] == 1) ? $this->fromLang('news_yes') : $this->fromLang('news_no');

            $comment = $this->utils->GetPartOfText($comment, 200);
            $links_url = '';
            $edit_url ='?' . $this->module_qs . '=news&catid=' . $catid . '&newsid=' . $id;
            $active_url = '?' . $this->module_qs . '=news&catid=' . $catid . '&newsid=' . $id . '&active=1';
            $inactive_url = '?' . $this->module_qs . '=news&catid=' . $catid . '&newsid=' . $id . '&active=2';
            if ((@$this->user->perm_string['2'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                //Perm for Edit
                $links_url .= str_replace('[EDIT_URL]', $edit_url, $edit_url_lang);
            }
            if ((@$this->user->perm_string['1'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                //Perm for Del
                $links_url .= str_replace('[ID]', $id, $del_url_lang);
            }
            if ((@$this->user->perm_string['2'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                //Perm for Edit
                if ($row['active'] == 1) {
                    $links_url .= str_replace('[ACTIVE_URL]', $active_url, $active_url_lang);
                } else {
                    $links_url .= str_replace('[INACTIVE_URL]', $inactive_url, $inactive_url_lang);
                }
            }
            $dataArr['data'][] = array(
                '<input type="checkbox" class="checkboxes" value="'.$id.'"/>',
                '<a href="'.$edit_url.'" title="'.$this->fromLang('news_edit').'">'.$name.'</a>',
                $this->utils->formattedDate($date),
                $links_url
            );
        }


        $_SESSION['draw'] = @$_SESSION['draw'] + 1;
        $dataArr['draw'] = $_SESSION['draw'];
        $dataArr['recordsTotal'] = $nums;
        $dataArr['recordsFiltered'] = $recordsTotal;//$ndx;

		echo json_encode($dataArr);
	}
}

$index = new Index();
include $index->lg_folder.'/index.lang.php';
require_once $index->lg_folder.'/'.$index->module.'.lang.php';
$index->onLoad();

/******************* ajax_news_list.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** ajax_news_list.php ******************/;
