<?php
/******************* help.php *******************
 *
 * Admin help file
 *
 * @author MicroPHP <info@microphp.com>
 * @web www.microphp.com
 * @copyright 2004-2019 MicroPHP
 *
 ******************** help.php ******************/

/** 
 * Define Namespace 
 */
namespace mcms5xx\admin;

define('iFolded', '../');
@$_GET['module'] = 'help';
require_once iFolded.'m/classes/adminpage.class.php';

class Index extends \mcms5xx\classes\AdminPage
{
    public function __construct()
    {
        parent::__construct();
    }

    public function onLoad()
    {
        $this->buildPage();
    }

    public function buildPage()
    {
        $user_type = $this->user->GetCurrentUserTypeText();

        $this->template->assign_vars(array(
            'MODULE_QS' => $this->module_qs,
            'TITLE' => $this->fromLangIndex('help').$this->fromLangIndex('seperator').$this->fromLangIndex('title'),
            'USER_RIGHT' => $this->fromLangIndex('user_right', $user_type),
            'LOGOUT' => $this->fromLangIndex('logout'),
            'YEAR' => date('Y'),
        ));
    }
}

$index = new Index();
include $index->lg_folder.'/index.lang.php';
require_once $index->lg_folder.'/'.$index->module.'.lang.php';

$index->onLoad();
$index->insertModule();

/******************* help.php *******************
*
* Copyright : (C) 2004 - 2019. All Rights Reserved
*
******************** help.php ******************/;
