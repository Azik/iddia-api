<?php
/******************* ajax_questions_list.php *******************
 *
 *
 * @author MicroPHP <info@microphp.com>
 * @web www.microphp.com
 * @copyright 2004-2019 MicroPHP
 *
 ******************** ajax_questions_list.php ******************/

/** 
 * Define Namespace 
 */
namespace mcms5xx\admin;

define('iFolded', '../');
require_once iFolded.'m/classes/adminpage.class.php';
@$_GET['module'] = 'questions';

class Index extends \mcms5xx\classes\AdminPage
{
    public function __construct()
    {
        parent::__construct();
    }

    public function onLoad()
    {
        $this->buildPage();
    }

    public function buildPage()
    {
        $user_type = $this->user->GetCurrentUserTypeText();

        if (!$this->user->IsLogin()) {
            return;
        }

        $this->listquestionsAjax();
    }
	
	private function listquestionsAjax()
	{
        /*$dataArr['data'] = array();
        $dataArr['data'][] = array(
            0,
            1,
            2,
            3,
            4
        );*/

        /*$p = '';
        foreach ($_GET as $key=>$index) {
            $p .= $key ."=>". $index ."\n";
        }
        $handle = @fopen("file.txt", "a+");
        @fwrite($handle, "\n\n"."POST:".$p);
        @fclose($handle);*/

        $dataArr['data'] = array();
        $nums = 0; $testid = $this->utils->UserGetInt('testid');
        $cat_sql = ($testid>0) ? " WHERE  (N.testid='" . $testid . "') " : "";
        $list_sql = "SELECT
          N.*,
          IF ((NL.`name`!=''), NL.`name`, NLIN.`name`) as `name`
        FROM " . $this->db->prefix . 'questions N
        LEFT JOIN ' . $this->db->prefix . "questionslocalizations NL ON (NL.questionid = N.questionid) AND (NL.lang='" . $this->default_lang . "')
        LEFT JOIN " . $this->db->prefix . "questionslocalizations NLIN ON ( NLIN.questionid = N.questionid ) AND (NLIN.name != '')
        ".$cat_sql."
        GROUP BY N.questionid
        ";

		
        $start = $this->utils->UserGetInt("start");
        $length = $this->utils->UserGetInt("length");
        $length = ($length > 0) ? $length : 10;

        //echo("<pre>\n\n\n\n\n".$list_sql."</pre>");
        //$this->questionsCount = $this->db->num_rows($list_sql);
        $w_sql = "";
        $s_value = (strlen(@$_GET['search']['value'])>0) ? $this->utils->txt_request_filter_user(@$_GET['search']['value']) : "";
        if (strlen($s_value)>2) {
            $w_sql = " WHERE
					(S.name LIKE '%".$s_value."%')
				 OR (S.questionid LIKE '%".$s_value."%')
				";
        }
        $sql = "SELECT S.* FROM ( ".$list_sql." ) S ".$w_sql." ORDER BY S.questionsdate DESC, S.questionid DESC ";
        $nums_sql = "SELECT COUNT(`questionid`) as `total` FROM ( ".$list_sql." ) S ".$w_sql;
        $nums_result = $this->db->query($nums_sql);
        $recordsTotal = 0;
        if ($nums_row = $this->db->fetch($nums_result)) {
            $recordsTotal = $nums_row['total'];
        }
        $this->questionsCount = $recordsTotal;
        $sql .= " LIMIT ".$start.", ".$length." ";
        //echo ($sql);
        $result = $this->db->query($sql);
        $edit_url_lang = '<a href="[EDIT_URL]" title="'.$this->fromLang('questions_edit').'" class="btn yellow"> '.$this->fromLang('questions_edit').'&nbsp; <i class="fa fa-pencil"></i></a>';
        $answers_url_lang = '<a href="[ANSWER_URL]" title="Cavablar" class="btn yellow"> answers&nbsp;</a>';
        $del_url_lang = '<a href="JavaScript:doquestionsDelete([ID])" title="'.$this->fromLang('questions_delete').'" class="btn red ask" title="'.$this->fromLang('questions_delete').'"> '.$this->fromLang('questions_delete').'&nbsp; <i class="fa fa-trash"></i></a>';
        $active_url_lang = '<a href="[ACTIVE_URL]" class="btn btn-success">'.$this->fromLang('questions_active').'&nbsp; <i class="fa fa-unlock"></i></a>';
        $inactive_url_lang = '<a href="[INACTIVE_URL]" class="btn btn-danger">'.$this->fromLang('questions_inactive').'&nbsp; <i class="fa fa-lock"></i></a>';
        while ($row = $this->db->fetch($result)) {
            $id = $row['questionid'];
            $date = $row['questionsdate'];
            $name = $row['name'];
            $active = ($row['active'] == 1) ? $this->fromLang('questions_yes') : $this->fromLang('questions_no');
           
            $links_url = '';
            $answers_url ='?' . $this->module_qs . '=questions&mod=answers&questionid=' . $id;
            $edit_url ='?' . $this->module_qs . '=questions&mod=questions&testid=' . $testid . '&questionid=' . $id;
            $active_url = '?' . $this->module_qs . '=questions&mod=questions&testid=' . $testid . '&questionid=' . $id . '&active=1';
            $inactive_url = '?' . $this->module_qs . '=questions&mod=questions&testid=' . $testid . '&questionid=' . $id . '&active=2';
			
			$links_url .= str_replace('[ANSWER_URL]', $answers_url, $answers_url_lang);
            
			if ((@$this->user->perm_string['2'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                //Perm for Edit
                $links_url .= str_replace('[EDIT_URL]', $edit_url, $edit_url_lang);
            }
            if ((@$this->user->perm_string['1'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                //Perm for Del
                $links_url .= str_replace('[ID]', $id, $del_url_lang);
            }
            if ((@$this->user->perm_string['2'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                //Perm for Edit
                if ($row['active'] == 1) {
                    $links_url .= str_replace('[ACTIVE_URL]', $active_url, $active_url_lang);
                } else {
                    $links_url .= str_replace('[INACTIVE_URL]', $inactive_url, $inactive_url_lang);
                }
            }
            $dataArr['data'][] = array(
                '<input type="checkbox" class="checkboxes" value="'.$id.'"/>',
                '<a href="'.$edit_url.'" title="'.$this->fromLang('questions_edit').'">'.$name.'</a>',
                $this->utils->formattedDate($date),
                $links_url
            );
        }


        $_SESSION['draw'] = @$_SESSION['draw'] + 1;
        $dataArr['draw'] = $_SESSION['draw'];
        $dataArr['recordsTotal'] = $nums;
        $dataArr['recordsFiltered'] = $recordsTotal;//$ndx;

		echo json_encode($dataArr);
	}
}

$index = new Index();
include $index->lg_folder.'/index.lang.php';
require_once $index->lg_folder.'/'.$index->module.'.lang.php';
$index->onLoad();

/******************* ajax_questions_list.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** ajax_questions_list.php ******************/;
