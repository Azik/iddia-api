<?php
/******************* slider_drag.php *******************
 *
 *
 * @author MicroPHP <info@microphp.com>
 * @web www.microphp.com
 * @copyright 2004-2019 MicroPHP
 *
 ******************** slider_drag.php ******************/

/** 
 * Define Namespace 
 */
namespace mcms5xx\admin;

define('iFolded', '../');
require_once iFolded.'m/classes/adminpage.class.php';
@$_GET['module'] = 'slider';

class Index extends \mcms5xx\classes\AdminPage
{
    public function __construct()
    {
        parent::__construct();
    }

    public function onLoad()
    {
        $this->buildPage();
    }

    public function buildPage()
    {
        $user_type = $this->user->GetCurrentUserTypeText();

        if (!$this->user->IsLogin()) {
            return;
        }

        if (($_SERVER['REQUEST_METHOD'] == 'POST') && ((@$this->user->perm_string['2'] == 1) || (@$this->user->perm_string['0'] == 1))) {
            $do_submit = $this->utils->UserPostInt('do_submit');
            if ($do_submit == 1) {
                $position = $this->utils->UserPostInt('position');
                $old_position = $this->utils->UserPostInt('old_position');
                $curr_id = $this->utils->UserPostInt('curr_id');
                $hitmode = $this->utils->UserPost('hitmode');

                switch ($hitmode) {

                    case 'before': {
                        /* B: Move before section */
                        $up_query = 'UPDATE '.$this->db->prefix.'slider
							SET 
								`position` = `position` - 2
							WHERE 
								(`position` > ' .$position.')
						';
                        $this->db->query($up_query);
                        /* $handle = @fopen("file.txt", "a+");
                        @fwrite($handle, "\n\n"."POST:".$up_query);
                        @fclose($handle); */

                        $this->db->query('UPDATE '.$this->db->prefix."slider
							SET 
								`position` = '" .($position - 1)."'
							WHERE 
								 (`sid` = " .$curr_id.')
							');

                        break;
                        /* E: Move before section */
                    }

                    case 'after': {
                        /* B: Move after section */
                        $up_query = 'UPDATE '.$this->db->prefix.'slider
							SET 
								`position` = `position` - 2
							WHERE 
								(`position` <= ' .($position + 1).')
						';
                        $this->db->query($up_query);
                        /* $handle = @fopen("file.txt", "a+");
                        @fwrite($handle, "\n\n"."POST:".$up_query);
                        @fclose($handle); */

                        $this->db->query('UPDATE '.$this->db->prefix."slider
							SET 
								`position` = '" .$position."'
							WHERE 
								 (`sid` = " .$curr_id.')
							');

                        break;
                        /* E: Move after section */
                    }
                }

                $this->orderSlider();
                echo time();

                /* $p = '';
                foreach ($_POST as $key=>$index) {
                    $p .= $key ."=>". $index ."\n";
                }
                $handle = @fopen("file.txt", "a+");
                @fwrite($handle, $p);
                @fclose($handle); */
            }
        }
    }
}

$index = new Index();
include $index->lg_folder.'/index.lang.php';

$index->onLoad();

/******************* slider_drag.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** slider_drag.php ******************/;
