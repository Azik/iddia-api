<?php
/******************* news_drag.php *******************
 *
 *
 * @author MicroPHP <info@microphp.com>
 * @web www.microphp.com
 * @copyright 2004-2019 MicroPHP
 *
 ******************** news_drag.php ******************/

/** 
 * Define Namespace 
 */
namespace mcms5xx\admin;

define('iFolded', '../');
require_once iFolded.'m/classes/adminpage.class.php';
@$_GET['module'] = 'news';

class Index extends \mcms5xx\classes\AdminPage
{
    public function __construct()
    {
        parent::__construct();
    }

    public function onLoad()
    {
        $this->buildPage();
    }

    public function buildPage()
    {
        $user_type = $this->user->GetCurrentUserTypeText();

        if (!$this->user->IsLogin()) {
            return;
        }

        if ((@$this->user->perm_string['2'] == 1) || (@$this->user->perm_string['0'] == 1)) {
            //Perm for Edit
            $this->upPosition();
        }

        /* $p = 'A';
        foreach ($_POST as $key=>$index) {
            $p .= $key ."=>". $index ."\n";
        }
        $handle = @fopen("file.txt", "a+");
        @fwrite($handle, $p);
        @fclose($handle); */
    }

    private function upPosition()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $do_submit = $this->utils->UserPostInt('do_submit');
            if ($do_submit == 1) {
                $catid = $this->utils->UserGetInt('catid');

                $new_position = $this->utils->UserPostInt('position');
                $old_position = $this->utils->UserPostInt('old_position');

                if ($old_position < $new_position) {
                    $ndx = 0;
                    $sql = 'SELECT catid, position
                    FROM ' .$this->db->prefix.'newscategories
                    WHERE position <= '.$new_position.'
                    ORDER BY position ASC';
                    $result = $this->db->query($sql);

                    /*$handle = @fopen("file.txt", "a+");
                    @fwrite($handle, $sql);
                    @fclose($handle);*/

                    while ($row = $this->db->fetch($result)) {
                        $up_query = 'UPDATE '.$this->db->prefix.'newscategories
                        SET
                            `position` = ' .$ndx.'
                        WHERE
                             (`catid`=' .$row['catid'].')
                        ';

                        /*$handle = @fopen("file.txt", "a+");
                        @fwrite($handle, $up_query);
                        @fclose($handle);*/

                        $this->db->query($up_query);
                        ++$ndx;
                    }
                } else {
                    $ndx = $new_position + 1;
                    $sql = 'SELECT catid, position
                    FROM ' .$this->db->prefix.'newscategories
                    WHERE position >= '.$new_position.'
                    ORDER BY position ASC';
                    $result = $this->db->query($sql);
                    /*$handle = @fopen("file.txt", "a+");
                    @fwrite($handle, $sql);
                    @fclose($handle);*/

                    while ($row = $this->db->fetch($result)) {
                        $up_query = 'UPDATE '.$this->db->prefix.'newscategories
                        SET
                            `position` = ' .$ndx.'
                        WHERE
                              (`catid`=' .$row['catid'].')
                        ';

                        /*$handle = @fopen("file.txt", "a+");
                        @fwrite($handle, $up_query);
                        @fclose($handle);*/

                        $this->db->query($up_query);
                        ++$ndx;
                    }
                }
                $curr_id = $this->utils->UserPostInt('curr_id');
                $up_query = 'UPDATE '.$this->db->prefix.'newscategories
							SET
								`position` = ' .$new_position.'
							WHERE
								(`catid` = ' .$curr_id.')
						';

                /*$handle = @fopen("file.txt", "a+");
                @fwrite($handle, $up_query);
                @fclose($handle);*/

                $this->db->query($up_query);

                $this->orderNewsCat();
            }
        }
        echo time();
    }

    private function upPosition1()
    {


        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $do_submit = $this->utils->UserPostInt('do_submit');
            if ($do_submit == 1) {
                $position = $this->utils->UserPostInt('position');
                $old_position = $this->utils->UserPostInt('old_position');
                $curr_id = $this->utils->UserPostInt('curr_id');
                $hitmode = $this->utils->UserPost('hitmode');

                if ((@$this->user->perm_string['2'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                    //Perm for Edit
                    switch ($hitmode) {

                        case 'before': {
                            /* B: Move before section */
                            /* $up_query = "UPDATE " . $this->db->prefix . "newscategories
                                SET
                                    `position` = `position`+1
                                WHERE
                                    (`position` > " . $position . ")
                                "; */
                            $up_query = 'UPDATE '.$this->db->prefix.'newscategories
							SET 
								`position` = `position` - 2
							WHERE 
								(`position` > ' .$position.')
						';
                            $this->db->query($up_query);
                            /* $handle = @fopen("file.txt", "a+");
                            @fwrite($handle, "\n\n"."POST:".$up_query);
                            @fclose($handle); */

                            $this->db->query('UPDATE '.$this->db->prefix."newscategories
							SET 
								`position` = '" .($position - 1)."'
							WHERE 
								 (`catid` = " .$curr_id.')
							');

                            $this->orderNewsCat();
                            break;
                            /* E: Move before section */
                        }

                        case 'after': {
                            /* B: Move after section */
                            $up_query = 'UPDATE '.$this->db->prefix.'newscategories
							SET 
								`position` = `position` - 2
							WHERE 
								(`position` <= ' .($position + 1).')
						';
                            $this->db->query($up_query);
                            /* $handle = @fopen("file.txt", "a+");
                            @fwrite($handle, "\n\n"."POST:".$up_query);
                            @fclose($handle); */

                            $this->db->query('UPDATE '.$this->db->prefix."newscategories
							SET 
								`position` = '" .$position."'
							WHERE 
								 (`catid` = " .$curr_id.')
							');

                            $this->orderNewsCat();
                            break;
                            /* E: Move after section */
                        }
                    }
                }
                /* $move_id = $this->utils->UserPostInt('move_id');
                $curr_id = $this->utils->UserPostInt('curr_id');
                $hitmode = $this->utils->UserPost('hitmode');

                 */
                /* $sort_order = $this->utils->UserPost('sort_order');
                $sortArr = explode(',', $sort_order);
                $pos_id = 0;
                echo time();
                foreach ($sortArr as $sid) {
                    $sid = (int)$sid;
                    if ($sid > 0) {
                        $this->db->query("UPDATE `" . $this->db->prefix . "newscategories`
                                SET
                                    `position`= " . (++$pos_id) . "
                                WHERE
                                    `catid`=" . $sid . " ");
                        //echo $sid.' - ';
                    }
                } */
                //print_r($sortArr);
                /* $p = '';
                foreach ($_POST as $key=>$index) {
                    $p .= $key ."=>". $index ."\n";
                }
                echo $p; */
            }
        }
        echo time();
    }


}

$index = new Index();
include $index->lg_folder.'/index.lang.php';

$index->onLoad();

/******************* news_drag.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** news_drag.php ******************/;
