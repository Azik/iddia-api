<?php
/******************* slider_activate.php *******************
 *
 *
 * @author MicroPHP <info@microphp.com>
 * @web www.microphp.com
 * @copyright 2004-2019 MicroPHP
 *
 ******************** slider_activate.php ******************/

/** 
 * Define Namespace 
 */
namespace mcms5xx\admin;

define('iFolded', '../');
require_once iFolded.'m/classes/adminpage.class.php';
@$_GET['module'] = 'slider';

class Index extends \mcms5xx\classes\AdminPage
{
    public function __construct()
    {
        parent::__construct();
    }

    public function onLoad()
    {
        if ((@$this->user->perm_string['2'] == 1) || (@$this->user->perm_string['0'] == 1)) {
            //Perm for Edit
            $this->buildPage();
        }
    }

    public function buildPage()
    {
        $user_type = $this->user->GetCurrentUserTypeText();

        if (!$this->user->IsLogin()) {
            return;
        }

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $do_submit = $this->utils->UserPostInt('do_submit');
            if ($do_submit == 1) {
                /* $move_id = $this->utils->UserPostInt('move_id');
                $curr_id = $this->utils->UserPostInt('curr_id');
                $hitmode = $this->utils->UserPost('hitmode');

                 */
                $id = $this->utils->UserPostInt('id');
                if ($id > 0) {
                    $active = ($this->utils->UserPost('checked') == 'false') ? 0 : 1;

                    $this->db->query('UPDATE `'.$this->db->prefix.'slider`
					SET 
						`active`= ' .$active.'
					WHERE 
						`sid`=' .$id.' ');
                }
                echo time();
                //print_r($sortArr);
                /* $p = '';
                foreach ($_POST as $key=>$index) {
                    $p .= $key ."=>". $index ."\n";
                }
                $handle = @fopen("file.txt", "a+");
                @fwrite($handle, $p);
                @fclose($handle); */
            }
        }
    }
}

$index = new Index();
include $index->lg_folder.'/index.lang.php';

$index->onLoad();

/******************* slider_activate.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** slider_activate.php ******************/;
