<?php
/******************* index.php *******************
 *
 * Admin Index file
 *
 * @author MicroPHP <info@microphp.com>
 * @web www.microphp.com
 * @copyright 2004-2019 MicroPHP
 *
 ******************** index.php ******************/

/** 
 * Define Namespace 
 */
namespace mcms5xx\admin;

define('iFolded', '../');
require_once iFolded.'m/classes/adminpage.class.php';

class indexAdmin extends \mcms5xx\classes\AdminPage
{
    public function __construct()
    {
        parent::__construct();
    }

    public function onLoad()
    {
        $this->buildLangs();

        $this->buildPage();

        $this->template->assign_block_vars('module_'.$this->module, array());
        
    }

    public function buildPage()
    {
        $user_type = $this->user->GetCurrentUserTypeText();

        $this->template->assign_vars(array(
            'MODULE_QS' => $this->module_qs,
            'TITLE' => $this->fromLangIndex('title'),
            'USER_RIGHT' => $this->fromLangIndex('user_right', $user_type),
            'LOGOUT' => $this->fromLangIndex('logout'),
            'YEAR' => date('Y'),
        ));

        $this->buildMenu();
    }
}

$index = new indexAdmin();
include $index->lg_folder.'/index.lang.php';
switch ($index->module) {
    case 'login': {
        require_once $index->lg_folder.'/'.$index->module.'.lang.php';
        $index->onLoad();
        $index->insertModule();
        $index->pageLoad($index->module);
        break;
    }
    default: {
        require_once $index->lg_folder.'/'.$index->module.'.lang.php';
        $index->onLoad();
        $index->template->pparse('index.header');
        $index->insertModule();
        $index->template->pparse('index.footer');
        $index->pageLoad($index->module);
        break;
    }
}
/******************* index.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** index.php ******************/;
