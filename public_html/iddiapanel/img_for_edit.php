<?php
/******************* img_for_edit.php *******************
 *
 *
 * @author MicroPHP <info@microphp.com>
 * @web www.microphp.com
 * @copyright 2004-2019 MicroPHP
 *
 ******************** img_for_edit.php ******************/

/** 
 * Define Namespace 
 */
namespace mcms5xx\admin;

define('iFolded', '../');
require_once iFolded.'m/classes/adminpage.class.php';
@$_GET['module'] = 'file_manager';

class Image extends \mcms5xx\classes\AdminPage
{
    public $w;
    public $h;

    public function __construct()
    {
        parent::__construct();
        $this->w = $this->fromConfig('portfolioResize');
    }

    public function onLoad()
    {
        @header('Content-Type: image/jpeg');
        $thumb_limit = $this->fromConfig('thumb_limit');
        $id = $this->utils->UserGetInt('i');
        $thumb_img = $this->curr_folder.'img/img_'.$this->w.'x'.$this->h.'.jpg';
        $query = 'SELECT * FROM '.$this->db->prefix."files WHERE (`category`='image') &&  (`fileid`=".$id.')';
        $result = $this->db->query($query);
        if ($row = $this->db->fetch($result)) {
            $folder = $this->io->dateFolder('img/img_'.$this->w.'x'.$this->h.'/', $row['add_time']);
            $thumb_img = $folder.'/'.$id.'.jpg';
            if ((!@is_file($thumb_img)) || (@filesize($thumb_img) < $thumb_limit)) {
                $filename = $row['filename'];
                $dir = '../'.$this->fromConfig('upload_folder');
                $dir .= 'image/';
                $img_file = $dir.$filename;
                if (strlen($img_file) > 10) {
                    list($original_width, $original_height, $src_t, $src_a) = getimagesize($img_file);

                    if ($this->w >= $original_width) {
                        $this->w = $new_w = $original_width;
                        $this->h = $new_h = $original_height;
                    } else {
                        $calcP = array($original_width, $this->w);
                        $calc_p = $this->utils->calc_perc($calcP['1'], $calcP['0']);
                        $this->w = $new_w = round(($original_width / $calc_p) * 100, 4);
                        $this->h = $new_h = round(($original_height / $calc_p) * 100, 4);
                    }

                    $image_p = imagecreatetruecolor($this->w, $this->h);

                    $extension = $row['extension'];
                    switch ($extension) {
                        case 'jpg':
                        case 'jpeg': {
                            $image = imagecreatefromjpeg($img_file);
                            break;
                        }
                        case 'gif': {
                            $image = imagecreatefromgif($img_file);
                            break;
                        }
                        case 'png': {
                            $image = imagecreatefrompng($img_file);
                            break;
                        }
                        default: {
                        $image = imagecreatefromjpeg($img_file);
                        break;
                        }
                    }
                    imagecopyresampled($image_p, $image, 0, -round(($new_h - $this->h) / 5, 2), 0, 0, $new_w, $new_h, $original_width, $original_height);

                    @imagejpeg($image_p, $thumb_img, 100);
                    @chmod($thumb_img, 0767);
                    @imagedestroy($image_p);
                }
            }
        }
        @header('location: '.$thumb_img);
    }
}

$image = new Image();
$image->onLoad();

/******************* img_for_edit.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** img_for_edit.php ******************/;
