<?php
/******************* ajax.photo.php *******************
 *
 * Select images
 *
 * @author MicroPHP <info@microphp.com>
 * @web www.microphp.com
 * @copyright 2004-2019 MicroPHP
 *
 ******************** ajax.photo.php ******************/

/** 
 * Define Namespace 
 */
namespace mcms5xx\admin;

define('iFolded', '../');
require_once iFolded.'m/classes/adminpage.class.php';
@$_GET['module'] = 'file_manager';

class indexAdmin extends \mcms5xx\classes\AdminPage
{
    protected $fileCount = 0;
    protected $search_str = '';

    public function __construct()
    {
        $this->curr_module = 'file_manager';
        parent::__construct();
    }

    public function onLoad()
    {
        $this->buildLangs();
        
        $this->buildPage();
    }

    private function buildPage()
    {
        $user_type = $this->user->GetCurrentUserTypeText();

        if (!$this->user->IsLogin()) {
            return;
        }

        $this->buildMenu();

        $this->listImages();
    }

    private function listImages()
    {
        $ajaxPageLimit = $this->fromConfig('ajaxPageLimit');
        $page = $this->utils->UserGetInt('page');

        if ($page <= 0) {
            $page = 1;
        }

        $select_field = $this->utils->UserGet('select_field');
        $img_id = $this->utils->UserGetInt('img_id');
        $this->template->assign_var('IMGID', $img_id);
        $this->template->assign_var('SELECT_FIELD', $select_field);
        $this->template->assign_block_vars('files', array(
            'SELECT_FIELD' => $select_field
        ));
        $page_start = ($page - 1) * $ajaxPageLimit;
        $page_end = $ajaxPageLimit;
        $cat = 'image';
        $query = "SELECT * FROM " . $this->db->prefix . "files";
        $query .= " WHERE `category` ='" . $cat . "'";
        if ($this->search_str != '') {
            $query .= " AND `originalname` REGEXP '" . $this->search_str . "'";
        }

        $w_sql = "";
        $keyword = $this->utils->UserGet('keyword');
        $this->template->assign_var('SEARCH_KEYWORD', $keyword);
        if (strlen($keyword)>2) {
            $w_sql = " AND (
					(`category` LIKE '%".$keyword."%')
				 OR (`extension` LIKE '%".$keyword."%')
				 OR (`filename` LIKE '%".$keyword."%')
			)
			";
        }
        $query .= $w_sql;
        $query .= ' ORDER BY `fileid` DESC';
        $upload_folder = $this->fromConfig('upload_folder');
        $image_thumb_folder = $this->fromConfig('image_thumb_folder');
        $this->fileCount = $this->db->num_rows($query);
        // PAgination
        if ($this->fileCount>=$ajaxPageLimit) {
            $this->template->assign_block_vars('files.pages', array());
            for ($p = 1; $p<ceil($this->fileCount/$ajaxPageLimit); $p++) {
                $curr_class = ($p==$page) ? : "btn-primary";
                $this->template->assign_block_vars('files.pages.pg', array(
                    'PAGE' => $p,
                    'CURR_CLASS' => $curr_class,
                ));
            }
        }
        $query .= $this->db->get_limit($page_start, $page_end);
        $result = $this->db->query($query);
        $ndx = 0;
        while ($row = $this->db->fetch($result)) {
            if (($ndx%6)==0) {
                $this->template->assign_block_vars('files.items', array());
            }
            ++$ndx;
            $category = $row['category'];
            $extension = $row['extension'];
            $file_dir = $this->io->dateFolder($upload_folder . $category . '/', $row['add_time']);
            $filename = $row['filename'];

            $thumb_dir = $this->io->dateFolder($upload_folder . $category. '/' . $image_thumb_folder . '/', $row['add_time']);
            $thumb_file = ($category == 'image') ? '../' . $thumb_dir . '/' . $filename : (is_file($this->admin_folder . 'img/ico/' . $extension . '.png') ? $this->admin_folder . 'img/ico/' . $extension . '.png' : $this->admin_folder . 'img/ico/file.png');
            $curr_style = ($row['fileid']==$img_id) ? ' style="border:4px solid #32c5d2;"' : '';
            $thumb = '<img src="' . $thumb_file . '" alt="" class="thumb" '.$curr_style.' />';
            $this->template->assign_block_vars('files.items.list', array(
                'ID' => $row['fileid'],
                'FILENAME' => '<a class="fancybox-button" data-rel="fancybox-button" title="' . $filename . '" href="../' . $file_dir . '/' . $filename . '" target="_blank">' . $filename . '</a>',
                'THUMB' => '<a class="fancybox-button" data-rel="fancybox-button" title="' . $filename . '" >' . $thumb . '</a>',
                'THUMB_IMG' => $thumb,
                'COPY_URL' => '/' . $file_dir . '/' . $filename,
                'ORIGINALNAME' => $row['originalname'],
                'EXTENSION' => $row['extension'],
                'TITLE' => $row['title'],
                'FILE_SHOW_NAME' => $filename,
                'STYLE' => $curr_style,
            ));

        }

    }

}

$index = new indexAdmin();
include $index->lg_folder.'/index.lang.php';
$index->onLoad();
$index->template->set_filenames(array('ajax.photo' => 'ajax.photo.tpl'));
$index->template->pparse('ajax.photo');

/******************* ajax.photo.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** ajax.photo.php ******************/;
