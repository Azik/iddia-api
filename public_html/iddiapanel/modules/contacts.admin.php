<?php
/******************* contacts.admin.php *******************
 *
 * Contacts admin module
 *
 * @author MicroPHP <info@microphp.com>
 * @web www.microphp.com
 * @copyright 2004-2019 MicroPHP
 *
 ******************** contacts.admin.php ******************/

/** 
 * Define Namespace 
 */
namespace mcms5xx\admin\modules;

/**
 * Checking if module included normally.
 */
if (!defined('mCMScheck')) {
    die('mCMS by MicroPHP');
}

class Contacts extends \mcms5xx\classes\AdminPage
{
    protected $message = '';

    public function __construct()
    {
        $this->curr_module = 'contacts';
        parent::__construct();
        $this->onLoad();
    }

    private function onLoad()
    {
        if ((@$this->user->perm_string['2'] == 1) || (@$this->user->perm_string['0'] == 1)) {
            //Perm for Edit
            $this->doAction();
        }
        $this->buildMenu();
        $this->buildPage();
    }

    private function doAction()
    {
        $action = $this->utils->Post('action');
        switch ($action) {
            case 'save_contactsoptions':
                $this->saveContactsOptions();
                break;
        }
    }

    private function saveContactsOptions()
    {
        $key = 'contacts_mail_to';
        $value = $this->utils->Post('txt_mail_to');
        $this->addKey($key, base64_encode($value));

        $key = 'contacts_mail_from';
        $value = $this->utils->Post('txt_mail_from');
        $this->addKey($key, base64_encode($value));

        $key = 'contacts_mail_user';
        $value = $this->utils->Post('txt_mail_user');
        $this->addKey($key, base64_encode($value));

        $key = 'contacts_mail_pass';
        $value = $this->utils->Post('txt_mail_pass');
        $this->addKey($key, base64_encode($value));

        $key = 'contacts_mail_pop3';
        $value = $this->utils->Post('txt_mail_pop3');
        $this->addKey($key, base64_encode($value));

        $key = 'contacts_mail_smtp';
        $value = $this->utils->Post('txt_mail_smtp');
        $this->addKey($key, base64_encode($value));

        $key = 'contacts_mail_host';
        $value = $this->utils->Post('txt_mail_host');
        $this->addKey($key, base64_encode($value));

        $this->user->logOperation($this->user->GetUserId(), 'contacts', 0, 'contacts_save');
    }

    private function buildPage()
    {
        $this->buildMain();
        $this->buildContactsOptions();
    }

    private function buildMain()
    {
        $this->template->assign_var('TITLE', $this->fromLang('title'));
        $this->template->assign_block_vars('options', array(
            'URL' => '?'.$this->module_qs.'=contacts',
            'SAVE' => $this->fromLang('save'),
            'CANCEL' => $this->fromLang('cancel'),
        ));
    }

    private function buildContactsOptions()
    {
        $readonly = ((@$this->user->perm_string['2'] == 1) || (@$this->user->perm_string['0'] == 1)) ? '' : ' readonly';
        $disabled_css = ((@$this->user->perm_string['2'] == 1) || (@$this->user->perm_string['0'] == 1)) ? '' : ' disabled';
        $this->template->assign_block_vars('options.contactsoptions', array(
            'MAIL_TO' => $this->fromLang('mail_to'),
            'MAIL_FROM' => $this->fromLang('mail_from'),
            'MAIL_USER' => $this->fromLang('mail_user'),
            'MAIL_PASS' => $this->fromLang('mail_pass'),
            'MAIL_POP3' => $this->fromLang('mail_pop3'),
            'MAIL_SMTP' => $this->fromLang('mail_smtp'),
            'MAIL_HOST' => $this->fromLang('mail_host'),

            'MAIL_TO_VALUE' => base64_decode($this->getKey('contacts_mail_to', 'aW5mb0BtaWNyb3BocC5jb20=')),
            'MAIL_FROM_VALUE' => base64_decode($this->getKey('contacts_mail_from', 'aW5mb0BtaWNyb3BocC5jb20=')),
            'MAIL_USER_VALUE' => base64_decode($this->getKey('contacts_mail_user', 'aW5mb0BtaWNyb3BocC5jb20=')),
            'MAIL_PASS_VALUE' => base64_decode($this->getKey('contacts_mail_pass', 'cGFzc3dvcmQ=')),
            'MAIL_POP3_VALUE' => base64_decode($this->getKey('contacts_mail_pop3', 'bWljcm9waHAuY29t')),
            'MAIL_SMTP_VALUE' => base64_decode($this->getKey('contacts_mail_smtp', 'bWljcm9waHAuY29t')),
            'MAIL_HOST_VALUE' => base64_decode($this->getKey('contacts_mail_host', 'bWljcm9waHAuY29t')),

            'READONLY' => $readonly,
            'DISABLED_CSS' => $disabled_css,
        ));
        if ((@$this->user->perm_string['2'] == 1) || (@$this->user->perm_string['0'] == 1)) {
            //Perm for Edit
            $this->template->assign_block_vars('options.contactsoptions.perm_edit', array());
        }
    }
}

$contacts = new Contacts();
$contacts->template->pparse('contacts');

/******************* contacts.admin.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** contacts.admin.php ******************/;
