<?php
/******************* manage_users.admin.php *******************
 *
 * Manage users admin module
 *
 * @author MicroPHP <info@microphp.com>
 * @web www.microphp.com
 * @copyright 2004-2019 MicroPHP
 *
 ******************** manage_users.admin.php ******************/

/** 
 * Define Namespace 
 */
namespace mcms5xx\admin\modules;

/**
 * Checking if module included normally.
 */
if (!defined('mCMScheck')) {
    die('mCMS by MicroPHP');
}

class manageUsers extends \mcms5xx\classes\AdminPage
{
    protected $eventsCount;
    protected $userid;
    protected $perid;
    protected $message = '';

    public function __construct()
    {
        $this->curr_module = 'manage_users';
        parent::__construct();
        $this->onLoad();
    }

    private function onLoad()
    {
        $this->buildMenu();
        if (@$this->user->perm_string['0'] == 1) {
            // Full perm
            $this->doAction();
            $this->buildPage();
        }
    }

    private function doAction()
    {
        $this->userid = $this->utils->UserGet('userid');
        $this->perid = $this->utils->UserGetInt('perid');

        $action = $this->utils->UserPost('action');
        $action_id = $this->utils->UserPostInt('action_id');

        switch ($action) {
            case 'delete':
                $this->deleteUser($action_id);
                break;
            case 'save':
                $this->saveUser();
                break;
            case 'save_per':
                $this->savePermission();
                break;
        }
    }

    private function saveUser()
    {
        $userid = $this->utils->UserPostInt('txt_id');
        $username = $this->utils->Post('txt_username');
        if ($this->user->ExistUser($username)) {
            if ($this->user->GetUserIdByUserName($username) != $userid) {
                $this->message = $this->fromLang('user_exist');

                return;
            }
        }
        if ($userid > 0) {
            $this->updateUser($userid);
        } else {
            $this->insertUser();
        }
        $this->utils->Redirect('?' . $this->module_qs . '=manage_users');
    }

    private function insertUser()
    {
        $username = $this->utils->Post('txt_username');
        $usertype = $this->utils->Post('txt_usertype');
        //$password = $this->utils->Post('txt_password');
        $password = hash('sha256', $this->utils->Post('txt_password'));
        $name = $this->utils->Post('txt_name');
        $email = $this->utils->Post('txt_email');
        $updated = time();

        $query = 'insert into ' . $this->db->prefix . 'users ';
        $query .= '(`username`, `usertype`, `password`, `name`, `surname`, `email`, `phone`, `mobile`, `updated`, `permissions`, `active`) ';
        $query .= "values('" . $username . "','" . $usertype . "','" . $password . "','" . $name . "','','" . $email . "', '', '', '" . $updated . "', '', 1)";

        $result = $this->db->query($query);
        $inserted_id = $this->db->insert_id();

        $this->user->logOperation($this->user->GetUserId(), 'manage_users', $inserted_id, 'insert_user');

        return $result;
    }

    private function updateUser($userid)
    {
        if (!$this->user->IsInType(1)) {
            $row = $this->user->GetUser($userid);
            if ($row['usertype'] == 1 || $row['usertype'] == 2) {
                return '';
            }
        }

        $username = $this->utils->Post('txt_username');
        $usertype = $this->utils->Post('txt_usertype');
        $txt_password = $this->utils->Post('txt_password');
        /*
        $password = sha1($this->utils->Post('txt_password')); */
        $password = hash('sha256', $this->utils->Post('txt_password'));
        $name = $this->utils->Post('txt_name');
        $email = $this->utils->Post('txt_email');
        $updated = time();

        $query = 'update ' . $this->db->prefix . 'users set ';
        $query .= "username='" . $username . "',";
        $query .= "usertype='" . $usertype . "',";
        if (strlen($txt_password) > 1) {
            $query .= "`password` = '" . $password . "', ";
        }
        $query .= "name='" . $name . "',";
        $query .= "email='" . $email . "',";
        $query .= "updated='" . $updated . "' ";
        $query .= ' where userid=' . $userid;

        $result = $this->db->query($query);
        $this->user->logOperation($this->user->GetUserId(), 'manage_users', $userid, 'update_user');
        return $result;
    }

    private function deleteUser($id)
    {
        $query = 'DELETE FROM ' . $this->db->prefix . 'users WHERE (`userid`=' . $id . ') && (`usertype`!=1) ';
        $result = $this->db->query($query);

        $this->user->logOperation($this->user->GetUserId(), 'manage_users', $id, 'delete_user');
        return $result;
    }

    private function savePermission()
    {
        $userid = $this->utils->Post('txt_id');
        $permissions = ';';

        $result = $this->db->query('SELECT * FROM ' . $this->db->prefix . 'modules WHERE have_admin=1 AND type=3');
        while ($row = $this->db->fetch($result)) {
            $id = $row['moduleid'];

            $full_right = $this->utils->UserPostInt('txt_full_right_' . $id);
            if ($full_right == 1) {
                $permissions .= $id . '::11111;';
            } else {
                $right_txt = '00000';
                $del_right = $this->utils->UserPostInt('txt_del_right_' . $id);
                $right_txt['1'] = ($del_right == 1) ? '1' : '0';
                $edit_right = $this->utils->UserPostInt('txt_edit_right_' . $id);
                $right_txt['2'] = ($edit_right == 1) ? '1' : '0';
                $add_right = $this->utils->UserPostInt('txt_add_right_' . $id);
                $right_txt['3'] = ($add_right == 1) ? '1' : '0';
                $view_right = $this->utils->UserPostInt('txt_view_right_' . $id);
                $right_txt['4'] = (($view_right == 1) || ($add_right == 1) || ($edit_right == 1) || ($del_right == 1)) ? '1' : '0';
                $permissions .= $id . '::' . $right_txt . ';';
            }
            /*$checked = $this->utils->UserPostInt('txt_right_' . $id);
            if ($checked == 1)
                $permissions .= $id . ';';*/
        }

        $query = 'UPDATE ' . $this->db->prefix . 'users SET ';
        $query .= " `permissions` = '" . $permissions . "' ";
        $query .= ' WHERE
            `userid` = ' . $userid;

        $this->db->query($query);
        $this->user->logOperation($this->user->GetUserId(), 'manage_users', $userid, 'save_permission');
        $this->utils->Redirect('?' . $this->module_qs . '=manage_users');
    }

    private function buildPage()
    {
        $this->buildMain();

        if (is_numeric($this->userid) && $this->userid != 0) {
            $active = $this->utils->UserGetInt('active');
            if ($active > 0) {
                $activeSql = ($active == 1) ? ' `active`=0' : ' `active`=1';
                $query = 'UPDATE ' . $this->db->prefix . 'users SET ' . $activeSql . ' WHERE `userid`=' . $this->userid . ' ';
                $this->db->query($query);
                $this->utils->Redirect('?' . $this->module_qs . '=manage_users');
            } else {
                $this->buildEditMode();
            }
        } elseif (is_numeric($this->perid) && $this->perid != 0) {
            $this->buildPermissions();
        } else {
            if (strlen($this->userid) > 5) {
                $actArr = explode(':', $this->userid);
                $activeSql = ($actArr['0'] == 'activate') ? ' `active`=1' : ' `active`=0';
                $ids = (strlen($actArr['1']) > 0) ? ' `userid` IN ( ' . $actArr['1'] . ' )' : ' `userid`=0';
                $query = 'UPDATE ' . $this->db->prefix . 'users SET ' . $activeSql . ' WHERE ' . $ids . '';
                $this->db->query($query);
                $this->utils->Redirect('?' . $this->module_qs . '=manage_users');
            } else {
                $this->buildList();
            }
        }
    }

    private function buildMain()
    {
        $this->template->assign_var('TITLE', $this->fromLang('title'));
        $this->template->assign_var('DELETE_CONFIRM', $this->fromLang('delete_confirm'));
        $this->template->assign_block_vars('users', array());
    }

    private function buildList()
    {
        $curr_userid = $this->user->GetUserId();
        $this->template->assign_block_vars('users.list', array(
            'USERNAME' => $this->fromLang('username'),
            'USERTYPE' => $this->fromLang('usertype'),
            'ACTIVE' => $this->fromLang('users_active'),
            'INACTIVE' => $this->fromLang('users_inactive'),
            'ACTIVATE' => $this->fromLang('users_activate'),
            'INACTIVATE' => $this->fromLang('users_inactivate'),
            'EMAIL' => $this->fromLang('email'),
            'URL' => '?' . $this->module_qs . '=manage_users',
            'UPDATED' => $this->fromLang('updated'),
            'ADD' => $this->fromLang('add'),
            'EDIT' => $this->fromLang('edit'),
            'DELETE' => $this->fromLang('delete'),
            'PERMISSIONS' => $this->fromLang('permissions'),
        ));

        $sql = 'SELECT * FROM ' . $this->db->prefix . 'users';
        if (!$this->user->IsInType(1)) {
            $sql .= ' WHERE usertype<>1 AND usertype<>2';
        }
        $result = $this->db->query($sql);
        while ($row = $this->db->fetch($result)) {
            $id = $row['userid'];
            $username = $row['username'];
            $name = $row['name'];
            $password = $row['password'];
            $updated = $this->site->getFormattedDate($row['updated']);
            $usertype = $this->user->GetUserTypeText($row['usertype']);
            $email = $row['email'];

            $this->template->assign_block_vars('users.list.items', array(
                'ID' => $id,
                'USERNAME' => $username,
                'USERTYPE' => $usertype,
                'EMAIL' => $email,
                'UPDATED' => $updated,
                'ACTIVE_URL' => '?' . $this->module_qs . '=manage_users&userid=' . $id . '&active=1',
                'INACTIVE_URL' => '?' . $this->module_qs . '=manage_users&userid=' . $id . '&active=2',
            ));

            if ($row['usertype'] == 3) {
                $this->template->assign_block_vars('users.list.items.permission', array());
                $this->template->assign_block_vars('users.list.items.check', array());
                $this->template->assign_block_vars('users.list.items.do_del', array());
                if ($row['active'] == 1) {
                    $this->template->assign_block_vars('users.list.items.active', array());
                } else {
                    $this->template->assign_block_vars('users.list.items.inactive', array());
                }
            }
        }
    }

    private function buildEditMode()
    {
        $id = $this->userid;
        $username = '';
        $password = '';
        $name = '';
        $email = '';
        $usertype_options = $this->user->GetUserTypeOptions();

        $result = $this->db->query('SELECT * FROM ' . $this->db->prefix . "users WHERE userid='" . $id . "'");
        if ($row = $this->db->fetch($result)) {
            $username = $row['username'];
            $password = $row['password'];
            $name = $row['name'];
            $email = $row['email'];
            $usertype_options = $this->user->GetUserTypeOptions($row['usertype']);
        }

        $this->template->assign_block_vars('users.edit', array(
            'ID' => $id,
            'USERNAME' => $this->fromLang('username'),
            'USERNAME_VALUE' => $username,
            'PASSWORD' => $this->fromLang('password'),
            'PASSWORD_VALUE' => '',
            'USERTYPE' => $this->fromLang('usertype'),
            'USERTYPE_OPTIONS' => $usertype_options,
            'NAME' => $this->fromLang('name'),
            'NAME_VALUE' => $name,
            'EMAIL' => $this->fromLang('email'),
            'EMAIL_VALUE' => $email,
            'SAVE' => $this->fromLang('save'),
            'CANCEL' => $this->fromLang('cancel'),
            'URL' => '?' . $this->module_qs . '=manage_users',
        ));

        if ($this->message != '') {
            $this->template->assign_block_vars('users.edit.alert', array(
                'MESSAGE' => $this->message,));
        }
    }

    private function buildPermissions()
    {
        $id = $this->perid;

        $perms = $this->user->GetUserPermissions($id);
        $username = $perms['username'];

        $this->template->assign_block_vars('users.permissions', array(
            'ID' => $id,
            'PERMISSIONS' => $this->fromLang('permissions'),
            'USERNAME' => $this->fromLang('username'),
            'USERNAME_VALUE' => $username,
            'MODULE' => $this->fromLang('module'),
            'RIGHT' => $this->fromLang('right'),
            'RIGHT_FULL' => $this->fromLang('right_full'),
            'RIGHT_DEL' => $this->fromLang('right_del'),
            'RIGHT_EDIT' => $this->fromLang('right_edit'),
            'RIGHT_ADD' => $this->fromLang('right_add'),
            'RIGHT_VIEW' => $this->fromLang('right_view'),
            'SAVE' => $this->fromLang('save'),
            'CANCEL' => $this->fromLang('cancel'),
            'URL' => '?' . $this->module_qs . '=manage_users',
        ));

        $result = $this->db->query("SELECT
            M.*,
            ML.`title`
        FROM `" . $this->db->prefix . "modules` M
        INNER JOIN `" . $this->db->prefix . "modulelocalizations` ML ON ML.`moduleid` = M.`moduleid` AND ML.`lang` = '" . $this->lang . "'
        WHERE 
            (M.`have_admin` = 1)
        AND (M.`type` = 3) ");
        while ($row = $this->db->fetch($result)) {
            $id = $row['moduleid'];
            $name = $row['name'];
            $description = $row['description'];
            $title = $row['title'];
            
            $checked_full = $checked_del = $checked_edit = $checked_add = $checked_view = '';
            if (array_key_exists($id, $perms)) {
                $checked_full = (@$perms[$id]['0'] == 1) ? ' checked' : '';
                $checked_del = (@$perms[$id]['1'] == 1) ? ' checked' : '';
                $checked_edit = (@$perms[$id]['2'] == 1) ? ' checked' : '';
                $checked_add = (@$perms[$id]['3'] == 1) ? ' checked' : '';
                $checked_view = (@$perms[$id]['4'] == 1) ? ' checked' : '';
            }

            $this->template->assign_block_vars('users.permissions.items', array(
                'ID' => $id,
                'MDULE_NAME' => $name,
                'TITLE' => $title,
                'CHECKED_FULL' => $checked_full,
                'CHECKED_DEL' => $checked_del,
                'CHECKED_EDIT' => $checked_edit,
                'CHECKED_ADD' => $checked_add,
                'CHECKED_VIEW' => $checked_view,
            ));

        }

        if ($this->message != '') {
            $this->template->assign_block_vars('users.permissions.alert', array(
                'MESSAGE' => $this->message,));
        }
    }

}

$manage_users = new manageUsers();
$manage_users->template->pparse('manage_users');

/******************* manage_users.admin.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** manage_users.admin.php ******************/;
