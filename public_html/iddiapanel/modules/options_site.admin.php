<?php
/******************* options_site.admin.php *******************
 *
 * Site Options admin module
 *
 * @author MicroPHP <info@microphp.com>
 * @web www.microphp.com
 * @copyright 2004-2019 MicroPHP
 *
 ******************** options_site.admin.php ******************/

/** 
 * Define Namespace 
 */
namespace mcms5xx\admin\modules;

/**
 * Checking if module included normally.
 */
if (!defined('mCMScheck')) {
    die('mCMS by MicroPHP');
}

class optionsAdmin extends \mcms5xx\classes\AdminPage
{
    protected $mode;
    protected $message = '';

    public function __construct()
    {
        $this->curr_module = 'options_site';
        parent::__construct();
        $this->onLoad();
    }

    private function onLoad()
    {
        $this->buildMenu();
        $this->mode = $this->utils->Get('mode');

        if ((@$this->user->perm_string['2'] == 1) || (@$this->user->perm_string['0'] == 1)) {
            //Perm for Edit
            $this->doAction();
        }
        $this->buildPage();
    }

    private function doAction()
    {
        $action = $this->utils->Post('action');
        $action_id = $this->utils->Post('action_id');

        switch ($action) {
            case 'save_siteoptions':
                $this->saveSiteOptions();
                $this->user->logOperation($this->user->GetUserId(), 'options_site', 0, 'update');
                break;
        }
    }

    private function saveSiteOptions()
    {
        $opt_key = 'charset';
        $value = $this->utils->Post('txt_charset');
        $this->addKey($opt_key, $value);

        $opt_key = 'slider';
        $value = $this->utils->UserPostInt('txt_slider');
        $this->addKey($opt_key, $value);

        $opt_key = 'parallax';
        $value = $this->utils->UserPostInt('txt_parallax');
        $this->addKey($opt_key, $value);

        foreach ($this->langs as $for_key => $value) {
            $lang = $value;
            $opt_key = 'site_name';
            $value = $this->utils->Post('txt_site_name_' . $lang);
            $this->addKeyLang($opt_key, $lang, $value);

            $opt_key = 'site_title';
            $value = $this->utils->Post('txt_site_title_' . $lang);
            $this->addKeyLang($opt_key, $lang, $value);

            $opt_key = 'meta_keywords';
            $value = $this->utils->Post('txt_meta_keywords_' . $lang);
            $this->addKeyLang($opt_key, $lang, $value);

            $opt_key = 'meta_description';
            $value = $this->utils->Post('txt_meta_description_' . $lang);
            $this->addKeyLang($opt_key, $lang, $value);

            $opt_key = 'meta_email';
            $value = $this->utils->Post('txt_meta_email_' . $lang);
            $this->addKeyLang($opt_key, $lang, $value);

            $opt_key = 'meta_author';
            $value = $this->utils->Post('txt_meta_author_' . $lang);
            $this->addKeyLang($opt_key, $lang, $value);

            $opt_key = 'site_copyright';
            $value = $this->utils->Post('txt_site_copyright_' . $lang);
            $this->addKeyLang($opt_key, $lang, $value);
        }

        $langs = $_POST['txt_langs'];
        $langs = (is_array($langs)) ? array_unique($langs) : array('en');

        foreach ($langs as $langFolder) {
            if (!is_dir($this->lang_folder . $langFolder . '/')) {
                //echo "Not found: ".$this->lang_folder.$langFolder."/\n";
                $this->io->recurseCopy($this->lang_folder . 'en/', $this->lang_folder . $langFolder . '/');
            }
        }
        //print_r($langs);exit();
        $showlangs = $_POST['txt_showlangs'];
        $mainlang = $_POST['txt_mainlang'];
        $txt_adminlang = $_POST['txt_adminlang'];

        $langs = implode(',', $langs);
        $showlangs = implode(',', $showlangs);

        $opt_key = 'lang_langs';
        $value = $langs;
        $this->addKey($opt_key, $value);

        $opt_key = 'lang_showlangs';
        $value = $showlangs;
        $this->addKey($opt_key, $value);

        $opt_key = 'lang_mainlang';
        $value = $mainlang;
        $this->addKey($opt_key, $value);

        $opt_key = 'lang_adminmainlang';
        $value = $txt_adminlang;
        $this->addKey($opt_key, $value);

        $this->utils->Redirect('?' . $this->module_qs . '=options_site&mode=siteoptions');
    }

    private function buildPage()
    {
        $this->buildMain();

        switch ($this->mode) {
            case 'siteoptions': {
                $this->buildSiteOptions();
                break;
			}
            default: {
                $this->buildSiteOptions();
				break;
			}
        }
    }

    private function buildMain()
    {
        $readonly = ((@$this->user->perm_string['2'] == 1) || (@$this->user->perm_string['0'] == 1)) ? '' : ' readonly';
        $disabled_css = ((@$this->user->perm_string['2'] == 1) || (@$this->user->perm_string['0'] == 1)) ? '' : ' disabled';
        $disabled_type = ((@$this->user->perm_string['2'] == 1) || (@$this->user->perm_string['0'] == 1)) ? '' : ' disabled';
        $this->template->assign_var('TITLE', $this->fromLang('title'));
        $this->template->assign_var('READONLY', $readonly);
        $this->template->assign_var('DISABLED_CSS', $disabled_css);
        $this->template->assign_var('DISABLED_TYPE', $disabled_type);
        $this->template->assign_block_vars('options', array(
            'URL' => '?' . $this->module_qs . '=options_site',
            'SAVE' => $this->fromLang('save'),
            'CANCEL' => $this->fromLang('cancel'),
        ));
    }

    private function buildDefault()
    {
        $this->template->assign_block_vars('options.default', array(
            'TITLE' => $this->fromLang('title'),
            'SITEOPTIONS' => $this->fromLang('siteoptions'),
            'PROFILE' => $this->fromLang('profile'),
            'PASSWORD' => $this->fromLang('password'),
        ));
    }

    private function buildSiteOptions()
    {
        $slider_check = ($this->getKey('slider', '1') == 1) ? ' checked' : '';
        $parallax_check = ($this->getKey('parallax', '0') == 1) ? ' checked' : '';
        $this->template->assign_block_vars('sub_link', array(
            'MODE' => 'siteoptions',
            'TEXT' => $this->fromLang('siteoptions'),
        ));
        $this->template->assign_block_vars('options.siteoptions', array(
            'TITLE' => $this->fromLang('siteoptions'),
            'CHARSET' => $this->fromLang('charset'),
            'CHARSET_VALUE' => $this->getKey('charset', 'UTF-8'),
            'SLIDER' => $this->fromLang('slider'),
            'SLIDER_CHECK' => $slider_check,
            'PARALLAX' => $this->fromLang('parallax'),
            'PARALLAX_CHECK' => $parallax_check,
            'META' => $this->fromLang('meta'),
            'SITE_NAME' => $this->fromLang('site_name'),
            'SITE_TITLE' => $this->fromLang('site_title'),
            'META_KEYWORDS' => $this->fromLang('meta_keywords'),
            'META_DESCRIPTION' => $this->fromLang('meta_description'),
            'META_EMAIL' => $this->fromLang('meta_email'),
            'META_AUTHOR' => $this->fromLang('meta_author'),
            'SITE_COPYRIGHT' => $this->fromLang('site_copyright'),
            'LANG' => $this->fromLang('lang'),
            'SITE_LANG' => $this->fromLang('site_lang'),
            'ADMIN_LANG' => $this->fromLang('admin_lang'),
            'ADMIN_LANG_MAIN' => $this->fromLang('admin_lang_main'),
            'LANG_MAIN' => $this->fromLang('lang_main'),
            'LANG_SHOW' => $this->fromLang('lang_show'),
            'LANG_LANG' => $this->fromLang('lang_lang'),
        ));

        $ndx = 0;
        foreach ($this->langs as $opt_key => $value) {
            ++$ndx;
            $lang = $value;
            $class = ($ndx == 1) ? ' class="active"' : '';
            $fade_class = ($ndx == 1) ? ' in active' : '';
            $this->template->assign_block_vars('options.siteoptions.tab', array(
                'LANG' => $lang,
                'CLASS' => $class,
                'FADE_CLASS' => $fade_class,
                'SITE_NAME_VALUE' => $this->utils->formData($this->getKeyLang('site_name', $lang, '')),
                'SITE_TITLE_VALUE' => $this->utils->formData($this->getKeyLang('site_title', $lang, '')),
                'META_KEYWORDS_VALUE' => $this->utils->formData($this->getKeyLang('meta_keywords', $lang, '')),
                'META_DESCRIPTION_VALUE' => $this->utils->formData($this->getKeyLang('meta_description', $lang, '')),
                'META_EMAIL_VALUE' => $this->utils->formData($this->getKeyLang('meta_email', $lang, '')),
                'META_AUTHOR_VALUE' => $this->utils->formData($this->getKeyLang('meta_author', $lang, '')),
                'SITE_COPYRIGHT_VALUE' => $this->utils->formData($this->getKeyLang('site_copyright', $lang, '')),
            ));
        }

        $langs = $this->getKey('lang_langs', 'en');
        $showlangs = $this->getKey('lang_showlangs', 'en');
        $mainlang = $this->getKey('lang_mainlang', 'en');
        $admin_lang = $this->fromConfig('admin_lang');
        $admin_mainlang = $this->getKey('lang_adminmainlang', $admin_lang);

        $langs = explode(',', $langs);
        $showlangs = explode(',', $showlangs);

        foreach ($langs as $opt_key => $lang) {
            $main = '';
            $show = '';
            if ($lang == $mainlang) {
                $main = 'checked';
            }
            if (in_array($lang, $showlangs)) {
                $show = 'checked';
            }

            $this->template->assign_block_vars('options.siteoptions.langs', array(
                'LANG' => $lang,
                'MAIN' => $main,
                'SHOW' => $show,
            ));
            $admin_main_lang_check = ($admin_mainlang == $lang) ? 'checked' : '';
            $this->template->assign_block_vars('options.siteoptions.langs_admin', array(
                'LANG' => $lang,
                'ADMIN_MAIN_LANG_CHECK' => $admin_main_lang_check,
            ));
        }
    }

}

$optionsAdmin = new optionsAdmin();
$optionsAdmin->template->pparse('options_site');

/******************* options_site.admin.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** options_site.admin.php ******************/;
