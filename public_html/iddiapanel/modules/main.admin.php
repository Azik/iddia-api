<?php
/******************* main.admin.php *******************
 *
 * Dashboard, main admin module
 *
 * @author MicroPHP <info@microphp.com>
 * @web www.microphp.com
 * @copyright 2004-2019 MicroPHP
 *
 ******************** main.admin.php ******************/

/** 
 * Define Namespace 
 */
namespace mcms5xx\admin\modules;

/**
 * Checking if module included normally.
 */
if (!defined('mCMScheck')) {
    die('mCMS by MicroPHP');
}

class MainAdmin extends \mcms5xx\classes\AdminPage
{
    public $colorArr = array();
    public $nw_number_x = 0;
    public $nw_desc_x = 0;
    public $sm_number_x = 0;
    public $sm_desc_x = 0;
    public $fe_number_x = 0;
    public $fe_desc_x = 0;
    public $fm_number_x = 0;
    public $fm_desc_x = 0;
    public $md_number_x = 0;
    public $md_desc_x = 0;

    public function __construct()
    {
        parent::__construct();
        $this->onLoad();
    }

    private function onLoad()
    {
        $this->template->assign_vars(array(
            'TITLE' => $this->fromLang('title'),
            'TITLE_INFO' => $this->fromLang('title_info'),
        ));
        $this->buildMenu();
        $this->dashboardStats();
    }

    private function dashboardStats()
    {
        if (!$this->user->IsLogin()) {
            return;
        }

        $this->colorArr = $this->fromConfig('colorArr');
        $this->colorArr = $this->utils->array_random($this->colorArr);

        $double_check = 0;
        $color_idx = 0;
        $result = $this->db->query('SELECT M.*, ML.`title`
		FROM ' .$this->db->prefix.'modules M
		INNER JOIN ' .$this->db->prefix."modulelocalizations ML ON ML.`moduleid` = M.`moduleid` AND ML.lang='".$this->lang."'
		WHERE (M.`have_admin`=1) && (M.`active`=1) ORDER BY M.position");
        //$result = $this->db->query("SELECT * FROM " . $this->db->prefix . "modules WHERE (`have_admin`=1) && (`active`=1) ORDER BY position");
        while ($row = $this->db->fetch($result)) {
            $double_class = '';
            if (count($this->colorArr) == $color_idx) {
                $color_idx = 0;
                $this->colorArr = $this->utils->array_random($this->colorArr);
            }
            if (($double_check % 4) == 0) {
                //$double_class = ' double';
                $double_check += 2;
            } else {
                ++$double_check;
            }
            $id = $row['moduleid'];
            $name = $row['name'];
            $type = $row['type'];
            $description = $row['description'];
            $title = $row['title'];

            /* $result_loc = $this->db->query("SELECT * FROM " . $this->db->prefix . "modulelocalizations WHERE moduleid='" . $id . "' AND lang='" . $this->lang . "'");
            if ($row_loc = $this->db->fetch($result_loc)) {
                $title = $row_loc['title'];
            } */
            $is_right = false;
            switch ($type) {
                case 1:
                    if ($this->user->IsInType(1)) {
                        $is_right = true;
                    }
                    break;
                case 2:
                    if ($this->user->IsInTypes('1,2')) {
                        $is_right = true;
                    }
                    break;
                default:
                    $userid = $this->user->GetUserId();
                    $perms = $this->user->GetUserPermissions($userid);
                    if ($this->user->IsInTypes('1,2') || (array_key_exists($id, $perms) && ($perms[$id] > 0))) {
                        $is_right = true;
                    }
                    break;
            }

            if ($is_right) {
                $menu_url = 'index.php?'.$this->module_qs.'='.$name;

                if ($name != 'options') {
                    $number = '';
                    if (strlen($row['tables']) > 0) {
                        $table_nms = explode(',', $row['tables']);
                        if (count($table_nms) > 0) {
                            $cnt_sql = $this->db->query('SELECT count(*) AS count FROM '.$this->db->prefix.$table_nms['0'].$this->db->get_limit(0, 1));
                            if ($cnt_row = $this->db->fetch($cnt_sql)) {
                                $number = $cnt_row['count'];
                            }
                        }
                    }
                    $this->template->assign_block_vars('dashboard_stats', array(
                        'COLOR' => $this->colorArr[$color_idx].$double_class,
                        'COLOR_NAME' => $this->colorArr[$color_idx],
                        'CLASS_FA' => $row['icon'],
                        'URL' => $menu_url,
                        'NAME' => $title,
                        'DESCRIPTION' => $description,
                        'NUMBER_INT' => (int) $number,
                        'NUMBER' => $number,
                    ));

                    $view_type = $row['view_type'];
                    switch ($view_type) {
                        case 'tile':{
                            $this->template->assign_block_vars('dashboard_stats.tile', array());
                            $this->template->assign_block_vars('dashboard_stats.tile.corner', array());
                            if ($number > 0) {
                                $this->template->assign_block_vars('dashboard_stats.tile.number', array());
                            } else {
                                $this->template->assign_block_vars('dashboard_stats.tile.txt', array());
                            }
                            break;
                        }
                        case 'tile2x':{
                            $this->template->assign_block_vars('dashboard_stats.tile2x', array());
                            if ($number > 0) {
                                $this->template->assign_block_vars('dashboard_stats.tile2x.number', array());
                            } else {
                                $this->template->assign_block_vars('dashboard_stats.tile2x.txt', array());
                            }
                            break;
                        }
                        case 'gallery':{
                            $this->template->assign_block_vars('dashboard_stats.gallery', array());
                            if ($number > 0) {
                                $this->template->assign_block_vars('dashboard_stats.gallery.number', array());
                            } else {
                                $this->template->assign_block_vars('dashboard_stats.gallery.txt', array());
                            }
                            break;
                        }
                        case 'stat1x':{
                            $this->template->assign_block_vars('dashboard_stats.stat1x', array());
                            if ($number > 0) {
                                $this->template->assign_block_vars('dashboard_stats.stat1x.number', array());
                            } else {
                                $this->template->assign_block_vars('dashboard_stats.stat1x.txt', array());
                            }
                            break;
                        }
                        default:{
                            $this->template->assign_block_vars('dashboard_stats.stat', array());
                            if ($number > 0) {
                                $this->template->assign_block_vars('dashboard_stats.stat.number', array());
                            } else {
                                $this->template->assign_block_vars('dashboard_stats.stat.txt', array());
                            }
                            break;
                        }
                    }

                    switch ($name) {
                        // Case for NEWS
                        case 'news': {
                            if ($this->user->hasPermission('news')) {
                                $this->newsChart(1);
                            }
                            break;
                        }
                    }
                }
            }
            ++$color_idx;
        }

        $this->colorArr = $this->utils->array_random($this->colorArr);
        // Help
        $this->template->assign_block_vars('dashboard_stats', array(
            'COLOR' => $this->colorArr['0'],
            'COLOR_NAME' => $this->colorArr['0'],
            'CLASS_FA' => 'fa fa-question',
            'URL' => 'help.php',
            'NAME' => $this->fromLangIndex('help'),
        ));
        $this->template->assign_block_vars('dashboard_stats.tile', array());
        $this->template->assign_block_vars('dashboard_stats.tile.txt', array());

        // Log out
        $this->template->assign_block_vars('dashboard_stats', array(
            'COLOR' => $this->colorArr['1'],
            'COLOR_NAME' => $this->colorArr['1'],
            'CLASS_FA' => 'fa fa-power-off',
            'URL' => 'index.php?'.$this->module_qs.'=action&act=logout',
            'NAME' => $this->fromLangIndex('logout'),
        ));
        $this->template->assign_block_vars('dashboard_stats.tile', array());
        $this->template->assign_block_vars('dashboard_stats.tile.txt', array());

        // Last log
        $this->lastLog();
    }

    private function newsChart($ndx = 0)
    {
        $this->template->assign_block_vars('news_stats', array());

        $this->template->assign_vars(array(
            'NEWS_STATISTICS' => $this->fromLang('news_statistics'),
            'NEWS_TEXT' => $this->fromLang('news'),
            'NEWS_URL' => '?'.$this->module_qs.'=news',
            'NEWS_STATISTICS_COUNT' => $this->fromLang('news_statistics_count'),
            'NEWS_STATISTICS_INFO' => $this->fromLang('news_statistics_info'),
        ));
        $this->template->assign_block_vars('news_charts', array());
        $today = strtotime(date('d').' '.date('F').' '.date('Y').'');
        $days = 31;
        for ($d = 0; $d < $days; ++$d) {
            //for ($d=31; $d>0; $d--) {
            /* $curr_beg = $today - $d*86400; */
            /* $curr_end = $today - $d*86400 + 86399; */
            $curr_end = $today - 30 * 86400 + $d * 86400 + 86399;
            $mdt = date('d', $curr_end);
            $day_sql = ($d == ($days - 1)) ? 'SELECT count(`newsid`) as count
				FROM ' .$this->db->prefix.'news' : 'SELECT count(`newsid`) as count
				FROM ' .$this->db->prefix.'news
				WHERE 
					`newsdate` <= ' .$curr_end.'';
            $day_result = $this->db->query($day_sql);
            /* echo($day_sql.'<br/>'); */
            $cnt = 0;
            if ($day_row = $this->db->fetch($day_result)) {
                $cnt = $day_row['count'];
            }
            $this->template->assign_block_vars('news_charts.newscount', array(
                //'DAY' => date('j', $curr_end),
                'DAY' => $d,
                'COUNT' => $cnt,
            ));
            /* $this->template->assign_block_vars('news_charts.newsvisitors', array(
                'DAY' => $d,
                'COUNT' => $cnt+$d,
            )); */
        }
    }

    private function lastLog()
    {
        $type = $this->user->GetUserType();
        $curr_userid = $this->user->GetUserId();
        $d_date_format = $this->fromConfig('d_date_format');

        $this->template->assign_block_vars('last_log', array(
            'TITLE' => $this->fromLangIndex('title_logs'),
        ));
        $where_sql = ($type > 2) ? ' WHERE ( L.user_id = '.$curr_userid.') ' : '';
        $sql = 'SELECT
              L.*,
              U.username
			FROM ' .$this->db->prefix.'logs L
			LEFT JOIN ' .$this->db->prefix.'users U ON U.userid = L.user_id
			        '.$where_sql.'
			ORDER BY L.`date_int` DESC
			'.$this->db->get_limit(0, 20);
        $result = $this->db->query($sql);
        while ($row = $this->db->fetch($result)) {
            $operation_mode = $row['operation_mode'];
            $operation_mode = (!is_null($this->fromLangIndex('logs_'.$operation_mode))) ? $this->fromLangIndex('logs_'.$operation_mode) : $operation_mode;
            $user = ($row['user_id'] == 0) ? $this->fromLangIndex('nouser_logs') : $row['username'];
            $this->template->assign_block_vars('last_log.items', array(
                'MODE' => $operation_mode,
                'USER' => $user,
                'DATE' => date($d_date_format, $row['date_int']),
            ));
        }
    }
}

$main = new MainAdmin();
$main->template->pparse('main');

/******************* main.admin.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** main.admin.php ******************/;
