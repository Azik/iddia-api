<?php
/******************* gallery_manager.admin.php *******************
 *
 * Gallery manager admin module
 *
 * @author MicroPHP <info@microphp.com>
 * @web www.microphp.com
 * @copyright 2004-2019 MicroPHP
 *
 ******************** gallery_manager.admin.php ******************/

/** 
 * Define Namespace 
 */
namespace mcms5xx\admin\modules;

/**
 * Checking if module included normally.
 */
if (!defined('mCMScheck')) {
    die('mCMS by MicroPHP');
}

require_once '../m/classes/paging.class.php';

/**
 * Gallery manager admin module
 */
class galleryManager extends \mcms5xx\classes\AdminPage
{
    protected $message = '';
    protected $fileCount = 0;
    protected $search_str = '';

    public function __construct()
    {
        $this->curr_module = 'gallery_manager';
        parent::__construct();
        $this->onLoad();
    }

    private function onLoad()
    {
        $this->buildMenu();
        $this->doAction();
        $this->buildPage();
    }

    private function doAction()
    {
        $catid = $this->utils->UserGetInt('catid');
        if ($catid > 0) {
            $cat_name = $this->catName($catid);
            $this->template->assign_block_vars('gallery_images', array(
                'CATID' => $catid,
                'CAT_NAME' => $cat_name,
            ));
            $this->template->assign_block_vars('gallery_images_js', array(
                'CATID' => $catid,
            ));
            $this->template->assign_block_vars('nav', array(
                'URL' => "&catid=" . $catid,
                'TITLE' => $cat_name,
            ));

            if ((@$this->user->perm_string['3'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                //Perm for Add
                $this->template->assign_block_vars('gallery_images.perm_add', array());
            }
            if ((@$this->user->perm_string['1'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                //Perm for Del
                $this->template->assign_block_vars('gallery_images.perm_del', array());
                $this->template->assign_block_vars('gallery_images_js.perm_del', array());
            }
        }
        if ((@$this->user->perm_string['3'] == 1) || (@$this->user->perm_string['0'] == 1)) {
            //Perm for Add
            if ($this->utils->Post('btn_upload')) {
                $this->upload();
            }
        }
        if ((@$this->user->perm_string['1'] == 1) || (@$this->user->perm_string['0'] == 1)) {
            //Perm for Del
            $del_id = $this->utils->Post('del_id');
            if (is_numeric($del_id) && $del_id != 0) {
                $this->delete($del_id);
            }
        }

        $id = $this->utils->Post('select_id');
        $action = $this->utils->Post('action');
        //echo "<pre>".print_r($_POST, true)."</pre>";

        switch ($action) {
            case 'delete': {
                if ((@$this->user->perm_string['1'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                    //Perm for Del
                    $this->deleteCat($id);
                }
                break;
            }
            case 'save': {
                $this->saveCat($id);
                break;
            }
            case 'save_options': {
                if ((@$this->user->perm_string['2'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                    //Perm for Edit
                    $this->saveOptions();
                }
                break;
            }
        }

        if ($this->utils->Get('search')) {
            $this->setSearch();
        }
    }

    private function buildPage()
    {
        $this->template->assign_var('TITLE', $this->fromLang('title'));
        $this->template->assign_var('CAT_CONFIRM', $this->fromLang('cat_confirm'));
        if ($this->message != '') {
            $this->template->assign_block_vars('error', array(
                'MESSAGE' => $this->message, ));
        }
        $catid = $this->utils->Get('catid');
        if (!is_numeric($catid) || $catid == 0) {
            $this->buildCategory();
        } else {
            $this->bindCategories();
            $this->buildUpload();
            $this->buildSearch();
            $this->buildFileList();
            $this->buildPaging();
        }
    }

    private function deleteCat($id)
    {
        $gallery_folder = $this->fromConfig('gallery_folder');
        $gallery_sub = $this->fromConfig('gallery_sub');
        $gallery_sub_f = '../'.$gallery_folder.'/'.str_replace('[id]', $id, $gallery_sub);
        if ($this->utils->io->DeleteDirTree($gallery_sub_f, true)) {
            $this->db->query('DELETE FROM '.$this->db->prefix.'gallery WHERE gid='.$id);
            $this->db->query('DELETE FROM '.$this->db->prefix.'gallerylocalizations WHERE gid='.$id);
            $this->db->query('DELETE FROM '.$this->db->prefix.'gallery_images WHERE gid='.$id);
        }
        $this->user->logOperation($this->user->GetUserId(), 'gallery_manager', $id, 'cat_delete');
    }

    private function addCatLocalization($catid)
    {
        $query = 'DELETE FROM '.$this->db->prefix."gallerylocalizations WHERE gid='".$catid."'";
        $this->db->query($query);

        foreach ($this->langs as $key => $value) {
            $lang = $value;
            $name = $this->utils->Post('txt_name_'.$lang);
            $gallery_desc = $this->utils->Post('txt_gallery_desc_'.$lang);

            $query = 'INSERT INTO '.$this->db->prefix.'gallerylocalizations(`gid`, `galleryname`, `gallery_desc`, `lang`)
            VALUES(' .$catid.", '".$name."', '".$gallery_desc."', '".$lang."')";
            $this->db->query($query);
        }
    }

    private function saveCat($id)
    {
        if ($id == -1) {
            if ((@$this->user->perm_string['3'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                //Perm for Add
                $this->addCat();
            }
        } else {
            if ((@$this->user->perm_string['2'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                //Perm for Edit
                $this->updateCat($id);
            }
        }
        $this->utils->Redirect('?'.$this->module_qs.'=gallery_manager');
    }

    private function addCat()
    {
        $result = $this->db->query('SELECT * FROM '.$this->db->prefix.'gallery ORDER BY position DESC '.$this->db->get_limit(0, 1));
        $row = $this->db->fetch($result);
        $pos_c = $row['position'] + 1;
        $active = $this->utils->UserPostInt('txt_active');

        $query = 'INSERT INTO '.$this->db->prefix.'gallery(`gdate`, `active`, `position`) VALUES('.time().', '.$active.", '".$pos_c."')";
        $this->db->query($query);
        $inserted_id = $this->db->insert_id();

        $gallery_folder = $this->fromConfig('gallery_folder');
        $gallery_sub = $this->fromConfig('gallery_sub');
        @mkdir('../'.$gallery_folder.'/'.str_replace('[id]', $inserted_id, $gallery_sub), 0767);

        $this->addCatLocalization($inserted_id);

        $this->user->logOperation($this->user->GetUserId(), 'gallery_manager', $inserted_id, 'cat_add');
    }

    private function updateCat($id)
    {
        $active = $this->utils->UserPostInt('txt_active');

        $query = 'UPDATE '.$this->db->prefix."gallery SET `active`='".$active."' WHERE gid='".$id."'";
        $this->db->query($query);

        $this->addCatLocalization($id);

        $this->user->logOperation($this->user->GetUserId(), 'gallery_manager', $id, 'cat_update');
    }

    private function buildCategory()
    {
        $this->template->assign_block_vars('gallery_cat', array());

        $cat_edit_id = $this->utils->Get('cateditid');
        if (!is_numeric($cat_edit_id) || $cat_edit_id == 0) {
            if (strlen($cat_edit_id) > 5) {
                $actArr = explode(':', $cat_edit_id);
                $activeSql = ($actArr['0'] == 'activate') ? ' `active`=1' : ' `active`=0';
                $ids = (strlen($actArr['1']) > 0) ? ' `gid` IN ( '.$actArr['1'].' )' : ' `gid`=0';
                $query = 'UPDATE '.$this->db->prefix.'gallery SET '.$activeSql.' WHERE '.$ids.'';
                $this->db->query($query);
                $this->utils->Redirect('?'.$this->module_qs.'=gallery_manager');
            } else {
                $this->buildCatList();
                if ((@$this->user->perm_string['2'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                    //Perm for Edit
                    $this->buildGalleryOptions();
                }
            }
        } else {
            $active = $this->utils->UserGetInt('active');
            if ($active > 0) {
                $activeSql = ($active == 1) ? ' `active`=0' : ' `active`=1';
                $query = 'UPDATE '.$this->db->prefix.'gallery SET '.$activeSql." WHERE `gid`='".$cat_edit_id."'";
                $this->db->query($query);
                $this->utils->Redirect('?'.$this->module_qs.'=gallery_manager');
            } else {
                $this->buildCatEdit($cat_edit_id);
            }
        }
    }

    private function catName($id)
    {
        $name = '';
        $result_loc = $this->db->query('SELECT * FROM '.$this->db->prefix."gallerylocalizations WHERE gid='".$id."' AND lang='".$this->default_lang."'");
        if ($row_loc = $this->db->fetch($result_loc)) {
            $name = $row_loc['galleryname'];
        }

        return $name;
    }

    private function buildCatList()
    {
        $gallery_code = $this->fromConfig('gallery_code');
        $this->template->assign_block_vars('gallery_cat.list', array(
            'ADD' => $this->fromLang('cat_add'),
            'ADD_URL' => '?'.$this->module_qs.'=gallery_manager&cateditid=-1',
            'ACTIVE' => $this->fromLang('gallery_active'),
            'INACTIVE' => $this->fromLang('gallery_inactive'),
            'ACTIVATE' => $this->fromLang('gallery_activate'),
            'INACTIVATE' => $this->fromLang('gallery_inactivate'),
            'URL' => '?'.$this->module_qs.'=gallery_manager',
            'NAME' => $this->fromLang('cat_name'),
            'CODE' => $this->fromLang('cat_code'),
            'POSITION' => $this->fromLang('cat_position'),
            'EDIT' => $this->fromLang('cat_edit'),
            'DELETE' => $this->fromLang('cat_delete'),
            'DELETE_CONFIRM' => $this->fromLang('cat_confirm'),
            'COPY' => $this->fromLang('cat_copy'),
        ));

        if ((@$this->user->perm_string['3'] == 1) || (@$this->user->perm_string['0'] == 1)) {
            //Perm for Add
            $this->template->assign_block_vars('gallery_cat.list.perm_add', array());
        }
        if ((@$this->user->perm_string['2'] == 1) || (@$this->user->perm_string['0'] == 1)) {
            //Perm for Edit
            $this->template->assign_block_vars('gallery_cat.list.perm_edit', array());
        }
        if ((@$this->user->perm_string['1'] == 1) || (@$this->user->perm_string['0'] == 1)) {
            //Perm for Del
            $this->template->assign_block_vars('gallery_cat.list.perm_del', array());
        }
        $ndx = 0;
        $hidden_val = '';

        $sql = 'SELECT * FROM '.$this->db->prefix.'gallery ORDER BY position ASC';
        $result = $this->db->query($sql);
        $cat_sayi = $this->db->num_rows($sql);

        while ($row = $this->db->fetch($result)) {
            $id = $row['gid'];
            $hidden_val .= ($ndx == 0) ? '' : ',';
            $hidden_val .= $id;
            ++$ndx;
            $name = '';
            $position_num = $row['position'];

            $result_loc = $this->db->query('SELECT * FROM '.$this->db->prefix."gallerylocalizations WHERE gid='".$id."' AND lang='".$this->default_lang."'");
            if ($row_loc = $this->db->fetch($result_loc)) {
                $name = $row_loc['galleryname'];
            }

            $name = $this->utils->GetPartOfString($name, 50);
            $code = '<input type="text" value="'.str_replace('[id]', $id, $gallery_code).'" style="margin:0px;width:145px" id="copy_'.$id.'"/>';

            $this->template->assign_block_vars('gallery_cat.list.items', array(
                'ID' => $id,
                'NAME' => $name,
                'POSITION_NUM' => $position_num,
                'CODE' => $code,
                'EDIT_URL' => '?'.$this->module_qs.'=gallery_manager&cateditid='.$id,
                'ACTIVE_URL' => '?'.$this->module_qs.'=gallery_manager&cateditid='.$id.'&active=1',
                'INACTIVE_URL' => '?'.$this->module_qs.'=gallery_manager&cateditid='.$id.'&active=2',
            ));

            if ((@$this->user->perm_string['2'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                //Perm for Edit
                $this->template->assign_block_vars('gallery_cat.list.items.perm_edit', array());

                if ($row['active'] == 1) {
                    $this->template->assign_block_vars('gallery_cat.list.items.active', array());
                } else {
                    $this->template->assign_block_vars('gallery_cat.list.items.inactive', array());
                }
            }
            if ((@$this->user->perm_string['1'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                //Perm for Del
                $this->template->assign_block_vars('gallery_cat.list.items.perm_del', array());
            }
        }
        $this->template->assign_block_vars('gallery_cat.list.hidden', array(
            'VALUE' => $hidden_val,
        ));
    }

    private function buildCatEdit($cat_edit_id)
    {
        $cat_query = 'SELECT * FROM '.$this->db->prefix."gallery WHERE gid='".$cat_edit_id."' ";
        $cat_result = $this->db->query($cat_query);
        $cat_row = $this->db->fetch($cat_result);

        $active_chk = (($cat_row['active'] == 1) || ($cat_edit_id == 0) || ($cat_edit_id == -1)) ? 'checked' : '';

        $this->template->assign_block_vars('gallery_cat.catedit', array(
            'ACTIVE' => $this->fromLang('cat_active'),
            'ACTIVE_CHK' => $active_chk,
            'SAVE' => $this->fromLang('cat_save'),
            'CANCEL' => $this->fromLang('cat_cancel'),
            'URL' => '?'.$this->module_qs.'=gallery_manager',
            'ID' => $cat_edit_id,
        ));

        $ndx = 0;
        foreach ($this->langs as $key => $value) {
            ++$ndx;
            $lang = $value;
            $name = $gallery_desc = '';
            $class = ($ndx == 1) ? ' class="active"' : '';
            $fade_class = ($ndx == 1) ? ' in active' : '';

            $query = 'SELECT * FROM '.$this->db->prefix."gallerylocalizations WHERE gid='".$cat_edit_id."' AND lang='".$lang."'";
            $result = $this->db->query($query);
            if ($row = $this->db->fetch($result)) {
                $name = $row['galleryname'];
                $gallery_desc = $row['gallery_desc'];
            }

            $this->template->assign_block_vars('gallery_cat.catedit.tab', array(
                'LANG' => $lang,
                'CLASS' => $class,
                'FADE_CLASS' => $fade_class,
                'NAME' => $this->fromLang('cat_name'),
                'NAME_VALUE' => $name,
                'GALLERY_DESC' => $this->fromLang('cat_desc'),
                'GALLERY_DESC_VALUE' => $gallery_desc,
            ));
        }
    }

    private function bindCategories()
    {
        $this->template->assign_var('CATEGORY', $this->fromLang('category'));
        $this->template->assign_var('CAT_ALL', $this->fromLang('cat_all'));
        $cat = $this->utils->Get('cat');
        $file_types = $this->fromConfig('file_types');

        foreach ($file_types as $key => $value) {
            $sel = '';
            if ($key == $cat) {
                $sel = 'selected';
            }

            $this->template->assign_block_vars('cat_option', array(
                'VALUE' => $key,
                'SELECTED' => $sel, ));
        }
    }

    private function buildUpload()
    {
        $upload_count = $this->fromConfig('multiple_upload_count');

        $this->template->assign_block_vars('upload', array(
            'FILE' => $this->fromLang('upload_file'),
            'TITLE' => $this->fromLang('upload_title'),
            'UPLOAD' => $this->fromLang('upload'), ));
        for ($i = 0; $i < $upload_count; ++$i) {
            $this->template->assign_block_vars('upload.file', array());
        }
    }

    private function buildSearch()
    {
        $this->template->assign_block_vars('search', array(
            'TEXT' => $this->fromLang('search_text'),
            'SEARCH' => $this->fromLang('search'),
            'SEARCH_TXT' => $this->search_str,
            'HELP' => $this->fromLang('search_help'), ));
    }

    private function buildFileList()
    {
        $this->template->assign_block_vars('files', array(
            'ID' => $this->fromLang('file_id'),
            'FILENAME' => $this->fromLang('file_name'),
            'ORIGINALNAME' => $this->fromLang('file_originalname'),
            'EXTENSION' => $this->fromLang('file_extension'),
            'TITLE' => $this->fromLang('file_title'),
            'COPY_URL' => $this->fromLang('copy_url'),
            'ACTION' => $this->fromLang('file_action'),
            'CONFIRM' => $this->fromLang('file_delete_confirm'),
        ));

        $in_page = $this->fromConfig('file_in_page');
        $site_url = $this->fromConfig('site_url');
        $page = $this->utils->Get('page');

        if (!is_numeric($page)) {
            $page = 1;
        }

        if ($page == 0) {
            $page = 1;
        }

        $start = ($page - 1) * $in_page;
        $end = $in_page;
        $cat = $this->utils->Get('cat');
        $have_where = false;
        $query = 'SELECT * FROM '.$this->db->prefix.'files';
        if ($cat != '' && $cat != 'all') {
            $query .= " WHERE category ='".$cat."'";
            $have_where = true;
        }
        if ($this->search_str != '') {
            if ($have_where) {
                $query .= " and originalname REGEXP '".$this->search_str."'";
            } else {
                $query .= " WHERE originalname REGEXP '".$this->search_str."'";
            }
        }
        $query .= ' ORDER BY fileid DESC';

        $this->fileCount = $this->db->num_rows($query);
        $query .= $this->db->get_limit($start, $end);
        $result = $this->db->query($query);
        $ndx = 0;
        while ($row = $this->db->fetch($result)) {
            $class = (($ndx % 2) == 0) ? ' class="alternate-row"' : '';
            ++$ndx;
            $this->template->assign_block_vars('files.all', array(
                'ID' => $row['fileid'],
                'FILENAME' => '<a href="../uploads/'.$row['category'].'/'.$row['filename'].'" tagret="_blank">'.$row['filename'].'</a>',
                'COPY_URL' => '/uploads/'.$row['category'].'/'.$row['filename'],
                'ORIGINALNAME' => $row['originalname'],
                'EXTENSION' => $row['extension'],
                'TITLE' => $row['title'],
                'CLASS' => $class,
                'DELETE' => $this->fromLang('file_delete'),
            ));
        }
    }

    private function buildGalleryOptions()
    {
        $watermark_chk = ($this->getKey('watermark', 0) == 1) ? ' checked' : '';
        $this->template->assign_block_vars('gallery_options', array(
            'TITLE' => $this->fromLang('gallery_options'),
            'WATERMARK' => $this->fromLang('watermark'),
            'WATERMARK_CHK' => $watermark_chk,
            'WATERMARK_NAME' => $this->fromLang('watermark_name'),
            'WATERMARK_NAME_VALUE' => $this->getKey('watermark_name', ''),
            'WATERMARK_COLOR' => $this->fromLang('watermark_color'),
            'WATERMARK_COLOR_VALUE' => $this->getKey('watermark_color', '#000000'),
            'SAVE' => $this->fromLang('watermark_save'),
            'CANCEL' => $this->fromLang('watermark_cancel'),
        ));
    }

    private function saveOptions()
    {
        $watermark = $this->utils->UserPostInt('txt_watermark');
        $this->addKey('watermark', $watermark);

        $watermark_name = $this->utils->UserPost('txt_watermark_name');
        $this->addKey('watermark_name', $watermark_name);

        $watermark_color = $this->utils->UserPost('txt_watermark_color');
        $this->addKey('watermark_color', $watermark_color);

        $this->user->logOperation($this->user->GetUserId(), 'gallery_manager', 0, 'options_save');
        $this->utils->Redirect('?'.$this->module_qs.'=gallery_manager');
    }

    private function buildPaging()
    {
        $arr = $this->getPagingArray($this->fileCount);

        /*
        print("<pre>");
        print_r($arr);
        print("</pre>");
        */

        $prev_url = '';

        if ($arr['PREV_LINK_PAGE']) {
            $prev_url = 'href="'.$arr['prefix'].$arr['PREV_LINK_PAGE'].'"';
        }

        $next_url = '';

        if ($arr['NEXT_LINK_PAGE']) {
            $next_url = 'href="'.$arr['prefix'].$arr['NEXT_LINK_PAGE'].'"';
        }

        $this->template->assign_block_vars('paging', array(
            'PREV_URL' => $prev_url,
            'NEXT_URL' => $next_url,
        ));

        /*
        print("<pre>");
        print_r($arr["PAGE_NUMBERS"]);
        print("</pre>");
        */

        $count = (is_array($arr['PAGE_NUMBERS'])) ? count($arr['PAGE_NUMBERS']) : 0;
        for ($i = 0; $i < $count; ++$i) {
            $num = $arr['PAGE_NUMBERS'][$i];
            $num_url = '';
            $sep = false;
            if ($i < ($count - 1)) {
                $sep = true;
            }

            if ($arr['CURRENT_PAGE'] != $num) {
                $num_url = 'href="'.$arr['prefix'].$num.'"';
            }

            $this->template->assign_block_vars('paging.numbers', array(
                'URL' => $num_url,
                'TEXT' => $num, ));

            if ($sep) {
                $this->template->assign_block_vars('paging.numbers.sep', array());
            }
        }
    }

    private function getPagingArray($count)
    {
        $in_page = $this->fromConfig('file_in_page');

        //$file_count = $this->db->num_rows("SELECT * FROM ".$this->db->prefix."files");
        $file_count = $count;
        $page_count = ceil($file_count / $in_page);

        //paging begin
        $new_url = $_SERVER['REQUEST_URI'];
        $new_url = $this->removeQS($new_url, 'page');
        $new_url .= '&';

        $paging = new \mcms5xx\classes\PagedResults();
        $paging->TotalResults = $file_count;
        $paging->ResultsPerPage = $in_page;
        $paging->LinksPerPage = 10;
        $paging->PageVarName = 'page';
        $paging->UrlPrefix = $new_url;
        //paging end

        $arr = $paging->InfoArray();
        $arr['prefix'] = $paging->Prefix;

        return $arr;
    }

    private function removeQS($url, $qs)
    {
        return $this->utils->removeQueryString($url, $qs);
    }

    private function upload()
    {
        $files = $_FILES['txt_files'];
        $titles = $this->utils->Post('txt_titles');
        /*
        print("<pre>");
        print_r($files);
        print("</pre>");
        */

        for ($i = 0; $i < count($files); ++$i) {
            $filename = $files['name'][$i];
            $originalname = strtolower($filename);
            if (trim($filename) != '') {
                $ext = $this->utils->GetFileExtension($filename);
                if (in_array($ext, $this->getFileTypesArray())) {
                    $cat = $this->getFileCat($ext);
                    $file_dir = '../'.$this->fromConfig('upload_folder').$cat.'/';
                    $filename = $this->utils->GetFileName($file_dir, $filename);

                    if (move_uploaded_file($files['tmp_name'][$i], $file_dir.$filename) && chmod($file_dir.$filename, 0777)) {
                        $query = 'INSERT INTO '.$this->db->prefix."files (originalname, filename, extension, category, title) VALUES('".$originalname."','".$filename."','".$ext."','".$cat."','".strtolower($titles[$i])."' )";
                        $this->db->query($query);
                    }
                    $this->utils->Redirect('?'.$this->module_qs.'=file_manager');
                } else {
                    $this->message = $this->fromLang('invalid_type', $ext);
                }
            }
        }
    }

    private function delete($id)
    {
        $query = 'SELECT * FROM '.$this->db->prefix.'files WHERE fileid='.$id;
        $result = $this->db->query($query);
        if ($row = $this->db->fetch($result)) {
            $cat = $row['category'];
            $filename = $row['filename'];
            $file_dir = '../'.$this->fromConfig('upload_folder').$cat.'/';
            @unlink($file_dir.$filename);
            $query = 'DELETE FROM '.$this->db->prefix.'files WHERE fileid='.$id;
            $this->db->query($query);
        }
        $this->user->logOperation($this->user->GetUserId(), 'gallery_manager', $id, 'delete');
        $this->utils->Redirect('?'.$this->module_qs.'=file_manager');
    }

    private function setSearch()
    {
        $search = $this->utils->Get('search');
        //$search = str_replace("*", "%", $search);
        //$search = str_replace("?", "_", $search);
        $this->search_str = $search;
    }

}

$gallery_manager = new galleryManager();
$gallery_manager->template->pparse('gallery_manager');

/******************* gallery_manager.admin.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** gallery_manager.admin.php ******************/;
