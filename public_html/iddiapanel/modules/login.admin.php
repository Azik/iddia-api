<?php
/******************* login.admin.php *******************
 *
 * Admin login module
 *
 * @author MicroPHP <info@microphp.com>
 * @web www.microphp.com
 * @copyright 2004-2019 MicroPHP
 *
 ******************** login.admin.php ******************/

/** 
 * Define Namespace 
 */
namespace mcms5xx\admin\modules;

/**
 * Checking if module included normally.
 */
if (!defined('mCMScheck')) {
    die('mCMS by MicroPHP');
}

class Login extends \mcms5xx\classes\AdminPage
{
    public function __construct()
    {
        parent::__construct();
        $this->onLoad();
    }

    private function onLoad()
    {
        global $lnum;
        $this->generateToken();

        $_SESSION['LNUM'] = $lnum;
        //if($this->utils->Post('btn_login'))
        if (@$_SERVER['REQUEST_METHOD'] == 'POST') {
            $this->doLogin();
        } else {
            // Log operation
            $this->user->logOperation($this->user->GetUserId(), 'login', 0, 'login_page');
        }

        $this->template->assign_vars(array(
            'TITLE' => $this->fromLang('title'),
            'USERNAME_REQUEST' => $this->fromLang('username_request'),
            'USERNAME' => $this->fromLang('username'),
            'PASSWORD' => $this->fromLang('password'),
            'ENTER' => $this->fromLang('enter'),
            'RESET' => $this->fromLang('reset'),
        ));
    }

    private function doLogin()
    {
        $teiken = $this->utils->UserPost('teiken');
        $stoken = $this->utils->dataFullFilter($_SESSION['token']);
        $username = $this->utils->UserPost('txt_username');
        $password = $this->utils->UserPost('txt_password');
        if ($this->user->Login($username, $password, $teiken, $stoken)) {
            // Log operation
            $this->user->logOperation($this->user->GetUserId(), 'login', 0, 'login_ok');
            $this->utils->Redirect('index.php');
        } else {
            // Log operation
            $this->user->logOperation($this->user->GetUserId(), 'login', 0, 'login_incorrect');
            $this->template->assign_block_vars('alert', array(
                'MESSAGE' => $this->fromLang('incorrect'),));
        }
    }
}

$login = new Login();
$login->template->pparse('login');

/******************* login.admin.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** login.admin.php ******************/;
