<?php
/******************* blocks.admin.php *******************
 *
 * Blocks admin module
 *
 * @author MicroPHP <info@microphp.com>
 * @web www.microphp.com
 * @copyright 2004-2019 MicroPHP
 *
 ******************** blocks.admin.php ******************/

/** 
 * Define Namespace 
 */
namespace mcms5xx\admin\modules;

/**
 * Checking if module included normally.
 */
if (!defined('mCMScheck')) {
    die('mCMS by MicroPHP');
}

require_once '../m/classes/paging.class.php';

class Blocks extends \mcms5xx\classes\AdminPage
{
    protected $blocksCount;

    public function __construct()
    {
        $this->curr_module = 'blocks';
        parent::__construct();
        $this->onLoad();
    }

    private function onLoad()
    {
        $this->buildMenu();
        $this->doAction();
        $this->buildPage();
    }

    private function doAction()
    {
        $id = $this->utils->UserPostInt('select_id');

        $cat_action = $this->utils->Post('cat_action');

        switch ($cat_action) {
            case 'delete': {
                if ((@$this->user->perm_string['1'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                    //Perm for Del
                    $this->deleteCat($id);
                    $this->user->logOperation($this->user->GetUserId(), 'blocks', $id, 'blocks_cat_del');
                }
                break;
            }
            case 'save': {
                $this->saveCat($id);
                break;
            }
        }

        $blocks_action = $this->utils->Post('blocks_action');
        $catid = $this->utils->UserGetInt('catid');

        switch ($blocks_action) {
            case 'delete': {
                if ((@$this->user->perm_string['1'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                    //Perm for Del
                    $this->deleteBlocks($id);
                    $this->user->logOperation($this->user->GetUserId(), 'blocks', $id, 'blocks_delete');
                }
                break;
            }
            case 'save': {
                $this->saveBlocks($id, $catid);
                break;
            }
        }
    }

    private function deleteCat($id)
    {
        $this->db->query('DELETE FROM '.$this->db->prefix.'blockscategories WHERE catid='.$id);
        $this->db->query('DELETE FROM '.$this->db->prefix.'blockscategorylocalizations WHERE catid='.$id);

        $result = $this->db->query('SELECT blocksid FROM '.$this->db->prefix.'blocks WHERE catid='.$id);
        while ($row = $this->db->fetch($result)) {
            $blocksid = $row['blocksid'];
            $this->db->query('DELETE FROM '.$this->db->prefix.'blockslocalizations WHERE blocksid='.$blocksid);
        }
        $this->db->query('DELETE FROM '.$this->db->prefix.'blocks WHERE catid='.$id);
    }

    private function addCatLocalization($catid)
    {
        $query = 'DELETE FROM `'.$this->db->prefix."blockscategorylocalizations` WHERE `catid`='".$catid."'";
        $this->db->query($query);

        foreach ($this->langs as $key => $value) {
            $lang = $value;
            $name = $this->utils->UserTextPost('txt_name_'.$lang);
            $more_txt = $this->utils->UserTextPost('txt_more_txt_'.$lang);
            $imgid = $this->utils->UserPostInt('txt_imgid_'.$lang);
            $info = $this->utils->UserTextPost('txt_info_'.$lang);
            $embed_code = $this->utils->UserTextPost('txt_embed_code_'.$lang);
            $url = $this->utils->UserTextPost('txt_url_'.$lang);
            $query = 'INSERT INTO '.$this->db->prefix.'blockscategorylocalizations(`catid`, `lang`, `name`, `more_txt`, `imgid`, `info`, `embed_code`, `url`)
            VALUES(' .$catid.", '".$lang."', '".$name."', '".$more_txt."', ".$imgid.", '".$info."', '".$embed_code."', '".$url."')";
            $this->db->query($query);
        }
    }

    private function saveCat($id)
    {
        if ($id == -1) {
            if ((@$this->user->perm_string['3'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                //Perm for Add
                $this->addCat();
            }
        } else {
            if ((@$this->user->perm_string['2'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                //Perm for Edit
                $this->updateCat($id);
            }
        }

        $this->utils->Redirect('?'.$this->module_qs.'=blocks');
    }

    private function addCat()
    {
        $result = $this->db->query('SELECT * FROM '.$this->db->prefix.'blockscategories ORDER BY position DESC '.$this->db->get_limit(0, 1));
        $row = $this->db->fetch($result);
        $pos_c = $row['position'] + 1;
        $active = $this->utils->UserPostInt('txt_active');

        $query = 'INSERT INTO '.$this->db->prefix.'blockscategories (`position`, `active`) VALUES('.$pos_c.', '.$active.')';
        $this->db->query($query);
        $inserted_id = $this->db->insert_id();

        $this->addCatLocalization($inserted_id);

        $this->user->logOperation($this->user->GetUserId(), 'blocks', $inserted_id, 'blocks_cat_add');
    }

    private function updateCat($id)
    {
        $active = $this->utils->UserPostInt('txt_active');

        $query = 'UPDATE `'.$this->db->prefix."blockscategories` SET `active`='".$active."' WHERE catid=".$id.'';
        $this->db->query($query);

        $this->addCatLocalization($id);
        $this->user->logOperation($this->user->GetUserId(), 'blocks', $id, 'blocks_cat_update');
    }

    private function deleteBlocks($id)
    {
        $this->db->query('DELETE FROM '.$this->db->prefix.'blocks WHERE blocksid='.$id);
        $this->db->query('DELETE FROM '.$this->db->prefix.'blockslocalizations WHERE blocksid='.$id);
    }

    private function saveBlocks($id, $catid)
    {
        if ($id == -1) {
            if ((@$this->user->perm_string['3'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                //Perm for Add
                $this->addBlocks($catid);
            }
        } else {
            if ((@$this->user->perm_string['2'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                //Perm for Edit
                $this->updateBlocks($id, $catid);
            }
        }

        $this->utils->Redirect('?'.$this->module_qs.'=blocks&catid='.$catid);
    }

    private function addBlocks($catid)
    {
        $txt_active = $this->utils->UserPostInt('txt_active');

        $query = 'INSERT INTO '.$this->db->prefix.'blocks(`catid`, `active`) VALUES('.$catid.', '.$txt_active.')';
        $this->db->query($query);
        $inserted_id = $this->db->insert_id();

        $this->addBlocksLocalization($inserted_id);
        $this->user->logOperation($this->user->GetUserId(), 'blocks', $inserted_id, 'blocks_add');
    }

    private function updateBlocks($id, $catid)
    {
        $txt_active = $this->utils->UserPostInt('txt_active');

        $query = 'UPDATE `'.$this->db->prefix.'blocks` SET `active`='.$txt_active." WHERE blocksid='".$id."'";
        $this->db->query($query);

        $this->addBlocksLocalization($id);

        $this->user->logOperation($this->user->GetUserId(), 'blocks', $id, 'blocks_update');
    }

    private function addBlocksLocalization($blocksid)
    {
        $query = 'DELETE FROM '.$this->db->prefix."blockslocalizations WHERE blocksid='".$blocksid."'";
        $this->db->query($query);

        foreach ($this->langs as $key => $value) {
            $lang = $value;
            $img = $this->utils->UserPostInt('txt_img_'.$lang);
            $img02 = $this->utils->UserPostInt('txt_img02_'.$lang);
            $img_url = $this->utils->Post('txt_img_url_'.$lang);
            $name = $this->utils->Post('txt_name_'.$lang);
            $link_name = $this->utils->Post('txt_link_name_'.$lang);
            $text = $this->utils->Post('txt_text_'.$lang);
            $url = $this->utils->Post('txt_url_'.$lang);

            $query = 'INSERT INTO '.$this->db->prefix.'blockslocalizations(`blocksid`, `lang`, `img`, `img02`, `img_url`, `name`, `link_name`, `text`, `url`)
            VALUES(' .$blocksid.", '".$lang."', ".$img.', '.$img02.", '".$img_url."', '".$name."', '".$link_name."', '".$text."', '".$url."')";
            $this->db->query($query);
        }
    }

    //endregion

    //region page building

    private function buildPage()
    {
        $this->buildMain();

        $catid = $this->utils->Get('catid');
        if (!is_numeric($catid) || $catid == 0) {
            $this->buildCategory();
        } else {
            $this->buildBlocks($catid);
        }
    }

    private function buildMain()
    {
        $this->template->assign_var('TITLE', $this->fromLang('title'));
        $this->template->assign_var('DELETE_CONFIRM', $this->fromLang('blocks_confirm'));
    }

    private function buildCategoryOperation()
    {
        $operid = $this->utils->UserGetInt('operid');
        if ($operid > 0) {
            $oper = $this->utils->Get('oper');
            if ($oper == 'down') {
                /* B: Down */
                $Down1SQL = $this->db->fetch($this->db->query('SELECT * FROM '.$this->db->prefix."blockscategories WHERE catid='".$operid."'"));
                $cur_pos = $Down1SQL['position'];

                $new_pos = $cur_pos + 1;
                $Down2SQL = $this->db->fetch($this->db->query('SELECT * FROM '.$this->db->prefix."blockscategories WHERE position='".$new_pos."'"));

                $query = 'UPDATE '.$this->db->prefix."blockscategories SET position='".$new_pos."' WHERE catid='".$Down1SQL['catid']."'";
                $this->db->query($query);

                $query = 'UPDATE '.$this->db->prefix."blockscategories SET position='".$cur_pos."' WHERE catid='".$Down2SQL['catid']."'";
                $this->db->query($query);
                /* E: Down */
            } elseif ($oper == 'up') {
                /* B: Down */
                $Down1SQL = $this->db->fetch($this->db->query('SELECT * FROM '.$this->db->prefix."blockscategories WHERE catid='".$operid."'"));
                $cur_pos = $Down1SQL['position'];

                $new_pos = $cur_pos - 1;
                $Down2SQL = $this->db->fetch($this->db->query('SELECT * FROM '.$this->db->prefix."blockscategories WHERE position='".$new_pos."'"));

                $query = 'UPDATE '.$this->db->prefix."blockscategories SET position='".$new_pos."' WHERE catid='".$Down1SQL['catid']."'";
                $this->db->query($query);

                $query = 'UPDATE '.$this->db->prefix."blockscategories SET position='".$cur_pos."' WHERE catid='".$Down2SQL['catid']."'";
                $this->db->query($query);
                /* E: Down */
            }
        }
        $this->orderPosition();
    }

    private function orderPosition()
    {
        $c_pos = 0;
        $result = $this->db->query('SELECT * FROM '.$this->db->prefix.'blockscategories ORDER BY position ASC');
        while ($row = $this->db->fetch($result)) {
            ++$c_pos;
            $query = 'UPDATE '.$this->db->prefix."blockscategories SET position='".$c_pos."' WHERE catid='".$row['catid']."'";
            $this->db->query($query);
        }
    }

    private function buildBlockOperation($catid = 0)
    {
        $operid = $this->utils->UserGetInt('operid');
        if ($operid > 0) {
            $oper = $this->utils->Get('oper');
            if ($oper == 'down') {
                /* B: Down */
                $Down1SQL = $this->db->fetch($this->db->query('SELECT * FROM '.$this->db->prefix."blocks WHERE blocksid='".$blocksid."'"));
                $cur_pos = $Down1SQL['position'];

                $new_pos = $cur_pos + 1;
                $Down2SQL = $this->db->fetch($this->db->query('SELECT * FROM '.$this->db->prefix."blocks WHERE (position='".$new_pos."') && (catid=".$catid.') '));

                $query = 'UPDATE '.$this->db->prefix."blocks SET position='".$new_pos."' WHERE blocksid='".$Down1SQL['blocksid']."'";
                $this->db->query($query);

                $query = 'UPDATE '.$this->db->prefix."blocks SET position='".$cur_pos."' WHERE blocksid='".$Down2SQL['blocksid']."'";
                $this->db->query($query);
                /* E: Down */
            } elseif ($oper == 'up') {
                /* B: Down */
                $Down1SQL = $this->db->fetch($this->db->query('SELECT * FROM '.$this->db->prefix."blocks WHERE blocksid='".$operid."'"));
                $cur_pos = $Down1SQL['position'];

                $new_pos = $cur_pos - 1;
                $Down2SQL = $this->db->fetch($this->db->query('SELECT * FROM '.$this->db->prefix."blocks WHERE (position='".$new_pos."') && (catid=".$catid.') '));

                $query = 'UPDATE '.$this->db->prefix."blocks SET position='".$new_pos."' WHERE blocksid='".$Down1SQL['blocksid']."'";
                $this->db->query($query);

                $query = 'UPDATE '.$this->db->prefix."blocks SET position='".$cur_pos."' WHERE blocksid='".$Down2SQL['blocksid']."'";
                $this->db->query($query);
                /* E: Down */
            }
        }
        $this->orderBlockPosition($catid);
    }

    private function orderBlockPosition($catid = 0)
    {
        $c_pos = 0;
        $result = $this->db->query('SELECT * FROM '.$this->db->prefix.'blocks WHERE `catid`='.$catid.' ORDER BY `position` ASC');
        while ($row = $this->db->fetch($result)) {
            $query = 'UPDATE '.$this->db->prefix."blocks SET position='".(++$c_pos)."' WHERE blocksid='".$row['blocksid']."'";
            $this->db->query($query);
        }
    }

    private function buildCategory()
    {
        $this->template->assign_block_vars('cat', array());

        $cat_edit_id = $this->utils->Get('cateditid');
        if (!is_numeric($cat_edit_id) || $cat_edit_id == 0) {
            /* $this->buildCategoryOperation(); */
            if (strlen($cat_edit_id) > 5) {
                $actArr = explode(':', $cat_edit_id);
                $activeSql = ($actArr['0'] == 'activate') ? ' `active`=1' : ' `active`=0';
                $ids = (strlen($actArr['1']) > 0) ? ' `catid` IN ( '.$actArr['1'].' )' : ' `catid`=0';
                $query = 'UPDATE '.$this->db->prefix.'blockscategories SET '.$activeSql.' WHERE '.$ids.'';
                $this->db->query($query);
                $this->utils->Redirect('?'.$this->module_qs.'=blocks');
            } else {
                $this->buildCatList();
            }
        } else {
            $active = $this->utils->UserGetInt('active');
            if ($active > 0) {
                $activeSql = ($active == 1) ? ' `active`=0' : ' `active`=1';
                $query = 'UPDATE '.$this->db->prefix.'blockscategories SET '.$activeSql." WHERE `catid`='".$cat_edit_id."'";
                $this->db->query($query);
                $this->utils->Redirect('?'.$this->module_qs.'=blocks');
            } else {
                $this->buildCatEdit($cat_edit_id);
            }
        }
    }

    private function buildCatList()
    {
        $this->template->assign_block_vars('cat.list', array(
            'CAT' => $this->fromLang('cat_title'),
            'ADD' => $this->fromLang('cat_add'),
            'ADD_URL' => '?'.$this->module_qs.'=blocks&cateditid=-1',
            'ACTIVE' => $this->fromLang('blocks_active'),
            'INACTIVE' => $this->fromLang('blocks_inactive'),
            'ACTIVATE' => $this->fromLang('blocks_activate'),
            'INACTIVATE' => $this->fromLang('blocks_inactivate'),
            'URL' => '?'.$this->module_qs.'=blocks',
            'NAME' => $this->fromLang('cat_name'),
            'ID' => $this->fromLang('cat_id'),
            'COPY_ID' => $this->fromLang('copy_id'),
            'POSITION' => $this->fromLang('cat_position'),
            'EDIT' => $this->fromLang('cat_edit'),
            'DELETE' => $this->fromLang('cat_delete'),
            'DELETE_CONFIRM' => $this->fromLang('cat_confirm'),
        ));
        if ((@$this->user->perm_string['3'] == 1) || (@$this->user->perm_string['0'] == 1)) {
            //Perm for Add
            $this->template->assign_block_vars('cat.list.perm_add', array());
        }
        if ((@$this->user->perm_string['2'] == 1) || (@$this->user->perm_string['0'] == 1)) {
            //Perm for Edit
            $this->template->assign_block_vars('cat.list.perm_edit', array());
        }

        if ((@$this->user->perm_string['1'] == 1) || (@$this->user->perm_string['0'] == 1)) {
            //Perm for Del
            $this->template->assign_block_vars('cat.list.perm_del', array());
        }

        $ndx = 0;
        $hidden_val = '';

        $sql = 'SELECT * FROM '.$this->db->prefix.'blockscategories ORDER BY position ASC';
        $result = $this->db->query($sql);
        $cat_sayi = $this->db->num_rows($sql);

        while ($row = $this->db->fetch($result)) {
            $id = $row['catid'];
            $position_id = $row['position'];
            $hidden_val .= ($ndx == 0) ? '' : ',';
            $hidden_val .= $id;
            ++$ndx;
            $name = '';
            $position = '';

            /* UP */
            $position .= ($row['position'] > 1) ? '<a href="index.php?module=blocks&oper=up&operid='.$id.'" style="margin-left:10px;"><img src="n_up.gif" alt="Up" title="Up" /></a>' : '<a style="margin-left:20px;"></a>';

            /* DOWN */
            $position .= ($row['position'] < $cat_sayi) ? '<a href="index.php?module=blocks&oper=down&operid='.$id.'" style="margin-left:15px;"><img src="n_down.gif" alt="Down" title="Down" /></a>' : '<span></span>';

            $result_loc = $this->db->query('SELECT * FROM '.$this->db->prefix."blockscategorylocalizations WHERE catid='".$id."' AND lang='".$this->default_lang."'");
            if ($row_loc = $this->db->fetch($result_loc)) {
                $name = $row_loc['name'];
            }

            $name = $this->utils->GetPartOfString($name, 50);
            $code = '&lt;!-- BEGIN blocks_'.$id.' --&gt; '."\n".'
&lt;!-- END blocks_'.$id.' --&gt;';
            $this->template->assign_block_vars('cat.list.items', array(
                'ID' => $id,
                'POSITION_ID' => $position_id,
                'NAME' => $name,
                'CODE' => $code,
                'POSITION' => $position,
                'EDIT_URL' => '?'.$this->module_qs.'=blocks&cateditid='.$id,
                'ACTIVE_URL' => '?'.$this->module_qs.'=blocks&cateditid='.$id.'&active=1',
                'INACTIVE_URL' => '?'.$this->module_qs.'=blocks&cateditid='.$id.'&active=2',
            ));
            if ((@$this->user->perm_string['2'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                //Perm for Edit
                $this->template->assign_block_vars('cat.list.items.perm_edit', array());
                if ($row['active'] == 1) {
                    $this->template->assign_block_vars('cat.list.items.active', array());
                } else {
                    $this->template->assign_block_vars('cat.list.items.inactive', array());
                }
            }
            if ((@$this->user->perm_string['1'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                //Perm for Del
                $this->template->assign_block_vars('cat.list.items.perm_del', array());
            }
        }
        $this->template->assign_block_vars('cat.list.hidden', array(
            'VALUE' => $hidden_val,
        ));
    }

    private function buildCatEdit($cat_edit_id)
    {
        $cat_query = 'SELECT * FROM '.$this->db->prefix."blockscategories WHERE catid='".$cat_edit_id."' ";
        $cat_result = $this->db->query($cat_query);
        $cat_row = $this->db->fetch($cat_result);

        $active_chk = (($cat_row['active'] == 1) || ($cat_edit_id == -1)) ? 'checked' : '';

        $this->template->assign_block_vars('cat.catedit', array(
            'TAB_HEADER_WIDTH' => (count($this->langs) * 75),
            'ACTIVE' => $this->fromLang('cat_active'),
            'ACTIVE_CHK' => $active_chk,
            'SAVE' => $this->fromLang('cat_save'),
            'CANCEL' => $this->fromLang('cat_cancel'),
            'URL' => '?'.$this->module_qs.'=blocks',
            'ID' => $cat_edit_id,
        ));

        $ndx = 0;
        foreach ($this->langs as $key => $value) {
            ++$ndx;
            $lang = $value;
            $name = $more_txt = $info = $embed_code = $url = '';
            $imgid = 0;
            $class = ($ndx == 1) ? ' class="active"' : '';
            $fade_class = ($ndx == 1) ? ' in active' : '';

            $query = 'SELECT * FROM '.$this->db->prefix."blockscategorylocalizations WHERE catid='".$cat_edit_id."' AND lang='".$lang."'";
            $result = $this->db->query($query);
            if ($row = $this->db->fetch($result)) {
                $name = $row['name'];
                $more_txt = $row['more_txt'];
                $info = $row['info'];
                $imgid = $row['imgid'];
                $embed_code = $row['embed_code'];
                $url = $row['url'];
            }

            $this->template->assign_block_vars('cat.catedit.tab', array(
                'LANG' => $lang,
                'CLASS' => $class,
                'FADE_CLASS' => $fade_class,
                'NAME' => $this->fromLang('cat_name'),
                'NAME_VALUE' => $name,
                'MORE_TXT' => $this->fromLang('cat_more_txt'),
                'MORE_TXT_VALUE' => $more_txt,
                'IMGID' => $this->fromLang('cat_imgid'),
                'IMGID_VALUE' => $imgid,
                'INFO' => $this->fromLang('cat_info'),
                'INFO_VALUE' => $info,
                'EMBED_CODE' => $this->fromLang('cat_embed_code'),
                'EMBED_CODE_VALUE' => $embed_code,
                'URL' => $this->fromLang('cat_url'),
                'URL_VALUE' => $url,
            ));
        }
    }

    private function buildBlocks($catid)
    {
        $this->template->assign_block_vars('blocks', array(
            'CATID' => $catid,
        ));

        $blocksid = $this->utils->Get('blocksid');
        $blocks_id = $this->utils->Get('blocks_id');
        if ((!is_numeric($blocksid) || $blocksid == 0) && (!is_numeric($blocks_id) || $blocks_id == 0)) {
            //$this->buildBlockOperation($catid);
            if (strlen($blocksid) > 5) {
                $actArr = explode(':', $blocksid);
                $activeSql = ($actArr['0'] == 'activate') ? ' `active`=1' : ' `active`=0';
                $ids = (strlen($actArr['1']) > 0) ? ' (`blocksid` IN ( '.$actArr['1'].' )) && (`catid`='.$catid.') ' : ' (`blocksid`=0) && (`catid`='.$catid.') ';
                $query = 'UPDATE '.$this->db->prefix.'blocks SET '.$activeSql.' WHERE '.$ids.'';
                $this->db->query($query);
                $this->utils->Redirect('?'.$this->module_qs.'=blocks&catid='.$catid);
            } else {
                $this->buildBlocksList($catid);
            }
        } else {
            $active = $this->utils->UserGetInt('active');
            if ($active > 0) {
                $activeSql = ($active == 1) ? ' `active`=0' : ' `active`=1';
                $query = 'UPDATE '.$this->db->prefix.'blocks SET '.$activeSql.' WHERE (`blocksid`='.$blocksid.') && (`catid`='.$catid.') ';
                $this->db->query($query);
                $this->utils->Redirect('?'.$this->module_qs.'=blocks&catid='.$catid);
            } else {
                $cat_name = $this->getCatName($catid);
                $this->template->assign_block_vars('nav', array(
                    'TITLE' => $cat_name,
                    'URL' => '&catid='.$catid,
                ));
                $this->buildBlocksEdit($catid, $blocksid);
            }
        }
    }

    private function buildBlocksList($catid)
    {
        $cat_name = $this->getCatName($catid);
        $this->template->assign_block_vars('nav', array(
            'TITLE' => $cat_name,
            'URL' => '&catid='.$catid,
        ));
        $this->template->assign_block_vars('blocks.list', array(
            'CAT' => $cat_name,
            'CAT_ID' => $catid,
            'NAME' => $this->fromLang('blocks_name'),
            'ACTIVE' => $this->fromLang('blocks_active'),
            'INACTIVE' => $this->fromLang('blocks_inactive'),
            'ACTIVATE' => $this->fromLang('blocks_activate'),
            'INACTIVATE' => $this->fromLang('blocks_inactivate'),
            'ADD' => $this->fromLang('blocks_add'),
            'ADD_URL' => '?'.$this->module_qs.'=blocks&catid='.$catid.'&blocksid=-1',
            'POSITION' => $this->fromLang('cat_position'),
            'URL' => '?'.$this->module_qs.'=blocks&catid='.$catid,
            'EDIT' => $this->fromLang('blocks_edit'),
            'DELETE' => $this->fromLang('blocks_delete'),
            'DELETE_CONFIRM' => $this->fromLang('blocks_confirm'),
        ));
        if ((@$this->user->perm_string['3'] == 1) || (@$this->user->perm_string['0'] == 1)) {
            //Perm for Add
            $this->template->assign_block_vars('blocks.list.perm_add', array());
        }
        if ((@$this->user->perm_string['2'] == 1) || (@$this->user->perm_string['0'] == 1)) {
            //Perm for Edit
            $this->template->assign_block_vars('blocks.list.perm_edit', array());
        }

        if ((@$this->user->perm_string['1'] == 1) || (@$this->user->perm_string['0'] == 1)) {
            //Perm for Del
            $this->template->assign_block_vars('blocks.list.perm_del', array());
        }

        $ndx = 0;
        $hidden_val = '';

        //for paging begin
        $in_page = $this->fromConfig('news_admin_in_page');
        $page = $this->utils->Get('page');
        if (!is_numeric($page)) {
            $page = 1;
        }

        if ($page < 1) {
            $page = 1;
        }

        $start = ($page - 1) * $in_page;
        $end = $in_page;
        //for paging end

        $query = 'SELECT * FROM '.$this->db->prefix."blocks WHERE catid='".$catid."' ORDER BY `position` ASC";
        $this->blocksCount = $this->db->num_rows($query);
        //$query .= $this->db->get_limit($start, $end);

        $result = $this->db->query($query);
        while ($row = $this->db->fetch($result)) {
            $id = $row['blocksid'];
            $position_id = $row['position'];
            $hidden_val .= ($ndx == 0) ? '' : ',';
            $hidden_val .= $id;
            ++$ndx;
            $name = '';
            $comment = '';

            $position = '';

            /* UP */
            $position .= ($row['position'] > 1) ? '<a href="index.php?module=blocks&oper=up&operid='.$id.'&catid='.$catid.'" style="margin-left:10px;"><img src="n_up.gif" alt="Up" title="Up" /></a>' : '<a style="margin-left:20px;"></a>';

            /* DOWN */
            $position .= ($row['position'] < $this->blocksCount) ? '<a href="index.php?module=blocks&oper=down&operid='.$id.'&catid='.$catid.'" style="margin-left:15px;"><img src="n_down.gif" alt="Down" title="Down" /></a>' : '<span></span>';

            $result_loc = $this->db->query('SELECT * FROM '.$this->db->prefix."blockslocalizations WHERE blocksid='".$id."' AND lang='".$this->default_lang."'");
            if ($row_loc = $this->db->fetch($result_loc)) {
                $name = $row_loc['name'];
                $comment = $row_loc['text'];
            }

            $comment = $this->utils->GetPartOfString($comment, 200);

            $this->template->assign_block_vars('blocks.list.items', array(
                'ID' => $id,
                'POSITION_ID' => $position_id,
                'NAME' => $name,
                'COMMENT' => $comment,
                'POSITION' => $position,
                'EDIT_URL' => '?'.$this->module_qs.'=blocks&catid='.$catid.'&blocksid='.$id,
                'ACTIVE_URL' => '?'.$this->module_qs.'=blocks&catid='.$catid.'&blocksid='.$id.'&active=1',
                'INACTIVE_URL' => '?'.$this->module_qs.'=blocks&catid='.$catid.'&blocksid='.$id.'&active=2',
            ));
            if ((@$this->user->perm_string['2'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                //Perm for Edit
                $this->template->assign_block_vars('blocks.list.items.perm_edit', array());
                if ($row['active'] == 1) {
                    $this->template->assign_block_vars('blocks.list.items.active', array());
                } else {
                    $this->template->assign_block_vars('blocks.list.items.inactive', array());
                }
            }

            if ((@$this->user->perm_string['1'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                //Perm for Del
                $this->template->assign_block_vars('blocks.list.items.perm_del', array());
            }
        }
        $this->template->assign_block_vars('blocks.list.hidden', array(
            'VALUE' => $hidden_val,
        ));

        $this->buildPaging();
    }

    private function buildPaging()
    {
        $arr = $this->getPagingArray($this->blocksCount);

        /*
        print("<pre>");
        print_r($arr);
        print("</pre>");
        */

        $prev_url = '';

        if ($arr['PREV_LINK_PAGE']) {
            $prev_url = 'href="'.$arr['prefix'].$arr['PREV_LINK_PAGE'].'"';
        }

        $next_url = '';

        if ($arr['NEXT_LINK_PAGE']) {
            $next_url = 'href="'.$arr['prefix'].$arr['NEXT_LINK_PAGE'].'"';
        }

        $this->template->assign_block_vars('blocks.list.paging', array(
            'PREV_URL' => $prev_url,
            'NEXT_URL' => $next_url, ));

        /*
        print("<pre>");
        print_r($arr["PAGE_NUMBERS"]);
        print("</pre>");
        */

		$count = (is_array($arr['PAGE_NUMBERS'])) ? count($arr['PAGE_NUMBERS']) : 0;
        for ($i = 0; $i < $count; ++$i) {
            $num = $arr['PAGE_NUMBERS'][$i];
            $num_url = '';
            $sep = false;
            if ($i < ($count - 1)) {
                $sep = true;
            }

            if ($arr['CURRENT_PAGE'] != $num) {
                $num_url = 'href="'.$arr['prefix'].$num.'"';
            }

            $this->template->assign_block_vars('blocks.list.paging.numbers', array(
                'URL' => $num_url,
                'TEXT' => $num, ));

            if ($sep) {
                $this->template->assign_block_vars('blocks.list.paging.numbers.sep', array());
            }
        }
    }

    private function getPagingArray($count)
    {
        $in_page = $this->fromConfig('news_admin_in_page');

        $page_count = @ceil($count / $in_page);

        //paging begin
        $new_url = $_SERVER['REQUEST_URI'];
        $new_url = $this->utils->removeQueryString($new_url, 'page');
        $new_url .= '&';

        $paging = new \mcms5xx\classes\PagedResults();
        $paging->TotalResults = $count;
        $paging->ResultsPerPage = $in_page;
        $paging->LinksPerPage = 10;
        $paging->PageVarName = 'page';
        $paging->UrlPrefix = $new_url;
        //paging end

        $arr = $paging->InfoArray();
        $arr['prefix'] = $paging->Prefix;

        return $arr;
    }

    private function buildBlocksEdit($catid, $blocksid)
    {
        $active_chk = 'checked';
        $query = 'SELECT * FROM '.$this->db->prefix."blocks WHERE blocksid='".$blocksid."'";
        $result = $this->db->query($query);
        if ($row = $this->db->fetch($result)) {
            $active_chk = ($row['active'] == 1) ? 'checked' : '';
        }

        $this->template->assign_block_vars('blocks.edit', array(
            'ACTIVE' => $this->fromLang('blocks_active'),
            'ACTIVE_CHK' => $active_chk,
            'TAB_HEADER_WIDTH' => (count($this->langs) * 75),
            'SAVE' => $this->fromLang('blocks_save'),
            'CANCEL' => $this->fromLang('blocks_cancel'),
            'URL' => '?'.$this->module_qs.'=blocks&catid='.$catid,
            'ID' => $blocksid,
            'CATID' => $catid,
        ));
        $ndx = 0;
        foreach ($this->langs as $key => $value) {
            ++$ndx;
            $lang = $value;
            $name = $link_name = $img = $img02 = $img_url = $text = $url = '';
            $class = ($ndx == 1) ? ' class="active"' : '';
            $fade_class = ($ndx == 1) ? ' in active' : '';

            $query = 'SELECT * FROM '.$this->db->prefix."blockslocalizations WHERE blocksid='".$blocksid."' AND lang='".$lang."'";
            $result = $this->db->query($query);
            if ($row = $this->db->fetch($result)) {
                $name = $this->ekranFilter($row['name']);
                $link_name = $this->ekranFilter($row['link_name']);
                $img = $row['img'];
                $img02 = $row['img02'];
                $img_url = $row['img_url'];
                $text = $row['text'];
                $url = $row['url'];
                $text = $this->rteSafe($text);
            }

            $this->template->assign_block_vars('blocks.edit.tab', array(
                'LANG' => $lang,
                'CLASS' => $class,
                'FADE_CLASS' => $fade_class,
                'IMG' => $this->fromLang('blocks_img'),
                'IMG_VALUE' => $img,
                'IMG02' => $this->fromLang('blocks_img02'),
                'IMG02_VALUE' => $img02,
                'IMG_URL' => $this->fromLang('blocks_img_url'),
                'IMG_URL_VALUE' => $img_url,
                'NAME' => $this->fromLang('blocks_name'),
                'NAME_VALUE' => $name,
                'LINK_NAME' => $this->fromLang('blocks_link_name'),
                'LINK_NAME_VALUE' => $link_name,
                'TEXT' => $this->fromLang('blocks_text'),
                'TEXT_VALUE' => $text,
                'URL' => $this->fromLang('blocks_url'),
                'URL_VALUE' => $url,
            ));
        }
    }

    //endregion

    //region actions
    private function getCatName($catid)
    {
        $name = '';
        $result_loc = $this->db->query('SELECT * FROM '.$this->db->prefix."blockscategorylocalizations WHERE catid='".$catid."' AND lang='".$this->default_lang."'");
        if ($row_loc = $this->db->fetch($result_loc)) {
            $name = $row_loc['name'];
        }

        return $name;
    }

    private function rteSafe($strText)
    {
        //returns safe code for preloading in the RTE
        $tmpString = $strText;

        //convert all types of single quotes
        /*$tmpString = str_replace(chr(145), chr(39), $tmpString);
        $tmpString = str_replace(chr(146), chr(39), $tmpString);
        $tmpString = str_replace("'", "&#39;", $tmpString);*/

        //convert all types of double quotes
        /*$tmpString = str_replace(chr(147), chr(34), $tmpString);
        $tmpString = str_replace(chr(148), chr(34), $tmpString);*/
        //$tmpString = str_replace("\"", "\"", $tmpString);

        //replace carriage returns & line feeds
        /*$tmpString = str_replace(chr(10), " ", $tmpString);
        $tmpString = str_replace(chr(13), " ", $tmpString);*/

        return $tmpString;
    }
    //endregion
}

$blocks = new Blocks();
$blocks->template->pparse('blocks');

/******************* blocks.admin.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** blocks.admin.php ******************/;
