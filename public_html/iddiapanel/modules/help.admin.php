<?php
/******************* help.admin.php *******************
 *
 * Help admin module
 *
 * @author MicroPHP <info@microphp.com>
 * @web www.microphp.com
 * @copyright 2004-2019 MicroPHP
 *
 ******************** help.admin.php ******************/

/** 
 * Define Namespace 
 */
namespace mcms5xx\admin\modules;

/**
 * Checking if module included normally.
 */
if (!defined('mCMScheck')) {
    die('mCMS by MicroPHP');
}

class Help extends \mcms5xx\classes\AdminPage
{
    public function __construct()
    {
        $this->curr_module = 'help';
        parent::__construct();
        $this->onLoad();
    }

    private function onLoad()
    {
        $this->template->assign_vars(array(
            'TITLE' => $this->fromLang('title'),
            'TITLE_SUB' => $this->fromLang('title_sub'),
        ));
        $this->buildMenu();
        $this->loadHelp();
    }

    private function loadHelp()
    {
        foreach ($this->fromLang('blocks') as $id => $values) {
            $class = ($id == 1) ? ' class="active"' : '';
            $active_class = ($id == 1) ? ' active' : '';
            $after = ($id == 1) ? '<span class="after"></span> ' : '';
            $this->template->assign_block_vars('panel', array(
                'NAME' => $values['name'],
                'TAB_ID' => $id,
                'CLASS' => $class,
                'ACTIVE_CLASS' => $active_class,
                'ICON' => $values['icon'],
                'AFTER' => $after,
            ));
            foreach ($values['tabs'] as $tid => $tabsArr) {
                $in = ($tid == 1) ? ' in' : '';
                $this->template->assign_block_vars('panel.tab', array(
                    'ID' => $tid,
                    'IN' => $in,
                    'NAME' => $tabsArr['name'],
                    'TEXT' => $tabsArr['text'],
                ));
            }
        }
    }
}

$help = new Help();
$help->template->pparse('help');

/******************* help.admin.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** help.admin.php ******************/;
