<?php
/******************* subscribe.admin.php *******************
 *
 * Subscribe admin module
 *
 * @author MicroPHP <info@microphp.com>
 * @web www.microphp.com
 * @copyright 2004-2019 MicroPHP
 *
 ******************** subscribe.admin.php ******************/

/** 
 * Define Namespace 
 */
namespace mcms5xx\admin\modules;

/**
 * Checking if module included normally.
 */
if (!defined('mCMScheck')) {
    die('mCMS by MicroPHP');
}

require_once '../m/classes/paging.class.php';

class Subscribe extends \mcms5xx\classes\AdminPage
{
    public function __construct()
    {
        $this->curr_module = 'subscribe';
        parent::__construct();
        $this->onLoad();
    }

    private function onLoad()
    {
        $this->buildMenu();
        $this->doAction();
        $this->buildPage();
    }

    private function doAction()
    {
        //$id = $this->utils->Post('select_id');
        $id = $this->utils->UserPostInt('select_id');

        //echo "<pre>".print_r($_POST, true)."</pre>";
        $cat_action = $this->utils->Post('cat_action');
        $templates_action = $this->utils->Post('templates_action');
        $broadcast_action = $this->utils->Post('broadcast_action');

        if ((@$this->user->perm_string['1'] == 1) || (@$this->user->perm_string['0'] == 1)) {
            //Perm for Del
            switch ($cat_action) {
                case 'delete':
                    $this->deleteCat($id);
                    break;
            }
        }

        switch ($templates_action) {
            case 'save': {
                $this->saveTemplate($id);
                break;
            }
            case 'delete': {
                if ((@$this->user->perm_string['1'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                    //Perm for Del
                    $this->deleteTemplate($id);
                }
                break;
            }
        }

        switch ($broadcast_action) {
            case 'save': {
                $this->saveBroadcast($id);
                break;
            }
            case 'delete': {
                if ((@$this->user->perm_string['1'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                    //Perm for Del
                    $this->deleteBroadcast($id);
                }
                break;
            }
        }
    }

    private function deleteCat($id)
    {
        $this->db->query('DELETE FROM '.$this->db->prefix.'subscribe WHERE sid='.$id);
        $this->user->logOperation($this->user->GetUserId(), 'subscribe', $id, 'subscribe_delete');
    }

    private function saveTemplate($id)
    {
        if ($id == -1) {
            if ((@$this->user->perm_string['3'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                //Perm for Add
                $this->addTemplate();
            }
        } else {
            if ((@$this->user->perm_string['2'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                //Perm for Edit
                $this->updateTemplate($id);
            }
        }

        $this->utils->Redirect('?'.$this->module_qs.'=subscribe&section=templates');
    }

    private function addTemplate()
    {
        $active = $this->utils->UserPostInt('txt_active');

        $query = 'INSERT INTO '.$this->db->prefix.'subscribe_templates (`active`) VALUES('.$active.')';
        $this->db->query($query);
        $inserted_id = $this->db->insert_id();

        $this->addTemplateLocalization($inserted_id);

        $this->user->logOperation($this->user->GetUserId(), 'subscribe', $inserted_id, 'template_add');
    }

    private function addTemplateLocalization($catid)
    {
        $query = 'DELETE FROM `'.$this->db->prefix."subscribe_templateslocalizations` WHERE `tid`='".$catid."'";
        $this->db->query($query);

        foreach ($this->langs as $key => $value) {
            $lang = $value;
            $name = $this->utils->Post('txt_name_'.$lang);
            $template = $this->utils->Post('txt_template_'.$lang);
            $query = 'INSERT INTO '.$this->db->prefix.'subscribe_templateslocalizations(`tid`, `lang`, `name`, `template`) VALUES('.$catid.", '".$lang."', '".$name."', '".$template."')";
            $this->db->query($query);
        }
    }

    private function updateTemplate($id)
    {
        $active = $this->utils->UserPostInt('txt_active');

        $query = 'UPDATE `'.$this->db->prefix."subscribe_templates` SET `active`='".$active."' WHERE tid=".$id.'';
        $this->db->query($query);

        $this->addTemplateLocalization($id);

        $this->user->logOperation($this->user->GetUserId(), 'subscribe', $id, 'template_update');
    }

    // B: Templates

    private function deleteTemplate($id)
    {
        $query = 'DELETE FROM '.$this->db->prefix.'subscribe_templateslocalizations WHERE `tid` = '.$id.'';
        $this->db->query($query);

        $query = 'DELETE FROM '.$this->db->prefix.'subscribe_templates WHERE `tid` = '.$id.'';
        $this->db->query($query);

        $this->user->logOperation($this->user->GetUserId(), 'subscribe', $id, 'template_delete');

        $this->utils->Redirect('?'.$this->module_qs.'=subscribe&section=templates');
    }

    private function saveBroadcast($id)
    {
        if ($id == -1) {
            if ((@$this->user->perm_string['3'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                //Perm for Add
                $this->addBroadcast();
            }
        } else {
            if ((@$this->user->perm_string['2'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                //Perm for Edit
                $this->updateBroadcast($id);
            }
        }
        $this->utils->Redirect('?'.$this->module_qs.'=subscribe&section=broadcast');
    }

    private function addBroadcast()
    {
        $tid = $this->utils->UserPostInt('txt_template');
        $date = time();
        $startdate = $this->utils->Post('startdate');
        $date_arr = explode('/', $startdate);
        if (count($date_arr) == 3) {
            $d = $date_arr[0];
            $m = $date_arr[1];
            $y = $date_arr[2];
            if (is_numeric($d) && is_numeric($m) && is_numeric($y)) {
                $date = mktime(0, 0, 0, $m, $d, $y);
            }
        }
        $active = $this->utils->UserPostInt('txt_active');

        $query = 'INSERT INTO '.$this->db->prefix.'subscribe_broadcast (`bid`, `tid`, `startdate`, `active`)
		VALUES(NULL, '.$tid.',  '.((int) $date).',  '.$active.')';
        $this->db->query($query);
        $inserted_id = $this->db->insert_id();

        if ($inserted_id > 0) {
            $result = $this->db->query('SELECT * FROM '.$this->db->prefix.'subscribe WHERE `active`=1');
            while ($row = $this->db->fetch($result)) {
                $sid = $row['sid'];
                $sbm_query = 'INSERT INTO '.$this->db->prefix.'subscribe_broadcast_mails (`bid`, `sid`) VALUES('.$inserted_id.', '.$sid.')';
                $this->db->query($sbm_query);
            }
        }
        $this->addBroadcastLocalization($inserted_id);

        $this->user->logOperation($this->user->GetUserId(), 'subscribe', $inserted_id, 'broadcast_add');
    }

    private function addBroadcastLocalization($bid)
    {
        $query = 'DELETE FROM `'.$this->db->prefix."subscribe_broadcastlocalizations` WHERE `bid`='".$bid."'";
        $this->db->query($query);

        foreach ($this->langs as $key => $value) {
            $lang = $value;
            $text = $this->utils->Post('txt_template_'.$lang);
            $query = 'INSERT INTO '.$this->db->prefix.'subscribe_broadcastlocalizations(`bid`, `lang`, `text`) VALUES('.$bid.", '".$lang."', '".$text."')";
            $this->db->query($query);
        }
    }

    private function updateBroadcast($id)
    {
        $tid = $this->utils->UserPostInt('txt_template');
        $date = time();
        $startdate = $this->utils->Post('startdate');
        $date_arr = explode('/', $startdate);
        if (count($date_arr) == 3) {
            $d = $date_arr[0];
            $m = $date_arr[1];
            $y = $date_arr[2];
            if (is_numeric($d) && is_numeric($m) && is_numeric($y)) {
                $date = mktime(0, 0, 0, $m, $d, $y);
            }
        }
        $active = $this->utils->UserPostInt('txt_active');

        $query = 'UPDATE `'.$this->db->prefix.'subscribe_broadcast`
			SET
				`tid` = '.$tid.',
				`startdate` = '.((int) $date).',
				`active` = '.$active.'
			WHERE
				`bid` = '.$id.'';
        $this->db->query($query);

        $this->addBroadcastLocalization($id);
        $this->user->logOperation($this->user->GetUserId(), 'subscribe', $id, 'broadcast_update');
    }

    private function deleteBroadcast($id)
    {
        $query = 'DELETE FROM '.$this->db->prefix.'subscribe_broadcastlocalizations WHERE `bid` = '.$id.'';
        $this->db->query($query);

        $query = 'DELETE FROM '.$this->db->prefix.'subscribe_broadcast WHERE `bid` = '.$id.'';
        $this->db->query($query);

        $query = 'DELETE FROM '.$this->db->prefix.'subscribe_broadcast_mails WHERE `bid` = '.$id.'';
        $this->db->query($query);

        $this->user->logOperation($this->user->GetUserId(), 'subscribe', $id, 'broadcast_delete');
        $this->utils->Redirect('?'.$this->module_qs.'=subscribe&section=broadcast');
    }

    private function buildPage()
    {
        $this->buildMain();

        $section = $this->utils->Get('section');
        switch ($section) {
            case 'list':{
                $this->buildCategory();
                break;
            }
            case 'templates':{
                $this->buildTemplates();
                break;
            }
            case 'broadcast':{
                $this->buildBroadcast();
                break;
            }
            default:{
                $this->template->assign_block_vars('links', array(
                    'TEXT' => $this->fromLang('title_list'),
                    'URL' => '?'.$this->module_qs.'=subscribe&section=list',
                ));
                $this->template->assign_block_vars('links', array(
                    'TEXT' => $this->fromLang('title_templates'),
                    'URL' => '?'.$this->module_qs.'=subscribe&section=templates',
                ));
                $this->template->assign_block_vars('links', array(
                    'TEXT' => $this->fromLang('title_broadcast'),
                    'URL' => '?'.$this->module_qs.'=subscribe&section=broadcast',
                ));
                break;
            }
        }
    }

    private function buildMain()
    {
        $this->template->assign_var('TITLE', $this->fromLang('title'));
    }
    // E: Templates

    // B: Broadcast

    private function buildCategory()
    {
        $this->template->assign_block_vars('navigation', array(
            'TITLE' => $this->fromLang('title_list'),
            'URL' => '&section=list',
        ));
        $this->template->assign_block_vars('cat', array(
            'TITLE' => $this->fromLang('title_list'),
        ));
        $cat_edit_id = $this->utils->Get('cateditid');
        if (!is_numeric($cat_edit_id) || $cat_edit_id == 0) {
            /* $this->buildCategoryOperation(); */
            if (strlen($cat_edit_id) > 5) {
                $actArr = explode(':', $cat_edit_id);
                $activeSql = ($actArr['0'] == 'activate') ? ' `active`=1' : ' `active`=0';
                $ids = (strlen($actArr['1']) > 0) ? ' `sid` IN ( '.$actArr['1'].' )' : ' `sid`=0';
                $query = 'UPDATE '.$this->db->prefix.'subscribe SET '.$activeSql.' WHERE '.$ids.'';
                $this->db->query($query);
                $this->utils->Redirect('?'.$this->module_qs.'=subscribe&section=list');
            } else {
                $this->buildCatList();
            }
        } else {
            $active = $this->utils->UserGetInt('active');
            if ($active > 0) {
                $activeSql = ($active == 1) ? ' `active`=0' : ' `active`=1';
                $query = 'UPDATE '.$this->db->prefix.'subscribe SET '.$activeSql." WHERE sid='".$cat_edit_id."'";
                $this->db->query($query);
                $this->utils->Redirect('?'.$this->module_qs.'=subscribe&section=list');
            } else {
                //$this->buildCatEdit($cat_edit_id);
            }
        }
    }

    private function buildCatList()
    {
        $subscribers_total = $subscribers_active = 0;
        $stats_result = $this->db->query('SELECT
			(SELECT COUNT(sid) FROM '.$this->db->prefix.'subscribe ) AS subscribers_total,
			(SELECT COUNT(sid) FROM '.$this->db->prefix.'subscribe WHERE active = 1) AS subscribers_active
		');
        while ($stats_row = $this->db->fetch($stats_result)) {
            $subscribers_total = $stats_row['subscribers_total'];
            $subscribers_active = $stats_row['subscribers_active'];
        }
        $this->template->assign_block_vars('cat.list', array(
            'CAT' => $this->fromLang('cat_title'),
            'URL' => '?'.$this->module_qs.'=subscribe',
            'EMAIL' => $this->fromLang('email'),
            'NAME' => $this->fromLang('name'),
            'PHONE' => $this->fromLang('phone'),
            'LANG' => $this->fromLang('lang'),
            'DATE' => $this->fromLang('date'),
            'EDIT' => $this->fromLang('edit'),
            'DELETE' => $this->fromLang('delete'),
            'DELETE_CONFIRM' => $this->fromLang('confirm'),
            'ACTIVE' => $this->fromLang('active'),
            'INACTIVE' => $this->fromLang('inactive'),
            'ACTIVATE' => $this->fromLang('activate'),
            'INACTIVATE' => $this->fromLang('inactivate'),
            'SUBSCRIBERS_TOTAL_NM' => $this->fromLang('subscribers_total'),
            'SUBSCRIBERS_ACTIVE_NM' => $this->fromLang('subscribers_active'),
            'SUBSCRIBERS_TOTAL' => $subscribers_total,
            'SUBSCRIBERS_ACTIVE' => $subscribers_active,
        ));
        if ((@$this->user->perm_string['1'] == 1) || (@$this->user->perm_string['0'] == 1)) {
            //Perm for Del
            $this->template->assign_block_vars('cat.list.perm_del', array());
        }
        $dateformat = $this->fromConfig('dateformat');
        $sql = 'SELECT * FROM '.$this->db->prefix.'subscribe ORDER BY sid DESC';
        $result = $this->db->query($sql);
        $cat_sayi = $this->db->num_rows($sql);

        while ($row = $this->db->fetch($result)) {
            $id = $row['sid'];
            $name = $row['name'];
            $phone = $row['phone'];
            $email = $row['email'];
            $lang = $row['lang'];
            $date = date($dateformat, $row['add_date']);

            $this->template->assign_block_vars('cat.list.items', array(
                'EMAIL' => $email,
                'NAME' => $name,
                'PHONE' => $phone,
                'DATE' => $date,
                'LANG' => $lang,
                'ID' => $id,
                'ACTIVE_URL' => '?'.$this->module_qs.'=subscribe&section=list&cateditid='.$id.'&active=1',
                'INACTIVE_URL' => '?'.$this->module_qs.'=subscribe&section=list&cateditid='.$id.'&active=2',
            ));
            if ((@$this->user->perm_string['2'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                //Perm for Edit
                if ($row['active'] == 1) {
                    $this->template->assign_block_vars('cat.list.items.active', array());
                } else {
                    $this->template->assign_block_vars('cat.list.items.inactive', array());
                }
            }
            if ((@$this->user->perm_string['1'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                //Perm for Del
                $this->template->assign_block_vars('cat.list.items.perm_del', array());
            }
        }
    }

    private function buildTemplates()
    {
        $this->template->assign_block_vars('navigation', array(
            'TITLE' => $this->fromLang('title_templates'),
            'URL' => '&section=templates',
        ));
        $this->template->assign_block_vars('templates', array());

        $templatesid = $this->utils->Get('templatesid');
        $templates_id = $this->utils->Get('templates_id');
        //print_r($_GET);exit();
        if ((!is_numeric($templatesid) || $templatesid == 0) && (!is_numeric($templates_id) || $templates_id == 0)) {
            if (strlen($templatesid) > 5) {
                $actArr = explode(':', $templatesid);
                $activeSql = ($actArr['0'] == 'activate') ? ' `active`=1' : ' `active`=0';
                $ids = (strlen($actArr['1']) > 0) ? ' (`tid` IN ( '.$actArr['1'].' )) ' : ' (`tid`=0) ';
                $query = 'UPDATE '.$this->db->prefix.'subscribe_templates SET '.$activeSql.' WHERE '.$ids.'';
                //echo($query);exit();
                $this->db->query($query);
                $this->utils->Redirect('?'.$this->module_qs.'=subscribe&section=templates');
            } else {
                $this->buildTemplatesList();
            }
        } else {
            $active = $this->utils->UserGetInt('active');
            if ($active > 0) {
                $activeSql = ($active == 1) ? ' `active`=0' : ' `active`=1';
                $query = 'UPDATE '.$this->db->prefix.'subscribe_templates SET '.$activeSql.' WHERE (`tid`='.$templatesid.')';
                $this->db->query($query);
                $this->utils->Redirect('?'.$this->module_qs.'=subscribe&section=templates');
            } else {
                $this->buildTemplatesEdit($templatesid);
            }
        }
    }

    private function buildTemplatesList()
    {
        $this->template->assign_block_vars('templates.list', array(
            'TITLE' => $this->fromLang('title_templates'),
            'NAME' => $this->fromLang('templates_name'),
            'ACTIVE' => $this->fromLang('active'),
            'INACTIVE' => $this->fromLang('inactive'),
            'ACTIVATE' => $this->fromLang('activate'),
            'INACTIVATE' => $this->fromLang('inactivate'),
            'ADD' => $this->fromLang('templates_add'),
            'ADD_URL' => '?'.$this->module_qs.'=subscribe&section=templates&templatesid=-1',
            'URL' => '?'.$this->module_qs.'=subscribe&section=templates',
            'EDIT' => $this->fromLang('edit'),
            'DELETE' => $this->fromLang('delete'),
            'DELETE_CONFIRM' => $this->fromLang('confirm'),
        ));
        if ((@$this->user->perm_string['3'] == 1) || (@$this->user->perm_string['0'] == 1)) {
            //Perm for Add
            $this->template->assign_block_vars('templates.list.perm_add', array());
        }
        if ((@$this->user->perm_string['2'] == 1) || (@$this->user->perm_string['0'] == 1)) {
            //Perm for Edit
            $this->template->assign_block_vars('templates.list.perm_edit', array());
        }
        if ((@$this->user->perm_string['1'] == 1) || (@$this->user->perm_string['0'] == 1)) {
            //Perm for Del
            $this->template->assign_block_vars('templates.list.perm_del', array());
        }

        $ndx = 0;
        $hidden_val = '';

        //for paging begin
        $in_page = $this->fromConfig('news_admin_in_page');
        $page = $this->utils->Get('page');
        if (!is_numeric($page)) {
            $page = 1;
        }

        if ($page < 1) {
            $page = 1;
        }

        $start = ($page - 1) * $in_page;
        $end = $in_page;
        //for paging end

        $query = 'SELECT ST.*, STL.`name`
			FROM '.$this->db->prefix.'subscribe_templates ST
			INNER JOIN '.$this->db->prefix."subscribe_templateslocalizations STL ON STL.tid = ST.tid AND STL.lang='".$this->default_lang."'
			ORDER BY STL.name ASC";
        $this->blocksCount = $this->db->num_rows($query);
        //$query .= $this->db->get_limit($start, $end);
        //echo ($query);

        $result = $this->db->query($query);
        while ($row = $this->db->fetch($result)) {
            $id = $row['tid'];
            $hidden_val .= ($ndx == 0) ? '' : ',';
            $hidden_val .= $id;
            ++$ndx;
            $name = $row['name'];

            $this->template->assign_block_vars('templates.list.items', array(
                'NAME' => $name,
                'POSITION' => $ndx,
                'EDIT_URL' => '?'.$this->module_qs.'=subscribe&section=templates&templatesid='.$id,
                'ACTIVE_URL' => '?'.$this->module_qs.'=subscribe&section=templates&templatesid='.$id.'&active=1',
                'INACTIVE_URL' => '?'.$this->module_qs.'=subscribe&section=templates&templatesid='.$id.'&active=2',
                'ID' => $id,
            ));
            if ((@$this->user->perm_string['2'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                //Perm for Edit
                $this->template->assign_block_vars('templates.list.items.perm_edit', array());

                if ($row['active'] == 1) {
                    $this->template->assign_block_vars('templates.list.items.active', array());
                } else {
                    $this->template->assign_block_vars('templates.list.items.inactive', array());
                }
            }
            if ((@$this->user->perm_string['1'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                //Perm for Del
                $this->template->assign_block_vars('templates.list.items.perm_del', array());
            }
        }
        $this->template->assign_block_vars('templates.list.hidden', array(
            'VALUE' => $hidden_val,
        ));
    }

    private function buildTemplatesEdit($cat_edit_id)
    {
        $cat_query = 'SELECT * FROM '.$this->db->prefix."subscribe_templates WHERE tid='".$cat_edit_id."' ";
        $cat_result = $this->db->query($cat_query);
        $cat_row = $this->db->fetch($cat_result);

        $active_chk = (($cat_row['active'] == 1) || ($cat_edit_id == -1)) ? 'checked' : '';

        $this->template->assign_block_vars('templates.edit', array(
            'TAB_HEADER_WIDTH' => (count($this->langs) * 75),
            'ACTIVE' => $this->fromLang('templates_active'),
            'ACTIVE_CHK' => $active_chk,
            'SAVE' => $this->fromLang('templates_save'),
            'CANCEL' => $this->fromLang('templates_cancel'),
            'URL' => '?'.$this->module_qs.'=subscribe',
            'ID' => $cat_edit_id,
        ));

        $ndx = 0;
        foreach ($this->langs as $key => $value) {
            ++$ndx;
            $lang = $value;
            $name = $template = '';
            $class = ($ndx == 1) ? ' class="active"' : '';
            $fade_class = ($ndx == 1) ? ' in active' : '';

            $query = 'SELECT * FROM '.$this->db->prefix."subscribe_templateslocalizations WHERE tid='".$cat_edit_id."' AND lang='".$lang."'";
            $result = $this->db->query($query);
            if ($row = $this->db->fetch($result)) {
                $name = $row['name'];
                $template = $row['template'];
            }

            $this->template->assign_block_vars('templates.edit.tab', array(
                'LANG' => $lang,
                'CLASS' => $class,
                'FADE_CLASS' => $fade_class,
                'NAME' => $this->fromLang('templates_name'),
                'NAME_VALUE' => $name,
                'TEMPLATE' => $this->fromLang('templates_template'),
                'TEMPLATE_VALUE' => $template,
            ));
        }
    }

    private function buildBroadcast()
    {
        $this->template->assign_block_vars('navigation', array(
            'TITLE' => $this->fromLang('title_broadcast'),
            'URL' => '&section=broadcast',
        ));
        $this->template->assign_block_vars('broadcast', array());

        $broadcastid = $this->utils->Get('broadcastid');
        $broadcast_id = $this->utils->Get('broadcast_id');
        if ((!is_numeric($broadcastid) || $broadcastid == 0) && (!is_numeric($broadcast_id) || $broadcast_id == 0)) {
            if (strlen($broadcastid) > 5) {
                $actArr = explode(":", $broadcastid);
                $activeSql = ($actArr['0']=='activate') ? " `active`=1" : " `active`=0";
                $ids = (strlen($actArr['1'])>0) ? " (`bid` IN ( ".$actArr['1']." ))  " : " (`bid`=0) ";
                $query = "UPDATE ".$this->db->prefix."subscribe_broadcast SET ".$activeSql." WHERE ".$ids."";
                //echo$query;exit();
                $this->db->query($query);
                $this->utils->Redirect("?".$this->module_qs."=subscribe&section=broadcast");
            } else {
                $this->buildBroadcastList();
            }
        } else {
            $active = $this->utils->UserGetInt('active');
            if ($active > 0) {
                $activeSql = ($active==1) ? " `active`=0" : " `active`=1";
                $broadcastid = (int) $broadcastid;
                if ($broadcastid>0) {
                    $query = "UPDATE " . $this->db->prefix . "subscribe_broadcast SET " . $activeSql . " WHERE (`bid`=" . $broadcastid . ") ";
                    $this->db->query($query);
                }
                $this->utils->Redirect("?".$this->module_qs."=subscribe&section=broadcast");
            } else {
                $this->buildBroadcastEdit($broadcastid);
            }
        }
    }

    private function buildBroadcastList()
    {
        $this->template->assign_block_vars('broadcast.list', array(
            'TITLE' => $this->fromLang('title_broadcast'),
            'NAME' => $this->fromLang('broadcast_name'),
            'ACTIVE' => $this->fromLang('active'),
            'INACTIVE' => $this->fromLang('inactive'),
            'ACTIVATE' => $this->fromLang('activate'),
            'INACTIVATE' => $this->fromLang('inactivate'),
            'MAIL_STATS' => $this->fromLang('blocks_mail_stats'),
            'ADD' => $this->fromLang('broadcast_add'),
            'ADD_URL' => '?'.$this->module_qs.'=subscribe&section=broadcast&broadcastid=-1',
            'URL' => '?'.$this->module_qs.'=subscribe&section=broadcast',
            'EDIT' => $this->fromLang('edit'),
            'DELETE' => $this->fromLang('delete'),
            'DELETE_CONFIRM' => $this->fromLang('confirm'),
        ));
        if ((@$this->user->perm_string['3'] == 1) || (@$this->user->perm_string['0'] == 1)) {
            //Perm for Add
            $this->template->assign_block_vars('broadcast.list.perm_add', array());
        }
        if ((@$this->user->perm_string['2'] == 1) || (@$this->user->perm_string['0'] == 1)) {
            //Perm for Edit
            $this->template->assign_block_vars('broadcast.list.perm_edit', array());
        }
        if ((@$this->user->perm_string['1'] == 1) || (@$this->user->perm_string['0'] == 1)) {
            //Perm for Del
            $this->template->assign_block_vars('broadcast.list.perm_del', array());
        }

        $ndx = 0;
        $hidden_val = '';

        //for paging begin
        $in_page = $this->fromConfig('news_admin_in_page');
        $page = $this->utils->Get('page');
        if (!is_numeric($page)) {
            $page = 1;
        }

        if ($page < 1) {
            $page = 1;
        }

        $start = ($page - 1) * $in_page;
        $end = $in_page;
        //for paging end

        $query = "SELECT
				ST.*,
				(SELECT COUNT(SBM.bid) FROM ".$this->db->prefix."subscribe_broadcast_mails SBM 
				WHERE 
				    (SBM.bid = ST.bid) AND (SBM.sended=1)) AS sended_mails,
				(SELECT COUNT(SBMO.bid) FROM ".$this->db->prefix."subscribe_broadcast_mails SBMO
				 INNER JOIN " . $this->db->prefix . "subscribe S ON (S.`sid` = SBMO.`sid`) AND (S.`active` = 1) 
				WHERE 
				    (SBMO.bid = ST.bid) AND (SBMO.sended=0)) AS waiting_mails,
				STL.text
			FROM ".$this->db->prefix."subscribe_broadcast ST
			INNER JOIN ".$this->db->prefix."subscribe_broadcastlocalizations STL ON STL.bid = ST.bid AND STL.lang='".$this->default_lang."'
			ORDER BY ST.startdate ASC";
        //echo("<pre>".$query."</pre>");
        $this->blocksCount = $this->db->num_rows($query);
        //$query .= $this->db->get_limit($start, $end);
        //echo ($query);

        $result = $this->db->query($query);
        while ($row = $this->db->fetch($result)) {
            $id = $row['bid'];
            $hidden_val .= ($ndx == 0) ? '' : ',';
            $hidden_val .= $id;
            ++$ndx;
            $name = $row['text'];
            $mail_stats = $row['sended_mails'].'/'.$row['waiting_mails'];
            $this->template->assign_block_vars('broadcast.list.items', array(
                'NAME' => $name,
                'MAIL_STATS' => $mail_stats,
                'EDIT_URL' => '?'.$this->module_qs.'=subscribe&section=broadcast&broadcastid='.$id,
                'ACTIVE_URL' => '?'.$this->module_qs.'=subscribe&section=broadcast&broadcastid='.$id.'&active=1',
                'INACTIVE_URL' => '?'.$this->module_qs.'=subscribe&section=broadcast&broadcastid='.$id.'&active=2',
                'ID' => $id,
            ));
            if ((@$this->user->perm_string['2'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                //Perm for Edit
                $this->template->assign_block_vars('broadcast.list.items.perm_edit', array());
                if ($row['active'] == 1) {
                    $this->template->assign_block_vars('broadcast.list.items.active', array());
                } else {
                    $this->template->assign_block_vars('broadcast.list.items.inactive', array());
                }
            }
            if ((@$this->user->perm_string['1'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                //Perm for Del
                $this->template->assign_block_vars('broadcast.list.items.perm_del', array());
            }
        }
        $this->template->assign_block_vars('broadcast.list.hidden', array(
            'VALUE' => $hidden_val,
        ));
    }

    private function buildBroadcastEdit($cat_edit_id)
    {
        $cat_query = 'SELECT * FROM '.$this->db->prefix."subscribe_broadcast WHERE bid='".$cat_edit_id."' ";
        $cat_result = $this->db->query($cat_query);
        $startdate_value = date('d/m/Y');
        if ($cat_row = $this->db->fetch($cat_result)) {
            $startdate_value = $cat_row['startdate'];
            $startdate_value = date('d/m/Y', $startdate_value);
        }

        $active_chk = (($cat_row['active'] == 1) || ($cat_edit_id == -1)) ? 'checked' : '';

        $this->template->assign_block_vars('broadcast.edit', array(
            'TAB_HEADER_WIDTH' => (count($this->langs) * 75),
            'TEMPLATE' => $this->fromLang('broadcast_template'),
            'STARTDATE' => $this->fromLang('broadcast_startdate'),
            'STARTDATE_VALUE' => $startdate_value,
            'ACTIVE' => $this->fromLang('templates_active'),
            'ACTIVE_CHK' => $active_chk,
            'SAVE' => $this->fromLang('templates_save'),
            'CANCEL' => $this->fromLang('templates_cancel'),
            'URL' => '?'.$this->module_qs.'=subscribe',
            'ID' => $cat_edit_id,
        ));

        $query = 'SELECT ST.*, STL.name
			FROM '.$this->db->prefix.'subscribe_templates ST
			INNER JOIN '.$this->db->prefix."subscribe_templateslocalizations STL ON STL.tid = ST.tid AND STL.lang='".$this->default_lang."'
			ORDER BY STL.name ASC";
        $result = $this->db->query($query);
        while ($row = $this->db->fetch($result)) {
            $id = $row['tid'];
            $name = $row['name'];
            $tSel = ($id == 0) ? 'selected' : '';
            $this->template->assign_block_vars('broadcast.edit.templates', array(
                'TEXT' => $name,
                'VALUE' => $id,
                'SELECTED' => $tSel,
            ));
        }

        $ndx = 0;
        foreach ($this->langs as $key => $value) {
            ++$ndx;
            $lang = $value;
            $text = '';
            $class = ($ndx == 1) ? ' class="active"' : '';
            $fade_class = ($ndx == 1) ? ' in active' : '';

            $query = 'SELECT * FROM '.$this->db->prefix."subscribe_broadcastlocalizations WHERE bid='".$cat_edit_id."' AND lang='".$lang."'";
            $result = $this->db->query($query);
            if ($row = $this->db->fetch($result)) {
                $text = $row['text'];
            }

            $this->template->assign_block_vars('broadcast.edit.tab', array(
                'LANG' => $lang,
                'CLASS' => $class,
                'FADE_CLASS' => $fade_class,
                'TEXT' => $this->fromLang('broadcast_text'),
                'TEXT_VALUE' => $text,
            ));
        }
    }
    // E: Broadcast
}

$subscribe = new Subscribe();
$subscribe->template->pparse('subscribe');

/******************* subscribe.admin.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** subscribe.admin.php ******************/;
