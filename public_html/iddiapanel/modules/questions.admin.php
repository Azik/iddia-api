<?php
/******************* questions.admin.php *******************
 *
 * Questions admin module
 *
 * @author MicroPHP <info@microphp.com>
 * @web www.microphp.com
 * @copyright 2004-2019 MicroPHP
 *
 ******************** questions.admin.php ******************/

/** 
 * Define Namespace 
 */
namespace mcms5xx\admin\modules;

/**
 * Checking if module included normally.
 */
if (!defined('mCMScheck')) {
    die('mCMS by MicroPHP');
}

require_once '../m/classes/paging.class.php';

class questions extends \mcms5xx\classes\AdminPage
{
    protected $questionsCount;

    public function __construct()
    {
        $this->curr_module = 'questions';
        parent::__construct();
        $this->onLoad();
    }

    private function onLoad()
    {
        $this->buildMenu();
        $this->doAction();
        $this->buildPage();
    }

    private function doAction()
    {
        $id = $this->utils->Post('select_id');
		
        if ((@$this->user->perm_string['2'] == 1) || (@$this->user->perm_string['0'] == 1)) {
            //Perm for Edit
            $action = $this->utils->Post('action');
            switch ($action) {
                case 'save_questionscat_options':
                    $this->saveQuestionscatOptions();
                    break;
            }
        }

		

				$cat_action = $this->utils->Post('cat_action');

				switch ($cat_action) {
					case 'delete': {
						if ((@$this->user->perm_string['1'] == 1) || (@$this->user->perm_string['0'] == 1)) {
							//Perm for Del
							$this->deleteCat($id);
						}
						break;
					}
					case 'save': {
						$this->saveCat($id);
						break;
					}
				}

				$questions_action = $this->utils->Post('questions_action');
				$catid = $this->utils->Get('catid');

				switch ($questions_action) {
					case "delete": {
						if ((@$this->user->perm_string['1'] == 1) || (@$this->user->perm_string['0'] == 1)) {
							//Perm for Del
							$this->deleteQuestions($id);
						}
						break;
					}
					case "save": {
						$post_catid = $this->utils->UserPostInt('txt_category');
						$testid = ($post_catid > 0) ? $post_catid : $testid;
						$this->saveQuestions($id, $testid);
						break;
					}
				}
	
	
				$answers_action = $this->utils->Post('answers_action');
				$catid = $this->utils->Get('catid');
				$questionid = $this->utils->Get('questionid');
				$answerid = $this->utils->Get('answerid');

				switch ($answers_action) {
					case "delete": {
						if ((@$this->user->perm_string['1'] == 1) || (@$this->user->perm_string['0'] == 1)) {
							//Perm for Del
							$this->deleteAnswers($id);
						}
						break;
					}
					case "save": {
						//$post_catid = $this->utils->UserPostInt('txt_category');
						//$catid = ($post_catid > 0) ? $post_catid : $catid;
					
						$this->saveAnswers($answerid, $questionid);
						break;
					}
				}	


				$tests_action = $this->utils->Post('tests_action');
				switch ($tests_action) {
					case "delete": {
						if ((@$this->user->perm_string['1'] == 1) || (@$this->user->perm_string['0'] == 1)) {
							//Perm for Del
							$this->deleteTests($id);
						}
						break;
					}
					case "save": {
						$this->saveTests($id, $catid);
						break;
					}
				}


				$correct_answers_action = $this->utils->Post('correct_answers_action');
				$catid = $this->utils->Get('catid');
				$questionid = $this->utils->Get('questionid');
				$answerid = $this->utils->Get('answerid');

				switch ($correct_answers_action) {
					case "delete": {
						if ((@$this->user->perm_string['1'] == 1) || (@$this->user->perm_string['0'] == 1)) {
							//Perm for Del
							$this->deleteCorrectAnswers($id);
						}
						break;
					}
					case "save": {
						//$post_catid = $this->utils->UserPostInt('txt_category');
						//$catid = ($post_catid > 0) ? $post_catid : $catid;
						$testid = $this->utils->Get('testid');
						$this->saveCorrectAnswers($answerid, $testid);
						break;
					}
				}	


				


    }

    private function correct_answers($catid)
    { 
        //$cat_name = $this->getCatName($catid);
       

		$query = 'SELECT catid FROM ' . $this->db->prefix . "tests WHERE testid='" . $catid . "'";
		$cateid = 0;
		$cates_name = "";
		$test_name = "";
		
        $result = $this->db->query($query);
        if ($row = $this->db->fetch($result)) {
			$cates_name = $this->getCatName($row['catid']);
			$test_name = $this->getTestName($catid);
			$cateid = $row['catid'];
		}
		
		
		
		//$cat_name = $this->getQuestionName($catid);
        $this->template->assign_block_vars('correct_answers', array(
			'TEST_NAME' => $test_name,//$questions_name,
            'TEST_ID' => $catid,
            'CATES_NAME' => $cates_name,
            'CATES_ID' => $cateid,
        )); 


        $answerid = $this->utils->Get('answerid');
        $answer_id = $this->utils->Get('answer_id');
        if ((!is_numeric($answerid) || $answerid == 0) && (!is_numeric($answer_id) || $answer_id == 0)) {
            if (strlen($answerid) > 5) {
                $actArr = explode(':', $answerid);
                $activeSql = ($actArr['0'] == 'activate') ? ' `active`=1' : ' `active`=0';
                $ids = (strlen($actArr['1']) > 1) ? ' `answerid` IN ( ' . $actArr['1'] . ' )' : ' `answerid`=0';
                $query = 'UPDATE ' . $this->db->prefix . 'tests_correct_answers SET ' . $activeSql . ' WHERE ' . $ids . '';
                $this->db->query($query);
                $this->utils->Redirect('?' . $this->module_qs . '=questions&mod=answers&questionid=' . $catid);
            } else {
                $this->buildCorrectAnswersList($catid);
            }
        } else {
            $active = $this->utils->UserGetInt('active');
            if ($active > 0) {
                $activeSql = ($active == 1) ? ' `active`=0' : ' `active`=1';
                $query = 'UPDATE ' . $this->db->prefix . 'tests_correct_answers SET ' . $activeSql . " WHERE answerid='" . $answerid . "'";
                $this->db->query($query);
                $this->utils->Redirect('?' . $this->module_qs . '=questions&mod=answers&questionid=' . $catid);
            } else {
                $this->buildCorrectAnswersEdit($catid, $answerid);
            }
        }
	
	
	}
	
	
	
    private function answers($catid)
    { 
        //$cat_name = $this->getCatName($catid);
        $query = 'SELECT testid FROM ' . $this->db->prefix . "questions WHERE questionid='" . $catid . "'";
		$testid = 0;
		$questionid = 0;
		$questions_name = "";
		
        $result = $this->db->query($query);
        if ($row = $this->db->fetch($result)) {
			$questions_name = $this->getTestName($row['testid']);
			$testid = $row['testid'];
		}
		

		$query = 'SELECT catid FROM ' . $this->db->prefix . "tests WHERE testid='" . $testid . "'";
		$cateid = 0;
		$cates_name = "";
		
        $result = $this->db->query($query);
        if ($row = $this->db->fetch($result)) {
			$cates_name = $this->getCatName($row['catid']);
			$cateid = $row['catid'];
		}
		
		
		
		$cat_name = $this->getQuestionName($catid);
        $this->template->assign_block_vars('answers', array(
            'CAT_NAME' => $cat_name,
            'CAT_ID' => $catid,
            'QUESTIONS_NAME' => $questions_name,
            'QUESTIONS_ID' => $testid,
            'CATES_NAME' => $cates_name,
            'CATES_ID' => $cateid,
        ));


        $answerid = $this->utils->Get('answerid');
        $answer_id = $this->utils->Get('answer_id');
        if ((!is_numeric($answerid) || $answerid == 0) && (!is_numeric($answer_id) || $answer_id == 0)) {
            if (strlen($answerid) > 5) {
                $actArr = explode(':', $answerid);
                $activeSql = ($actArr['0'] == 'activate') ? ' `active`=1' : ' `active`=0';
                $ids = (strlen($actArr['1']) > 1) ? ' `answerid` IN ( ' . $actArr['1'] . ' )' : ' `answerid`=0';
                $query = 'UPDATE ' . $this->db->prefix . 'questionanswers SET ' . $activeSql . ' WHERE ' . $ids . '';
                $this->db->query($query);
                $this->utils->Redirect('?' . $this->module_qs . '=questions&mod=answers&questionid=' . $catid);
            } else {
                $this->buildAnswersList($catid);
            }
        } else {
            $active = $this->utils->UserGetInt('active');
            if ($active > 0) {
                $activeSql = ($active == 1) ? ' `active`=0' : ' `active`=1';
                $query = 'UPDATE ' . $this->db->prefix . 'questionanswers SET ' . $activeSql . " WHERE answerid='" . $answerid . "'";
                $this->db->query($query);
                $this->utils->Redirect('?' . $this->module_qs . '=questions&mod=answers&questionid=' . $catid);
            } else {
                $this->buildAnswersEdit($catid, $answerid);
            }
        }
	
	
	}
	
	
	


    private function buildAnswersEdit($catid, $answerid)
    {
		
        //$questionsdate = date('d/m/Y');
        $answersdate = date('Y-m-d H:i');
        $active_chk = $index_chk = 'checked';
		$questionid = $catid;
		$position = 0;
		$variant = '';
        $query = 'SELECT * FROM ' . $this->db->prefix . "questionanswers WHERE answerid='" . $answerid . "'";
		
        $result = $this->db->query($query);
        if ($row = $this->db->fetch($result)) {
            $answersdate = $row['answersdate'];
            $questionid = $row['questionid'];
            $position = $row['position'];
            $variant = $row['variant'];
            //$questionsdate = date('d/m/Y', $questionsdate);
            $answersdate = date('Y-m-d H:i', $answersdate);
            $active_chk = ($row['active'] == 1) ? 'checked' : '';
       }

        $this->template->assign_block_vars('answers.edit', array(
            'DATE' => $this->fromLang('questions_date'),
            'DATE_VALUE' => $answersdate,
            'POSITION_VALUE' => $position,
            'VARIANT_VALUE' => $variant,
			'CATID' => $questionid,
            'ACTIVE' => $this->fromLang('questions_active'),
            'ACTIVE_CHK' => $active_chk,
            'CATEGORY' => $this->fromLang('questions_category'),
            'TAB_HEADER_WIDTH' => (count($this->langs) * 75),
            'SAVE' => $this->fromLang('questions_save'),
            'CANCEL' => $this->fromLang('questions_cancel'),
            'URL' => '?' . $this->module_qs . '=questions&mod=answers&questionid=' . $questionid,
            'ID' => $answerid,
        ));
     
        $cat_sql = 'SELECT 
			N.*, 
			NL.name 
			FROM ' . $this->db->prefix . 'questionanswers N
			INNER JOIN ' . $this->db->prefix . "questionanswerslocalizations NL ON NL.answerid=N.answerid
			WHERE
				NL.lang='" . $this->default_lang . "'
			ORDER BY N.answerid ASC";
        $cat_result = $this->db->query($cat_sql);
        while ($cat_row = $this->db->fetch($cat_result)) {
            $cat_id = $cat_row['answerid'];
            $cSel = ($cat_id == $answerid) ? 'selected' : '';
            $this->template->assign_block_vars('answers.edit.category', array(
                'TEXT' => $cat_row['name'],
                'VALUE' => $cat_id,
                'SELECTED' => $cSel,
            ));
        }


        $ndx = 0;
        foreach ($this->langs as $key => $value) {
            ++$ndx;
            $lang = $value;
            $header = 0;
            $name = $comment = $text = $slug = '';
            $seo_header = $seo_description = $seo_keywords = '';
            $class = ($ndx == 1) ? ' class="active"' : '';
            $fade_class = ($ndx == 1) ? ' in active' : '';

            $query = 'SELECT * FROM ' . $this->db->prefix . "questionanswerslocalizations WHERE answerid='" . $answerid . "' AND lang='" . $lang . "'";
            $result = $this->db->query($query);
            if ($row = $this->db->fetch($result)) {
                $name = $this->ekranFilter($row['name']);

            }

            $this->template->assign_block_vars('answers.edit.tab', array(
                'LANG' => $lang,
                'CLASS' => $class,
                'FADE_CLASS' => $fade_class,
                'NAME' => $this->fromLang('questions_name'),
                'NAME_VALUE' => $name,
                'HEADER' => $this->fromLang('questions_header'),
                'HEADER_VALUE' => $header,
             
            ));
        }
    }	
	
    private function buildCorrectAnswersEdit($catid, $answerid)
    {
		
        //$questionsdate = date('d/m/Y');
        $answersdate = date('Y-m-d H:i');
        $active_chk = $index_chk = 'checked';
		$testid = $catid;
		$variant = '';
        $query = 'SELECT * FROM ' . $this->db->prefix . "tests_correct_answers WHERE answerid='" . $answerid . "'";
		
        $result = $this->db->query($query);
        if ($row = $this->db->fetch($result)) {
            $answersdate = $row['add_date'];
            $testid = $row['testid'];
           $variant = $row['variant'];
            //$questionsdate = date('d/m/Y', $questionsdate);
            $answersdate = date('Y-m-d H:i', $answersdate);
            $active_chk = ($row['active'] == 1) ? 'checked' : '';
       }

        $this->template->assign_block_vars('correct_answers.edit', array(
            'DATE' => $this->fromLang('questions_date'),
            'DATE_VALUE' => $answersdate,
            'VARIANT_VALUE' => $variant,
			'CATID' => $testid,
            'ACTIVE' => $this->fromLang('questions_active'),
            'ACTIVE_CHK' => $active_chk,
            'CATEGORY' => $this->fromLang('questions_category'),
            'TAB_HEADER_WIDTH' => (count($this->langs) * 75),
            'SAVE' => $this->fromLang('questions_save'),
            'CANCEL' => $this->fromLang('questions_cancel'),
            'URL' => '?' . $this->module_qs . '=questions&mod=correct_answers&testid=' . $testid,
            'ID' => $answerid,
        ));
     
        $cat_sql = 'SELECT 
			N.*, 
			NL.name 
			FROM ' . $this->db->prefix . 'tests_correct_answers N
			INNER JOIN ' . $this->db->prefix . "tests_correct_answerslocalizations NL ON NL.answerid=N.answerid
			WHERE
				NL.lang='" . $this->default_lang . "'
			ORDER BY N.answerid ASC";
        $cat_result = $this->db->query($cat_sql);
        while ($cat_row = $this->db->fetch($cat_result)) {
            $cat_id = $cat_row['answerid'];
            $cSel = ($cat_id == $answerid) ? 'selected' : '';
            $this->template->assign_block_vars('correct_answers.edit.category', array(
                'TEXT' => $cat_row['name'],
                'VALUE' => $cat_id,
                'SELECTED' => $cSel,
            ));
        }


        $ndx = 0;
        foreach ($this->langs as $key => $value) {
            ++$ndx;
            $lang = $value;
            $header = 0;
            $name = $comment = $text = $slug = '';
            $seo_header = $seo_description = $seo_keywords = '';
            $class = ($ndx == 1) ? ' class="active"' : '';
            $fade_class = ($ndx == 1) ? ' in active' : '';

            $query = 'SELECT * FROM ' . $this->db->prefix . "tests_correct_answerslocalizations WHERE answerid='" . $answerid . "' AND lang='" . $lang . "'";
            $result = $this->db->query($query);
            if ($row = $this->db->fetch($result)) {
                $name = $this->ekranFilter($row['name']);

            }

            $this->template->assign_block_vars('correct_answers.edit.tab', array(
                'LANG' => $lang,
                'CLASS' => $class,
                'FADE_CLASS' => $fade_class,
                'NAME' => $this->fromLang('questions_name'),
                'NAME_VALUE' => $name,
                'HEADER' => $this->fromLang('questions_header'),
                'HEADER_VALUE' => $header,
             
            ));
        }
    }	
	
	private function deleteCat($id)
    {
		$this->db->delete($this->db->prefix . 'questionscategories', " catid='".$id."'");
		$this->db->delete($this->db->prefix . 'questionscategorylocalizations', " catid='".$id."'");

      
        $result = $this->db->query('SELECT questionid FROM ' . $this->db->prefix . 'questions WHERE catid=' . $id);
        while ($row = $this->db->fetch($result)) {
            $questionid = $row['questionid'];
			$this->db->delete($this->db->prefix . 'questionslocalizations', " questionid='".$id."'");
        }
		$this->db->delete($this->db->prefix . 'questions', " catid='".$id."'");

        $this->user->logOperation($this->user->GetUserId(), 'questions', $id, 'cat_delete');
    }

    private function addCatLocalization($catid)
    {
		$this->db->delete($this->db->prefix . "questionscategorylocalizations ", "catid='" . $catid . "'");

        foreach ($this->langs as $key => $value) {
            $lang = $value;
            $name = $this->utils->Post('txt_name_' . $lang);
			if (strlen($name) > 0) {
				$header = $this->utils->Post('txt_header_' . $lang);

				$dataInsert = array();
				$dataInsert['catid'] = $catid;
				$dataInsert['lang'] = $lang;
				$dataInsert['name'] = $name;
				$dataInsert['header'] = $header;
				$inserted_id = $this->db->insert($this->db->prefix.'questionscategorylocalizations' , $dataInsert);
			}
        }
    }

    private function saveCat($id)
    {
        if ($id == -1) {
            if ((@$this->user->perm_string['3'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                //Perm for Add
                $this->addCat();
            }
        } else {
            if ((@$this->user->perm_string['2'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                //Perm for Edit
                $this->updateCat($id);
            }
        }
        $this->utils->Redirect('?' . $this->module_qs . '=questions');
    }

    private function addCat()
    {
        $result = $this->db->query('SELECT * FROM ' . $this->db->prefix . 'questionscategories ORDER BY position DESC ' . $this->db->get_limit(0, 1));
        $row = $this->db->fetch($result);
        $pos_c = $row['position'] + 1;
        $active = $this->utils->UserPostInt('txt_active');
		
        $index = $this->utils->UserPostInt('txt_index');
        $image = $this->utils->UserPostInt('txt_header');
        $level = $this->utils->UserPostInt('txt_level');
        $icon = $this->utils->Post('txt_icon');
       
		$dataInsert = array();
		$dataInsert['position'] = $pos_c;
		$dataInsert['active'] = $active;
		$dataInsert['index'] = $index;
		$dataInsert['image'] = $image;
		$dataInsert['level'] = $level;
		$dataInsert['icon'] = $icon;
		$inserted_id = $this->db->insert($this->db->prefix.'questionscategories' , $dataInsert);

        $this->addCatLocalization($inserted_id);
        $this->user->logOperation($this->user->GetUserId(), 'questions', $inserted_id, 'cat_add');
    }

    private function updateCat($id)
    {
        $active = $this->utils->UserPostInt('txt_active');
        $image = $this->utils->UserPostInt('txt_header');
        $index = $this->utils->UserPostInt('txt_index');
        $level = $this->utils->UserPostInt('txt_level');
        $icon = $this->utils->Post('txt_icon');


		$dataUpdate = array();
		$dataUpdate['level'] = $level;
		$dataUpdate['image'] = $image;
		$dataUpdate['active'] = $active;
		$dataUpdate['index'] = $index;
		$dataUpdate['icon'] = $icon;
		$this->db->update($this->db->prefix.'questionscategories' , $dataUpdate, " catid=".$id."");

        $this->addCatLocalization($id);
        $this->user->logOperation($this->user->GetUserId(), 'questions', $id, 'cat_update');
    }

    private function deleteQuestions($id)
    {
		$this->db->delete($this->db->prefix . 'questions', " questionid='".$id."'");
		$this->db->delete($this->db->prefix . 'questionslocalizations', " questionid='".$id."'");

        $this->user->logOperation($this->user->GetUserId(), 'questions', $id, 'questions_delete');
    }
	
    private function deleteAnswers($id)
    {
		$this->db->delete($this->db->prefix . 'questionanswers', " answerid='".$id."'");
		$this->db->delete($this->db->prefix . 'questionanswerslocalizations', " answerid='".$id."'");

        $this->user->logOperation($this->user->GetUserId(), 'questions', $id, 'answers_delete');
    }  

	private function deleteCorrectAnswers($id)
    {
		$this->db->delete($this->db->prefix . 'tests_correct_answers', " answerid='".$id."'");
		$this->db->delete($this->db->prefix . 'tests_correct_answerslocalizations', " answerid='".$id."'");

        $this->user->logOperation($this->user->GetUserId(), 'questions', $id, 'correct_answers');
    }  

	

	private function deleteTests($id)
    {
		$this->db->delete($this->db->prefix . 'tests', " testid='".$id."'");
		$this->db->delete($this->db->prefix . 'testslocalizations', " testid='".$id."'");

        $this->user->logOperation($this->user->GetUserId(), 'tests', $id, 'tests_delete');
    }

    private function saveQuestions($id, $catid)
    {
        if ($id == -1) {
            if ((@$this->user->perm_string['3'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                //Perm for Add
                $this->addQuestions($catid);
            }
        } else {
            if ((@$this->user->perm_string['2'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                //Perm for Edit
                $this->updateQuestions($id, $catid);
            }
        }
        $this->utils->Redirect('?' . $this->module_qs . '=questions&mod=questions&testid=' . $catid);
    }
	
	
    private function saveTests($id, $catid)
    {
        if ($id == -1) {
            if ((@$this->user->perm_string['3'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                //Perm for Add
                $this->addTests($catid);
            }
        } else {
            if ((@$this->user->perm_string['2'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                //Perm for Edit
                $this->updateTests($id, $catid);
            }
        }
        $this->utils->Redirect('?' . $this->module_qs . '=questions&mod=tests&catid=' . $catid);
    }
	
	
    private function saveAnswers($id, $questionid)
    {

        if ($id == -1) {
            if ((@$this->user->perm_string['3'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                //Perm for Add
                $this->addAnswers($questionid);
            }
        } else {
            if ((@$this->user->perm_string['2'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                //Perm for Edit
				
                $this->updateAnswers($id, $questionid);
            }
        }
        $this->utils->Redirect('?' . $this->module_qs . '=questions&mod=answers&questionid=' . $questionid);
    }

    private function saveCorrectAnswers($id, $testid)
    {

        if ($id == -1) {
            if ((@$this->user->perm_string['3'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                //Perm for Add
                $this->addCorrectAnswers($testid);
            }
        } else {
            if ((@$this->user->perm_string['2'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                //Perm for Edit
				
                $this->updateCorrectAnswers($id, $testid);
            }
        }
        $this->utils->Redirect('?' . $this->module_qs . '=questions&mod=correct_answers&testid=' . $testid);
    }

    private function addAnswers($questionid)
    {
        $date = time();
        $txt_active = $this->utils->UserPostInt('txt_active');
        $txt_position = $this->utils->UserPostInt('txt_position');
        $txt_variant = $this->utils->Post('txt_variant');
       
		$dataInsert = array();
		$dataInsert['questionid'] = $questionid;
		$dataInsert['answersdate'] = $date;
		$dataInsert['position'] = $txt_position;
		$dataInsert['variant'] = $txt_variant;
		$dataInsert['active'] = $txt_active;
		$inserted_id = $this->db->insert($this->db->prefix.'questionanswers' , $dataInsert);

        $this->addAnswersLocalization($inserted_id);
        $this->user->logOperation($this->user->GetUserId(), 'questionanswers', $inserted_id, 'answers_add');
    }

    private function updateAnswers($id, $questionid = 0)
    {
        $date = time();
        $txt_active = $this->utils->UserPostInt('txt_active');
        $txt_position = $this->utils->UserPostInt('txt_position');
        $txt_variant = $this->utils->Post('txt_variant');
		
		$dataUpdate = array();
		$dataUpdate['questionid'] = $questionid;
		$dataUpdate['position'] = $txt_position;
		$dataUpdate['variant'] = $txt_variant;
		$dataUpdate['active'] = $txt_active;
		$this->db->update($this->db->prefix.'questionanswers' , $dataUpdate, " answerid=".$id."");


        $this->addAnswersLocalization($id);
        $this->user->logOperation($this->user->GetUserId(), 'questions', $id, 'answers_update');
    }

    private function addAnswersLocalization($id)
    {
		$this->db->delete($this->db->prefix . 'questionanswerslocalizations', " answerid='".$id."'");
       
        foreach ($this->langs as $key => $value) {
            $lang = $value;
            $name = $this->utils->Post('txt_name_' . $lang);
			if (strlen($name) > 0) {
				
				$dataInsert = array();
				$dataInsert['answerid'] = $id;
				$dataInsert['name'] = $name;
				$dataInsert['lang'] = $lang;
				$inserted_id = $this->db->insert($this->db->prefix.'questionanswerslocalizations' , $dataInsert);				
				
			}
        }
    }
	
	
	
	
	
	
	
    private function addCorrectAnswers($testid)
    {
        $date = time();
        $txt_active = $this->utils->UserPostInt('txt_active');
        $txt_position = $this->utils->UserPostInt('txt_position');
        $txt_variant = $this->utils->Post('txt_variant');
       
		$dataInsert = array();
		$dataInsert['testid'] = $testid;
		$dataInsert['add_date'] = $date;
		$dataInsert['variant'] = $txt_variant;
		$dataInsert['active'] = $txt_active;
		$inserted_id = $this->db->insert($this->db->prefix.'tests_correct_answers' , $dataInsert);

        $this->addCorrectAnswersLocalization($inserted_id);
        $this->user->logOperation($this->user->GetUserId(), 'tests_correct_answers', $inserted_id, 'correctanswers_add');
    }

    private function updateCorrectAnswers($id, $testid = 0)
    {
		
        $date = time();
        $txt_active = $this->utils->UserPostInt('txt_active');
        $txt_variant = $this->utils->Post('txt_variant');
		
		$dataUpdate = array();
		$dataUpdate['testid'] = $testid;
		$dataUpdate['variant'] = $txt_variant;
		$dataUpdate['active'] = $txt_active;
		$this->db->update($this->db->prefix.'tests_correct_answers' , $dataUpdate, " answerid=".$id."");


        $this->addCorrectAnswersLocalization($id);
        $this->user->logOperation($this->user->GetUserId(), 'questions', $id, 'correct_answers_update');
    }

    private function addCorrectAnswersLocalization($id)
    {
		$this->db->delete($this->db->prefix . 'tests_correct_answerslocalizations', " answerid='".$id."'");
       
        foreach ($this->langs as $key => $value) {
            $lang = $value;
            $name = $this->utils->Post('txt_name_' . $lang);
			if (strlen($name) > 0) {
				
				$dataInsert = array();
				$dataInsert['answerid'] = $id;
				$dataInsert['name'] = $name;
				$dataInsert['lang'] = $lang;
				$inserted_id = $this->db->insert($this->db->prefix.'tests_correct_answerslocalizations' , $dataInsert);				
				
			}
        }
    }
	
	
	
	
	
	
	
    private function addTests($catid)
    {
        $date = time();
        $txt_active = $this->utils->UserPostInt('txt_active');
        $txt_index = $this->utils->UserPostInt('txt_index');
        $txt_header = $this->utils->UserPostInt('txt_header');

		$dataInsert = array();
		$dataInsert['catid'] = $catid;
		$dataInsert['testdate'] = $date;
		$dataInsert['image'] = $txt_header;
		$dataInsert['active'] = $txt_active;
		$inserted_id = $this->db->insert($this->db->prefix.'tests' , $dataInsert);

        $this->addTestsLocalization($inserted_id);
        $this->user->logOperation($this->user->GetUserId(), 'tests', $inserted_id, 'tests_add');
    }


    private function addTestsLocalization($testid)
    {
		$this->db->delete($this->db->prefix . 'testslocalizations', " testid='".$testid."'");
       
        foreach ($this->langs as $key => $value) {
            $lang = $value;
            $name = $this->utils->Post('txt_name_' . $lang);
            $header = $this->utils->Post('txt_header_' . $lang);
			if (strlen($name) > 0) {
				
				$dataInsert = array();
				$dataInsert['testid'] = $testid;
				$dataInsert['name'] = $name;
				$dataInsert['header'] = $header;
				$dataInsert['lang'] = $lang;
				$inserted_id = $this->db->insert($this->db->prefix.'testslocalizations' , $dataInsert);				
				
			}
        }
    }
	
	
    private function updateTests($id, $catid = 0)
    {
        $date = time();
        $txt_active = $this->utils->UserPostInt('txt_active');
        $txt_header = $this->utils->UserPostInt('txt_header');

		$dataUpdate = array();
		$dataUpdate['catid'] = $catid;
//		$dataUpdate['questionsdate'] = $date;
		$dataUpdate['header'] = $txt_header;
		$dataUpdate['active'] = $txt_active;
		$this->db->update($this->db->prefix.'tests' , $dataUpdate, " testid=".$id."");


        $this->addTestsLocalization($id);
        $this->user->logOperation($this->user->GetUserId(), 'tests', $id, 'tests_update');
    }
	
    private function addQuestions($catid)
    {
        $date = time();
        $txt_active = $this->utils->UserPostInt('txt_active');
        $txt_index = $this->utils->UserPostInt('txt_index');
        $txt_header = $this->utils->UserPostInt('txt_header');
        $txt_answers_limit = $this->utils->UserPostInt('txt_answers_limit');
    
		$dataInsert = array();
		$dataInsert['testid'] = $catid;
		$dataInsert['questionsdate'] = $date;
		$dataInsert['answers_limit'] = $txt_answers_limit;
		$dataInsert['header'] = $txt_header;
		$dataInsert['active'] = $txt_active;
		$inserted_id = $this->db->insert($this->db->prefix.'questions' , $dataInsert);

        $this->addQuestionsLocalization($inserted_id);
        $this->user->logOperation($this->user->GetUserId(), 'questions', $inserted_id, 'questions_add');
    }

    private function updateQuestions($id, $catid = 0)
    {
        $date = time();
        $txt_active = $this->utils->UserPostInt('txt_active');
        $txt_header = $this->utils->UserPostInt('txt_header');
        $txt_answers_limit = $this->utils->UserPostInt('txt_answers_limit');

		$dataUpdate = array();
		$dataUpdate['testid'] = $catid;
		$dataUpdate['answers_limit'] = $txt_answers_limit;
//		$dataUpdate['questionsdate'] = $date;
		$dataUpdate['header'] = $txt_header;
		$dataUpdate['active'] = $txt_active;
		$this->db->update($this->db->prefix.'questions' , $dataUpdate, " questionid=".$id."");


        $this->addQuestionsLocalization($id);
        $this->user->logOperation($this->user->GetUserId(), 'questions', $id, 'questions_update');
    }

    private function addQuestionsLocalization($questionid)
    {
		$this->db->delete($this->db->prefix . 'questionslocalizations', " questionid='".$questionid."'");
        /*$this->site->deleteFromSearch($questionid, 'questions');*/

        foreach ($this->langs as $key => $value) {
            $lang = $value;
            $name = $this->utils->Post('txt_name_' . $lang);
			if (strlen($name) > 0) {
				
				$dataInsert = array();
				$dataInsert['questionid'] = $questionid;
				$dataInsert['name'] = $name;
				$dataInsert['lang'] = $lang;
				$inserted_id = $this->db->insert($this->db->prefix.'questionslocalizations' , $dataInsert);				
				
			}
        }
    }

    private function buildPage()
    {
        $this->buildMain();

		
		$mod = $this->utils->Get('mod');
		if($mod == 'questions') {
			$testid = $this->utils->Get('testid');
			$this->buildQuestions($testid);
			//$this->buildCatOptions();
		}
		else if($mod == 'tests') {
			$catid = $this->utils->Get('catid');
			$this->buildTests($catid);
		}
		else if($mod == 'answers') {
			$questionid = $this->utils->Get('questionid');
			$this->answers($questionid);
		}
		else if($mod == 'correct_answers') {
			$testid = $this->utils->Get('testid');
			$this->correct_answers($testid);
		}
		else {

			$catid = $this->utils->Get('catid');
			$testid = $this->utils->Get('testid');
			
			if (!is_numeric($testid) || $testid == 0) {
				$this->buildCategory();
			} 
		}
    }

    private function buildMain()
    {
        $this->template->assign_var('TITLE', $this->fromLang('title'));
        $this->template->assign_var('CAT_CONFIRM', $this->fromLang('cat_confirm'));
        $this->template->assign_var('QUESTIONS_CONFIRM', $this->fromLang('questions_confirm'));
    }

    private function buildCategory()
    {
        $this->template->assign_block_vars('cat', array());

        $cat_edit_id = $this->utils->Get('cateditid');
        if (!is_numeric($cat_edit_id) || $cat_edit_id == 0) {

            if (strlen($cat_edit_id) > 5) {
                $actArr = explode(':', $cat_edit_id);
                $activeSql = ($actArr['0'] == 'activate') ? ' `active`=1' : ' `active`=0';
                $ids = (strlen($actArr['1']) > 0) ? ' `catid` IN ( ' . $actArr['1'] . ' )' : ' `catid`=0';
                $query = 'UPDATE ' . $this->db->prefix . 'questionscategories SET ' . $activeSql . ' WHERE ' . $ids . '';
                $this->db->query($query);
                $this->utils->Redirect('?' . $this->module_qs . '=questions');
            } else {
                $this->buildCatList();
            }
        } else {
            $active = $this->utils->UserGetInt('active');
            if ($active > 0) {
                $activeSql = ($active == 1) ? ' `active`=0' : ' `active`=1';
                $query = 'UPDATE ' . $this->db->prefix . 'questionscategories SET ' . $activeSql . " WHERE catid='" . $cat_edit_id . "'";
                $this->db->query($query);
                $this->utils->Redirect('?' . $this->module_qs . '=questions');
            } else {
                if ($cat_edit_id > 0) {
                    if ((@$this->user->perm_string['2'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                        //Perm for Edit
                        $this->buildCatEdit($cat_edit_id);
                    }
                } else {
                    if ((@$this->user->perm_string['3'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                        //Perm for Add
                        $this->buildCatEdit($cat_edit_id);
                    }
                }
            }
        }
    }

    private function buildCatList()
    {
        $this->template->assign_block_vars('cat_list', array());
        $this->template->assign_block_vars('cat.list', array(
            'CAT' => $this->fromLang('cat_title'),
            'LAST_questions' => $this->fromLang('last_questions'),
            'LAST_questions_URL' => '?' . $this->module_qs . '=questions&catid=-1',
            'ADD' => $this->fromLang('cat_add'),
            'ADD_URL' => '?' . $this->module_qs . '=questions&cateditid=-1',
            'ADD_questions' => $this->fromLang('questions_add'),
            'ADD_questions_URL' => '?' . $this->module_qs . '=questions&catid=-1&questionid=-1',
            'ACTIVE' => "".$this->fromLang('cat_active'),
            'INDEX' => $this->fromLang('cat_index'),
            'INACTIVE' => $this->fromLang('questions_inactive'),
            'ACTIVATE' => $this->fromLang('questions_activate'),
            'INACTIVATE' => $this->fromLang('questions_inactivate'),
            'URL' => '?' . $this->module_qs . '=questions&mod=tests',
            'NAME' => $this->fromLang('cat_name'),
            'HEADER' => $this->fromLang('cat_header'),
            'POSITION' => $this->fromLang('cat_position'),
            'EDIT' => $this->fromLang('cat_edit'),
            'DELETE' => $this->fromLang('cat_delete'),
            'DELETE_CONFIRM' => $this->fromLang('cat_confirm'),
        ));
        if ((@$this->user->perm_string['2'] == 1) || (@$this->user->perm_string['0'] == 1)) {
            //Perm for Edit
            $this->template->assign_block_vars('cat.list.perm_edit', array());
        }

        if ((@$this->user->perm_string['3'] == 1) || (@$this->user->perm_string['0'] == 1)) {
            //Perm for Add
            $this->template->assign_block_vars('cat.list.perm_add', array());
        }
        $ndx = 0;
        $hidden_val = '';

        $sql = 'SELECT 
			N.*, 
			NL.name, 
			NL.header
			FROM ' . $this->db->prefix . 'questionscategories N
			INNER JOIN ' . $this->db->prefix . "questionscategorylocalizations NL ON NL.catid=N.catid
			WHERE
				NL.lang='" . $this->default_lang . "'
			ORDER BY N.position ASC";
        $result = $this->db->query($sql);
        $cat_sayi = $this->db->num_rows($sql);
        while ($row = $this->db->fetch($result)) {
            $id = $row['catid'];
            $position = $row['position'];
            $hidden_val .= ($ndx == 0) ? '' : ',';
            $hidden_val .= $id;
            ++$ndx;
            $name = $this->utils->GetPartOfString($row['name'], 50);
            $header = $this->utils->GetPartOfString($row['header'], 50);
            $active = ($row['active'] == 1) ? $this->fromLang('cat_yes') : $this->fromLang('cat_no');
            $this->template->assign_block_vars('cat.list.items', array(
                'ID' => $id,
                'POSITION' => $position,
                'NAME' => $name,
                'HEADER' => $header,
                'ACTIVE' => $active,
                'EDIT_URL' => '?' . $this->module_qs . '=questions&cateditid=' . $id,
                'ACTIVE_URL' => '?' . $this->module_qs . '=questions&cateditid=' . $id . '&active=1',
                'INACTIVE_URL' => '?' . $this->module_qs . '=questions&cateditid=' . $id . '&active=2',
            ));
            if ((@$this->user->perm_string['1'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                //Perm for Del
                $this->template->assign_block_vars('cat.list.items.perm_del', array());
            }
            if ((@$this->user->perm_string['2'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                //Perm for Edit
                $this->template->assign_block_vars('cat.list.items.perm_edit', array());

                if ($row['active'] == 1) {
                    $this->template->assign_block_vars('cat.list.items.active', array());
                } else {
                    $this->template->assign_block_vars('cat.list.items.inactive', array());
                }
            }
        }
        $this->template->assign_block_vars('cat.list.hidden', array(
            'VALUE' => $hidden_val,
        ));
    }

    private function buildCatEdit($cat_edit_id)
    {
        $cat_query = 'SELECT * FROM ' . $this->db->prefix . "questionscategories WHERE catid='" . $cat_edit_id . "' ";
        $cat_result = $this->db->query($cat_query);
        $cat_row = $this->db->fetch($cat_result);

        $active_chk = (($cat_row['active'] == 1) || ($cat_edit_id == -1)) ? 'checked' : '';
        $index_chk = (($cat_row['index'] == 1) || ($cat_edit_id == -1)) ? 'checked' : '';
        $image = $cat_row['image'];
        $icon = $cat_row['icon'];
        $level = $cat_row['level'];
        $this->template->assign_block_vars('cat_catedit', array());
        $this->template->assign_block_vars('cat.catedit', array(
            'TAB_HEADER_WIDTH' => (count($this->langs) * 75),
            'ACTIVE' => $this->fromLang('cat_active'),
            'ACTIVE_CHK' => $active_chk,
            'INDEX' => $this->fromLang('cat_index'),
            'INDEX_CHK' => $index_chk,
            'ICON_VALUE' => $icon,
            'HEADER_VALUE' => $image,
            'SAVE' => $this->fromLang('cat_save'),
            'CANCEL' => $this->fromLang('cat_cancel'),
            'URL' => '?' . $this->module_qs . '=questions',
            'ID' => $cat_edit_id,
        ));


        $tSel = ($level == 0) ? 'selected' : '';
        $this->template->assign_block_vars('cat.catedit.levels', array(
            'TEXT' => $this->fromLang('questions_template_default'),
            'VALUE' => 0,
            'SELECTED' => $tSel,
        ));
        $level_views = $this->fromConfig('levels');
        foreach ($level_views as $tid => $tname) {
            $tSel = ($level == $tid) ? 'selected' : '';
            $this->template->assign_block_vars('cat.catedit.levels', array(
                'TEXT' => $tname,
                'VALUE' => $tid,
                'SELECTED' => $tSel,
            ));
        }


        $ndx = 0;
        foreach ($this->langs as $key => $value) {
            ++$ndx;
            $lang = $value;
            $name = '';
            $header = '';
            $class = ($ndx == 1) ? ' class="active"' : '';
            $fade_class = ($ndx == 1) ? ' in active' : '';

            $query = 'SELECT * FROM ' . $this->db->prefix . "questionscategorylocalizations WHERE catid='" . $cat_edit_id . "' AND lang='" . $lang . "'";
            $result = $this->db->query($query);
            if ($row = $this->db->fetch($result)) {
                $name = $row['name'];
                $header = $row['header'];
            }

            $this->template->assign_block_vars('cat.catedit.tab', array(
                'LANG' => $lang,
                'CLASS' => $class,
                'FADE_CLASS' => $fade_class,
                'NAME' => $this->fromLang('cat_name'),
                'NAME_VALUE' => $name,
                'HEADER' => $this->fromLang('cat_header'),
                'HEADER_VALUE' => $header,
            ));
        }
    }

    private function buildCatOptions()
    {
        $this->template->assign_block_vars('questionscat_options', array(
            'TITLE' => $this->fromLang('questionscat_options'),
            'ANSWERS_LIMIT_VALUE' => '10',          
            'SAVE' => $this->fromLang('questionscat_options_save'),
            'CANCEL' => $this->fromLang('questionscat_options_cancel'),
        ));
    }

    private function saveQuestionscatOptions()
    {
		$testid = $this->utils->Get('testid');
        $txt_answers_limit = $this->utils->UserPostInt('txt_answers_limit');
        $this->user->logOperation($this->user->GetUserId(), 'questions', 0, 'questions_options_update');
    }

    private function buildTests($catid)
    {
        $cat_name = $this->getCatName($catid);
        $this->template->assign_block_vars('tests', array(
            'CAT_NAME' => $cat_name,
            'CAT_ID' => $catid,
        ));

        $testid = $this->utils->Get('testid');
        $test_id = $this->utils->Get('test_id');
        if ((!is_numeric($testid) || $testid == 0) && (!is_numeric($test_id) || $test_id == 0)) {
            if (strlen($testid) > 5) {
                $actArr = explode(':', $testid);
                $activeSql = ($actArr['0'] == 'activate') ? ' `active`=1' : ' `active`=0';
                $ids = (strlen($actArr['1']) > 1) ? ' `testid` IN ( ' . $actArr['1'] . ' )' : ' `testid`=0';
                $query = 'UPDATE ' . $this->db->prefix . 'tests SET ' . $activeSql . ' WHERE ' . $ids . '';
                $this->db->query($query);
                $this->utils->Redirect('?' . $this->module_qs . '=questions&mod=tests&catid=' . $catid);
            } else {
                $this->buildTestsList($catid);
            }
        } else {
            $active = $this->utils->UserGetInt('active');
            if ($active > 0) {
                $activeSql = ($active == 1) ? ' `active`=0' : ' `active`=1';
                $query = 'UPDATE ' . $this->db->prefix . 'tests SET ' . $activeSql . " WHERE testid='" . $testid . "'";
                $this->db->query($query);
                $this->utils->Redirect('?' . $this->module_qs . '=questions&mod=tests&catid=' . $catid);
            } else {
                $this->buildTestsEdit($catid, $testid);
            }
        }
    }
	


    private function buildTestsList($catid)
    {
        $cat_name = $this->getCatName($catid);

        $this->template->assign_block_vars('cat_list', array());
        $this->template->assign_block_vars('test_list', array());
        $this->template->assign_block_vars('tests.list', array(
            'CAT' => $cat_name,
            'CATID' => $catid,
            'NAME' => $this->fromLang('questions_name'),
            'ACTIVE' => $this->fromLang('questions_active'),
            'READ_DATE' => $this->fromLang('questions_read_date'),
            'TOTAL_VIEWS' => $this->fromLang('questions_total_views'),
            'MONTH_VIEWS' => $this->fromLang('questions_month_views'),
            'INACTIVE' => $this->fromLang('questions_inactive'),
            'ACTIVATE' => $this->fromLang('questions_activate'),
            'INACTIVATE' => $this->fromLang('questions_inactivate'),
            'ADD' => $this->fromLang('questions_add'),
            'ADD_URL' => '?' . $this->module_qs . '=questions&mod=tests&catid=' . $catid . '&testid=-1',
            'DATE' => $this->fromLang('questions_date'),
            'COMMENT' => $this->fromLang('questions_comment'),
            'URL' => '?' . $this->module_qs . '=questions&catid=' . $catid,
            'EDIT' => $this->fromLang('questions_edit'),
            'DELETE' => $this->fromLang('questions_delete'),
            'DELETE_CONFIRM' => $this->fromLang('questions_confirm'),
        ));

        if ((@$this->user->perm_string['2'] == 1) || (@$this->user->perm_string['0'] == 1)) {
            //Perm for Edit
            $this->template->assign_block_vars('tests.list.perm_edit', array());
        }
        if ((@$this->user->perm_string['3'] == 1) || (@$this->user->perm_string['0'] == 1)) {
            //Perm for Add
            $this->template->assign_block_vars('tests.list.perm_add', array());
        }
   
        $cat_sql = ($catid>0) ? " WHERE  (N.catid='" . $catid . "') " : "";

        $query = "SELECT
          N.*,
          IF ((NL.`name`!=''), NL.`name`, NLIN.`name`) as `name`
        FROM " . $this->db->prefix . 'tests N
        LEFT JOIN ' . $this->db->prefix . "testslocalizations NL ON (NL.testid = N.testid) AND (NL.lang='" . $this->default_lang . "')
        LEFT JOIN " . $this->db->prefix . "testslocalizations NLIN ON ( NLIN.testid = N.testid ) AND (NLIN.name != '')
        ".$cat_sql."
        GROUP BY N.testid
        ORDER BY N.testdate DESC";

        //echo("<pre>\n\n\n\n\n".$query."</pre>");
        //$this->questionsCount = $this->db->num_rows($query);
        //$query .= $this->db->get_limit($start, $end);
        $result = $this->db->query($query);
        while ($row = $this->db->fetch($result)) {
            $id = $row['testid'];
            $date = $row['testdate'];
            $name = $row['name'];
            $active = ($row['active'] == 1) ? $this->fromLang('questions_yes') : $this->fromLang('questions_no');
            $index = ($row['index'] == 1) ? $this->fromLang('questions_yes') : $this->fromLang('questions_no');


            $this->template->assign_block_vars('tests.list.items', array(
                'NAME' => $name,
                'ACTIVE' => $active,
                'INDEX' => $index,
                'DATE' => $this->utils->formattedDate($date),
                'EDIT_URL' => '?' . $this->module_qs . '=questions&mod=questions&testid=' . $id,
                'CORRECT_ANSWERS_URL' => '?' . $this->module_qs . '=questions&mod=correct_answers&testid=' . $id,
                'ACTIVE_URL' => '?' . $this->module_qs . '=questions&mod=tests&catid=' . $catid . '&testid=' . $id . '&active=1',
                'INACTIVE_URL' => '?' . $this->module_qs . '=questions&mod=tests&catid=' . $catid . '&testid=' . $id . '&active=2',
                'ID' => $id,
            ));
            if ((@$this->user->perm_string['1'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                //Perm for Del
                $this->template->assign_block_vars('tests.list.items.perm_del', array());
            }
            if ((@$this->user->perm_string['2'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                //Perm for Edit
                $this->template->assign_block_vars('tests.list.items.perm_edit', array());

                if ($row['active'] == 1) {
                    $this->template->assign_block_vars('tests.list.items.active', array());
                } else {
                    $this->template->assign_block_vars('tests.list.items.inactive', array());
                }
            }
        }

    }
	
    private function buildQuestions($testid)
    {
        $cat_name = $this->getTestName($testid);

        $query = 'SELECT catid FROM ' . $this->db->prefix . "tests WHERE testid='" . $testid . "'";
		$questionid = 0;
		$questions_name = "";
		
        $result = $this->db->query($query);
        if ($row = $this->db->fetch($result)) {
			$cats_name = $this->getCatName($row['catid']);
			$catid = $row['catid'];
		}



        $this->template->assign_block_vars('questions', array(
            'CAT_NAME' => $cat_name,
            'CAT_ID' => $testid,
            'CAT1_NAME' => $cats_name,
            'CAT1_ID' => $catid,
        ));

        $questionid = $this->utils->Get('questionid');
        $question_id = $this->utils->Get('question_id');
        if ((!is_numeric($questionid) || $questionid == 0) && (!is_numeric($question_id) || $question_id == 0)) {
            if (strlen($questionid) > 5) {
                $actArr = explode(':', $questionid);
                $activeSql = ($actArr['0'] == 'activate') ? ' `active`=1' : ' `active`=0';
                $ids = (strlen($actArr['1']) > 1) ? ' `questionid` IN ( ' . $actArr['1'] . ' )' : ' `questionid`=0';
                $query = 'UPDATE ' . $this->db->prefix . 'questions SET ' . $activeSql . ' WHERE ' . $ids . '';
                $this->db->query($query);
                $this->utils->Redirect('?' . $this->module_qs . '=questions&testid=' . $questionid);
            } else {
                $this->buildQuestionsList($testid);
            }
        } else {
					

            $active = $this->utils->UserGetInt('active');
			
            if ($active > 0) {
                $activeSql = ($active == 1) ? ' `active`=0' : ' `active`=1';
			
                $query = 'UPDATE ' . $this->db->prefix . 'questions SET ' . $activeSql . " WHERE questionid='" . $questionid . "'";
                $this->db->query($query);
                $this->utils->Redirect('?' . $this->module_qs . '=questions&mod=questions&testid=' . $testid);
            } else {
                $this->buildQuestionsEdit($testid, $questionid);
            }
        }
    }

    private function buildQuestionsList($testid)
    {
		
        $cat_name = $this->getTestName($testid);

        $this->template->assign_block_vars('cat_list', array());
        $this->template->assign_block_vars('questions_list', array());
        $this->template->assign_block_vars('questions.list', array(
            'CAT' => $cat_name,
            'CATID' => $testid,
            'NAME' => $this->fromLang('questions_name'),
            'ACTIVE' => $this->fromLang('questions_active'),
            'READ_DATE' => $this->fromLang('questions_read_date'),
            'TOTAL_VIEWS' => $this->fromLang('questions_total_views'),
            'MONTH_VIEWS' => $this->fromLang('questions_month_views'),
            'INACTIVE' => $this->fromLang('questions_inactive'),
            'ACTIVATE' => $this->fromLang('questions_activate'),
            'INACTIVATE' => $this->fromLang('questions_inactivate'),
            'ADD' => $this->fromLang('questions_add'),
            'ADD_URL' => '?' . $this->module_qs . '=questions&mod=questions&testid=' . $testid . '&questionid=-1',
            'DATE' => $this->fromLang('questions_date'),
            'COMMENT' => $this->fromLang('questions_comment'),
            'URL' => '?' . $this->module_qs . '=questions&testid=' . $testid,
            'EDIT' => $this->fromLang('questions_edit'),
            'DELETE' => $this->fromLang('questions_delete'),
            'DELETE_CONFIRM' => $this->fromLang('questions_confirm'),
        ));

        if ((@$this->user->perm_string['2'] == 1) || (@$this->user->perm_string['0'] == 1)) {
            //Perm for Edit
            $this->template->assign_block_vars('questions.list.perm_edit', array());
        }
        if ((@$this->user->perm_string['3'] == 1) || (@$this->user->perm_string['0'] == 1)) {
            //Perm for Add
            $this->template->assign_block_vars('questions.list.perm_add', array());
        }
        //for paging begin
        $in_page = $this->fromConfig('questions_admin_in_page');
        $page = $this->utils->UserGetInt('page');
        $page = ($page <= 0) ? 1 : $page;

        $start = ($page - 1) * $in_page;
        $end = $in_page;
        //for paging end

        $cat_sql = ($testid>0) ? " WHERE  (N.testid='" . $testid . "') " : "";

        $query = "SELECT
          N.*,
          IF ((NL.`name`!=''), NL.`name`, NLIN.`name`) as `name`
        FROM " . $this->db->prefix . 'questions N
        LEFT JOIN ' . $this->db->prefix . "questionslocalizations NL ON (NL.questionid = N.questionid) AND (NL.lang='" . $this->default_lang . "')
        LEFT JOIN " . $this->db->prefix . "questionslocalizations NLIN ON ( NLIN.questionid = N.questionid ) AND (NLIN.name != '')
        ".$cat_sql."
        GROUP BY N.questionid
        ORDER BY N.questionsdate DESC";


        //echo("<pre>\n\n\n\n\n".$query."</pre>");
        //$this->questionsCount = $this->db->num_rows($query);
        $query .= $this->db->get_limit(0, 0);
        //$query .= $this->db->get_limit($start, $end);
        $result = $this->db->query($query);
        while ($row = $this->db->fetch($result)) {
            $id = $row['questionid'];
            $date = $row['questionsdate'];
            $name = $row['name'];
            $comment = $row['text'];
            $read_date = (int)$row['read_date'];
            $total_views = (int)$row['total_views'];
            $month_views = (int)$row['month_views'];
            $active = ($row['active'] == 1) ? $this->fromLang('questions_yes') : $this->fromLang('questions_no');
            $index = ($row['index'] == 1) ? $this->fromLang('questions_yes') : $this->fromLang('questions_no');

            $comment = $this->utils->GetPartOfString($comment, 200);

            $this->template->assign_block_vars('questions.list.items', array(
                'NAME' => $name,
                'ACTIVE' => $active,
                'INDEX' => $index,
                'READ_DATE' => (($read_date > 0) ? $this->utils->formattedDate($read_date) : '-'),
                'TOTAL_VIEWS' => $total_views,
                'MONTH_VIEWS' => $month_views,
                'COMMENT' => $comment,
                'DATE' => $this->utils->formattedDate($date),
                'EDIT_URL' => '?' . $this->module_qs . '=questions&mod=questions&testid=' . $testid . '&questionid=' . $id,
                'ACTIVE_URL' => '?' . $this->module_qs . '=questions&mod=questions&testid=' . $testid . '&questionid=' . $id . '&active=1',
                'INACTIVE_URL' => '?' . $this->module_qs . '=questions&mod=questions&testid=' . $testid . '&questionid=' . $id . '&active=2',
                'ID' => $id,
            ));
            if ((@$this->user->perm_string['1'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                //Perm for Del
                $this->template->assign_block_vars('questions.list.items.perm_del', array());
            }
            if ((@$this->user->perm_string['2'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                //Perm for Edit
                $this->template->assign_block_vars('questions.list.items.perm_edit', array());

                if ($row['active'] == 1) {
                    $this->template->assign_block_vars('questions.list.items.active', array());
                } else {
                    $this->template->assign_block_vars('questions.list.items.inactive', array());
                }
            }
        }

        $this->buildPaging();
    }

    private function buildAnswersList($catid)
    {

        $this->template->assign_block_vars('answers.list', array(
            'CAT' => "",
            'CATID' => $catid,
            'NAME' => $this->fromLang('questions_name'),
            'ACTIVE' => $this->fromLang('questions_active'),
            'INACTIVE' => $this->fromLang('questions_inactive'),
            'ACTIVATE' => $this->fromLang('questions_activate'),
            'INACTIVATE' => $this->fromLang('questions_inactivate'),
            'ADD' => $this->fromLang('questions_add'),
            'ADD_URL' => '?' . $this->module_qs . '=questions&mod=answers&answerid=-1&questionid='.$catid,
            'DATE' => $this->fromLang('questions_date'),
            'URL' => '?' . $this->module_qs . '=questions&catid=' . $catid,
            'EDIT' => $this->fromLang('questions_edit'),
            'DELETE' => $this->fromLang('questions_delete'),
            'DELETE_CONFIRM' => $this->fromLang('questions_confirm'),
        ));

		
        $cat_sql = " WHERE  (N.questionid='" . $catid . "') ";
        $query = "SELECT
          N.*,
          IF ((NL.`name`!=''), NL.`name`, NLIN.`name`) as `name`
        FROM " . $this->db->prefix . 'questionanswers N
        LEFT JOIN ' . $this->db->prefix . "questionanswerslocalizations NL ON (NL.answerid = N.answerid) AND (NL.lang='" . $this->default_lang . "')
        LEFT JOIN " . $this->db->prefix . "questionanswerslocalizations NLIN ON ( NLIN.answerid = N.answerid ) AND (NLIN.name != '')
        ".$cat_sql."
        GROUP BY N.answerid
        ORDER BY N.answersdate DESC";
		
		

        //echo("<pre>\n\n\n\n\n".$query."</pre>");
        //$this->questionsCount = $this->db->num_rows($query);
        //$query .= $this->db->get_limit(0, 0);
        //$query .= $this->db->get_limit($start, $end);
        $result = $this->db->query($query);
        while ($row = $this->db->fetch($result)) {
            $id = $row['answerid'];
            $date = $row['answersdate'];
            $name = $row['name'];
            $variant = $row['variant'];
        
            $active = ($row['active'] == 1) ? $this->fromLang('questions_yes') : $this->fromLang('questions_no');
           
           
            $this->template->assign_block_vars('answers.list.items', array(
                'NAME' => $name,
                'ACTIVE' => $active,
                'VARIANT' => $variant,
                'DATE' => $this->utils->formattedDate($date),
                'EDIT_URL' => '?' . $this->module_qs . '=questions&questionid=' . $catid . '&mod=answers&answerid=' . $id,
                'ACTIVE_URL' => '?' . $this->module_qs . '=questions&questionid=' . $catid . '&mod=answers&answerid=' . $id . '&active=1',
                'INACTIVE_URL' => '?' . $this->module_qs . '=questions&questionid=' . $catid . '&mod=answers&answerid=' . $id . '&active=2',
                'ID' => $id,
            ));
            if ((@$this->user->perm_string['1'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                //Perm for Del
                $this->template->assign_block_vars('answers.list.items.perm_del', array());
            }
            if ((@$this->user->perm_string['2'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                //Perm for Edit
                $this->template->assign_block_vars('answers.list.items.perm_edit', array());

                if ($row['active'] == 1) {
                    $this->template->assign_block_vars('answers.list.items.active', array());
                } else {
                    $this->template->assign_block_vars('answers.list.items.inactive', array());
                }
            }
        }		
		
		
		
    }

    private function buildCorrectAnswersList($catid)
    {

        $this->template->assign_block_vars('correct_answers.list', array(
            'CAT' => "",
            'CATID' => $catid,
            'NAME' => $this->fromLang('questions_name'),
            'ACTIVE' => $this->fromLang('questions_active'),
            'INACTIVE' => $this->fromLang('questions_inactive'),
            'ACTIVATE' => $this->fromLang('questions_activate'),
            'INACTIVATE' => $this->fromLang('questions_inactivate'),
            'ADD' => $this->fromLang('questions_add'),
            'ADD_URL' => '?' . $this->module_qs . '=questions&mod=correct_answers&answerid=-1&testid='.$catid,
            'DATE' => $this->fromLang('questions_date'),
            'URL' => '?' . $this->module_qs . '=questions&catid=' . $catid,
            'EDIT' => $this->fromLang('questions_edit'),
            'DELETE' => $this->fromLang('questions_delete'),
            'DELETE_CONFIRM' => $this->fromLang('questions_confirm'),
        ));

		
        $cat_sql = " WHERE  (N.testid='" . $catid . "') ";
        $query = "SELECT
          N.*,
          IF ((NL.`name`!=''), NL.`name`, NLIN.`name`) as `name`
        FROM " . $this->db->prefix . 'tests_correct_answers N
        LEFT JOIN ' . $this->db->prefix . "tests_correct_answerslocalizations NL ON (NL.answerid = N.answerid) AND (NL.lang='" . $this->default_lang . "')
        LEFT JOIN " . $this->db->prefix . "tests_correct_answerslocalizations NLIN ON ( NLIN.answerid = N.answerid ) AND (NLIN.name != '')
        ".$cat_sql."
        GROUP BY N.answerid
        ORDER BY N.add_date DESC";
	
		

        $result = $this->db->query($query);
        while ($row = $this->db->fetch($result)) {
            $id = $row['answerid'];
            $date = $row['add_date'];
            $name = $row['name'];
            $variant = $row['variant'];
        
            $active = ($row['active'] == 1) ? $this->fromLang('questions_yes') : $this->fromLang('questions_no');
           
           
            $this->template->assign_block_vars('correct_answers.list.items', array(
                'NAME' => $name,
                'ACTIVE' => $active,
                'VARIANT' => $variant,
                'DATE' => $this->utils->formattedDate($date),
                'EDIT_URL' => '?' . $this->module_qs . '=questions&testid=' . $catid . '&mod=correct_answers&answerid=' . $id,
                'ACTIVE_URL' => '?' . $this->module_qs . '=questions&testid=' . $catid . '&mod=correct_answers&answerid=' . $id . '&active=1',
                'INACTIVE_URL' => '?' . $this->module_qs . '=questions&testid=' . $catid . '&mod=correct_answers&answerid=' . $id . '&active=2',
                'ID' => $id,
            ));
            if ((@$this->user->perm_string['1'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                //Perm for Del
                $this->template->assign_block_vars('correct_answers.list.items.perm_del', array());
            }
            if ((@$this->user->perm_string['2'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                //Perm for Edit
                $this->template->assign_block_vars('correct_answers.list.items.perm_edit', array());

                if ($row['active'] == 1) {
                    $this->template->assign_block_vars('correct_answers.list.items.active', array());
                } else {
                    $this->template->assign_block_vars('correct_answers.list.items.inactive', array());
                }
            }
        }		
		
		
		
    }

    private function buildPaging()
    {
        $arr = $this->getPagingArray($this->questionsCount);

        /* print("<pre>");
        echo "\n\n\n\n".$this->questionsCount;
        print_r($arr);
        print("</pre>"); */

        $prev_url = '';
        $prev_num = 0;

        if ($arr['PREV_LINK_PAGE']) {
            $prev_url = 'href="' . $arr['prefix'] . $arr['PREV_LINK_PAGE'] . '"';
            $prev_num = $arr['PREV_LINK_PAGE'];
        }

        $next_url = '';
        $next_num = 0;

        if ($arr['NEXT_LINK_PAGE']) {
            $next_url = 'href="' . $arr['prefix'] . $arr['NEXT_LINK_PAGE'] . '"';
            $next_num = $arr['NEXT_LINK_PAGE'];
        }

        $this->template->assign_block_vars('questions.list.paging', array(
            'PREV_URL' => $prev_url,
            'PREV_NUM' => $prev_num,
            'NEXT_URL' => $next_url,
            'NEXT_NUM' => $next_num,
        ));

        /*
        print("<pre>");
        print_r($arr["PAGE_NUMBERS"]);
        print("</pre>");
        */

		$count = (is_array($arr['PAGE_NUMBERS'])) ? count($arr['PAGE_NUMBERS']) : 0;
        for ($i = 0; $i < $count; ++$i) {
            $num = $arr['PAGE_NUMBERS'][$i];
            $num_url = '';
            $sep = false;
            if ($i < ($count - 1)) {
                $sep = true;
            }

            if ($arr['CURRENT_PAGE'] != $num) {
                $num_url = 'href="' . $arr['prefix'] . $num . '"';
            }

            $this->template->assign_block_vars('questions.list.paging.numbers', array(
                'URL' => $num_url,
                'TEXT' => $num,));

            if ($sep) {
                $this->template->assign_block_vars('questions.list.paging.numbers.sep', array());
            }
        }
    }

    private function getPagingArray($count)
    {
        $in_page = $this->fromConfig('questions_admin_in_page');

        $page_count = @ceil($count / $in_page);

        //paging begin
        $new_url = $_SERVER['REQUEST_URI'];
        $new_url = $this->utils->removeQueryString($new_url, 'page');
        $new_url .= '&';

        $paging = new \mcms5xx\classes\PagedResults();
        $paging->TotalResults = $count;
        $paging->ResultsPerPage = $in_page;
        $paging->LinksPerPage = 10;
        $paging->PageVarName = 'page';
        $paging->UrlPrefix = $new_url;
        //paging end

        $arr = $paging->InfoArray();
        $arr['prefix'] = $paging->Prefix;

        return $arr;
    }

    private function buildQuestionsEdit($testid, $questionid)
    {
        //$questionsdate = date('d/m/Y');
        $questionsdate = date('Y-m-d H:i');
        $active_chk = $index_chk = 'checked';
		$header = 0;
		$answers_limit = 0;
        $query = 'SELECT * FROM ' . $this->db->prefix . "questions WHERE questionid='" . $questionid . "'";
        $result = $this->db->query($query);
        if ($row = $this->db->fetch($result)) {
            $questionsdate = $row['questionsdate'];
            $header = $row['header'];
            $answers_limit = $row['answers_limit'];
            //$questionsdate = date('d/m/Y', $questionsdate);
            $questionsdate = date('Y-m-d H:i', $questionsdate);
            $active_chk = ($row['active'] == 1) ? 'checked' : '';
       }
        $this->template->assign_block_vars('cat_catedit', array());

        $this->template->assign_block_vars('questions.edit', array(
            'DATE' => $this->fromLang('questions_date'),
            'DATE_VALUE' => $questionsdate,
            'HEADER_VALUE' => $header,
            'ANSWERS_LIMIT_VALUE' => $answers_limit,
            'CATID' => $testid,
            'ACTIVE' => $this->fromLang('questions_active'),
            'ACTIVE_CHK' => $active_chk,
            'CATEGORY' => $this->fromLang('questions_category'),
            'TAB_HEADER_WIDTH' => (count($this->langs) * 75),
            'SAVE' => $this->fromLang('questions_save'),
            'CANCEL' => $this->fromLang('questions_cancel'),
            'URL' => '?' . $this->module_qs . '=questions&catid=' . $testid,
            'ID' => $questionid,
        ));
     

        $cat_sql = 'SELECT 
			N.*, 
			NL.name, 
			NL.header
			FROM ' . $this->db->prefix . 'tests N
			INNER JOIN ' . $this->db->prefix . "testslocalizations NL ON NL.testid=N.testid
			WHERE
				NL.lang='" . $this->default_lang . "'
			ORDER BY N.position ASC";
		
        $cat_result = $this->db->query($cat_sql);
        while ($cat_row = $this->db->fetch($cat_result)) {
            $cat_id = $cat_row['testid'];
            $cSel = ($cat_id == $testid) ? 'selected' : '';
            $this->template->assign_block_vars('questions.edit.category', array(
                'TEXT' => $cat_row['name'],
                'VALUE' => $cat_id,
                'SELECTED' => $cSel,
            ));
        }


        $ndx = 0;
        foreach ($this->langs as $key => $value) {
            ++$ndx;
            $lang = $value;
            $header = 0;
            $name = $comment = $text = $slug = '';
            $seo_header = $seo_description = $seo_keywords = '';
            $class = ($ndx == 1) ? ' class="active"' : '';
            $fade_class = ($ndx == 1) ? ' in active' : '';

            $query = 'SELECT * FROM ' . $this->db->prefix . "questionslocalizations WHERE questionid='" . $questionid . "' AND lang='" . $lang . "'";
            $result = $this->db->query($query);
            if ($row = $this->db->fetch($result)) {
                $name = $this->ekranFilter($row['name']);

            }

            $this->template->assign_block_vars('questions.edit.tab', array(
                'LANG' => $lang,
                'CLASS' => $class,
                'FADE_CLASS' => $fade_class,
                'NAME' => $this->fromLang('questions_name'),
                'NAME_VALUE' => $name,
                'HEADER' => $this->fromLang('questions_header'),
                'HEADER_VALUE' => $header,
             
            ));
        }
    }

    private function buildTestsEdit($catid, $testid)
    {
        $testdate = date('Y-m-d H:i');
        $active_chk = $index_chk = 'checked';
		$header = 0;
        $query = 'SELECT * FROM ' . $this->db->prefix . "tests WHERE testid='" . $testid . "'";
        $result = $this->db->query($query);
        if ($row = $this->db->fetch($result)) {
            $testdate = $row['testdate'];
            $header = $row['image'];
            $testdate = date('Y-m-d H:i', $testdate);
            $active_chk = ($row['active'] == 1) ? 'checked' : '';
       }

        $this->template->assign_block_vars('cat_catedit', array());

        $this->template->assign_block_vars('tests.edit', array(
            'DATE' => $this->fromLang('questions_date'),
            'DATE_VALUE' => $testdate,
            'HEADER_VALUE' => $header,
            'CATID' => $catid,
            'ACTIVE' => $this->fromLang('questions_active'),
            'ACTIVE_CHK' => $active_chk,
            'CATEGORY' => $this->fromLang('questions_category'),
            'TAB_HEADER_WIDTH' => (count($this->langs) * 75),
            'SAVE' => $this->fromLang('questions_save'),
            'CANCEL' => $this->fromLang('questions_cancel'),
            'URL' => '?' . $this->module_qs . '=questions&mod=tests&testid='.$testid.'&catid=' . $catid,
            'ID' => $testid,
        ));
     
        $cat_sql = 'SELECT 
			N.*, 
			NL.name, 
			NL.header
			FROM ' . $this->db->prefix . 'questionscategories N
			INNER JOIN ' . $this->db->prefix . "questionscategorylocalizations NL ON NL.catid=N.catid
			WHERE
				NL.lang='" . $this->default_lang . "'
			ORDER BY N.position ASC";

        $cat_result = $this->db->query($cat_sql);
        while ($cat_row = $this->db->fetch($cat_result)) {
            $cat_id = $cat_row['catid'];
            $cSel = ($cat_id == $catid) ? 'selected' : '';
            $this->template->assign_block_vars('tests.edit.category', array(
                'TEXT' => $cat_row['name'],
                'VALUE' => $cat_id,
                'SELECTED' => $cSel,
            ));
        }


        $ndx = 0;
        foreach ($this->langs as $key => $value) {
            ++$ndx;
            $lang = $value;
            $header = "";
            $name = $comment = $text = $slug = '';
            $seo_header = $seo_description = $seo_keywords = '';
            $class = ($ndx == 1) ? ' class="active"' : '';
            $fade_class = ($ndx == 1) ? ' in active' : '';

            $query = 'SELECT * FROM ' . $this->db->prefix . "testslocalizations WHERE testid='" . $testid . "' AND lang='" . $lang . "'";
            $result = $this->db->query($query);
            if ($row = $this->db->fetch($result)) {
                $name = $this->ekranFilter($row['name']);
                $header = $row['header'];

            }

            $this->template->assign_block_vars('tests.edit.tab', array(
                'LANG' => $lang,
                'CLASS' => $class,
                'FADE_CLASS' => $fade_class,
                'NAME' => $this->fromLang('questions_name'),
                'NAME_VALUE' => $name,
                'HEADER' => $this->fromLang('questions_header'),
                'HEADER_VALUE' => $header,
             
            ));
        }
    }

    private function getCatName($catid)
    {
        $name = $this->fromLang('last_questions');
        $result_loc = $this->db->query('SELECT * FROM ' . $this->db->prefix . "questionscategorylocalizations WHERE catid='" . $catid . "' AND lang='" . $this->default_lang . "'");
        if ($row_loc = $this->db->fetch($result_loc)) {
            $name = $row_loc['name'];
        }

        return $name;
    }
	
    private function getTestName($testid)
    {
        $name = $this->fromLang('last_questions');
        $result_loc = $this->db->query('SELECT * FROM ' . $this->db->prefix . "testslocalizations WHERE testid='" . $testid . "' AND lang='" . $this->default_lang . "'");
		if ($row_loc = $this->db->fetch($result_loc)) {
            $name = $row_loc['name'];
        }

        return $name;
    }

    private function getQuestionName($catid)
    {
		$name = "";
        $result_loc = $this->db->query('SELECT * FROM ' . $this->db->prefix . "questionslocalizations WHERE questionid='" . $catid . "' AND lang='" . $this->default_lang . "'");
        if ($row_loc = $this->db->fetch($result_loc)) {
            $name = $row_loc['name'];
        }

        return $name;
    }

    private function rteSafe($strText)
    {
        //returns safe code for preloading in the RTE
        $tmpString = $strText;

        //convert all types of single quotes
        /*$tmpString = str_replace(chr(145), chr(39), $tmpString);
        $tmpString = str_replace(chr(146), chr(39), $tmpString);
        $tmpString = str_replace("'", "&#39;", $tmpString);*/

        //convert all types of double quotes
        /*$tmpString = str_replace(chr(147), chr(34), $tmpString);
        $tmpString = str_replace(chr(148), chr(34), $tmpString);*/
        //$tmpString = str_replace("\"", "\"", $tmpString);

        //replace carriage returns & line feeds
        /*$tmpString = str_replace(chr(10), " ", $tmpString);
        $tmpString = str_replace(chr(13), " ", $tmpString);*/

        return $tmpString;
    }
}

$questions = new questions();
$questions->template->pparse('questions');

/******************* questions.admin.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** questions.admin.php ******************/;
