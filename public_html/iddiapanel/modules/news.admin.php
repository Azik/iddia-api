<?php
/******************* news.admin.php *******************
 *
 * News admin module
 *
 * @author MicroPHP <info@microphp.com>
 * @web www.microphp.com
 * @copyright 2004-2019 MicroPHP
 *
 ******************** news.admin.php ******************/

/** 
 * Define Namespace 
 */
namespace mcms5xx\admin\modules;

/**
 * Checking if module included normally.
 */
if (!defined('mCMScheck')) {
    die('mCMS by MicroPHP');
}

require_once '../m/classes/paging.class.php';

class News extends \mcms5xx\classes\AdminPage
{
    protected $newsCount;

    public function __construct()
    {
        $this->curr_module = 'news';
        parent::__construct();
        $this->onLoad();
    }

    private function onLoad()
    {
        $this->buildMenu();
        $this->doAction();
        $this->buildPage();
    }

    private function doAction()
    {
        $id = $this->utils->Post('select_id');

        if ((@$this->user->perm_string['2'] == 1) || (@$this->user->perm_string['0'] == 1)) {
            //Perm for Edit
            $action = $this->utils->Post('action');
            switch ($action) {
                case 'save_newscat_options':
                    $this->saveNewscatOptions($id);
                    break;
            }
        }

        $cat_action = $this->utils->Post('cat_action');

        switch ($cat_action) {
            case 'delete': {
                if ((@$this->user->perm_string['1'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                    //Perm for Del
                    $this->deleteCat($id);
                }
                break;
            }
            case 'save': {
                $this->saveCat($id);
                break;
            }
        }

        $news_action = $this->utils->Post('news_action');
        $catid = $this->utils->Get('catid');

        switch ($news_action) {
            case "delete": {
                if ((@$this->user->perm_string['1'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                    //Perm for Del
                    $this->deleteNews($id);
                }
                break;
            }
            case "save": {
                $post_catid = $this->utils->UserPostInt('txt_category');
                $catid = ($post_catid > 0) ? $post_catid : $catid;
                $this->saveNews($id, $catid);
                break;
            }
        }
    }

    private function deleteCat($id)
    {
        $this->db->query('DELETE FROM ' . $this->db->prefix . 'newscategories WHERE catid=' . $id);
        $this->db->query('DELETE FROM ' . $this->db->prefix . 'newscategorylocalizations WHERE catid=' . $id);

        $result = $this->db->query('SELECT newsid FROM ' . $this->db->prefix . 'news WHERE catid=' . $id);
        while ($row = $this->db->fetch($result)) {
            $newsid = $row['newsid'];
            $this->db->query('DELETE FROM ' . $this->db->prefix . 'newslocalizations WHERE newsid=' . $newsid);
            $this->db->query('DELETE FROM ' . $this->db->prefix . 'news_read WHERE newsid=' . $newsid);
        }
        $this->db->query('DELETE FROM ' . $this->db->prefix . 'news WHERE catid=' . $id);

        $this->user->logOperation($this->user->GetUserId(), 'news', $id, 'cat_delete');
    }

    private function addCatLocalization($catid)
    {
        $query = 'DELETE FROM ' . $this->db->prefix . "newscategorylocalizations WHERE catid='" . $catid . "'";
        $this->db->query($query);

        foreach ($this->langs as $key => $value) {
            $lang = $value;
            $name = $this->utils->Post('txt_name_' . $lang);
			if (strlen($name) > 0) {
				$header = $this->utils->Post('txt_header_' . $lang);

				$query = 'INSERT INTO ' . $this->db->prefix . "newscategorylocalizations(catid, lang, name, header) VALUES('" . $catid . "','" . $lang . "','" . $name . "','" . $header . "')";

				$this->db->query($query);
			}
        }
    }

    private function saveCat($id)
    {
        if ($id == -1) {
            if ((@$this->user->perm_string['3'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                //Perm for Add
                $this->addCat();
            }
        } else {
            if ((@$this->user->perm_string['2'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                //Perm for Edit
                $this->updateCat($id);
            }
        }
        $this->utils->Redirect('?' . $this->module_qs . '=news');
    }

    private function addCat()
    {
        $result = $this->db->query('SELECT * FROM ' . $this->db->prefix . 'newscategories ORDER BY position DESC ' . $this->db->get_limit(0, 1));
        $row = $this->db->fetch($result);
        $pos_c = $row['position'] + 1;
        $active = $this->utils->UserPostInt('txt_active');
        $index = $this->utils->UserPostInt('txt_index');
        $event = $this->utils->UserPostInt('txt_event');

        $query = 'INSERT INTO ' . $this->db->prefix . 'newscategories(`position`, `active`, `index`, `event`) VALUES(' . $pos_c . ', ' . $active . ', ' . $index . ', ' . $event . ')';
        $this->db->query($query);
        $inserted_id = $this->db->insert_id();

        $this->addCatLocalization($inserted_id);
        $this->user->logOperation($this->user->GetUserId(), 'news', $inserted_id, 'cat_add');
    }

    private function updateCat($id)
    {
        $active = $this->utils->UserPostInt('txt_active');
        $index = $this->utils->UserPostInt('txt_index');
        $event = $this->utils->UserPostInt('txt_event');

        $query = 'UPDATE ' . $this->db->prefix . 'newscategories SET `active`=' . $active . ', `index`=' . $index . ', `event`=' . $event . " WHERE catid='" . $id . "'";
        $this->db->query($query);

        $this->addCatLocalization($id);
        $this->user->logOperation($this->user->GetUserId(), 'news', $id, 'cat_update');
    }

    private function deleteNews($id)
    {
        $this->db->query('DELETE FROM ' . $this->db->prefix . 'news WHERE `newsid` = ' . $id);
        $this->db->query('DELETE FROM ' . $this->db->prefix . 'newslocalizations WHERE `newsid` = ' . $id);
        $this->db->query('DELETE FROM ' . $this->db->prefix . 'news_read WHERE `newsid` = ' . $id);
        $this->user->logOperation($this->user->GetUserId(), 'news', $id, 'news_delete');
    }

    private function saveNews($id, $catid)
    {
        if ($id == -1) {
            if ((@$this->user->perm_string['3'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                //Perm for Add
                $this->addNews($catid);
            }
        } else {
            if ((@$this->user->perm_string['2'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                //Perm for Edit
                $this->updateNews($id, $catid);
            }
        }
        $this->utils->Redirect('?' . $this->module_qs . '=news&catid=' . $catid);
    }

    private function addNews($catid)
    {
        $date = time();
        $cal_newsdate = $this->utils->Post('cal_newsdate');
        $txt_active = $this->utils->UserPostInt('txt_active');
        $txt_index = $this->utils->UserPostInt('txt_index');
        $txt_template = $this->utils->UserPostInt('txt_template');
        $date = strtotime($cal_newsdate);

        $query = "INSERT INTO " . $this->db->prefix . "news(`catid`, `newsdate`, `active`, `index`, `template`) 
        VALUES('" . $catid . "', '" . $date . "', " . $txt_active . ", " . $txt_index . ", " . $txt_template . ") ";
        $this->db->query($query);
        $inserted_id = $this->db->insert_id();

        $this->addNewsLocalization($inserted_id);
        $this->user->logOperation($this->user->GetUserId(), 'news', $inserted_id, 'news_add');
    }

    private function updateNews($id, $catid = 0)
    {
        $date = time();
        $cal_newsdate = $this->utils->Post('cal_newsdate');
        $txt_active = $this->utils->UserPostInt('txt_active');
        $txt_index = $this->utils->UserPostInt('txt_index');
        $txt_template = $this->utils->UserPostInt('txt_template');
        $date = strtotime($cal_newsdate);

        $query = "UPDATE `" . $this->db->prefix . "news` 
        SET 
            `catid` = " . $catid . ", 
            `newsdate` = '" . $date . "', 
            `active` = " . $txt_active . ", 
            `index` = " . $txt_index . ", 
            `template` = " . $txt_template . " 
         WHERE 
            `newsid` = " . $id . " ";
        $this->db->query($query);

        $this->addNewsLocalization($id);
        $this->user->logOperation($this->user->GetUserId(), 'news', $id, 'news_update');
    }

    private function addNewsLocalization($newsid)
    {
        $query = 'DELETE FROM ' . $this->db->prefix . "newslocalizations WHERE newsid='" . $newsid . "'";
        $this->db->query($query);
        /*$this->site->deleteFromSearch($newsid, 'news');*/

        foreach ($this->langs as $key => $value) {
            $lang = $value;
            $name = $this->utils->Post('txt_name_' . $lang);
			if (strlen($name) > 0) {
				$header = $this->utils->Post('txt_header_' . $lang);
				$comment = $this->utils->Post('txt_comment_' . $lang);
				$text = $this->utils->Post('txt_text_' . $lang);
				$slug = $this->utils->Post('txt_slug_' . $lang);
				$slug = (strlen($slug) > 0) ? $this->generateSlug($slug, $lang, 'newslocalizations') : $this->generateSlug($name, $lang, 'newslocalizations');
				$seo_header = $this->utils->Post('seo_header_' . $lang);
				$seo_description = $this->utils->Post('seo_description_' . $lang);
				$seo_keywords = $this->utils->Post('seo_keywords_' . $lang);

				$query = 'INSERT INTO ' . $this->db->prefix . "newslocalizations(`newsid`, `lang`, `name`, `header`, `comment`, `text`, `seo_header`, `seo_description`, `seo_keywords`, `slug`) 
				VALUES('" . $newsid . "','" . $lang . "','" . $name . "','" . $header . "','" . $comment . "','" . $text . "', '" . $seo_header . "', '" . $seo_description . "', '" . $seo_keywords . "', '" . $slug . "')";
				$this->db->query($query);
			}
        }
    }

    private function buildPage()
    {
        $this->buildMain();

        $catid = $this->utils->Get('catid');
        if (!is_numeric($catid) || $catid == 0) {
            $this->buildCategory();
        } else {
            $this->buildNews($catid);
        }
    }

    private function buildMain()
    {
        $this->template->assign_var('TITLE', $this->fromLang('title'));
        $this->template->assign_var('CAT_CONFIRM', $this->fromLang('cat_confirm'));
        $this->template->assign_var('NEWS_CONFIRM', $this->fromLang('news_confirm'));
    }

    private function buildCategory()
    {
        $this->template->assign_block_vars('cat', array());

        $cat_edit_id = $this->utils->Get('cateditid');
        if (!is_numeric($cat_edit_id) || $cat_edit_id == 0) {

            if (strlen($cat_edit_id) > 5) {
                $actArr = explode(':', $cat_edit_id);
                $activeSql = ($actArr['0'] == 'activate') ? ' `active`=1' : ' `active`=0';
                $ids = (strlen($actArr['1']) > 0) ? ' `catid` IN ( ' . $actArr['1'] . ' )' : ' `catid`=0';
                $query = 'UPDATE ' . $this->db->prefix . 'newscategories SET ' . $activeSql . ' WHERE ' . $ids . '';
                $this->db->query($query);
                $this->utils->Redirect('?' . $this->module_qs . '=news');
            } else {
                $this->buildCatList();
                if ((@$this->user->perm_string['2'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                    //Perm for Edit
                    $this->buildCatOptions();
                }
            }
        } else {
            $active = $this->utils->UserGetInt('active');
            if ($active > 0) {
                $activeSql = ($active == 1) ? ' `active`=0' : ' `active`=1';
                $query = 'UPDATE ' . $this->db->prefix . 'newscategories SET ' . $activeSql . " WHERE catid='" . $cat_edit_id . "'";
                $this->db->query($query);
                $this->utils->Redirect('?' . $this->module_qs . '=news');
            } else {
                if ($cat_edit_id > 0) {
                    if ((@$this->user->perm_string['2'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                        //Perm for Edit
                        $this->buildCatEdit($cat_edit_id);
                    }
                } else {
                    if ((@$this->user->perm_string['3'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                        //Perm for Add
                        $this->buildCatEdit($cat_edit_id);
                    }
                }
            }
        }
    }

    private function buildCatList()
    {
        $this->template->assign_block_vars('cat_list', array());
        $this->template->assign_block_vars('cat.list', array(
            'CAT' => $this->fromLang('cat_title'),
            'LAST_NEWS' => $this->fromLang('last_news'),
            'LAST_NEWS_URL' => '?' . $this->module_qs . '=news&catid=-1',
            'ADD' => $this->fromLang('cat_add'),
            'ADD_URL' => '?' . $this->module_qs . '=news&cateditid=-1',
            'ADD_NEWS' => $this->fromLang('news_add'),
            'ADD_NEWS_URL' => '?' . $this->module_qs . '=news&catid=-1&newsid=-1',
            'ACTIVE' => $this->fromLang('cat_active'),
            'INDEX' => $this->fromLang('cat_index'),
            'EVENT' => $this->fromLang('cat_event'),
            'INACTIVE' => $this->fromLang('news_inactive'),
            'ACTIVATE' => $this->fromLang('news_activate'),
            'INACTIVATE' => $this->fromLang('news_inactivate'),
            'URL' => '?' . $this->module_qs . '=news',
            'NAME' => $this->fromLang('cat_name'),
            'HEADER' => $this->fromLang('cat_header'),
            'POSITION' => $this->fromLang('cat_position'),
            'EDIT' => $this->fromLang('cat_edit'),
            'DELETE' => $this->fromLang('cat_delete'),
            'DELETE_CONFIRM' => $this->fromLang('cat_confirm'),
        ));
        if ((@$this->user->perm_string['2'] == 1) || (@$this->user->perm_string['0'] == 1)) {
            //Perm for Edit
            $this->template->assign_block_vars('cat.list.perm_edit', array());
        }

        if ((@$this->user->perm_string['3'] == 1) || (@$this->user->perm_string['0'] == 1)) {
            //Perm for Add
            $this->template->assign_block_vars('cat.list.perm_add', array());
        }
        $ndx = 0;
        $hidden_val = '';

        $sql = 'SELECT 
			N.*, 
			NL.name, 
			NL.header
			FROM ' . $this->db->prefix . 'newscategories N
			INNER JOIN ' . $this->db->prefix . "newscategorylocalizations NL ON NL.catid=N.catid
			WHERE
				NL.lang='" . $this->default_lang . "'
			ORDER BY N.position ASC";
        $result = $this->db->query($sql);
        $cat_sayi = $this->db->num_rows($sql);
        while ($row = $this->db->fetch($result)) {
            $id = $row['catid'];
            $position = $row['position'];
            $hidden_val .= ($ndx == 0) ? '' : ',';
            $hidden_val .= $id;
            ++$ndx;
            $name = $this->utils->GetPartOfString($row['name'], 50);
            $header = $this->utils->GetPartOfString($row['header'], 50);
            $active = ($row['active'] == 1) ? $this->fromLang('cat_yes') : $this->fromLang('cat_no');
            $index = ($row['index'] == 1) ? $this->fromLang('cat_yes') : $this->fromLang('cat_no');
            $event = ($row['event'] == 1) ? $this->fromLang('cat_yes') : $this->fromLang('cat_no');
            $this->template->assign_block_vars('cat.list.items', array(
                'ID' => $id,
                'POSITION' => $position,
                'NAME' => $name,
                'HEADER' => $header,
                'ACTIVE' => $active,
                'INDEX' => $index,
                'EVENT' => $event,
                'EDIT_URL' => '?' . $this->module_qs . '=news&cateditid=' . $id,
                'ACTIVE_URL' => '?' . $this->module_qs . '=news&cateditid=' . $id . '&active=1',
                'INACTIVE_URL' => '?' . $this->module_qs . '=news&cateditid=' . $id . '&active=2',
            ));
            if ((@$this->user->perm_string['1'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                //Perm for Del
                $this->template->assign_block_vars('cat.list.items.perm_del', array());
            }
            if ((@$this->user->perm_string['2'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                //Perm for Edit
                $this->template->assign_block_vars('cat.list.items.perm_edit', array());

                if ($row['active'] == 1) {
                    $this->template->assign_block_vars('cat.list.items.active', array());
                } else {
                    $this->template->assign_block_vars('cat.list.items.inactive', array());
                }
            }
        }
        $this->template->assign_block_vars('cat.list.hidden', array(
            'VALUE' => $hidden_val,
        ));
    }

    private function buildCatEdit($cat_edit_id)
    {
        $cat_query = 'SELECT * FROM ' . $this->db->prefix . "newscategories WHERE catid='" . $cat_edit_id . "' ";
        $cat_result = $this->db->query($cat_query);
        $cat_row = $this->db->fetch($cat_result);

        $active_chk = (($cat_row['active'] == 1) || ($cat_edit_id == -1)) ? 'checked' : '';
        $index_chk = ($cat_row['index'] == 1) ? 'checked' : '';
        $event_chk = ($cat_row['event'] == 1) ? 'checked' : '';

        $this->template->assign_block_vars('cat_catedit', array());
        $this->template->assign_block_vars('cat.catedit', array(
            'TAB_HEADER_WIDTH' => (count($this->langs) * 75),
            'ACTIVE' => $this->fromLang('cat_active'),
            'ACTIVE_CHK' => $active_chk,
            'INDEX' => $this->fromLang('cat_index'),
            'INDEX_CHK' => $index_chk,
            'EVENT' => $this->fromLang('cat_event'),
            'EVENT_CHK' => $event_chk,
            'SAVE' => $this->fromLang('cat_save'),
            'CANCEL' => $this->fromLang('cat_cancel'),
            'URL' => '?' . $this->module_qs . '=news',
            'ID' => $cat_edit_id,
        ));

        $ndx = 0;
        foreach ($this->langs as $key => $value) {
            ++$ndx;
            $lang = $value;
            $name = '';
            $header = '';
            $class = ($ndx == 1) ? ' class="active"' : '';
            $fade_class = ($ndx == 1) ? ' in active' : '';

            $query = 'SELECT * FROM ' . $this->db->prefix . "newscategorylocalizations WHERE catid='" . $cat_edit_id . "' AND lang='" . $lang . "'";
            $result = $this->db->query($query);
            if ($row = $this->db->fetch($result)) {
                $name = $row['name'];
                $header = $row['header'];
            }

            $this->template->assign_block_vars('cat.catedit.tab', array(
                'LANG' => $lang,
                'CLASS' => $class,
                'FADE_CLASS' => $fade_class,
                'NAME' => $this->fromLang('cat_name'),
                'NAME_VALUE' => $name,
                'HEADER' => $this->fromLang('cat_header'),
                'HEADER_VALUE' => $header,
            ));
        }
    }

    private function buildCatOptions()
    {
        $this->template->assign_block_vars('newscat_options', array(
            'TITLE' => $this->fromLang('newscat_options'),
            'NEWS_FOOT_LIMIT' => $this->fromLang('news_foot_limit'),
            'NEWS_FOOT_LIMIT_VALUE' => $this->getKey('news_foot_limit', 3),
            'NEWS_INDEX_LIMIT' => $this->fromLang('news_index_limit'),
            'NEWS_INDEX_LIMIT_VALUE' => $this->getKey('news_index_limit', 5),
            'NEWS_PAGE_LIMIT' => $this->fromLang('news_page_limit'),
            'NEWS_PAGE_LIMIT_VALUE' => $this->getKey('news_page_limit', 10),
            'NEWS_BLOCK_SEPERATOR' => $this->fromLang('news_block_seperator'),
            'NEWS_BLOCK_SEPERATOR_VALUE' => $this->getKey('news_block_seperator', 3),
            'NEWS_DATE_FORMAT' => $this->fromLang('news_date_format'),
            'NEWS_DATE_FORMAT_VALUE' => $this->getKey('news_date_format', 'd.m.Y'),
            'SAVE' => $this->fromLang('newscat_options_save'),
            'CANCEL' => $this->fromLang('newscat_options_cancel'),
        ));
    }

    private function saveNewscatOptions()
    {
        $news_foot_limit = $this->utils->UserPostInt('txt_news_foot_limit');
        $this->addKey('news_foot_limit', $news_foot_limit);

        $news_index_limit = $this->utils->UserPostInt('txt_news_index_limit');
        $this->addKey('news_index_limit', $news_index_limit);

        $news_page_limit = $this->utils->UserPostInt('txt_news_page_limit');
        $this->addKey('news_page_limit', $news_page_limit);

        $news_block_seperator = $this->utils->UserPostInt('txt_news_block_seperator');
        $this->addKey('news_block_seperator', $news_block_seperator);

        $news_date_format = $this->utils->UserPost('txt_news_date_format');
        $this->addKey('news_date_format', $news_date_format);

        $this->user->logOperation($this->user->GetUserId(), 'news', 0, 'options_update');
    }

    private function buildNews($catid)
    {
        $cat_name = $this->getCatName($catid);
        $this->template->assign_block_vars('news', array(
            'CAT_NAME' => $cat_name,
            'CAT_ID' => $catid,
        ));

        $newsid = $this->utils->Get('newsid');
        $news_id = $this->utils->Get('news_id');
        if ((!is_numeric($newsid) || $newsid == 0) && (!is_numeric($news_id) || $news_id == 0)) {
            if (strlen($newsid) > 5) {
                $actArr = explode(':', $newsid);
                $activeSql = ($actArr['0'] == 'activate') ? ' `active`=1' : ' `active`=0';
                $ids = (strlen($actArr['1']) > 1) ? ' `newsid` IN ( ' . $actArr['1'] . ' )' : ' `newsid`=0';
                $query = 'UPDATE ' . $this->db->prefix . 'news SET ' . $activeSql . ' WHERE ' . $ids . '';
                $this->db->query($query);
                $this->utils->Redirect('?' . $this->module_qs . '=news&catid=' . $catid);
            } else {
                $this->buildNewsList($catid);
            }
        } else {
            $active = $this->utils->UserGetInt('active');
            if ($active > 0) {
                $activeSql = ($active == 1) ? ' `active`=0' : ' `active`=1';
                $query = 'UPDATE ' . $this->db->prefix . 'news SET ' . $activeSql . " WHERE newsid='" . $newsid . "'";
                $this->db->query($query);
                $this->utils->Redirect('?' . $this->module_qs . '=news&catid=' . $catid);
            } else {
                $this->buildNewsEdit($catid, $newsid);
            }
        }
    }

    private function buildNewsList($catid)
    {
        $cat_name = $this->getCatName($catid);

        $this->template->assign_block_vars('cat_list', array());
        $this->template->assign_block_vars('news_list', array());
        $this->template->assign_block_vars('news.list', array(
            'CAT' => $cat_name,
            'CATID' => $catid,
            'NAME' => $this->fromLang('news_name'),
            'ACTIVE' => $this->fromLang('news_active'),
            'INDEX' => $this->fromLang('news_index'),
            'READ_DATE' => $this->fromLang('news_read_date'),
            'TOTAL_VIEWS' => $this->fromLang('news_total_views'),
            'MONTH_VIEWS' => $this->fromLang('news_month_views'),
            'INACTIVE' => $this->fromLang('news_inactive'),
            'ACTIVATE' => $this->fromLang('news_activate'),
            'INACTIVATE' => $this->fromLang('news_inactivate'),
            'ADD' => $this->fromLang('news_add'),
            'ADD_URL' => '?' . $this->module_qs . '=news&catid=' . $catid . '&newsid=-1',
            'DATE' => $this->fromLang('news_date'),
            'COMMENT' => $this->fromLang('news_comment'),
            'URL' => '?' . $this->module_qs . '=news&catid=' . $catid,
            'EDIT' => $this->fromLang('news_edit'),
            'DELETE' => $this->fromLang('news_delete'),
            'DELETE_CONFIRM' => $this->fromLang('news_confirm'),
        ));

        if ((@$this->user->perm_string['2'] == 1) || (@$this->user->perm_string['0'] == 1)) {
            //Perm for Edit
            $this->template->assign_block_vars('news.list.perm_edit', array());
        }
        if ((@$this->user->perm_string['3'] == 1) || (@$this->user->perm_string['0'] == 1)) {
            //Perm for Add
            $this->template->assign_block_vars('news.list.perm_add', array());
        }
        //for paging begin
        $in_page = $this->fromConfig('news_admin_in_page');
        $page = $this->utils->UserGetInt('page');
        $page = ($page <= 0) ? 1 : $page;

        $start = ($page - 1) * $in_page;
        $end = $in_page;
        //for paging end

        $cat_sql = ($catid>0) ? " WHERE  (N.catid='" . $catid . "') " : "";

        $query = "SELECT
          N.*,
          IF ((NL.`name`!=''), NL.`name`, NLIN.`name`) as `name`,
          IF ((NL.`name`!=''), NL.`text`, NLIN.`text`) as `text`,
          NR.read_date,
          NR.total_views,
          NR.month_views
        FROM " . $this->db->prefix . 'news N
        LEFT JOIN ' . $this->db->prefix . 'news_read NR ON (NR.newsid = N.newsid)
        LEFT JOIN ' . $this->db->prefix . "newslocalizations NL ON (NL.newsid = N.newsid) AND (NL.lang='" . $this->default_lang . "')
        LEFT JOIN " . $this->db->prefix . "newslocalizations NLIN ON ( NLIN.newsid = N.newsid ) AND (NLIN.name != '')
        ".$cat_sql."
        GROUP BY N.newsid
        ORDER BY N.newsdate DESC";
        //echo("<pre>\n\n\n\n\n".$query."</pre>");
        //$this->newsCount = $this->db->num_rows($query);
        $query .= $this->db->get_limit(0, 0);
        //$query .= $this->db->get_limit($start, $end);
        $result = $this->db->query($query);
        while ($row = $this->db->fetch($result)) {
            $id = $row['newsid'];
            $date = $row['newsdate'];
            $name = $row['name'];
            $comment = $row['text'];
            $read_date = (int)$row['read_date'];
            $total_views = (int)$row['total_views'];
            $month_views = (int)$row['month_views'];
            $active = ($row['active'] == 1) ? $this->fromLang('news_yes') : $this->fromLang('news_no');
            $index = ($row['index'] == 1) ? $this->fromLang('news_yes') : $this->fromLang('news_no');

            $comment = $this->utils->GetPartOfString($comment, 200);

            $this->template->assign_block_vars('news.list.items', array(
                'NAME' => $name,
                'ACTIVE' => $active,
                'INDEX' => $index,
                'READ_DATE' => (($read_date > 0) ? $this->utils->formattedDate($read_date) : '-'),
                'TOTAL_VIEWS' => $total_views,
                'MONTH_VIEWS' => $month_views,
                'COMMENT' => $comment,
                'DATE' => $this->utils->formattedDate($date),
                'EDIT_URL' => '?' . $this->module_qs . '=news&catid=' . $catid . '&newsid=' . $id,
                'ACTIVE_URL' => '?' . $this->module_qs . '=news&catid=' . $catid . '&newsid=' . $id . '&active=1',
                'INACTIVE_URL' => '?' . $this->module_qs . '=news&catid=' . $catid . '&newsid=' . $id . '&active=2',
                'ID' => $id,
            ));
            if ((@$this->user->perm_string['1'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                //Perm for Del
                $this->template->assign_block_vars('news.list.items.perm_del', array());
            }
            if ((@$this->user->perm_string['2'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                //Perm for Edit
                $this->template->assign_block_vars('news.list.items.perm_edit', array());

                if ($row['active'] == 1) {
                    $this->template->assign_block_vars('news.list.items.active', array());
                } else {
                    $this->template->assign_block_vars('news.list.items.inactive', array());
                }
            }
        }

        $this->buildPaging();
    }

    private function buildPaging()
    {
        $arr = $this->getPagingArray($this->newsCount);

        /* print("<pre>");
        echo "\n\n\n\n".$this->newsCount;
        print_r($arr);
        print("</pre>"); */

        $prev_url = '';
        $prev_num = 0;

        if ($arr['PREV_LINK_PAGE']) {
            $prev_url = 'href="' . $arr['prefix'] . $arr['PREV_LINK_PAGE'] . '"';
            $prev_num = $arr['PREV_LINK_PAGE'];
        }

        $next_url = '';
        $next_num = 0;

        if ($arr['NEXT_LINK_PAGE']) {
            $next_url = 'href="' . $arr['prefix'] . $arr['NEXT_LINK_PAGE'] . '"';
            $next_num = $arr['NEXT_LINK_PAGE'];
        }

        $this->template->assign_block_vars('news.list.paging', array(
            'PREV_URL' => $prev_url,
            'PREV_NUM' => $prev_num,
            'NEXT_URL' => $next_url,
            'NEXT_NUM' => $next_num,
        ));

        /*
        print("<pre>");
        print_r($arr["PAGE_NUMBERS"]);
        print("</pre>");
        */

		$count = (is_array($arr['PAGE_NUMBERS'])) ? count($arr['PAGE_NUMBERS']) : 0;
        for ($i = 0; $i < $count; ++$i) {
            $num = $arr['PAGE_NUMBERS'][$i];
            $num_url = '';
            $sep = false;
            if ($i < ($count - 1)) {
                $sep = true;
            }

            if ($arr['CURRENT_PAGE'] != $num) {
                $num_url = 'href="' . $arr['prefix'] . $num . '"';
            }

            $this->template->assign_block_vars('news.list.paging.numbers', array(
                'URL' => $num_url,
                'TEXT' => $num,));

            if ($sep) {
                $this->template->assign_block_vars('news.list.paging.numbers.sep', array());
            }
        }
    }

    private function getPagingArray($count)
    {
        $in_page = $this->fromConfig('news_admin_in_page');

        $page_count = @ceil($count / $in_page);

        //paging begin
        $new_url = $_SERVER['REQUEST_URI'];
        $new_url = $this->utils->removeQueryString($new_url, 'page');
        $new_url .= '&';

        $paging = new \mcms5xx\classes\PagedResults();
        $paging->TotalResults = $count;
        $paging->ResultsPerPage = $in_page;
        $paging->LinksPerPage = 10;
        $paging->PageVarName = 'page';
        $paging->UrlPrefix = $new_url;
        //paging end

        $arr = $paging->InfoArray();
        $arr['prefix'] = $paging->Prefix;

        return $arr;
    }

    private function buildNewsEdit($catid, $newsid)
    {
        $template = 0;
        //$newsdate = date('d/m/Y');
        $newsdate = date('Y-m-d H:i');
        $active_chk = $index_chk = 'checked';
        $query = 'SELECT * FROM ' . $this->db->prefix . "news WHERE newsid='" . $newsid . "'";
        $result = $this->db->query($query);
        if ($row = $this->db->fetch($result)) {
            $template = $row['template'];
            $newsdate = $row['newsdate'];
            //$newsdate = date('d/m/Y', $newsdate);
            $newsdate = date('Y-m-d H:i', $newsdate);
            $active_chk = ($row['active'] == 1) ? 'checked' : '';
            $index_chk = ($row['index'] == 1) ? 'checked' : '';
        }

        $this->template->assign_block_vars('cat_catedit', array());

        $this->template->assign_block_vars('news.edit', array(
            'DATE' => $this->fromLang('news_date'),
            'DATE_VALUE' => $newsdate,
            'CATID' => $catid,
            'ACTIVE' => $this->fromLang('news_active'),
            'ACTIVE_CHK' => $active_chk,
            'INDEX' => $this->fromLang('news_index'),
            'INDEX_CHK' => $index_chk,
            'CATEGORY' => $this->fromLang('news_category'),
            'TEMPLATE' => $this->fromLang('news_template'),
            'TAB_HEADER_WIDTH' => (count($this->langs) * 75),
            'SAVE' => $this->fromLang('news_save'),
            'CANCEL' => $this->fromLang('news_cancel'),
            'URL' => '?' . $this->module_qs . '=news&catid=' . $catid,
            'ID' => $newsid,
        ));
        $tSel = ($template == 0) ? 'selected' : '';
        $this->template->assign_block_vars('news.edit.templates', array(
            'TEXT' => $this->fromLang('news_template_default'),
            'VALUE' => 0,
            'SELECTED' => $tSel,
        ));
        $templates_views = $this->fromConfig('templates_views');
        foreach ($templates_views as $tid => $tname) {
            $tSel = ($template == $tid) ? 'selected' : '';
            $this->template->assign_block_vars('news.edit.templates', array(
                'TEXT' => $tname,
                'VALUE' => $tid,
                'SELECTED' => $tSel,
            ));
        }

        $cat_sql = 'SELECT 
			N.*, 
			NL.name, 
			NL.header
			FROM ' . $this->db->prefix . 'newscategories N
			INNER JOIN ' . $this->db->prefix . "newscategorylocalizations NL ON NL.catid=N.catid
			WHERE
				NL.lang='" . $this->default_lang . "'
			ORDER BY N.position ASC";
        $cat_result = $this->db->query($cat_sql);
        while ($cat_row = $this->db->fetch($cat_result)) {
            $cat_id = $cat_row['catid'];
            $cSel = ($cat_id == $catid) ? 'selected' : '';
            $this->template->assign_block_vars('news.edit.category', array(
                'TEXT' => $cat_row['name'],
                'VALUE' => $cat_id,
                'SELECTED' => $cSel,
            ));
        }


        $ndx = 0;
        foreach ($this->langs as $key => $value) {
            ++$ndx;
            $lang = $value;
            $header = 0;
            $name = $comment = $text = $slug = '';
            $seo_header = $seo_description = $seo_keywords = '';
            $class = ($ndx == 1) ? ' class="active"' : '';
            $fade_class = ($ndx == 1) ? ' in active' : '';

            $query = 'SELECT * FROM ' . $this->db->prefix . "newslocalizations WHERE newsid='" . $newsid . "' AND lang='" . $lang . "'";
            $result = $this->db->query($query);
            if ($row = $this->db->fetch($result)) {
                $name = $this->ekranFilter($row['name']);
                $header = (int)$row['header'];
                $comment = $row['comment'];
                $text = $row['text'];
                $slug = $row['slug'];
                $seo_header = $this->ekranFilter($row['seo_header']);
                $seo_description = $row['seo_description'];
                $seo_keywords = $row['seo_keywords'];
                $text = $this->rteSafe($text);
            }

            $this->template->assign_block_vars('news.edit.tab', array(
                'LANG' => $lang,
                'CLASS' => $class,
                'FADE_CLASS' => $fade_class,
                'NAME' => $this->fromLang('news_name'),
                'NAME_VALUE' => $name,
                'HEADER' => $this->fromLang('news_header'),
                'HEADER_VALUE' => $header,
                'COMMENT' => $this->fromLang('news_comment'),
                'COMMENT_VALUE' => $comment,
                'TEXT' => $this->fromLang('news_text'),
                'TEXT_VALUE' => $text,
                'SLUG' => $this->fromLang('news_slug'),
                'SLUG_VALUE' => $slug,
                'SEO_HEADER' => $this->fromLang('news_seo_header'),
                'SEO_HEADER_VALUE' => $seo_header,
                'SEO_DESCRIPTION' => $this->fromLang('news_seo_description'),
                'SEO_DESCRIPTION_VALUE' => $seo_description,
                'SEO_KEYWORDS' => $this->fromLang('news_seo_keywords'),
                'SEO_KEYWORDS_VALUE' => $seo_keywords,
            ));
        }
    }

    private function getCatName($catid)
    {
        $name = $this->fromLang('last_news');
        $result_loc = $this->db->query('SELECT * FROM ' . $this->db->prefix . "newscategorylocalizations WHERE catid='" . $catid . "' AND lang='" . $this->default_lang . "'");
        if ($row_loc = $this->db->fetch($result_loc)) {
            $name = $row_loc['name'];
        }

        return $name;
    }

    private function rteSafe($strText)
    {
        //returns safe code for preloading in the RTE
        $tmpString = $strText;

        //convert all types of single quotes
        /*$tmpString = str_replace(chr(145), chr(39), $tmpString);
        $tmpString = str_replace(chr(146), chr(39), $tmpString);
        $tmpString = str_replace("'", "&#39;", $tmpString);*/

        //convert all types of double quotes
        /*$tmpString = str_replace(chr(147), chr(34), $tmpString);
        $tmpString = str_replace(chr(148), chr(34), $tmpString);*/
        //$tmpString = str_replace("\"", "\"", $tmpString);

        //replace carriage returns & line feeds
        /*$tmpString = str_replace(chr(10), " ", $tmpString);
        $tmpString = str_replace(chr(13), " ", $tmpString);*/

        return $tmpString;
    }
}

$news = new News();
$news->template->pparse('news');

/******************* news.admin.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** news.admin.php ******************/;
