<?php
/******************* options_profile.admin.php *******************
 *
 * Profile Options admin module
 *
 * @author MicroPHP <info@microphp.com>
 * @web www.microphp.com
 * @copyright 2004-2019 MicroPHP
 *
 ******************** options_profile.admin.php ******************/

/** 
 * Define Namespace 
 */
namespace mcms5xx\admin\modules;

/**
 * Checking if module included normally.
 */
if (!defined('mCMScheck')) {
    die('mCMS by MicroPHP');
}

class optionsAdmin extends \mcms5xx\classes\AdminPage
{
    protected $mode;
    protected $message = '';

    public function __construct()
    {
        $this->curr_module = 'options_profile';
        parent::__construct();
        $this->onLoad();
    }

    private function onLoad()
    {
        $this->buildMenu();
        $this->mode = $this->utils->Get('mode');

        if ((@$this->user->perm_string['2'] == 1) || (@$this->user->perm_string['0'] == 1)) {
            //Perm for Edit
            $this->doAction();
        }
        $this->buildPage();
    }

    private function doAction()
    {
        $action = $this->utils->Post('action');
        $action_id = $this->utils->Post('action_id');

        switch ($action) {
            case 'changepassword':
                $this->changePassword();
                $this->user->logOperation($this->user->GetUserId(), 'options_profile', 0, 'change_password');
                break;
            case 'save':
                $this->saveProfile();
                $this->user->logOperation($this->user->GetUserId(), 'options_profile', 0, 'profile_update');
                break;
        }
    }

    private function changePassword()
    {
        $oldpass = $this->utils->Post('txt_oldpass');
        $newpass = $this->utils->Post('txt_newpass');
        $confirm_newpass = $this->utils->Post('txt_confirmnewpass');

        $userid = $this->user->GetUserId();

        if ($newpass != $confirm_newpass) {
            $this->message = $this->fromLang('passwords_not_match');

            return;
        }

        $query = 'SELECT * FROM ' . $this->db->prefix . "users WHERE `userid`='" . $userid . "' AND `password`='" . hash('sha256', $oldpass) . "'";
        $result = $this->db->query($query);

        if (!$row = $this->db->fetch($result)) {
            $this->message = $this->fromLang('pass_incorrect');

            return;
        }

        $query = 'update ' . $this->db->prefix . 'users set ';
        $query .= "password='" . hash('sha256', $newpass) . "',";
        $query .= "updated='" . time() . "'";
        $query .= ' where userid=' . $userid;

        $result = $this->db->query($query);

        if ($result) {
            $this->message = $this->fromLang('pass_success');
        }
    }

    private function saveProfile()
    {
        $userid = $this->user->GetUserId();

        $name = $this->utils->Post('txt_name');
        $email = $this->utils->Post('txt_email');
        $updated = time();

        $query = 'update ' . $this->db->prefix . 'users set ';
        $query .= "name='" . $name . "',";
        $query .= "email='" . $email . "',";
        $query .= "updated='" . $updated . "' ";
        $query .= ' where userid=' . $userid;

        $result = $this->db->query($query);

        return $result;
    }

    private function buildPage()
    {
        $this->buildMain();

        switch ($this->mode) {
            case 'profile':
                $this->buildProfile();
                break;
            case 'password':
                $this->buildChangePassword();
                break;
            default: {
                $this->buildDefault();
				break;
			}
        }
    }

    private function buildMain()
    {
        $readonly = ((@$this->user->perm_string['2'] == 1) || (@$this->user->perm_string['0'] == 1)) ? '' : ' readonly';
        $disabled_css = ((@$this->user->perm_string['2'] == 1) || (@$this->user->perm_string['0'] == 1)) ? '' : ' disabled';
        $disabled_type = ((@$this->user->perm_string['2'] == 1) || (@$this->user->perm_string['0'] == 1)) ? '' : ' disabled';
        $this->template->assign_var('TITLE', $this->fromLang('title'));
        $this->template->assign_var('READONLY', $readonly);
        $this->template->assign_var('DISABLED_CSS', $disabled_css);
        $this->template->assign_var('DISABLED_TYPE', $disabled_type);
        $this->template->assign_block_vars('options', array(
            'URL' => '?' . $this->module_qs . '=options_profile',
            'SAVE' => $this->fromLang('save'),
            'CANCEL' => $this->fromLang('cancel'),
        ));
    }

    private function buildDefault()
    {
        $this->template->assign_block_vars('options.default', array(
            'TITLE' => $this->fromLang('title'),
            'SITEOPTIONS' => $this->fromLang('siteoptions'),
            'PROFILE' => $this->fromLang('profile'),
            'PASSWORD' => $this->fromLang('password'),
        ));
    }

    private function buildChangePassword()
    {
        $this->template->assign_block_vars('sub_link', array(
            'MODE' => 'password',
            'TEXT' => $this->fromLang('password'),
        ));
        $this->template->assign_block_vars('options.changepassword', array(
            'TITLE' => $this->fromLang('password'),
            'OLDPASS' => $this->fromLang('old_pass'),
            'NEWPASS' => $this->fromLang('new_pass'),
            'CONFIRMNEWPASS' => $this->fromLang('confirm_new_pass'),
        ));

        if ($this->message != '') {
            $this->template->assign_block_vars('options.changepassword.alert', array(
                'MESSAGE' => $this->message,));
        }
    }

    private function buildProfile()
    {
        $this->template->assign_block_vars('sub_link', array(
            'MODE' => 'profile',
            'TEXT' => $this->fromLang('profile'),
        ));
        $id = $this->user->GetUserId();
        $name = '';
        $email = '';

        $result = $this->db->query('SELECT * FROM ' . $this->db->prefix . "users WHERE userid='" . $id . "'");
        if ($row = $this->db->fetch($result)) {
            $name = $row['name'];
            $email = $row['email'];
        }

        $this->template->assign_block_vars('options.profile', array(
            'TITLE' => $this->fromLang('profile'),
            'ID' => $id,
            'NAME' => $this->fromLang('name'),
            'NAME_VALUE' => $name,
            'EMAIL' => $this->fromLang('email'),
            'EMAIL_VALUE' => $email,
        ));

        if ($this->message != '') {
            $this->template->assign_block_vars('options.profile.alert', array(
                'MESSAGE' => $this->message,));
        }
    }
}

$optionsAdmin = new optionsAdmin();
$optionsAdmin->template->pparse('options_profile');

/******************* options_profile.admin.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** options_profile.admin.php ******************/;
