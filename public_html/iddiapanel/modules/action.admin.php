<?php
/******************* action.admin.php *******************
 *
 * Actions admin module
 *
 * @author MicroPHP <info@microphp.com>
 * @web www.microphp.com
 * @copyright 2004-2019 MicroPHP
 *
 ******************** action.admin.php ******************/

/** 
 * Define Namespace 
 */
namespace mcms5xx\admin\modules;

/**
 * Checking if module included normally.
 */
if (!defined('mCMScheck')) {
    die('mCMS by MicroPHP');
}

class Action extends \mcms5xx\classes\AdminPage
{
    public function __construct()
    {
        parent::__construct();
        $this->onLoad();
    }

    private function onLoad()
    {
        $act = $this->utils->UserGet('act');

        /* Actions */
        switch ($act) {
            case 'logout': {
                // Log operation
                $this->user->logOperation($this->user->GetUserId(), 'logout', 0, 'logout');

                // Logout
                $this->user->Logout();
                break;
            }
        }
    }
}

$action = new Action();

/******************* action.admin.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** action.admin.php ******************/;
