<?php
/******************* mail_log.admin.php *******************
 *
 * Sended mails log admin module
 *
 * @author MicroPHP <info@microphp.com>
 * @web www.microphp.com
 * @copyright 2004-2019 MicroPHP
 *
 ******************** mail_log.admin.php ******************/

/** 
 * Define Namespace 
 */
namespace mcms5xx\admin\modules;

/**
 * Checking if module included normally.
 */
if (!defined('mCMScheck')) {
    die('mCMS by MicroPHP');
}

class mail_log extends \mcms5xx\classes\AdminPage
{
    public function __construct()
    {
        $this->curr_module = 'mail_log';
        parent::__construct();

        $this->onLoad();
    }

    private function onLoad()
    {
        $this->buildMenu();
        $this->buildPage();
    }

    private function buildPage()
    {
        $this->buildMain();

        $this->buildList();
    }

    private function buildMain()
    {
        $this->template->assign_vars(array(
            'TITLE' => $this->fromLang('title'),
        ));
    }

    private function buildList()
    {
        $this->template->assign_block_vars('message_sends', array());
        $this->template->assign_block_vars('message_sends.list', array(
            'EMAIL' => $this->fromLang('email'),
            'EMAIL_SUBJECT' => $this->fromLang('email_subject'),
            'LOG_TIME' => $this->fromLang('log_time'),
            'URL' => "?" . $this->module_qs . "=mail_log",
            'ADD' => $this->fromLang('add'),
            'EDIT' => $this->fromLang('edit'),
            'DELETE' => $this->fromLang('delete'),
            'DELETE_CONFIRM' => $this->fromLang('delete_confirm'),
        ));

        $sql = "SELECT
          MAIL.*
        FROM " . $this->db->prefix . "message_sends MAIL
        ";
        $sql .= " ORDER BY MAIL.msgid DESC LIMIT 0, 200 ";
        $result = $this->db->query($sql);
        while ($row = $this->db->fetch($result)) {
            $id = $row['msgid'];
            $email = $row['mail_to'];
            $email_subject = $row['mail_subject'];

            $log_time = DATE("r", $row['send_datetime']);

            $this->template->assign_block_vars('message_sends.list.items', array(
                'ID' => $id,
                'EMAIL' => $email,
                'EMAIL_SUBJECT' => $email_subject,
                'LOG_TIME' => $log_time,
                'ACTIVE_URL' => "?" . $this->module_qs . "=mail_log&pid=" . $id . "&active=1",
                'INACTIVE_URL' => "?" . $this->module_qs . "=mail_log&pid=" . $id . "&active=2",
            ));
        }
    }

}

$mail_log = new mail_log();
$mail_log->template->set_filenames(array('mail_log' => "mail_log.tpl"));
$mail_log->template->pparse('mail_log');


/******************* mail_log.admin.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** mail_log.admin.php ******************/;

?>