<?php
/******************* file_manager.admin.php *******************
 *
 * File manager admin module
 *
 * @author MicroPHP <info@microphp.com>
 * @web www.microphp.com
 * @copyright 2004-2019 MicroPHP
 *
 ******************** file_manager.admin.php ******************/

/** 
 * Define Namespace 
 */
namespace mcms5xx\admin\modules;

/**
 * Checking if module included normally.
 */
if (!defined('mCMScheck')) {
    die('mCMS by MicroPHP');
}

require_once '../m/classes/paging.class.php';

class fileManager extends \mcms5xx\classes\AdminPage
{
    protected $message = '';
    protected $fileCount = 0;
    protected $search_str = '';

    public function __construct()
    {
        $this->curr_module = 'file_manager';
        parent::__construct();
        $this->onLoad();
    }

    private function onLoad()
    {
        if ((@$this->user->perm_string['3'] == 1) || (@$this->user->perm_string['0'] == 1)) {
            //Perm for Add
            $this->template->assign_block_vars('perm_add', array());
        }
        $this->buildMenu();
        $this->doAction();
        $this->buildPage();
    }

    private function doAction()
    {
        if ($this->utils->Post('btn_upload')) {
            $this->upload();
        }
        if ((@$this->user->perm_string['1'] == 1) || (@$this->user->perm_string['0'] == 1)) {
            //Perm for Del
            $del_id = $this->utils->Post('del_id');
            if (is_numeric($del_id) && $del_id != 0) {
                $this->delete($del_id);
            }
        }

        if ($this->utils->Get('search')) {
            $this->setSearch();
        }
    }

    private function buildPage()
    {
        $this->template->assign_var('TITLE', $this->fromLang('title'));
        if ($this->message != '') {
            $this->template->assign_block_vars('error', array(
                'MESSAGE' => $this->message,));
        }

        $this->bindCategories();
        $this->buildUpload();
        $this->buildSearch();
        $this->buildFileList();
        $this->buildPaging();
    }

    private function bindCategories()
    {
        $this->template->assign_var('CATEGORY', $this->fromLang('category'));
        $this->template->assign_var('CAT_ALL', $this->fromLang('cat_all'));
        $cat = $this->utils->Get('cat');
        $file_types = $this->fromConfig('file_types');

        foreach ($file_types as $key => $value) {
            $sel = '';
            if ($key == $cat) {
                $sel = 'selected';
            }

            $this->template->assign_block_vars('cat_option', array(
                'VALUE' => $key,
                'SELECTED' => $sel,));
        }
    }

    private function buildUpload()
    {
        $upload_count = $this->fromConfig('multiple_upload_count');

        $this->template->assign_block_vars('upload', array(
            'FILE' => $this->fromLang('upload_file'),
            'TITLE' => $this->fromLang('upload_title'),
            'UPLOAD' => $this->fromLang('upload'),
        ));

        for ($i = 0; $i < $upload_count; ++$i) {
            $this->template->assign_block_vars('upload.file', array());
        }
    }

    private function buildSearch()
    {
        $this->template->assign_block_vars('search', array(
            'TEXT' => $this->fromLang('search_text'),
            'SEARCH' => $this->fromLang('search'),
            'SEARCH_TXT' => $this->search_str,
            'HELP' => $this->fromLang('search_help'),));
    }

    private function buildFileList()
    {
        $this->template->assign_block_vars('files', array(
            'THUMB' => $this->fromLang('file_thumb'),
            'ID' => $this->fromLang('file_id'),
            'FILENAME' => $this->fromLang('file_name'),
            'ORIGINALNAME' => $this->fromLang('file_originalname'),
            'EXTENSION' => $this->fromLang('file_extension'),
            'TITLE' => $this->fromLang('file_title'),
            'COPY_URL' => $this->fromLang('copy_url'),
            'ACTION' => $this->fromLang('file_action'),
            'DELETE' => $this->fromLang('file_delete'),
            'CONFIRM' => $this->fromLang('file_delete_confirm'),
            'IMAGE_OPENER' => $this->fromLang('image_opener'),
        ));
        if ((@$this->user->perm_string['2'] == 1) || (@$this->user->perm_string['0'] == 1)) {
            //Perm for Edit
            $this->template->assign_block_vars('files.perm_edit', array());
        }

        if ((@$this->user->perm_string['1'] == 1) || (@$this->user->perm_string['0'] == 1)) {
            //Perm for Del
            $this->template->assign_block_vars('files.perm_del', array());
        }
    }

    private function buildPaging()
    {
        $arr = $this->getPagingArray($this->fileCount);

        /*
        print("<pre>");
        print_r($arr);
        print("</pre>");
        */

        $prev_url = '';

        if ($arr['PREV_LINK_PAGE']) {
            $prev_url = 'href="' . $arr['prefix'] . $arr['PREV_LINK_PAGE'] . '"';
        }

        $next_url = '';

        if ($arr['NEXT_LINK_PAGE']) {
            $next_url = 'href="' . $arr['prefix'] . $arr['NEXT_LINK_PAGE'] . '"';
        }

        $this->template->assign_block_vars('paging', array(
            'PREV_URL' => $prev_url,
            'NEXT_URL' => $next_url,
        ));

        /*
        print("<pre>");
        print_r($arr["PAGE_NUMBERS"]);
        print("</pre>");
        */

		$count = (is_array($arr['PAGE_NUMBERS'])) ? count($arr['PAGE_NUMBERS']) : 0;
        for ($i = 0; $i < $count; ++$i) {
            $num = $arr['PAGE_NUMBERS'][$i];
            $num_url = '';
            $sep = false;
            if ($i < ($count - 1)) {
                $sep = true;
            }

            if ($arr['CURRENT_PAGE'] != $num) {
                $num_url = 'href="' . $arr['prefix'] . $num . '"';
            }

            $this->template->assign_block_vars('paging.numbers', array(
                'URL' => $num_url,
                'TEXT' => $num,
            ));

            if ($sep) {
                $this->template->assign_block_vars('paging.numbers.sep', array());
            }
        }
    }

    private function getPagingArray($count)
    {
        $in_page = $this->fromConfig('file_in_page');

        //$file_count = $this->db->num_rows("SELECT * FROM ".$this->db->prefix."files");
        $file_count = $count;
        $page_count = ceil($file_count / $in_page);

        //paging begin
        $new_url = $_SERVER['REQUEST_URI'];
        $new_url = $this->removeQS($new_url, 'page');
        $new_url .= '&';

        $paging = new \mcms5xx\classes\PagedResults();
        $paging->TotalResults = $file_count;
        $paging->ResultsPerPage = $in_page;
        $paging->LinksPerPage = 10;
        $paging->PageVarName = 'page';
        $paging->UrlPrefix = $new_url;
        //paging end

        $arr = $paging->InfoArray();
        $arr['prefix'] = $paging->Prefix;

        return $arr;
    }

    private function removeQS($url, $qs)
    {
        return $this->utils->removeQueryString($url, $qs);
    }

    private function upload()
    {
        $files = @$_FILES['txt_files'];
        $titles = @$_POST['txt_titles'];
        /*
        print("<pre>");
        print_r($files);
        print("</pre>");
        */

        for ($i = 0; $i < count($files); ++$i) {
            $filename = $this->utils->filename_filter($files['name'][$i]);
            $originalname = strtolower($filename);
            if (trim($filename) != '') {
                $ext = $this->utils->GetFileExtension($filename);
                if (in_array($ext, $this->getFileTypesArray())) {
                    $cat = $this->getFileCat($ext);
                    $file_dir = '../' . $this->fromConfig('upload_folder') . $cat . '/';
                    $add_time = time();
                    $file_dir = $this->io->dateFolder($file_dir, $add_time) . '/';
                    $filename = $this->utils->GetFileName($file_dir, $filename);
                    $titles[$i] = $this->utils->txt_request_filter($titles[$i]);
                    if (move_uploaded_file($files['tmp_name'][$i], $file_dir . $filename) && chmod($file_dir . $filename, 0777)) {
                        $query = 'INSERT INTO ' . $this->db->prefix . "files (`originalname`, `filename`, `extension`, `category`, `title`, `add_time`) VALUES('" . $originalname . "','" . $filename . "','" . $ext . "','" . $cat . "','" . strtolower($titles[$i]) . "', " . $add_time . ' )';
                        $this->db->query($query);
                    }
                    $this->utils->Redirect('?' . $this->module_qs . '=file_manager');
                } else {
                    $this->message = $this->fromLang('invalid_type', $ext);
                }
            }
        }
    }

    private function delete($id)
    {
        $query = 'SELECT * FROM ' . $this->db->prefix . 'files WHERE fileid=' . $id;
        $result = $this->db->query($query);
        if ($row = $this->db->fetch($result)) {
            $cat = $row['category'];
            $filename = $row['filename'];
            $file_dir = '../' . $this->fromConfig('upload_folder') . $cat . '/';
            $file_dir = $this->io->dateFolder($file_dir, $row['add_time']) . '/';
            @unlink($file_dir . $filename);
            if ($cat == 'image') {
                $image_thumb_folder = $this->fromConfig('image_thumb_folder');
                $thumb_dir = '../' . $this->fromConfig('upload_folder') . $cat . '/' . $image_thumb_folder . '/';
                $thumb_dir = $this->io->dateFolder($thumb_dir, $row['add_time']) . '/';
                @unlink($thumb_dir . $filename);
            }
            $query = 'DELETE FROM ' . $this->db->prefix . 'files WHERE fileid=' . $id;
            $this->db->query($query);
        }
        $this->user->logOperation($this->user->GetUserId(), 'file_manager', $id, 'delete');
        $this->utils->Redirect('?' . $this->module_qs . '=file_manager');
    }

    private function setSearch()
    {
        $search = $this->utils->Get('search');
        //$search = str_replace("*", "%", $search);
        //$search = str_replace("?", "_", $search);
        $this->search_str = $search;
    }
}

$file_manager = new fileManager();
$file_manager->template->pparse('file_manager');

/******************* file_manager.admin.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** file_manager.admin.php ******************/;
