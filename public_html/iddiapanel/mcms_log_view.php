<?php
/******************* mcms_log_view.php *******************
 *
 * Events log file
 * 
 * @author MicroPHP <info@microphp.com>
 * @web www.microphp.com
 * @copyright 2004-2019 MicroPHP
 *
 ******************** mcms_log_view.php ******************/

/** 
 * Define Namespace 
 */
namespace mcms5xx\admin;

@$_GET['module'] = 'mcms_log';

define('iFolded', '../');
require_once iFolded.'m/classes/adminpage.class.php';

class mcmsLogIndex extends \mcms5xx\classes\AdminPage
{

    public function __construct()
    {
        parent::__construct();
    }

    public function onLoad()
    {
        $this->buildPage();
    }

    public function buildPage()
    {
        $user_type = $this->user->GetCurrentUserTypeText();
        $logid = $this->utils->UserGetInt('logid');
        $sql = "SELECT
          MCMSLOG.*,
		  U.username AS username
        FROM " . $this->db->prefix . "logs MCMSLOG
		LEFT JOIN " . $this->db->prefix . "users U ON U.`userid` = MCMSLOG.user_id
        WHERE
           MCMSLOG.logsid = ".$logid."
        ";
        $result = $this->db->query($sql);
        //echo($sql);
        $returnTxt = "";
        if ($row = $this->db->fetch($result)) {
            $id = $row['logsid'];
            $user_id = $row['user_id'];
            $log_time = DATE("r", $row['date_int']);
            $returnTxt .= '<div><strong>'.$this->fromLang('log_time').':</strong>'.$log_time.'<br/><br/></div>';
            $user = $row['username'];
            $operation = $row['operation_mode'];
            $page = $row['page'];
            $ip = $row['ip'];
            $user_agent = $row['comp'];
            $module_name = $row['module_name'];
            $moduleid = $row['moduleid'];

            $returnTxt .= '<div><strong>'.$this->fromLang('user').':</strong>'.$user.'<br/><br/></div>';
            $returnTxt .= '<div><strong>'.$this->fromLang('user_id').':</strong>'.$user_id.'<br/><br/></div>';
            $returnTxt .= '<div><strong>'.$this->fromLang('module_name').':</strong>'.$module_name.'<br/><br/></div>';
            $returnTxt .= '<div><strong>'.$this->fromLang('operation').':</strong>'.$operation.'<br/><br/></div>';
            if ($moduleid > 0) $returnTxt .= '<div><strong>'.$this->fromLang('moduleid').':</strong>'.$moduleid.'<br/><br/></div>';
            $returnTxt .= '<div><strong>'.$this->fromLang('page').':</strong>'.$page.'<br/><br/></div>';
            $returnTxt .= '<div><strong>'.$this->fromLang('ip').':</strong>'.$ip.'<br/><br/></div>';
            $returnTxt .= '<div><strong>'.$this->fromLang('user_agent').':</strong>'.$user_agent.'<br/><br/></div>';


        }
        echo $returnTxt;
    }

}


$mcmsLogIndex = new mcmsLogIndex();
include($mcmsLogIndex->lg_folder . '/index.lang.php');
require_once($mcmsLogIndex->lg_folder . '/' . $mcmsLogIndex->module . '.lang.php');

$mcmsLogIndex->onLoad();

/******************* mcms_log_view.php *******************
*
* Copyright : (C) 2004 - 2019. All Rights Reserved
*
******************** mcms_log_view.php ******************/
?>