<?php
/******************* ajax.upload.php *******************
 *
 * Ajax upload script for file managed
 *
 * @author MicroPHP <info@microphp.com>
 * @web www.microphp.com
 * @copyright 2004-2019 MicroPHP
 *
 ******************** ajax.upload.php ******************/

/** 
 * Define Namespace 
 */
namespace mcms5xx\admin;

define('iFolded', '../');
require_once iFolded . 'm/classes/adminpage.class.php';
@$_GET['module'] = 'file_manager';

class fileManager extends \mcms5xx\classes\AdminPage
{
    protected $message = '';
    protected $fileCount = 0;
    protected $search_str = '';

    public function __construct()
    {
        $this->curr_module = 'file_manager';
        parent::__construct();
        $this->onLoad();
    }

    private function onLoad()
    {
        $files = @$_FILES['file_up'];
        //print("<pre>"); print_r($files); print("</pre>");
        $img_id = 0;
        if (is_array($files)) {
            $filename = $this->utils->filename_filter($files['name']);
            $originalname = strtolower($filename);
            if (trim($filename) != '') {
                $ext = $this->utils->GetFileExtension($filename);
                if (in_array($ext, $this->getFileTypesArray())) {
                    $cat = $this->getFileCat($ext);
                    if ($cat == 'image') {
                        $file_dir = '../' . $this->fromConfig('upload_folder') . $cat . '/';
                        $add_time = time();
                        $file_dir = $this->io->dateFolder($file_dir, $add_time) . '/';
                        $filename = $this->utils->GetFileName($file_dir, $filename);
                        $titles = $this->utils->txt_request_filter(str_replace("." . $ext, "", $originalname));

                        $img_file = $file_dir . $filename;
                        if (move_uploaded_file($files['tmp_name'], $img_file) && chmod($img_file, 0777)) {

                            $image_thumb_folder = $this->fromConfig('image_thumb_folder');
                            $image_thumb_width = $this->fromConfig('image_thumb_width');
                            $thumb_dir = '../'.$this->fromConfig('upload_folder').$cat.'/'.$image_thumb_folder.'/';
                            $thumb_dir = $this->io->dateFolder($thumb_dir, $add_time).'/';
                            $thumb_img = $thumb_dir.$filename;
                            list($original_width, $original_height, $src_t, $src_a) = getimagesize($img_file);
                            $calcP = array($original_width, $image_thumb_width);
                            $calc_p = $this->utils->calc_perc($calcP['1'], $calcP['0']);
                            $image_thumb_height = round(($original_height / $calc_p) * 100, 4);
                            $src_w = round(($image_thumb_width / 100) * $calc_p, 4);
                            $src_h = round(($image_thumb_height / 100) * $calc_p, 4);
                            $image_p = imagecreatetruecolor($image_thumb_width, $image_thumb_height);

                            switch ($ext) {
                                case 'jpg':
                                case 'jpeg': {
                                    $image = imagecreatefromjpeg($img_file);
                                    break;
                                }
                                case 'gif': {
                                    $image = imagecreatefromgif($img_file);
                                    break;
                                }
                                case 'png': {
                                    $image = imagecreatefrompng($img_file);
                                    break;
                                }
                                default: {
                                    $image = imagecreatefromjpeg($img_file);
                                    break;
                                }
                            }
                            imagecopyresampled($image_p, $image, 0, 0, 0, 0, $image_thumb_width, $image_thumb_height, $src_w, $src_h);
                            switch ($ext) {
                                case 'jpg':
                                case 'jpeg': {
                                    @imagejpeg($image_p, $thumb_img, 100);
                                    break;
                                }
                                case 'gif': {
                                    @imagegif($image_p, $thumb_img);
                                    break;
                                }
                                case 'png': {
                                    @imagepng($image_p, $thumb_img, 9);
                                    break;
                                }
                                default: {
                                    @imagejpeg($image_p, $thumb_img, 100);
                                    break;
                                }
                            }
                            @chmod($thumb_img, 0767);
                            @imagedestroy($image_p);

                            $query = 'INSERT INTO ' . $this->db->prefix . "files (`originalname`, `filename`, `extension`, `category`, `title`, `add_time`) 
                            VALUES('" . $originalname . "','" . $filename . "','" . $ext . "','" . $cat . "','" . strtolower($titles) . "', " . $add_time . ' )';
                            $this->db->query($query);
                            $img_id = $this->db->insert_id();
                            $this->user->logOperation($this->user->GetUserId(), 'file_manager', $img_id, 'upload_image');
                        }
                        //echo "<img src='" . $thumb_img . "' />";
                        echo '<a class="fancybox-button" data-rel="fancybox-button" title="'.$titles.'" onclick="selectImage('.$img_id.')"><img src="'.$thumb_img.'" alt=""></a>';
                    } else {
                        echo 'invalid file';
                    }
                } else {
                    echo 'invalid file';
                }
            }
        }

    }

}

$file_manager = new fileManager();

/******************* ajax.upload.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** ajax.upload.php ******************/;
