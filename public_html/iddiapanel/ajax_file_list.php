<?php
/******************* blocks_drag.php *******************
 *
 *
 * @author MicroPHP <info@microphp.com>
 * @web www.microphp.com
 * @copyright 2004-2019 MicroPHP
 *
 ******************** blocks_drag.php ******************/

/** 
 * Define Namespace 
 */
namespace mcms5xx\admin;

define('iFolded', '../');
require_once iFolded.'m/classes/adminpage.class.php';
@$_GET['module'] = 'file_manager';

class Index extends \mcms5xx\classes\AdminPage
{
    public function __construct()
    {
        parent::__construct();
    }

    public function onLoad()
    {
        $this->buildPage();
    }

    public function buildPage()
    {
        $user_type = $this->user->GetCurrentUserTypeText();

        if (!$this->user->IsLogin()) {
            return;
        }

        $this->listNewsAjax();
    }
	
	private function listNewsAjax()
	{
        /*$p = '';
        foreach ($_GET as $key=>$index) {
            $p .= $key ."=>". $index ."\n";
        }
        $handle = @fopen("file.txt", "a+");
        @fwrite($handle, "\n\n"."POST:".$p);
        @fclose($handle);*/

        $dataArr['data'] = array();
        /*$dataArr['data'][] = array(
            0,
            1,
            2,
            3,
            4,
            5,
            6
        );*/
        $recordsTotal = 0;
        $nums = 0;
        $upload_folder = $this->fromConfig('upload_folder');
        $image_thumb_folder = $this->fromConfig('image_thumb_folder');
        $list_sql = "SELECT
          F.*
        FROM `" . $this->db->prefix . "files` F
        GROUP BY F.`fileid`
        ";

        $start = $this->utils->UserGetInt("start");
        $length = $this->utils->UserGetInt("length");
        $length = ($length > 0) ? $length : 10;

        //echo("<pre>\n\n\n\n\n".$list_sql."</pre>");
        //$this->newsCount = $this->db->num_rows($list_sql);
        $w_sql = "";
        $s_value = (strlen(@$_GET['search']['value'])>0) ? $this->utils->txt_request_filter_user(@$_GET['search']['value']) : "";
        if (strlen($s_value)>2) {
            $w_sql = " WHERE
					(S.`category` LIKE '%".$s_value."%')
				 OR (S.`extension` LIKE '%".$s_value."%')
				 OR (S.`filename` LIKE '%".$s_value."%')
				";
        }
        $sql = "SELECT S.* FROM ( ".$list_sql." ) S ".$w_sql." ORDER BY S.`fileid` DESC ";
        $nums_sql = "SELECT COUNT(`fileid`) as `total` FROM ( ".$list_sql." ) S ".$w_sql;
        $nums_result = $this->db->query($nums_sql);
        if ($nums_row = $this->db->fetch($nums_result)) {
            $recordsTotal = $nums_row['total'];
        }
        $this->fileCount = $recordsTotal;
        $sql .= " LIMIT ".$start.", ".$length." ";
        $result = $this->db->query($sql);
        /*$edit_url_lang = '<a href="[EDIT_URL]" title="'.$this->fromLang('news_edit').'" class="btn yellow"> '.$this->fromLang('news_edit').'&nbsp; <i class="fa fa-pencil"></i></a>';
        $del_url_lang = '<a href="JavaScript:doNewsDelete([ID])" title="'.$this->fromLang('news_delete').'" class="btn red ask" title="'.$this->fromLang('news_delete').'"> '.$this->fromLang('news_delete').'&nbsp; <i class="fa fa-trash"></i></a>';
        $active_url_lang = '<a href="[ACTIVE_URL]" class="btn btn-success">'.$this->fromLang('news_active').'&nbsp; <i class="fa fa-unlock"></i></a>';
        $inactive_url_lang = '<a href="[INACTIVE_URL]" class="btn btn-danger">'.$this->fromLang('news_inactive').'&nbsp; <i class="fa fa-lock"></i></a>';*/
        $ndx = 0;
        $image_edit_url = '<a href="JavaScript:imageOpener([ID])" title="'.$this->fromLang('image_opener').'"><img src="'.$this->admin_folder.'img/user_edit.png" alt="'.$this->fromLang('image_opener').'" title="'.$this->fromLang('image_opener').'" /></a>&nbsp;&nbsp;';
        $image_delete_url = '&nbsp;&nbsp;<a href="JavaScript:doDelete([ID])" title="'.$this->fromLang('file_delete').'" class="ask"><img src="'.$this->admin_folder.'img/trash.png" alt="" title=""/></a>';
        while ($row = $this->db->fetch($result)) {
            ++$ndx;
            $category = $row['category'];
            $extension = $row['extension'];
            $file_dir = $this->io->dateFolder($upload_folder . $category . '/', $row['add_time']);
            $filename = $row['filename'];
            $thumb_dir = $this->io->dateFolder($upload_folder . $category . '/' . $image_thumb_folder . '/', $row['add_time']);
            $thumb_file = ($category == 'image') ? '../' . $thumb_dir . '/' . $filename : (is_file($this->admin_folder . 'img/ico/' . $extension . '.png') ? $this->admin_folder . 'img/ico/' . $extension . '.png' : $this->admin_folder . 'img/ico/file.png');
            $thumb = '<img src="' . $thumb_file . '" alt="" class="thumb" />';
            $fileid = $row['fileid'];
            $copy_url = '/' . $file_dir . '/' . $filename;
            $action_url = '';
            if ((@$this->user->perm_string['2'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                //Perm for Edit
                if ($category == 'image') {
                    $action_url .= str_replace('[ID]', $fileid, $image_edit_url);
                }
            }

            if ((@$this->user->perm_string['1'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                //Perm for Del
                $action_url .= str_replace('[ID]', $fileid, $image_delete_url);
            }
            $dataArr['data'][] = array(
                '<a class="fancybox-button" data-rel="fancybox-button" title="' . $filename . '" href="../' . $file_dir . '/' . $filename . '" target="_blank">' . $thumb . '</a>',
                $fileid,
                '<a class="fancybox-button" data-rel="fancybox-button" title="' . $filename . '" href="../' . $file_dir . '/' . $filename . '" target="_blank">' . $filename . '</a><br/><input type="text" name="copy_'.$fileid.'" id="copy_'.$fileid.'" class="file_url" value="'.$copy_url.'">',
                '<a onclick="copyToClipboard(document.getElementById(\'copy_'.$fileid.'\'));"> <img src="'.$this->admin_folder.'img/copy.png" alt="'.$this->fromLang('copy_url').'" title="'.$this->fromLang('copy_url').'"/> </a>',
                $action_url
            );
            /*$this->template->assign_block_vars('files.all', array(
                'ID' => $row['fileid'],
                'FILENAME' => '<a class="fancybox-button" data-rel="fancybox-button" title="' . $filename . '" href="../' . $file_dir . '/' . $filename . '" target="_blank">' . $filename . '</a>',
                'THUMB' => '<a class="fancybox-button" data-rel="fancybox-button" title="' . $filename . '" href="../' . $file_dir . '/' . $filename . '" target="_blank">' . $thumb . '</a>',
                'COPY_URL' => '/' . $file_dir . '/' . $filename,
                'ORIGINALNAME' => $row['originalname'],
                'EXTENSION' => $row['extension'],
                'TITLE' => $row['title'],
                'CLASS' => $class,
                'DELETE' => $this->fromLang('file_delete'),
            ));
            if ((@$this->user->perm_string['2'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                //Perm for Edit
                $this->template->assign_block_vars('files.all.perm_edit', array());
                if ($category == 'image') {
                    $this->template->assign_block_vars('files.all.perm_edit.image', array());
                }
            }

            if ((@$this->user->perm_string['1'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                //Perm for Del
                $this->template->assign_block_vars('files.all.image', array());
            }*/

        }


        $_SESSION['draw'] = @$_SESSION['draw'] + 1;
        $dataArr['draw'] = $_SESSION['draw'];
        $dataArr['recordsTotal'] = $nums;
        $dataArr['recordsFiltered'] = $recordsTotal;//$ndx;

		echo json_encode($dataArr);
	}
}

$index = new Index();
include $index->lg_folder.'/index.lang.php';
require_once $index->lg_folder.'/'.$index->module.'.lang.php';
$index->onLoad();

/******************* blocks_drag.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** blocks_drag.php ******************/;
