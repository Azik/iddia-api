<?php
/******************* mail_view.php *******************
 *
 * View mail info file
 * 
 * @author MicroPHP <info@microphp.com>
 * @web www.microphp.com
 * @copyright 2004-2019 MicroPHP
 *
 ******************** mail_view.php ******************/

/** 
 * Define Namespace 
 */
namespace mcms5xx\admin;

 @$_GET['module'] = 'mail_log';

define('iFolded', '../');
require_once iFolded.'m/classes/adminpage.class.php';

class sendMailIndex extends \mcms5xx\classes\AdminPage
{

    public function __construct()
    {
        parent::__construct();
    }

    public function onLoad()
    {
        $this->buildPage();
    }

    public function buildPage()
    {
        $user_type = $this->user->GetCurrentUserTypeText();
        $mailid = $this->utils->UserGetInt('mailid');
        $sql = "SELECT
          MAIL.*
        FROM " . $this->db->prefix . "message_sends MAIL
        WHERE
           MAIL.msgid = ".$mailid."
        ";
        $result = $this->db->query($sql);
        //echo($sql);
        $returnTxt = "";
        if ($row = $this->db->fetch($result)) {
            $log_time = DATE("r", $row['send_datetime']);
            $returnTxt .= '<div><strong>'.$this->fromLang('date').':</strong>'.$log_time.'<br/><br/></div>';

            $email = $row['mail_to'];
            $returnTxt .= '<div><strong>'.$this->fromLang('mail_to').':</strong>'.$email.'<br/><br/></div>';

            $email_subject = $row['mail_subject'];
            $returnTxt .= '<div><strong>'.$this->fromLang('subject').':</strong>'.$email_subject.'<br/><br/></div>';

            $mail_txt = $row['mail_txt'];
            $returnTxt .= '<div><strong>'.$this->fromLang('mail').':</strong><hr/>'.$mail_txt.'<hr/><br/><br/></div>';

        }
        echo $returnTxt;
    }

}


$sendMailIndex = new sendMailIndex();
include($sendMailIndex->lg_folder . '/index.lang.php');
require_once($sendMailIndex->lg_folder . '/' . $sendMailIndex->module . '.lang.php');

$sendMailIndex->onLoad();

/******************* mail_view.php *******************
*
* Copyright : (C) 2004 - 2019. All Rights Reserved
*
******************** mail_view.php ******************/
?>