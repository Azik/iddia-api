<?php
/******************* ajax.link.php *******************
 *
 * Select pages from section manages
 *
 * @author MicroPHP <info@microphp.com>
 * @web www.microphp.com
 * @copyright 2004-2019 MicroPHP
 *
 ******************** ajax.link.php ******************/

/** 
 * Define Namespace 
 */
namespace mcms5xx\admin;

define('iFolded', '../');
require_once iFolded.'m/classes/adminpage.class.php';
$_GET['module'] = 'section';

class indexAdmin extends \mcms5xx\classes\AdminPage
{

    public function __construct()
    {
        $this->curr_module = 'section';
        parent::__construct();
    }

    public function onLoad()
    {
        $this->buildLangs();
        
        $this->buildPage();
    }

    private function buildPage()
    {
        $user_type = $this->user->GetCurrentUserTypeText();

        if (!$this->user->IsLogin()) {
            return;
        }

        $this->buildMenu();

        $this->listMenuLinks();
    }

    private function listMenuLinks()
    {
        $select_field = $this->utils->UserGet('select_field');
        $link = $this->utils->UserGetInt('link');
        $this->template->assign_var('LINK_ID', $link);
        $this->template->assign_var('SELECT_FIELD', $select_field);
        $this->template->assign_block_vars('mcms_toolbar', array(
            'SELECT_LINK' => $this->fromLang('toolbar_select_link'),
            'SELECT_FIELD' => $select_field
        ));
        $this->template->assign_block_vars('mcms_tree', array());
    }

}

$index = new indexAdmin();
include $index->lg_folder.'/index.lang.php';
require_once $index->lg_folder.'/'.$index->module.'.lang.php';
$index->onLoad();
$index->template->set_filenames(array('ajax.link' => 'ajax.link.tpl'));
$index->template->pparse('ajax.link');

/******************* ajax.link.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** ajax.link.php ******************/;
