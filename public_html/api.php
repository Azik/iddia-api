<?php
/******************* api.php *******************
 *

 *
 ******************** api.php ******************/

/**
 * Define Namespace
 */
namespace mcms5xx;

/**
 * Include view page class
 */
require_once 'm/classes/viewpage.class.php';
require_once 'm/classes/APIJsonResponse.php';
require_once "m/classes/vendor/autoload.php";
use \Firebase\JWT\JWT;

class apiController extends \mcms5xx\classes\ViewPage
{
    public $langs;
    public $permalinks = '';
    public $perma_type = '';
    public $inside_lang = '';
    public $index_lang = '';

    public $operation = '';
    public $isSuccess = false;
    public $code = 0;
    public $data = array();
    public $errors = array();
    public $result = array();

    public function __construct()
    {
        parent::__construct();
        //$this->utils->require_auth();
        
    }

    public function onLoad()
    {
        $this->operation = $this
            ->utils
            ->UserGet('op');

        switch ($this->operation)
        {
            case 'verifyCode':
                {
                        $this->verifyCode();
                    break;
                }
            case 'checkVerifyCode':
                {
                    $this->checkVerifyCode();
                    break;
                }
            case 'register':
                {
                    $this->register();
                    break;
                }
            case 'login':
                {
                    $this->login();
                    break;
                }
            case 'exist':
                {
                    $this->exist();
                    break;
                }
            case 'changePrivateStatus':
                {
                    $this->changePrivateStatus();
                    break;
                }
            }

            $res = new \mcms5xx\classes\APIJsonResponse($this->isSuccess, $this->errors, $this->data);
        }

        private function changePrivateStatus()
        {
            $data = array();
            $request = (array)json_decode(file_get_contents('php://input') , true);
            $userID = $request['userID'];
            $isPrivate = $request['isPrivate'];

            $dataUpd = array();
            switch ($this->operation)
            {
                case true:
                    {
                            $dataUpd['private'] = 1;
                            $this
                                ->db
                                ->update($this
                                ->db->prefix . 'members', $dataUpd, " member_id=" . $userID . "");
                            $this->isSuccess = true;
                        break;
                    }
                case false:
                    {
                        $dataUpd['private'] = 0;
                        $this
                            ->db
                            ->update($this
                            ->db->prefix . 'members', $dataUpd, " member_id=" . $userID . "");
                        $this->isSuccess = true;
                        break;
                    }

                }

            }

            private function logout()
            {
                $data = array();
                $request = (array)json_decode(file_get_contents('php://input') , true);

                $userID = $request['userID'];

                $this->isSuccess = true;

            }

            private function login()
            {

            }

            private function exist()
            {
                $data = array();
                $request = (array)json_decode(file_get_contents('php://input') , true);
                $userID = 0;
                $type = $request['type']; //mail, email
                $username = $request['username'];

                $queryCheck = 'SELECT
		 M.*
		 FROM `' . $this
                    ->db->prefix . "members` M
		 WHERE M.m_user='" . trim($login) . "' LIMIT 1";

                $resultCheck = $this
                    ->db
                    ->query($queryCheck);
                if ($rowCheck = $this
                    ->db
                    ->fetch($resultCheck))
                {
                    $this->isSuccess = true;
                    $userID = $rowCheck['member_id'];
                    $data['userID'] = $userID;
                    $this->data = $data;
                }
                else
                {
                    $this->isSuccess = false;
                    $err = array();
                    $err['code'] = - 2;
                    $err['message'] = 'User does not exist';
                    $this->errors[] = $err;
                }

            }

            private function verifyCode()
            {
                $request = (array)json_decode(file_get_contents('php://input') , true);
                $type = $request['type'];
                $obj = $request['obj'];
                $now = time();
                $verifyCode = rand(10, 1000000);

                /*
                $queryCheck = 'SELECT
                M.*
                FROM `' .$this->db->prefix."verify_codes` M
                WHERE M.m_mail='".trim($login)."' AND M.m_pass='".trim($password)."' ";
                
                $resultCheck = $this->db->query($queryCheck);
                if ($rowCheck = $this->db->fetch($resultCheck)) {
                }
                */

                if (strlen($type) == 0 || strlen($obj) == 0)
                {
                    $this->ok = false;
                    $err = array();
                    $err['code'] = - 1;
                    $err['message'] = 'is not correct';
                    $this->errors[] = $err;
                }
                else
                {
                    $dataIns = array();
                    switch ($type)
                    {
                        case 'email':
                            {

                                    $dataIns['type'] = $type;
                                    $dataIns['obj'] = $obj;
                                    $dataIns['code'] = $verifyCode;
                                    $dataIns['add_date'] = $now;
                                    $this
                                        ->db
                                        ->insert($this
                                        ->db->prefix . 'verify_codes', $dataIns);
                                    //$msg = "sharebm code: ";
                                    //mail("abbasov.azik@gmail.com","sharebm register verify",$msg);
                                    $this->data['verifyCode'] = $verifyCode;
                                    $this->ok = true;
                                break;
                            }
                        case 'mobile':
                            {
                                $this->ok = true;
                                break;
                            }

                        }

                    }

                }

                private function checkVerifyCode()
                {
                    $request = (array)json_decode(file_get_contents('php://input') , true);
                    $code = $request['code'];
                    $now = time();

                    if (strlen($code) == 0)
                    {
                        $this->ok = false;
                        $err = array();
                        $err['code'] = - 1;
                        $err['message'] = 'is not correct';
                        $this->errors[] = $err;
                    }
                    else
                    {
                        $this->ok = true;
                    }

                }

                private function register()
                {
                    $data = array();
                    $request = (array)json_decode(file_get_contents('php://input') , true);
                    $fullname = $request['fullname'];
                    $username = $request['username'];
                    $mobile = $request['mobile'];
                    $email = $request['email'];
                    $interestid = $request['interestid'];
                    $password = $request['password'];
                    $inserted_id = 0;

                    if (strlen($fullname) < 3)
                    {
                        $this->isSuccess = false;
                        $err = array();
                        $err['code'] = - 1;
                        $err['message'] = 'User is empty';
                        $this->errors[] = $err;
                    }
                    else
                    {

                        $queryCheck = 'SELECT
			M.*
			FROM `' . $this
                            ->db->prefix . "members` M
			WHERE M.m_mail='" . trim($email) . "' ";

                        $resultCheck = $this
                            ->db
                            ->query($queryCheck);
                        if ($rowCheck = $this
                            ->db
                            ->fetch($resultCheck))
                        {
                            $this->isSuccess = false;
                            $err = array();
                            $err['code'] = - 2;
                            $err['message'] = 'User is exist';
                            $this->errors[] = $err;
                        }
                        else $this->isSuccess = true;

                        if ($this->isSuccess)
                        {

                            // if user not exist register
                            $dataInsert = array();
                            $dataInsert['m_user'] = trim($username);
                            $dataInsert['m_name'] = trim($fullname);
                            $dataInsert['m_mail'] = trim($email);
                            $dataInsert['m_pass'] = $password;
                            $dataInsert['reg_date'] = time();
                            $dataInsert['last_visit_date'] = time();
                            $dataInsert['m_phone'] = trim($mobile);
                            $dataInsert['interestid'] = trim($interestid);
                            $dataInsert['m_birthdate'] = '';
                            $dataInsert['comment'] = '';
                            $dataInsert['activate_info'] = '';

                            $dataInsert['active'] = 1;

                            $inserted_id = $this
                                ->db
                                ->insert($this
                                ->db->prefix . 'members', $dataInsert);
                            if ($inserted_id == 0) $this->isSuccess = false;
                            else $data['userID'] = $inserted_id;
                            $this->data = $data;

                        }

                    }

                }

            }

            $apiController = new apiController();

            include $apiController->lg_folder . '/index.lang.php';
            $apiController->onLoad();

            /******************* api.php *******************
             *
             * Copyright : (C) 2004 - 2019. All Rights Reserved
             *
             ******************** api.php ******************/;

?>
