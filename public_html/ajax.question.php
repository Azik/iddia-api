<?php
/******************* ajax.question.php *******************
 *
 *
 ******************** ajax.question.php ******************/

/**
 * Include view page class
 */
require_once 'm/classes/viewpage.class.php';

/**
 * search members
 */
class ajaxquestion extends \mcms5xx\classes\ViewPage
{
    public $langs;
    public $permalinks = '';
    public $perma_type = '';
    public $inside_lang = '';
    public $index_lang = '';
    public $errors = array();
    public $data = '';
    public $response = array();
    public $isSuccess = true;
    public $queryWhere = '';
    public $productsCount = 0;
    public $filterItems = array();

    public function __construct()
    {
        parent::__construct();
    }

    public function onLoad()
    {
        if (@$_SERVER['REQUEST_METHOD'] == 'POST') {
            $this->buildQuestions();
        }
    }


    /**
     * build Question process
     */
    private function buildQuestions()
    {
        $this->isSuccess = true;
        //$subCatsArr = $this->utils->UserPostIntArr('sub_cats');
        $testid = $this->utils->UserPostInt('tid');
       
       

        if ($this->isSuccess) {
            /* All OK */
             $sql = 'SELECT 
            S.*, SL.name
				FROM '.$this->db->prefix.'questions S
				INNER JOIN ' .$this->db->prefix."questionslocalizations SL ON SL.questionid = S.questionid
				WHERE SL.lang='" .$this->lang."' && S.active = 1 && S.testid='".(int)$testid."'
				ORDER BY S.questionsdate DESC
			";
			$result = $this->db->query($sql);
			$ndx = 0;
			$slide_nums = $this->db->num_rows($sql);
    
            $ndx = 0;

			   while ($row = $this->db->fetch($result)) {
					
					$id = $row['questionid'];
					$name = $row['name'];
				   
					if ($ndx == 0) {
						$this->template->assign_block_vars('questions', array());
					}
					
					
					++$ndx;	
					$this->template->assign_block_vars('questions.items', array(
						'ID' => $id,
						'NDX' => $ndx,
						'NAME' => $name,
						'NAME_UP' => $this->nameup($name),
						'TITLE' => $name,
					));
					
					$this->template->assign_block_vars('questions.items.answers', array());
					$this->buildAnswers($id, 'questions.items.answers');
				}
            $this->generateResult();

        }

        echo $this->data;

    }

	
	

    private function buildAnswers($questionid, $template)
    {
        $sql = 'SELECT 
            S.*, SL.name
        FROM '.$this->db->prefix.'questionanswers S
		INNER JOIN ' .$this->db->prefix."questionanswerslocalizations SL ON SL.answerid = S.answerid
		WHERE SL.lang='" .$this->lang."' && S.active = 1 && S.questionid='".(int)$questionid."'
		ORDER BY S.answersdate ASC
		";
	   /* echo($sql);	 */
        $result = $this->db->query($sql);
        $ndx = 0;
        $slide_nums = $this->db->num_rows($sql);
        while ($row = $this->db->fetch($result)) {
            
			$checked = '';
            $id = $row['answerid'];
            $name = $row['name'];
           
			if($ndx == 0)
				$checked = ' checked';

            ++$ndx;	
            $this->template->assign_block_vars($template.'.aitems', array(
				'ID' => $id,
				'NDX' => $ndx,
				'CHECKED' => $checked,
				'NAME' => $name,
				'NAME_UP' => $this->nameup($name),
				'TITLE' => $name,
		    ));
			
			
        }
    }
	



    private function generateResult()
    {
        $this->template->set_filenames(array('question_section' => 'question_section.tpl'));
        $return_string = $this->template->pparse('question_section', true);
        echo $return_string;
    }


}

$index = new ajaxquestion();

include $index->lg_folder . '/index.lang.php';
$index->onLoad();

/******************* ajax.question.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** ajax.question.php ******************/;
