<?php
/******************* ajax.login.php *******************
 *
 *
 ******************** ajax.login.php ******************/

/**
 * Include view page class
 */
require_once 'm/classes/viewpage.class.php';

/**
 * Login members
 */
class ajaxLogin extends \mcms5xx\classes\ViewPage
{
    public $langs;
    public $permalinks = '';
    public $perma_type = '';
    public $inside_lang = '';
    public $index_lang = '';
    public $errors = array();
    public $response = array();
    public $isSuccess = true;
    public $redUrl = '';

	public $session_logged = "member_logged";
	public $session_email = "member_email";
	public $session_userid = "member_id";
	public $session_usertype = "member_usertype";
	
    public $prf_logged = -1;
    public $prf_logged_id = -1;

    public function __construct()
    {
        parent::__construct();
    }

    public function onLoad()
    {
        $this->buildPage();

        $this->prf_logged = $this->utils->filterInt($this->utils->GetSession($this->session_logged));
        $this->prf_logged_id = $this->utils->filterInt($this->utils->GetSession($this->session_userid));

        if (@$_SERVER['REQUEST_METHOD'] == 'POST') {
            $action = $this->utils->Post('action');
            switch ($action) {
                case 'otp':
                    $this->Otp();
				break;
                case 'login':
                    $this->Login();
				break;
            }


		} 
    }

    /**
     * Build page
     */
    private function buildPage()
    {
		
	
    }
	
	/**
     * Otp 	
     */
    private function Otp()
    {
		$red_url = '';
		$code = '';
		$this->isSuccess = true;
		$_sign_phone = $this->utils->UserPost('sign_phone');
		
	
		if(strlen(trim($_sign_phone)) != 12){
			$this->errors[] = $this->fromLangIndex('phone_regex_not_correct');
			$this->isSuccess = false;
		}
		
		
		if($this->isSuccess) {
			$otpRes = $this->sendOTP($_sign_phone);
			if($otpRes['isSuccess'] != 1) {
				$this->isSuccess = false;
				$this->errors[]	= $otpRes['errors']; 
			}
			else {
				$this->isSuccess = true;
				$this->errors	= array(); 
			}
		}
	
		
		$this->response['redURL'] = $red_url;
		//$this->response['code'] = $code;
		$this->response['isSuccess'] = $this->isSuccess;
		$this->response['messages'] = $this->errors;
		die(json_encode($this->response));
	
    }
	
	
	private function sendOTP($phone) {
		$apiUrl = $this->fromConfig('apiUrl');	
		$apiUrl = $apiUrl."?msisdn=".$phone."";
		$ret = array();
		
			
			$result = $this->utils->retriveData($apiUrl, "POST");
			$isSuccess = $result['response']['isSuccess'];
			$errors = array();
			if($isSuccess != 1){
				$errors = $result['response']['errors'];
				$ret['errors'] = $errors;
				$ret['isSuccess'] = false;
				
			}
			else {
				$ret['errors'] = array();
				$ret['isSuccess'] = true;
				//$ret['pin'] = $result['response']['body']['pin'];
				$ret['rid'] = $result['response']['body']['rid'];
//				$this->utils->SetSession('otpcode', $ret['pin']);
				$this->utils->SetSession('phone', $phone);
				$this->utils->SetSession('rid', $ret['rid']);
			}
			
			
			
		
		return $ret;
	}
	
	
	private function checkOTP($code) {

		//$otpcode = $this->utils->getSession('otpcode');
		$rid = $this->utils->getSession('rid');
		$phone = $this->utils->getSession('phone');

		$apiUrl = $this->fromConfig('apiUrl');	
		$apiUrl = $apiUrl."?msisdn=".$phone."&pin=".$code."&rid=".$rid."";
		$ret = array();
		
			
			$result = $this->utils->retriveData($apiUrl, "POST");
			$isSuccess = $result['response']['isSuccess'];
			$errors = array();
			if($isSuccess != 1){
				$errors = $result['response']['errors'];
				$ret['errors'] = $errors;
				$ret['isSuccess'] = false;
				
			}
			else {
				$ret['errors'] = array();
				$ret['isSuccess'] = true;
				$ret['otpcode'] = $code;
				$ret['level'] = $result['response']['body']['userInfo']['level'];
				$ret['extraDetail'] = $result['response']['body']['userInfo']['extraDetail'];
			}
			
			
			
		
		return $ret;
	}
	
	/**
     * Login 	
     */
    private function Login()
    {
		$red_url = '';
		$this->isSuccess = true;
		$sign_pin = $this->utils->UserPost('sign_pin');

	
		$phone = $this->utils->getSession('phone');

		
		if($this->isSuccess) {
			/* B: OK */

			
			if($this->isSuccess) {
				$otpRes = $this->checkOTP($sign_pin);
				if($otpRes['isSuccess'] != 1) {
					$this->isSuccess = false;
					$this->errors[]	= $otpRes['errors']; 
				}
				else {
					$this->utils->SetSession($this->session_logged, 9);
					$this->utils->SetSession($this->session_usertype, $otpRes['level']);
					$this->utils->SetSession($this->session_userid, $phone);
					$this->utils->SetSession('extraDetail', $otpRes['extraDetail']);
					$this->utils->SetSession('otpcode', $otpRes['otpcode']);
					$red_url = $this->curr_folder . str_replace('[lang]', $this->lang, $this->permalinks[$this->perma_type]['index'][$this->curr_lang]);
					$this->isSuccess = true;
					$this->errors	= array(); 
				}
			}
			
		}
		
		$this->response['redURL'] = $red_url;
		$this->response['isSuccess'] = $this->isSuccess;
		$this->response['messages'] = $this->errors;
		die(json_encode($this->response));
	
    }
	
}

$index = new ajaxLogin();

include $index->lg_folder . '/index.lang.php';
$index->onLoad();

/******************* ajax.register.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** ajax.register.php ******************/;