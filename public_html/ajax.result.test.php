<?php
/******************* ajax.result.test.php *******************
 *
 *
 ******************** ajax.result.test.php ******************/

/**
 * Include view page class
 */
require_once 'm/classes/viewpage.class.php';

/**
 * Login members
 */
class ajaxResultTest extends \mcms5xx\classes\ViewPage
{
    public $langs;
    public $permalinks = '';
    public $perma_type = '';
    public $inside_lang = '';
    public $index_lang = '';
    public $errors = array();
    public $response = array();
    public $isSuccess = true;
    public $redUrl = '';

	public $session_logged = "member_logged";
	public $session_email = "member_email";
	public $session_userid = "member_id";
	public $session_usertype = "member_usertype";
	
    public $prf_logged = -1;
    public $prf_logged_id = -1;

    public function __construct()
    {
        parent::__construct();
    }

    public function onLoad()
    {
        $this->buildPage();

        $this->prf_logged = $this->utils->filterInt($this->utils->GetSession($this->session_logged));
        $this->prf_logged_id = $this->utils->filterInt($this->utils->GetSession($this->session_userid));

        if (@$_SERVER['REQUEST_METHOD'] == 'POST') {
            //$action = $this->utils->Post('action');
			$this->result();
		} 
    }

    /**
     * Build page
     */
    private function buildPage()
    {
		
	
    }
	
	/**
     * Result Test 	
     */
    private function result()
    {
		$red_url = '';
		$code = '';
		$this->isSuccess = true;
        $answers = $this->utils->UserPostArr('answers');
		
		$_answers = array();
		$variants = array();
		if(count($answers) > 0) {
			
			$_answers = $this->fillAnswers($answers);
			if(count($_answers) > 0) {
			
				$groupVariants = $this->GroupByVariant($_answers); 
				$rowCorrect = $this->correctAnswerById($groupVariants['answerid']);
				//
				$this->template->assign_block_vars('result', array(
					'CORRECT_ANSWER' => $rowCorrect['name']
				));

			}
		}
		else 
			$this->isSuccess = false;

		if($this->isSuccess) {
			$this->generateResult('result_section');
		}
    }
	
	
	private function GroupByVariant($answers) {
		$outputArray = array();

		foreach ( $answers as $record ) {
			if ( !key_exists($record['variant'], $outputArray) ) {
				$outputArray[$record['variant']]['count'] = 0;
				$outputArray[$record['variant']]['answerid'] = $record['answerid'];
			}
			$outputArray[$record['variant']]['count'] += 1;
		}
		
		$max = 0;
		$item = array();
		foreach ($outputArray as $o){
			if((int)$o['count'] >= $max) {
				$max = (int)$o['count'];
				$item = $o;
			}
		}
		
		return $item;
	}
	
	private function fillAnswers($answers) {
		$_answers = array();
		foreach($answers as $answer){
			$a = explode(':', $answer);
			$questionid = $a[0];
			$answerid = $a[1];
			$row = $this->getAnswerById($answerid);
			$arr = array();
			$arr['questionid'] = $questionid;
			$arr['answerid'] = $answerid;
			$arr['variant'] = $row['variant'];
			$_answers[] = $arr;
		}
		
		return $_answers;
	}
	
	
	private function getAnswerById($answerid) {
		$answerSQL = $this->db->query('SELECT 
			S.*
		FROM '.$this->db->prefix.'questionanswers S
		INNER JOIN ' .$this->db->prefix."questionanswerslocalizations SL ON SL.answerid = S.answerid
		WHERE SL.lang='" .$this->lang."' && S.active = 1 && S.answerid='".$answerid."'
		");
		$answerResult = $this->db->fetch($answerSQL);
		return $answerResult;
	}
	
	private function correctAnswerById($answerid) {
		$answerSQL = $this->db->query('SELECT 
			S.*, SL.name
		FROM '.$this->db->prefix.'tests_correct_answers S
		INNER JOIN ' .$this->db->prefix."tests_correct_answerslocalizations SL ON SL.answerid = S.answerid
		WHERE SL.lang='" .$this->lang."' && S.active = 1 && S.answerid='".$answerid."'
		");
		$answerResult = $this->db->fetch($answerSQL);
		return $answerResult;
	}
	
	
	private function generateResult($tpl)
    {
        $this->template->set_filenames(array($tpl => ''.$tpl.'.tpl'));
        $return_string = $this->template->pparse($tpl, true);
        echo $return_string;
    }

	
}

$index = new ajaxResultTest();

include $index->lg_folder . '/index.lang.php';
$index->onLoad();

/******************* ajax.result.test.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** ajax.result.test.php ******************/;