
	<!-- BEGIN::PAGE LEVEL PLUGINS -->
	<link href="{ROOT}assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" type="text/css" />
	<link href="{ROOT}assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
	<link href="{ROOT}assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
	<!-- END::PAGE LEVEL PLUGINS -->
	
	<link href="{ROOT}assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
	<script src="{ROOT}assets/global/plugins/jquery.min.js" type="text/javascript"></script>
	
	<script type="text/javascript">
		
		function clickAc(act) {
			//alert(act);
			var set = $(".checkboxes");
			var fields = '';
			$(set).each(function () {
				var checked = $(this).is(":checked");
				if (checked) {
					var v = $(this).val();
					fields += (fields != '') ? ',' : '';
					fields += v;
					//alert("aaa:"+v);
				}
			});
			if (fields != '')
				window.location.href = '?{MODULE_QS}=news&cateditid=' + act + ':' + fields;
		}
		function clickAcNews(act, catid) {
			//alert(act);
			var set = $(".checkboxes");
			var fields = '';
			$(set).each(function () {
				var checked = $(this).is(":checked");
				if (checked) {
					var v = $(this).val();
					fields += (fields != '') ? ',' : '';
					fields += v;
					//alert("aaa:"+v);
				}
			});
			if (fields != '')
				window.location.href = '?{MODULE_QS}=news&catid=' + catid + '&newsid=' + act + ':' + fields;
		}
		function click_cancel(act, id) {
			if (act == 'news') {
				window.location.href = '?{MODULE_QS}=news&catid=' + id;
			} else {
				window.location.href = '?{MODULE_QS}=news';
			}
		}

		function doCatDelete(id) {
			if (confirm('{CAT_CONFIRM}')) {
				document.getElementById("cat_action").value = 'delete';
				document.getElementById('select_id').value = id;
				document.getElementById('cat_subm').click();
			}
		}
		
		function doCatAction(act, id, mess) {
			document.getElementById("cat_action").value = act;
			document.getElementById('select_id').value = id;

			if (act == 'delete') {
				if (confirm(mess)) {
					document.getElementById("cat_subm").click();
				}
			}
			else {
				document.getElementById("cat_subm").click();
			}
		}

		function doNewsDelete(id) {
			if (confirm('{NEWS_CONFIRM}')) {
				document.getElementById("news_action").value = 'delete';
				document.getElementById('select_id').value = id;
				document.getElementById('news_subm').click();
			}
		}
		
		function doNewsAction(act, id, mess) {
			document.getElementById("news_action").value = act;
			document.getElementById('select_id').value = id;

			if (act == 'delete') {
				if (confirm(mess)) {
					document.getElementById("news_subm").click();
				}
			}
			else {
				document.getElementById("news_subm").click();
			}
		}

		function doOptionsAction(options_action) {
			document.getElementById("options_action").value = options_action;
			document.getElementById("options_subm").click();
		}
		
		function redirect(url) {
			window.location.href = url;
		}
	</script>

		<!-- BEGIN::CONTENT -->
		<div class="page-content-wrapper" style="min-width:450px !important;">
			<!-- BEGIN::CONTENT BODY -->
			<div class="page-content">
				<!-- BEGIN::PAGE HEADER-->
				<!-- BEGIN::PAGE BAR -->
				<div class="page-bar">
					<ul class="page-breadcrumb">
						<li>
							<a href="index.php">{HOME}</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<!-- BEGIN cat -->
						<li>
							<span><a href="index.php?{MODULE_QS}=news">{TITLE}</a></span>
						</li>
						<!-- END cat -->
						<!-- BEGIN news -->
						<li>
							<span><a href="index.php?{MODULE_QS}=news">{TITLE}</a></span>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<span><a href="index.php?{MODULE_QS}=news&catid={news.CAT_ID}">{news.CAT_NAME}</a></span>
						</li>
						<!-- END news -->
					</ul>
				</div>
				<!-- END::PAGE BAR -->
				<!-- BEGIN::PAGE TITLE-->
				<h3 class="page-title"> {TITLE} </h3>
				<!-- END::PAGE TITLE-->
				<!-- END::PAGE HEADER-->
				
				<!-- BEGIN cat -->
				<div class="row">
					<div class="col-md-12">
						<!-- BEGIN::NEWS CAT-->
						<div class="portlet light bordered">
							<div class="portlet-title">
								<div class="caption font-dark">
									<i class="fa fa-calendar"></i>
									<span class="caption-subject bold uppercase">{TITLE}</span>
								</div>
							</div>
							<form action="" method="POST" class="form-horizontal" name="form_cat" id="form_cat">
								<input type="hidden" id="cat_action" name="cat_action" value="">
								<input type="hidden" value="0" id="select_id" name="select_id">
								<input type="submit" id="cat_subm" name="cat_subm" style="display:none;">
							
							<!-- BEGIN list -->
							<div class="portlet-body">
								<!-- BEGIN perm_add -->
								<div class="clearfix">
								
									<div class="btn-group">
										<a href="{cat.list.LAST_NEWS_URL}" class="btn yellow">
											{cat.list.LAST_NEWS} &nbsp; <i class="fa fa-list"></i>
										</a>
									</div>
									<div class="btn-group">
										<a href="{cat.list.ADD_URL}" class="btn green">
											{cat.list.ADD} &nbsp; <i class="fa fa-plus"></i>
										</a>
									</div>
									<div class="btn-group">
										<a href="{cat.list.ADD_NEWS_URL}" class="btn green">
											{cat.list.ADD_NEWS} &nbsp; <i class="fa fa-plus"></i>
										</a>
									</div>
									
								</div>
								<!-- END perm_add -->
								<br/>
								<table class="table table-striped table-bordered table-hover" width="100%" id="news_cat_list">
									<thead>
											<th style="width:0px !important;display:none;"></th>
											<th style="width:12px !important;text-align:center;"><input type="checkbox" class="group-checkable" data-set="#news_cat_list .checkboxes"/></th>
											<th>{cat.list.NAME}</th>
											<th>{cat.list.HEADER}</th>
											<th></th>
										</tr>
									</thead>
									<tbody>
										<!-- BEGIN items -->
										<tr data-position="{cat.list.items.POSITION}" id="{cat.list.items.ID}">
											<td style="display:none;">{cat.list.items.POSITION}</td>
											<td style="width:12px !important;text-align:center;" id="chk_{cat.list.items.ID}"><input type="checkbox" class="checkboxes" value="{cat.list.items.ID}"/></td>
											<td><a href="{cat.list.URL}&catid={cat.list.items.ID}">{cat.list.items.NAME}</a></td>
											<td>{cat.list.items.HEADER}</td>
											<td style="width : 320px !important;">
											    <!-- BEGIN perm_edit -->
												<a href="{cat.list.items.EDIT_URL}" title="{cat.list.EDIT}" class="btn yellow">
													{cat.list.EDIT}
													 &nbsp; <i class="fa fa-pencil"></i>
												</a>
												<!-- END perm_edit -->
												<!-- BEGIN perm_del -->
												<a href="JavaScript:doCatDelete({cat.list.items.ID})" title="{cat.list.DELETE}" class="btn red ask"> 
													{cat.list.DELETE}
													 &nbsp; <i class="fa fa-trash"></i>
												</a>
												<!-- END perm_del -->
												
												<!-- BEGIN active -->
												<a href="{cat.list.items.ACTIVE_URL}" class="btn btn-success">{cat.list.ACTIVE}
													 &nbsp; <i class="fa fa-unlock"></i></a>
												<!-- END active -->
												<!-- BEGIN inactive -->
												<a href="{cat.list.items.INACTIVE_URL}" class="btn btn-danger">{cat.list.INACTIVE}
													 &nbsp; <i class="fa fa-lock"></i></a>
												<!-- END inactive -->
											</td>
										</tr>
										<!-- END items -->
									</tbody>
								</table>
								<!-- BEGIN hidden -->
								<input type="hidden" name="sort_order" id="sort_order" value="{cat.list.hidden.VALUE}"/>
								<!-- END hidden -->
								
								<br/>
								<!-- BEGIN perm_edit -->
								<table>
									<tfoot>
									<tr>
										<td class="ac"><span class="label label-success"
															 onclick="clickAc('activate')">{cat.list.ACTIVATE}</span></td>
										<td style="width:10px;"></td>
										<td class="ac"><span class="label label-danger"
															 onclick="clickAc('inactivate')">{cat.list.INACTIVATE}</span></td>
										<td></td>
									</tr>
									</tfoot>
								</table>
								<!-- END perm_edit -->
							</div>
							<!-- END list -->
							
							<!-- BEGIN catedit -->
							<div class="portlet-body" id="panel_editbar">
								<div class="form-group form-md-checkboxes">
									<label class="col-md-3 control-label" for="txt_active">&nbsp;</label>
									<div class="col-md-6">
										<div class="md-checkbox-list">
											<div class="md-checkbox">
												<input type="checkbox" name="txt_active" id="txt_active" value="1" class="md-check"  {cat.catedit.ACTIVE_CHK} />
												<label for="txt_active">
													<span></span>
													<span class="check"></span>
													<span class="box"></span>{cat.catedit.ACTIVE}
												</label>
											</div>
										</div>
									</div>
								</div>
								<div class="form-group form-md-checkboxes">
									<label class="col-md-3 control-label" for="txt_index">&nbsp;</label>
									<div class="col-md-6">
										<div class="md-checkbox-list">
											<div class="md-checkbox">
												<input type="checkbox" name="txt_index" id="txt_index" value="1" class="md-check" {cat.catedit.INDEX_CHK} />
												<label for="txt_index">
													<span></span>
													<span class="check"></span>
													<span class="box"></span>{cat.catedit.INDEX}
												</label>
											</div>
										</div>
									</div>
								</div>
								<div class="form-group form-md-checkboxes">
									<label class="col-md-3 control-label" for="txt_event">&nbsp;</label>
									<div class="col-md-6">
										<div class="md-checkbox-list">
											<div class="md-checkbox">
												<input type="checkbox" name="txt_event" id="txt_event" value="1" class="md-check" {cat.catedit.EVENT_CHK} />
												<label for="txt_event">
													<span></span>
													<span class="check"></span>
													<span class="box"></span>{cat.catedit.EVENT}
												</label>
											</div>
										</div>
									</div>
								</div>									
								<div class="tabbable-line boxless tabbable-reversed">
									<ul class="nav nav-tabs">
										<!-- BEGIN tab -->
										<li{cat.catedit.tab.CLASS}><a href="#content_{cat.catedit.tab.LANG}" data-toggle="tab">{cat.catedit.tab.LANG}</a></li>
										<!-- END tab -->
									</ul>
								</div>
								
								<div class="tab-content">
									<!-- BEGIN tab -->
									<div class="tab-pane {cat.catedit.tab.FADE_CLASS}" id="content_{cat.catedit.tab.LANG}">
										<div class="portlet box green">
										<div class="portlet-title">
											<div class="caption">
												<i class="fa fa-language"></i>{cat.catedit.tab.LANG} 
											</div>
										</div>
										<div class="portlet-body form">
										<div class="form-group form-md-line-input">
											<label class="col-md-3 control-label" for="txt_name_{cat.catedit.tab.LANG}">{cat.catedit.tab.NAME}</label>
											<div class="col-md-8">
												<input type="text" class="form-control" name="txt_name_{cat.catedit.tab.LANG}"
													   id="txt_name_{cat.catedit.tab.LANG}" value="{cat.catedit.tab.NAME_VALUE}"
													   placeholder="{cat.catedit.tab.NAME}">
												<div class="form-control-focus"> </div>
											</div>
										</div>
										<div class="form-group form-md-line-input">
											<label class="col-md-3 control-label" for="txt_header_{cat.catedit.tab.LANG}">{cat.catedit.tab.HEADER}</label>
											<div class="col-md-8">
												<textarea class="form-control ckeditor" name="txt_header_{cat.catedit.tab.LANG}"
														  id="txt_header_{cat.catedit.tab.LANG}" rows="3">{cat.catedit.tab.HEADER_VALUE}</textarea>
												<div class="form-control-focus"> </div>
											</div>
										</div>
										<div class="form-actions">
											<div class="row">
												<div class="col-md-offset-3 col-md-9">
													<button type="button" class="btn blue" id="btn_save" name="btn_save"
															onClick="doCatAction('save', {cat.catedit.ID},  '');"><i class="fa fa-ok"></i> {cat.catedit.SAVE}
													</button>
													<button type="reset" class="btn" id="btn_cancel" name="btn_cancel"
															onclick="click_cancel('cat', 0);">{cat.catedit.CANCEL}</button>
												</div>
											</div>
										</div>
										</div>
										</div>
									</div>
									<!-- END tab -->
								</div>
							
							</div>
							<!-- END catedit -->
							
							</form>
						</div>
						<!-- END::NEWS CAT-->
					</div>
				</div>
				<!-- END cat -->
				
				
				<!-- BEGIN newscat_options -->
				<div class="row">
					<div class="col-md-12">
						<!-- B: TABLE PORTLET-->
						<button type="button" id="newscat_button" class="btn green" style="margin-bottom:15px !important;">{newscat_options.TITLE} &nbsp; <i class="fa fa-plus"></i></button>
						<script>
							$("#newscat_button").click(function () {
								$("#newscat_options").toggle("fade", 1000);
							});
						</script>
						
						<div class="portlet light bordered" id="newscat_options" style="display: none;">
							<div class="portlet-title" style="margin-bottom:-10px;">
								<h4>{newscat_options.TITLE}</h4>
							</div>
							<form action="" method="POST" name="options_form" id="options_form">
								<input type="hidden" id="options_action" name="action" value="">
								<input type="submit" id="options_subm" name="options_subm" style="display:none;">

								<div class="portlet-body form" id="panel_editbar">
								<div class="form-body">
									<div class="form-group">
										<label>{newscat_options.NEWS_FOOT_LIMIT}</label>
										<input type="number" class="form-control input-medium" name="txt_news_foot_limit" id="txt_news_foot_limit"
												   value="{newscat_options.NEWS_FOOT_LIMIT_VALUE}"
												   placeholder="{newscat_options.NEWS_FOOT_LIMIT}" >
									</div>
									<div class="form-group">
										<label>{newscat_options.NEWS_INDEX_LIMIT}</label>
										<input type="number" class="form-control input-medium" name="txt_news_index_limit" id="txt_news_index_limit"
												   value="{newscat_options.NEWS_INDEX_LIMIT_VALUE}"
												   placeholder="{newscat_options.NEWS_INDEX_LIMIT}" >
									</div>
									<div class="form-group">
										<label>{newscat_options.NEWS_PAGE_LIMIT}</label>
										<input type="number" class="form-control input-medium" name="txt_news_page_limit" id="txt_news_page_limit"
												   value="{newscat_options.NEWS_PAGE_LIMIT_VALUE}"
												   placeholder="{newscat_options.NEWS_PAGE_LIMIT}" >
									</div>
									<div class="form-group">
										<label>{newscat_options.NEWS_BLOCK_SEPERATOR}</label>
										<input type="number" class="form-control input-medium" name="txt_news_block_seperator" id="txt_news_block_seperator"
												   value="{newscat_options.NEWS_BLOCK_SEPERATOR_VALUE}"
												   placeholder="{newscat_options.NEWS_BLOCK_SEPERATOR}" >
									</div>
									<div class="form-group">
										<label>{newscat_options.NEWS_DATE_FORMAT}</label>
										<input type="text" class="form-control input-medium" name="txt_news_date_format" id="txt_news_date_format"
												   value="{newscat_options.NEWS_DATE_FORMAT_VALUE}"
												   placeholder="{newscat_options.NEWS_DATE_FORMAT}" >
									</div>
									
									<div class="form-actions">
										<button type="button" class="btn blue" id="btn_save" name="btn_save"
												onClick="doOptionsAction('save_newscat_options');"><i class="fa fa-ok"></i> {newscat_options.SAVE}</button>
										<button type="reset" class="btn" id="btn_cancel" name="btn_cancel"
												onclick="click_cancel('cat', 0);">{newscat_options.CANCEL}</button>
									</div>
								</div>
								</div>

							</form>

						</div>
						<!-- E: TABLE PORTLET-->

					</div>
				</div>
				<!-- END newscat_options -->
				
				<!-- BEGIN news -->
				<div class="row">
					<div class="col-md-12">
						<!-- BEGIN::NEWS CAT-->
						<div class="portlet light bordered">
							<div class="portlet-title">
								<div class="caption font-dark">
									<i class="fa fa-calendar"></i>
									<span class="caption-subject bold uppercase">{news.CAT_NAME}</span>
								</div>
							</div>
							<form action="" method="POST" class="form-horizontal" name="form_news" id="form_news">
								<input type="hidden" id="news_action" name="news_action" value="">
								<input type="hidden" value="0" id="select_id" name="select_id">
								<input type="submit" id="news_subm" name="news_subm" style="display:none;">
							
								<!-- BEGIN list -->
								<div class="portlet-body">
									<!-- BEGIN perm_add -->
									<div class="clearfix">
										<div class="btn-group">
											<a href="{news.list.ADD_URL}" class="btn green">
												{news.list.ADD} &nbsp; <i class="fa fa-plus"></i>
											</a>
										</div>
									</div>
									<!-- END perm_add -->
									<br/>
									<table class="table table-striped table-bordered table-hover" width="100%" id="news_list">
										<thead>
											<tr>
												<th style="width:12px !important;"><input type="checkbox" class="group-checkable" data-set="#news_list .checkboxes"/></th>
												<th>{news.list.NAME}</th>
												<th style="width:150px !important;">{news.list.DATE}</th>
												<th style="width:280px !important;"></th>
											</tr>
										</thead>
										<tbody>
											<!-- BEGIN items -->
											<tr id="{news.list.items.ID}">
												<td id="chk_{news.list.items.ID}"><input type="checkbox" class="checkboxes" value="{news.list.items.ID}"/></td>
												<td><a href="{news.list.items.EDIT_URL}" title="{news.list.EDIT}">{news.list.items.NAME}</a></td>
												<td>{news.list.items.DATE}</td>
												<td>{news.list.items.ACTIVE}</td>
												<td>{news.list.items.INDEX}</td>
												<td>{news.list.items.TOTAL_VIEWS}</td>
												<td>{news.list.items.MONTH_VIEWS}</td>
												<td>{news.list.items.READ_DATE}</td>
												<td>{news.list.items.COMMENT}</td>
												<td>
												<!-- BEGIN perm_edit -->
												<a href="{news.list.items.EDIT_URL}" title="{news.list.EDIT}" class="btn yellow"> {news.list.EDIT}&nbsp; <i class="fa fa-pencil"></i></a>
												<!-- END perm_edit -->
												<!-- BEGIN perm_del -->
												<a href="JavaScript:doNewsDelete({news.list.items.ID})" title="{news.list.DELETE}" class="btn red ask" title="{cat.list.DELETE}"> {news.list.DELETE}&nbsp; <i class="fa fa-trash"></i></a>
												<!-- END perm_del -->
													<!-- BEGIN active -->
													<a href="{news.list.items.ACTIVE_URL}" class="btn btn-success">{news.list.ACTIVE}&nbsp; <i class="fa fa-unlock"></i></a>
													<!-- END active -->
													<!-- BEGIN inactive -->
													<a href="{news.list.items.INACTIVE_URL}" class="btn btn-danger">{news.list.INACTIVE}&nbsp; <i class="fa fa-lock"></i></a>
													<!-- END inactive -->
												</td>
											</tr>
											<!-- END items -->
										</tbody>
									</table>
									<!-- BEGIN hidden -->
									<input type="hidden" name="sort_order" id="sort_order" value="{news.list.hidden.VALUE}"/>
									<!-- END hidden -->
									
									<br/>
									<!-- BEGIN perm_edit -->
									<table>
										<tfoot>
										<tr>
											<td class="ac"><span class="btn btn-success"
																 onclick="clickAcNews('activate', {news.list.CATID})">{news.list.ACTIVATE}</span></td>
											<td style="width:10px;"></td>
											<td class="ac"><span class="btn btn-danger"
																 onclick="clickAcNews('inactivate', {news.list.CATID})">{news.list.INACTIVATE}</span></td>
											<td></td>
										</tr>
										</tfoot>
									</table>
									<!-- END perm_edit -->
								</div>
								<!-- END list -->
								
								<!-- BEGIN edit -->
								<div class="portlet-body" id="panel_editbar">
									<div class="form-group form-md-line-input">
										<label class="col-md-3 control-label" for="txt_category">{news.edit.CATEGORY}</label>
										<div class="col-md-6">
											<select class="form-control" name="txt_category" id="txt_category">
												<!-- BEGIN category -->
												<option value="{news.edit.category.VALUE}" {news.edit.category.SELECTED}>{news.edit.category.TEXT}</option>
												<!-- END category -->
											</select>
											<div class="form-control-focus"> </div>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3" for="datetimepicker">{news.edit.DATE}</label>
										<div class="col-md-4">
											<div class="input-group date form_datetime">
												<input type="text" size="16" value="{news.edit.DATE_VALUE}" class="form-control" id="datetimepicker" name="cal_newsdate" >
												<span class="input-group-btn">
													<button class="btn default date-set" type="button">
														<i class="fa fa-calendar"></i>
													</button>
												</span>
											</div>
										</div>
									</div>
									<div class="form-group form-md-checkboxes">
										<label class="col-md-3 control-label" for="txt_active">&nbsp;</label>
										<div class="col-md-6">
											<div class="md-checkbox-list">
												<div class="md-checkbox">
													<input type="checkbox" name="txt_active" id="txt_active" value="1" class="md-check" {news.edit.ACTIVE_CHK}/>
													<label for="txt_active">
														<span></span>
														<span class="check"></span>
														<span class="box"></span>{news.edit.ACTIVE}
													</label>
												</div>
											</div>
										</div>
									</div>
									<div class="form-group form-md-checkboxes">
										<label class="col-md-3 control-label" for="txt_index">&nbsp;</label>
										<div class="col-md-6">
											<div class="md-checkbox-list">
												<div class="md-checkbox">
													<input type="checkbox" name="txt_index" id="txt_index" value="1" class="md-check" {news.edit.INDEX_CHK}/>
													<label for="txt_index">
														<span></span>
														<span class="check"></span>
														<span class="box"></span>{news.edit.INDEX}
													</label>
												</div>
											</div>
										</div>
									</div>
									<div class="form-group form-md-line-input">
										<label class="col-md-3 control-label" for="txt_template">{news.edit.TEMPLATE}</label>
										<div class="col-md-6">
											<select class="form-control" name="txt_template" id="txt_template">
												<!-- BEGIN templates -->
												<option value="{news.edit.templates.VALUE}" {news.edit.templates.SELECTED}>{news.edit.templates.TEXT}</option>
												<!-- END templates -->
											</select>
											<div class="form-control-focus"> </div>
										</div>
									</div>
									
									<div class="tabbable-line boxless tabbable-reversed">
										<ul class="nav nav-tabs">
											<!-- BEGIN tab -->
											<li{news.edit.tab.CLASS}><a href="#content_{news.edit.tab.LANG}" data-toggle="tab">{news.edit.tab.LANG}</a></li>
											<!-- END tab -->
										</ul>
									</div>
									<div class="tab-content">
										<!-- BEGIN tab -->
										<div class="tab-pane {news.edit.tab.FADE_CLASS}" id="content_{news.edit.tab.LANG}">
											<div class="portlet box green">
											<div class="portlet-title">
												<div class="caption">
													<i class="fa fa-language"></i>{news.edit.tab.LANG}
												</div>
											</div>
											<div class="portlet-body form">
											<div class="form-group form-md-line-input">
												<label class="col-md-3 control-label" for="txt_name_{news.edit.tab.LANG}">{news.edit.tab.NAME}</label>
												<div class="col-md-8">
													<input type="text" class="form-control" name="txt_name_{news.edit.tab.LANG}"
														   id="txt_name_{news.edit.tab.LANG}" value="{news.edit.tab.NAME_VALUE}"
														   placeholder="{news.edit.tab.NAME}">
													<div class="form-control-focus"> </div>
												</div>
											</div>
											<div class="form-group form-md-line-input">
												<label class="col-md-3 control-label cursor-pointer" for="txt_header_{news.edit.tab.LANG}" onclick="openPhoto($('#txt_header_{news.edit.tab.LANG}').val(), 'txt_header_{news.edit.tab.LANG}')">{news.edit.tab.HEADER}</label>
												<div class="col-md-8">
													<input type="text" class="form-control" name="txt_header_{news.edit.tab.LANG}"
														   id="txt_header_{news.edit.tab.LANG}" value="{news.edit.tab.HEADER_VALUE}"
														   placeholder="{news.edit.tab.HEADER}">
													<div class="form-control-focus"> </div>
												</div>
											</div>
											<div class="form-group form-md-line-input">
												<label class="col-md-3 control-label" for="txt_comment_{news.edit.tab.LANG}">{news.edit.tab.COMMENT}</label>
												<div class="col-md-8">
													<textarea class="form-control" name="txt_comment_{news.edit.tab.LANG}"
															  id="txt_comment_{news.edit.tab.LANG}" rows="3">{news.edit.tab.COMMENT_VALUE}</textarea>
													<div class="form-control-focus"> </div>
												</div>
											</div>
											<div class="form-group form-md-line-input">
												<label class="col-md-3 control-label" for="txt_text_{news.edit.tab.LANG}">{news.edit.tab.TEXT}</label>
												<div class="col-md-8">
													<textarea class="form-control ckeditor" name="txt_text_{news.edit.tab.LANG}"
															  id="txt_text_{news.edit.tab.LANG}" rows="3">{news.edit.tab.TEXT_VALUE}</textarea>
													<div class="form-control-focus"> </div>
												</div>
											</div>
											
											
											<div class="form-group form-md-line-input">
												<label class="col-md-3 control-label" for="seo_header_{news.edit.tab.LANG}">{news.edit.tab.SEO_HEADER}</label>
												<div class="col-md-8">
													<input type="text" class="form-control" name="seo_header_{news.edit.tab.LANG}"
														   id="seo_header_{news.edit.tab.LANG}" value="{news.edit.tab.SEO_HEADER_VALUE}"
														   placeholder="{news.edit.tab.SEO_HEADER}">
													<div class="form-control-focus"> </div>
												</div>
											</div>
											<div class="form-group form-md-line-input">
												<label class="col-md-3 control-label" for="seo_description_{news.edit.tab.LANG}">{news.edit.tab.SEO_DESCRIPTION}</label>
												<div class="col-md-8">
													<textarea class="form-control" name="seo_description_{news.edit.tab.LANG}"
															  id="seo_description_{news.edit.tab.LANG}" rows="3">{news.edit.tab.SEO_DESCRIPTION_VALUE}</textarea>
													<div class="form-control-focus"> </div>
												</div>
											</div>
											<div class="form-group form-md-line-input">
												<label class="col-md-3 control-label" for="seo_keywords_{news.edit.tab.LANG}">{news.edit.tab.SEO_KEYWORDS}</label>
												<div class="col-md-8">
													<textarea class="form-control" name="seo_keywords_{news.edit.tab.LANG}"
															  id="seo_keywords_{news.edit.tab.LANG}" rows="3">{news.edit.tab.SEO_KEYWORDS_VALUE}</textarea>
													<div class="form-control-focus"> </div>
												</div>
											</div>
											
											<div class="form-group form-md-line-input">
												<label class="col-md-3 control-label" for="txt_slug_{news.edit.tab.LANG}">{news.edit.tab.SLUG}</label>
												<div class="col-md-8">
													<input type="text" class="form-control" name="txt_slug_{news.edit.tab.LANG}"
														   id="txt_slug_{news.edit.tab.LANG}" value="{news.edit.tab.SLUG_VALUE}"
														   placeholder="{news.edit.tab.SLUG}">
													<div class="form-control-focus"> </div>
												</div>
											</div>
											<div class="form-actions">
												<div class="row">
													<div class="col-md-offset-3 col-md-9">
														<button type="button" class="btn blue" id="btn_save" name="btn_save"
																onclick="doNewsAction('save', {news.edit.ID},  '');"><i class="fa fa-ok"></i> {news.edit.SAVE}
														</button>
														<button type="reset" class="btn" id="btn_cancel" name="btn_cancel"
																onclick="click_cancel('news', {news.edit.CATID});">{news.edit.CANCEL}</button>
													</div>
												</div>
											</div>
											</div>
											</div>
										</div>
										<!-- END tab -->
									</div>
								
								</div>
								<!-- END edit -->
								
							</form>
						</div>
						<!-- END::NEWS CAT-->
					</div>
				</div>
				<!-- END news -->
				
			</div>
			<!-- END::CONTENT BODY -->
		</div>
		<!-- END::CONTENT -->
	</div>
	<!-- END::CONTAINER -->
	
	
	
	<!-- BEGIN news -->
	<!-- BEGIN::PAGE LEVEL PLUGINS -->
	<script src="{ROOT}assets/global/scripts/datatable.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
	<!-- END::PAGE LEVEL PLUGINS -->
	<!-- END news -->
	
	<script type="text/javascript" charset="utf-8">
		$(document).ready(function () {
			$('.group-checkable').click(function(){
				//alert($('.group-checkable').is(":checked"));
				var ch = $('.group-checkable').is(":checked");
				var set = $(".checkboxes");
				$(set).each(function () {
					$(this).prop('checked', ch);
					var v = $(this).val();
					if (ch) {
						$("#chk_" + v).find('span').addClass('checked');
					} else {
						$("#chk_" + v).find('span').removeClass('checked');
					}
				});
			});
			
			$('#datetimepicker').datetimepicker({
				format: 'yyyy-mm-dd hh:ii',
				autoclose: true
			});
		});
	</script>
	<!-- BEGIN cat -->
	<script src="{ROOT}assets/global/scripts/dt/jquery.js" type="text/javascript"></script>
	<!-- BEGIN catedit -->
	<script src="{ROOT}assets/global/plugins/jquery.min.js" type="text/javascript"></script>
	<!-- END catedit -->
	<!-- 
	<script src="{ROOT}assets/global/scripts/datatable.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
	 -->
	 
	<script src="{ROOT}assets/global/scripts/dt/jquery-ui.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/scripts/dt/jquery.dataTables.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/scripts/dt/jquery.dataTables.rowReordering.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/scripts/dt/jquery.dataTables.rowGrouping.js" type="text/javascript"></script>
	
	<script type="text/javascript" charset="utf-8">
		$(document).ready(function () {
			var sortInput = jQuery('#sort_order');
			
			var oTable= $("#news_cat_list").dataTable({
				"bJQueryUI": true,
				"bDestroy": false,
				"bProcessing": false,
				"bSortable": false,
			});
		
			oTable.rowReordering({
				sURL : 'news_drag.php', 
				fnAlert: function(message) {
					//alert("order"); 
				}
			});
							//.clear().destroy()
			$('.group-checkable').click(function(){
				//alert($('.group-checkable').is(":checked"));
				var ch = $('.group-checkable').is(":checked");
				$('.checkboxes').prop('checked', ch);
			});
		});
	</script>
	<!-- END cat -->
	<!-- BEGIN news -->
	<!-- BEGIN list -->
	<script>
	$(document).ready(function () {
		var newsTable= $("#news_list").dataTable({
			"bJQueryUI": true,
			"bDestroy": false,
			"bProcessing": true,
			"bSortable": false,
			"processing": true,
			"serverSide": true,
			"ajax":  'ajax_news_list.php?catid={news.list.CATID}&rand_num=' + Math.floor((Math.random() * 10000000))
		});
	});
	</script>
	<!-- END list -->
	<!-- END news -->
	
	<script src="{ROOT}assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>

	<!-- BEGIN::THEME GLOBAL SCRIPTS -->
	<script src="{ROOT}assets/global/scripts/app.js" type="text/javascript"></script>
	<!-- END::THEME GLOBAL SCRIPTS -->
	<!-- BEGIN::PAGE LEVEL SCRIPTS -->
	<script src="{ROOT}assets/pages/scripts/table-datatables-responsive.js" type="text/javascript"></script>
	<!-- END::PAGE LEVEL SCRIPTS -->
	
	<script src="{ROOT}assets/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
	
	<!-- BEGIN news -->
	<!-- BEGIN edit -->
	<!-- BEGIN tab -->
	<script>
		CKEDITOR.replace( 'txt_text_{news.edit.tab.LANG}', {
			filebrowserImageBrowseUrl: 'ck.photo.php',
		} );
	</script>
	<!-- END tab -->
	<!-- END edit -->
	<!-- END news -->