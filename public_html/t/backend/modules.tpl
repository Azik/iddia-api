	<link href="{ROOT}assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" type="text/css" />
	<link href="{ROOT}assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
	<link href="{ROOT}assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
	
	<link href="{ROOT}assets/global/plugins/codemirror/lib/codemirror.css" rel="stylesheet" type="text/css" />
	<link href="{ROOT}assets/global/plugins/codemirror/theme/neat.css" rel="stylesheet" type="text/css" />
	<link href="{ROOT}assets/global/plugins/codemirror/theme/ambiance.css" rel="stylesheet" type="text/css" />
	<link href="{ROOT}assets/global/plugins/codemirror/theme/material.css" rel="stylesheet" type="text/css" />
	<link href="{ROOT}assets/global/plugins/codemirror/theme/neo.css" rel="stylesheet" type="text/css" />
 
	<script src="{ROOT}assets/global/plugins/jquery.min.js" type="text/javascript"></script>

	<script type="text/javascript">
		
		function doDelete(id) {
			document.getElementById("action").value = 'delete';
			document.getElementById('action_id').value = id;
			document.getElementById('action_submit').click();
		}

		function doAction(act, id, mess) {
			document.getElementById("action_id").value = id;
			document.getElementById("action").value = act;

			if (act == 'delete') {
				if (confirm(mess)) {
					document.getElementById("action_submit").click();
				}
			}
			else {
				document.getElementById("action_submit").click();
			}
		}

		function click_cancel() {
			window.location.href = '?{MODULE_QS}=modules';
		}

		function redirect(url) {
			window.location.href = url;
		}
	</script>

		<!-- BEGIN::CONTENT -->
		<div class="page-content-wrapper" style="min-width:750px !important;">
			<!-- BEGIN::CONTENT BODY -->
			<div class="page-content">
				<!-- BEGIN::PAGE HEADER-->
				<!-- BEGIN::PAGE BAR -->
				<div class="page-bar">
					<ul class="page-breadcrumb">
						<li>
							<a href="index.php">{HOME}</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<span><a href="index.php?{MODULE_QS}=modules">{TITLE}</a></span>
						</li>
					</ul>
				</div>
				<!-- END::PAGE BAR -->
				<!-- BEGIN::PAGE TITLE-->
				<h3 class="page-title"> {TITLE} </h3>
				<!-- END::PAGE TITLE-->
				<!-- END::PAGE HEADER-->
				
                <!-- BEGIN modules -->
				<div class="row">
					<div class="col-md-12">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-dark">
								<i class="fa fa-code"></i>
								<span class="caption-subject bold uppercase">{TITLE}</span>
								<!-- BEGIN sub_name -->
								<span class="caption-subject">{modules.sub_name.TITLE}</span>
								<!-- END sub_name -->
							</div>
						</div>
					<form name="form_action" id="form_action" class="form-horizontal" method="post" action="" enctype="multipart/form-data">
                    <input type="hidden" id="action" name="action" value="0">
                    <input type="hidden" id="action_id" name="action_id" value="0">
                    <input type="submit" id="action_submit" name="action_submit" style="display:none;">
                    <!-- BEGIN list -->
                    <div class="portlet-body">
						<table class="table table-striped table-bordered table-hover" width="100%" id="modules_list">
                            <thead>
                            <tr>
								<th style="width:0px !important;display:none;"></th>
                                <th style="width : 150px !important;">{modules.list.NAME}</th>
                                <th>{modules.list.DESCRIPTION}</th>
                                <th style="width : 192px !important;">{modules.list.EDIT} / {modules.list.DELETE}</th>
                                <th style="width : 80px !important;">{modules.list.ACTIVE}</th>
                                <th style="min-width : 200px !important;">{modules.list.OPERATIONS}</th>
                            </tr>
                            </thead>
							<tbody>
                            <!-- BEGIN items -->
							<tr data-position="{modules.list.items.POSITION}" id="{modules.list.items.ID}">
								<td style="display:none;">{modules.list.items.POSITION}</td>
								<td style="width : 150px !important;">{modules.list.items.NAME}</td>
								<td>{modules.list.items.DESCRIPTION}</td>
								<td class="option text-center" style="width : 192px !important;" title="{modules.list.EDIT}"><a
											href="{modules.URL}&id={modules.list.items.ID}" class="btn yellow">{modules.list.EDIT}
										&nbsp; <i class="fa fa-pencil"></i>
									</a>
									
									<!-- BEGIN delete -->
									<a href="JavaScript:doDelete({modules.list.items.ID})"
									   title="{modules.list.DELETE}" class="btn red ask" onclick="return confirm('{modules.list.DELETE_CONFIRM}');">
										{modules.list.DELETE}
										&nbsp; <i class="fa fa-trash"></i>
									   </a>
									<!-- END delete -->

									<!-- BEGIN no_delete -->
									<span class="btn default" disabled title="{modules.list.DELETE}">{modules.list.DELETE}
										&nbsp; <i class="fa fa-trash"></i>
									</span>
									<!-- END no_delete -->
								</td>
								<td class="option text-center" style="width : 80px !important;">{modules.list.items.ACTIVE}</td>
								<td class="option" style="min-width : 200px !important;">
									
									<!-- BEGIN view_link -->
									<a href="{modules.list.items.EDIT_VIEW}"
									   title="{modules.list.EDIT_VIEW}" class="btn green">
										<i class="fa fa-code"></i> 
									   </a>
									<!-- END view_link -->
									<!-- BEGIN view_link_template -->
									<a href="{modules.list.items.EDIT_VIEW_TEMPLATE}"
									   title="{modules.list.EDIT_VIEW_TEMPLATE}" class="btn green">
									    <i class="fa fa-html5"></i> 
									   </a>
									<!-- END view_link_template -->
									<!-- BEGIN admin_link -->
									<a href="{modules.list.items.EDIT_ADMIN}"
									   title="{modules.list.EDIT_ADMIN}" class="btn green">
										<i class="fa fa-file-code"></i> 
									   </a>
									<!-- END admin_link -->
									<!-- BEGIN admin_link_template -->
									<a href="{modules.list.items.EDIT_ADMIN_TEMPLATE}"
									   title="{modules.list.EDIT_ADMIN_TEMPLATE}" class="btn green">
										 <i class="fa fa-css3"></i> 
									   </a>
									<!-- END admin_link_template -->
									<!-- BEGIN cnfg_link -->
									<a href="{modules.list.items.EDIT_CNFG}"
									   title="{modules.list.EDIT_CNFG}" class="btn green">
										<i class="fa fa-cog"></i> 
									   </a>
									<!-- END cnfg_link -->
									<!-- BEGIN seo_link -->
									<a href="{modules.list.items.EDIT_SEO}"
									   title="{modules.list.EDIT_SEO}" class="btn green">
										<i class="fa fa-search"></i>
									   </a>
									<!-- END seo_link -->
									<!-- BEGIN language_link -->
									<a href="{modules.list.items.EDIT_LANGUAGE}"
									   title="{modules.list.EDIT_LANGUAGE}" class="btn green">
										<i class="fa fa-language"></i> 
									   </a>
									<!-- END language_link -->
								</td>
							</tr>
                            <!-- END items -->
						</tbody>
						</table>
						
						<!-- BEGIN hidden -->
						<input type="hidden" name="sort_order" id="sort_order" value="{modules.list.hidden.VALUE}"/>
						<!-- END hidden -->
                        <br/>

                        <div class="clearfix">
                            <div class="btn-group">
                                <button type="button" id="sample_editable_1_new"
                                        onclick="document.location.href='{modules.URL}&mode=add'" class="btn green">
                                    {modules.list.ADD} &nbsp; <i class="fa fa-plus"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <!-- END list -->

                    <!-- BEGIN add -->
                    <div class="portlet-body" id="panel_editbar">
						<div class="form-group form-md-checkboxes">
							<label class="col-md-3 control-label" for="txt_active">&nbsp;</label>
							<div class="col-md-6">
								<div class="md-checkbox-list">
									<div class="md-checkbox">
										<input type="checkbox" name="txt_active" id="txt_active" value="1" class="md-check" {modules.add.ACTIVE_CHK} />
										<label for="txt_active">
											<span></span>
											<span class="check"></span>
											<span class="box"></span>{modules.add.ACTIVE}
										</label>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group form-md-line-input">
							<label class="col-md-3 control-label" for="txt_file">{modules.add.FILE}</label>
							<div class="col-md-8">
								<span class="fileupload-new">{modules.add.SELECT_FILE}</span>
								<span class="fileupload-exists">{modules.add.CHANGE}</span>
								<input name="txt_file" class="form-control" id="txt_file" type="file" class="default" accept=".zip"/>
								<div class="form-control-focus"></div>
							</div>
						</div>
						
                        <!-- BEGIN alert -->
                        <div class="form-actions" style="color:#f00;">
                            {modules.add.alert.MESSAGE}
                        </div>
                        <!-- END alert -->
						<div class="form-actions">
							<div class="row">
								<div class="col-md-offset-3 col-md-9">
									<button type="button" class="btn blue" id="btn_save" name="btn_save"
											onclick="doAction('add', 0,  '');"><i class="fa fa-ok"></i> {modules.add.INSTALL}
									</button>
									<button type="reset" class="btn" id="btn_cancel" name="btn_cancel"
											onclick="click_cancel();">{modules.add.CANCEL}</button>
								</div>
							</div>
						</div>
                    </div>
                    <!-- END add -->

                    <!-- BEGIN edit -->
					<div class="portlet-body form-body" id="panel_editbar">
						<div class="form-group form-md-checkboxes">
							<label class="col-md-3 control-label" for="txt_active">&nbsp;</label>
							<div class="col-md-6">
								<div class="md-checkbox-list">
									<div class="md-checkbox">
										<input type="checkbox" name="txt_active" id="txt_active" value="1" class="md-check" {modules.edit.ACTIVE_CHK}/>
										<label for="txt_active">
											<span></span>
											<span class="check"></span>
											<span class="box"></span>{modules.edit.ACTIVE}
										</label>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group form-md-checkboxes">
							<label class="col-md-3 control-label" for="txt_admin">&nbsp;</label>
							<div class="col-md-6">
								<div class="md-checkbox-list">
									<div class="md-checkbox">
										<input type="checkbox" name="txt_admin" id="txt_admin" value="1" class="md-check" {modules.edit.ADMIN_CHK}/>
										<label for="txt_admin">
											<span></span>
											<span class="check"></span>
											<span class="box"></span>{modules.edit.ADMIN}
										</label>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group form-md-checkboxes">
							<label class="col-md-3 control-label" for="txt_view">&nbsp;</label>
							<div class="col-md-6">
								<div class="md-checkbox-list">
									<div class="md-checkbox">
										<input type="checkbox" name="txt_view" id="txt_view" value="1" class="md-check" {modules.edit.VIEW_CHK}/>
										<label for="txt_view">
											<span></span>
											<span class="check"></span>
											<span class="box"></span>{modules.edit.VIEW}
										</label>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">{modules.edit.ICON}</label>
							<div class="col-md-4">
								<select class="bs-select form-control" style="font-family: 'Font Awesome 5 Free', 'Font Awesome 5 Brands', Arial;" name="txt_icon" id="txt_icon" data-show-subtext="true">
									<!-- BEGIN icons -->
									<option value="{modules.edit.icons.VALUE}" {modules.edit.icons.SELECTED} data-icon="{modules.edit.icons.ICON}">{modules.edit.icons.ICON_CODE} {modules.edit.icons.ICON}</option>
									<!-- END icons -->
								</select>
							</div>
						</div>
						<div class="form-group form-md-line-input">
							<label class="col-md-3 control-label" for="txt_description">{modules.edit.DESCRIPTION}</label>
							<div class="col-md-8">
								<textarea class="form-control ckeditor" name="txt_description"
										  id="txt_description" rows="3">{modules.edit.DESCRIPTION_VALUE}</textarea>
								<div class="form-control-focus"> </div>
							</div>
						</div>								
						<div class="tabbable-line boxless tabbable-reversed">
							<ul class="nav nav-tabs">
								<!-- BEGIN tab -->
								<li{modules.edit.tab.CLASS}><a href="#content_{modules.edit.tab.LANG}" data-toggle="tab">{modules.edit.tab.LANG}</a></li>
								<!-- END tab -->
							</ul>
						</div>
						<div class="tab-content">
							<!-- BEGIN tab -->
							<div class="tab-pane {modules.edit.tab.FADE_CLASS}" id="content_{modules.edit.tab.LANG}">
								<div class="portlet box green">
								<div class="portlet-title">
									<div class="caption">
										<i class="fa fa-language"></i>{modules.edit.tab.LANG} 
									</div>
								</div>
								<div class="portlet-body form">
								<div class="form-group form-md-line-input">
									<label class="col-md-3 control-label" for="txt_title_{modules.edit.tab.LANG}">{modules.edit.tab.TITLE}</label>
									<div class="col-md-8">
										<input type="text" class="form-control" name="txt_title_{modules.edit.tab.LANG}"
											   id="txt_title_{modules.edit.tab.LANG}" value="{modules.edit.tab.TITLE_VALUE}"
											   placeholder="{modules.edit.tab.TITLE}">
										<div class="form-control-focus"> </div>
									</div>
								</div>
								<div class="form-group form-md-line-input">
									<label class="col-md-3 control-label" for="txt_seo_title_{modules.edit.tab.LANG}">{modules.edit.tab.SEO_TITLE}</label>
									<div class="col-md-8">
										<input type="text" class="form-control" name="txt_seo_title_{modules.edit.tab.LANG}"
											   id="txt_seo_title_{modules.edit.tab.LANG}" value="{modules.edit.tab.SEO_TITLE_VALUE}"
											   placeholder="{modules.edit.tab.SEO_TITLE}">
										<div class="form-control-focus"> </div>
									</div>
								</div>
								<div class="form-actions">
									<div class="row">
										<div class="col-md-offset-3 col-md-9">
											<button type="button" class="btn blue" id="btn_save" name="btn_save"
													onClick="doAction('update', {modules.edit.ID},  '');"><i class="fa fa-ok"></i> {modules.edit.SAVE}
											</button>
											<button type="reset" class="btn" id="btn_cancel" name="btn_cancel"
													onclick="click_cancel();">{modules.edit.CANCEL}</button>
										</div>
									</div>
								</div>
								</div>
								</div>
							</div>
							<!-- END tab -->
						</div>
                    </div>
                    <!-- END edit -->

                    <!-- BEGIN edit_language -->
					<div class="portlet-body form-body" id="panel_editbar">
						<div class="tabbable-line boxless tabbable-reversed">
							<ul class="nav nav-tabs">
								<!-- BEGIN tab -->
								<li{modules.edit_language.tab.CLASS}><a href="#content_{modules.edit_language.tab.LANG}" data-toggle="tab">{modules.edit_language.tab.LANG}</a></li>
								<!-- END tab -->
							</ul>
						</div>
						<div class="tab-content">
							<!-- BEGIN tab -->
							<div class="tab-pane {modules.edit_language.tab.FADE_CLASS}" id="content_{modules.edit_language.tab.LANG}">
								<div class="portlet box green">
								<div class="portlet-title">
									<div class="caption">
										<i class="fa fa-language"></i>{modules.edit_language.tab.LANG} 
									</div>
								</div>
								<div class="portlet-body form">
								<div class="form-group form-md-line-input">
									<label class="col-md-3 control-label" for="txt_lang_file_{modules.edit_language.tab.LANG}">{modules.edit_language.LANG_FILE}</label>
									<div class="col-md-8">
										<textarea class="form-control" name="txt_lang_file_{modules.edit_language.tab.LANG}"
												  id="txt_lang_file_{modules.edit_language.tab.LANG}" rows="25">{modules.edit_language.tab.LANG_FILE_VALUE}</textarea>
										<div class="form-control-focus"> </div>
									</div>
								</div>
								<div class="form-actions">
									<div class="row">
										<div class="col-md-offset-3 col-md-9">
											<button type="button" class="btn blue" id="btn_save" name="btn_save"
													onClick="doAction('language_update', {modules.edit_language.ID},  '');"><i class="fa fa-ok"></i> {modules.edit_language.SAVE}
											</button>
											<button type="reset" class="btn" id="btn_cancel" name="btn_cancel"
													onclick="click_cancel();">{modules.edit_language.CANCEL}</button>
										</div>
									</div>
								</div>
								</div>
								</div>
							</div>
							<!-- END tab -->
						</div>
					</div>
                    <!-- END edit_language -->

                    <!-- BEGIN edit_view -->
					<div class="portlet-body form-body">				
						<div class="portlet box green">
						<div class="portlet-body form">
						<div class="form-group form-md-line-input">
							<label class="col-md-3 control-label" for="txt_module_file">{modules.edit_view.MODULE_FILE}</label>
							<div class="col-md-8">
								<textarea class="form-control" name="txt_module_file"
										  id="txt_module_file" rows="25">{modules.edit_view.MODULE_FILE_VALUE}</textarea>
								<div class="form-control-focus"> </div>
							</div>
						</div>
						<div class="form-actions">
							<div class="row">
								<div class="col-md-offset-3 col-md-9">
									<button type="button" class="btn blue" id="btn_save" name="btn_save"
											onClick="doAction('view_module_update', {modules.edit_view.ID},  '');"><i class="fa fa-ok"></i> {modules.edit_view.SAVE}
									</button>
									<button type="reset" class="btn" id="btn_cancel" name="btn_cancel"
											onclick="click_cancel();">{modules.edit_view.CANCEL}</button>
								</div>
							</div>
						</div>
						</div>
						</div>
					</div>
                    <!-- END edit_view -->

                    <!-- BEGIN edit_admin -->
					<div class="portlet-body form-body">				
						<div class="portlet box green">
						<div class="portlet-body form">
						<div class="form-group form-md-line-input">
							<label class="col-md-3 control-label" for="txt_module_file">{modules.edit_admin.MODULE_FILE}</label>
							<div class="col-md-8">
								<textarea class="form-control" name="txt_module_file"
										  id="txt_module_file" rows="25">{modules.edit_admin.MODULE_FILE_VALUE}</textarea>
								<div class="form-control-focus"> </div>
							</div>
						</div>
						<div class="form-actions">
							<div class="row">
								<div class="col-md-offset-3 col-md-9">
									<button type="button" class="btn blue" id="btn_save" name="btn_save"
											onClick="doAction('admin_module_update', {modules.edit_admin.ID},  '');"><i class="fa fa-ok"></i> {modules.edit_admin.SAVE}
									</button>
									<button type="reset" class="btn" id="btn_cancel" name="btn_cancel"
											onclick="click_cancel();">{modules.edit_admin.CANCEL}</button>
								</div>
							</div>
						</div>
						</div>
						</div>
					</div>
                    <!-- END edit_admin -->

                    <!-- BEGIN edit_view_template -->
					<div class="portlet-body form-body" id="panel_editbar">
						<div class="tabbable-line boxless tabbable-reversed">
							<ul class="nav nav-tabs">
								<!-- BEGIN tab -->
								<li{modules.edit_view_template.tab.CLASS}><a href="#content_{modules.edit_view_template.tab.NDX}" data-toggle="tab">{modules.edit_view_template.tab.LANG}</a></li>
								<!-- END tab -->
							</ul>
						</div>
						<div class="tab-content">
							<!-- BEGIN tab -->
							<div class="tab-pane {modules.edit_view_template.tab.FADE_CLASS}" id="content_{modules.edit_view_template.tab.NDX}">
								<div class="portlet box green">
								<div class="portlet-title">
									<div class="caption">
										<i class="fa fa-language"></i>{modules.edit_view_template.tab.LANG} 
									</div>
								</div>
								<div class="portlet-body form">
								<div class="form-group form-md-line-input">
									<label class="col-md-3 control-label" for="txt_file_{modules.edit_view_template.tab.NDX}">{modules.edit_view_template.MODULE_FILE}</label>
									<div class="col-md-8">
										<textarea class="form-control code_editor_html" name="txt_file_{modules.edit_view_template.tab.NDX}"
												  id="txt_file_{modules.edit_view_template.tab.NDX}" rows="25">{modules.edit_view_template.tab.FILE_VALUE}</textarea>
										<div class="form-control-focus"> </div>
									</div>
								</div>
								<div class="form-actions">
									<div class="row">
										<div class="col-md-offset-3 col-md-9">
											<button type="button" class="btn blue" id="btn_save" name="btn_save"
													onClick="doAction('view_template_update', {modules.edit_view_template.ID},  '');"><i class="fa fa-ok"></i> {modules.edit_view_template.SAVE}
											</button>
											<button type="reset" class="btn" id="btn_cancel" name="btn_cancel"
													onclick="click_cancel();">{modules.edit_view_template.CANCEL}</button>
										</div>
									</div>
								</div>
								</div>
								</div>
							</div>
							<!-- END tab -->
						</div>
					</div>
                    <!-- END edit_view_template -->

                    <!-- BEGIN edit_admin_template -->
					<div class="portlet-body form-body" id="panel_editbar">
						<div class="tabbable-line boxless tabbable-reversed">
							<ul class="nav nav-tabs">
								<!-- BEGIN tab -->
								<li{modules.edit_admin_template.tab.CLASS}><a href="#content_{modules.edit_admin_template.tab.NDX}" data-toggle="tab">{modules.edit_admin_template.tab.LANG}</a></li>
								<!-- END tab -->
							</ul>
						</div>
						<div class="tab-content">
							<!-- BEGIN tab -->
							<div class="tab-pane {modules.edit_admin_template.tab.FADE_CLASS}" id="content_{modules.edit_admin_template.tab.NDX}">
								<div class="portlet box green">
								<div class="portlet-title">
									<div class="caption">
										<i class="fa fa-language"></i>{modules.edit_admin_template.tab.LANG} 
									</div>
								</div>
								<div class="portlet-body form">
								<div class="form-group form-md-line-input">
									<label class="col-md-3 control-label" for="txt_file_{modules.edit_admin_template.tab.NDX}">{modules.edit_admin_template.MODULE_FILE}</label>
									<div class="col-md-8">
										<textarea class="form-control" name="txt_file_{modules.edit_admin_template.tab.NDX}"
												  id="txt_file_{modules.edit_admin_template.tab.NDX}" rows="25">{modules.edit_admin_template.tab.FILE_VALUE}</textarea>
										<div class="form-control-focus"> </div>
									</div>
								</div>
								<div class="form-actions">
									<div class="row">
										<div class="col-md-offset-3 col-md-9">
											<button type="button" class="btn blue" id="btn_save" name="btn_save"
													onClick="doAction('admin_template_update', {modules.edit_admin_template.ID},  '');"><i class="fa fa-ok"></i> {modules.edit_admin_template.SAVE}
											</button>
											<button type="reset" class="btn" id="btn_cancel" name="btn_cancel"
													onclick="click_cancel();">{modules.edit_admin_template.CANCEL}</button>
										</div>
									</div>
								</div>
								</div>
								</div>
							</div>
							<!-- END tab -->
						</div>
					</div>
                    <!-- END edit_admin_template -->

                    <!-- BEGIN edit_config -->
					<div class="portlet-body form-body" id="panel_editbar">
						<div class="tabbable-line boxless tabbable-reversed">
							<ul class="nav nav-tabs">
								<!-- BEGIN tab -->
								<li{modules.edit_config.tab.CLASS}><a href="#content_{modules.edit_config.tab.NDX}" data-toggle="tab">{modules.edit_config.tab.LANG}</a></li>
								<!-- END tab -->
							</ul>
						</div>
						<div class="tab-content">
							<!-- BEGIN tab -->
							<div class="tab-pane {modules.edit_config.tab.FADE_CLASS}" id="content_{modules.edit_config.tab.NDX}">
								<div class="portlet box green">
								<div class="portlet-title">
									<div class="caption">
										<i class="fa fa-language"></i>{modules.edit_config.tab.LANG} 
									</div>
								</div>
								<div class="portlet-body form">
								<div class="form-group form-md-line-input">
									<label class="col-md-3 control-label" for="txt_file_{modules.edit_config.tab.NDX}">{modules.edit_config.MODULE_FILE}</label>
									<div class="col-md-8">
										<textarea class="form-control" name="txt_file_{modules.edit_config.tab.NDX}"
												  id="txt_file_{modules.edit_config.tab.NDX}" rows="25">{modules.edit_config.tab.FILE_VALUE}</textarea>
										<div class="form-control-focus"> </div>
									</div>
								</div>
								<div class="form-actions">
									<div class="row">
										<div class="col-md-offset-3 col-md-9">
											<button type="button" class="btn blue" id="btn_save" name="btn_save"
													onClick="doAction('config_update', {modules.edit_config.ID},  '');"><i class="fa fa-ok"></i> {modules.edit_config.SAVE}
											</button>
											<button type="reset" class="btn" id="btn_cancel" name="btn_cancel"
													onclick="click_cancel();">{modules.edit_config.CANCEL}</button>
										</div>
									</div>
								</div>
								</div>
								</div>
							</div>
							<!-- END tab -->
						</div>
					</div>
                    <!-- END edit_config -->

                    <!-- BEGIN edit_seo -->
					<div class="portlet-body form-body" id="panel_editbar">
						<div class="form-group form-md-line-input">
							<label class="col-md-3 control-label" for="txt_seo_table">{modules.edit_seo.SEO_TABLE}</label>
							<div class="col-md-6">
								<select class="form-control" name="txt_seo_table" id="txt_seo_table">
									<option value="" >&gt;&gt;{modules.edit_seo.SEO_TABLE}&lt;&lt;</option>
									<!-- BEGIN select_seo_table -->
									<option value="{modules.edit_seo.select_seo_table.VALUE}" {modules.edit_seo.select_seo_table.SELECTED}>{modules.edit_seo.select_seo_table.TEXT}</option>
									<!-- END select_seo_table -->
								</select>
								<div class="form-control-focus"> </div>
							</div>
						</div>
						<div class="form-group form-md-line-input">
							<label class="col-md-3 control-label" for="txt_table_id">{modules.edit_seo.TABLE_ID}</label>
							<div class="col-md-6">
								<select class="form-control" name="txt_table_id" id="txt_table_id">
									<option value="" >&gt;&gt;{modules.edit_seo.TABLE_ID}&lt;&lt;</option>
									<!-- BEGIN select_table_id -->
									<option value="{modules.edit_seo.select_table_id.VALUE}" {modules.edit_seo.select_table_id.SELECTED}>{modules.edit_seo.select_table_id.TEXT}</option>
									<!-- END select_table_id -->
								</select>
								<div class="form-control-focus"> </div>
							</div>
						</div>
						<div class="form-group form-md-line-input">
							<label class="col-md-3 control-label" for="txt_title_header">{modules.edit_seo.TITLE_HEADER}</label>
							<div class="col-md-6">
								<select class="form-control" name="txt_title_header" id="txt_title_header">
									<option value="" >&gt;&gt;{modules.edit_seo.TITLE_HEADER}&lt;&lt;</option>
									<!-- BEGIN select_title_header -->
									<option value="{modules.edit_seo.select_title_header.VALUE}" {modules.edit_seo.select_title_header.SELECTED}>{modules.edit_seo.select_title_header.TEXT}</option>
									<!-- END select_title_header -->
								</select>
								<div class="form-control-focus"> </div>
							</div>
						</div>
						<div class="form-group form-md-line-input">
							<label class="col-md-3 control-label" for="txt_title_description">{modules.edit_seo.TITLE_DESCRIPTION}</label>
							<div class="col-md-6">
								<select class="form-control" name="txt_title_description" id="txt_title_description">
									<option value="" >&gt;&gt;{modules.edit_seo.TITLE_DESCRIPTION}&lt;&lt;</option>
									<!-- BEGIN select_title_description -->
									<option value="{modules.edit_seo.select_title_description.VALUE}" {modules.edit_seo.select_title_description.SELECTED}>{modules.edit_seo.select_title_description.TEXT}</option>
									<!-- END select_title_description -->
								</select>
								<div class="form-control-focus"> </div>
							</div>
						</div>
						<div class="form-group form-md-line-input">
							<label class="col-md-3 control-label" for="txt_title_keywords">{modules.edit_seo.TITLE_KEYWORDS}</label>
							<div class="col-md-6">
								<select class="form-control" name="txt_title_keywords" id="txt_title_keywords">
									<option value="" >&gt;&gt;{modules.edit_seo.TITLE_KEYWORDS}&lt;&lt;</option>
									<!-- BEGIN select_title_keywords -->
									<option value="{modules.edit_seo.select_title_keywords.VALUE}" {modules.edit_seo.select_title_keywords.SELECTED}>{modules.edit_seo.select_title_keywords.TEXT}</option>
									<!-- END select_title_keywords -->
								</select>
								<div class="form-control-focus"> </div>
							</div>
						</div>
						<div class="form-actions">
							<div class="row">
								<div class="col-md-offset-3 col-md-9">
									<button type="button" class="btn blue" id="btn_save" name="btn_save"
											onClick="doAction('seo_update', {modules.edit_seo.ID},  '');"><i class="fa fa-ok"></i> {modules.edit_seo.SAVE}
									</button>
									<button type="reset" class="btn" id="btn_cancel" name="btn_cancel"
											onclick="click_cancel();">{modules.edit_seo.CANCEL}</button>
								</div>
							</div>
						</div>
					</div>
					<script>
					$(document).ready(function () {
						var tbls = [];
						<!-- BEGIN tables -->
						tbls.push("{modules.edit_seo.tables.NAME}");
						var tbl_{modules.edit_seo.tables.NAME} = [];
						<!-- BEGIN columns -->
						tbl_{modules.edit_seo.tables.NAME}.push("{modules.edit_seo.tables.columns.VALUE}");
						<!-- END columns -->
						
						<!-- END tables -->
						$("#txt_seo_table").change(function(){
							//alert(this.value);
							$('#txt_table_id').empty().append($("<option></option>")
												.attr("value", "")
												.text(">>{modules.edit_seo.TABLE_ID}<<"));
							$('#txt_title_header').empty().append($("<option></option>")
												.attr("value", "")
												.text(">>{modules.edit_seo.TITLE_HEADER}<<"));
							$('#txt_title_description').empty().append($("<option></option>")
												.attr("value", "")
												.text(">>{modules.edit_seo.TITLE_DESCRIPTION}<<"));
							$('#txt_title_keywords').empty().append($("<option></option>")
												.attr("value", "")
												.text(">>{modules.edit_seo.TITLE_KEYWORDS}<<"));
							if (this.value!="") {
							var selectValues = eval("tbl_"+this.value);
							
							//table_id
							$.each(selectValues, function(key, value) {
								 $('#txt_table_id')
									 .append($("<option></option>")
												.attr("value",value)
												.text(value)); 
							});
							
							//title_header
							$.each(selectValues, function(key, value) {
								 $('#txt_title_header')
									 .append($("<option></option>")
												.attr("value",value)
												.text(value)); 
							});
							
							//title_description
							$.each(selectValues, function(key, value) {
								 $('#txt_title_description')
									 .append($("<option></option>")
												.attr("value",value)
												.text(value)); 
							});
							
							//title_keywords
							$.each(selectValues, function(key, value) {
								 $('#txt_title_keywords')
									 .append($("<option></option>")
												.attr("value",value)
												.text(value)); 
							});
							}
							
						});
					});
					</script>
                    <!-- END edit_seo -->

                </form>
					</div>
					</div>
				</div>
                <!-- END modules -->

				
			</div>
			<!-- END::CONTENT BODY -->
		</div>
		<!-- END::CONTENT -->
	</div>
	<!-- END::CONTAINER -->
	
	<!-- BEGIN::PAGE LEVEL PLUGINS -->
	<script src="{ROOT}assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>
	<!-- END::PAGE LEVEL PLUGINS -->
	
	<!-- BEGIN modules -->
	<!-- BEGIN list -->
	<script src="{ROOT}assets/global/scripts/dt/jquery.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/scripts/dt/jquery-ui.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/scripts/datatable.js" type="text/javascript"></script>
	
	<!-- <script src="{ROOT}assets/global/scripts/dt/jquery.dataTables.js" type="text/javascript"></script> -->
	<script src="{ROOT}assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/scripts/dt/jquery.dataTables.rowReordering.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/scripts/dt/jquery.dataTables.rowGrouping.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>

	<script type="text/javascript" charset="utf-8">
		$(document).ready(function () {
			
			var sortInput = jQuery('#sort_order');
			
			var oTable= $("#modules_list").dataTable({
				"bJQueryUI": true,
				"bDestroy": false,
				"bProcessing": false,
				"bSortable": false,
			});
		
			oTable.rowReordering({
				sURL : 'modules_drag.php', 
				fnAlert: function(message) {
					//alert("order"); 
				}
			});
							//.clear().destroy()
			$('.group-checkable').click(function(){
				//alert($('.group-checkable').is(":checked"));
				var ch = $('.group-checkable').is(":checked");
				$('.checkboxes').prop('checked', ch);
			});
		});
	</script>
	<!-- END list -->
	<!-- END modules -->
	
	<!-- BEGIN::THEME GLOBAL SCRIPTS -->
	<script src="{ROOT}assets/global/plugins/codemirror/lib/codemirror.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/plugins/codemirror/mode/javascript/javascript.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/plugins/codemirror/mode/htmlmixed/htmlmixed.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/plugins/codemirror/mode/htmlembedded/htmlembedded.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/plugins/codemirror/mode/php/php.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/plugins/codemirror/mode/css/css.js" type="text/javascript"></script>
	
	<!-- END::THEME GLOBAL SCRIPTS -->
	<!-- BEGIN::PAGE LEVEL SCRIPTS -->
	<script src="{ROOT}assets/pages/scripts/components-bootstrap-select.min.js" type="text/javascript"></script>
	<script src="{ROOT}assets/pages/scripts/components-code-editors.js" type="text/javascript"></script>    	
	<!-- END::PAGE LEVEL SCRIPTS -->
	
	<script src="{ROOT}assets/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>