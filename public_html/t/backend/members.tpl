
	<script type="text/javascript">
	function clickAc(act) {
		//alert(act);
		var set = $(".checkboxes");
		var fields = '';
		$(set).each(function () {
			var checked = $(this).is(":checked");
			if (checked) {
				var v = $(this).val();
				fields += (fields != '') ? ',' : '';
				fields += v;
				//alert("aaa:"+v);
			}
		});
		if (fields != '')
			window.location.href = '?{MODULE_QS}=manage_users&userid=' + act + ':' + fields;
	}
	function doDelete(id) {
		if (confirm('{DELETE_CONFIRM}')) {
			document.getElementById("action").value = 'delete';
			document.getElementById('action_id').value = id;
			document.getElementById('action_submit').click();
		}
	}

	function doAction(act, id, mess) {
		document.getElementById("action_id").value = id;
		document.getElementById("action").value = act;

		if (act == 'delete') {
			if (confirm(mess)) {
				document.getElementById("action_submit").click();
			}
		}
		else {
			document.getElementById("action_submit").click();
		}
	}

	function redirect(url) {
		window.location.href = url;
	}

	function click_cancel() {
		window.location.href = '?{MODULE_QS}=manage_users';
	}
	function click_members_cancel() {
		window.location.href = '?{MODULE_QS}=members';
	}
	</script>
	
		<!-- BEGIN::CONTENT -->
		<div class="page-content-wrapper" style="min-width:950px !important;">
			<!-- BEGIN::CONTENT BODY -->
			<div class="page-content">
				<!-- BEGIN::PAGE HEADER-->
				<!-- BEGIN::PAGE BAR -->
				<div class="page-bar">
					<ul class="page-breadcrumb">
						<li>
							<a href="index.php">{HOME}</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<span><a href="index.php?{MODULE_QS}=manage_users">{TITLE}</a></span>
						</li>
					</ul>
				</div>
				<!-- END::PAGE BAR -->
				<!-- BEGIN::PAGE TITLE-->
				<h3 class="page-title"> {TITLE} </h3>
				<!-- END::PAGE TITLE-->
				<!-- END::PAGE HEADER-->				
				
				<!-- BEGIN users -->
				<div class="row">
					<div class="col-md-12">
				<form name="form_action" id="form_action" method="post" class="form-horizontal" action="">
					<input type="hidden" id="action" name="action" value="0">
					<input type="hidden" id="action_id" name="action_id" value="0">
					<input type="submit" id="action_submit" name="action_submit" style="display:none;">

					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-dark">
								<i class="fa fa-user"></i>
								<span class="caption-subject bold uppercase">{TITLE}</span>
							</div>
						</div>
					
					<!-- BEGIN list -->
					
					
					
					<!-- BEGIN search -->
					<div class="portlet-body" id="panel_editbar">
					
					<div class="row">
  
  				<div class="col-sm-4">

					<div class="form-group form-md-line-input">
							<label class="col-md-3 control-label" for="txt_username">User name</label>
							<div class="col-md-8">
								<input type="text" class="form-control" name="txt_username" title="User name" 
									   id="txt_username" value="{users.list.search.USERNAME_VALUE}"
									   placeholder="User name" autocomplete="off">
								<div class="form-control-focus"> </div>
							</div>
					</div>
										
				</div>

  				<div class="col-sm-4">

					<div class="form-group form-md-line-input">
							<label class="col-md-3 control-label" for="txt_email">Email</label>
							<div class="col-md-8">
								<input type="text" class="form-control" name="txt_email" title="Email" 
									   id="txt_email" value="{list.search.EMAIL_VALUE}"
									   placeholder="Email" autocomplete="off">
								<div class="form-control-focus"> </div>
							</div>
					</div>
										
				</div>

				<div class="col-sm-4">

					<div class="form-group form-md-line-input">
							<label class="col-md-3 control-label" for="txt_phone">Phone</label>
							<div class="col-md-8">
								<input type="text" class="form-control" name="txt_phone" title="User name" 
									   id="txt_phone" value="{list.search.PHONE_VALUE}"
									   placeholder="Phone" autocomplete="off">
								<div class="form-control-focus"> </div>
							</div>
					</div>
										
				</div>




</div>


						<input type="hidden" name="search_action" value="1">
						
					<div class="col-sm-4"></div>
					<div class="col-sm-4"></div>
					<div class="col-sm-4">
						<div class="form-actions">
							<div class="row">
								<div class="col-md-offset-3 col-md-9">
									<button type="button" class="btn blue" id="btn_save" name="btn_save"
											onclick="doAction('search', 0,  '');"><i class="fa fa-search"></i> Axtar
									</button>
								</div>
							</div>
						</div>
					</div>	
					</div>
					<!-- END search -->
					
					<div class="portlet-body">
						<!--<div class="clearfix">
							<div class="btn-group">
								<a href="{users.list.URL}&userid=-1" class="btn green">
									{users.list.ADD} &nbsp; <i class="fa fa-plus"></i>
								</a>
							</div>
						</div> -->
						<br/>
						
						<table class="table table-striped table-bordered table-hover" id="list_users">
							<thead>
							<tr>
								<th style="width:25px;"><input type="checkbox" class="group-checkable"
															   data-set="#list_users .checkboxes"/></th>
								<th>{users.list.USERNAME}</th>
								<th style="width : 120px !important;">{users.list.PHONE}</th>
								<th style="width : 150px !important;">{users.list.EMAIL}</th>
								<th style="width : 100px !important;">{users.list.CREATED}</th>
								<th style="width : 60px !important;">{users.list.EDIT}</th>
								<th style="width : 60px !important;">{users.list.DELETE}</th>
								<th style="width : 90px !important;">{users.list.MEDIA}</th>
								<th style="width : 15% !important;"></th>
							</tr>
							</thead>
							<tbody>
							<!-- BEGIN items -->
							<tr class="odd gradeX">
								<td id="chk_{users.list.items.ID}">
									<!-- BEGIN check -->
									<input type="checkbox" class="checkboxes" value="{users.list.items.ID}"/>
									<!-- END check -->
								</td>
								<td>{users.list.items.USERNAME}</td>
								<td>{users.list.items.PHONE}</td>
								<td>{users.list.items.EMAIL}</td>
								<td>{users.list.items.CREATED}</td>
								<td><a href="{users.list.URL}&userid={users.list.items.ID}" class="btn yellow">{users.list.EDIT}&nbsp; <i class="fa fa-pencil"></i></a></td>
								<td>
									<!-- BEGIN do_del -->
									<a href="JavaScript:doDelete({users.list.items.ID})" title="{users.list.DELETE}"
									    class="btn red ask"> {users.list.DELETE}&nbsp; <i class="fa fa-trash"></i></a></td>
									<!-- END do_del -->
								<td>
									<a href="{users.list.URL}&perid={users.list.items.ID}&mod=permissions" class="btn btn-default">
										{users.list.MEDIA}
									&nbsp; <i class="fa fa-camera"></i></a>
								</td>
								<td>
									<!-- BEGIN active -->
									<a href="{users.list.items.ACTIVE_URL}" class="btn btn-success">
										{users.list.ACTIVE}&nbsp; <i class="fa fa-unlock"></i></a>
									<!-- END active -->
									<!-- BEGIN inactive -->
									<a href="{users.list.items.INACTIVE_URL}" class="btn btn-danger">
										{users.list.INACTIVE}&nbsp; <i class="fa fa-lock"></i></a>
									<!-- END inactive -->
									<a href="{users.list.URL}&perid={users.list.items.ID}&mod=changepassword" class="btn btn-default">
										Şifrə
									&nbsp; <i class="fa fa-key"></i></a>
								</td>
							</tr>
							<!-- END items -->
							</tbody>
						</table>
						<br/>
						<table>
							<tfoot>
							<tr>
								<td class="ac"><span class="label label-success"
													 onclick="clickAc('activate')">{users.list.ACTIVATE}</span></td>
								<td style="width:10px;"></td>
								<td class="ac"><span class="label label-danger"
													 onclick="clickAc('inactivate')">{users.list.INACTIVATE}</span></td>
								<td></td>
							</tr>
							</tfoot>
						</table>
					</div>
					<!-- END list -->

					<!-- BEGIN edit -->
					<div class="portlet-body" id="panel_editbar">
						<input type="hidden" name="txt_id" id="txt_id" value="{users.edit.ID}">
						
						<div class="form-group form-md-line-input">
							<label class="col-md-3 control-label" for="txt_username">{users.edit.USERNAME}</label>
							<div class="col-md-8">
								<input type="text" class="form-control" name="txt_username" pattern="[a-z0-9]{3,}" title="{USERNAME}" 
									   id="txt_username" value="{users.edit.USERNAME_VALUE}"
									   placeholder="{users.edit.USERNAME}" autocomplete="off">
								<div class="form-control-focus"> </div>
							</div>
						</div>
						<div class="form-group form-md-line-input">
							<label class="col-md-3 control-label" for="txt_password">{users.edit.PASSWORD}</label>
							<div class="col-md-8">
								<input type="password" class="form-control" name="txt_password"
									   id="txt_password" value="{users.edit.PASSWORD_VALUE}"
									   placeholder="{users.edit.PASSWORD}" autocomplete="off">
								<div class="form-control-focus"> </div>
							</div>
						</div>
						<div class="form-group form-md-line-input">
							<label class="col-md-3 control-label" for="txt_usertype">{users.edit.USERTYPE}</label>
							<div class="col-md-6">
								<select class="form-control" name="txt_usertype" id="txt_usertype">
									{users.edit.USERTYPE_OPTIONS}
								</select>
								<div class="form-control-focus"> </div>
							</div>
						</div>
						<div class="form-group form-md-line-input">
							<label class="col-md-3 control-label" for="txt_name">{users.edit.NAME}</label>
							<div class="col-md-8">
								<input type="text" class="form-control" name="txt_name"
									   id="txt_name" value="{users.edit.NAME_VALUE}"
									   placeholder="{users.edit.NAME}" autocomplete="off">
								<div class="form-control-focus"> </div>
							</div>
						</div>
						<div class="form-group form-md-line-input">
							<label class="col-md-3 control-label" for="txt_email">{users.edit.EMAIL}</label>
							<div class="col-md-8">
								<input type="text" class="form-control" name="txt_email"
									   id="txt_email" value="{users.edit.EMAIL_VALUE}"
									   placeholder="{users.edit.EMAIL}" autocomplete="off">
								<div class="form-control-focus"> </div>
							</div>
						</div>
						
						<!-- BEGIN alert -->
						<div class="form-actions" style="color:#f00;">
							{users.edit.alert.MESSAGE}
						</div>
						<!-- END alert -->
						<div class="form-actions">
							<div class="row">
								<div class="col-md-offset-3 col-md-9">
									<button type="button" class="btn blue" id="btn_save" name="btn_save"
											onclick="doAction('save', {users.edit.ID},  '');"><i class="fa fa-ok"></i> {users.edit.SAVE}
									</button>
									<button type="reset" class="btn" id="btn_cancel" name="btn_cancel"
											onclick="click_cancel();">{users.edit.CANCEL}</button>
								</div>
							</div>
						</div>
					</div>
					<!-- END edit -->
					
					
					
					<!-- BEGIN changepassword -->
					<div class="portlet-body" id="panel_editbar">
						<input type="hidden" name="txt_id" id="txt_id" value="{users.changepassword.ID}">
						
				
						<div class="form-group form-md-line-input">
							<label class="col-md-3 control-label" for="txt_password">{users.changepassword.PASSWORD}</label>
							<div class="col-md-8">
								<input type="password" class="form-control" name="txt_password"
									   id="txt_password" value="{users.changepassword.PASSWORD_VALUE}"
									   placeholder="{users.changepassword.PASSWORD}" autocomplete="off">
								<div class="form-control-focus"> </div>
							</div>
						</div>
					
				
						
						<!-- BEGIN alert -->
						<div class="form-actions" style="color:#f00;">
							{users.changepassword.alert.MESSAGE}
						</div>
						<!-- END alert -->
						<div class="form-actions">
							<div class="row">
								<div class="col-md-offset-3 col-md-9">
									<button type="button" class="btn blue" id="btn_save" name="btn_save"
											onclick="doAction('savechangepassword', {users.changepassword.ID},  '');"><i class="fa fa-ok"></i> {users.changepassword.SAVE}
									</button>
									<button type="reset" class="btn" id="btn_cancel" name="btn_cancel"
											onclick="click_members_cancel();">{users.changepassword.CANCEL}</button>
								</div>
							</div>
						</div>
					</div>
					<!-- END changepassword -->

					<!-- BEGIN permissions -->
					<div class="portlet box light-grey">
						<div class="portlet-title" style="margin-bottom:25px;">
							<h4><i class="fa fa-user"></i>{users.permissions.PERMISSIONS}</h4>
						</div>
						<div style="margin-left:10px;margin-top:-10px;">
							<h4><strong>{users.permissions.USERNAME}:</strong> {users.permissions.USERNAME_VALUE}</h4>
						</div>
						<div class="portlet-body">
							<input type="hidden" name="txt_id" id="txt_id" value="{users.permissions.ID}">
							<table class="table table-striped table-bordered table-hover">
								<thead>
								<tr>
									<th>{users.permissions.MODULE}<br/><br/></th>
									<th style="width:90px;" class="text-center">
										<label>
										<strong>{users.permissions.RIGHT_FULL}</strong><br/>
										<input type="checkbox" class="right-full-checkable" data-set=".right-full"/>
										</label>
									</th>
									<th style="width:75px;" class="text-center">
										<label>
										<strong>{users.permissions.RIGHT_DEL}</strong><br/>
										<input type="checkbox" class="right-del-checkable" data-set=".right-del"/>
										</label>
									</th>
									<th style="width:75px;" class="text-center">
										<label>
										<strong>{users.permissions.RIGHT_EDIT}</strong><br/>
										<input type="checkbox" class="right-edit-checkable" data-set=".right-edit"/>
										</label>
									</th>
									<th style="width:75px;" class="text-center">
										<label>
										<strong>{users.permissions.RIGHT_ADD}</strong><br/>
										<input type="checkbox" class="right-add-checkable" data-set=".right-add"/>
										</label>
									</th>
									<th style="width:75px;" class="text-center">
										<label>
										<strong>{users.permissions.RIGHT_VIEW}</strong><br/>
										<input type="checkbox" class="right-view-checkable" data-set=".right-view"/>
										</label>
									</th>
								</tr>
								</thead>
								<tbody>
								<!-- BEGIN items -->
								<tr class="odd gradeX">
									<td>
										<label for="txt_full_right_{users.permissions.items.ID}">{users.permissions.items.TITLE}</label>
									</td>
									<td style="text-align:center;" title="{users.permissions.items.TITLE} : {users.permissions.RIGHT_FULL}"><input type="checkbox"
																		  name="txt_full_right_{users.permissions.items.ID}"
																		  id="txt_full_right_{users.permissions.items.ID}"
																		  class="right-full checkboxes" data-value="{users.permissions.items.ID}"
																		  value="1" {users.permissions.items.CHECKED_FULL} />
									</td>
									<td style="text-align:center;" title="{users.permissions.items.TITLE} : {users.permissions.RIGHT_DEL}"><input type="checkbox"
																		  name="txt_del_right_{users.permissions.items.ID}"
																		  id="txt_del_right_{users.permissions.items.ID}"
																		  class="right-del checkboxes" data-value="{users.permissions.items.ID}"
																		  value="1" {users.permissions.items.CHECKED_DEL} />
									</td>
									<td style="text-align:center;" title="{users.permissions.items.TITLE} : {users.permissions.RIGHT_EDIT}"><input type="checkbox"
																		  name="txt_edit_right_{users.permissions.items.ID}"
																		  id="txt_edit_right_{users.permissions.items.ID}"
																		  class="right-edit checkboxes" data-value="{users.permissions.items.ID}"
																		  value="1" {users.permissions.items.CHECKED_EDIT} />
									</td>
									<td style="text-align:center;" title="{users.permissions.items.TITLE} : {users.permissions.RIGHT_ADD}"><input type="checkbox"
																		  name="txt_add_right_{users.permissions.items.ID}"
																		  id="txt_add_right_{users.permissions.items.ID}"
																		  class="right-add checkboxes" data-value="{users.permissions.items.ID}"
																		  value="1" {users.permissions.items.CHECKED_ADD} />
									</td>
									<td style="text-align:center;" title="{users.permissions.items.TITLE} : {users.permissions.RIGHT_VIEW}"><input type="checkbox"
																		  name="txt_view_right_{users.permissions.items.ID}"
																		  id="txt_view_right_{users.permissions.items.ID}"
																		  class="right-view checkboxes" data-value="{users.permissions.items.ID}"
																		  value="1" {users.permissions.items.CHECKED_VIEW} />
									</td>
								</tr>
								<!-- END items -->
								<!-- BEGIN alert -->
								<div class="form-actions" style="color:#f00;">
									{users.permissions.alert.MESSAGE}
								</div>
								<!-- END alert -->
								<tr class="odd gradeX">
									<td style="text-align:center;" colspan="6">
										<button type="button" class="btn blue" id="btn_save" name="btn_save"
												onClick="doAction('save_per', {users.permissions.ID},  '');"><i
													class="fa fa-ok"></i> {users.permissions.SAVE}</button>
										&nbsp;
										<button type="reset" class="btn" id="btn_cancel" name="btn_cancel"
												onclick="click_cancel();">{users.permissions.CANCEL}</button>
									</td>
								</tr>
								</tbody>
							</table>
						</div>
					</div>
					<!-- END permissions -->

				</form>
					</div>
				</div>
				<!-- END users -->
				
			</div>
			<!-- END::CONTENT BODY -->
		</div>
		<!-- END::CONTENT -->
	</div>
	<!-- END::CONTAINER -->
	
	<script src="{ROOT}assets/global/plugins/jquery.min.js" type="text/javascript"></script>

	<script type="text/javascript">
		$(document).ready(function () {
		
			$('.group-checkable').click(function(){
				//alert($('.group-checkable').is(":checked"));
				var ch = $('.group-checkable').is(":checked");
				var set = $(".checkboxes");
				$(set).each(function () {
					$(this).prop('checked', ch);
					var v = $(this).val();
					if (ch) {
						$("#chk_" + v).find('span').addClass('checked');
					} else {
						$("#chk_" + v).find('span').removeClass('checked');
					}
				});
			});
			
			$('.right-full-checkable').click(function(){
				//alert($('.right-full-checkable').is(":checked"));
				var ch = $('.right-full-checkable').is(":checked");
				var set = $(".right-full");
				$(set).each(function () {
					$(this).prop('checked', ch);
					var v = $(this).data("value");
					if (ch) {
						$("#uniform-txt_full_right_" + v).find('span').addClass('checked');
					} else {
						$("#uniform-txt_full_right_" + v).find('span').removeClass('checked');
					}
				});
			});
			
			$('.right-del-checkable').click(function(){
				var ch = $('.right-del-checkable').is(":checked");
				var set = $(".right-del");
				$(set).each(function () {
					$(this).prop('checked', ch);
					var v = $(this).data("value");
					if (ch) {
						$("#uniform-txt_del_right_" + v).find('span').addClass('checked');
					} else {
						$("#uniform-txt_del_right_" + v).find('span').removeClass('checked');
					}
				});
			});
			
			$('.right-edit-checkable').click(function(){
				var ch = $('.right-edit-checkable').is(":checked");
				var set = $(".right-edit");
				$(set).each(function () {
					$(this).prop('checked', ch);
					var v = $(this).data("value");
					if (ch) {
						$("#uniform-txt_edit_right_" + v).find('span').addClass('checked');
					} else {
						$("#uniform-txt_edit_right_" + v).find('span').removeClass('checked');
					}
				});
			});
			
			$('.right-add-checkable').click(function(){
				var ch = $('.right-add-checkable').is(":checked");
				var set = $(".right-add");
				$(set).each(function () {
					$(this).prop('checked', ch);
					var v = $(this).data("value");
					if (ch) {
						$("#uniform-txt_add_right_" + v).find('span').addClass('checked');
					} else {
						$("#uniform-txt_add_right_" + v).find('span').removeClass('checked');
					}
				});
			});
			
			$('.right-view-checkable').click(function(){
				var ch = $('.right-view-checkable').is(":checked");
				var set = $(".right-view");
				$(set).each(function () {
					$(this).prop('checked', ch);
					var v = $(this).data("value");
					if (ch) {
						$("#uniform-txt_view_right_" + v).find('span').addClass('checked');
					} else {
						$("#uniform-txt_view_right_" + v).find('span').removeClass('checked');
					}
				});
			});
		});
	</script>