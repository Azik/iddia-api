<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<head>
	<meta content="noindex, nofollow" name="robots"/>
	<meta charset="utf-8" />
	<title>{TITLE}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1" name="viewport" />
	<meta content="MicroPHP" name="author"/>
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
	<link href="{ROOT}assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
	<link href="{ROOT}assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
	<link href="{ROOT}assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<link href="{ROOT}assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
	<link href="{ROOT}assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
	<link href="{ROOT}assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
	<link href="{ROOT}assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
	<link href="{ROOT}assets/global/css/components-md.min.css" rel="stylesheet" id="style_components" type="text/css" />
	<link href="{ROOT}assets/global/css/plugins-md.min.css" rel="stylesheet" type="text/css" />
	<link href="{ROOT}assets/pages/css/login-5.css" rel="stylesheet" type="text/css" />
	<link rel="shortcut icon" href="favicon.ico" />
</head>
<body class="login">
	<div class="user-login-5">
		<div class="row bs-reset">
			<div class="col-md-6 login-container bs-reset">
				<img class="login-logo login-6" src="{ROOT}img/login-invert.png" />
				<div class="login-content">
					<h1>{TITLE}</h1>
					<p> </p>
					<form action="index.php?module=login" class="login-form" method="post">
						<input type="hidden" name="teiken" value="{TOKEN}">
						<div class="row">
							<div class="col-xs-6">
								<input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="{USERNAME}" pattern="[a-z0-9]{3,}" title="{USERNAME}" name="txt_username" required/>
							</div>
							<div class="col-xs-6">
								<input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="{PASSWORD}" name="txt_password" required/>
							</div>
						</div>
						<!-- BEGIN alert -->
						<div class="alert alert-danger">
							<button class="close" data-close="alert"></button>
							<span>{alert.MESSAGE}</span>
						</div>
						<!-- END alert -->
						<div class="alert alert-danger display-hide">
							<button class="close" data-close="alert"></button>
							<span>{USERNAME_REQUEST}</span>
						</div>
						<div class="row">
							<div class="col-sm-4"></div>
							<div class="col-sm-8 text-right">
								<button class="btn blue" type="submit">{ENTER}</button>
							</div>
						</div>
					</form>
				</div>
				<div class="login-footer">
					<div class="row bs-reset">
						<div class="col-xs-4 bs-reset">
							<ul class="login-social">
								<li>
									<a href="https://www.facebook.com/MicroPHP-594367047257976/" onclick="window.open(this.href,'_blank');return false;">
										<i class="icon-social-facebook"></i>
									</a>
								</li>
								<li>
									<a href="https://twitter.com/microphp" onclick="window.open(this.href,'_blank');return false;">
										<i class="icon-social-twitter"></i>
									</a>
								</li>
								<li>
									<a href="http://www.microphp.com/" onclick="window.open(this.href,'_blank');return false;">
										<i class="fa fa-link"></i>
									</a>
								</li>
							</ul>
						</div>
						<div class="col-xs-8 bs-reset">
							<div class="login-copyright text-right">
								<p>
								&copy; {YEAR} mCMS | Powered by <a href="http://microphp.com" onclick="window.open(this.href,'_blank');return false;">MicroPHP</a>
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6 bs-reset">
				<div class="login-bg"> </div>
			</div>
		</div>
	</div>
	<!--[if lt IE 9]>
	<script src="{ROOT}assets/global/plugins/respond.min.js"></script>
	<script src="{ROOT}assets/global/plugins/excanvas.min.js"></script> 
	<![endif]-->
	<script src="{ROOT}assets/global/plugins/jquery.min.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/plugins/backstretch/jquery.backstretch.min.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/scripts/app.min.js" type="text/javascript"></script>
	<script src="{ROOT}assets/pages/scripts/login-5.js" type="text/javascript"></script>
</body>
</html>