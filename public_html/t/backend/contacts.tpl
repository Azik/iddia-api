
	<script>
		function click_cancel() {
			window.location.href = '?{MODULE_QS}=contacts';
		}
		function doAction(act, id, mess) {
			document.getElementById("action_id").value = id;
			document.getElementById("action").value = act;

			if (act == 'delete') {
				if (confirm(mess)) {
					document.getElementById("action_submit").click();
				}
			}
			else {
				document.getElementById("action_submit").click();
			}
		}

		function redirect(url) {
			window.location.href = url;
		}
	</script>

	
		<!-- BEGIN::CONTENT -->
		<div class="page-content-wrapper">
			<!-- BEGIN::CONTENT BODY -->
			<div class="page-content">
				<!-- BEGIN::PAGE HEADER-->
				<!-- BEGIN::PAGE BAR -->
				<div class="page-bar">
					<ul class="page-breadcrumb">
						<li>
							<a href="index.php">{HOME}</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<span><a href="index.php?{MODULE_QS}=contacts">{TITLE}</a></span>
						</li>
					</ul>
				</div>
				<!-- END::PAGE BAR -->
				<!-- BEGIN::PAGE TITLE-->
				<h3 class="page-title"> {TITLE} </h3>
				<!-- END::PAGE TITLE-->
				<!-- END::PAGE HEADER-->
				
				
				
				<!-- BEGIN options -->
				<div class="row">
					<div class="col-md-12">
						<div class="portlet light bordered">
							<div class="portlet-title">
								<div class="caption font-dark">
									<i class="fa fa-user"></i>
									<span class="caption-subject bold uppercase">{TITLE}</span>
								</div>
							</div>
							
				<form name="form_action" class="form-horizontal" id="form_action" method="post" action="">
					<input type="hidden" id="action" name="action" value="0">
					<input type="hidden" id="action_id" name="action_id" value="0">
					<input type="submit" id="action_submit" name="action_submit" style="display:none;">

					<!-- BEGIN contactsoptions -->
					<div class="portlet-body" id="panel_editbar">
						<div class="form-group form-md-line-input">
							<label class="col-md-3 control-label" for="txt_mail_to">{options.contactsoptions.MAIL_TO}</label>
							<div class="col-md-8">
								<input type="text" class="form-control{options.contactsoptions.DISABLED_CSS}" name="txt_mail_to"
									   id="txt_mail_to" value="{options.contactsoptions.MAIL_TO_VALUE}"
									   placeholder="{options.contactsoptions.MAIL_TO}" {options.contactsoptions.READONLY}>
								<div class="form-control-focus"> </div>
							</div>
						</div>
						<div class="form-group form-md-line-input">
							<label class="col-md-3 control-label" for="txt_mail_from">{options.contactsoptions.MAIL_FROM}</label>
							<div class="col-md-8">
								<input type="text" class="form-control{options.contactsoptions.DISABLED_CSS}" name="txt_mail_from"
									   id="txt_mail_from" value="{options.contactsoptions.MAIL_FROM_VALUE}"
									   placeholder="{options.contactsoptions.MAIL_FROM}" {options.contactsoptions.READONLY}>
								<div class="form-control-focus"> </div>
							</div>
						</div>
						<div class="form-group form-md-line-input">
							<label class="col-md-3 control-label" for="txt_mail_user">{options.contactsoptions.MAIL_USER}</label>
							<div class="col-md-8">
								<input type="text" class="form-control{options.contactsoptions.DISABLED_CSS}" name="txt_mail_user"
									   id="txt_mail_user" value="{options.contactsoptions.MAIL_USER_VALUE}"
									   placeholder="{options.contactsoptions.MAIL_USER}" {options.contactsoptions.READONLY}>
								<div class="form-control-focus"> </div>
							</div>
						</div>
						<div class="form-group form-md-line-input">
							<label class="col-md-3 control-label" for="txt_mail_pass">{options.contactsoptions.MAIL_PASS}</label>
							<div class="col-md-8">
								<input type="text" class="form-control{options.contactsoptions.DISABLED_CSS}" name="txt_mail_pass"
									   id="txt_mail_pass" value="{options.contactsoptions.MAIL_PASS_VALUE}"
									   placeholder="{options.contactsoptions.MAIL_PASS}" {options.contactsoptions.READONLY}>
								<div class="form-control-focus"> </div>
							</div>
						</div>
						<div class="form-group form-md-line-input">
							<label class="col-md-3 control-label" for="txt_mail_pop3">{options.contactsoptions.MAIL_POP3}</label>
							<div class="col-md-8">
								<input type="text" class="form-control{options.contactsoptions.DISABLED_CSS}" name="txt_mail_pop3"
									   id="txt_mail_pop3" value="{options.contactsoptions.MAIL_POP3_VALUE}"
									   placeholder="{options.contactsoptions.MAIL_POP3}" {options.contactsoptions.READONLY}>
								<div class="form-control-focus"> </div>
							</div>
						</div>
						<div class="form-group form-md-line-input">
							<label class="col-md-3 control-label" for="txt_mail_smtp">{options.contactsoptions.MAIL_SMTP}</label>
							<div class="col-md-8">
								<input type="text" class="form-control{options.contactsoptions.DISABLED_CSS}" name="txt_mail_smtp"
									   id="txt_mail_smtp" value="{options.contactsoptions.MAIL_SMTP_VALUE}"
									   placeholder="{options.contactsoptions.MAIL_SMTP}" {options.contactsoptions.READONLY}>
								<div class="form-control-focus"> </div>
							</div>
						</div>
						<div class="form-group form-md-line-input">
							<label class="col-md-3 control-label" for="txt_mail_host">{options.contactsoptions.MAIL_HOST}</label>
							<div class="col-md-8">
								<input type="text" class="form-control{options.contactsoptions.DISABLED_CSS}" name="txt_mail_host"
									   id="txt_mail_host" value="{options.contactsoptions.MAIL_HOST_VALUE}"
									   placeholder="{options.contactsoptions.MAIL_HOST}" {options.contactsoptions.READONLY}>
								<div class="form-control-focus"> </div>
							</div>
						</div>
						
						
						<!-- BEGIN alert -->
						<div class="form-actions" style="color:#f00;">
							{options.contactsoptions.alert.MESSAGE}
						</div>
						<!-- END alert -->
						<!-- BEGIN perm_edit -->
						<div class="form-actions">
							<div class="row">
								<div class="col-md-offset-3 col-md-9">
									<button type="button" class="btn blue" id="btn_save" name="btn_save"
											onClick="doAction('save_contactsoptions', 0,  '');"><i class="fa fa-ok"></i> {options.SAVE}
									</button>
									<button type="reset" class="btn" id="btn_cancel" name="btn_cancel"
											onclick="click_cancel();">{options.CANCEL}</button>
								</div>
							</div>
						</div>
						<!-- END perm_edit -->
					</div>
					<!-- END contactsoptions -->


				</form>
						</div>
					</div>
				</div>
				<!-- END options -->

				
			</div>
			<!-- END::CONTENT BODY -->
		</div>
		<!-- END::CONTENT -->
	</div>
	<!-- END::CONTAINER -->
	
	<script src="{ROOT}assets/global/plugins/jquery.min.js" type="text/javascript"></script>
	