	<!-- BEGIN::PAGE LEVEL PLUGINS -->
	<link href="{ROOT}assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
	<link href="{ROOT}assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
	<link href="{ROOT}assets/global/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet" type="text/css" />
	<link href="{ROOT}assets/global/plugins/jquery-file-upload/blueimp-gallery/blueimp-gallery.min.css" rel="stylesheet" type="text/css" />
	<link href="{ROOT}assets/global/plugins/jquery-file-upload/css/jquery.fileupload.css" rel="stylesheet" type="text/css" />
	<link href="{ROOT}assets/global/plugins/jquery-file-upload/css/jquery.fileupload-ui.css" rel="stylesheet" type="text/css" />
	<!-- END::PAGE LEVEL PLUGINS -->
	<script>
	function copyToClipboard(elem) {
		  // create hidden text element, if it doesn't already exist
		var targetId = "_hiddenCopyText_";
		var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
		var origSelectionStart, origSelectionEnd;
		if (isInput) {
			// can just use the original source element for the selection and copy
			target = elem;
			//alert(target);
			//origSelectionStart = elem.selectionStart;
			origSelectionStart = 0;
			//origSelectionEnd = elem.selectionEnd;
			origSelectionEnd = elem.value.length;
		} else {
			// must use a temporary form element for the selection and copy
			target = document.getElementById(targetId);
			if (!target) {
				var target = document.createElement("textarea");
				target.style.position = "absolute";
				target.style.left = "-9999px";
				target.style.top = "0";
				target.id = targetId;
				document.body.appendChild(target);
			}
			target.textContent = elem.textContent;
		}
		// select the content
		var currentFocus = document.activeElement;
		target.focus();
		target.setSelectionRange(0, target.value.length);
		
		// copy the selection
		var succeed;
		try {
			  succeed = document.execCommand("copy");
		} catch(e) {
			succeed = false;
		}
		//alert(succeed);
		
		return succeed;
	}
	</script>

	<script src="{ROOT}assets/global/plugins/jquery.min.js" type="text/javascript"></script>

	<script type="text/javascript">
	
	function clickAc(act) {
		//alert(act);
		var set = $(".checkboxes");
		var fields = '';
		$(set).each(function () {
			var checked = $(this).is(":checked");
			if (checked) {
				var v = $(this).val();
				fields += (fields != '') ? ',' : '';
				fields += v;
				//alert("aaa:"+v);
			}
		});
		if (fields!='') 
			window.location.href = '?{MODULE_QS}=gallery_manager&cateditid='+act+':'+fields;
	}
	function click_cancel()
	{	
		window.location.href = '?{MODULE_QS}=gallery_manager';
	} 
	
	function doCatDelete(id)
	{
		if (confirm('{CAT_CONFIRM}')) {
			document.getElementById("cat_action").value = 'delete';
			document.getElementById('select_id').value = id;
			document.getElementById('cat_subm').click();	
		}
	}
	function doCatAction(act, id,  mess)
	{		
		document.getElementById("cat_action").value = act;
		document.getElementById('select_id').value = id;
		
		if (act == 'delete') 
		{
			if(confirm(mess))
			{			
				document.getElementById("cat_subm").click();
			}
		}
		else
		{		
			document.getElementById("cat_subm").click();
		}			
	}

    function doOptionsAction() {
        document.getElementById("options_action").value = 'save_options';
        document.getElementById("options_subm").click();
    }

	function redirect(url)
	{	
		window.location.href = url;
	} 
	</script>
	
		<!-- BEGIN::CONTENT -->
		<div class="page-content-wrapper" style="min-width:650px !important;">
			<!-- BEGIN::CONTENT BODY -->
			<div class="page-content">
				<!-- BEGIN::PAGE HEADER-->
				<!-- BEGIN::PAGE BAR -->
				<div class="page-bar">
					<ul class="page-breadcrumb">
						<li>
							<a href="index.php">{HOME}</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<span><a href="index.php?{MODULE_QS}=gallery_manager">{TITLE}</a></span>
						</li>
						<!-- BEGIN nav -->
						<li>
							<i class="fa fa-angle-right"></i>
							<span><a href="index.php?{MODULE_QS}=gallery_manager{nav.URL}">{nav.TITLE}</a></span>
						</li>
						<!-- END nav -->
					</ul>
				</div>
				<!-- END::PAGE BAR -->
				<!-- BEGIN::PAGE TITLE-->
				<h3 class="page-title"> {TITLE} </h3>
				<!-- END::PAGE TITLE-->
				<!-- END::PAGE HEADER-->
								
				<!-- BEGIN gallery_cat -->
				<div class="row">
					<div class="col-md-12">
						<!-- B:EXAMPLE TABLE PORTLET-->
						<div class="portlet light bordered">
							<div class="portlet-title">
								<div class="caption font-dark">
									<i class="fa fa-camera"></i>
									<span class="caption-subject bold uppercase">{TITLE}</span>
								</div>
							</div>
							<form action="" method="POST" class="form-horizontal" name="form_cat" id="form_cat" >
							<input type="hidden" id="cat_action" name="action" value="">
							<input type="hidden" value="0" id="select_id" name="select_id" >
							<input type="submit" id="cat_subm" name="cat_subm" style="display:none;">
							<!-- BEGIN list -->
							<div class="portlet-body">
							<!-- BEGIN perm_add -->
							<div class="clearfix">
								<div class="btn-group">
									<a href="{gallery_cat.list.ADD_URL}" class="btn green">
										{gallery_cat.list.ADD} &nbsp; <i class="fa fa-plus"></i>
									</a>
								</div>
							</div>
							<!-- END perm_add -->
							<br/>
							<table class="table table-striped table-bordered table-hover" width="100%" id="sample_1">
							<thead>
								<tr>
									<th style="width:0px !important;display:none;"></th>
									<th style="width:12px !important;text-align:center;"><input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes"/></th>
									<th>{gallery_cat.list.NAME}</th>
									<th style="width : 150px !important;">{gallery_cat.list.CODE}</th>
									<!-- BEGIN perm_edit -->
									<th style="width : 100px !important;">{gallery_cat.list.EDIT}</th>
									<!-- END perm_edit -->
									<!-- BEGIN perm_del -->
									<th style="width : 100px !important;">{gallery_cat.list.DELETE}</th>
									<!-- END perm_del -->
									<th style="width : 100px !important;"></th>
								</tr>
							</thead>
							<tbody>
							<!-- BEGIN items -->
							<tr data-position="{gallery_cat.list.items.POSITION_NUM}" id="{gallery_cat.list.items.ID}">
								<td style="display:none;">{gallery_cat.list.items.POSITION_NUM}</td>
								<td style="width:12px !important;text-align:center;"><input type="checkbox" class="checkboxes" value="{gallery_cat.list.items.ID}" /></td>
								<td><a href="{gallery_cat.list.URL}&catid={gallery_cat.list.items.ID}">{gallery_cat.list.items.NAME}</a></td>
								<td style="width : 150px !important;">{gallery_cat.list.items.CODE}
									<a onclick="copyToClipboard(document.getElementById('copy_{gallery_cat.list.items.ID}'));"> <img src="{ROOT}img/copy.png" alt="{gallery_cat.list.COPY}" title="{gallery_cat.list.COPY}"/> </a>
								</td>
								<!-- BEGIN perm_edit -->
								<td class="option text-center"><a href="{gallery_cat.list.items.EDIT_URL}" title="{gallery_cat.list.EDIT}" class="btn yellow">
									{gallery_cat.list.EDIT}
									&nbsp; <i class="fa fa-pencil"></i>
								</a></td>
								<!-- END perm_edit -->
								<!-- BEGIN perm_del -->
								<td class="option text-center"><a href="JavaScript:doCatDelete({gallery_cat.list.items.ID})" title="{gallery_cat.list.DELETE}" title="{gallery_cat.list.DELETE}" class="btn red ask"> 
									{gallery_cat.list.DELETE}
									&nbsp; <i class="fa fa-trash">
								</a></td>
								<!-- END perm_del -->
								<td class="option text-center">
									
									<!-- BEGIN active -->
									<a href="{gallery_cat.list.items.ACTIVE_URL}" class="btn btn-success">{gallery_cat.list.ACTIVE}
										 &nbsp; <i class="fa fa-unlock"></i></a>
									<!-- END active -->
									<!-- BEGIN inactive -->
									<a href="{gallery_cat.list.items.INACTIVE_URL}" class="btn btn-danger">{gallery_cat.list.INACTIVE}
										 &nbsp; <i class="fa fa-lock"></i></a>
									<!-- END inactive -->
									
								</td>
							</tr>
							<!-- END items -->
							</tbody>
							</table>
							<!-- BEGIN hidden -->
							<input type="hidden" name="sort_order" id="sort_order" value="{gallery_cat.list.hidden.VALUE}" />
							<!-- END hidden -->
							<br/>
							<!-- BEGIN perm_edit -->
							<table>
								<tfoot>
								<tr>
									<td class="ac"><span class="label label-success"
														 onclick="clickAc('activate')">{gallery_cat.list.ACTIVATE}</span></td>
									<td style="width:10px;"></td>
									<td class="ac"><span class="label label-danger"
														 onclick="clickAc('inactivate')">{gallery_cat.list.INACTIVATE}</span></td>
									<td></td>
								</tr>
								</tfoot>
							</table>
							<!-- END perm_edit -->
							</div>
							<!-- END list -->
							
							<!-- BEGIN catedit -->
							<div class="portlet-body" id="panel_editbar">
							<div class="form-group form-md-checkboxes">
								<label class="col-md-3 control-label" for="txt_active">&nbsp;</label>
								<div class="col-md-6">
									<div class="md-checkbox-list">
										<div class="md-checkbox">
											<input type="checkbox" name="txt_active" id="txt_active" value="1" class="md-check"  {gallery_cat.catedit.ACTIVE_CHK} />
											<label for="txt_active">
												<span></span>
												<span class="check"></span>
												<span class="box"></span>{gallery_cat.catedit.ACTIVE}
											</label>
										</div>
									</div>
								</div>
							</div>
							<div class="tabbable-line boxless tabbable-reversed">
								<ul class="nav nav-tabs">
									<!-- BEGIN tab -->
									<li{gallery_cat.catedit.tab.CLASS}><a href="#content_{gallery_cat.catedit.tab.LANG}" data-toggle="tab">{gallery_cat.catedit.tab.LANG}</a></li>
									<!-- END tab -->
								</ul>
							</div>
							
							<div class="tab-content">
								<!-- BEGIN tab -->
								<div class="tab-pane {gallery_cat.catedit.tab.FADE_CLASS}" id="content_{gallery_cat.catedit.tab.LANG}">
									<div class="portlet box green">
									<div class="portlet-title">
										<div class="caption">
											<i class="fa fa-language"></i>{gallery_cat.catedit.tab.LANG} 
										</div>
									</div>
									<div class="portlet-body form">
									<div class="form-group form-md-line-input">
										<label class="col-md-3 control-label" for="txt_name_{gallery_cat.catedit.tab.LANG}">{gallery_cat.catedit.tab.NAME}</label>
										<div class="col-md-8">
											<input type="text" class="form-control" name="txt_name_{gallery_cat.catedit.tab.LANG}"
												   id="txt_name_{gallery_cat.catedit.tab.LANG}" value="{gallery_cat.catedit.tab.NAME_VALUE}"
												   placeholder="{gallery_cat.catedit.tab.NAME}">
											<div class="form-control-focus"> </div>
										</div>
									</div>
									<div class="form-group form-md-line-input">
										<label class="col-md-3 control-label" for="txt_gallery_desc_{gallery_cat.catedit.tab.LANG}">{gallery_cat.catedit.tab.GALLERY_DESC}</label>
										<div class="col-md-8">
											<textarea class="form-control ckeditor" name="txt_gallery_desc_{gallery_cat.catedit.tab.LANG}"
													  id="txt_gallery_desc_{gallery_cat.catedit.tab.LANG}" rows="3">{gallery_cat.catedit.tab.GALLERY_DESC_VALUE}</textarea>
											<div class="form-control-focus"> </div>
										</div>
									</div>
									<div class="form-actions">
										<div class="row">
											<div class="col-md-offset-3 col-md-9">
												<button type="button" class="btn blue" id="btn_save" name="btn_save"
														onClick="doCatAction('save', {gallery_cat.catedit.ID},  '');"><i class="fa fa-ok"></i> {gallery_cat.catedit.SAVE}
												</button>
												<button type="reset" class="btn" id="btn_cancel" name="btn_cancel"
														onclick="click_cancel();">{gallery_cat.catedit.CANCEL}</button>
											</div>
										</div>
									</div>
									</div>
									</div>
								</div>
								<!-- END tab -->
							</div>
							</div>
							<!-- END catedit -->

							</form>
						</div>
						<!-- E:EXAMPLE TABLE PORTLET-->
					</div>
				</div>
				<!-- END gallery_cat -->
				
				<!-- BEGIN gallery_images -->
				<!-- BEGIN::PAGE TITLE-->
				<h3 class="page-title">
					<strong>{gallery_images.CAT_NAME}</strong><br/>
				</h3>
				<!-- END::PAGE TITLE-->
				<!-- END::PAGE HEADER-->
				<div class="row">
					<div class="col-md-12">
						<form id="fileupload" action="index.php?{MODULE_QS}=gallery_manager&catid={gallery_images.CATID}" method="POST" enctype="multipart/form-data">
							<!-- Redirect browsers with JavaScript disabled to the origin page -->
							<noscript><input type="hidden" name="redirect" value="index.php?{MODULE_QS}=gallery_manager&catid={gallery_images.CATID}"></noscript>
							<!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
							<div class="row fileupload-buttonbar">
								<div class="col-lg-7">
									<!-- BEGIN perm_add -->
									<!-- The fileinput-button span is used to style the file input field as button -->
									<span class="btn green fileinput-button">
										<i class="fa fa-plus"></i>
										<span> Add files... </span>
										<input type="file" name="files[]" multiple="">
									</span>
									<button type="submit" class="btn blue start">
										<i class="fa fa-upload"></i>
										<span> Start upload </span>
									</button>
									<button type="reset" class="btn warning cancel">
										<i class="fa fa-ban-circle"></i>
										<span> Cancel upload </span>
									</button>
									<!-- END perm_add -->
									<!-- BEGIN perm_del -->
									<button type="button" class="btn red delete">
										<i class="fa fa-trash"></i>
										<span> Delete </span>
									</button>
									<input type="checkbox" class="toggle">
									<!-- END perm_del -->
									<!-- The global file processing state -->
									<span class="fileupload-process"> </span>
								</div>
								<!-- The global progress information -->
								<div class="col-lg-5 fileupload-progress fade">
									<!-- The global progress bar -->
									<div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
										<div class="progress-bar progress-bar-success" style="width:0%;"> </div>
									</div>
									<!-- The extended global progress information -->
									<div class="progress-extended"> &nbsp; </div>
								</div>
							</div>
							<!-- The table listing the files available for upload/download -->
							<table role="presentation" class="table table-striped clearfix">
								<tbody class="files"> </tbody>
							</table>
						</form>
					</div>
				</div>
				<!-- END gallery_images -->
				
				<!-- BEGIN gallery_images_js -->
				<!-- The blueimp Gallery widget -->
				<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" data-filter=":even">
					<div class="slides"> </div>
					<h3 class="title"></h3>
					<a class="prev"> ‹ </a>
					<a class="next"> › </a>
					<a class="close white"> </a>
					<a class="play-pause"> </a>
					<ol class="indicator"> </ol>
				</div>
				<!-- BEGIN::JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
				<script id="template-upload" type="text/x-tmpl"> {% for (var i=0, file; file=o.files[i]; i++) { %}
					<tr class="template-upload fade">
						<td>
							<span class="preview"></span>
						</td>
						<td>
							<p class="name">{%=file.name%}</p>
							<strong class="error text-danger label label-danger"></strong>
						</td>
						<td>
							<p class="size">Processing...</p>
							<div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
								<div class="progress-bar progress-bar-success" style="width:0%;"></div>
							</div>
						</td>
						<td> {% if (!i && !o.options.autoUpload) { %}
							<button class="btn blue start" disabled>
								<i class="fa fa-upload"></i>
								<span>Start</span>
							</button> {% } %} {% if (!i) { %}
							<button class="btn red cancel">
								<i class="fa fa-ban"></i>
								<span>Cancel</span>
							</button> {% } %} </td>
					</tr> {% } %} </script>
				<!-- The template to display files available for download -->
				<script id="template-download" type="text/x-tmpl"> 
				{% for (var i=0, file; file=o.files[i]; i++) { %}
					<tr class="template-download fade">
						<td>
							<span class="preview"> 
								{% if (file.thumbnail_url) { %}
								<a href="{%=file.url%}" title="{%=file.name%}" data-gallery="{%=file.thumbnail_url&&'gallery'%}" download="{%=file.name%}" data-gallery>
									<img src="{%=file.thumbnail_url%}">
								</a> 
								{% } %}
							</span>
						</td>
						<td>
							<p class="name"> 
								{% if (file.url) { %}
								<a href="{%=file.url%}" title="{%=file.name%}" data-gallery="{%=file.thumbnail_url&&'gallery'%}" download="{%=file.name%}" data-gallery>{%=file.name%}</a>
								{% } else { %}
								<span>{%=file.name%}</span> 
								{% } %}
							</p> 
							{% if (file.error) { %}
							<div>
								<span class="label label-danger">Error</span> {%=file.error%}
							</div> 
							{% } %}
						</td>
						<td>
							<span class="size">{%=o.formatFileSize(file.size)%}</span>
						</td>
						<td class="blck"> 
							<!-- BEGIN perm_del -->
							{% if (file.delete_url) { %}
							<button class="btn red delete btn-sm" data-type="{%=file.delete_type%}" data-url="{%=file.delete_url%}" {% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}' {% } %}>
								<i class="fa fa-trash-o"></i>
								<span>Delete</span>
							</button>
							<input type="checkbox" name="delete" value="1" class="toggle"> 
							{% } else { %}
							<button class="btn yellow cancel btn-sm">
								<i class="fa fa-ban"></i>
								<span>Cancel</span>
							</button> 
							{% } %} 
							<!-- END perm_del -->
						</td>
					</tr>
				{% } %}
				</script>
				
				<script type="text/javascript">
					var catid = {gallery_images_js.CATID};
				</script>
				<!-- END gallery_images_js -->
				
				
				
				
				<!-- BEGIN gallery_options -->
				<div class="row">
					<div class="col-md-12">
						<!-- B: TABLE PORTLET-->
						<button type="button" id="gallery_button" class="btn green" style="margin-bottom:15px !important;">{gallery_options.TITLE} &nbsp; <i class="fa fa-plus"></i></button>
						<script>
							$("#gallery_button").click(function () {
								$("#gallery_options").toggle("fade", 1000);
							});
						</script>
						
						<div class="portlet light bordered" id="gallery_options" style="display: none;">
							<div class="portlet-title" style="margin-bottom:-25px;">
								<h4>{gallery_options.TITLE}</h4>
							</div>
							<form action="" method="POST" name="options_form" id="options_form">
								<input type="hidden" id="options_action" name="action" value="">
								<input type="submit" id="options_subm" name="options_subm" style="display:none;">

								<div class="portlet-body form" id="panel_editbar">
								<div class="form-body">
									<div class="form-group">
										<label for="txt_watermark">&nbsp;</label>
										<div class="checkbox-list">
											<label>
												<input type="checkbox" name="txt_watermark" id="txt_watermark" value="1" {gallery_options.WATERMARK_CHK} /> 
												{gallery_options.WATERMARK}
											</label>
										</div>
									</div>
									<div class="form-group">
										<label>{gallery_options.WATERMARK_NAME}</label>
										<input type="text" class="form-control input-medium" name="txt_watermark_name" id="txt_watermark_name"
												   value="{gallery_options.WATERMARK_NAME_VALUE}"
												   placeholder="{gallery_options.WATERMARK_NAME}" >
									</div>
									<div class="form-group">
										<label>{gallery_options.WATERMARK_COLOR}</label>
										<input type="text" class="form-control input-medium" name="txt_watermark_color" id="txt_watermark_color"
												   value="{gallery_options.WATERMARK_COLOR_VALUE}"
												   placeholder="{gallery_options.WATERMARK_COLOR}" >
									</div>
									
									<div class="form-actions">
										<button type="button" class="btn blue" id="btn_save" name="btn_save"
												onClick="doOptionsAction();"><i class="fa fa-ok"></i> {gallery_options.SAVE}</button>
										<button type="reset" class="btn" id="btn_cancel" name="btn_cancel"
												onclick="click_cancel('cat', 0);">{gallery_options.CANCEL}</button>
									</div>
								</div>
								</div>

							</form>

						</div>
						<!-- E: TABLE PORTLET-->

					</div>
				</div>
				<!-- END gallery_options -->

			</div>
			<!-- END::CONTENT BODY -->
		</div>
		<!-- END::CONTENT -->
	</div>
	<!-- END::CONTAINER -->
	
	<!-- BEGIN::PAGE LEVEL PLUGINS -->
	<script src="{ROOT}assets/global/plugins/fancybox/source/jquery.fancybox.pack.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/plugins/jquery-file-upload/js/vendor/jquery.ui.widget.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/plugins/jquery-file-upload/js/vendor/tmpl.min.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/plugins/jquery-file-upload/js/vendor/load-image.min.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/plugins/jquery-file-upload/js/vendor/canvas-to-blob.min.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/plugins/jquery-file-upload/blueimp-gallery/jquery.blueimp-gallery.min.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/plugins/jquery-file-upload/js/jquery.iframe-transport.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/plugins/jquery-file-upload/js/jquery.fileupload.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/plugins/jquery-file-upload/js/jquery.fileupload-process.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/plugins/jquery-file-upload/js/jquery.fileupload-image.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/plugins/jquery-file-upload/js/jquery.fileupload-audio.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/plugins/jquery-file-upload/js/jquery.fileupload-video.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/plugins/jquery-file-upload/js/jquery.fileupload-validate.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/plugins/jquery-file-upload/js/jquery.fileupload-ui.js" type="text/javascript"></script>
	<!-- END::PAGE LEVEL PLUGINS -->

	<!-- BEGIN gallery_cat -->
	<script src="{ROOT}assets/global/scripts/dt/jquery.js" type="text/javascript"></script>
	<!-- BEGIN catedit -->
	<script src="{ROOT}assets/global/plugins/jquery.min.js" type="text/javascript"></script>
	<!-- END catedit -->
	<script src="{ROOT}assets/global/scripts/dt/jquery-ui.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/scripts/dt/jquery.dataTables.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/scripts/dt/jquery.dataTables.rowReordering.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/scripts/dt/jquery.dataTables.rowGrouping.js" type="text/javascript"></script>
	 
	<script type="text/javascript" charset="utf-8">
		$(document).ready(function () {
			var sortInput = jQuery('#sort_order');
			
			var oTable= $("#sample_1").dataTable({
				"bJQueryUI": true,
				"bDestroy": false,
				"bProcessing": false,
				"bSortable": false,
			});
		
			oTable.rowReordering({
				sURL : 'gallery_drag.php', 
				fnAlert: function(message) {
					//alert("order"); 
				}
			});
							//.clear().destroy()
			$('.group-checkable').click(function(){
				//alert($('.group-checkable').is(":checked"));
				var ch = $('.group-checkable').is(":checked");
				$('.checkboxes').prop('checked', ch);
			});
		});
	</script>
	<!-- END gallery_cat -->
	
	<!-- BEGIN::PAGE LEVEL SCRIPTS -->
	<script src="{ROOT}assets/pages/scripts/form-fileupload.js" type="text/javascript"></script>
	<!-- END::PAGE LEVEL SCRIPTS -->
	
	
	<script src="{ROOT}assets/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
	