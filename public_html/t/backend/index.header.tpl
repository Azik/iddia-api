<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<head>
	<meta content="noindex, nofollow" name="robots"/>
	<meta charset="utf-8" />
	<title>{TITLE}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1" name="viewport" />
	<meta content="MicroPHP" name="author"/>
	<!-- BEGIN::GLOBAL MANDATORY STYLES -->
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
	<link href="{ROOT}assets/global/plugins/font-awesome/css/fontawesome-all.css" rel="stylesheet" type="text/css" />
	<link href="{ROOT}assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
	<link href="{ROOT}assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<link href="{ROOT}assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
	<link href="{ROOT}assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
	<!-- END::GLOBAL MANDATORY STYLES -->
	
	<!-- BEGIN module_main -->
	<!-- BEGIN::PAGE LEVEL PLUGINS -->
	<link href="{ROOT}assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" />
	<link href="{ROOT}assets/global/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
	<link href="{ROOT}assets/global/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css" />
	<link href="{ROOT}assets/global/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css" />
	<!-- END::PAGE LEVEL PLUGINS -->
	<!-- END module_main -->
	
	<!-- BEGIN module_section -->
	<!-- BEGIN::PAGE LEVEL PLUGINS -->
	<link href="{ROOT}assets/global/plugins/jstree/dist/themes/default/style.min.css" rel="stylesheet" type="text/css" />
	<!-- END::PAGE LEVEL PLUGINS -->
	<!-- END module_section -->
	
	<link href="{ROOT}assets/global/css/components-md.min.css" rel="stylesheet" id="style_components" type="text/css" />
	<link href="{ROOT}assets/global/css/plugins-md.min.css" rel="stylesheet" type="text/css" />
	<link href="{ROOT}assets/layouts/layout/css/layout.css" rel="stylesheet" type="text/css" />
	<link href="{ROOT}assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
	<link href="{ROOT}assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
	<link rel="shortcut icon" href="favicon.ico" />
	
	<!-- BEGIN module_section -->

	<script type="text/javascript">
		
		var arrLangs = new Array({LANG_LIST});
		function defineShowHide(sectionIndex) {
			//var sectionIndex = obj.selectedIndex;
			//alert(sectionIndex);
			switch (sectionIndex) {
				case 0 :
				case 1 :
				{
					//document.getElementById('tr_content').style.display = '';
					document.getElementById('tr_link').style.display = 'none';
					document.getElementById('tr_module').style.display = 'none';
					for (i = 0; i < arrLangs.length; i++) {
						for (j = 1; j <= 2; j++) {
							var elmTR = document.getElementById('tr_content_' + arrLangs[i] + '_' + j);
							elmTR.style.display = '';
						}
					}

					document.getElementById('txt_type').selectedIndex = sectionIndex;
					break;
				}

				case 2 :
				{
					//document.getElementById('tr_content').style.display = 'none';
					document.getElementById('tr_link').style.display = '';
					document.getElementById('tr_module').style.display = 'none';

					for (i = 0; i < arrLangs.length; i++) {
						for (j = 1; j <= 2; j++) {
							var elmTR = document.getElementById('tr_content_' + arrLangs[i] + '_' + j);
							elmTR.style.display = 'none';
						}
					}

					document.getElementById('txt_type').selectedIndex = sectionIndex;

					break;
				}

				case 3 :
				case 4 :
				{
					//document.getElementById('tr_content').style.display = 'none';
					document.getElementById('tr_link').style.display = 'none';
					document.getElementById('tr_module').style.display = '';

					for (i = 0; i < arrLangs.length; i++) {
						for (j = 1; j <= 2; j++) {
							var elmTR = document.getElementById('tr_content_' + arrLangs[i] + '_' + j);
							elmTR.style.display = 'none';
						}
					}

					document.getElementById('txt_type').selectedIndex = sectionIndex;

					break;
				}
			}
		}
	</script>
	<script type="text/javascript">
		function mCMSadd() {
			var to_id = document.getElementById('select_id').value;
			window.location.href = "index.php?{MODULE_QS}=section&c_id=-1&to=" + to_id;
		}

		function mCMSedit() {
			var sel_id = document.getElementById('select_id').value;
			if (sel_id != "0") {
				window.location.href = "index.php?{MODULE_QS}=section&c_id=" + sel_id;
			}
		}

		function replaceQS(sURL, val) {
			var rURL = sURL;

			reg = /(\?)?(\&)?c_id=([\w.*%_-])*/ig;

			rURL = rURL.replace(reg, '');

			if (rURL.indexOf("?") > 0) {
				if ((rURL.substr(rURL.length - 1, 1) != "&") && (rURL.substr(rURL.length - 1, 1) != "?")) {
					rURL += "&";
				}
			}
			else {
				rURL += "?";
			}

			rURL += 'c_id=' + val;

			return rURL;
		}

		function replaceQSTO(sURL, val) {
			var rURL = sURL;

			reg = /(\?)?(\&)?to=([\w.*%_-])*/ig;

			rURL = rURL.replace(reg, '');

			if (rURL.indexOf("?") > 0) {
				if ((rURL.substr(rURL.length - 1, 1) != "&") && (rURL.substr(rURL.length - 1, 1) != "?")) {
					rURL += "&";
				}
			}
			else {
				rURL += "?";
			}

			rURL += 'to=' + val;

			return rURL;
		}

		function click_cancel() {
			window.location.href = removeQSTO(removeQS(self.location.href));
		}

		function removeQS(sURL) {
			var rURL = sURL;

			reg = /(\?)?(\&)?c_id=([\w.*%_-])*/ig;

			rURL = rURL.replace(reg, '');

			return rURL;
		}

		function removeQSTO(sURL) {
			var rURL = sURL;

			reg = /(\?)?(\&)?to=([\w.*%_-])*/ig;

			rURL = rURL.replace(reg, '');

			return rURL;
		}

		function doAct(act, mess) {
			document.getElementById("action").value = act;

			if (act == 'delete') {
				if (confirm(mess)) {
					document.getElementById("form_edit").submit();
				}
			}
			else {
				//document.getElementById("form_edit").submit();
				document.getElementById("subm").click();
			}
		}
	</script>
	<!-- END module_section -->
	
</head>
<!-- END::HEAD -->
<body class="page-header-fixed page-sidebar-closed-hide-logo 
<!-- BEGIN module_main -->	
page-container-bg-solid 
<!-- END module_main -->
page-content-white page-md">
	<!-- BEGIN::HEADER -->
	<div class="page-header navbar navbar-fixed-top">
		<!-- BEGIN::HEADER INNER -->
		<div class="page-header-inner ">
			<!-- BEGIN::LOGO -->
			<div class="page-logo">
				<a href="index.php"><img src="{ROOT}assets/layouts/layout/img/logo.png" alt="logo" class="logo-default" /></a>
				<div class="menu-toggler sidebar-toggler"> </div>
			</div>
			<!-- END::LOGO -->
			<!-- BEGIN::RESPONSIVE MENU TOGGLER -->
			<a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
			<!-- END::RESPONSIVE MENU TOGGLER -->
			<!-- BEGIN::TOP NAVIGATION MENU -->
			<div class="top-menu">
				<ul class="nav navbar-nav pull-right">
					<!-- B:USER RIGHTS -->
					<li class="dropdown_info">
						<p>{USER_RIGHT}</p>
					</li>
					<!-- E:USER RIGHTS -->
					<!-- B:HOMEPAGE -->
					<li class="dropdown home" title="{VISIT_SITE}">
						<a class="dropdown-toggle" href="../" onclick="window.open(this.href,'_blank');return false;"
						   title="{VISIT_SITE}" title="{VISIT_SITE}">
							<i class="icon-home"></i>
						</a>
						<ul></ul>
					</li>
					<!-- E:HOMEPAGE -->
					<!-- BEGIN::USER LOGIN DROPDOWN -->
					<!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
					<li class="dropdown dropdown-user">
						<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
							<img alt="" class="img-circle" src="{ROOT}assets/layouts/layout/img/avatar.png" />
							<span class="username username-hide-on-mobile"> {USER_NAME} </span>
							<i class="fa fa-angle-down"></i>
						</a>
						<ul class="dropdown-menu dropdown-menu-default">
							<!-- BEGIN header_options_site -->
							<li>
								<a href="index.php?{MODULE_QS}=options_site&mode=siteoptions"><i
									class="fa fa-cog"></i> {SITEOPTIONS}</a>
							</li>
							<!-- END header_options_site -->
							<!-- BEGIN header_options_profile -->
							<li>
								<a href="index.php?{MODULE_QS}=options_profile&mode=profile">
									<i class="icon-user"></i> {PROFILE} </a>
							</li>
							<li>
								<a href="index.php?{MODULE_QS}=options_profile&mode=password">
									<i class="fa fa-lock"></i> {PASSWORD} </a>
							</li>
							<!-- END header_options_profile -->
							<li class="divider"> </li>
							<li>
								<a href="index.php?{MODULE_QS}=action&act=logout">
									<i class="fa fa-power-off"></i> {LOGOUT} </a>
							</li>
						</ul>
					</li>
					<!-- END::USER LOGIN DROPDOWN -->
					<!-- BEGIN::QUICK SIDEBAR TOGGLER -->
					<!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
					<li class="dropdown dropdown-quick-sidebar-toggler">
						<a href="index.php?{MODULE_QS}=action&act=logout" class="dropdown-toggle">
							<i class="icon-logout"></i>
						</a>
					</li>
					<!-- END::QUICK SIDEBAR TOGGLER -->
				</ul>
			</div>
			<!-- END::TOP NAVIGATION MENU -->
		</div>
		<!-- END::HEADER INNER -->
	</div>
	<!-- END::HEADER -->
	<!-- BEGIN::HEADER & CONTENT DIVIDER -->
	<div class="clearfix"> </div>
	<!-- END::HEADER & CONTENT DIVIDER -->
	<!-- BEGIN::CONTAINER -->
	<div class="page-container">
		<!-- BEGIN::SIDEBAR -->
		<div class="page-sidebar-wrapper">
			<!-- BEGIN::SIDEBAR -->
			<div class="page-sidebar navbar-collapse collapse">
				<!-- BEGIN::SIDEBAR MENU -->
				<ul class="page-sidebar-menu  page-header-fixed page-sidebar-menu-light " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
					<!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
					<li class="sidebar-toggler-wrapper hide">
						<!-- BEGIN::SIDEBAR TOGGLER BUTTON -->
						<div class="sidebar-toggler"> </div>
						<!-- END::SIDEBAR TOGGLER BUTTON -->
					</li>
					<!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
					<li class="sidebar-search-wrapper">
						<!-- BEGIN::RESPONSIVE QUICK SEARCH FORM -->
						<form class="sidebar-search" action="">
							<div class="input-group">
								<span class="input-group-btn">
									<a href="javascript:;" class="btn submit">
										<i></i>
									</a>
								</span>
							</div>
						</form>
						<!-- END::RESPONSIVE QUICK SEARCH FORM -->
					</li>
					<!-- BEGIN menu -->
					<!-- BEGIN heading -->
					<li class="heading">
						<h3 class="uppercase">{menu.heading.NAME}</h3>
					</li>
					<!-- END heading -->
					<!-- BEGIN items -->
					<li class="nav-item {menu.items.ACTIVE_CLASS_NM}">
						<a href="{menu.items.URL}" class="nav-link nav-toggle">
							<i class="{menu.items.ICON}"></i>
							<span class="title">{menu.items.TEXT}</span>
							{menu.items.SELECTED_CLASS}
						</a>
						<!-- BEGIN sub -->
						<ul class="sub-menu">
							<!-- BEGIN link -->
							<li class="nav-item">
								<a href="{menu.items.sub.link.URL}" class="nav-link ">
									<span class="title">{menu.items.sub.link.TEXT}</span>
								</a>
							</li>
							<!-- END link -->
						</ul>
						<!-- END sub -->
					</li>
					<!-- END items -->
					<!-- END menu -->
				</ul>
				<!-- END::SIDEBAR MENU -->
			</div>
			<!-- END::SIDEBAR -->
		</div>
		<!-- END::SIDEBAR -->
		
		<div id="ajax_dialog"></div>