	
	<script language="javascript">
		function addLang(btn) {
			if (document.getElementById) {
				var lg = document.getElementById('txt_lang').value;
				if (lg.length < 2)
					return;
				document.getElementById('txt_lang').value = '';

				tr = btn;
				while (tr.tagName != 'TR') tr = tr.parentNode;
				tr = tr.previousSibling.previousSibling;
				var newTr = tr.parentNode.insertBefore(tr.cloneNode(true), tr.nextSibling);

				thisChilds = newTr.getElementsByTagName('td');

				for (var i = 0; i < thisChilds.length; i++) {
					if (thisChilds[i].className == 'lang') {
						thisChilds[i].innerHTML = lg;
						break;
					}
				}

				var count = document.getElementsByName('txt_langs[]').length;
				document.getElementsByName('txt_langs[]')[count - 1].value = lg;
				document.getElementsByName('txt_showlangs[]')[count - 1].value = lg;
				document.getElementsByName('txt_mainlang')[count - 1].value = lg;

				document.getElementsByName('txt_showlangs[]')[count - 1].checked = false;
				document.getElementsByName('txt_mainlang')[count - 1].checked = false;

				checkLast();
			}
		}

		function checkLang() {
			var count = document.getElementsByName('txt_langs[]').length;

			var found = false;
			for (var i = 0; i < count; i++) {
				if (document.getElementsByName('txt_mainlang')[i].checked) {
					found = true;
					break;
				}
			}

			if (!found)
				document.getElementsByName('txt_mainlang')[0].checked = true;
		}

		function removeLang(btn) {
			if (document.getElementById) {
				tr = btn;
				while (tr.tagName != 'TR') tr = tr.parentNode;
				tr.parentNode.removeChild(tr);
				checkLast();
				checkLang();
			}
		}

		function checkLast() {
			btns = document.getElementsByName('minus');
			for (i = 0; i < btns.length; i++) {
				btns[i].disabled = (btns.length == 1) ? true : false;
			}
		}
	</script>
	<script language="javascript" event="onload" for="window">
		checkLast();
	</script>

	<script>
		function click_cancel() {
			window.location.href = '?{MODULE_QS}=options_profile';
		}
		function doAction(act, id, mess) {
			document.getElementById("action_id").value = id;
			document.getElementById("action").value = act;

			if (act == 'delete') {
				if (confirm(mess)) {
					document.getElementById("action_submit").click();
				}
			}
			else {
				document.getElementById("action_submit").click();
			}
		}

		function redirect(url) {
			window.location.href = url;
		}
	</script>

		<!-- BEGIN::CONTENT -->
		<div class="page-content-wrapper">
			<!-- BEGIN::CONTENT BODY -->
			<div class="page-content">
				<!-- BEGIN::PAGE HEADER-->
				<!-- BEGIN::PAGE TITLE-->
				<h3 class="page-title"> {TITLE} </h3>
				<!-- END::PAGE TITLE-->
				<!-- BEGIN::PAGE BAR -->
				<div class="page-bar">
					<ul class="page-breadcrumb">
						<li>
							<a href="index.php">{HOME}</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<span><a href="index.php?{MODULE_QS}=options_profile">{TITLE}</a></span>
						</li>
						<!-- BEGIN sub_link -->
						<li>
							<i class="fa fa-angle-right"></i>
							<a href="index.php?{MODULE_QS}=options_profile&mode={sub_link.MODE}">{sub_link.TEXT}</a>
						</li>
						<!-- END sub_link -->
					</ul>
				</div>
				<!-- END::PAGE BAR -->
				<!-- END::PAGE HEADER-->
				
				
		<!-- BEGIN options -->
		<div class="row">
			<div class="col-md-12">
			<form name="form_action" id="form_action" class="form-horizontal" method="post" action="">
				<input type="hidden" id="action" name="action" value="0">
				<input type="hidden" id="action_id" name="action_id" value="0">
				<input type="submit" id="action_submit" name="action_submit" style="display:none;">

				<!-- BEGIN default -->
				<div class="portlet light bordered">
					<div class="portlet-title">
						<div class="caption font-dark">
							<i class="fa fa-wrench"></i>
							<span class="caption-subject bold uppercase">{options.default.TITLE}</span>
						</div>
					</div>
					<div class="note note-info">
						<a href="{options.URL}&mode=profile" class="btn blue btn-block">{options.default.PROFILE}</a>
					</div>
					<div class="note note-info">
						<a href="{options.URL}&mode=password" class="btn blue btn-block">{options.default.PASSWORD}</a>
					</div>
				</div>
				<!-- END default -->

				<!-- BEGIN profile -->
				<div class="portlet light bordered">
					<div class="portlet-title">
						<div class="caption font-dark">
							<i class="fa fa-user"></i>
							<span class="caption-subject bold uppercase">{options.profile.TITLE}</span>
						</div>
					</div>
					<input type="hidden" name="txt_id" id="txt_id" value="{options.profile.ID}">
					<div class="portlet-body" id="panel_editbar">
						<div class="form-group form-md-line-input">
							<label class="col-md-3 control-label" for="txt_name">{options.profile.NAME}</label>
							<div class="col-md-8">
								<input type="text" class="form-control{DISABLED_CSS}" name="txt_name"
									   id="txt_name" value="{options.profile.NAME_VALUE}"
									   placeholder="{options.profile.NAME}"{READONLY}{DISABLED_TYPE}>
								<div class="form-control-focus"> </div>
							</div>
						</div>
						<div class="form-group form-md-line-input">
							<label class="col-md-3 control-label" for="txt_email">{options.profile.EMAIL}</label>
							<div class="col-md-8">
								<input type="text" class="form-control{DISABLED_CSS}" name="txt_email"
									   id="txt_email" value="{options.profile.EMAIL_VALUE}"
									   placeholder="{options.profile.EMAIL}"{READONLY}{DISABLED_TYPE}>
								<div class="form-control-focus"> </div>
							</div>
						</div>
						<!-- BEGIN alert -->
						<div class="form-actions" style="color:#f00;">
							{options.profile.alert.MESSAGE}
						</div>
						<!-- END alert -->
						<div class="form-actions">
							<div class="row">
								<div class="col-md-offset-3 col-md-9">
									<button type="button" class="btn blue" id="btn_save" name="btn_save"
											onClick="doAction('save', 0,  '');"{READONLY}{DISABLED_TYPE}><i class="fa fa-ok"></i> {options.SAVE}
									</button>
									<button type="reset" class="btn" id="btn_cancel" name="btn_cancel"
											onclick="click_cancel();"{READONLY}{DISABLED_TYPE}>{options.CANCEL}</button>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- END profile -->
				
				<!-- BEGIN changepassword -->
				<div class="portlet light bordered">
					<div class="portlet-title">
						<div class="caption font-dark">
							<i class="fa fa-user"></i>
							<span class="caption-subject bold uppercase">{options.changepassword.TITLE}</span>
						</div>
					</div>
					<div class="portlet-body" id="panel_editbar">
						<div class="form-group form-md-line-input">
							<label class="col-md-3 control-label" for="txt_oldpass">{options.changepassword.OLDPASS}</label>
							<div class="col-md-8">
								<input type="password" class="form-control{DISABLED_CSS}" name="txt_oldpass"
									   id="txt_oldpass" value=""
									   placeholder="{options.changepassword.OLDPASS}" {READONLY}{DISABLED_TYPE}>
								<div class="form-control-focus"> </div>
							</div>
						</div>
						<div class="form-group form-md-line-input">
							<label class="col-md-3 control-label" for="txt_newpass">{options.changepassword.NEWPASS}</label>
							<div class="col-md-8">
								<input type="password" class="form-control{DISABLED_CSS}" name="txt_newpass"
									   id="txt_newpass" value=""
									   placeholder="{options.changepassword.NEWPASS}" {READONLY}{DISABLED_TYPE}>
								<div class="form-control-focus"> </div>
							</div>
						</div>
						<div class="form-group form-md-line-input">
							<label class="col-md-3 control-label" for="txt_confirmnewpass">{options.changepassword.CONFIRMNEWPASS}</label>
							<div class="col-md-8">
								<input type="password" class="form-control{DISABLED_CSS}" name="txt_confirmnewpass"
									   id="txt_confirmnewpass" value=""
									   placeholder="{options.changepassword.CONFIRMNEWPASS}" {READONLY}{DISABLED_TYPE}>
								<div class="form-control-focus"> </div>
							</div>
						</div>
						<!-- BEGIN alert -->
						<div>
							<font color="red">{options.changepassword.alert.MESSAGE}</font>
						</div>
						<!-- END alert -->
						<div class="form-actions">
							<div class="row">
								<div class="col-md-offset-3 col-md-9">
									<button type="button" class="btn blue{DISABLED_CSS}" id="btn_save" name="btn_save"
											onClick="doAction('changepassword', 0,  '');"{READONLY}{DISABLED_TYPE}><i class="fa fa-ok"></i> {options.SAVE}
									</button>
									<button type="reset" class="btn{DISABLED_CSS}" id="btn_cancel" name="btn_cancel"
											onclick="redirect('{options.URL}');"{READONLY}{DISABLED_TYPE}>{options.CANCEL}</button>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- END changepassword -->

			</form>			
			</div>
		</div>
		<!-- END options -->
				
				
			</div>
			<!-- END::CONTENT BODY -->
		</div>
		<!-- END::CONTENT -->
	</div>
	<!-- END::CONTAINER -->
	<script src="{ROOT}assets/global/plugins/jquery.min.js" type="text/javascript"></script>
	