<html>
<head>
</head>
<body>

<div class="row">
<div class="col-md-12">
	<strong>{_LINK_SELECT_TITLE_}</strong>
</div>
<div class="row">
<div class="col-md-12">
	<!-- BEGIN mcms_toolbar -->
	<table style="display: none;" id="panel_toolbar_zero" width="100%" border="0" cellpadding="0"
		   cellspacing="0">
		<tr>
			<td style="width:300px !important;margin-top:0px;">&nbsp;</td>
			<td>
				<h3 class="panel-title" id="panel-title-zero"></h3>
				<input type="hidden" value="{LINK_ID}" id="link_select_id" name="link_select_id">
				<button type="button" class="btn blue" onclick="selectPageLink();"><i class="m-fa fa-swapright"></i> {mcms_toolbar.SELECT_LINK}</button>
			</td>
		</tr>
	</table>
	<br/>
	<script>
	function selectPageLink() {
		var link_id = document.getElementById("link_select_id").value;
		$("#{mcms_toolbar.SELECT_FIELD}").val(link_id);
		$("#ajax_dialog").dialog('close');
	}
	</script>

	<!-- END mcms_toolbar -->
</div>
<div class="col-md-12">
	<!-- BEGIN mcms_tree -->
	<div class="portlet">
		<div class="portlet-body">
			<div id="mcms_tree" class="tree-demo" {LEFT_STYLE}> </div>
		</div>
	</div>
	<!-- END mcms_tree -->
</div>
</div>
</div>


<script src="{ROOT}assets/pages/scripts/ui-tree-links.js" type="text/javascript"></script>
</body>
</html>