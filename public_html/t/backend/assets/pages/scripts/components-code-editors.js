var ComponentsCodeEditors = function () {
    
    var handleDemo1 = function () {
        var myTextArea = document.getElementById('code_js_editor');
        var myCodeMirror = CodeMirror.fromTextArea(myTextArea, {
            lineNumbers: true,
            matchBrackets: true,
            styleActiveLine: true,
            theme:"ambiance",
            mode: 'javascript'
        });
    }

    var handleDemo2 = function () {
        var myTextArea = document.getElementById('code_editor_demo_2');
        var myCodeMirror = CodeMirror.fromTextArea(myTextArea, {
            lineNumbers: true,
            matchBrackets: true,
            styleActiveLine: true,
            theme:"material",
            mode: 'css'
        });
    }

    var handleDemo3 = function () {
        var myTextArea = document.getElementById('code_editor_demo_3');
        var myCodeMirror = CodeMirror.fromTextArea(myTextArea, {
            lineNumbers: true,
            matchBrackets: true,
            styleActiveLine: true,
            theme:"neat",
            mode: 'javascript',
            readOnly: true
        });
    }

    var handleDemo4 = function () {
        var myTextArea = document.getElementById('code_editor_demo_4');
        var myCodeMirror = CodeMirror.fromTextArea(myTextArea, {
            lineNumbers: true,
            matchBrackets: true,
            styleActiveLine: true,
            theme:"neo",
            mode: 'css',
            readOnly: true
        });
    }
	
    var handleHTML1 = function () {
        var myTextArea = document.getElementById('txt_file_1');
        var myCodeMirror = CodeMirror.fromTextArea(myTextArea, {
            lineNumbers: true,
			mode: "html"
        });
    }
    var handleHTML2 = function () {
        var myTextArea = document.getElementById('txt_file_2');
        var myCodeMirror = CodeMirror.fromTextArea(myTextArea, {
            lineNumbers: true,
			mode: "html"
        });
    }
    var handleHTML3 = function () {
        var myTextArea = document.getElementById('txt_file_3');
        var myCodeMirror = CodeMirror.fromTextArea(myTextArea, {
            lineNumbers: true,
			mode: "html"
        });
    }
    var handlePHP = function () {
        var myTextArea = document.getElementById('txt_lang_file_en');
        var myCodeMirror = CodeMirror.fromTextArea(myTextArea, {
            lineNumbers: true,
			mode: "php"
        });
    }
    var handlePHP2 = function () {
        var myTextArea = document.getElementById('txt_module_file');
        var myCodeMirror = CodeMirror.fromTextArea(myTextArea, {
            lineNumbers: true,
			mode: "php"
        });
    }


    return {
        //main function to initiate the module
        init: function () {
            //handleDemo1();
            //handleDemo2();
            //handleDemo3();
            //handleDemo4();
			handleHTML1();
			handleHTML2();
			handleHTML3();
			handlePHP();
			handlePHP2();
        }
    };

}();

jQuery(document).ready(function() {    
   ComponentsCodeEditors.init(); 
});