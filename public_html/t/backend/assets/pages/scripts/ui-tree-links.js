var UITree = function () {
	
     var mCMSajaxTree = function() {
		
        $("#mcms_tree").jstree({
            "core" : {
                "themes" : {
                    "responsive": false
                }, 
                // so that create works
                "check_callback" : true,
                'data' : {
                    'url' : function (node) {
						var link_id = document.getElementById("link_select_id").value;
                      return './tree_json.php?link_id='+link_id;
                    },
                    'data' : function (node) {
                      return { 'parent' : node.id };
                    }
                }
            },
            "types" : {
                "default" : {
                    "icon" : "fa fa-folder icon-state-warning icon-lg"
                },
                "file" : {
                    "icon" : "fa fa-file icon-state-warning icon-lg"
                }
            },
			"state" : { "key" : "demo3" },
            "plugins" : [ "dnd", "state", "types" ]
        });
		
		
		
		$('#mcms_tree').on("select_node.jstree", function (e, data) {
			//console.log(data);
			//alert(data.node.id);
			var nodeid = parseInt(data.node.id);
			var pid = parseInt(data.node.parent);
			pid = (isNaN(pid)) ? 0 : pid;
			nodeid = (isNaN(nodeid)) ? 0 : nodeid;
			
			if (nodeid == 0) {
				document.getElementById('panel_toolbar_zero').style.display = 'none';
				document.getElementById('panel-title-zero').innerHTML = '';
			} else {
				document.getElementById('panel_toolbar_zero').style.display = '';
				document.getElementById('panel-title-zero').innerHTML = data.node.text;
			}
			document.getElementById('link_select_id').value = nodeid;
		});
    
    }
	
    return {
        //main function to initiate the module
        init: function () {

            mCMSajaxTree();
        }
    };

}();

if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function() {    
       UITree.init();
    });
}