		
        <div class="page-footer">
            <div class="page-footer-inner"> 
				{YEAR} &copy; 
				mCMS | Powered by <a href="http://microphp.com" onclick="window.open(this.href,'_blank');return false;">MicroPHP</a>
            </div>
            <div class="scroll-to-top">
                <i class="icon-arrow-up"></i>
            </div>
        </div>
        <!--[if lt IE 9]>
		<script src="{ROOT}assets/global/plugins/respond.min.js"></script>
		<script src="{ROOT}assets/global/plugins/excanvas.min.js"></script> 
		<![endif]-->
        
		<!-- BEGIN module_section -->
        <script src="{ROOT}assets/global/plugins/jquery.min.js" type="text/javascript"></script>
		<!-- END module_section -->
		
        <script src="{ROOT}assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <!-- <script src="{ROOT}assets/global/plugins/js.cookie.min.js" type="text/javascript"></script> -->
        <script src="{ROOT}assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="{ROOT}assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="{ROOT}assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="{ROOT}assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
        <script src="{ROOT}assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END::CORE PLUGINS -->
		<!-- BEGIN module_main -->
        <!-- BEGIN::PAGE LEVEL PLUGINS -->
        <script src="{ROOT}assets/global/plugins/moment.min.js" type="text/javascript"></script>
        <script src="{ROOT}assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js" type="text/javascript"></script>
        <script src="{ROOT}assets/global/plugins/morris/morris.min.js" type="text/javascript"></script>
        <script src="{ROOT}assets/global/plugins/morris/raphael-min.js" type="text/javascript"></script>
        <script src="{ROOT}assets/global/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
        <script src="{ROOT}assets/global/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
        
		
        <script src="{ROOT}assets/global/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
        <script src="{ROOT}assets/global/plugins/flot/jquery.flot.min.js" type="text/javascript"></script>
        <script src="{ROOT}assets/global/plugins/flot/jquery.flot.resize.min.js" type="text/javascript"></script>
        <script src="{ROOT}assets/global/plugins/flot/jquery.flot.categories.min.js" type="text/javascript"></script>
        <script src="{ROOT}assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js" type="text/javascript"></script>
        <script src="{ROOT}assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
        <script src="{ROOT}assets/global/plugins/jqvmap/jqvmap/jquery.vmap.js" type="text/javascript"></script>
        <script src="{ROOT}assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js" type="text/javascript"></script>
        <script src="{ROOT}assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
        <script src="{ROOT}assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js" type="text/javascript"></script>
        <script src="{ROOT}assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js" type="text/javascript"></script>
        <script src="{ROOT}assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js" type="text/javascript"></script>
        <script src="{ROOT}assets/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>
        <!-- END::PAGE LEVEL PLUGINS -->
        <!-- END module_main -->
		<!-- BEGIN module_section -->
		<!-- BEGIN::PAGE LEVEL PLUGINS -->
        <script src="{ROOT}assets/global/plugins/jstree/dist/jstree.min.js" type="text/javascript"></script>
        <!-- END::PAGE LEVEL PLUGINS -->
		<!-- END module_section -->
		
		<!-- BEGIN::THEME GLOBAL SCRIPTS -->
        <script src="{ROOT}assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END::THEME GLOBAL SCRIPTS -->
		<!-- BEGIN::PAGE LEVEL SCRIPTS -->
        <!-- BEGIN module_main -->
        <script src="{ROOT}assets/pages/scripts/dashboard.min.js" type="text/javascript"></script>
		<!-- END module_main -->
		
		<!-- BEGIN module_section -->
		<script src="{ROOT}assets/pages/scripts/ui-tree.js" type="text/javascript"></script>
		<!-- END module_section -->
		
		<!-- END::PAGE LEVEL SCRIPTS -->
		<!-- BEGIN::THEME LAYOUT SCRIPTS -->
		<script src="{ROOT}assets/layouts/layout/scripts/layout.js" type="text/javascript"></script>
		<script src="{ROOT}assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
		<!-- END::THEME LAYOUT SCRIPTS -->
		
		<script src="{ROOT}assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
		<link href="{ROOT}assets/global/plugins/jquery-ui/jquery-ui.mcms.css" rel="stylesheet">
		
		<script type="text/javascript">
			jQuery.browser = { };
			
			$.curCSS = function (element, attrib, val) {
				$(element).css(attrib, val);
			};
			(function () {
				jQuery.browser.msie = false;
				jQuery.browser.version = 0;
				if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
					jQuery.browser.msie = true;
					jQuery.browser.version = RegExp.$1;
				}
			})();
		</script>
		<script>
		$("#ajax_dialog").dialog ({
		  autoOpen : false,
		  height: 500,
		  width: 700,
		  modal: true,
		  resizable: true,
		  dialogClass: 'no-close success-dialog'
		});
		
		function openPhoto(img_id=0, select_field='') {
			img_id = (Math.floor(img_id) == img_id && $.isNumeric(img_id)) ? img_id : 0;
			
			$("#ajax_dialog").dialog ("open");
			$("#ajax_dialog").html('{_IMAGE_SELECT_LOADING_}');
			$("#ajax_dialog").dialog('option', 'title', '{_IMAGE_SELECT_TITLE_}');
			
			// B: Ajax Dialog
			var ajax_url = "ajax.photo.php";
			$.ajax({
			  type: 'GET', 
			  contentType: "application/x-www-form-urlencoded; charset=utf-8",
			  url: ajax_url,
			  data: { 
				img_id: img_id,
				select_field: select_field,
				erize_num: Math.floor((Math.random() * 10000000))
			  }, 
			  dataType: 'html',
			  success: function (data) {
				if (data!='') {
					$("#ajax_dialog").html(data);
					$("#ajax_dialog").dialog('option', 'title', '{_IMAGE_SELECT_TITLE_}');
				} else {
					$("#ajax_dialog").html('{_AJAX_ERROR_}');
					$("#ajax_dialog").dialog('option', 'title', '{_AJAX_ERROR_}');
				}
			  },
			  error: function (result) {
				$("#ajax_dialog").html('{_AJAX_ERROR_}');
				$("#ajax_dialog").dialog('option', 'title', '{_AJAX_ERROR_}');
			  }
			});
		
		}
		function openLink(link='', select_field='') {
			link = (Math.floor(link) == link && $.isNumeric(link)) ? link : 0;
			
			$("#ajax_dialog").dialog ("open");
			$("#ajax_dialog").html('{_LINK_SELECT_LOADING_}');
			$("#ajax_dialog").dialog('option', 'title', '{_LINK_SELECT_TITLE_}');
			
			// B: Ajax Dialog
			var ajax_url = "ajax.link.php";
			$.ajax({
			  type: 'GET', 
			  contentType: "application/x-www-form-urlencoded; charset=utf-8",
			  url: ajax_url,
			  data: { 
				link: link,
				select_field: select_field,
				erize_num: Math.floor((Math.random() * 10000000))
			  }, 
			  dataType: 'html',
			  success: function (data) {
				if (data!='') {
					$("#ajax_dialog").html(data);
					$("#ajax_dialog").dialog('option', 'title', '{_LINK_SELECT_TITLE_}');
				} else {
					$("#ajax_dialog").html('{_AJAX_ERROR_}');
					$("#ajax_dialog").dialog('option', 'title', '{_AJAX_ERROR_}');
				}
			  },
			  error: function (result) {
				$("#ajax_dialog").html('{_AJAX_ERROR_}');
				$("#ajax_dialog").dialog('option', 'title', '{_AJAX_ERROR_}');
			  }
			});
		
		}
		</script>
		
</body>

</html>