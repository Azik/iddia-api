	
	<!-- BEGIN::PAGE LEVEL PLUGINS -->
	<link href="{ROOT}assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
	<link href="{ROOT}assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
	<!-- END::PAGE LEVEL PLUGINS -->
	<script src="{ROOT}assets/global/plugins/jquery.min.js" type="text/javascript"></script>	
	
	<script type="text/javascript">
		function clickAc(act) {
			//alert(act);
			var set = $(".checkboxes");
			var fields = '';
			$(set).each(function () {
				var checked = $(this).is(":checked");
				if (checked) {
					var v = $(this).val();
					fields += (fields != '') ? ',' : '';
					fields += v;
					//alert("aaa:"+v);
				}
			});
			if (fields != '')
				window.location.href = '?{MODULE_QS}=slider&slideeditid=' + act + ':' + fields;
		}

		function click_cancel() {
			window.location.href = '?{MODULE_QS}=slider';
		}

		function doDelete(id) {
			if (confirm('{DELETE_CONFIRM}')) {
				document.getElementById("slide_action").value = 'delete';
				document.getElementById('select_id').value = id;
				document.getElementById('slide_subm').click();
			}
		}
		function doSlideAction(act, id, mess) {
			document.getElementById("slide_action").value = act;
			document.getElementById('select_id').value = id;

			if (act == 'delete') {
				if (confirm(mess)) {
					document.getElementById("slide_subm").click();
				}
			}
			else {
				document.getElementById("slide_subm").click();
			}
		}

		function changeStatus(checked, id) {
			jQuery.ajax({
				success: function (result) {
				},
				data: 'id=' + id + '&checked=' + checked + '&ajax=' + true + '&do_submit=1&byajax=1', //need [0]?
				type: 'post',
				url: 'slider_activate.php'
			});
			//alert(3);
		}

		function redirect(url) {
			window.location.href = url;
		}
	</script>

		<!-- BEGIN::CONTENT -->
		<div class="page-content-wrapper" style="min-width:500px !important;">
			<!-- BEGIN::CONTENT BODY -->
			<div class="page-content">
				<!-- BEGIN::PAGE HEADER-->
				<!-- BEGIN::PAGE BAR -->
				<div class="page-bar">
					<ul class="page-breadcrumb">
						<li>
							<a href="index.php">{HOME}</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<span><a href="index.php?{MODULE_QS}=slider">{TITLE}</a></span>
						</li>
					</ul>
				</div>
				<!-- END::PAGE BAR -->
				<!-- BEGIN::PAGE TITLE-->
				<h3 class="page-title"> {TITLE} </h3>
				<!-- END::PAGE TITLE-->
				<!-- END::PAGE HEADER-->
				
				<!-- BEGIN slide -->
				<div class="row">
					<div class="col-md-12">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-dark">
								<i class="fa fa-tasks"></i>
								<span class="caption-subject bold uppercase">{TITLE}</span>
							</div>
						</div>
					<form action="" method="POST" class="form-horizontal" name="form_cat" id="form_cat">
						<input type="hidden" id="slide_action" name="slide_action" value="">
						<input type="hidden" value="0" id="select_id" name="select_id">
						<input type="submit" id="slide_subm" name="slide_subm" style="display:none;">
						<!-- BEGIN list -->
						<div class="portlet-body">
							<!-- BEGIN perm_add -->
							<div class="clearfix">
								<div class="btn-group">
									<a href="{slide.list.ADD_URL}" class="btn green">
										{slide.list.ADD} &nbsp; <i class="fa fa-plus"></i>
									</a>
								</div>
							</div>
							<!-- END perm_add -->
							<br/>
							<table class="table table-striped table-bordered table-hover" width="100%" id="sample_1">
								<thead>
									<tr>
										<th style="width:0px !important;display:none;"></th>
										<th style="width:12px !important;text-align:center;"><input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes"/></th>
										<th>{slide.list.NAME}</th>
										<!-- BEGIN perm_edit -->
										<th style="width : 100px !important;">{slide.list.EDIT}</th>
										<!-- END perm_edit -->
										<!-- BEGIN perm_del -->
										<th style="width : 100px !important;">{slide.list.DELETE}</th>
										<!-- END perm_del -->
										<th style="width : 100px !important;"></th>
									</tr>
								</thead>
								<tbody>
								<!-- BEGIN items -->
								<tr data-position="{slide.list.items.POSITION}" id="{slide.list.items.ID}">
									<td style="display:none;">{slide.list.items.POSITION}</td>
									<td id="chk_{slide.list.items.ID}" style="width:12px !important;text-align:center;"><input type="checkbox" class="checkboxes"
																  value="{slide.list.items.ID}"/></td>
									<td>{slide.list.items.NAME}</td>
									<!-- BEGIN perm_edit -->
									<td class="option text-center">
										<a href="{slide.list.items.EDIT_URL}" title="{slide.list.EDIT}" class="btn yellow">
											{slide.list.EDIT}
											 &nbsp; <i class="fa fa-pencil"></i>
										</a>
									</td>
									<!-- END perm_edit -->
									<!-- BEGIN perm_del -->
									<td class="option text-center">
										<a href="JavaScript:doDelete({slide.list.items.ID})" title="{slide.list.DELETE}" class="btn red ask"> 
											{slide.list.DELETE}
											 &nbsp; <i class="fa fa-trash"></i>
										</a>
									</td>
									<!-- END perm_del -->
									<td class="option text-center" style="width : 100px !important;">
										<!-- BEGIN perm_edit -->
										<div class="success-toggle-button" style=" margin-top:-2px !important;">
											<input type="checkbox" {slide.list.items.STATUS} onchange="changeStatus(this.checked, {slide.list.items.ID})" class="make-switch switch-large" data-label-icon="fa fa-fullscreen" data-on-text="<i class='fa fa-check'></i>" data-off-text="<i class='fa fa-times'></i>">
										</div>
										<!-- END perm_edit -->
									</td>
								</tr>
								<!-- END items -->
								</tbody>
							</table>
                            <!-- BEGIN hidden -->
                            <input type="hidden" name="sort_order" id="sort_order" value="{slide.list.hidden.VALUE}"/>
                            <!-- END hidden -->
							<br/>
							<!-- BEGIN perm_edit -->
							<table>
								<tfoot>
								<tr>
									<td class="ac"><span class="label label-success"
														 onclick="clickAc('activate')">{slide.list.ACTIVATE}</span></td>
									<td style="width:10px;"></td>
									<td class="ac"><span class="label label-danger"
														 onclick="clickAc('inactivate')">{slide.list.INACTIVATE}</span></td>
									<td></td>
								</tr>
								</tfoot>
							</table>
							<!-- END perm_edit -->
						</div>
						<!-- END list -->
						
						<!-- BEGIN slideedit -->
						<div class="portlet-body" id="panel_editbar">
							<div class="form-group form-md-checkboxes">
								<label class="col-md-3 control-label" for="txt_active">&nbsp;</label>
								<div class="col-md-6">
									<div class="md-checkbox-list">
										<div class="md-checkbox">
											<input type="checkbox" name="txt_active" id="txt_active" value="1" class="md-check"  {slide.slideedit.ACTIVE_CHK} />
											<label for="txt_active">
												<span></span>
												<span class="check"></span>
												<span class="box"></span>{slide.slideedit.ACTIVE}
											</label>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group form-md-line-input">
								<label class="col-md-3 control-label cursor-pointer" for="txt_img" onclick="openPhoto($('#txt_img').val(), 'txt_img')">{slide.slideedit.SLIDE_IMG}</label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="txt_img"
										   id="txt_img" value="{slide.slideedit.TXT_IMG}"
										   placeholder="{slide.slideedit.SLIDE_IMG}">
									<div class="form-control-focus"> </div>
								</div>
							</div>
							<div class="tabbable-line boxless tabbable-reversed">
								<ul class="nav nav-tabs">
									<!-- BEGIN tab -->
									<li{slide.slideedit.tab.CLASS}><a href="#content_{slide.slideedit.tab.LANG}" data-toggle="tab">{slide.slideedit.tab.LANG}</a></li>
									<!-- END tab -->
								</ul>
							</div>
							<div class="tab-content">
							<!-- BEGIN tab -->
							<div class="tab-pane {slide.slideedit.tab.FADE_CLASS}" id="content_{slide.slideedit.tab.LANG}">
								<div class="portlet box green">
								<div class="portlet-title">
									<div class="caption">
										<i class="fa fa-language"></i>{slide.slideedit.tab.LANG} 
									</div>
								</div>
								<div class="portlet-body form">
								<div class="form-group form-md-line-input">
									<label class="col-md-3 control-label" for="txt_name_{slide.slideedit.tab.LANG}">{slide.slideedit.tab.NAME}</label>
									<div class="col-md-8">
										<input type="text" class="form-control" name="txt_name_{slide.slideedit.tab.LANG}"
											   id="txt_name_{slide.slideedit.tab.LANG}" value="{slide.slideedit.tab.NAME_VALUE}"
											   placeholder="{slide.slideedit.tab.NAME}">
										<div class="form-control-focus"> </div>
									</div>
								</div>
								<div class="form-group form-md-line-input">
									<label class="col-md-3 control-label" for="txt_more_txt_{slide.slideedit.tab.LANG}">{slide.slideedit.tab.MORE_TXT}</label>
									<div class="col-md-8">
										<input type="text" class="form-control" name="txt_more_txt_{slide.slideedit.tab.LANG}"
											   id="txt_more_txt_{slide.slideedit.tab.LANG}" value="{slide.slideedit.tab.MORE_TXT_VALUE}"
											   placeholder="{slide.slideedit.tab.MORE_TXT}">
										<div class="form-control-focus"> </div>
									</div>
								</div>
								<div class="form-group form-md-line-input">
									<label class="col-md-3 control-label" for="txt_text_{slide.slideedit.tab.LANG}">{slide.slideedit.tab.TEXT}</label>
									<div class="col-md-8">
										<textarea class="form-control ckeditor" name="txt_text_{slide.slideedit.tab.LANG}"
												  id="txt_text_{slide.slideedit.tab.LANG}" rows="3">{slide.slideedit.tab.TEXT_VALUE}</textarea>
										<div class="form-control-focus"> </div>
									</div>
								</div>
								<div class="form-group form-md-line-input">
									<label class="col-md-3 control-label" for="txt_embed_code_{slide.slideedit.tab.LANG}">{slide.slideedit.tab.EMBED_CODE}</label>
									<div class="col-md-8">
										<textarea class="form-control" name="txt_embed_code_{slide.slideedit.tab.LANG}"
												  id="txt_embed_code_{slide.slideedit.tab.LANG}" rows="3">{slide.slideedit.tab.EMBED_CODE_VALUE}</textarea>
										<div class="form-control-focus"> </div>
									</div>
								</div>
								<div class="form-group form-md-line-input">
									<label class="col-md-3 control-label" for="txt_url_{slide.slideedit.tab.LANG}">{slide.slideedit.tab.URL}</label>
									<div class="col-md-8">
										<input type="text" class="form-control" name="txt_url_{slide.slideedit.tab.LANG}"
											   id="txt_url_{slide.slideedit.tab.LANG}" value="{slide.slideedit.tab.URL_VALUE}"
											   placeholder="{slide.slideedit.tab.URL}">
										<div class="form-control-focus"> </div>
									</div>
								</div>
								<div class="form-actions">
									<div class="row">
										<div class="col-md-offset-3 col-md-9">
											<button type="button" class="btn blue" id="btn_save" name="btn_save"
													onClick="doSlideAction('save', {slide.slideedit.ID},  '');"><i class="fa fa-ok"></i> {slide.slideedit.SAVE}
											</button>
											<button type="reset" class="btn" id="btn_cancel" name="btn_cancel"
													onclick="click_cancel();">{slide.slideedit.CANCEL}</button>
										</div>
									</div>
								</div>
								
								</div>
								</div>
							</div>
							<!-- END tab -->
							</div>
						</div>
						<!-- END slideedit -->
					</form>
					</div>
					</div>
				</div>
				<!-- END slide -->
				
			</div>
			<!-- END::CONTENT BODY -->
		</div>
		<!-- END::CONTENT -->
	</div>
	<!-- END::CONTAINER -->
	
	<!-- BEGIN::CORE PLUGINS -->
	
	<!-- <script src="{ROOT}assets/global/scripts/dt/jquery.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/scripts/dt/jquery-ui.js" type="text/javascript"></script> -->
	<!-- <script src="{ROOT}assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script> -->
	<!-- <script src="{ROOT}assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script> -->
	<!-- END::CORE PLUGINS -->
	
	<!-- BEGIN::THEME GLOBAL SCRIPTS -->
	<script src="{ROOT}assets/global/scripts/app.js" type="text/javascript"></script>
	<!-- END::THEME GLOBAL SCRIPTS -->
	
	<!-- BEGIN::THEME LAYOUT SCRIPTS -->
	<script src="{ROOT}assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
	<script src="{ROOT}assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
	<!-- END::THEME LAYOUT SCRIPTS -->
	
	<!-- BEGIN slide -->
		<!-- BEGIN list -->
	<script src="{ROOT}assets/global/scripts/dt/jquery.js" type="text/javascript"></script>
		<!-- END list -->
	<!-- END slide -->

	<link href="{ROOT}assets/global/plugins/bootstrap-switch/css/bootstrap-switch.css" rel="stylesheet" type="text/css" />
	<script src="{ROOT}assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>

	<script src="{ROOT}assets/global/scripts/dt/jquery-ui.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/scripts/dt/jquery.dataTables.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/scripts/dt/jquery.dataTables.rowReordering.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/scripts/dt/jquery.dataTables.rowGrouping.js" type="text/javascript"></script>
	
	<script type="text/javascript" charset="utf-8">
		$(document).ready(function () {
			$('.group-checkable').click(function(){
				//alert($('.group-checkable').is(":checked"));
				var ch = $('.group-checkable').is(":checked");
				var set = $(".checkboxes");
				$(set).each(function () {
					$(this).prop('checked', ch);
					var v = $(this).val();
					if (ch) {
						$("#chk_" + v).find('span').addClass('checked');
					} else {
						$("#chk_" + v).find('span').removeClass('checked');
					}
				});
			});
			
			var sortInput = jQuery('#sort_order');
			
			var oTable= $("#sample_1").dataTable({
				"bJQueryUI": true,
				"bDestroy": false,
				"bProcessing": false,
				"bSortable": false,
			});
		
			oTable.rowReordering({
				sURL : 'slider_drag.php', 
				fnAlert: function(message) {
					//alert("order"); 
				}
			});
			
		});
	</script>
	
	<script src="{ROOT}assets/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
        