	
	<script>var img_folder = "{ROOT}" + "assets/calendar_img/";</script>
	<script type="text/javascript" src="{ROOT}assets/js/calendarDateInput.js"></script>
	
	<script src="{ROOT}assets/global/plugins/jquery.min.js" type="text/javascript"></script>
	<script type="text/javascript" charset="utf-8">
		$(document).ready(function () {
			$('.group-checkable').click(function(){
				var ch = $('.group-checkable').is(":checked");
				var set = $(".checkboxes");
				$(set).each(function () {
					$(this).prop('checked', ch);
					var v = $(this).val();
					if (ch) {
						$("#chk_" + v).find('span').addClass('checked');
					} else {
						$("#chk_" + v).find('span').removeClass('checked');
					}
				});
			});
		});
	</script>
	<script type="text/javascript">
	function doCatAction(act, id,  mess)
	{		
		document.getElementById("cat_action").value = act;
		document.getElementById('select_id').value = id;
		
		if (act == 'delete') 
		{
			if(confirm(mess))
			{			
				document.getElementById("cat_subm").click();
			}
		}
		else
		{		
			document.getElementById("cat_subm").click();
		}			
	}

	function doTemplatesAction(act, id,  mess)
	{		
		document.getElementById("templates_action").value = act;
		document.getElementById('select_id').value = id;
		
		if (act == 'delete') 
		{
			if(confirm(mess))
			{			
				document.getElementById("templates_subm").click();
			}
		}
		else
		{		
			document.getElementById("templates_subm").click();
		}			
	}

	function doBroadcastAction(act, id,  mess)
	{		
		document.getElementById("broadcast_action").value = act;
		document.getElementById('select_id').value = id;
		
		if (act == 'delete') 
		{
			if(confirm(mess))
			{			
				document.getElementById("broadcast_subm").click();
			}
		}
		else
		{		
			document.getElementById("broadcast_subm").click();
		}			
	}

	function click_cancel(act, id)
	{
		if (act=='templates') {
			window.location.href = '?{MODULE_QS}=subscribe&section=templates';
		} else if (act=='broadcast') {
			window.location.href = '?{MODULE_QS}=subscribe&section=broadcast';
		} else {
			window.location.href = '?{MODULE_QS}=subscribe';
		}
	} 

	<!-- BEGIN templates -->
	<!-- BEGIN list -->
	function clickBlockAc(act) {
		//alert(act);
		var set = $(".checkboxes");
		var fields = '';
		$(set).each(function () {
			var checked = $(this).is(":checked");
			if (checked) {
				var v = $(this).val();
				fields += (fields != '') ? ',' : '';
				fields += v;
				//alert("aaa:"+v);
			}
		});
		if (fields!='') 
			window.location.href = '?{MODULE_QS}=subscribe&section=templates&templatesid='+act+':'+fields;
	}
	<!-- END list -->
	<!-- END templates -->

	<!-- BEGIN broadcast -->
	<!-- BEGIN list -->
	function clickBlockAc(act) {
		//alert(act);
		var set = $(".checkboxes");
		var fields = '';
		$(set).each(function () {
			var checked = $(this).is(":checked");
			if (checked) {
				var v = $(this).val();
				fields += (fields != '') ? ',' : '';
				fields += v;
				//alert("aaa:"+v);
			}
		});
		if (fields!='') 
			window.location.href = '?{MODULE_QS}=subscribe&section=broadcast&broadcastid='+act+':'+fields;
	}
	<!-- END list -->
	<!-- END broadcast -->
		
		
	function redirect(url)
	{	
		window.location.href = url;
	} 
	</script>

		<!-- BEGIN::CONTENT -->
		<div class="page-content-wrapper" style="min-width:850px !important;">
			<!-- BEGIN::CONTENT BODY -->
			<div class="page-content">
				<!-- BEGIN::PAGE HEADER-->
				<!-- BEGIN::PAGE BAR -->
				<div class="page-bar">
					<ul class="page-breadcrumb">
						<li>
							<a href="index.php">{HOME}</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<span><a href="index.php?{MODULE_QS}=subscribe">{TITLE}</a></span>
						</li>
                        <!-- BEGIN navigation -->
                        <li>
                            <i class="fa fa-angle-right"></i>
							<a href="index.php?{MODULE_QS}=subscribe{navigation.URL}">{navigation.TITLE}</a>
						</li>
                        <!-- END navigation -->
					</ul>
				</div>
				<!-- END::PAGE BAR -->
				<!-- BEGIN::PAGE TITLE-->
				<h3 class="page-title"> {TITLE} </h3>
				<!-- END::PAGE TITLE-->
				<!-- END::PAGE HEADER-->
				<div class="row">
					<div class="col-md-12">
                <!-- BEGIN links -->
				<div class="note note-info">
					<a href="{links.URL}" class="btn blue btn-block">{links.TEXT}</a>
				</div>
                <!-- END links -->
				
				
				<!-- BEGIN cat -->
				<form action=""  method="POST" name="form_cat" id="form_cat" >
				<input type="hidden" id="cat_action" name="cat_action" value="">
				<input type="hidden" value="0" id="select_id" name="select_id" >
				<input type="submit" id="cat_subm" name="cat_subm" style="display:none;">
				<div class="portlet box blue">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-envelope"></i>{cat.TITLE}
					</div>
				</div>
				<!-- BEGIN list -->
				<div class="portlet-body">
					<div class="table-responsive">
					<table class="table table-bordered">
						<thead>
						<tr>
							<th>
								{cat.list.SUBSCRIBERS_TOTAL_NM}: {cat.list.SUBSCRIBERS_TOTAL}
							</th>
							<th>
								{cat.list.SUBSCRIBERS_ACTIVE_NM}:  {cat.list.SUBSCRIBERS_ACTIVE} 
							</th>
						</tr>
						</thead>
					</table>
					<table class="table table-striped table-bordered table-hover">
						<thead>
							<tr>
								<th> # </th>
								<th> {cat.list.EMAIL} </th>
								<th> {cat.list.NAME} </th>
								<th> {cat.list.PHONE} </th>
								<th> {cat.list.DATE} </th>
								<th> {cat.list.LANG} </th>
								<!-- BEGIN perm_del -->
								<th></th>
								<!-- END perm_del -->
								<th></th>
							</tr>
						</thead>
						<tbody>
							<!-- BEGIN items -->
							<tr>
								<td></td>
								<td>
									{cat.list.items.EMAIL}
								</td>
								<td>
									{cat.list.items.NAME}
								</td>
								<td>
									{cat.list.items.PHONE}
								</td>
								<td>
									{cat.list.items.DATE}
								</td>
								<td>
									{cat.list.items.LANG}
								</td>
								<!-- BEGIN perm_del -->
								<td style="text-align:center;">
									<a href="JavaScript:doCatAction('delete', {cat.list.items.ID}, '{cat.list.DELETE_CONFIRM}')" class="btn red ask">{cat.list.DELETE}
											 &nbsp; <i class="fa fa-trash"></i></a>
								</td>
								<!-- END perm_del -->
								<td style="text-align:center;">
									<!-- BEGIN active -->
									<a href="{cat.list.items.ACTIVE_URL}" class="btn btn-success">{cat.list.ACTIVE} &nbsp; <i class="fa fa-unlock"></i></a>
									<!-- END active -->
									<!-- BEGIN inactive -->
									<a href="{cat.list.items.INACTIVE_URL}" class="btn btn-danger">{cat.list.INACTIVE} &nbsp; <i class="fa fa-lock"></i></a>
									<!-- END inactive -->
								</td>
							</tr>
							<!-- END items -->
						</tbody>
					</table>
                    
					</div>
				</div>
				<!-- END list -->
				
				</div>
				</form>
				<!-- END cat -->
				
				
				<!-- BEGIN templates -->
				<div class="portlet light bordered">
				<form action="" method="POST" class="form-horizontal" name="form_templates" id="form_templates" onsubmit="return submitForm();">
				<input type="hidden" id="templates_action" name="templates_action" value="">
				<input type="hidden" value="0" id="select_id" name="select_id" >
				<input type="submit" id="templates_subm" name="templates_subm" style="display:none;">
				<!-- BEGIN list -->
				<div class="portlet-title">
					<div class="caption font-dark">
						<i class="fa fa-eye"></i>
						<span class="caption-subject bold uppercase">{templates.list.TITLE}</span>
					</div>
				</div>
				
				<div class="portlet-body">
				<!-- BEGIN perm_add -->
				<div class="clearfix">
					<div class="btn-group">
						<a href="{templates.list.ADD_URL}" class="btn green">
							{templates.list.ADD} &nbsp; <i class="fa fa-plus"></i>
						</a>
					</div>
				</div>
				<!-- END perm_add -->
				<br/>
				<table class="table table-striped table-bordered table-hover" id="sample_1">
				<thead>
					<tr>
						<th style="width:12px !important;text-align:center;"><input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes"/></th>
						<th>{templates.list.NAME}</th>
						<!-- BEGIN perm_edit -->
						<th style="width : 100px !important;text-align : center;">{templates.list.EDIT}</th>
						<!-- END perm_edit -->
						<!-- BEGIN perm_del -->
						<th style="width : 100px !important;text-align : center;">{templates.list.DELETE}</th>
						<!-- END perm_del -->
						<th style="width : 100px !important;"></th>
					</tr>
				</thead>
				<tbody>
				<!-- BEGIN items -->
				<tr data-position="{templates.list.items.POSITION}" id="{templates.list.items.ID}">
					<td style="width:8px;" id="chk_{templates.list.items.ID}"><input type="checkbox" class="checkboxes" value="{templates.list.items.ID}" /></td>
					<td>{templates.list.items.NAME}</td>
					<!-- BEGIN perm_edit -->
					<td class="option text-center"><a href="{templates.list.items.EDIT_URL}" class="btn yellow">{templates.list.EDIT}&nbsp; <i class="fa fa-pencil"></i></a></td>
					<!-- END perm_edit -->
					<!-- BEGIN perm_del -->
					<td class="option text-center"><a href="JavaScript:doTemplatesAction('delete', {templates.list.items.ID}, '{templates.list.DELETE_CONFIRM}')" class="btn red ask">{templates.list.DELETE}&nbsp; <i class="fa fa-trash"></i></a></td>
					<!-- END perm_del -->
					<td class="option">
						<!-- BEGIN active -->
						<a href="{templates.list.items.ACTIVE_URL}" class="btn btn-success">
							{templates.list.ACTIVE}
							&nbsp; <i class="fa fa-unlock"></i></a>
						<!-- END active -->
						<!-- BEGIN inactive -->
						<a href="{templates.list.items.INACTIVE_URL}" class="btn btn-danger">
							{templates.list.INACTIVE}
							&nbsp; <i class="fa fa-lock"></i></a>
						<!-- END inactive -->
					</td>
				</tr>
				<!-- END items -->
				</tbody>
				</table>
				
				<!-- BEGIN hidden -->
				<input type="hidden" name="sort_order" id="sort_order" value="{templates.list.hidden.VALUE}" />
				<!-- END hidden -->
				</ul><br/>
				<!-- BEGIN perm_edit -->
				<tfoot>
					<tr>
						<td class="ac"><span class="label label-success" onclick="clickBlockAc('activate')">{templates.list.ACTIVATE}</span></td>
						<td style="width:10px;"></td>
						<td class="ac"><span class="label label-danger" onclick="clickBlockAc('inactivate')">{templates.list.INACTIVATE}</span></td>
						<td></td>
					</tr>
				</tfoot>
				<!-- END perm_edit -->
				</table>
				</div>
				<!-- END list -->
				
				<!-- BEGIN edit -->
				<div class="portlet-body" id="panel_editbar">
				<div class="form-group form-md-checkboxes">
					<label class="col-md-3 control-label" for="txt_active">&nbsp;</label>
					<div class="col-md-6">
						<div class="md-checkbox-list">
							<div class="md-checkbox">
								<input type="checkbox" name="txt_active" id="txt_active" value="1" class="md-check"  {templates.edit.ACTIVE_CHK} />
								<label for="txt_active">
									<span></span>
									<span class="check"></span>
									<span class="box"></span>{templates.edit.ACTIVE}
								</label>
							</div>
						</div>
					</div>
				</div>							
				<div class="tabbable-line boxless tabbable-reversed">
					<ul class="nav nav-tabs">
						<!-- BEGIN tab -->
						<li{templates.edit.tab.CLASS}><a href="#content_{templates.edit.tab.LANG}" data-toggle="tab">{templates.edit.tab.LANG}</a></li>
						<!-- END tab -->
					</ul>
				</div>
				<div class="tab-content">
					<!-- BEGIN tab -->
					<div class="tab-pane {templates.edit.tab.FADE_CLASS}" id="content_{templates.edit.tab.LANG}">
						<div class="portlet box green">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-language"></i>{templates.edit.tab.LANG} 
							</div>
						</div>
						<div class="portlet-body form">
						<div class="form-group form-md-line-input">
							<label class="col-md-3 control-label" for="txt_name_{templates.edit.tab.LANG}">{templates.edit.tab.NAME}</label>
							<div class="col-md-8">
								<input type="text" class="form-control" name="txt_name_{templates.edit.tab.LANG}"
									   id="txt_name_{templates.edit.tab.LANG}" value="{templates.edit.tab.NAME_VALUE}"
									   placeholder="{templates.edit.tab.NAME}">
								<div class="form-control-focus"> </div>
							</div>
						</div>
						<div class="form-group form-md-line-input">
							<label class="col-md-3 control-label" for="txt_template_{templates.edit.tab.LANG}">{templates.edit.tab.TEMPLATE}<br/>[TEXT]</label>
							<div class="col-md-8">
								<textarea class="form-control" name="txt_template_{templates.edit.tab.LANG}"
										  id="txt_template_{templates.edit.tab.LANG}" rows="3">{templates.edit.tab.TEMPLATE_VALUE}</textarea>
								<div class="form-control-focus"> </div>
							</div>
						</div>
						<div class="form-actions">
							<div class="row">
								<div class="col-md-offset-3 col-md-9">
									<button type="button" class="btn blue" id="btn_save" name="btn_save"
											onClick="doTemplatesAction('save', {templates.edit.ID},  '');"><i class="fa fa-ok"></i> {templates.edit.SAVE}
									</button>
									<button type="reset" class="btn" id="btn_cancel" name="btn_cancel"
											onclick="click_cancel('templates', {templates.edit.ID});">{templates.edit.CANCEL}</button>
								</div>
							</div>
						</div>
						</div>
						</div>
					</div>
					<!-- END tab -->
				</div>
				
				</div>
				<!-- END edit -->
				
				</form>
				</div>
				<!-- END templates -->
				
				
				
				
				<!-- BEGIN broadcast -->
				<form action="" method="POST" class="form-horizontal" name="form_broadcast" id="form_broadcast" onsubmit="return submitForm();">
				<input type="hidden" id="broadcast_action" name="broadcast_action" value="">
				<input type="hidden" value="0" id="select_id" name="select_id" >
				<input type="submit" id="broadcast_subm" name="broadcast_subm" style="display:none;">
				<!-- BEGIN list -->
				<div class="portlet light bordered">
				<div class="portlet-title">
					<div class="caption font-dark">
						<i class="fa fa-eye"></i>
						<span class="caption-subject bold uppercase">{broadcast.list.TITLE}</span>
					</div>
				</div>
				<div class="portlet-body">
				<!-- BEGIN perm_add -->
				<div class="clearfix">
					<div class="btn-group">
						<a href="{broadcast.list.ADD_URL}" class="btn green">
							{broadcast.list.ADD} &nbsp; <i class="fa fa-plus"></i>
						</a>
					</div>
				</div>
				<!-- END perm_add -->
				<br/>
				<table class="table table-striped table-bordered table-hover" id="sample_1">
				<thead>
					<tr>
						<th style="width:8px;"><input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" /></th>
						<th>{broadcast.list.NAME}</th>
						<!-- BEGIN perm_edit -->
						<th style="width : 100px !important;text-align : center;">{broadcast.list.EDIT}</th>
						<!-- END perm_edit -->
						<!-- BEGIN perm_del -->
						<th style="width : 100px !important;text-align : center;">{broadcast.list.DELETE}</th>
						<!-- END perm_del -->
						<th style="width : 120px !important;">{broadcast.list.MAIL_STATS}</th>
						<th style="width : 100px !important;"></th>
					</tr>
				</thead>
				<tbody>
				<!-- BEGIN items -->
				<tr id="{broadcast.list.items.ID}">
					<td style="width:8px;" id="chk_{broadcast.list.items.ID}"><input type="checkbox" class="checkboxes" value="{broadcast.list.items.ID}" /></td>
					<td>{broadcast.list.items.NAME}</td>
					<!-- BEGIN perm_edit -->
					<td class="option text-center"><a href="{broadcast.list.items.EDIT_URL}" class="btn yellow">{broadcast.list.EDIT}&nbsp; <i class="fa fa-pencil"></i></a></td>
					<!-- END perm_edit -->
					<!-- BEGIN perm_del -->
					<td class="option text-center"><a href="JavaScript:doBroadcastAction('delete', {broadcast.list.items.ID}, '{broadcast.list.DELETE_CONFIRM}')" class="btn red ask">{broadcast.list.DELETE}&nbsp; <i class="fa fa-trash"></i></a></td>
					<!-- END perm_del -->
					<td class="option text-center" style="width : 120px !important;">
						{broadcast.list.items.MAIL_STATS}
					</td>
					<td class="option">
						<!-- BEGIN active -->
						<a href="{broadcast.list.items.ACTIVE_URL}" class="btn btn-success">
						{broadcast.list.ACTIVE}
						&nbsp; <i class="fa fa-unlock"></i></a>
						<!-- END active -->
						<!-- BEGIN inactive -->
						<a href="{broadcast.list.items.INACTIVE_URL}" class="btn btn-danger">
						{broadcast.list.INACTIVE}
						&nbsp; <i class="fa fa-lock"></i></a>
						<!-- END inactive -->
					</td>
				</tr>
				<!-- END items -->
				</tbody>
				</table>
				<!-- BEGIN hidden -->
				<input type="hidden" name="sort_order" id="sort_order" value="{broadcast.list.hidden.VALUE}" />
				<!-- END hidden -->
				
				<!-- BEGIN perm_edit -->
				<table >
				<tfoot>
					<tr>
						<td class="ac"><span class="label label-success" onclick="clickBlockAc('activate')">{broadcast.list.ACTIVATE}</span></td>
						<td style="width:10px;"></td>
						<td class="ac"><span class="label label-danger" onclick="clickBlockAc('inactivate')">{broadcast.list.INACTIVATE}</span></td>
						<td></td>
					</tr>
				</tfoot>
				</table>
				<!-- END perm_edit -->
				</div>
				<!-- END list -->
				
				<!-- BEGIN edit -->
				<div class="portlet-body" id="panel_editbar">
				<div class="form-group form-md-line-input">
					<label class="col-md-3 control-label" for="txt_template">{broadcast.edit.TEMPLATE}</label>
					<div class="col-md-6">
						<select class="form-control" name="txt_template" id="txt_template">
							<!-- BEGIN templates -->
							<option value="{broadcast.edit.templates.VALUE}" {broadcast.edit.templates.SELECTED}>{broadcast.edit.templates.TEXT}</option>
							<!-- END templates -->
						</select>
						<div class="form-control-focus"> </div>
					</div>
				</div>
				<div class="form-group form-md-checkboxes">
					<label class="col-md-3 control-label" for="txt_active">&nbsp;</label>
					<div class="col-md-6">
						<div class="md-checkbox-list">
							<div class="md-checkbox">
								<input type="checkbox" name="txt_active" id="txt_active" value="1" class="md-check" {broadcast.edit.ACTIVE_CHK}/>
								<label for="txt_active">
									<span></span>
									<span class="check"></span>
									<span class="box"></span>{broadcast.edit.ACTIVE}
								</label>
							</div>
						</div>
					</div>
				</div>
				
				<div class="form-group form-md-line-input">
					<label class="col-md-3 control-label" for="startdate">{broadcast.edit.STARTDATE}</label>
					<div class="col-md-8">
						<script>DateInput('startdate', true, 'DD/MM/YYYY', '{broadcast.edit.STARTDATE_VALUE}')</script>
						<div class="form-control-focus"> </div>
					</div>
				</div>
				<div class="tabbable-line boxless tabbable-reversed">
					<ul class="nav nav-tabs">
						<!-- BEGIN tab -->
						<li{broadcast.edit.tab.CLASS}><a href="#content_{broadcast.edit.tab.LANG}" data-toggle="tab">{broadcast.edit.tab.LANG}</a></li>
						<!-- END tab -->
					</ul>
				</div>
								<div class="tab-content">
					<!-- BEGIN tab -->
					<div class="tab-pane {broadcast.edit.tab.FADE_CLASS}" id="content_{broadcast.edit.tab.LANG}">
						<div class="portlet box green">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-language"></i>{broadcast.edit.tab.LANG}
							</div>
						</div>
						<div class="portlet-body form">
						<div class="form-group form-md-line-input">
							<label class="col-md-3 control-label" for="txt_template_{broadcast.edit.tab.LANG}">{broadcast.edit.tab.TEXT}</label>
							<div class="col-md-8">
								<textarea class="form-control" name="txt_template_{broadcast.edit.tab.LANG}"
										  id="txt_template_{broadcast.edit.tab.LANG}" rows="8">{broadcast.edit.tab.TEXT_VALUE}</textarea>
								<div class="form-control-focus"> </div>
							</div>
						</div>
						<div class="form-actions">
							<div class="row">
								<div class="col-md-offset-3 col-md-9">
									<button type="button" class="btn blue" id="btn_save" name="btn_save"
											onclick="doBroadcastAction('save', {broadcast.edit.ID},  '');"><i class="fa fa-ok"></i> {broadcast.edit.SAVE}
									</button>
									<button type="reset" class="btn" id="btn_cancel" name="btn_cancel"
											onclick="click_cancel('broadcast', {broadcast.edit.ID});">{broadcast.edit.CANCEL}</button>
								</div>
							</div>
						</div>
						</div>
						</div>
					</div>
					<!-- END tab -->
				</div>
				</div>
				<!-- END edit -->
				
				</form>
				<!-- END broadcast -->
				
					</div>
				</div>
				
			</div>
			<!-- END::CONTENT BODY -->
		</div>
		<!-- END::CONTENT -->
	</div>
	<!-- END::CONTAINER -->