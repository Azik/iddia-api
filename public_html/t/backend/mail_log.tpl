	<script src="{ROOT}assets/global/plugins/jquery.min.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>

	<script type="text/javascript" charset="utf-8">
		function clickAc(act) {
			//alert(act);
			var set = $(".checkboxes");
			var fields = '';
			$(set).each(function () {
				var checked = $(this).is(":checked");
				if (checked) {
					var v = $(this).val();
					fields += (fields != '') ? ',' : '';
					fields += v;
					//alert("aaa:"+v);
				}
			});
			if (fields != '')
				window.location.href = '?{MODULE_QS}=mail_log&cateditid=' + act + ':' + fields;
		}
		function clickAcCategories(act, catid) {
			//alert(act);
			var set = $(".checkboxes");
			var fields = '';
			$(set).each(function () {
				var checked = $(this).is(":checked");
				if (checked) {
					var v = $(this).val();
					fields += (fields != '') ? ',' : '';
					fields += v;
					//alert("aaa:"+v);
				}
			});
			if (fields != '')
				window.location.href = '?{MODULE_QS}=mail_log&catid=' + catid + '&categoriesid=' + act + ':' + fields;
		}
		function click_cancel(act, id) {
			if (act == 'categories') {
				window.location.href = '?{MODULE_QS}=mail_log&catid=' + id;
			} else {
				window.location.href = '?{MODULE_QS}=mail_log';
			}
		}

		function doCatDelete(subid, id) {
			if (confirm('{CAT_CONFIRM}')) {
				document.getElementById("cat_action").value = 'cat_delete';
				document.getElementById('select_id').value = id;
				document.getElementById('cat_subm').click();
			}
		}
		
		function doCatAction(act, id, mess) {
			document.getElementById("cat_action").value = act;
			document.getElementById('select_id').value = id;

			if (act == 'delete') {
				if (confirm(mess)) {
					document.getElementById("cat_subm").click();
				}
			}
			else {
				document.getElementById("cat_subm").click();
			}
		}

		function doCategoriesDelete(id) {
			if (confirm('{CATEGORIES_CONFIRM}')) {
				document.getElementById("categories_action").value = 'delete';
				document.getElementById('select_id').value = id;
				document.getElementById('categories_subm').click();
			}
		}
		
		function doCategoriesAction(act, id, mess) {
			document.getElementById("categories_action").value = act;
			document.getElementById('select_id').value = id;

			if (act == 'delete') {
				if (confirm(mess)) {
					document.getElementById("categories_subm").click();
				}
			}
			else {
				document.getElementById("categories_subm").click();
			}
		}

		function doOptionsAction(options_action) {
			document.getElementById("options_action").value = options_action;
			document.getElementById("options_subm").click();
		}
		
		function redirect(url) {
			window.location.href = url;
		}
	</script>

		<!-- BEGIN::CONTENT -->
		<div class="page-content-wrapper" style="min-width:600px !important;">
			<!-- BEGIN::CONTENT BODY -->
			<div class="page-content">
				<!-- BEGIN::PAGE HEADER-->
				<!-- BEGIN::PAGE TITLE-->
				<h3 class="page-title"> {TITLE} </h3>
				<!-- END::PAGE TITLE-->
				<!-- BEGIN::PAGE BAR -->
				<div class="page-bar">
					<ul class="page-breadcrumb">
						<li>
							<a href="index.php">{HOME}</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<span><a href="index.php?{MODULE_QS}=mail_log">{TITLE}</a></span>
						</li>
					</ul>
				</div>
				<!-- END::PAGE BAR -->
				<!-- END::PAGE HEADER-->
				
				<!-- BEGIN message_sends -->
				<div class="row">
					<div class="col-md-12">
						<div class="portlet light bordered">
							<div class="portlet-title">
								<div class="caption font-dark">
									<i class="fa fa-calendar"></i>
									<span class="caption-subject bold uppercase">{TITLE}</span>
								</div>
							</div>
							<form action="" method="POST" class="form-horizontal" name="form_cat" id="form_cat">
								<input type="hidden" id="cat_action" name="cat_action" value="">
								<input type="hidden" value="0" id="select_id" name="select_id">
								<input type="submit" id="cat_subm" name="cat_subm" style="display:none;">
							
					<!-- BEGIN list -->
					<div class="portlet-body">
						<br/>
						<div id="dialog">
						</div>
						
						<table class="table table-striped table-bordered table-hover" id="list_users">
							<thead>
							<tr>
								<th style="width:25px;"><input type="checkbox" class="group-checkable"
															   data-set="#list_users .checkboxes"/></th>
								<th style="width : 150px !important;">{message_sends.list.EMAIL}</th>
								<th style="width : 250px !important;">{message_sends.list.EMAIL_SUBJECT}</th>
								<th style="width : 250px !important;">{message_sends.list.LOG_TIME}</th>
								<th style="width : 100px !important;"></th>
							</tr>
							</thead>
							<tbody>
							<!-- BEGIN items -->
							<tr class="odd gradeX">
								<td id="chk_{message_sends.list.items.ID}">
									<!-- BEGIN check -->
									<input type="checkbox" class="checkboxes" value="{message_sends.list.items.ID}"/>
									<!-- END check -->
								</td>
								<td><a href="javascript:;" onclick="openDialog({message_sends.list.items.ID})">{message_sends.list.items.EMAIL}</a></td>
								<td><a href="javascript:;" onclick="openDialog({message_sends.list.items.ID})">{message_sends.list.items.EMAIL_SUBJECT}</a></td>
								<td>{message_sends.list.items.LOG_TIME}</td>
								<td>
									<!-- BEGIN do_del -->
									<a href="JavaScript:doDelete({users.list.items.ID})" title="{users.list.DELETE}"
									    class="btn red ask"> {users.list.DELETE}&nbsp; <i class="fa fa-trash"></i></a>
									<!-- END do_del -->
									<!-- BEGIN active -->
									<a href="{users.list.items.ACTIVE_URL}" class="btn btn-success">
										{users.list.ACTIVE}&nbsp; <i class="fa fa-unlock"></i></a>
									<!-- END active -->
									<!-- BEGIN inactive -->
									<a href="{users.list.items.INACTIVE_URL}" class="btn btn-danger">
										{users.list.INACTIVE}&nbsp; <i class="fa fa-lock"></i></a>
									<!-- END inactive -->
								</td>
							</tr>
							<!-- END items -->
							</tbody>
						</table>
					</div>
					<!-- END list -->

							
							</form>
						</div>
					</div>
				</div>
				<!-- END message_sends -->
				
			</div>
			<!-- END::CONTENT BODY -->
		</div>
		<!-- END::CONTENT -->
	</div>
	<!-- END::CONTAINER -->
	
	
	
	
	<!-- BEGIN message_sends -->
	<!-- BEGIN::PAGE LEVEL PLUGINS -->
	<script src="{ROOT}assets/global/scripts/dt/jquery.js" type="text/javascript"></script>
	 
	<!-- BEGIN edit -->
	<script src="{ROOT}assets/global/plugins/jquery.min.js" type="text/javascript"></script>
	<!-- END edit -->
	<script src="{ROOT}assets/global/scripts/dt/jquery-ui.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/scripts/dt/jquery.dataTables.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/scripts/dt/jquery.dataTables.rowReordering.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/scripts/dt/jquery.dataTables.rowGrouping.js" type="text/javascript"></script>
	
	
	<!-- END::PAGE LEVEL PLUGINS -->
	<script type="text/javascript" charset="utf-8">
		$(document).ready(function () {
			$('.group-checkable').click(function(){
				//alert($('.group-checkable').is(":checked"));
				var ch = $('.group-checkable').is(":checked");
				var set = $(".checkboxes");
				$(set).each(function () {
					$(this).prop('checked', ch);
					var v = $(this).val();
					if (ch) {
						$("#chk_" + v).find('span').addClass('checked');
					} else {
						$("#chk_" + v).find('span').removeClass('checked');
					}
				});
			});
		});
		
		
		
		function openDialog(mailid) {
			if (mailid>0) {
				$("#dialog").dialog ({
				  autoOpen : false,
				  height: 500,
				  width: 700,
				  modal: true,
				  resizable: true,
				  dialogClass: 'no-close success-dialog'
				});
				$("#dialog").dialog ("open");
				//$("#dialog").dialog(opt).dialog("open");

				$("#dialog").html('Loading...');
				$("#dialog").dialog('option', 'title', 'Loading...');

				// B: Ajax Dialog
				var sebet_file = "mail_view.php";
				$.ajax({
				  type: 'GET', 
				  contentType: "application/x-www-form-urlencoded; charset=utf-8",
				  url: sebet_file,
				  data: { 
					mailid: mailid,
					erize_num: Math.floor((Math.random() * 10000000))
				  }, 
				  dataType: 'html',
				  success: function (data) {
					if (data!='') {
						$("#dialog").html(data);
						$("#dialog").dialog('option', 'title', 'Details');
					} else {
						$("#dialog").html('Error');
						$("#dialog").dialog('option', 'title', 'Error');
					}
				  },
				  error: function (result) {
					$("#dialog").html('Error');
					$("#dialog").dialog('option', 'title', 'Error');
				  }
				});
			}
		}
	</script>
	<!-- END message_sends -->
	
	
	<script src="{ROOT}assets/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
    