	
	<script language="javascript">
		function addLang(btn) {
			if (document.getElementById) {
				var lg = document.getElementById('txt_lang').value;
				if (lg.length < 2)
					return;
				document.getElementById('txt_lang').value = '';

				tr = btn;
				while (tr.tagName != 'TR') tr = tr.parentNode;
				tr = tr.previousSibling.previousSibling;
				var newTr = tr.parentNode.insertBefore(tr.cloneNode(true), tr.nextSibling);

				thisChilds = newTr.getElementsByTagName('td');

				for (var i = 0; i < thisChilds.length; i++) {
					if (thisChilds[i].className == 'lang') {
						thisChilds[i].innerHTML = lg;
						break;
					}
				}

				var count = document.getElementsByName('txt_langs[]').length;
				document.getElementsByName('txt_langs[]')[count - 1].value = lg;
				document.getElementsByName('txt_showlangs[]')[count - 1].value = lg;
				document.getElementsByName('txt_mainlang')[count - 1].value = lg;

				document.getElementsByName('txt_showlangs[]')[count - 1].checked = false;
				document.getElementsByName('txt_mainlang')[count - 1].checked = false;

				checkLast();
			}
		}

		function checkLang() {
			var count = document.getElementsByName('txt_langs[]').length;

			var found = false;
			for (var i = 0; i < count; i++) {
				if (document.getElementsByName('txt_mainlang')[i].checked) {
					found = true;
					break;
				}
			}

			if (!found)
				document.getElementsByName('txt_mainlang')[0].checked = true;
		}

		function removeLang(btn) {
			if (document.getElementById) {
				tr = btn;
				while (tr.tagName != 'TR') tr = tr.parentNode;
				tr.parentNode.removeChild(tr);
				checkLast();
				checkLang();
			}
		}

		function checkLast() {
			btns = document.getElementsByName('minus');
			for (i = 0; i < btns.length; i++) {
				btns[i].disabled = (btns.length == 1) ? true : false;
			}
		}
	</script>
	<script language="javascript" event="onload" for="window">
		checkLast();
	</script>

	<script>
		function click_cancel() {
			window.location.href = '?{MODULE_QS}=options_site';
		}
		function doAction(act, id, mess) {
			document.getElementById("action_id").value = id;
			document.getElementById("action").value = act;

			if (act == 'delete') {
				if (confirm(mess)) {
					document.getElementById("action_submit").click();
				}
			}
			else {
				document.getElementById("action_submit").click();
			}
		}

		function redirect(url) {
			window.location.href = url;
		}
	</script>

		<!-- BEGIN::CONTENT -->
		<div class="page-content-wrapper">
			<!-- BEGIN::CONTENT BODY -->
			<div class="page-content">
				<!-- BEGIN::PAGE HEADER-->
				<!-- BEGIN::PAGE TITLE-->
				<h3 class="page-title"> {TITLE} </h3>
				<!-- END::PAGE TITLE-->
				<!-- BEGIN::PAGE BAR -->
				<div class="page-bar">
					<ul class="page-breadcrumb">
						<li>
							<a href="index.php">{HOME}</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<span><a href="index.php?{MODULE_QS}=options_site">{TITLE}</a></span>
						</li>
						<!-- BEGIN sub_link -->
						<li>
							<i class="fa fa-angle-right"></i>
							<a href="index.php?{MODULE_QS}=options_site&mode={sub_link.MODE}">{sub_link.TEXT}</a>
						</li>
						<!-- END sub_link -->
					</ul>
				</div>
				<!-- END::PAGE BAR -->
				<!-- END::PAGE HEADER-->
				
				
		<!-- BEGIN options -->
		<div class="row">
			<div class="col-md-12">
			<form name="form_action" id="form_action" class="form-horizontal" method="post" action="">
				<input type="hidden" id="action" name="action" value="0">
				<input type="hidden" id="action_id" name="action_id" value="0">
				<input type="submit" id="action_submit" name="action_submit" style="display:none;">

				<!-- BEGIN default -->
				<div class="portlet light bordered">
					<div class="portlet-title">
						<div class="caption font-dark">
							<i class="fa fa-wrench"></i>
							<span class="caption-subject bold uppercase">{options.default.TITLE}</span>
						</div>
					</div>
					<div class="note note-info">
						<a href="{options.URL}&mode=siteoptions" class="btn blue btn-block">{options.default.SITEOPTIONS}</a>
					</div>
				</div>
				<!-- END default -->

				<!-- BEGIN siteoptions -->
				<div class="portlet light bordered">
					<div class="portlet-title">
						<div class="caption font-dark">
							<i class="fa fa-wrench"></i>
							<span class="caption-subject bold uppercase">{options.siteoptions.TITLE}</span>
						</div>
					</div>
					<div class="portlet-body" id="panel_editbar">
						<div class="form-group form-md-line-input">
							<label class="col-md-3 control-label" for="txt_charset">{options.siteoptions.CHARSET}</label>
							<div class="col-md-8">
								<input type="text" class="form-control{DISABLED_CSS}" name="txt_charset"
									   id="txt_charset" value="{options.siteoptions.CHARSET_VALUE}"
									   placeholder="{options.siteoptions.CHARSET}" {READONLY}>
								<div class="form-control-focus"> </div>
							</div>
						</div>
						<div class="form-group form-md-checkboxes">
							<label class="col-md-3 control-label" for="txt_slider">&nbsp;</label>
							<div class="col-md-6">
								<div class="md-checkbox-list">
									<div class="md-checkbox">
										<input type="checkbox" name="txt_slider" id="txt_slider" value="1" class="md-check{DISABLED_CSS}" {options.siteoptions.SLIDER_CHECK}  {READONLY}/>
										<label for="txt_slider">
											<span></span>
											<span class="check"></span>
											<span class="box"></span>{options.siteoptions.SLIDER}
										</label>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group form-md-checkboxes">
							<label class="col-md-3 control-label" for="txt_parallax">&nbsp;</label>
							<div class="col-md-6">
								<div class="md-checkbox-list">
									<div class="md-checkbox">
										<input type="checkbox" name="txt_parallax" id="txt_parallax" value="1" class="md-check{DISABLED_CSS}" {options.siteoptions.PARALLAX_CHECK}  {READONLY}/>
										<label for="txt_parallax">
											<span></span>
											<span class="check"></span>
											<span class="box"></span>{options.siteoptions.PARALLAX}
										</label>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group form-md-line-input">
							<div class="col-md-12">
								<strong>{options.siteoptions.META}</strong>
							</div>
						</div>
						<div class="tabbable-line boxless tabbable-reversed">
							<ul class="nav nav-tabs">
								<!-- BEGIN tab -->
								<li{options.siteoptions.tab.CLASS}><a href="#content_{options.siteoptions.tab.LANG}" data-toggle="tab">{options.siteoptions.tab.LANG}</a></li>
								<!-- END tab -->
							</ul>
						</div>
						<div class="tab-content">
						<!-- BEGIN tab -->
						<div class="tab-pane {options.siteoptions.tab.FADE_CLASS}" id="content_{options.siteoptions.tab.LANG}">
							<div class="portlet box green">
							<div class="portlet-title">
								<div class="caption">
									<i class="fa fa-language"></i>{options.siteoptions.tab.LANG} 
								</div>
							</div>
							<div class="portlet-body form">
								<div class="form-group form-md-line-input">
									<label class="col-md-3 control-label" for="txt_site_name_{options.siteoptions.tab.LANG}">{options.siteoptions.SITE_NAME}</label>
									<div class="col-md-8">
										<input type="text" class="form-control{DISABLED_CSS}" name="txt_site_name_{options.siteoptions.tab.LANG}"
											   id="txt_site_name_{options.siteoptions.tab.LANG}" value="{options.siteoptions.tab.SITE_NAME_VALUE}"
											   placeholder="{options.siteoptions.SITE_NAME}" {READONLY}>
										<div class="form-control-focus"> </div>
									</div>
								</div>
								<div class="form-group form-md-line-input">
									<label class="col-md-3 control-label" for="txt_site_title_{options.siteoptions.tab.LANG}">{options.siteoptions.SITE_TITLE}</label>
									<div class="col-md-8">
										<input type="text" class="form-control{DISABLED_CSS}" name="txt_site_title_{options.siteoptions.tab.LANG}"
											   id="txt_site_title_{options.siteoptions.tab.LANG}" value="{options.siteoptions.tab.SITE_TITLE_VALUE}"
											   placeholder="{options.siteoptions.SITE_TITLE}" {READONLY}>
										<div class="form-control-focus"> </div>
									</div>
								</div>
								<div class="form-group form-md-line-input">
									<label class="col-md-3 control-label" for="txt_meta_keywords_{options.siteoptions.tab.LANG}">{options.siteoptions.META_KEYWORDS}</label>
									<div class="col-md-8">
										<input type="text" class="form-control{DISABLED_CSS}" name="txt_meta_keywords_{options.siteoptions.tab.LANG}"
											   id="txt_meta_keywords_{options.siteoptions.tab.LANG}" value="{options.siteoptions.tab.META_KEYWORDS_VALUE}"
											   placeholder="{options.siteoptions.META_KEYWORDS}" {READONLY}>
										<div class="form-control-focus"> </div>
									</div>
								</div>
								<div class="form-group form-md-line-input">
									<label class="col-md-3 control-label" for="txt_meta_description_{options.siteoptions.tab.LANG}">{options.siteoptions.META_DESCRIPTION}</label>
									<div class="col-md-8">
										<input type="text" class="form-control{DISABLED_CSS}" name="txt_meta_description_{options.siteoptions.tab.LANG}"
											   id="txt_meta_description_{options.siteoptions.tab.LANG}" value="{options.siteoptions.tab.META_DESCRIPTION_VALUE}"
											   placeholder="{options.siteoptions.META_DESCRIPTION}" {READONLY}>
										<div class="form-control-focus"> </div>
									</div>
								</div>
								<div class="form-group form-md-line-input">
									<label class="col-md-3 control-label" for="txt_meta_email_{options.siteoptions.tab.LANG}">{options.siteoptions.META_EMAIL}</label>
									<div class="col-md-8">
										<input type="text" class="form-control{DISABLED_CSS}" name="txt_meta_email_{options.siteoptions.tab.LANG}"
											   id="txt_meta_email_{options.siteoptions.tab.LANG}" value="{options.siteoptions.tab.META_EMAIL_VALUE}"
											   placeholder="{options.siteoptions.META_EMAIL}" {READONLY}>
										<div class="form-control-focus"> </div>
									</div>
								</div>
								<div class="form-group form-md-line-input">
									<label class="col-md-3 control-label" for="txt_meta_author_{options.siteoptions.tab.LANG}">{options.siteoptions.META_AUTHOR}</label>
									<div class="col-md-8">
										<input type="text" class="form-control{DISABLED_CSS}" name="txt_meta_author_{options.siteoptions.tab.LANG}"
											   id="txt_meta_author_{options.siteoptions.tab.LANG}" value="{options.siteoptions.tab.META_AUTHOR_VALUE}"
											   placeholder="{options.siteoptions.META_AUTHOR}" {READONLY}>
										<div class="form-control-focus"> </div>
									</div>
								</div>
								<div class="form-group form-md-line-input">
									<label class="col-md-3 control-label" for="txt_site_copyright_{options.siteoptions.tab.LANG}">{options.siteoptions.SITE_COPYRIGHT}</label>
									<div class="col-md-8">
										<input type="text" class="form-control{DISABLED_CSS}" name="txt_site_copyright_{options.siteoptions.tab.LANG}"
											   id="txt_site_copyright_{options.siteoptions.tab.LANG}" value="{options.siteoptions.tab.SITE_COPYRIGHT_VALUE}"
											   placeholder="{options.siteoptions.SITE_COPYRIGHT}" {READONLY}>
										<div class="form-control-focus"> </div><br/>
									</div>
								</div>
							</div>
							</div>
						</div>
						<!-- END tab -->
						</div>
						<div class="form-group form-md-line-input">
							<div class="col-md-12">
								<strong>{options.siteoptions.SITE_LANG}</strong>
							</div>
						</div>
						<div class="portlet-body">
						<table class="table table-striped table-bordered table-hover" width="100%" id="sample_1">
						<thead>
							<tr>
								<th style="width:100px;">{options.siteoptions.LANG_MAIN}</th>
								<th style="width:100px;">{options.siteoptions.LANG_SHOW}</th>
								<th style="width:100px;">{options.siteoptions.LANG_LANG}</th>
								<th style="width:100px;"></th>
								<th></th>
							</tr>
						</thead>
						<tbody>
						<!-- BEGIN langs -->
						<tr>
							<td>
								<label>
									<input type="radio" name="txt_mainlang" {options.siteoptions.langs.MAIN} value="{options.siteoptions.langs.LANG}" {READONLY} {DISABLED_TYPE}>
								</label>
							</td>
							<td>
								<label for="txt_showlangs_{options.siteoptions.langs.LANG}">
									<input type="checkbox" name="txt_showlangs[]" id="txt_showlangs_{options.siteoptions.langs.LANG}" value="{options.siteoptions.langs.LANG}" {options.siteoptions.langs.SHOW} {READONLY}{DISABLED_TYPE}>
								</label>
								<input type="hidden" name="txt_langs[]" id="txt_langs[]" value="{options.siteoptions.langs.LANG}">
							</td>
							<td class="lang">
								<label for="txt_showlangs_{options.siteoptions.langs.LANG}">
								{options.siteoptions.langs.LANG}
								</label>
							</td>
							<td><INPUT id="minus" type="button" class="btn red{DISABLED_CSS}"{DISABLED_TYPE} value=" - " name="minus" onClick="removeLang(this);" {READONLY}></td>
						</tr>
						<!-- END langs -->
						 <tr>
							<td></td>
							<td></td>
							<td align="right" ><br/>
								<input type="text" name="txt_lang" id="txt_lang" maxlength="2" style="width:40px;"{READONLY}{DISABLED_TYPE}>
							</td>
							<td><br/>
								<input id="plus" type="button" class="btn blue{DISABLED_CSS}" value=" + " name="plus" onclick="addLang(this);" {READONLY}{DISABLED_TYPE}>
							</td>
						</tr>
						</tbody>
						</table>
						</div>
						<div class="form-group form-md-line-input">
							<div class="col-md-12">
								<strong>{options.siteoptions.ADMIN_LANG}</strong>
							</div>
						</div>
						<div class="portlet-body">
						<table class="table table-striped table-bordered table-hover" width="100%" id="sample_1">
						<thead>
							<tr>
								<th style="width:100px;">{options.siteoptions.ADMIN_LANG_MAIN}</th>
								<th style="width:100px;">{options.siteoptions.LANG_LANG}</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
						<!-- BEGIN langs_admin -->
						<tr>
							<td><label for="txt_adminlang_{options.siteoptions.langs_admin.LANG}"><input type="radio" name="txt_adminlang" {options.siteoptions.langs_admin.ADMIN_MAIN_LANG_CHECK}
									   id="txt_adminlang_{options.siteoptions.langs_admin.LANG}" value="{options.siteoptions.langs_admin.LANG}"{READONLY}{DISABLED_TYPE}></label></td>
							<td class="lang"><label for="txt_adminlang_{options.siteoptions.langs_admin.LANG}">{options.siteoptions.langs_admin.LANG}</label></td>
						</tr>
						<!-- END langs_admin -->
						</tbody>
						</table>
						</div>
						<!-- BEGIN alert -->
						<div class="form-group form-md-line-input">
							<div class="col-md-12">
								<font color="red">{options.siteoptions.alert.MESSAGE}</font>
							</div>
						</div>
						<!-- END alert -->
						<div class="form-actions">
							<div class="row">
								<div class="col-md-offset-3 col-md-9">
									<button type="button" class="btn blue{DISABLED_CSS}" id="btn_save" name="btn_save"
											onClick="doAction('save_siteoptions', 0,  '');"{READONLY}{DISABLED_TYPE}><i class="fa fa-ok"></i> {options.SAVE}
									</button>
									<button type="reset" class="btn{DISABLED_CSS}" id="btn_cancel" name="btn_cancel"
											onclick="redirect('{options.URL}');"{READONLY}{DISABLED_TYPE}>{options.CANCEL}</button>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- END siteoptions -->
				
			</form>			
			</div>
		</div>
		<!-- END options -->
				
				
			</div>
			<!-- END::CONTENT BODY -->
		</div>
		<!-- END::CONTENT -->
	</div>
	<!-- END::CONTAINER -->
	<script src="{ROOT}assets/global/plugins/jquery.min.js" type="text/javascript"></script>
	