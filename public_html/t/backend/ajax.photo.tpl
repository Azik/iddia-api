<html>
<head>
</head>
<body>

<div class="tabbable-line boxless tabbable-reversed">
	<ul class="nav nav-tabs">
		<li class="active"><a href="#content_select" data-toggle="tab">{_IMAGE_TAB_SELECT_}</a></li>
		<li><a href="#content_upload" data-toggle="tab">{_IMAGE_TAB_UPLOAD_}</a></li>
	</ul>
</div>
<div class="tab-content">

<!-- BEG: Select image //-->
<div class="tab-pane in active" id="content_select">
	<div class="portlet box green">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-language"></i>{_IMAGE_TAB_SELECT_}
		</div>
		<div class="form-group">
			<input type="text" class="form-control" id="search_image" value="{SEARCH_KEYWORD}" placeholder="{_IMAGE_SEARCH_}" autocomplete="off" >
		</div>
	</div>
<div class="portlet-body form">
<!-- BEGIN files -->
<div class="row">
<div class="col-md-12">
	<strong>{_FILE_SELECT_TITLE_}</strong>
</div>
</div>
<!-- BEGIN items -->
<div class="row">
<!-- BEGIN list -->
<div class="col-md-2">
	<a class="fancybox-button" data-rel="fancybox-button" title="{files.items.list.FILE_SHOW_NAME}" onclick="selectImage({files.items.list.ID})">{files.items.list.THUMB_IMG}</a>
	<br/><br/>
</div>
<!-- END list -->
</div>
<!-- END items -->
<!-- BEGIN pages -->
<div class="row" style="margin:10px 10px 10px 0px !important;">
<!-- BEGIN pg -->
<div class="col-md-1 text-center">
	<a href="#" class="btn {files.pages.pg.CURR_CLASS}" onclick="openPagePhoto({IMGID}, {files.pages.pg.PAGE})">{files.pages.pg.PAGE}</a> &nbsp;
</div>
<!-- END pg -->
</div>
<br/>
<!-- END pages -->
<script>
function selectImage(img_id) {
	$("#{files.SELECT_FIELD}").val(img_id);
	$("#ajax_dialog").dialog('close');
}
</script>
<!-- END files -->

</div>
</div>
</div>
<!-- END: Select image //-->

<!-- BEG: Upload image //-->
<div class="tab-pane in" id="content_upload">
	<div class="portlet box green">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-language"></i>{_IMAGE_TAB_UPLOAD_}
		</div>
	</div>
	<div class="portlet-body form">
	
		<div class="row">
			<div class="col-md-12">
				<div class="form-group form-md-line-input">
				<form id="fileupload" action="" method="POST" enctype="multipart/form-data">
					<div class="col-md-8">
						<input type="file" name="file_up" placeholder="{_AJAX_PHOTO_FILE_}">
					</div>
					<div class="col-md-4">
						<input type="submit" value="{_AJAX_PHOTO_FILE_UPLOAD_}">
					</div>
				</form>
				</div>
				<div class="form-group form-md-line-input">
					<div class="col-md-1"></div>
					<div class="col-md-11">
						<div id="preview"><img src="images/no-image.jpg" alt="" /></div>
						<div id="err"></div>
					</div>
					<br/><br/><br/><br/>
				</div>
			</div>
		</div>
		
	</div>
	</div>
</div>
<!-- END: Upload image //-->

</div>

<script>
$(document).ready(function (e) {
 $("#search_image").on('keyup', function() {
	var keyword = $( this ).val();
	if (keyword.length > 2 ) {
		openPagePhoto({IMGID}, 1, keyword);
	}
 });
 
 $("#fileupload").on('submit',(function(e) {
  e.preventDefault();
  $.ajax({
    url: "ajax.upload.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	beforeSend : function()
    {
		$("#err").fadeOut();
   },
   success: function(data)
   {
    if(data=='invalid file')
    {
     // invalid file format.
     $("#err").html("{_INVALID_TYPE_}").fadeIn();
    }
    else
    {
     // view uploaded file.
     $("#preview").html(data).fadeIn();
     $("#fileupload")[0].reset(); 
    }
      },
     error: function(e) 
      {
    $("#err").html(e).fadeIn();
      }          
    });
 }));
});


function openPagePhoto(img_id=0, page=0, keyword='') {
	
	img_id = (Math.floor(img_id) == img_id && $.isNumeric(img_id)) ? img_id : 0;
	
	$("#ajax_dialog").dialog ("open");
	$("#ajax_dialog").html('{_IMAGE_SELECT_LOADING_}');
	$("#ajax_dialog").dialog('option', 'title', '{_IMAGE_SELECT_TITLE_}');
	
	// B: Ajax Dialog
	var ajax_url = "ajax.photo.php";
	$.ajax({
	  type: 'GET', 
	  contentType: "application/x-www-form-urlencoded; charset=utf-8",
	  url: ajax_url,
	  data: {
		page: page,
		img_id: img_id,
		keyword: keyword,
		select_field: '{SELECT_FIELD}',
		erize_num: Math.floor((Math.random() * 10000000))
	  }, 
	  dataType: 'html',
	  success: function (data) {
		if (data!='') {
			$("#ajax_dialog").html(data);
			$("#ajax_dialog").dialog('option', 'title', '{_IMAGE_SELECT_TITLE_}');
		} else {
			$("#ajax_dialog").html('{_AJAX_ERROR_}');
			$("#ajax_dialog").dialog('option', 'title', '{_AJAX_ERROR_}');
		}
	  },
	  error: function (result) {
		$("#ajax_dialog").html('{_AJAX_ERROR_}');
		$("#ajax_dialog").dialog('option', 'title', '{_AJAX_ERROR_}');
	  }
	});

}

</script>

</body>
</html>