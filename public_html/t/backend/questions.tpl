
	<!-- BEGIN::PAGE LEVEL PLUGINS -->
	<link href="{ROOT}assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" type="text/css" />
	<link href="{ROOT}assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
	<link href="{ROOT}assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
	<!-- END::PAGE LEVEL PLUGINS -->
	
	<link href="{ROOT}assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
	<script src="{ROOT}assets/global/plugins/jquery.min.js" type="text/javascript"></script>
	
	<script type="text/javascript">
		
		function clickAc(act) {
			//alert(act);
			var set = $(".checkboxes");
			var fields = '';
			$(set).each(function () {
				var checked = $(this).is(":checked");
				if (checked) {
					var v = $(this).val();
					fields += (fields != '') ? ',' : '';
					fields += v;
					//alert("aaa:"+v);
				}
			});
			if (fields != '')
				window.location.href = '?{MODULE_QS}=questions&cateditid=' + act + ':' + fields;
		}
		function clickAcquestions(act, catid) {
			//alert(act);
			var set = $(".checkboxes");
			var fields = '';
			$(set).each(function () {
				var checked = $(this).is(":checked");
				if (checked) {
					var v = $(this).val();
					fields += (fields != '') ? ',' : '';
					fields += v;
					//alert("aaa:"+v);
				}
			});
			if (fields != '')
				window.location.href = '?{MODULE_QS}=questions&catid=' + catid + '&questionid=' + act + ':' + fields;
		}
		function click_cancel(act, id) {
			if (act == 'questions') {
				window.location.href = '?{MODULE_QS}=questions&mod=questions&testid=' + id;
			}
			else if (act == 'tests') {
				window.location.href = '?{MODULE_QS}=questions&mod=tests&catid=' + id;
			}
			else if (act == 'answers') {
				window.location.href = '?{MODULE_QS}=questions&mod=answers&questionid=' + id;
			} else {
				window.location.href = '?{MODULE_QS}=questions';
			}
		}

		function doCatDelete(id) {
			if (confirm('{CAT_CONFIRM}')) {
				document.getElementById("cat_action").value = 'delete';
				document.getElementById('select_id').value = id;
				document.getElementById('cat_subm').click();
			}
		}
		
		function doCatAction(act, id, mess) {
			document.getElementById("cat_action").value = act;
			document.getElementById('select_id').value = id;
				debugger;

			if (act == 'delete') {
				if (confirm(mess)) {
					document.getElementById("cat_subm").click();
				}
			}
			else {
				document.getElementById("cat_subm").click();
			}
		}

		function doquestionsDelete(id) {
			if (confirm('{QUESTIONS_CONFIRM}')) {
				document.getElementById("questions_action").value = 'delete';
				document.getElementById('select_id').value = id;
				document.getElementById('questions_subm').click();
			}
		}


		function docorrect_answersDelete(id) {
			if (confirm('{QUESTIONS_CONFIRM}')) {
				document.getElementById("correct_answers_action").value = 'delete';
				document.getElementById('select_id').value = id;
				document.getElementById('correct_answers_subm').click();
			}
		}
		
		
		function dotestsDelete(id) {
			if (confirm('{QUESTIONS_CONFIRM}')) {
				document.getElementById("tests_action").value = 'delete';
				document.getElementById('select_id').value = id;
				document.getElementById('tests_subm').click();
			}
		}

		function doanswersDelete(id) {
			if (confirm('{QUESTIONS_CONFIRM}')) {
				document.getElementById("answers_action").value = 'delete';
				document.getElementById('select_id').value = id;
				document.getElementById('answers_subm').click();
			}
		}
		
		
		
		function dotestsAction(act, id, mess) {
			document.getElementById("tests_action").value = act;
			document.getElementById('select_id').value = id;

			if (act == 'delete') {
				if (confirm(mess)) {
					document.getElementById("tests_subm").click();
				}
			}
			else {
				document.getElementById("tests_subm").click();
			}
		}
		
		
		function doquestionsAction(act, id, mess) {
			document.getElementById("questions_action").value = act;
			document.getElementById('select_id').value = id;

			if (act == 'delete') {
				if (confirm(mess)) {
					document.getElementById("questions_subm").click();
				}
			}
			else {
				document.getElementById("questions_subm").click();
			}
		}
		
		function doanswersAction(act, id, mess) {
			document.getElementById("answers_action").value = act;
			document.getElementById('select_id').value = id;

			if (act == 'delete') {
				if (confirm(mess)) {
					document.getElementById("answers_subm").click();
				}
			}
			else {
				document.getElementById("answers_subm").click();
			}
		}
		
		function docorrect_answersAction(act, id, mess) {
			document.getElementById("correct_answers_action").value = act;
			document.getElementById('select_id').value = id;

			if (act == 'delete') {
				if (confirm(mess)) {
					document.getElementById("correct_answers_subm").click();
				}
			}
			else {
				document.getElementById("correct_answers_subm").click();
			}
		}

		function doOptionsAction(options_action) {
			document.getElementById("options_action").value = options_action;
			document.getElementById("options_subm").click();
		}
		
		function redirect(url) {
			window.location.href = url;
		}
	</script>

		<!-- BEGIN::CONTENT -->
		<div class="page-content-wrapper" style="min-width:450px !important;">
			<!-- BEGIN::CONTENT BODY -->
			<div class="page-content">
				<!-- BEGIN::PAGE HEADER-->
				<!-- BEGIN::PAGE BAR -->
				<div class="page-bar">
					<ul class="page-breadcrumb">
						<li>
							<a href="index.php">{HOME}</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<!-- BEGIN cat -->
						<li>
							<span><a href="index.php?{MODULE_QS}=questions">{TITLE}</a></span>
						</li>
						<!-- END cat -->
						
						<!-- BEGIN tests -->
						<li>
							<span><a href="index.php?{MODULE_QS}=questions">{TITLE}</a></span>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<span><a href="index.php?{MODULE_QS}=questions&mod=tests&catid={tests.CAT_ID}">{tests.CAT_NAME}</a></span>
							
						</li>
						<!-- END tests -->

						<!-- BEGIN correct_answers -->
						<li>
							<span><a href="index.php?{MODULE_QS}=questions">{TITLE}</a></span>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<span><a href="index.php?{MODULE_QS}=questions&mod=tests&catid={correct_answers.CATES_ID}">{correct_answers.CATES_NAME}</a></span>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<span><a href="index.php?{MODULE_QS}=questions&mod=tests&catid={correct_answers.CATES_ID}">{correct_answers.TEST_NAME}AA</a></span>
						</li>
						<!-- END correct_answers -->

						<!-- BEGIN questions -->
						<li>
							<span><a href="index.php?{MODULE_QS}=questions">{TITLE}</a></span>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<span><a href="index.php?{MODULE_QS}=questions&mod=tests&catid={questions.CAT1_ID}">{questions.CAT1_NAME}</a></span>
						</li>
						<li>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<span><a href="index.php?{MODULE_QS}=questions&mod=questions&testid={questions.CAT_ID}">{questions.CAT_NAME}</a></span>
							
						</li>
						<!-- END questions -->

						<!-- BEGIN answers -->
						<li>
							<span><a href="index.php?{MODULE_QS}=questions">{TITLE}</a></span>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<span><a href="index.php?{MODULE_QS}=questions&catid={answers.CATES_ID}">{answers.CATES_NAME}</a></span>
						</li>
						<li>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<span><a href="index.php?{MODULE_QS}=questions&mod=questions&testid={answers.QUESTIONS_ID}">{answers.QUESTIONS_NAME}</a></span>
						</li>
						<li>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<span><a href="index.php?{MODULE_QS}=questions&mod=answers&questionid={answers.CAT_ID}">{answers.CAT_NAME}</a></span>
						</li>
						<!-- END answers -->
					</ul>
				</div>
				<!-- END::PAGE BAR -->
				<!-- BEGIN::PAGE TITLE-->
				<h3 class="page-title"> {TITLE} </h3>
				<!-- END::PAGE TITLE-->
				<!-- END::PAGE HEADER-->
				
				<!-- BEGIN cat -->
				<div class="row">
					<div class="col-md-12">
						<!-- BEGIN::questions CAT-->
						<div class="portlet light bordered">
							<div class="portlet-title">
								<div class="caption font-dark">
									<i class="fa fa-calendar"></i>
									<span class="caption-subject bold uppercase">{TITLE}</span>
								</div>
							</div>
							<form action="" method="POST" class="form-horizontal" name="form_cat" id="form_cat">
								<input type="hidden" id="cat_action" name="cat_action" value="">
								<input type="hidden" value="0" id="select_id" name="select_id">
								<input type="submit" id="cat_subm" name="cat_subm" style="display:none;">
							
							<!-- BEGIN list -->
							<div class="portlet-body">
								<!-- BEGIN perm_add -->
								<div class="clearfix">
								
									<!--<div class="btn-group">
										<a href="{cat.list.LAST_questions_URL}" class="btn yellow">
											{cat.list.LAST_questions} &nbsp; <i class="fa fa-list"></i>
										</a>
									</div> -->
									<div class="btn-group">
										<a href="{cat.list.ADD_URL}" class="btn green">
											{cat.list.ADD} &nbsp; <i class="fa fa-plus"></i>
										</a>
									</div>
									<!--<div class="btn-group">
										<a href="{cat.list.ADD_questions_URL}" class="btn green">
											{cat.list.ADD_questions} &nbsp; <i class="fa fa-plus"></i>
										</a>
									</div> -->
									
								</div>
								<!-- END perm_add -->
								<br/>
								<table class="table table-striped table-bordered table-hover" width="100%" id="questions_cat_list">
									<thead>
											<th style="width:0px !important;display:none;"></th>
											<th style="width:12px !important;text-align:center;"><input type="checkbox" class="group-checkable" data-set="#questions_cat_list .checkboxes"/></th>
											<th>{cat.list.NAME}</th>
											<th>{cat.list.HEADER}</th>
											<th></th>
										</tr>
									</thead>
									<tbody>
										<!-- BEGIN items -->
										<tr data-position="{cat.list.items.POSITION}" id="{cat.list.items.ID}">
											<td style="display:none;">{cat.list.items.POSITION}</td>
											<td style="width:12px !important;text-align:center;" id="chk_{cat.list.items.ID}"><input type="checkbox" class="checkboxes" value="{cat.list.items.ID}"/></td>
											<td><a href="{cat.list.URL}&catid={cat.list.items.ID}">{cat.list.items.NAME}</a></td>
											<td>{cat.list.items.HEADER}</td>
											<td style="width : 320px !important;">
											    <!-- BEGIN perm_edit -->
												<a href="{cat.list.items.EDIT_URL}" title="{cat.list.EDIT}" class="btn yellow">
													{cat.list.EDIT}
													 &nbsp; <i class="fa fa-pencil"></i>
												</a>
												<!-- END perm_edit -->
												<!-- BEGIN perm_del -->
												<a href="JavaScript:doCatDelete({cat.list.items.ID})" title="{cat.list.DELETE}" class="btn red ask"> 
													{cat.list.DELETE}
													 &nbsp; <i class="fa fa-trash"></i>
												</a>
												<!-- END perm_del -->
												
												<!-- BEGIN active -->
												<a href="{cat.list.items.ACTIVE_URL}" class="btn btn-success">{cat.list.ACTIVE}
													 &nbsp; <i class="fa fa-unlock"></i></a>
												<!-- END active -->
												<!-- BEGIN inactive -->
												<a href="{cat.list.items.INACTIVE_URL}" class="btn btn-danger">{cat.list.INACTIVE}
													 &nbsp; <i class="fa fa-lock"></i></a>
												<!-- END inactive -->
											</td>
										</tr>
										<!-- END items -->
									</tbody>
								</table>
								<!-- BEGIN hidden -->
								<input type="hidden" name="sort_order" id="sort_order" value="{cat.list.hidden.VALUE}"/>
								<!-- END hidden -->
								
								<br/>
								<!-- BEGIN perm_edit -->
								<table>
									<tfoot>
									<tr>
										<td class="ac"><span class="label label-success"
															 onclick="clickAc('activate')">{cat.list.ACTIVATE}</span></td>
										<td style="width:10px;"></td>
										<td class="ac"><span class="label label-danger"
															 onclick="clickAc('inactivate')">{cat.list.INACTIVATE}</span></td>
										<td></td>
									</tr>
									</tfoot>
								</table>
								<!-- END perm_edit -->
							</div>
							<!-- END list -->
							
							<!-- BEGIN catedit -->
							<div class="portlet-body" id="panel_editbar">
								<div class="form-group form-md-checkboxes">
									<label class="col-md-3 control-label" for="txt_active">&nbsp;</label>
									<div class="col-md-6">
										<div class="md-checkbox-list">
											<div class="md-checkbox">
												<input type="checkbox" name="txt_active" id="txt_active" value="1" class="md-check"  {cat.catedit.ACTIVE_CHK} />
												<label for="txt_active">
													<span></span>
													<span class="check"></span>
													<span class="box"></span>{cat.catedit.ACTIVE}
												</label>
											</div>
										</div>
									</div>
								</div>
								
								<div class="form-group form-md-checkboxes">
									<label class="col-md-3 control-label" for="txt_index">&nbsp;</label>
									<div class="col-md-6">
										<div class="md-checkbox-list">
											<div class="md-checkbox">
												<input type="checkbox" name="txt_index" id="txt_index" value="1" class="md-check"  {cat.catedit.INDEX_CHK} />
												<label for="txt_index">
													<span></span>
													<span class="check"></span>
													<span class="box"></span>{cat.catedit.INDEX}
												</label>
											</div>
										</div>
									</div>
								</div>
								
								

							

								<div class="form-group form-md-line-input">
									<label class="col-md-3 control-label" for="txt_level">Level</label>
									<div class="col-md-6">
										<select class="form-control" name="txt_level" id="txt_level">
											<!-- BEGIN levels -->
											<option value="{cat.catedit.levels.VALUE}" {cat.catedit.levels.SELECTED}>{cat.catedit.levels.TEXT}</option>
											<!-- END levels -->
										</select>
										<div class="form-control-focus"> </div>
									</div>
								</div>


								<div class="form-group form-md-line-input">
									<label class="col-md-3 control-label cursor-pointer" for="txt_icon">Icon</label>
									<div class="col-md-8">
										<input type="text" class="form-control" name="txt_icon"
											   id="txt_icon" value="{cat.catedit.ICON_VALUE}"
											   placeholder="Icon">
										<div class="form-control-focus"> </div>
									</div>
								</div>
								
								
								<div class="form-group form-md-line-input">
									<label class="col-md-3 control-label cursor-pointer" for="txt_header" onclick="openPhoto($('#txt_header').val(), 'txt_header')">Image</label>
									<div class="col-md-8">
										<input type="text" class="form-control" name="txt_header"
											   id="txt_header" value="{cat.catedit.HEADER_VALUE}"
											   placeholder="Image">
										<div class="form-control-focus"> </div>
									</div>
								</div>

								<div class="tabbable-line boxless tabbable-reversed">
									<ul class="nav nav-tabs">
										<!-- BEGIN tab -->
										<li{cat.catedit.tab.CLASS}><a href="#content_{cat.catedit.tab.LANG}" data-toggle="tab">{cat.catedit.tab.LANG}</a></li>
										<!-- END tab -->
									</ul>
								</div>
								
								<div class="tab-content">
									<!-- BEGIN tab -->
									<div class="tab-pane {cat.catedit.tab.FADE_CLASS}" id="content_{cat.catedit.tab.LANG}">
										<div class="portlet box green">
										<div class="portlet-title">
											<div class="caption">
												<i class="fa fa-language"></i>{cat.catedit.tab.LANG} 
											</div>
										</div>
										<div class="portlet-body form">
										<div class="form-group form-md-line-input">
											<label class="col-md-3 control-label" for="txt_name_{cat.catedit.tab.LANG}">{cat.catedit.tab.NAME}</label>
											<div class="col-md-8">
												<input type="text" class="form-control" name="txt_name_{cat.catedit.tab.LANG}"
													   id="txt_name_{cat.catedit.tab.LANG}" value="{cat.catedit.tab.NAME_VALUE}"
													   placeholder="{cat.catedit.tab.NAME}">
												<div class="form-control-focus"> </div>
											</div>
										</div>
										<div class="form-group form-md-line-input">
											<label class="col-md-3 control-label" for="txt_header_{cat.catedit.tab.LANG}">{cat.catedit.tab.HEADER}</label>
											<div class="col-md-8">
												<textarea class="form-control ckeditor" name="txt_header_{cat.catedit.tab.LANG}"
														  id="txt_header_{cat.catedit.tab.LANG}" rows="3">{cat.catedit.tab.HEADER_VALUE}</textarea>
												<div class="form-control-focus"> </div>
											</div>
										</div>
										<div class="form-actions">
											<div class="row">
												<div class="col-md-offset-3 col-md-9">
													<button type="button" class="btn blue" id="btn_save" name="btn_save"
															onClick="doCatAction('save', {cat.catedit.ID},  '');"><i class="fa fa-ok"></i> {cat.catedit.SAVE}
													</button>
													<button type="reset" class="btn" id="btn_cancel" name="btn_cancel"
															onclick="click_cancel('cat', 0);">{cat.catedit.CANCEL}</button>
												</div>
											</div>
										</div>
										</div>
										</div>
									</div>
									<!-- END tab -->
								</div>
							
							</div>
							<!-- END catedit -->
							
							</form>
						</div>
						<!-- END::questions CAT-->
					</div>
				</div>
				<!-- END cat -->
				



				
				<!-- BEGIN tests -->
				<div class="row">
					<div class="col-md-12">
						<!-- BEGIN::tests CAT-->
						<div class="portlet light bordered">
							<div class="portlet-title">
								<div class="caption font-dark">
									<i class="fa fa-calendar"></i>
									<span class="caption-subject bold uppercase">{tests.CAT_NAME} / Tests</span>
								</div>
							</div>
							<form action="" method="POST" class="form-horizontal" name="form_tests" id="form_tests">
								<input type="hidden" id="tests_action" name="tests_action" value="">
								<input type="hidden" value="0" id="select_id" name="select_id">
								<input type="submit" id="tests_subm" name="tests_subm" style="display:none;">
							
								<!-- BEGIN list -->
								<div class="portlet-body">
									<!-- BEGIN perm_add -->
									<div class="clearfix">
										<div class="btn-group">
											<a href="{tests.list.ADD_URL}" class="btn green">
												{tests.list.ADD} &nbsp; <i class="fa fa-plus"></i>
											</a>
										</div>
									</div>
									<!-- END perm_add -->
									<br/>
									<table class="table table-striped table-bordered table-hover" width="100%" id="questions_list">
										<thead>
											<tr>
												<th style="width:12px !important;"><input type="checkbox" class="group-checkable" data-set="#questions_list .checkboxes"/></th>
												<th>{tests.list.NAME}</th>
												<th style="width:120px !important;">{tests.list.DATE}</th>
												<th style="width:120px !important;">Active</th>
												<th style="width:100px !important;">Index</th>
												<th style="width:500px !important;"></th>
											</tr>
										</thead>
										<tbody>
											<!-- BEGIN items -->
											<tr id="{tests.list.items.ID}">
												<td id="chk_{tests.list.items.ID}"><input type="checkbox" class="checkboxes" value="{tests.list.items.ID}"/></td>
												<td><a href="{tests.list.items.EDIT_URL}" title="{tests.list.EDIT}">{tests.list.items.NAME}</a></td>
												<td>{tests.list.items.DATE}</td>
												<td>{tests.list.items.ACTIVE}</td>
												<td>{tests.list.items.INDEX}</td>
												<td>
												<a href="{tests.list.items.CORRECT_ANSWERS_URL}" title="Correct answers" class="btn yellow"> Correct answers&nbsp;</a>
												<!-- BEGIN perm_edit -->
												<a href="{tests.list.items.EDIT_URL}" title="{tests.list.EDIT}" class="btn yellow"> {tests.list.EDIT}&nbsp; <i class="fa fa-pencil"></i></a>
												<!-- END perm_edit -->
												<!-- BEGIN perm_del -->
												<a href="JavaScript:dotestsDelete({tests.list.items.ID})" title="{tests.list.DELETE}" class="btn red ask" title="{cat.list.DELETE}"> {tests.list.DELETE}&nbsp; <i class="fa fa-trash"></i></a>
												<!-- END perm_del -->
													<!-- BEGIN active -->
													<a href="{tests.list.items.ACTIVE_URL}" class="btn btn-success">{tests.list.ACTIVE}&nbsp; <i class="fa fa-unlock"></i></a>
													<!-- END active -->
													<!-- BEGIN inactive -->
													<a href="{tests.list.items.INACTIVE_URL}" class="btn btn-danger">{tests.list.INACTIVE}&nbsp; <i class="fa fa-lock"></i></a>
													<!-- END inactive -->
												</td>
											</tr>
											<!-- END items -->
										</tbody>
									</table>
									<!-- BEGIN hidden -->
									<input type="hidden" name="sort_order" id="sort_order" value="{tests.list.hidden.VALUE}"/>
									<!-- END hidden -->
									
									<br/>
									<!-- BEGIN perm_edit -->
									<table>
										<tfoot>
										<tr>
											<td class="ac"><span class="btn btn-success"
																 onclick="clickAcquestions('activate', {tests.list.CATID})">{tests.list.ACTIVATE}</span></td>
											<td style="width:10px;"></td>
											<td class="ac"><span class="btn btn-danger"
																 onclick="clickAcquestions('inactivate', {tests.list.CATID})">{tests.list.INACTIVATE}</span></td>
											<td></td>
										</tr>
										</tfoot>
									</table>
									<!-- END perm_edit -->
								</div>
								<!-- END list -->
								
								<!-- BEGIN edit -->
								<div class="portlet-body" id="panel_editbar">
									<div class="form-group form-md-line-input">
										<label class="col-md-3 control-label" for="txt_category">{tests.edit.CATEGORY}</label>
										<div class="col-md-6">
											<select class="form-control" name="txt_category" id="txt_category">
												<!-- BEGIN category -->
												<option value="{tests.edit.category.VALUE}" {tests.edit.category.SELECTED}>{tests.edit.category.TEXT}</option>
												<!-- END category -->
											</select>
											<div class="form-control-focus"> </div>
										</div>
									</div>

									<div class="form-group form-md-line-input">
										<label class="col-md-3 control-label cursor-pointer" for="txt_header" onclick="openPhoto($('#txt_header').val(), 'txt_header')">Image</label>
										<div class="col-md-8">
											<input type="text" class="form-control" name="txt_header"
												   id="txt_header" value="{tests.edit.HEADER_VALUE}"
												   placeholder="Image">
											<div class="form-control-focus"> </div>
										</div>
									</div>

									<div class="form-group form-md-checkboxes">
										<label class="col-md-3 control-label" for="txt_active">&nbsp;</label>
										<div class="col-md-6">
											<div class="md-checkbox-list">
												<div class="md-checkbox">
													<input type="checkbox" name="txt_active" id="txt_active" value="1" class="md-check" {tests.edit.ACTIVE_CHK}/>
													<label for="txt_active">
														<span></span>
														<span class="check"></span>
														<span class="box"></span>{tests.edit.ACTIVE}
													</label>
												</div>
											</div>
										</div>
									</div>
								
									<div class="tabbable-line boxless tabbable-reversed">
										<ul class="nav nav-tabs">
											<!-- BEGIN tab -->
											<li{tests.edit.tab.CLASS}><a href="#content_{tests.edit.tab.LANG}" data-toggle="tab">{tests.edit.tab.LANG}</a></li>
											<!-- END tab -->
										</ul>
									</div>
									<div class="tab-content">
										<!-- BEGIN tab -->
										<div class="tab-pane {tests.edit.tab.FADE_CLASS}" id="content_{tests.edit.tab.LANG}">
											<div class="portlet box green">
											<div class="portlet-title">
												<div class="caption">
													<i class="fa fa-language"></i>{tests.edit.tab.LANG}
												</div>
											</div>
											<div class="portlet-body form">
											<div class="form-group form-md-line-input">
												<label class="col-md-3 control-label" for="txt_name_{tests.edit.tab.LANG}">{tests.edit.tab.NAME}</label>
												<div class="col-md-8">
													<input type="text" class="form-control" name="txt_name_{tests.edit.tab.LANG}"
														   id="txt_name_{tests.edit.tab.LANG}" value="{tests.edit.tab.NAME_VALUE}"
														   placeholder="{tests.edit.tab.NAME}">
													<div class="form-control-focus"> </div>
												</div>
											</div>

											<div class="form-group form-md-line-input">
												<label class="col-md-3 control-label" for="txt_header_{tests.edit.tab.LANG}">Text</label>
												<div class="col-md-8">
													<textarea class="form-control ckeditor" name="txt_header_{tests.edit.tab.LANG}"
															  id="txt_header_{tests.edit.tab.LANG}" rows="3">{tests.edit.tab.HEADER_VALUE}</textarea>
													<div class="form-control-focus"> </div>
												</div>
											</div>
									
										
											<div class="form-actions">
												<div class="row">
													<div class="col-md-offset-3 col-md-9">
														<button type="button" class="btn blue" id="btn_save" name="btn_save"
																onclick="dotestsAction('save', {tests.edit.ID},  '');"><i class="fa fa-ok"></i> {tests.edit.SAVE}
														</button>
														<button type="reset" class="btn" id="btn_cancel" name="btn_cancel"
																onclick="click_cancel('tests', {tests.edit.CATID});">{tests.edit.CANCEL}</button>
													</div>
												</div>
											</div>
											</div>
											</div>
										</div>
										<!-- END tab -->
									</div>
									
									
								
								</div>
								<!-- END edit -->
								
							</form>
						</div>
						<!-- END::tests CAT-->
					</div>
				</div>
				<!-- END tests -->


				
				<!-- BEGIN questions -->
				<div class="row">
					<div class="col-md-12">
						<!-- BEGIN::questions CAT-->
						<div class="portlet light bordered">
							<div class="portlet-title">
								<div class="caption font-dark">
									<i class="fa fa-calendar"></i>
									<span class="caption-subject bold uppercase">{questions.CAT_NAME} / Questions</span>
								</div>
							</div>
							<form action="" method="POST" class="form-horizontal" name="form_questions" id="form_questions">
								<input type="hidden" id="questions_action" name="questions_action" value="">
								<input type="hidden" value="0" id="select_id" name="select_id">
								<input type="submit" id="questions_subm" name="questions_subm" style="display:none;">
							
								<!-- BEGIN list -->
								<div class="portlet-body">
									<!-- BEGIN perm_add -->
									<div class="clearfix">
										<div class="btn-group">
											<a href="{questions.list.ADD_URL}" class="btn green">
												ADD QUESTION &nbsp; <i class="fa fa-plus"></i>
											</a>
										</div>
									</div>
									<!-- END perm_add -->
									<br/>
									<table class="table table-striped table-bordered table-hover" width="100%" id="questions_list">
										<thead>
											<tr>
												<th style="width:12px !important;"><input type="checkbox" class="group-checkable" data-set="#questions_list .checkboxes"/></th>
												<th>{questions.list.NAME}</th>
												<th style="width:150px !important;">{questions.list.DATE}</th>
												<th style="width:280px !important;"></th>
											</tr>
										</thead>
										<tbody>
											<!-- BEGIN items -->
											<tr id="{questions.list.items.ID}">
												<td id="chk_{questions.list.items.ID}"><input type="checkbox" class="checkboxes" value="{questions.list.items.ID}"/></td>
												<td><a href="{questions.list.items.EDIT_URL}" title="{questions.list.EDIT}">{questions.list.items.NAME}</a></td>
												<td>{questions.list.items.DATE}</td>
												<td>{questions.list.items.ACTIVE}</td>
												<td>{questions.list.items.INDEX}</td>
												<td>{questions.list.items.TOTAL_VIEWS}</td>
												<td>{questions.list.items.MONTH_VIEWS}</td>
												<td>{questions.list.items.READ_DATE}</td>
												<td>{questions.list.items.COMMENT}</td>
												<td>
												<!-- BEGIN perm_edit -->
												<a href="{questions.list.items.EDIT_URL}" title="{questions.list.EDIT}" class="btn yellow"> {questions.list.EDIT}&nbsp; <i class="fa fa-pencil"></i></a>
												<!-- END perm_edit -->
												<!-- BEGIN perm_del -->
												<a href="JavaScript:doquestionsDelete({questions.list.items.ID})" title="{questions.list.DELETE}" class="btn red ask" title="{cat.list.DELETE}"> {questions.list.DELETE}&nbsp; <i class="fa fa-trash"></i></a>
												<!-- END perm_del -->
													<!-- BEGIN active -->
													<a href="{questions.list.items.ACTIVE_URL}" class="btn btn-success">{questions.list.ACTIVE}&nbsp; <i class="fa fa-unlock"></i></a>
													<!-- END active -->
													<!-- BEGIN inactive -->
													<a href="{questions.list.items.INACTIVE_URL}" class="btn btn-danger">{questions.list.INACTIVE}&nbsp; <i class="fa fa-lock"></i></a>
													<!-- END inactive -->
												</td>
											</tr>
											<!-- END items -->
										</tbody>
									</table>
									<!-- BEGIN hidden -->
									<input type="hidden" name="sort_order" id="sort_order" value="{questions.list.hidden.VALUE}"/>
									<!-- END hidden -->
									
									<br/>
									<!-- BEGIN perm_edit -->
									<table>
										<tfoot>
										<tr>
											<td class="ac"><span class="btn btn-success"
																 onclick="clickAcquestions('activate', {questions.list.CATID})">{questions.list.ACTIVATE}</span></td>
											<td style="width:10px;"></td>
											<td class="ac"><span class="btn btn-danger"
																 onclick="clickAcquestions('inactivate', {questions.list.CATID})">{questions.list.INACTIVATE}</span></td>
											<td></td>
										</tr>
										</tfoot>
									</table>
									<!-- END perm_edit -->
								</div>
								<!-- END list -->
								
								<!-- BEGIN edit -->
								<div class="portlet-body" id="panel_editbar">
									<div class="form-group form-md-line-input">
										<label class="col-md-3 control-label" for="txt_category">{questions.edit.CATEGORY}</label>
										<div class="col-md-6">
											<select class="form-control" name="txt_category" id="txt_category">
												<!-- BEGIN category -->
												<option value="{questions.edit.category.VALUE}" {questions.edit.category.SELECTED}>{questions.edit.category.TEXT}</option>
												<!-- END category -->
											</select>
											<div class="form-control-focus"> </div>
										</div>
									</div>

									<div class="form-group form-md-line-input">
										<label class="col-md-3 control-label cursor-pointer" for="txt_header" onclick="openPhoto($('#txt_header').val(), 'txt_header')">Image</label>
										<div class="col-md-8">
											<input type="text" class="form-control" name="txt_header"
												   id="txt_header" value="{questions.edit.HEADER_VALUE}"
												   placeholder="Image">
											<div class="form-control-focus"> </div>
										</div>
									</div>
									
									
									<div class="form-group form-md-line-input">
										<label class="col-md-3 control-label cursor-pointer" for="txt_answers_limit">Answers limit</label>
										<div class="col-md-8">
											<input type="text" class="form-control" name="txt_answers_limit"
												   id="txt_answers_limit" value="{questions.edit.ANSWERS_LIMIT_VALUE}"
												   placeholder="Answers limit">
											<div class="form-control-focus"> </div>
										</div>
									</div>

									<div class="form-group form-md-checkboxes">
										<label class="col-md-3 control-label" for="txt_active">&nbsp;</label>
										<div class="col-md-6">
											<div class="md-checkbox-list">
												<div class="md-checkbox">
													<input type="checkbox" name="txt_active" id="txt_active" value="1" class="md-check" {questions.edit.ACTIVE_CHK}/>
													<label for="txt_active">
														<span></span>
														<span class="check"></span>
														<span class="box"></span>{questions.edit.ACTIVE}
													</label>
												</div>
											</div>
										</div>
									</div>

								
									<div class="tabbable-line boxless tabbable-reversed">
										<ul class="nav nav-tabs">
											<!-- BEGIN tab -->
											<li{questions.edit.tab.CLASS}><a href="#content_{questions.edit.tab.LANG}" data-toggle="tab">{questions.edit.tab.LANG}</a></li>
											<!-- END tab -->
										</ul>
									</div>
									<div class="tab-content">
										<!-- BEGIN tab -->
										<div class="tab-pane {questions.edit.tab.FADE_CLASS}" id="content_{questions.edit.tab.LANG}">
											<div class="portlet box green">
											<div class="portlet-title">
												<div class="caption">
													<i class="fa fa-language"></i>{questions.edit.tab.LANG}
												</div>
											</div>
											<div class="portlet-body form">
											<div class="form-group form-md-line-input">
												<label class="col-md-3 control-label" for="txt_name_{questions.edit.tab.LANG}">{questions.edit.tab.NAME}</label>
												<div class="col-md-8">
													<input type="text" class="form-control" name="txt_name_{questions.edit.tab.LANG}"
														   id="txt_name_{questions.edit.tab.LANG}" value="{questions.edit.tab.NAME_VALUE}"
														   placeholder="{questions.edit.tab.NAME}">
													<div class="form-control-focus"> </div>
												</div>
											</div>
										
										
											<div class="form-actions">
												<div class="row">
													<div class="col-md-offset-3 col-md-9">
														<button type="button" class="btn blue" id="btn_save" name="btn_save"
																onclick="doquestionsAction('save', {questions.edit.ID},  '');"><i class="fa fa-ok"></i> {questions.edit.SAVE}
														</button>
														<button type="reset" class="btn" id="btn_cancel" name="btn_cancel"
																onclick="click_cancel('questions', {questions.edit.CATID});">{questions.edit.CANCEL}</button>
													</div>
												</div>
											</div>
											</div>
											</div>
										</div>
										<!-- END tab -->
									</div>
									
									
								
								</div>
								<!-- END edit -->
								
							</form>
						</div>
						<!-- END::questions CAT-->
					</div>
				</div>
				<!-- END questions -->
				
				
				



				
				<!-- BEGIN questionscat_options -->
				<div class="row">
					<div class="col-md-12">
						<!-- B: TABLE PORTLET-->
						<button type="button" id="questionscat_button" class="btn green" style="margin-bottom:15px !important;">{questionscat_options.TITLE} &nbsp; <i class="fa fa-plus"></i></button>
						<script>
							$("#questionscat_button").click(function () {
								$("#questionscat_options").toggle("fade", 1000);
							});
						</script>
						
						<div class="portlet light bordered" id="questionscat_options" style="display: none;">
							<div class="portlet-title" style="margin-bottom:-10px;">
								<h4>{questionscat_options.TITLE}</h4>
							</div>
							<form action="" method="POST" name="options_form" id="options_form">
								<input type="hidden" id="options_action" name="action" value="">
								<input type="submit" id="options_subm" name="options_subm" style="display:none;">

								<div class="portlet-body form" id="panel_editbar">
								<div class="form-body">
									<div class="form-group">
										<label>Answers limit</label>
										<input type="number" class="form-control input-medium" name="txt_answers_limit" id="txt_answers_limit"
												   value="{questionscat_options.ANSWERS_LIMIT_VALUE}"
												   placeholder="Answers limit" >
									</div>
								
									
									<div class="form-actions">
										<button type="button" class="btn blue" id="btn_save" name="btn_save"
												onClick="doOptionsAction('save_questionscat_options');"><i class="fa fa-ok"></i> {questionscat_options.SAVE}</button>
										<button type="reset" class="btn" id="btn_cancel" name="btn_cancel"
												onclick="click_cancel('cat', 0);">{questionscat_options.CANCEL}</button>
									</div>
								</div>
								</div>

							</form>

						</div>
						<!-- E: TABLE PORTLET-->

					</div>
				</div>
				<!-- END questionscat_options -->
				







	
				<!-- BEGIN answers -->
				<div class="row">
					<div class="col-md-12">
						<!-- BEGIN::questions CAT-->
						<div class="portlet light bordered">
							<div class="portlet-title">
								<div class="caption font-dark">
									<i class="fa fa-calendar"></i>
									<span class="caption-subject bold uppercase">Add / Update answer</span>
								</div>
							</div>
							<form action="" method="POST" class="form-horizontal" name="form_answers" id="form_answers">
								<input type="hidden" id="answers_action" name="answers_action" value="">
								<input type="hidden" value="0" id="select_id" name="select_id">
								<input type="submit" id="answers_subm" name="answers_subm" style="display:none;">
							
								<!-- BEGIN list -->
								<div class="portlet-body">
									<div class="clearfix">
										<div class="btn-group">
											<a href="{answers.list.ADD_URL}" class="btn green">
												Answer add &nbsp; <i class="fa fa-plus"></i>
											</a>
										</div>
									</div>
									<br/>
									<table class="table table-striped table-bordered table-hover" width="100%" id="answers_list">
										<thead>
											<tr>
												<th style="width:12px !important;"><input type="checkbox" class="group-checkable" data-set="#answers_list .checkboxes"/></th>
												<th>{answers.list.NAME}</th>
												<th style="width:150px !important;">Variant</th>
												<th style="width:150px !important;">{answers.list.DATE}</th>
												<th style="width:180px !important;">Active</th>
												<th style="width:340px !important;"></th>
											</tr>
										</thead>
										<tbody>
											<!-- BEGIN items -->
											<tr id="{answers.list.items.ID}">
												<td id="chk_{answers.list.items.ID}"><input type="checkbox" class="checkboxes" value="{answers.list.items.ID}"/></td>
												<td>{answers.list.items.NAME}</td>
												<td>{answers.list.items.VARIANT}</td>
												<td>{answers.list.items.DATE}</td>
												<td>{answers.list.items.ACTIVE}</td>
												<td>
												<!-- BEGIN perm_edit -->
												<a href="{answers.list.items.EDIT_URL}" title="{answers.list.EDIT}" class="btn yellow"> {answers.list.EDIT}&nbsp; <i class="fa fa-pencil"></i></a>
												<!-- END perm_edit -->
												<!-- BEGIN perm_del -->
												<a href="JavaScript:doanswersDelete({answers.list.items.ID})" title="{answers.list.DELETE}" class="btn red ask" title="{cat.list.DELETE}"> {answers.list.DELETE}&nbsp; <i class="fa fa-trash"></i></a>
												<!-- END perm_del -->
													<!-- BEGIN active -->
													<a href="{answers.list.items.ACTIVE_URL}" class="btn btn-success">{answers.list.ACTIVE}&nbsp; <i class="fa fa-unlock"></i></a>
													<!-- END active -->
													<!-- BEGIN inactive -->
													<a href="{answers.list.items.INACTIVE_URL}" class="btn btn-danger">{answers.list.INACTIVE}&nbsp; <i class="fa fa-lock"></i></a>
													<!-- END inactive -->
												</td>
											</tr>
											<!-- END items -->
										</tbody>
									</table>
									<!-- BEGIN hidden -->
									<input type="hidden" name="sort_order" id="sort_order" value="{answers.list.hidden.VALUE}"/>
									<!-- END hidden -->
									
									<br/>
									<!-- BEGIN perm_edit -->
									<table>
										<tfoot>
										<tr>
											<td class="ac"><span class="btn btn-success"
																 onclick="clickAcquestions('activate', {answers.list.CATID})">{answers.list.ACTIVATE}</span></td>
											<td style="width:10px;"></td>
											<td class="ac"><span class="btn btn-danger"
																 onclick="clickAcquestions('inactivate', {answers.list.CATID})">{answers.list.INACTIVATE}</span></td>
											<td></td>
										</tr>
										</tfoot>
									</table>
									<!-- END perm_edit -->
								</div>
								<!-- END list -->
								
								
								
								
								<!-- BEGIN edit -->
								<div class="portlet-body" id="panel_editbar">


									<div class="form-group form-md-line-input">
										<label class="col-md-3 control-label cursor-pointer" for="txt_variant">Variant</label>
										<div class="col-md-8">
											<input type="text" class="form-control" name="txt_variant"
												   id="txt_variant" value="{answers.edit.VARIANT_VALUE}"
												   placeholder="Variant">
											<div class="form-control-focus"> </div>
										</div>
									</div>

									
									<div class="form-group form-md-line-input">
										<label class="col-md-3 control-label cursor-pointer" for="txt_position">Position</label>
										<div class="col-md-8">
											<input type="text" class="form-control" name="txt_position"
												   id="txt_position" value="{answers.edit.POSITION_VALUE}"
												   placeholder="Position">
											<div class="form-control-focus"> </div>
										</div>
									</div>

									<div class="form-group form-md-checkboxes">
										<label class="col-md-3 control-label" for="txt_active">&nbsp;</label>
										<div class="col-md-6">
											<div class="md-checkbox-list">
												<div class="md-checkbox">
													<input type="checkbox" name="txt_active" id="txt_active" value="1" class="md-check" {answers.edit.ACTIVE_CHK}/>
													<label for="txt_active">
														<span></span>
														<span class="check"></span>
														<span class="box"></span>{answers.edit.ACTIVE}
													</label>
												</div>
											</div>
										</div>
									</div>
									
							
									<div class="tabbable-line boxless tabbable-reversed">
										<ul class="nav nav-tabs">
											<!-- BEGIN tab -->
											<li{answers.edit.tab.CLASS}><a href="#content_{answers.edit.tab.LANG}" data-toggle="tab">{answers.edit.tab.LANG}</a></li>
											<!-- END tab -->
										</ul>
									</div>
									<div class="tab-content">
										<!-- BEGIN tab -->
										<div class="tab-pane {answers.edit.tab.FADE_CLASS}" id="content_{answers.edit.tab.LANG}">
											<div class="portlet box green">
											<div class="portlet-title">
												<div class="caption">
													<i class="fa fa-language"></i>{answers.edit.tab.LANG}
												</div>
											</div>
											<div class="portlet-body form">
											<div class="form-group form-md-line-input">
												<label class="col-md-3 control-label" for="txt_name_{answers.edit.tab.LANG}">Answer</label>
												<div class="col-md-8">
													<input type="text" class="form-control" name="txt_name_{answers.edit.tab.LANG}"
														   id="txt_name_{answers.edit.tab.LANG}" value="{answers.edit.tab.NAME_VALUE}"
														   placeholder="Answer">
													<div class="form-control-focus"> </div>
												</div>
											</div>			
										
											<div class="form-actions">
												<div class="row">
													<div class="col-md-offset-3 col-md-9">
														<button type="button" class="btn blue" id="btn_save" name="btn_save"
																onclick="doanswersAction('save', {answers.edit.ID},  '');"><i class="fa fa-ok"></i> {answers.edit.SAVE}
														</button>
														<button type="reset" class="btn" id="btn_cancel" name="btn_cancel"
																onclick="click_cancel('answers', {answers.edit.CATID});">{answers.edit.CANCEL}</button>
													</div>
												</div>
											</div>
											</div>
											</div>
										</div>
										<!-- END tab -->
									</div>
									
									
								
								</div>
								<!-- END edit -->
								
								
							</form>
						</div>
						<!-- END::questions CAT-->
					</div>
				</div>
				<!-- END answers -->
				






	
				<!-- BEGIN correct_answers -->
				<div class="row">
					<div class="col-md-12">
						<!-- BEGIN::questions CAT-->
						<div class="portlet light bordered">
							<div class="portlet-title">
								<div class="caption font-dark">
									<i class="fa fa-calendar"></i>
									<span class="caption-subject bold uppercase">Correct answers</span>
								</div>
							</div>
							<form action="" method="POST" class="form-horizontal" name="form_correct_answers" id="form_correct_answers">
								<input type="hidden" id="correct_answers_action" name="correct_answers_action" value="">
								<input type="hidden" value="0" id="select_id" name="select_id">
								<input type="submit" id="correct_answers_subm" name="correct_answers_subm" style="display:none;">
							
								<!-- BEGIN list -->
								<div class="portlet-body">
									<div class="clearfix">
										<div class="btn-group">
											<a href="{correct_answers.list.ADD_URL}" class="btn green">
												add &nbsp; <i class="fa fa-plus"></i>
											</a>
										</div>
									</div>
									<br/>
									<table class="table table-striped table-bordered table-hover" width="100%" id="correct_answers_list">
										<thead>
											<tr>
												<th style="width:12px !important;"><input type="checkbox" class="group-checkable" data-set="#correct_answers_list .checkboxes"/></th>
												<th>{correct_answers.list.NAME}</th>
												<th style="width:150px !important;">Variant</th>
												<th style="width:340px !important;"></th>
											</tr>
										</thead>
										<tbody>
											<!-- BEGIN items -->
											<tr id="{correct_answers.list.items.ID}">
												<td id="chk_{correct_answers.list.items.ID}"><input type="checkbox" class="checkboxes" value="{correct_answers.list.items.ID}"/></td>
												<td>{correct_answers.list.items.NAME}</td>
												<td>{correct_answers.list.items.VARIANT}</td>
												<td>
												<!-- BEGIN perm_edit -->
												<a href="{correct_answers.list.items.EDIT_URL}" title="{correct_answers.list.EDIT}" class="btn yellow"> {correct_answers.list.EDIT}&nbsp; <i class="fa fa-pencil"></i></a>
												<!-- END perm_edit -->
												<!-- BEGIN perm_del -->
												<a href="JavaScript:docorrect_answersDelete({correct_answers.list.items.ID})" title="{correct_answers.list.DELETE}" class="btn red ask" title="{cat.list.DELETE}"> {correct_answers.list.DELETE}&nbsp; <i class="fa fa-trash"></i></a>
												<!-- END perm_del -->
												</td>
											</tr>
											<!-- END items -->
										</tbody>
									</table>
									<!-- BEGIN hidden -->
									<input type="hidden" name="sort_order" id="sort_order" value="{correct_answers.list.hidden.VALUE}"/>
									<!-- END hidden -->
									
									<br/>
									<!-- BEGIN perm_edit -->
									<table>
										<tfoot>
										<tr>
											<td class="ac"><span class="btn btn-success"
																 onclick="clickAcquestions('activate', {correct_answers.list.CATID})">{correct_answers.list.ACTIVATE}</span></td>
											<td style="width:10px;"></td>
											<td class="ac"><span class="btn btn-danger"
																 onclick="clickAcquestions('inactivate', {correct_answers.list.CATID})">{correct_answers.list.INACTIVATE}</span></td>
											<td></td>
										</tr>
										</tfoot>
									</table>
									<!-- END perm_edit -->
								</div>
								<!-- END list -->
								
								
								
								
								<!-- BEGIN edit -->
								<div class="portlet-body" id="panel_editbar">


									<div class="form-group form-md-line-input">
										<label class="col-md-3 control-label cursor-pointer" for="txt_variant">Variant</label>
										<div class="col-md-8">
											<input type="text" class="form-control" name="txt_variant"
												   id="txt_variant" value="{correct_answers.edit.VARIANT_VALUE}"
												   placeholder="Variant">
											<div class="form-control-focus"> </div>
										</div>
									</div>

								

									<div class="form-group form-md-checkboxes">
										<label class="col-md-3 control-label" for="txt_active">&nbsp;</label>
										<div class="col-md-6">
											<div class="md-checkbox-list">
												<div class="md-checkbox">
													<input type="checkbox" name="txt_active" id="txt_active" value="1" class="md-check" {correct_answers.edit.ACTIVE_CHK}/>
													<label for="txt_active">
														<span></span>
														<span class="check"></span>
														<span class="box"></span>{correct_answers.edit.ACTIVE}
													</label>
												</div>
											</div>
										</div>
									</div>
									
							
									<div class="tabbable-line boxless tabbable-reversed">
										<ul class="nav nav-tabs">
											<!-- BEGIN tab -->
											<li{correct_answers.edit.tab.CLASS}><a href="#content_{correct_answers.edit.tab.LANG}" data-toggle="tab">{correct_answers.edit.tab.LANG}</a></li>
											<!-- END tab -->
										</ul>
									</div>
									<div class="tab-content">
										<!-- BEGIN tab -->
										<div class="tab-pane {correct_answers.edit.tab.FADE_CLASS}" id="content_{correct_answers.edit.tab.LANG}">
											<div class="portlet box green">
											<div class="portlet-title">
												<div class="caption">
													<i class="fa fa-language"></i>{correct_answers.edit.tab.LANG}
												</div>
											</div>
											<div class="portlet-body form">
											<div class="form-group form-md-line-input">
												<label class="col-md-3 control-label" for="txt_name_{correct_answers.edit.tab.LANG}">Result answer</label>
												<div class="col-md-8">
													<input type="text" class="form-control" name="txt_name_{correct_answers.edit.tab.LANG}"
														   id="txt_name_{correct_answers.edit.tab.LANG}" value="{correct_answers.edit.tab.NAME_VALUE}"
														   placeholder="Result answer">
													<div class="form-control-focus"> </div>
												</div>
											</div>			
										
											<div class="form-actions">
												<div class="row">
													<div class="col-md-offset-3 col-md-9">
														<button type="button" class="btn blue" id="btn_save" name="btn_save"
																onclick="docorrect_answersAction('save', {correct_answers.edit.ID},  '');"><i class="fa fa-ok"></i> {correct_answers.edit.SAVE}
														</button>
														<button type="reset" class="btn" id="btn_cancel" name="btn_cancel"
																onclick="click_cancel('correct_answers', {correct_answers.edit.CATID});">{correct_answers.edit.CANCEL}</button>
													</div>
												</div>
											</div>
											</div>
											</div>
										</div>
										<!-- END tab -->
									</div>
									
									
								
								</div>
								<!-- END edit -->
								
								
							</form>
						</div>
						<!-- END::questions CAT-->
					</div>
				</div>
				<!-- END correct_answers -->
				





				
			</div>
			<!-- END::CONTENT BODY -->
		</div>
		<!-- END::CONTENT -->
	</div>
	<!-- END::CONTAINER -->
	
	
	
	<!-- BEGIN questions -->
	<!-- BEGIN::PAGE LEVEL PLUGINS -->
	<script src="{ROOT}assets/global/scripts/datatable.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
	<!-- END::PAGE LEVEL PLUGINS -->
	<!-- END questions -->
	
	<script type="text/javascript" charset="utf-8">
		$(document).ready(function () {
			$('.group-checkable').click(function(){
				//alert($('.group-checkable').is(":checked"));
				var ch = $('.group-checkable').is(":checked");
				var set = $(".checkboxes");
				$(set).each(function () {
					$(this).prop('checked', ch);
					var v = $(this).val();
					if (ch) {
						$("#chk_" + v).find('span').addClass('checked');
					} else {
						$("#chk_" + v).find('span').removeClass('checked');
					}
				});
			});
			
			$('#datetimepicker').datetimepicker({
				format: 'yyyy-mm-dd hh:ii',
				autoclose: true
			});
		});
	</script>
	<!-- BEGIN cat -->
	<script src="{ROOT}assets/global/scripts/dt/jquery.js" type="text/javascript"></script>
	<!-- BEGIN catedit -->
	<script src="{ROOT}assets/global/plugins/jquery.min.js" type="text/javascript"></script>
	<!-- END catedit -->
	<!-- 
	<script src="{ROOT}assets/global/scripts/datatable.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
	 -->
	 
	<script src="{ROOT}assets/global/scripts/dt/jquery-ui.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/scripts/dt/jquery.dataTables.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/scripts/dt/jquery.dataTables.rowReordering.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/scripts/dt/jquery.dataTables.rowGrouping.js" type="text/javascript"></script>
	
	<script type="text/javascript" charset="utf-8">
		$(document).ready(function () {
			var sortInput = jQuery('#sort_order');
			
			var oTable= $("#questions_cat_list").dataTable({
				"bJQueryUI": true,
				"bDestroy": false,
				"bProcessing": false,
				"bSortable": false,
			});
		
			oTable.rowReordering({
				sURL : 'questions_drag.php', 
				fnAlert: function(message) {
					//alert("order"); 
				}
			});
							//.clear().destroy()
			$('.group-checkable').click(function(){
				//alert($('.group-checkable').is(":checked"));
				var ch = $('.group-checkable').is(":checked");
				$('.checkboxes').prop('checked', ch);
			});
		});
	</script>
	<!-- END cat -->
	<!-- BEGIN questions -->
	<!-- BEGIN list -->
	<script>
	$(document).ready(function () {
		var questionsTable= $("#questions_list").dataTable({
			"bJQueryUI": true,
			"bDestroy": false,
			"bProcessing": true,
			"bSortable": false,
			"processing": true,
			"serverSide": true,
			"ajax":  'ajax_questions_list.php?testid={questions.list.CATID}&rand_num=' + Math.floor((Math.random() * 10000000))
		});
	});
	</script>
	<!-- END list -->
	<!-- END questions -->
	
	<script src="{ROOT}assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>

	<!-- BEGIN::THEME GLOBAL SCRIPTS -->
	<script src="{ROOT}assets/global/scripts/app.js" type="text/javascript"></script>
	<!-- END::THEME GLOBAL SCRIPTS -->
	<!-- BEGIN::PAGE LEVEL SCRIPTS -->
	<script src="{ROOT}assets/pages/scripts/table-datatables-responsive.js" type="text/javascript"></script>
	<!-- END::PAGE LEVEL SCRIPTS -->
	
	<script src="{ROOT}assets/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
	
	<!-- BEGIN questions -->
	<!-- BEGIN edit -->
	<!-- BEGIN tab -->
	<script>
		CKEDITOR.replace( 'txt_text_{questions.edit.tab.LANG}', {
			filebrowserImageBrowseUrl: 'ck.photo.php',
		} );
	</script>
	<!-- END tab -->
	<!-- END edit -->
	<!-- END questions -->