<?php

$language['main']['view']['title'] = 'mCMS Install';
$language['main']['view']['step_x'] = 'Step [x] of [y]';
$language['main']['view']['step_01_title'] = 'Database Setup';
$language['main']['view']['step_02_title'] = 'Administration Panel';
$language['main']['view']['step_03_title'] = 'Site content';
$language['main']['view']['step_04_title'] = 'Finish installation';

$language['index']['view']['install_back'] = 'Back';
$language['index']['view']['install_next'] = 'Continue';
$language['index']['view']['install_submit'] = 'Confirm and Install';
$language['index']['view']['step_error'] = 'You have some installation errors. Please check below.';
$language['index']['view']['step_ok'] = 'Your validation is successful!';
$language['index']['view']['step_01_info'] = 'Provide MySQL Database details';
$language['index']['view']['step_01_db_name'] = 'Database name';
$language['index']['view']['step_01_db_name_info'] = 'Provide Database name';
$language['index']['view']['step_01_db_user'] = 'Database user';
$language['index']['view']['step_01_db_user_info'] = 'Provide Database user';
$language['index']['view']['step_01_db_pass'] = 'Database password';
$language['index']['view']['step_01_db_pass_info'] = 'Provide Database password';
$language['index']['view']['step_01_db_host'] = 'Database host';
$language['index']['view']['step_01_db_host_info'] = 'Provide Database host';
$language['index']['view']['step_01_db_prefix'] = 'Tables prefix';
$language['index']['view']['step_01_db_prefix_info'] = 'Provide tables prefix';
$language['index']['view']['step_01_db_connection'] = 'Connection type';
$language['index']['view']['step_01_db_connection_mysql'] = 'MySQL (Deprecated)';
$language['index']['view']['step_01_db_connection_mysqli'] = 'MySQL Improved (MySQLi)';
$language['index']['view']['step_01_db_connection_pdo'] = 'PDO';
$language['index']['view']['step_01_db_connection_info'] = 'Provide Database connection type';

$language['index']['view']['step_02_info'] = 'Provide Administration panel data';
$language['index']['view']['step_02_admin_name'] = 'Name';
$language['index']['view']['step_02_admin_name_info'] = 'Provide administrator Name';
$language['index']['view']['step_02_admin_email'] = 'E-mail';
$language['index']['view']['step_02_admin_email_info'] = 'Provide administrator E-mail';
$language['index']['view']['step_02_admin_username'] = 'Username';
$language['index']['view']['step_02_admin_username_info'] = 'Provide administrator Username';
$language['index']['view']['step_02_admin_password'] = 'Password';
$language['index']['view']['step_02_admin_password_info'] = 'Provide administrator Password';
$language['index']['view']['step_02_admin_folder'] = 'Admin panel address';
$language['index']['view']['step_02_admin_folder_info'] = 'Provide administration panel path (yoursite.com/adminpath)';

$language['index']['view']['step_03_info'] = 'Your site content details';
$language['index']['view']['step_03_site_encoding'] = 'Encoding';
$language['index']['view']['step_03_site_template'] = 'Site template';
$language['index']['view']['step_03_site_template_01'] = 'Sample 01 - OpenHouse';
$language['index']['view']['step_03_site_template_02'] = 'Sample 02 - HealthCare';
$language['index']['view']['step_03_site_template_03'] = 'Sample 03 - Zentro';
$language['index']['view']['step_03_site_template_04'] = 'Blank site';
$language['index']['view']['step_03_site_template_info'] = 'Select site template';
$language['index']['view']['step_03_site_name'] = 'Site name';
$language['index']['view']['step_03_site_title'] = 'Site title';
$language['index']['view']['step_03_site_keywords'] = 'Keywords';
$language['index']['view']['step_03_site_description'] = 'Description';
$language['index']['view']['step_03_site_email'] = 'E-mail';
$language['index']['view']['step_03_site_author'] = 'Author';
$language['index']['view']['step_03_site_copyright'] = 'Site Copyright';

$language['index']['view']['step_04_info'] = 'Confirm site informations';
$language['index']['view']['step_04_db'] = 'Database';
$language['index']['view']['step_04_admin'] = 'Administration panel';
$language['index']['view']['step_04_content'] = 'Site content';

$language['index']['view']['result_error_connection'] = 'Error connection to Database. ';
$language['index']['view']['result_installation_finished'] = "Congratulations!<br/><br/>
You finished Installation of mCMS.<br/><br/>
Admin area: <a href='{ADMIN_FOLDER}' target='_blank'>{ADMIN_FOLDER}</a><br/>
Username: {ADMIN_USERNAME}<br/>
Password: ****<br/>

{MYSQL_NOTE}
<br/>Please don't forget to check if install.php an install/ folder deleted from site folder.
";

$language['index']['view']['mysql_note'] = "<br/><strong>For Dump MySQL, please open install folder and import <a href='install/dump_mcms.sql' target='_blank'>dump_mcms.sql</a> file.</strong><br/>";

