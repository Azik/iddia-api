<?php

$language['logout']['view']['title'] = 'Log out';

$language['logout']['view']['ok'] = 'Logged out.<br/><br/>now your are redirecting to [firstpage].';
$language['logout']['view']['firstpage'] = 'first page';
