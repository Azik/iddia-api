<?php

$language['mtest']['admin']['title'] = 'mtest Manager';
$language['mtest']['admin']['slide_title'] = 'Categories';
$language['mtest']['admin']['slide_add'] = 'Add new mtest';
$language['mtest']['admin']['slide_name'] = 'Name';
$language['mtest']['admin']['slide_more_txt'] = 'More text';
$language['mtest']['admin']['slide_edit'] = 'Edit';
$language['mtest']['admin']['slide_delete'] = 'Delete';
$language['mtest']['admin']['slide_confirm'] = 'Are you sure?';
$language['mtest']['admin']['slide_active'] = 'Active';
$language['mtest']['admin']['slide_text'] = 'Text';
$language['mtest']['admin']['slide_embed_code'] = 'Embed code';
$language['mtest']['admin']['slide_url'] = 'URL';
$language['mtest']['admin']['slide_save'] = 'Save';
$language['mtest']['admin']['slide_cancel'] = 'Cancel';
$language['mtest']['admin']['mtest_active'] = 'Active';
$language['mtest']['admin']['mtest_inactive'] = 'Blocked';
$language['mtest']['admin']['mtest_activate'] = 'Activate selected';
$language['mtest']['admin']['mtest_inactivate'] = 'Block selected';

$language['mtest']['admin']['up_down'] = 'Up / Down ';
$language['mtest']['admin']['slide_up'] = 'UP';
$language['mtest']['admin']['slide_up_name'] = 'UP [name]';
$language['mtest']['admin']['slide_down'] = 'DOWN';
$language['mtest']['admin']['slide_down_name'] = 'DOWN [name]';

$language['mtest']['admin']['slide_img'] = 'Slide Img';
$language['mtest']['admin']['start_date'] = 'Start date';
$language['mtest']['admin']['end_date'] = 'End date';
$language['mtest']['admin']['date_between'] = 'Даты';
