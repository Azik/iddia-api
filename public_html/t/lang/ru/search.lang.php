<?php

$language['search']['view']['text_result'] = 'Please specify your request.';
$language['search']['view']['no_result'] = 'There are no results matching your search query.';
$language['search']['view']['search_key'] = 'Search keywords';
