<?php

$language['mcms_log']['admin']['title'] = 'Events Log';

$language['mcms_log']['admin']['user'] = 'User';
$language['mcms_log']['admin']['user_id'] = 'User ID';
$language['mcms_log']['admin']['log_time'] = 'Log time';
$language['mcms_log']['admin']['moduleid'] = 'Updated ID';
$language['mcms_log']['admin']['operation'] = 'Operation';
$language['mcms_log']['admin']['page'] = 'Page';
$language['mcms_log']['admin']['ip'] = 'IP';
$language['mcms_log']['admin']['user_agent'] = 'User Agent';
$language['mcms_log']['admin']['module_name'] = 'Module';

?>