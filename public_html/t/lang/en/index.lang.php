<?php

/* Admin section */
$language['index']['admin']['home'] = 'Home';
$language['index']['admin']['dashboard'] = 'Dashboard';
$language['index']['admin']['seperator'] = ' - ';
$language['index']['admin']['title'] = 'mCMS Admin Area';
$language['index']['admin']['images_editor'] = 'Image editor';
$language['index']['admin']['images_crop'] = 'Image crop';
$language['index']['admin']['images_crop_submit'] = 'Crop images';
$language['index']['admin']['images_close'] = ' Close ';
$language['index']['admin']['section_manager'] = 'Section Manager';
$language['index']['admin']['file_manager'] = 'File Manager';
$language['index']['admin']['heading_modules'] = 'Modules';
$language['index']['admin']['heading_features'] = 'Features';
$language['index']['admin']['manage_users'] = 'Manage Users';
$language['index']['admin']['help'] = 'Help';
$language['index']['admin']['logout'] = 'Log Out';
$language['index']['admin']['visit_site'] = 'Visit site';
$language['index']['admin']['siteoptions'] = 'Site Options';
$language['index']['admin']['profile'] = 'Your Profile';
$language['index']['admin']['password'] = 'Change Password';
$language['index']['admin']['not_permission'] = 'You are not authorized to view this page';
$language['index']['admin']['user_right'] = 'You have been granted {0} rights.';
$language['index']['admin']['usertype_superadmin'] = 'Super Admin';
$language['index']['admin']['usertype_admin'] = 'Admin';
$language['index']['admin']['usertype_manager'] = 'Content Manager';
$language['index']['admin']['guest'] = 'Guest';
$language['index']['admin']['wrong_request'] = 'WRONG REQUEST';
$language['index']['admin']['request_done'] = 'DONE';
$language['index']['admin']['confirm_message'] = '';
$language['index']['admin']['root_menu'] = '[menu]';
$language['index']['admin']['title_logs'] = 'Recent Activity log';
$language['index']['admin']['nouser_logs'] = 'No user';
$language['index']['admin']['logs_login_ok'] = 'Logged succes';
$language['index']['admin']['logs_logout'] = 'Logged out';
$language['index']['admin']['logs_login_page'] = 'Login page';
$language['index']['admin']['logs_login_incorrect'] = 'Login incorrect';
$language['index']['admin']['logs_section_update'] = 'Update section';
$language['index']['admin']['logs_section_add'] = 'Add section';
$language['index']['admin']['logs_section_delete'] = 'Delete section';
$language['index']['admin']['ajax_photo_file'] = 'Image File';
$language['index']['admin']['ajax_photo_file_upload'] = 'Upload Image';

// Photo select
$language['index']['admin']['file_select_title'] = 'Select file';
$language['index']['admin']['image_select_title'] = 'Select image';
$language['index']['admin']['image_select_loading'] = 'Loading...';
$language['index']['admin']['ajax_error'] = 'Error...';
$language['index']['admin']['image_tab_select'] = 'Select image';
$language['index']['admin']['image_tab_upload'] = 'Uplaod image';
$language['index']['admin']['invalid_type'] = 'File type is invalid. Contact to admin.';
$language['index']['admin']['image_search'] = 'Search image';

// Select link
$language['index']['admin']['link_select_title'] = 'Select link from site sections';
$language['index']['admin']['link_select_loading'] = 'Loading pages...';

/* View section */
$language['index']['view']['title_home'] = 'Home';
$language['index']['view']['title_404'] = '404 Not found';
$language['index']['view']['title_404_info'] = '404 The page you requested was not found';
$language['index']['view']['where_space'] = ' :: ';
$language['index']['view']['error'] = 'Error';
$language['index']['view']['error_info'] = 'Error while loading page.';
$language['index']['view']['follow_us_at'] = 'Follow our servcies';
$language['index']['view']['foot_top'] = 'Top';
$language['index']['view']['news_archive'] = 'News & Events';
$language['index']['view']['news'] = 'News & Events';
$language['index']['view']['news_categories'] = 'Categories';
$language['index']['view']['contacts'] = 'Contact Form';
$language['index']['view']['module_videos'] = 'Videos';
$language['index']['view']['module_events'] = 'Events';
$language['index']['view']['more'] = 'More';
$language['index']['view']['monthArr'] = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
$language['index']['view']['date_arr'] = array('january', 'february', 'march', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december');

$language['index']['view']['contact_us'] = 'Contact us';
$language['index']['view']['contact_us_info'] = 'You can communicate your questions= via the following form';
$language['index']['view']['contact_us_title'] = 'Contact us';
$language['index']['view']['contact_fullname'] = 'Name';
$language['index']['view']['contact_name'] = 'First Name';
$language['index']['view']['contact_surname'] = 'Last Name';
$language['index']['view']['contact_email'] = 'Email';
$language['index']['view']['contact_phone'] = 'Phone';
$language['index']['view']['contact_subject'] = 'Subject';
$language['index']['view']['contact_text'] = 'Message';
$language['index']['view']['captcha_code'] = 'Image captcha';
$language['index']['view']['contact_send'] = 'Send';
$language['index']['view']['contact_sended'] = 'Thank You. Your Message has been sent';
$language['index']['view']['contact_send_error'] = 'Error while sending message';

$language['index']['view']['mail_subject'] = 'mCMS - Contact form';
$language['index']['view']['mail_body'] = '<b>Name:</b> [full_name_value]<br/>
<b>E-mail:</b> [email_value]<br/>
<b>Subject:</b> [subject_value]<br/>
<b>Message:</b> [message_value]<br/>
';

$language['index']['view']['search'] = 'Search';
$language['index']['view']['search_placeholder'] = 'Type search words';
$language['index']['view']['events'] = 'Events';
$language['index']['view']['video_info'] = 'Videos from events';

$language['index']['view']['module_subscribe'] = 'Subscribe to our newsletter';
$language['index']['view']['module_subscribe_info'] = 'Subscribe our newsletter to receive the latest information in your inbox!';
$language['index']['view']['module_subscribe_name'] = 'Your Name';
$language['index']['view']['module_subscribe_email'] = 'Your Email';
$language['index']['view']['module_subscribe_phone'] = 'Mobile number';
$language['index']['view']['module_subscribe_send'] = 'SUBSCRIBE';
$language['index']['view']['module_subscribe_sended'] = 'Your message has been sent!';
$language['index']['view']['module_subscribe_send_error'] = 'Error sending message';
$language['index']['view']['subscribe_subject'] = 'mCMS - Subscribe';
$language['index']['view']['subscribe_body'] = 'Hello [full_name_value],<br/>
You subscribed to system<br/>
<b>E-poçt:</b> [email_value]<br/>
<b>Phone:</b> [phone_value]<br/><br/>
You can cancel your subscription at any time.<br/>
<a href="[unsubscribe_link]">Unsubscribe</a> [unsubscribe_link]<br/><br/>
';
$language['index']['view']['unsubscribe_title'] = 'Unsubscribe<strong>it</strong>';
$language['index']['view']['unsubscribe_ok'] = 'Your newsletter subscription has been approved!';
$language['index']['view']['unsubscribe_error'] = 'There was an error while unsubscribing. Please try again!';

$language['index']['view']['all_cat'] = 'Categories';

// Languages
$language['index']['view']['lang_list'] = array(
    'az' => 'Azerbaijan',
    'ru' => 'Russian',
    'en' => 'English',
);
