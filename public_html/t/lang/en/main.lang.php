<?php

/* ə */
$language['main']['admin']['title'] = 'mCMS Dashboard';
$language['main']['admin']['title_info'] = 'Multilingual Content Management System';
$language['main']['admin']['section_manager'] = 'Section Manager';
$language['main']['admin']['section_manager_number'] = '[x]';
$language['main']['admin']['section_manager_desc'] = 'Pages in [x] blocks';
$language['main']['admin']['form_entries'] = 'Scientific research';
$language['main']['admin']['form_entries_number'] = '[x]';
$language['main']['admin']['form_entries_desc'] = 'Scientific research';
$language['main']['admin']['file_manager'] = 'File Manager';
$language['main']['admin']['file_manager_number'] = '[x]';
$language['main']['admin']['file_manager_desc'] = 'Uploaded Files';
$language['main']['admin']['modules'] = 'Manage Modules';
$language['main']['admin']['modules_number'] = '[x]';
$language['main']['admin']['modules_desc'] = 'Installed modules';
$language['main']['admin']['news'] = 'News Manager';
$language['main']['admin']['news_number'] = '[x]';
$language['main']['admin']['news_desc'] = 'Site news';
$language['main']['admin']['news_statistics'] = 'News Statistics';
$language['main']['admin']['news_statistics_count'] = 'Site News';
$language['main']['admin']['news_statistics_info'] = 'News Views';
