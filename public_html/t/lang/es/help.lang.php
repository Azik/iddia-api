<?php

$language['help']['admin']['title'] = 'Help';
$language['help']['admin']['title_sub'] = 'mCMS v 5.1.0';

$language['help']['admin']['blocks'] = array(
    // Information
    '1' => array(
        'name' => 'Information',
        'icon' => 'info',
        'tabs' => array(
            '1' => array(
                'name' => 'Introduction',
                'text' => '
<p>This site and its dynamic operated sections are installed in a control panel mCMS. You need to follow this guide to manage the site.</p>
<p>We recommend to use Chrome, Firefox or Opera to work with mCMS content management system.</p>
',
            ),
            '2' => array(
                'name' => 'Structure of the system',
                'text' => '
<p>System can be in different languages.</p>
<p>The structure of the system is as follow:</p>
<p><em>4 type of menus</em> (bottom_menu, top_menu, middle_menu, foot_menu):</p>
<p><em>Greeting text</em> və <em>different modules</em> (blocks, news, gallery an etc.).</p>

',
            ),
            '3' => array(
                'name' => 'Contact',
                'text' => '
<p>This system was created by MicroPHP Web Development team.</p>
<p>if you have any questions you can contact with us.</p>

<p style="text-align:right"><em>URL: <a href="http://www.microphp.com" target="_blank">www.microphp.com</a></em></p>
<p style="text-align:right"><em>E-mail: info@microphp.com</em></p>
<p style="text-align:right"><em>Cell:	(+994 50) 6482737</em></p>

				',
            ),
        ),
    ),

    // B: ADMINISTRATORS
    '2' => array(
        'name' => 'For adminstartions',
        'icon' => 'support',
        'tabs' => array(
            '1' => array(
                'name' => 'Minimum requirements',
                'text' => '
<table style="width:100%;">
<thead>
	<tr class="help_table">
		<th>Software</th>
		<th style="width:120px;">Minimum requirements</th>
		<th style="width:120px;">Recommended</th>
		<th style="width:140px;">Web Site</th>
	</tr>
</thead>
<tbody>
	<tr class="help_table">
		<td>PHP, Multibyte String support</td>
		<td>PHP 5.3</td>
		<td>PHP latest (PHP 7.0 or above)</td>
		<td><a href="http://www.php.net/" target="_blank">http//php.net</a></td>
	</tr>
	<tr class="help_table">
		<td>MySQL</td>
		<td>5</td>
		<td>MySQL latest</td>
		<td><a href="http://www.mysql.com/" target="_blank">http://www.mysql.com</a></td>
	</tr>
	<tr class="help_table">
		<td>Apache</td>
		<td>2.0</td>
		<td>2.4</td>
		<td><a href="http://httpd.apache.org/" target="_blank">http://httpd.apache.org</a></td>
	</tr>
	<tr class="help_table">
		<td>or nginx</td>
		<td>1.5.9</td>
		<td>1.7.6</td>
		<td><a href="http://nginx.org/" target="_blank">http//nginx.org</a></td>
	</tr>
	<tr class="help_table">
		<td>Apache Module mod_rewrite</td>
		<td></td>
		<td></td>
		<td><a href="http://httpd.apache.org/docs/current/mod/mod_rewrite.html" target="_blank">mod_rewrite</a></td>
	</tr>
</tbody>
</table>
				',
            ),
            '20' => array(
                'name' => 'Global server installation',
                'text' => '
				<strong>Installation of system in CPanel</strong><br/>
				',
            ),
            '30' => array(
                'name' => 'mCMS Installation',
                'text' => '
				<strong>For install system</strong><br/>
				Type in Browser http://yoursite.com/install.php<br/>
				Go by steps and Install your site.<br/>
				After Installation complete be sure install.php and folder install/ deleted from site folder.
				',
            ),
        ),
    ),

    // B: SITE MANAGERS
    '3' => array(
        'name' => 'For content managers',
        'icon' => 'linux',
        'tabs' => array(
            '1' => array(
                'name' => 'Control Panel',
                'text' => '
<p>Saytdakı bütün yazılmış modulları, bölmələri mCMS İdarəetmə panelinin köməkliyi ilə əlavə etmək, dəyişmək və s. kontrol etmək mümkündür. </p>
<p>The given address should be included to enter the admin panel the login page will appear which you have to include your username and password to login the system and to be able to work with system modules.</p>
<p>After sucessfully log in process main page of the system will appear.</p>
<p><img src="help/login_page.jpg" alt="login" class="col-md-12"></p> 
<p>This page is Dashboard and carries on different statistical information, for example how many files you have uploaded to the modules, statistical graphics about news module and activity log.</p>
<p class="col-md-12">You need to click "Log Out button from the above menu to leave the system"</p>
<p><img src="help/logout_3_18.jpg" alt="Logout 1" class="col-md-6"></p>',
            ),
            '2' => array(
                'name' => 'Uploading files to system',
                'text' => '
<p>First of all you need to go to the “<a href="index.php?module=file_manager">Files</a>” to upload files, pictures and documents to the system.</p>
<p>You can click the specific section of file manager to select files or just drag and drop method. Files will automaticallu upload to the system.</p>
<p><img src="help/mcms_file_manager.jpg" alt="Upload" class="col-md-12"></p>
<p>File Id and Copy URL is for to include files to the different section of the website. You can crop images in different size with  action button. </p>
<p><img src="help/file_manager_3_4.jpg" alt="Image crop" class="col-md-12"></p>
<p>Click delete button to delete uploaded files.</p>
<p><br/><br/></p>
				',
            ),
            '3' => array(
                'name' => 'Using of the images in file manager',
                'text' => '
<p>For using uploded images from file manager you need to follow rules in below:</p>
<p><strong>{IMAGE_<em>ID</em>_<em>WIDTH</em>XX}</strong> - To show ID No. picture in specific WIDTH size (e.g: {IMAGE_160_300XX} - 160 No. picture, width - the size of the width of 300px)</p>
<p><strong>{IMAGE_<em>ID</em>_S<em>WIDTH</em>X<em>HEIGHT</em>}</strong> - To show ID No. picture in specific WIDTH and HEIGHT size (e.g: {IMAGE_160_S400X300} - 160 No. picture, width - the size of the width of 400px, height - the size of the height of 300px)</p>
<p><strong>{IMAGE_<em>ID</em>_XX<em>HEIGHT</em>}</strong> - To show ID No. picture in specific HEIGHT size (e.g: {IMAGE_160_XX300} - 160 No. picture, height - the size of the height of 300px)</p>
<p><img src="help/file_manager_3_22.jpg" alt="File manager - images" class="col-md-12"></p>
',
            ),
            '4' => array(
                'name' => 'Section Manager',
                'text' => '
<p>This module is to add, edit or delete websites main menus and submenus, which contains 4 menu blocks: top menu, middle menu, foot menu, bottom menu, static menu</p>
<p><img src="help/section_manager_3_5.jpg" alt="Section Manager" class="col-md-12"></p>
<p><strong>To add new menu</strong></p>
<p>Just click on the desired menu to add a menu or sub menu.</p>
<p>You have to fill information in opened page.</p>
<p><img src="help/section_manager_3_6.jpg" alt="Add Section" class="col-md-12"></p>
<p><strong><em>Index</em></strong> – may need for programmers, to show any information in the main page of website in future.</p>
<p><strong><em>Visible</em></strong> – to set the section visibility in the website</p>
<p><strong><em>New Window</em></strong> – to open page in new window (when clicks from menu in the website)</p>
<p><strong><em>Template</em></strong> – Structure of the design of website. </p>
<p><strong><em>Type</em></strong> –  type of the page<br/>
<ul>
	<li>Content - Information page to add content</li>
	<li>Welcome - Welcome text in the main page of the website</li>
	<li>No link content - for the footer of website. For example contact, map and etc.</li>
	<li>Link - URL to access a page when click menu</li>
	<li>Module - One of the modules in the website.</li>
</ul>
<p>You have to mention url if you choose link as a type of page. If you want to include links in different languages you have to mention as follows.</p>
<p><em>For example:</em> http://{LANG}.microphp.com </p>
<p><em>Result:</em> English: http://en.microphp.com, Spanish: http://es.microphp.com  </p>
<br/>
<em>Modules</em>
<ul>
	<li>Main page - redirect to the main page of website</li>
</ul>
	
</p>
<p>After setting the proporties of the web page following infromation filled.</p>
<p>The data shall be determined according to the language of the site.</p>
<p><em>Name</em> - Name of the page</p>
<p><em>Header img(ID)</em> - Id of the images from file manager. It will show image if the page shown with image.</p>
<p><em>Comment</em> - A short information about a page</p>
<p><em>Text</em> - text of the page</p>
<p><em>Link</em> - Included link will open if you click the name of the page.</p>
<p><em>Slug</em> - The URL of the link of page. It is for SEO (Search Engine Optimization). The system will automatically generate slug if you do not put anything in this form.</p>
<p>You can save all included information with save button, or cancel with cancel button.</br> Drag and drop method is used to change the order of the menus.</p>
<p><img src="help/section_manager_3_7.jpg" alt="Edit Section" class="col-md-12"></p>
<p><img src="help/section_manager_3_8.jpg" alt="Move Section" class="col-md-4"></p>
				',
            ),
            '5' => array(
                'name' => 'News manager',
                'text' => '
<p>You need to click News module in order to manage news of the website.</p>
<p><img src="help/mcms_news_manager.jpg" alt="news manager" class="col-md-12"></p>
<p>You can add News or Events category by clicking add category button</p>
<p><em>Index</em> - For showing category in the main page of the website</p>
<p><em>Active</em> – For showing category in the website</p>
<p><em>Event</em> - For showing news in the category as event. If Event is checked it automatically deleted from website when deadline finish</p>
<p>After setting the proporties of the web page following infromation filled.</p>
<p>The data shall be determined according to the language of the site.</p>
<p><em>Name</em> - Name of category</p>
<p><em>Description</em> - The text of category.</p>
<p><img src="help/news_manager_3_10.jpg" alt="add category" class="col-md-12"></p>
<p><img src="help/news_manager_3_11.jpg" alt="list category" class="col-md-12"></p>
<p><strong>Add news to website </strong> First you need to select in which category(news or event) you want add information and click on it.</p>
<p>Then click "Add news" button to add news</p>
<p><img src="help/mcms_add_news.jpg" alt="add news" class="col-md-12"></p>
<p>Fill information in opened page</p>
<p><img src="help/news_manager_3_12.jpg" alt="add news" class="col-md-12"></p>
<p><em>Date</em> - Date of news. If you set the date in future, news will publish in this date automatically</p>
<p><em>Active</em> – For showing news in the website</p>
<p><em>Index</em> - For showing category in the main page of the website</p>
<p><em>Template</em> – Design structure of news. Default seçilibsə susmaya görə təyin edilmiş dizayn. </p>
<p>After setting the proporties of the web page following infromation filled.</p>
<p><em>Name</em> - News name</p>
<p><em>Image</em> - Image id from file manager.</p>
<p><em>Comment</em> - Short information about news</p>
<p><em>Text</em> - News text</p>
<p><em>Slug</em> - The URL of the link of page. It is for SEO (Search Engine Optimization). The system will automatically generate slug if you do not put anything in this form.</p>
<p>Click save button to save information</p>',
            ),
            '6' => array(
                'name' => 'Gallery',
                'text' => '
<p>This module let you to create many galleries. You will first create a new gallery and then add files into it as following picture.</p>
<p><img src="help/mcms_gallery_manager.jpg" alt="Gallery manager" class="col-md-12"></p>
<p><strong>Qalereyanı saytda göstərmək</strong> üçün lazımi qalereyanın qarşısındakı kodu ({GALLERY_nömrə}) Bölmə redaktorunda, Xəbərlərdə və ya digər mətn olan bölmələrdə yerləşdirmək kifayətdir.</p>
',
            ),

            '7' => array(
                'name' => 'Options',
                'text' => '
<p>Options section lets you to set the in which language you want to produce your website, add keywords, add copyright information, set the language for the adim panel and etc.</p>',
            ),
        ),
    ),

    // B: HTML CODERS AND DESIGNERS
    '4' => array(
        'name' => 'For HTMl coders and Designers',
        'icon' => 'html5',
        'tabs' => array(
            '1' => array(
                'name' => 'HTML structure of website',
                'text' => '<p>All HTML files are saved in one folder</p>
				<p><span style="color:#f00">$template_folder = "t/";</span> - The folder containing the template files, language files, folder for upload documents and images.</p>
				<p><span style="color:#f00">$templates_view = array();</span> - Information about template folder</p>
				<p><span style="color:#f00">$templates_view["index.header"]</span> - Header of website (header)</p>
				<p><span style="color:#f00">$templates_view["index.footer"]</span> - Footer of website (footer)</p>
				<p><span style="color:#f00">$templates_view["main"]</span> - Index page</p>
				<p><span style="color:#f00">$templates_view["inside"]</span> - Inside page</p>
				<p><span style="color:#f00">$templates_view["gallery"]</span> - Gallery page</p>
				<p><span style="color:#f00">$templates_view["news"]</span> - News archive</p>
				<p><span style="color:#f00">$templates_view["news_more"]</span> - More information about news</p>
				<p><span style="color:#f00">$templates_view["search"]</span> - Search page</p>
				<p><span style="color:#f00">$templates_view["contacts"]</span> - For contact module</p>
				<p><span style="color:#f00">$templates_view["404"]</span> - Error page (header and footer is not included)</p>
				',
            ),
            '2' => array(
                'name' => 'Rule for all pages',
                'text' => '
				<p><span style="color:#f00">{SITE_COPYRIGHT}</span> - Copyright information, these information are taken from options section of the admin panel</p>
				<p><span style="color:#f00">{CURR_YEAR}</span> - Information about Current year.</p>
				<p><span style="color:#f00">{INDEX_URL}</span> - URL of index page</p>
				<p></p>
				<h2>Integration HTML files to the mCMS system</h2>
				<p>{SITE_FOLDER} - gives folder path, to be able to read your imported files to HTML add {SITE_FOLDER} in front of them (e.g. {SITE_FOLDER}css/style.css)</p>
				<h3>HEAD - Meta Tags</h3>
				<p>In the demo version of the system there are .TPL files in the template folder, index.header.tpl defines the header and index.footer.tpl defines the footer section of the website as they are usually static and do not need to change in every page it is useful to seperate them.</p>
				<p>"Site Options" menu in the admin panel which almost describes metadata within index.header.tpl file</p>
				<img src="help/mcms_site_options_diagram.jpg" width="600px" alt="mCMS Site options">
				<ol>
					<li>Encoding - meta charset="{CHARSET}"</li>
					<li><p>Site name and title  are used in the "title" tags of HTML (e.g. &lt;title&gt;{TITLE_HEADER}{TITLE_SEPERATOR}{TITLE}{TITLE_INSIDE2}&lt;/title&gt;)</p>
					{TITLE_HEADER} - defines Site name<br>
					
					{TITLE_SEPERATOR} - defines "-" sign between Site name and site title<br>
					
					{TITLE} - defines site title<br>
					
					{TITLE_INSIDE2} - defines other page titles of website for example about us, gallery and etc.
					</li>
					<li>Keywords - content element of meta tag (e.g. &lt;meta name="keywords" content="{KEYWORDS}"&gt;)</li>
					<li>Description - content element of meta tag (e.g. &lt;meta name="description" content="{DESCRIPTION}"&gt;)</li>
				</ol>
				<p>As shown in the above picture you can add name, keywords and etc in different languages which are very good for SEO because each title has relation to the content on the page.</p>
				<p>Author - {AUTHOR}, Email - {META_EMAIL} and Site copyright - {SITE_COPYRIGHT} are for the footer of the website (index.footer.tpl).</p>
				
				<h3>Navigation</h3>
				<p>There are 4 menu types for different section of the template: top menu, middle menu, foot menu and bottom menu. It depends on you to define in which section you want to put these menus.<br>
				<p>Example: <br>
				<p>You have to pay attention with orders. When you put BEGIN top_menu you have to close it other case it wont work.</p>
				<img src="help/top_menu.jpg" width="600px" alt="mCMS login page">
				</p>
				&lt;!-- BEGIN top_menu --&gt; - shows beginning of the top_menu.</br>
				{top_menu.NAME} - define menu names </br>
				{top_menu.NAME_UP} - Shows menu with upper letters</br>
				{top_menu.TITLE} - title of the menu. (title attribute of an &lt;a&gt; tag)</br>
				{top_menu.COMMENT} - comment section from section manager of admin panel </br>
				{top_menu.TEXT} - text section from section manager of admin panel </br>
				{top_menu.BLANK}
				{top_menu.SLUG} - auto generated slugs from section manafer of admin panel</br>
				{top_menu.URL} - menu link for the href element of an &lt;a&gt; tag</br>

				&lt;!-- BEGIN sub --&gt; -shows sub menus </br>

				&lt;!-- BEGIN menu --&gt;  
				{top_menu.sub.menu.CUR_CLASS} -class="current"</br>
				{top_menu.sub.menu.ACT_CLASS} -class="active"</br>
				{top_menu.sub.menu.DDWN_CLASS} -class="dropdown"</br>
				{top_menu.sub.menu.CLASS_SUB} -class="dropdown-submenu" OR class="last-element"</br>
				{top_menu.sub.menu.URL} - submenu link for the href element of an &lt;a&gt; tag</br>
				{top_menu.sub.menu.TITLE} - title of submenu</br>
				{top_menu.sub.menu.NAME} - name of submenu</br>
				{top_menu.sub.menu.ID} - ID of sub menu</br>
				&lt;!-- END menu --&gt;</br>
				&lt;!-- END sub --&gt;</br>
				&lt;!-- END top_menu --&gt;</br>
				</p>
				<p>Bottom_menu, foot_menu and middle_menu is also follows the same steps as above you can find them at template_rules.txt file.</p>
				
				<h1>Sliders</h1>
				<img src="help/slider.jpg" width="600px" height="300px" alt="mCMS login page">
				<p>Sliders are also begins and ends with BEGIN slider,END slider elements. Some explanation in below:</p>
				&lt;!-- BEGIN slider --&gt;</br>
				&lt;!-- BEGIN items --&gt;</br>
				{slider.items.NDX} - shows slider numbers in order.<br>
				{slider.items.CLASS_ACTIVE} - indicate css elements of HTML (class="active")</br>
				{slider.items.CLASS_VAL_ACTIVE} - only indicate "active". </br>
				{slider.items.IMG_FILE} - Image path. Autmotically takes from slide manager of admin panel.</br>
				{slider.items.TITLE} - alt atribute of img tag in HTML</br>
				{slider.items.NAME} - Slider name from slider managers.</br>
				{slider.items.TEXT_NOTAG} - Short information about slider, TEXT in slider manager</br>
				{slider.items.HREF} - href atribute of a tag in HTML. (e.g. href="URL")</br>
				{slider.items.MORE_TXT} - More text form from slide manager.</br>
				&lt;!-- END items --&gt;</br>
				&lt;!-- END slider --&gt;</br>
				
				<h1>Blocks</h1>
				<p>You can add unlimited numbers of blocks in different section of website. In demo version of OpenHouse template "Top Holiday Destinations" gallery, Testimonials, and gallery of the companies added from block manager.</p>
				<p>When you add new category to block manager it gives you HTML ID which you will use it in HTML to show different sections. For example</p>
				<p><img src="help/blocks.jpg" width="600px" alt="mCMS blocks"></p>
				&lt;!-- BEGIN blocks_14 --&gt; - here 14 is HTML ID from block manager. </br>
				&lt;!-- BEGIN items --&gt; item elements are all elements inside block category`</br>
				{blocks_14.items.IMG_FULL} - Image full path <br>
				{blocks_14.items.NAME} - Name from block manager inside block category<br>
				{blocks_14.items.TEXT_NOTAG} - Text from block manager inside block category<br>
				&lt;!-- END items --&gt;</br>
				&lt;!-- END blocks_14 --&gt;</br>
				<p>In this example social icons in the footer are also included from blocks manager.</p>
				
				<h1>Inside pages</h1>
				<p>There is no begin and end elements in inside pages if it only consists of Title and text. You can only add {NAME} as title and {TEXT} as more text about your post</p>
				',
            ),

            /* '5' => array(
                'name' => "Name25",
                'text' => 'Text255'
            ), */
        ),
    ),

);
