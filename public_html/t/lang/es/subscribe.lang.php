<?php

$language['subscribe']['admin']['title'] = 'Subscribe';

$language['subscribe']['admin']['title_list'] = 'List of subscribers';
$language['subscribe']['admin']['title_templates'] = 'Mail templates';
$language['subscribe']['admin']['title_broadcast'] = 'Broadcast';
$language['subscribe']['admin']['email'] = 'E-mail';
$language['subscribe']['admin']['name'] = 'Name';
$language['subscribe']['admin']['phone'] = 'Phone';
$language['subscribe']['admin']['lang'] = 'Lang';
$language['subscribe']['admin']['date'] = 'Date';
$language['subscribe']['admin']['edit'] = 'Edit';
$language['subscribe']['admin']['delete'] = 'Delete';
$language['subscribe']['admin']['confirm'] = 'Are you sure?';

$language['subscribe']['admin']['active'] = 'Active';
$language['subscribe']['admin']['inactive'] = 'Blocked';
$language['subscribe']['admin']['activate'] = 'Activate selected';
$language['subscribe']['admin']['inactivate'] = 'Block selected';
$language['subscribe']['admin']['subscribers_total'] = 'Total subscribers';
$language['subscribe']['admin']['subscribers_active'] = 'Active subscribers';

$language['subscribe']['admin']['templates_add'] = 'Add template';
$language['subscribe']['admin']['templates_active'] = 'Active';
$language['subscribe']['admin']['templates_name'] = 'Name';
$language['subscribe']['admin']['templates_template'] = 'HTML Template';
$language['subscribe']['admin']['templates_save'] = 'Save';
$language['subscribe']['admin']['templates_cancel'] = 'Cancel';

$language['subscribe']['admin']['broadcast_add'] = 'Add broadcast';
$language['subscribe']['admin']['broadcast_active'] = 'Active';
$language['subscribe']['admin']['broadcast_name'] = 'Name';
$language['subscribe']['admin']['broadcast_template'] = 'Select Template';
$language['subscribe']['admin']['broadcast_save'] = 'Save';
$language['subscribe']['admin']['broadcast_cancel'] = 'Cancel';

$language['subscribe']['admin']['subscribe_news'] = 'news';
$language['subscribe']['admin']['subscribe_action'] = 'action';
$language['subscribe']['admin']['broadcast_template'] = 'Template';
$language['subscribe']['admin']['broadcast_startdate'] = 'Date of sending';
$language['subscribe']['admin']['broadcast_text'] = 'Text';
$language['subscribe']['admin']['blocks_mail_stats'] = 'Sended/Waiting';
