<?php

$language['main']['view']['title'] = 'mCMS Install';
$language['main']['view']['step_x'] = 'Step [x] of [y]';
$language['main']['view']['step_01_title'] = 'Database Setup';
$language['main']['view']['step_02_title'] = 'Administration Panel';
$language['main']['view']['step_03_title'] = 'Site content';
$language['main']['view']['step_04_title'] = 'Finish installation';

$language['index']['view']['install_back'] = 'Back';
$language['index']['view']['install_next'] = 'Continue';
$language['index']['view']['install_submit'] = 'Submit';
$language['index']['view']['step_error'] = 'You have some installation errors. Please check below.';
$language['index']['view']['step_ok'] = 'Your validation is successful!';
$language['index']['view']['step_01_info'] = 'Provide MySQL Database details';
$language['index']['view']['step_01_db_name'] = 'Database name';
$language['index']['view']['step_01_db_name_info'] = 'Provide Database name';
$language['index']['view']['step_01_db_user'] = 'Database user';
$language['index']['view']['step_01_db_user_info'] = 'Provide Database user';
$language['index']['view']['step_01_db_pass'] = 'Database password';
$language['index']['view']['step_01_db_pass_info'] = 'Provide Database password';
$language['index']['view']['step_01_db_host'] = 'Database host';
$language['index']['view']['step_01_db_host_info'] = 'Provide Database host';
$language['index']['view']['step_01_db_prefix'] = 'Tables prefix';
$language['index']['view']['step_01_db_prefix_info'] = 'Provide tables prefix';
$language['index']['view']['step_01_db_connection'] = 'Connection type';
$language['index']['view']['step_01_db_connection_mysql'] = 'MySQL (Deprecated)';
$language['index']['view']['step_01_db_connection_mysqli'] = 'MySQL Improved';
$language['index']['view']['step_01_db_connection_pdo'] = 'PDO';
$language['index']['view']['step_01_db_connection_info'] = 'Provide Database connection type';

$language['index']['view']['step_02_info'] = 'Provide Adminsitration panel data';
$language['index']['view']['step_02_admin_name'] = 'Name';
$language['index']['view']['step_02_admin_name_info'] = 'Provide administrator Name';
$language['index']['view']['step_02_admin_email'] = 'E-mail';
$language['index']['view']['step_02_admin_email_info'] = 'Provide administrator E-mail';
$language['index']['view']['step_02_admin_username'] = 'Username';
$language['index']['view']['step_02_admin_username_info'] = 'Provide administrator Username';
$language['index']['view']['step_02_admin_password'] = 'Password';
$language['index']['view']['step_02_admin_password_info'] = 'Provide administrator Password';
$language['index']['view']['step_02_admin_folder'] = 'Admin panel address';
$language['index']['view']['step_02_admin_folder_info'] = 'Provide administration panel path';

