<?php

$language['categories']['admin']['title'] = 'Categories';

$language['categories']['admin']['save'] = 'Save';
$language['categories']['admin']['cancel'] = 'Cancel';
$language['categories']['admin']['cat_title'] = 'Categories';
$language['categories']['admin']['cat_add'] = 'Add category';
$language['categories']['admin']['cat_active'] = 'Active&nbsp;&nbsp;&nbsp;';
$language['categories']['admin']['cat_index'] = 'Index';
$language['categories']['admin']['cat_inactive'] = 'Blocked';
$language['categories']['admin']['cat_activate'] = 'Activate selected';
$language['categories']['admin']['cat_inactivate'] = 'Block selected';
$language['categories']['admin']['cat_name'] = 'Name';
$language['categories']['admin']['cat_imgid'] = 'Img id';
$language['categories']['admin']['cat_header'] = 'Description';
$language['categories']['admin']['cat_slug'] = 'Slug';
$language['categories']['admin']['cat_position'] = 'Position';
$language['categories']['admin']['cat_edit'] = 'Edit';
$language['categories']['admin']['cat_delete'] = 'Delete';
$language['categories']['admin']['cat_confirm'] = 'Are you sure?';
$language['categories']['admin']['cat_save'] = 'Save';
$language['categories']['admin']['cat_cancel'] = 'Cancel';

?>