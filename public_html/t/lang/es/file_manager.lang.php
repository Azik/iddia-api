<?php

$language['file_manager']['admin']['title'] = 'File Manager';
$language['file_manager']['admin']['category'] = 'Category:';
$language['file_manager']['admin']['cat_all'] = 'All';
$language['file_manager']['admin']['upload'] = 'Upload';
$language['file_manager']['admin']['upload_file'] = 'File:';
$language['file_manager']['admin']['upload_title'] = 'Name:';
$language['file_manager']['admin']['search'] = 'Find';
$language['file_manager']['admin']['search_text'] = 'Search:';
$language['file_manager']['admin']['search_help'] = '(use special characters: * and ?)';
$language['file_manager']['admin']['invalid_type'] = '"{0}" type file is invalid. Contact to admin.';
$language['file_manager']['admin']['file_thumb'] = 'Thumbnail';
$language['file_manager']['admin']['file_id'] = 'File Id';
$language['file_manager']['admin']['file_name'] = 'File Name';
$language['file_manager']['admin']['copy_url'] = 'Copy&nbsp;URL';
$language['file_manager']['admin']['file_originalname'] = 'Original Name';
$language['file_manager']['admin']['file_extension'] = 'Extension';
$language['file_manager']['admin']['file_title'] = 'Title';
$language['file_manager']['admin']['file_action'] = 'Action';
$language['file_manager']['admin']['file_delete'] = 'Delete';
$language['file_manager']['admin']['file_delete_confirm'] = 'Are you sure?';
$language['file_manager']['admin']['image_opener'] = 'Open editor';
