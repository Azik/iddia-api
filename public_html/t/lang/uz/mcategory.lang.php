<?php

$language['mcategory']['admin']['title'] = 'mcategory Manager';
$language['mcategory']['admin']['slide_title'] = 'Categories';
$language['mcategory']['admin']['slide_add'] = 'Add new mcategory';
$language['mcategory']['admin']['slide_name'] = 'Name';
$language['mcategory']['admin']['slide_more_txt'] = 'More text';
$language['mcategory']['admin']['slide_edit'] = 'Edit';
$language['mcategory']['admin']['slide_delete'] = 'Delete';
$language['mcategory']['admin']['slide_confirm'] = 'Are you sure?';
$language['mcategory']['admin']['slide_active'] = 'Active';
$language['mcategory']['admin']['slide_text'] = 'Text';
$language['mcategory']['admin']['slide_embed_code'] = 'Embed code';
$language['mcategory']['admin']['slide_url'] = 'URL';
$language['mcategory']['admin']['slide_save'] = 'Save';
$language['mcategory']['admin']['slide_cancel'] = 'Cancel';
$language['mcategory']['admin']['mcategory_active'] = 'Active';
$language['mcategory']['admin']['mcategory_inactive'] = 'Blocked';
$language['mcategory']['admin']['mcategory_activate'] = 'Activate selected';
$language['mcategory']['admin']['mcategory_inactivate'] = 'Block selected';

$language['mcategory']['admin']['up_down'] = 'Up / Down ';
$language['mcategory']['admin']['slide_up'] = 'UP';
$language['mcategory']['admin']['slide_up_name'] = 'UP [name]';
$language['mcategory']['admin']['slide_down'] = 'DOWN';
$language['mcategory']['admin']['slide_down_name'] = 'DOWN [name]';

$language['mcategory']['admin']['slide_img'] = 'Slide Img';
$language['mcategory']['admin']['start_date'] = 'Start date';
$language['mcategory']['admin']['end_date'] = 'End date';
$language['mcategory']['admin']['date_between'] = 'Даты';
