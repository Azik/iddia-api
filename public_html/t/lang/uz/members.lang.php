<?php

$language['members']['admin']['title'] = 'Manage Members';
$language['members']['admin']['add'] = 'Add New member';
$language['members']['admin']['username'] = 'Member Name';
$language['members']['admin']['usertype'] = 'Member Type';
$language['members']['admin']['email'] = 'E-mail';
$language['members']['admin']['updated'] = 'Updated';
$language['members']['admin']['edit'] = 'Edit';
$language['members']['admin']['delete'] = 'Delete';
$language['members']['admin']['delete_confirm'] = 'Are you sure?';
$language['members']['admin']['permissions'] = 'Permissions';
$language['members']['admin']['name'] = 'Name';
$language['members']['admin']['password'] = 'Password';
$language['members']['admin']['save'] = 'Save';
$language['members']['admin']['cancel'] = 'Cancel';
$language['members']['admin']['user_exist'] = 'Sorry, this username is already registered.';
$language['members']['admin']['module'] = 'Module';
$language['members']['admin']['right'] = 'Have Right';
$language['members']['admin']['right_full'] = 'Full Right';
$language['members']['admin']['right_del'] = 'Delete';
$language['members']['admin']['right_edit'] = 'Edit';
$language['members']['admin']['right_add'] = 'Add';
$language['members']['admin']['right_view'] = 'View';
$language['members']['admin']['users_active'] = 'Active';
$language['members']['admin']['users_inactive'] = 'Blocked';
$language['members']['admin']['users_activate'] = 'Activate selected';
$language['members']['admin']['users_inactivate'] = 'Block selected';
