<?php

$language['manage_users']['admin']['title'] = 'Manage Users';
$language['manage_users']['admin']['add'] = 'Add New User';
$language['manage_users']['admin']['username'] = 'User Name';
$language['manage_users']['admin']['usertype'] = 'User Type';
$language['manage_users']['admin']['email'] = 'E-mail';
$language['manage_users']['admin']['updated'] = 'Updated';
$language['manage_users']['admin']['edit'] = 'Edit';
$language['manage_users']['admin']['delete'] = 'Delete';
$language['manage_users']['admin']['delete_confirm'] = 'Are you sure?';
$language['manage_users']['admin']['permissions'] = 'Permissions';
$language['manage_users']['admin']['name'] = 'Name';
$language['manage_users']['admin']['password'] = 'Password';
$language['manage_users']['admin']['save'] = 'Save';
$language['manage_users']['admin']['cancel'] = 'Cancel';
$language['manage_users']['admin']['user_exist'] = 'Sorry, this username is already registered.';
$language['manage_users']['admin']['module'] = 'Module';
$language['manage_users']['admin']['right'] = 'Have Right';
$language['manage_users']['admin']['right_full'] = 'Full Right';
$language['manage_users']['admin']['right_del'] = 'Delete';
$language['manage_users']['admin']['right_edit'] = 'Edit';
$language['manage_users']['admin']['right_add'] = 'Add';
$language['manage_users']['admin']['right_view'] = 'View';
$language['manage_users']['admin']['users_active'] = 'Active';
$language['manage_users']['admin']['users_inactive'] = 'Blocked';
$language['manage_users']['admin']['users_activate'] = 'Activate selected';
$language['manage_users']['admin']['users_inactivate'] = 'Block selected';
