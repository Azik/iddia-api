<?php

$language['gallery_manager']['admin']['title'] = 'Gallery Manager';

$language['gallery_manager']['admin']['cat_name'] = 'Gallery Name';
$language['gallery_manager']['admin']['cat_desc'] = 'Description';
$language['gallery_manager']['admin']['cat_add'] = 'Add Gallery';
$language['gallery_manager']['admin']['cat_code'] = 'Code';
$language['gallery_manager']['admin']['cat_position'] = 'Position';
$language['gallery_manager']['admin']['cat_edit'] = 'Edit';
$language['gallery_manager']['admin']['cat_save'] = 'Save';
$language['gallery_manager']['admin']['cat_cancel'] = 'Cancel';
$language['gallery_manager']['admin']['cat_delete'] = 'Delete';
$language['gallery_manager']['admin']['cat_confirm'] = 'Are you sure?';
$language['gallery_manager']['admin']['cat_copy'] = 'Copy code';
$language['gallery_manager']['admin']['cat_active'] = 'Active';
$language['gallery_manager']['admin']['gallery_active'] = 'Active';
$language['gallery_manager']['admin']['gallery_inactive'] = 'Blocked';
$language['gallery_manager']['admin']['gallery_activate'] = 'Activate selected';
$language['gallery_manager']['admin']['gallery_inactivate'] = 'Block selected';

$language['gallery_manager']['admin']['gallery_options'] = 'Gallery Options';
$language['gallery_manager']['admin']['watermark'] = 'Have Watermark';
$language['gallery_manager']['admin']['watermark_name'] = 'Watermark name';
$language['gallery_manager']['admin']['watermark_color'] = 'Watermark color';
$language['gallery_manager']['admin']['watermark_save'] = 'Save';
$language['gallery_manager']['admin']['watermark_cancel'] = 'Cancel';
