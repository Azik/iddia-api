<?php

$language['options_profile']['admin']['title'] = 'Profile Options';
$language['options_profile']['admin']['siteoptions'] = 'Site Options';
$language['options_profile']['admin']['profile'] = 'Your Profile';
$language['options_profile']['admin']['password'] = 'Change Your Password';
$language['options_profile']['admin']['save'] = 'Save';
$language['options_profile']['admin']['cancel'] = 'Cancel';
$language['options_profile']['admin']['charset'] = 'Encoding';
$language['options_profile']['admin']['slider'] = 'Show slider';
$language['options_profile']['admin']['parallax'] = 'Parallax(One page) site';

$language['options_profile']['admin']['meta'] = 'META Tag';
$language['options_profile']['admin']['site_name'] = 'Site name';
$language['options_profile']['admin']['site_title'] = 'Site title';
$language['options_profile']['admin']['meta_keywords'] = 'Keywords';
$language['options_profile']['admin']['meta_description'] = 'Description';
$language['options_profile']['admin']['meta_email'] = 'E-mail';
$language['options_profile']['admin']['meta_author'] = 'Author';
$language['options_profile']['admin']['site_copyright'] = 'Site Copyright';

$language['options_profile']['admin']['lang'] = 'Languages';
$language['options_profile']['admin']['site_lang'] = 'Site Languages';
$language['options_profile']['admin']['admin_lang'] = 'Admin Panel Language';
$language['options_profile']['admin']['admin_lang_main'] = 'Language';
$language['options_profile']['admin']['lang_main'] = 'Main lang';
$language['options_profile']['admin']['lang_show'] = 'Show';
$language['options_profile']['admin']['lang_lang'] = 'Lang';

$language['options_profile']['admin']['old_pass'] = 'Old Password';
$language['options_profile']['admin']['new_pass'] = 'New Password';
$language['options_profile']['admin']['confirm_new_pass'] = 'Confirm New Password';
$language['options_profile']['admin']['passwords_not_match'] = 'Passwords does not match! Please retype password';
$language['options_profile']['admin']['pass_success'] = 'Your password has been changed';
$language['options_profile']['admin']['pass_incorrect'] = 'Your old password is not correct. Please try again';

$language['options_profile']['admin']['email'] = 'E-mail';
$language['options_profile']['admin']['name'] = 'Name';
