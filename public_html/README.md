mCMS 5.2.0 
====================
**Multilingual Content Management System**


## Development
-  PHP/MySQL


**How script works? **

1. Easy for control (Section manager)

2. SEO Optimized 



**Database connections**

- MySQLi - Using by default

- MySQL PDO

- MySQL (For old versions of PHP)



**Installation **

1. Download repository

2. Open install.php in Browser

3. Install mCMS in three steps

4. After Installation finished you see final information

5. If you use in Database MySQLi it install DB automaticaly else you need run .sql file manualy from «install/dump_mcms.sql» file

6. for change template (HTML) files read rules on +info/template_rules.txt file

7. language files in t/lang folder

8. After Installation delete +info/, install/ folder and install.php file


**Templates**

1. For configure templates you need to know HTML/CSS

2. Template files easy for change. 

3. for change template read rules on template_rules.txt file



**Language files**

1. language files in t/lang 

2. For change or add any language create language folder 

3. After adding language folder configure it from mCMS admin area, options menu.



**CRON Job**

Cron job running for send messages 

- Contacts form

- Subscription mails


*Configure your cron job*

```
#!cron

* * * * * lynx -dump http://{SITE_ADDRESS}/cron_queue.php >/dev/null 2>&1

* * * * * lynx -dump http://{SITE_ADDRESS}/cron_subscribe.php >/dev/null 2>&1

{SITE_ADDRESS} - address of site: microphp.com
```

OR 

```
#!cron

* * * * * wget http://{SITE_ADDRESS}/cron_queue.php >/dev/null 2>&1

* * * * * wget http://{SITE_ADDRESS}/cron_subscribe.php >/dev/null 2>&1

{SITE_ADDRESS} - address of site: microphp.com
```



**Useful files and informations**

- http://{SITE_ADDRESS}/news_rss.xml Last news in RSS format





* Author: MicroPHP <info@microphp.com>

* Web: [www.microphp.com](Link http://www.microphp.com)

* Copyright: 2004-2019


[CHANGELOG]: ./CHANGELOG.md
