## Version [5.x.x] - 201x-xx-xx
### Added
	- Photo Select, Upload feature from ckeditor
	- sanitize property to Utils Class
	- escape property to DB (MySQLi, PDO) Class
	- delete property to DB (MySQLi, PDO) Class 
	- update property to DB (MySQLi, PDO) Class
	- insert property to DB (MySQLi, PDO) Class
	- fetch_all property to DB (MySQLi, PDO) Class
	
### Changed
    - Bug on showing language [slug]
	- Small fix on Gallery admin. Delete method to POST

### Removed
	- Removed MySQL Class (Supporting MySQLi and PDO)


## Version [5.2.0] - 2018-07-18
### Added
    - Ajax for select image id from news, block, section and slider managers on Dashboard
	- Emails log (Sent mails)
    - File manager, list files (Ajax)
	- News manager, list news (Ajax)
	- News manager, list last news
	- Section manager - inside links 
	- Events log
    
### Changed
    - Combined gallery image files to one
	- Separately Site and Profile options module 


## Version [5.1.0] - 2017-07-04
### Added
    - Blank template "t/blank"
    - Demo module (Categories module)
    - Auto Install
    
### Changed
    - News admin module - permissions problem
    - Subscribe admin module - Mail templates, Broadcast activate problem
    - phpmailer bug
    
    
## Version [5.0.0] - 2016-10-04
### Added
    - New Dashboard
    - RBAC
    
### Changed
    - Module management
    
### Removed
    - Old code (PHP 4)

    
## Version [4.0.0] - 2014-07-01
### Changed
    - Section manager (Ajax)
    
    
## Version [3.0.0] - 2013-09-14
### Added
    - New Dashboard Design
    - Gallery manager
    
    
## Version [2.0.0] - 2008-05-23
### Added
    - User management
    
    
## Version [1.0.0] - 2004-02-06
    - Initial Release
