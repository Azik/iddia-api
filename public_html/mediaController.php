<?php
/******************* media.php *******************
 *

 *
 ******************** media.php ******************/

/**
 * Define Namespace
 */
namespace mcms5xx;

/**
 * Include view page class
 */
require_once 'm/classes/viewpage.class.php';
require_once 'm/classes/APIJsonResponse.php';

class mediaController extends \mcms5xx\classes\ViewPage
{
    public $langs;
    public $permalinks = '';
    public $perma_type = '';
    public $inside_lang = '';
    public $index_lang = '';

	public $operation = '';
    public $isSuccess = false;
	public $code = 0;
	public $data = array();
    public $errors = array();
    public $result = array();

    public function __construct()
    {
		parent::__construct();
		//$this->utils->require_auth();
    }

    public function onLoad()
    {
			$this->operation = $this->utils->UserGet('op');

			switch ($this->operation) {
				case 'like': {
					$this->like();
					break;
				}
			}

		$res = new \mcms5xx\classes\APIJsonResponse($this->isSuccess, $this->errors, $this->data);
    }



	private function like() {
    		$data = array();
    		$request = (array) json_decode(file_get_contents('php://input'), TRUE);
    		$userID = 0;
     		$login = $request['login'];
        $mediaid = $request['mediaid'];
     		$islike = $request['islike'];


    		$this->isSuccess = true;


	}







}

$mediaController = new mediaController();

include $mediaController->lg_folder . '/index.lang.php';
$mediaController->onLoad();

/******************* media.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** media.php ******************/;
