<?php
/******************* ajax.logout.php *******************
 *
 *
 ******************** ajax.logout.php ******************/

/**
 * Include view page class
 */
require_once 'm/classes/viewpage.class.php';

/**
 * Login members
 */
class ajaxLogout extends \mcms5xx\classes\ViewPage
{
    public $langs;
    public $permalinks = '';
    public $perma_type = '';
    public $inside_lang = '';
    public $index_lang = '';
    public $errors = array();
    public $response = array();
    public $isSuccess = true;
    public $redUrl = '';

	public $session_logged = "member_logged";
	public $session_email = "member_email";
	public $session_userid = "member_id";
	public $session_usertype = "member_usertype";
	
    public $prf_logged = -1;
    public $prf_logged_id = -1;

    public function __construct()
    {
        parent::__construct();
    }

    public function onLoad()
    {
        $this->buildPage();

        $this->prf_logged = $this->utils->filterInt($this->utils->GetSession($this->session_logged));
        $this->prf_logged_id = $this->utils->filterInt($this->utils->GetSession($this->session_userid));

        if (@$_SERVER['REQUEST_METHOD'] == 'POST') {
            $action = $this->utils->Post('action');
            switch ($action) {
                case 'logout':
                    $this->logoutUser();
				break;
              
            }


		} 
    }

    /**
     * Build page
     */
    private function buildPage()
    {
		
	
    }
	
	/**
     * logout 	
     */
    private function logoutUser()
    {
		$red_url = '';
		$code = '';
		$this->isSuccess = true;
		$red_url = $this->curr_folder . str_replace('[lang]', $this->lang, $this->permalinks[$this->perma_type]['index'][$this->curr_lang]);
	
		$this->logout();
		
		$this->response['redURL'] = $red_url;
		//$this->response['code'] = $code;
		$this->response['isSuccess'] = $this->isSuccess;
		$this->response['messages'] = $this->errors;
		die(json_encode($this->response));
	
    }
	
	
	
}

$index = new ajaxLogout();

include $index->lg_folder . '/index.lang.php';
$index->onLoad();

/******************* ajax.logout.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** ajax.logout.php ******************/;