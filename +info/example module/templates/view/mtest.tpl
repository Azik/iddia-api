	
	<!-- BEGIN::PAGE LEVEL PLUGINS -->
	<link href="{ROOT}assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
	<link href="{ROOT}assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
	<!-- END::PAGE LEVEL PLUGINS -->
	<script src="{ROOT}assets/global/plugins/jquery.min.js" type="text/javascript"></script>	
	
	<script type="text/javascript">
		function clickAc(act) {
			//alert(act);
			var set = $(".checkboxes");
			var fields = '';
			$(set).each(function () {
				var checked = $(this).is(":checked");
				if (checked) {
					var v = $(this).val();
					fields += (fields != '') ? ',' : '';
					fields += v;
					//alert("aaa:"+v);
				}
			});
			if (fields != '')
				window.location.href = '?{MODULE_QS}=partners&partnereditid=' + act + ':' + fields;
		}

		function click_cancel() {
			window.location.href = '?{MODULE_QS}=partners';
		}

		function doDelete(id) {
			if (confirm('{DELETE_CONFIRM}')) {
				document.getElementById("partner_action").value = 'delete';
				document.getElementById('select_id').value = id;
				document.getElementById('partner_subm').click();
			}
		}
		function dopartnerAction(act, id, mess) {
			document.getElementById("partner_action").value = act;
			document.getElementById('select_id').value = id;

			if (act == 'delete') {
				if (confirm(mess)) {
					document.getElementById("partner_subm").click();
				}
			}
			else {
				document.getElementById("partner_subm").click();
			}
		}

		function changeStatus(checked, id) {
			jQuery.ajax({
				success: function (result) {
				},
				data: 'id=' + id + '&checked=' + checked + '&ajax=' + true + '&do_submit=1&byajax=1', //need [0]?
				type: 'post',
				url: 'partners_activate.php'
			});
			//alert(3);
		}

		function redirect(url) {
			window.location.href = url;
		}
	</script>

		<!-- BEGIN::CONTENT -->
		<div class="page-content-wrapper" style="min-width:500px !important;">
			<!-- BEGIN::CONTENT BODY -->
			<div class="page-content">
				<!-- BEGIN::PAGE HEADER-->
				<!-- BEGIN::PAGE BAR -->
				<div class="page-bar">
					<ul class="page-breadcrumb">
						<li>
							<a href="index.php">{HOME}</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<span><a href="index.php?{MODULE_QS}=partners">{TITLE}</a></span>
						</li>
					</ul>
				</div>
				<!-- END::PAGE BAR -->
				<!-- BEGIN::PAGE TITLE-->
				<h3 class="page-title"> {TITLE} </h3>
				<!-- END::PAGE TITLE-->
				<!-- END::PAGE HEADER-->
				
				<!-- BEGIN partner -->
				<div class="row">
					<div class="col-md-12">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-dark">
								<i class="fa fa-tasks"></i>
								<span class="caption-subject bold uppercase">{TITLE}</span>
							</div>
						</div>
					<form action="" method="POST" class="form-horizontal" name="form_cat" id="form_cat">
						<input type="hidden" id="partner_action" name="partner_action" value="">
						<input type="hidden" value="0" id="select_id" name="select_id">
						<input type="submit" id="partner_subm" name="partner_subm" style="display:none;">
						<!-- BEGIN list -->
						<div class="portlet-body">
							<!-- BEGIN perm_add -->
							<div class="clearfix">
								<div class="btn-group">
									<a href="{partner.list.ADD_URL}" class="btn green">
										{partner.list.ADD} &nbsp; <i class="fa fa-plus"></i>
									</a>
								</div>
							</div>
							<!-- END perm_add -->
							<br/>
							<table class="table table-striped table-bordered table-hover" width="100%" id="sample_1">
								<thead>
									<tr>
										<th style="width:0px !important;display:none;"></th>
										<th style="width:12px !important;text-align:center;"><input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes"/></th>
										<th>{partner.list.NAME}</th>
										<!-- BEGIN perm_edit -->
										<th style="width : 100px !important;">{partner.list.EDIT}</th>
										<!-- END perm_edit -->
										<!-- BEGIN perm_del -->
										<th style="width : 100px !important;">{partner.list.DELETE}</th>
										<!-- END perm_del -->
										<th style="width : 100px !important;"></th>
									</tr>
								</thead>
								<tbody>
								<!-- BEGIN items -->
								<tr data-position="{partner.list.items.POSITION}" id="{partner.list.items.ID}">
									<td style="display:none;">{partner.list.items.POSITION}</td>
									<td id="chk_{partner.list.items.ID}" style="width:12px !important;text-align:center;"><input type="checkbox" class="checkboxes"
																  value="{partner.list.items.ID}"/></td>
									<td>{partner.list.items.NAME}</td>
									<!-- BEGIN perm_edit -->
									<td class="option text-center">
										<a href="{partner.list.items.EDIT_URL}" title="{partner.list.EDIT}" class="btn yellow">
											{partner.list.EDIT}
											 &nbsp; <i class="fa fa-pencil"></i>
										</a>
									</td>
									<!-- END perm_edit -->
									<!-- BEGIN perm_del -->
									<td class="option text-center">
										<a href="JavaScript:doDelete({partner.list.items.ID})" title="{partner.list.DELETE}" class="btn red ask"> 
											{partner.list.DELETE}
											 &nbsp; <i class="fa fa-trash"></i>
										</a>
									</td>
									<!-- END perm_del -->
									<td class="option text-center" style="width : 100px !important;">
										<!-- BEGIN perm_edit -->
										<div class="success-toggle-button" style=" margin-top:-2px !important;">
											<input type="checkbox" {partner.list.items.STATUS} onchange="changeStatus(this.checked, {partner.list.items.ID})" class="make-switch switch-large" data-label-icon="fa fa-fullscreen" data-on-text="<i class='fa fa-check'></i>" data-off-text="<i class='fa fa-times'></i>">
										</div>
										<!-- END perm_edit -->
									</td>
								</tr>
								<!-- END items -->
								</tbody>
							</table>
                            <!-- BEGIN hidden -->
                            <input type="hidden" name="sort_order" id="sort_order" value="{partner.list.hidden.VALUE}"/>
                            <!-- END hidden -->
							<br/>
							<!-- BEGIN perm_edit -->
							<table>
								<tfoot>
								<tr>
									<td class="ac"><span class="label label-success"
														 onclick="clickAc('activate')">{partner.list.ACTIVATE}</span></td>
									<td style="width:10px;"></td>
									<td class="ac"><span class="label label-danger"
														 onclick="clickAc('inactivate')">{partner.list.INACTIVATE}</span></td>
									<td></td>
								</tr>
								</tfoot>
							</table>
							<!-- END perm_edit -->
						</div>
						<!-- END list -->
						
						<!-- BEGIN partneredit -->
						<div class="portlet-body" id="panel_editbar">
							<div class="form-group form-md-checkboxes">
								<label class="col-md-3 control-label" for="txt_active">&nbsp;</label>
								<div class="col-md-6">
									<div class="md-checkbox-list">
										<div class="md-checkbox">
											<input type="checkbox" name="txt_active" id="txt_active" value="1" class="md-check"  {partner.partneredit.ACTIVE_CHK} />
											<label for="txt_active">
												<span></span>
												<span class="check"></span>
												<span class="box"></span>{partner.partneredit.ACTIVE}
											</label>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group form-md-line-input">
								<label class="col-md-3 control-label cursor-pointer" for="txt_img" onclick="openPhoto($('#txt_img').val(), 'txt_img')">{partner.partneredit.partner_IMG}</label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="txt_img"
										   id="txt_img" value="{partner.partneredit.TXT_IMG}"
										   placeholder="{partner.partneredit.partner_IMG}">
									<div class="form-control-focus"> </div>
								</div>
							</div>
							<div class="tabbable-line boxless tabbable-reversed">
								<ul class="nav nav-tabs">
									<!-- BEGIN tab -->
									<li{partner.partneredit.tab.CLASS}><a href="#content_{partner.partneredit.tab.LANG}" data-toggle="tab">{partner.partneredit.tab.LANG}</a></li>
									<!-- END tab -->
								</ul>
							</div>
							<div class="tab-content">
							<!-- BEGIN tab -->
							<div class="tab-pane {partner.partneredit.tab.FADE_CLASS}" id="content_{partner.partneredit.tab.LANG}">
								<div class="portlet box green">
								<div class="portlet-title">
									<div class="caption">
										<i class="fa fa-language"></i>{partner.partneredit.tab.LANG} 
									</div>
								</div>
								<div class="portlet-body form">
								<div class="form-group form-md-line-input">
									<label class="col-md-3 control-label" for="txt_name_{partner.partneredit.tab.LANG}">{partner.partneredit.tab.NAME}</label>
									<div class="col-md-8">
										<input type="text" class="form-control" name="txt_name_{partner.partneredit.tab.LANG}"
											   id="txt_name_{partner.partneredit.tab.LANG}" value="{partner.partneredit.tab.NAME_VALUE}"
											   placeholder="{partner.partneredit.tab.NAME}">
										<div class="form-control-focus"> </div>
									</div>
								</div>
							
								<div class="form-group form-md-line-input">
									<label class="col-md-3 control-label" for="txt_text_{partner.partneredit.tab.LANG}">{partner.partneredit.tab.TEXT}</label>
									<div class="col-md-8">
										<textarea class="form-control ckeditor" name="txt_text_{partner.partneredit.tab.LANG}"
												  id="txt_text_{partner.partneredit.tab.LANG}" rows="3">{partner.partneredit.tab.TEXT_VALUE}</textarea>
										<div class="form-control-focus"> </div>
									</div>
								</div>
								
								<div class="form-group form-md-line-input">
									<label class="col-md-3 control-label" for="txt_url_{partner.partneredit.tab.LANG}">{partner.partneredit.tab.URL}</label>
									<div class="col-md-8">
										<input type="text" class="form-control" name="txt_url_{partner.partneredit.tab.LANG}"
											   id="txt_url_{partner.partneredit.tab.LANG}" value="{partner.partneredit.tab.URL_VALUE}"
											   placeholder="{partner.partneredit.tab.URL}">
										<div class="form-control-focus"> </div>
									</div>
								</div>
								<div class="form-actions">
									<div class="row">
										<div class="col-md-offset-3 col-md-9">
											<button type="button" class="btn blue" id="btn_save" name="btn_save"
													onClick="dopartnerAction('save', {partner.partneredit.ID},  '');"><i class="fa fa-ok"></i> {partner.partneredit.SAVE}
											</button>
											<button type="reset" class="btn" id="btn_cancel" name="btn_cancel"
													onclick="click_cancel();">{partner.partneredit.CANCEL}</button>
										</div>
									</div>
								</div>
								
								</div>
								</div>
							</div>
							<!-- END tab -->
							</div>
						</div>
						<!-- END partneredit -->
					</form>
					</div>
					</div>
				</div>
				<!-- END partner -->
				
			</div>
			<!-- END::CONTENT BODY -->
		</div>
		<!-- END::CONTENT -->
	</div>
	<!-- END::CONTAINER -->
	
	<!-- BEGIN::CORE PLUGINS -->
	
	<!-- <script src="{ROOT}assets/global/scripts/dt/jquery.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/scripts/dt/jquery-ui.js" type="text/javascript"></script> -->
	<!-- <script src="{ROOT}assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script> -->
	<!-- <script src="{ROOT}assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script> -->
	<!-- END::CORE PLUGINS -->
	
	<!-- BEGIN::THEME GLOBAL SCRIPTS -->
	<script src="{ROOT}assets/global/scripts/app.js" type="text/javascript"></script>
	<!-- END::THEME GLOBAL SCRIPTS -->
	
	<!-- BEGIN::THEME LAYOUT SCRIPTS -->
	<script src="{ROOT}assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
	<script src="{ROOT}assets/layouts/global/scripts/quick-pidebar.min.js" type="text/javascript"></script>
	<!-- END::THEME LAYOUT SCRIPTS -->
	
	<!-- BEGIN partner -->
		<!-- BEGIN list -->
	<script src="{ROOT}assets/global/scripts/dt/jquery.js" type="text/javascript"></script>
		<!-- END list -->
	<!-- END partner -->

	<link href="{ROOT}assets/global/plugins/bootstrap-switch/css/bootstrap-switch.css" rel="stylesheet" type="text/css" />
	<script src="{ROOT}assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>

	<script src="{ROOT}assets/global/scripts/dt/jquery-ui.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/scripts/dt/jquery.dataTables.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/scripts/dt/jquery.dataTables.rowReordering.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/scripts/dt/jquery.dataTables.rowGrouping.js" type="text/javascript"></script>
	
	<script type="text/javascript" charset="utf-8">
		$(document).ready(function () {
			$('.group-checkable').click(function(){
				//alert($('.group-checkable').is(":checked"));
				var ch = $('.group-checkable').is(":checked");
				var set = $(".checkboxes");
				$(set).each(function () {
					$(this).prop('checked', ch);
					var v = $(this).val();
					if (ch) {
						$("#chk_" + v).find('span').addClass('checked');
					} else {
						$("#chk_" + v).find('span').removeClass('checked');
					}
				});
			});
			
			var sortInput = jQuery('#sort_order');
			
			var oTable= $("#sample_1").dataTable({
				"bJQueryUI": true,
				"bDestroy": false,
				"bProcessing": false,
				"bSortable": false,
			});
		
			oTable.rowReordering({
				sURL : 'partners_drag.php', 
				fnAlert: function(message) {
					//alert("order"); 
				}
			});
			
		});
	</script>
	
	<script src="{ROOT}assets/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
        