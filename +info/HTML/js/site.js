$(function(){

    //Popup
	$(".btn_login").click(function() {
		var type = $(this).attr("data-type");
		$("body").stop(true, true).addClass("noscroll");
		$(".login_popup").stop(true, true).addClass("show");
		$(".login_popup .inner").stop(true, true).removeClass("show");
		$(".login_popup .inner[data-id="+type+"]").stop(true, true).addClass("show");
		return false;
	});
	$(".login_popup .close").click(function(){
		$("body").stop(true, true).removeClass("noscroll");
		$(".login_popup").stop(true, true).removeClass("show");
		$(".login_popup .inner").stop(true, true).removeClass("show");
		return false;
    });
    
    //CategoryCarousel
    $("#categorycarousel").owlCarousel({
        dots: true,
        nav: true,
        autoplay: false,
        autoplayTimeout: 5000,
        autoplayHoverPause: true,
        loop: false,
        margin: 0,
        smartSpeed: 450,
        items: 1,
        dotsContainer: '.owldots',
        navContainer: '.owlnavs'
    });
    //Answer
    $("#answer").owlCarousel({
        dots: true,
        nav: true,
        autoplay: false,
        autoplayTimeout: 5000,
        autoplayHoverPause: true,
        loop: false,
        margin: 0,
        smartSpeed: 450,
        items: 1,
        dotsContainer: '.owldots',
        navContainer: '.owlnavs'
    });

    //MaskInput
    $.mask.definitions['9'] = '';
	$.mask.definitions['d'] = '[0-9]';
    $(".maskphone").mask("994ddddddddd");
    $(".maskpin").mask("ddd");

     //ImgSvg
     $('img.svg').each(function() {
        var $img = jQuery(this);
        var imgURL = $img.attr('src');
        var attributes = $img.prop("attributes");

        $.get(imgURL, function(data) {
            // Get the SVG tag, ignore the rest
            var $svg = jQuery(data).find('svg');

            // Remove any invalid XML tags
            $svg = $svg.removeAttr('xmlns:a');

            // Loop through IMG attributes and apply on SVG
            $.each(attributes, function() {
                $svg.attr(this.name, this.value);
            });

            // Replace IMG with SVG
            $img.replaceWith($svg);
        }, 'xml');
    });
});