-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 27, 2020 at 10:06 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 5.6.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mcms_narportal`
--

-- --------------------------------------------------------

--
-- Table structure for table `s_blocks`
--

CREATE TABLE `s_blocks` (
  `blocksid` int(11) NOT NULL,
  `catid` int(11) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `position` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_blocks`
--

INSERT INTO `s_blocks` (`blocksid`, `catid`, `active`, `position`) VALUES
(51, 17, 1, 0),
(60, 19, 1, 0),
(50, 17, 1, 0),
(49, 17, 1, 0),
(48, 17, 1, 0),
(59, 19, 1, 0),
(58, 18, 1, 0),
(57, 18, 1, 0),
(56, 18, 1, 0),
(55, 14, 1, 3),
(47, 17, 1, 0),
(46, 17, 1, 0),
(54, 14, 1, 2),
(53, 14, 1, 1),
(52, 17, 1, 0),
(62, 20, 1, 0),
(61, 19, 1, 0),
(63, 20, 1, 0),
(64, 16, 0, 0),
(65, 16, 0, 0),
(66, 16, 0, 0),
(67, 21, 1, 0),
(68, 21, 1, 0),
(69, 21, 0, 0),
(70, 21, 0, 0),
(71, 25, 1, 0),
(72, 25, 1, 0),
(73, 25, 1, 0),
(74, 26, 1, 0),
(75, 26, 1, 0),
(76, 26, 1, 0),
(77, 26, 1, 0),
(78, 26, 1, 0),
(79, 26, 1, 0),
(80, 16, 1, 0),
(81, 16, 1, 0),
(82, 29, 1, 1),
(83, 29, 1, 2),
(84, 29, 1, 5),
(85, 29, 1, 3),
(86, 29, 1, 4),
(87, 30, 1, 0),
(88, 30, 1, 0),
(89, 30, 1, 0),
(90, 30, 1, 0),
(91, 30, 1, 0),
(92, 30, 1, 0),
(93, 30, 1, 0),
(94, 30, 1, 0),
(95, 31, 1, 0),
(96, 31, 1, 0),
(97, 31, 1, 0),
(98, 33, 1, 0),
(99, 33, 1, 0),
(100, 33, 1, 0),
(101, 34, 1, 0),
(102, 34, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `s_blockscategories`
--

CREATE TABLE `s_blockscategories` (
  `catid` int(11) NOT NULL,
  `position` int(5) NOT NULL DEFAULT '1',
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_blockscategories`
--

INSERT INTO `s_blockscategories` (`catid`, `position`, `active`) VALUES
(16, 3, 1),
(15, 2, 1),
(14, 1, 1),
(17, 4, 1),
(18, 5, 0),
(19, 6, 0),
(20, 7, 0),
(21, 8, 1),
(22, 9, 0),
(23, 10, 0),
(24, 11, 1),
(25, 12, 1),
(26, 13, 1),
(27, 14, 1),
(28, 15, 0),
(29, 16, 0),
(30, 17, 0),
(31, 18, 0),
(32, 19, 0),
(33, 20, 0),
(34, 21, 0),
(35, 22, 1);

-- --------------------------------------------------------

--
-- Table structure for table `s_blockscategorylocalizations`
--

CREATE TABLE `s_blockscategorylocalizations` (
  `catid` int(11) NOT NULL DEFAULT '0',
  `lang` varchar(5) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `more_txt` text,
  `imgid` int(11) NOT NULL DEFAULT '0',
  `info` text NOT NULL,
  `embed_code` text,
  `url` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_blockscategorylocalizations`
--

INSERT INTO `s_blockscategorylocalizations` (`catid`, `lang`, `name`, `more_txt`, `imgid`, `info`, `embed_code`, `url`) VALUES
(16, 'en', 'Testimonials', '', 0, '<p>Lorem ipsum dolor sit amet</p>', '', ''),
(17, 'es', 'Fogonadura', '', 0, '', '', ''),
(15, 'en', 'Top Holiday Destinations', 'All', 0, '<p>Lorem ipsum dolor sit amet</p>', '', ''),
(21, 'es', 'Síguenos', '', 0, '', '', ''),
(14, 'en', 'About Us', '', 0, '<p>Aenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non. Pellentesque rutrum fringilla elementum. Curabitur tincidunt porta lorem vitae accumsan.&nbsp;<br />\r\nAenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non. Pellentesque rutrum fringilla elementum. Curabitur tincidunt porta lorem vitae accumsan.</p>', '', ''),
(22, 'en', 'Recent Posts', '', 0, '', '', ''),
(22, 'es', 'Mensajes recientes', '', 0, '', '', ''),
(23, 'en', 'News letter', '', 0, '<p>If you want to receive our latest news send directly to your email, please leave your email address bellow. Subscription is free and you can cancel anytime.</p>', '', ''),
(17, 'en', 'Partners', '', 0, '', '', ''),
(14, 'es', 'Sobre nosotros', '', 0, '<p>Aenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non. Pellentesque rutrum fringilla elementum. Curabitur tincidunt porta lorem vitae accumsan.&nbsp;<br />\r\nAenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non. Pellentesque rutrum fringilla elementum. Curabitur tincidunt porta lorem vitae accumsan.</p>', '', ''),
(15, 'es', 'Los mejores destinos de vacaciones', 'Todas', 0, '<p>Lorem ipsum dolor sit amet</p>', '', ''),
(16, 'es', 'Testimonios', '', 0, '<p>Lorem ipsum dolor sit amet</p>', '', ''),
(18, 'en', 'Lake Wanaka', '', 0, '', '', 'LakeWanaka'),
(19, 'en', 'Ohama Beach', '', 0, '', '', 'OhamaBeach'),
(20, 'en', 'Taupo Central', '', 0, '', '', 'TaupoCentral'),
(20, 'es', 'Taupo Central', '', 0, '', '', 'TaupoCentral'),
(18, 'es', 'Lake Wanaka', '', 0, '', '', 'LakeWanaka'),
(19, 'es', 'Ohama Beach', '', 0, '', '', 'OhamaBeach'),
(21, 'en', 'Follow us', '', 0, '', '', ''),
(23, 'es', 'Hoja informativa', '', 0, '<p>Si desea recibir las &uacute;ltimas noticias env&iacute;a directamente a su correo electr&oacute;nico, por favor deje su direcci&oacute;n de correo electr&oacute;nico abajo. La suscripci&oacute;n es gratuita y se puede cancelar en cualquier momento.</p>', '', ''),
(24, 'en', 'Information Service:', '', 0, '<p>800-2345-6789</p>', '', ''),
(24, 'es', 'Servicio de información:', '', 0, '<p>800-2345-6789</p>', '', ''),
(25, 'en', 'Offering a wide range of laboratory services', 'Read More', 0, '<p>Our fully computerised laboratory group offers a high quality, cost-effective service and clinical expertise to local general practitioners and other trusts, hospitals and healthcare providers. Our innovative laboratory services enhance patient health.</p>', '', 'services'),
(25, 'es', 'Ofreciendo una amplia gama de servicios de laboratorio', 'Lee mas', 0, '<p>Nuestro grupo de laboratorio totalmente informatizado ofrece una alta calidad, un servicio rentable y experiencia cl&iacute;nica para los profesionales locales generales y otros fideicomisos, hospitales y proveedores de atenci&oacute;n m&eacute;dica. Nuestros servicios innovadores de laboratorio mejoran la salud del paciente.</p>', '', 'servicios'),
(26, 'en', 'Most common tests', 'The List Links', 292, '<p>Our laboratory provides comprehensive, multidisciplinary pathology services. As part of a regular checkup, to get a diagnosis, or perhaps to provide a benchmark, your doctor may request one or more laboratory tests. Here is a list of the most common tests ordered and their purpose. Most tests are done using a blood sample. Some standard tests are usually performed on admission to a hospital or as part of an annual physical.</p>', '', ''),
(26, 'es', 'Pruebas más comunes', 'La Lista de Enlaces', 292, '<p>Nuestro laboratorio ofrece servicios integrales de patolog&iacute;a multidisciplinares. Como parte de un examen de rutina, para obtener un diagn&oacute;stico, o tal vez para proporcionar un punto de referencia, el m&eacute;dico puede solicitar una o m&aacute;s pruebas de laboratorio. Aqu&iacute; est&aacute; una lista de las pruebas m&aacute;s comunes ordenados y su prop&oacute;sito. La mayor&iacute;a de las pruebas se realizaron utilizando una muestra de sangre. Algunas pruebas est&aacute;ndar se realizan generalmente al ingreso en un hospital o como parte de un examen f&iacute;sico anual.</p>', '', ''),
(27, 'en', 'Offering a wide range of laboratory services', 'Read More', 0, '<p>Our fully computerised laboratory group offers a high quality, cost-effective service and clinical expertise to local general practitioners and other trusts, hospitals and healthcare providers. Our innovative laboratory services enhance patient health.</p>', '', ''),
(27, 'es', 'Ofreciendo una amplia gama de servicios de laboratorio', 'Lee mas', 0, '<p>Nuestro grupo de laboratorio totalmente informatizado ofrece una alta calidad, un servicio rentable y experiencia cl&iacute;nica para los profesionales locales generales y otros fideicomisos, hospitales y proveedores de atenci&oacute;n m&eacute;dica. Nuestros servicios innovadores de laboratorio mejoran la salud del paciente.</p>', '', ''),
(28, 'en', 'Home', 'LEARN MORE', 0, '<h1>ZENTRO RESTAURANT</h1>\r\n\r\n<h2>CLEAN &amp; SIMPLE DESIGN</h2>', '', '#gallery'),
(28, 'es', 'Inicio', 'APRENDE MÁS', 0, '<h1>ZENTRO RESTAURANTE</h1>\r\n\r\n<h2>DISE&Ntilde;O LIMPIO Y SENCILLO</h2>', '', '#gallery'),
(29, 'en', 'Food Gallery', '', 0, '', '', ''),
(29, 'es', 'Galería de comida', '', 0, '', '', ''),
(30, 'en', 'Special Menu', '', 0, '', '', ''),
(30, 'es', 'Menú especial', '', 0, '', '', ''),
(31, 'en', 'Chefs', '', 0, '<p>Meet Zentro chefs</p>', '', ''),
(31, 'es', 'Cocinero', '', 0, '<p>Conoce a los cocineros Zentro</p>', '', ''),
(32, 'en', 'Contact', '', 0, '<p>Contact Us</p>', '', ''),
(32, 'es', 'Contacto', '', 0, '<p>Cont&aacute;ctenos</p>', '', ''),
(33, 'en', 'Open Hours', '', 0, '', '', ''),
(33, 'es', 'Horarios de apertura', '', 0, '', '', ''),
(34, 'en', 'Contact Info.', '', 0, '', '', ''),
(34, 'es', 'Datos de contacto.', '', 0, '', '', ''),
(35, 'en', 'Bootstrap theme', '', 0, '', '', ''),
(35, 'es', 'El tema de Bootstrap', '', 0, '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `s_blockslocalizations`
--

CREATE TABLE `s_blockslocalizations` (
  `blocksid` int(11) NOT NULL DEFAULT '0',
  `lang` varchar(5) NOT NULL DEFAULT '',
  `img` int(11) NOT NULL DEFAULT '0',
  `img02` int(11) NOT NULL DEFAULT '0',
  `img_url` text NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `link_name` text,
  `text` longtext NOT NULL,
  `url` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_blockslocalizations`
--

INSERT INTO `s_blockslocalizations` (`blocksid`, `lang`, `img`, `img02`, `img_url`, `name`, `link_name`, `text`, `url`) VALUES
(47, 'en', 261, 0, '', 'Creative market', '', '', ''),
(48, 'en', 262, 0, '', 'designmodo', '', '', ''),
(49, 'en', 263, 0, '', 'evanto', '', '', ''),
(50, 'en', 264, 0, '', 'WordPress', '', '', ''),
(51, 'en', 265, 0, '', 'themeforest', '', '', ''),
(52, 'en', 266, 0, '', 'microlancer', '', '', ''),
(46, 'es', 260, 0, '', 'ae tust', '', '', ''),
(46, 'en', 260, 0, '', 'ae tust', '', '', ''),
(53, 'en', 275, 0, '', 'Text Heading A', '', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima quo. Aenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non.</p>', ''),
(54, 'en', 276, 0, '', 'Text Heading B', '', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima quo. Aenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non.</p>', ''),
(55, 'en', 277, 0, '', 'Text Heading C', '', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima quo. Aenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non.</p>', ''),
(56, 'en', 280, 281, '', '1', '', '<p>Aenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non.</p>', ''),
(57, 'en', 282, 283, '', '2', '', '<p>Aenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non.</p>', ''),
(58, 'en', 285, 284, '', '4', '', '<p>Aenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non.</p>', ''),
(59, 'en', 286, 287, '', '2', '', '<p>Aenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non.</p>', ''),
(60, 'en', 282, 283, '', '3', '', '<p>Aenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non.</p>', ''),
(61, 'en', 280, 281, '', '1', '', '<p>Aenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non.</p>', ''),
(62, 'en', 285, 284, '', '4', '', '<p>Aenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non.</p>', ''),
(63, 'en', 282, 283, '', '3', '', '<p>Aenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non.</p>', ''),
(64, 'en', 257, 0, '', 'WILL SMITH', '', '<p>Aenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non. Pellentesque rutrum fringilla elementum. Curabitur tincidunt porta lorem vitae accumsan. Aenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non. Pellentesque rutrum fringilla elementum. Curabitur tincidunt porta lorem vitae accumsan.</p>', ''),
(65, 'en', 258, 0, '', 'WILL SMITH', '', '<p>Aenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non. Pellentesque rutrum fringilla elementum. Curabitur tincidunt porta lorem vitae accumsan. Aenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non. Pellentesque rutrum fringilla elementum. Curabitur tincidunt porta lorem vitae accumsan.</p>', ''),
(66, 'en', 259, 0, '', 'WILL SMITH', '', '<p>Aenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non. Pellentesque rutrum fringilla elementum. Curabitur tincidunt porta lorem vitae accumsan. Aenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non. Pellentesque rutrum fringilla elementum. Curabitur tincidunt porta lorem vitae accumsan.</p>', ''),
(67, 'en', 0, 0, '', 'Twitter', 'fa fa-twitter', '', 'https://twitter.com/microphp'),
(68, 'en', 0, 0, '', 'Facebook', 'fa fa-facebook', '', 'https://www.facebook.com/webdevelopmentgroup/'),
(69, 'en', 0, 0, '', 'Linkedin', 'fa fa-linkedin', '', '#'),
(70, 'en', 0, 0, '', 'Pinterest', 'fa fa-pinterest', '', '#'),
(51, 'es', 265, 0, '', 'themeforest', '', '', ''),
(50, 'es', 264, 0, '', 'WordPress', '', '', ''),
(52, 'es', 266, 0, '', 'microlancer', '', '', ''),
(49, 'es', 263, 0, '', 'evanto', '', '', ''),
(48, 'es', 262, 0, '', 'designmodo', '', '', ''),
(47, 'es', 261, 0, '', 'Creative market', '', '', ''),
(53, 'es', 275, 0, '', 'Text Heading A', '', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima quo. Aenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non.</p>', ''),
(54, 'es', 276, 0, '', 'Text Heading B', '', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima quo. Aenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non.</p>', ''),
(55, 'es', 277, 0, '', 'Text Heading C', '', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima quo. Aenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non.</p>', ''),
(64, 'es', 257, 0, '', 'WILL SMITH', '', '<p>Aenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non. Pellentesque rutrum fringilla elementum. Curabitur tincidunt porta lorem vitae accumsan. Aenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non. Pellentesque rutrum fringilla elementum. Curabitur tincidunt porta lorem vitae accumsan.</p>', ''),
(65, 'es', 258, 0, '', 'WILL SMITH', '', '<p>Aenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non. Pellentesque rutrum fringilla elementum. Curabitur tincidunt porta lorem vitae accumsan. Aenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non. Pellentesque rutrum fringilla elementum. Curabitur tincidunt porta lorem vitae accumsan.</p>', ''),
(66, 'es', 259, 0, '', 'WILL SMITH', '', '<p>Aenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non. Pellentesque rutrum fringilla elementum. Curabitur tincidunt porta lorem vitae accumsan. Aenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non. Pellentesque rutrum fringilla elementum. Curabitur tincidunt porta lorem vitae accumsan.</p>', ''),
(58, 'es', 285, 284, '', '4', '', '<p>Aenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non.</p>', ''),
(57, 'es', 282, 283, '', '2', '', '<p>Aenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non.</p>', ''),
(56, 'es', 280, 281, '', '1', '', '<p>Aenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non.</p>', ''),
(61, 'es', 280, 281, '', '1', '', '<p>Aenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non.</p>', ''),
(60, 'es', 282, 283, '', '3', '', '<p>Aenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non.</p>', ''),
(59, 'es', 286, 287, '', '2', '', '<p>Aenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non.</p>', ''),
(63, 'es', 282, 283, '', '3', '', '<p>Aenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non.</p>', ''),
(62, 'es', 285, 284, '', '4', '', '<p>Aenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non.</p>', ''),
(67, 'es', 0, 0, '', 'Twitter', 'fa fa-twitter', '', 'https://twitter.com/microphp'),
(68, 'es', 0, 0, '', 'Facebook', 'fa fa-facebook', '', 'https://www.facebook.com/webdevelopmentgroup/'),
(69, 'es', 0, 0, '', 'Linkedin', 'fa fa-linkedin', '', '#'),
(70, 'es', 0, 0, '', 'Pinterest', 'fa fa-pinterest', '', '#'),
(71, 'es', 291, 0, '', 'La mayor red de centros de laboratorio', 'Lee mas', '<p>Tenemos contratos con laboratorios de todo el mundo para proporcionar servicios de laboratorio a los mejores precios. Tambi&eacute;n tenemos contratos con los laboratorios de independiente independientes para ...</p>', ''),
(71, 'en', 291, 0, '', 'The largest network of laboratory centers', 'Read More', '<p>We have contracts with laboratories all over the world to provide lab services at the best prices. We also contract with the free-standing independent laboratories for...</p>', '#'),
(72, 'en', 289, 0, '', 'The modern laboratory facilities', 'Read More', '<p>We&rsquo;ve done our research to ensure that the medical laboratory equipment we use has been industry approved and has surpassed statistical benchmarks for risk assessment. Our team members are highly trained...</p>', '#'),
(72, 'es', 289, 0, '', 'Las modernas instalaciones de laboratorio', 'Lee mas', '<p>Hemos hecho nuestra investigaci&oacute;n para asegurarse de que el equipo de laboratorio m&eacute;dica que usamos ha sido aprobada la industria y ha superado los puntos de referencia estad&iacute;sticos para la evaluaci&oacute;n de riesgos. Los miembros de nuestro equipo est&aacute;n altamente capacitados ...</p>', ''),
(73, 'en', 290, 0, '', 'Helpful test tips', 'Read More', '<p>If you are nervous or have a tendency to feel woozy or faint, tell the phlebotomist before you begin. Your blood can be drawn while you are lying down, which will help you avoid fainting and injuring yourself. If, at any time, you feel faint or lightheaded...</p>', '#'),
(73, 'es', 290, 0, '', 'Puntas de prueba votos', 'Lee mas', '<p>Si est&aacute; nervioso o tiene una tendencia a sentirse mareado o d&eacute;bil, decirle al flebotomista antes de empezar. Su sangre se puede dibujar mientras se est&aacute; acostado, lo que ayudar&aacute; a evitar desmayos y lesiones personales. Si, en cualquier momento, usted siente mareos o v&eacute;rtigo ...</p>', '#'),
(74, 'en', 0, 0, '', 'Flu tests', '', '', ''),
(74, 'es', 0, 0, '', 'Pruebas para la gripe', '', '', ''),
(75, 'en', 0, 0, '', 'Glucose', '', '', ''),
(75, 'es', 0, 0, '', 'Glucosa', '', '', ''),
(76, 'en', 0, 0, '', 'Semen Analysis', '', '', ''),
(76, 'es', 0, 0, '', 'El análisis del semen', '', '', ''),
(77, 'en', 0, 0, '', 'Uric Acid', '', '', ''),
(77, 'es', 0, 0, '', 'Ácido úrico', '', '', ''),
(78, 'en', 0, 0, '', 'Complete Blood Count', '', '', ''),
(78, 'es', 0, 0, '', 'Hemograma completo', '', '', ''),
(79, 'en', 0, 0, '', 'Hemoglobin or Glycohemoglobin', '', '', ''),
(79, 'es', 0, 0, '', 'Hemoglobina o Glicohemoglobina', '', '', ''),
(80, 'en', 293, 0, '', 'Timmy', '', '<p>Your company understands today&rsquo;s requirements, but that&rsquo;s not what makes you stand apart. You also understand today&rsquo;s business world and how to keep customers happy.</p>', '#'),
(80, 'es', 293, 0, '', 'Timmy', '', '<p>Su empresa es consciente de las necesidades actuales, pero eso no es lo que te hace estar al margen. Tambi&eacute;n entiende el mundo empresarial actual y la forma de mantener a los clientes contentos.</p>', ''),
(81, 'en', 294, 0, '', 'Timmy', '', '<p>Your company understands today&rsquo;s requirements, but that&rsquo;s not what makes you stand apart. You also understand today&rsquo;s business world and how to keep customers happy.</p>', ''),
(81, 'es', 294, 0, '', 'Timmy', '', '<p>Su empresa es consciente de las necesidades actuales, pero eso no es lo que te hace estar al margen. Tambi&eacute;n entiende el mundo empresarial actual y la forma de mantener a los clientes contentos.</p>', ''),
(82, 'en', 307, 0, '', 'Lemon-Rosemary Prawn', '', '<p>Seafood / Shrimp / Lemon</p>', ''),
(83, 'en', 306, 0, '', 'Lemon-Rosemary Vegetables', '', '<p>Tomato / Rosemary / Lemon</p>', ''),
(84, 'en', 308, 0, '', 'Lemon-Rosemary Bakery', '', '<p>Bread / Rosemary / Orange</p>', ''),
(85, 'en', 309, 0, '', 'Lemon-Rosemary Salad', '', '<p>Chicken / Rosemary / Green</p>', ''),
(86, 'en', 310, 0, '', 'Lemon-Rosemary Pizza', '', '<p>Pasta / Rosemary / Green</p>', ''),
(82, 'es', 307, 0, '', 'Limón-Romero gamba', '', '<p>Mariscos / camar&oacute;n / Lim&oacute;n</p>', ''),
(83, 'es', 306, 0, '', 'Verduras de limón y romero', '', '<p>Tomate / Romero / Lim&oacute;n</p>', ''),
(85, 'es', 309, 0, '', 'Ensalada de limón y romero', '', '<p>Pollo / Romero / verde</p>', ''),
(86, 'es', 310, 0, '', 'Limón Romero-pizza', '', '<p>Pasta / Romero / Verde</p>', ''),
(84, 'es', 308, 0, '', 'Limón-Romero Panadería', '', '<p>Pan / Romero / Naranja</p>', ''),
(87, 'es', 0, 0, '', 'Vegetal Limón-Romero ........................', '$20.50', '<p>Pollo / Romero / Lim&oacute;n</p>', ''),
(87, 'en', 0, 0, '', 'Lemon-Rosemary Vegetable ................', '$20.50', '<p>Chicken / Rosemary / Lemon</p>', ''),
(88, 'en', 0, 0, '', 'Lemon-Rosemary Meat ...........................', '$30.50', '<p>Meat / Rosemary / Lemon</p>', ''),
(89, 'en', 0, 0, '', 'Lemon-Rosemary Pork ........................', '$40.75', '<p>Pork / Tooplate / Lemon</p>', ''),
(90, 'es', 0, 0, '', 'Ensalada de naranja-Romero ...................', '$55.00', '<p>Ensalada / Romero / Naranja</p>', ''),
(90, 'en', 0, 0, '', 'Orange-Rosemary Salad ..........................', '$55.00', '<p>Salad / Rosemary / Orange</p>', ''),
(91, 'es', 0, 0, '', 'Limón-Romero Calamar .......................', '$65.00', '<p>Calamar / Romero / Lim&oacute;n</p>', ''),
(91, 'en', 0, 0, '', 'Lemon-Rosemary Squid ......................', '$65.00', '<p>Squid / Rosemary / Lemon</p>', ''),
(92, 'es', 0, 0, '', 'Camarones Naranja-Romero ....................', '$70.50', '<p>Camarones / Romero / Naranja</p>', ''),
(92, 'en', 0, 0, '', 'Orange-Rosemary Shrimp ........................', '$70.50', '<p>Shrimp / Rosemary / Orange</p>', ''),
(93, 'en', 0, 0, '', 'Lemon-Rosemary Prawn ...................', '$110.75', '<p>Chicken / Rosemary / Lemon</p>', ''),
(94, 'en', 0, 0, '', 'Lemon-Rosemary Seafood .....................', '$220.50', '<p>Seafood / Rosemary / Lemon</p>', ''),
(94, 'es', 0, 0, '', 'Limón-Romero Mariscos .........................', '$220.50', '<p>Mariscos / Romero / Lim&oacute;n</p>', ''),
(93, 'es', 0, 0, '', 'Limón-Romero gamba .........................', '$110.75', '<p>Pollo / Romero / Lim&oacute;n</p>', ''),
(89, 'es', 0, 0, '', 'Cerdo Limón-Romero ...........................', '$40.75', '<p>Cerdo / Tooplate / Lim&oacute;n</p>', ''),
(88, 'es', 0, 0, '', 'Limón-Romero Carne ................................', '$30.50', '<p>Carne / Romero / Lim&oacute;n</p>', ''),
(95, 'en', 312, 0, '', 'Thanya', '', '<p>Main Chef</p>', ''),
(95, 'es', 312, 0, '', 'Thanya', '', '<p>Chef principal</p>', ''),
(96, 'en', 311, 0, '', 'Lynda', '', '<p>Pizza Specialist</p>', ''),
(96, 'es', 311, 0, '', 'Lynda', '', '<p>Especialista de pizza</p>', ''),
(97, 'en', 313, 0, '', 'Jenny Ko', '', '<p>New Baker</p>', ''),
(97, 'es', 313, 0, '', 'Jenny Ko', '', '<p>Nueva Panadero</p>', ''),
(98, 'en', 0, 0, '', 'Sunday', '', '<p>10:30 AM - 10:00 PM</p>', ''),
(98, 'es', 0, 0, '', 'Domingo', '', '<p>10:30 AM - 10:00 PM</p>', ''),
(99, 'en', 0, 0, '', 'Mon-Fri', '', '<p>9:00 AM - 8:00 PM</p>', ''),
(99, 'es', 0, 0, '', 'De lunes a viernes', '', '<p>9:00 AM - 8:00 PM</p>', ''),
(100, 'en', 0, 0, '', 'Saturday', '', '<p>11:30 AM - 10:00 PM</p>', ''),
(100, 'es', 0, 0, '', 'Sábado', '', '<p>11:30 AM - 10:00 PM</p>', ''),
(101, 'en', 0, 0, '', 'Phone', 'fa fa-phone', '<p>090-080-0760</p>', ''),
(101, 'es', 0, 0, '', 'Teléfono', 'fa fa-phone', '<p>090-080-0760</p>', ''),
(102, 'en', 0, 0, '', 'Our Location', 'fa fa-map-marker', '<p>120 Duis aute irure, California, USA</p>', ''),
(102, 'es', 0, 0, '', 'Nuestra ubicación', 'fa fa-map-marker', '<p>120 Duis aute irure, California, USA</p>', '');

-- --------------------------------------------------------

--
-- Table structure for table `s_dashboard`
--

CREATE TABLE `s_dashboard` (
  `x_blocks` bigint(21) DEFAULT NULL,
  `x_pages` bigint(21) DEFAULT NULL,
  `x_formentries` bigint(21) DEFAULT NULL,
  `x_files` bigint(21) DEFAULT NULL,
  `x_modules` bigint(21) DEFAULT NULL,
  `x_news` bigint(21) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `s_files`
--

CREATE TABLE `s_files` (
  `fileid` int(11) NOT NULL,
  `originalname` varchar(255) NOT NULL DEFAULT '',
  `filename` varchar(255) NOT NULL DEFAULT '',
  `extension` varchar(20) NOT NULL DEFAULT '',
  `category` varchar(20) NOT NULL DEFAULT '',
  `title` varchar(255) NOT NULL DEFAULT '',
  `add_time` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_files`
--

INSERT INTO `s_files` (`fileid`, `originalname`, `filename`, `extension`, `category`, `title`, `add_time`) VALUES
(281, '1.jpg', '1.jpg', 'jpg', 'image', '', 1475498320),
(282, '3-thumb.jpg', '3-thumb.jpg', 'jpg', 'image', '', 1475499062),
(283, '3.jpg', '3.jpg', 'jpg', 'image', '', 1475499062),
(275, 'key.png', 'key.png', 'png', 'image', '', 1475496127),
(276, 'money.png', 'money.png', 'png', 'image', '', 1475496128),
(280, '1-thumb.jpg', '1-thumb.jpg', 'jpg', 'image', '', 1475498320),
(277, 'days.png', 'days.png', 'png', 'image', '', 1475496131),
(284, '4.jpg', '4.jpg', 'jpg', 'image', '', 1475499062),
(285, '4-thumb.jpg', '4-thumb.jpg', 'jpg', 'image', '', 1475499062),
(286, '2-thumb.jpg', '2-thumb.jpg', 'jpg', 'image', '', 1475499687),
(287, '2.jpg', '2.jpg', 'jpg', 'image', '', 1475499690),
(288, 'about.jpg', 'about.jpg', 'jpg', 'image', '', 1475526308),
(257, 'people1.jpg', 'people1.jpg', 'jpg', 'image', '', 1475482444),
(258, 'people2.jpg', 'people2.jpg', 'jpg', 'image', '', 1475482448),
(259, 'people3.jpg', 'people3.jpg', 'jpg', 'image', '', 1475482450),
(260, '15.jpg', '15.jpg', 'jpg', 'image', '', 1475482469),
(261, '16.jpg', '16.jpg', 'jpg', 'image', '', 1475482471),
(262, '17.jpg', '17.jpg', 'jpg', 'image', '', 1475482473),
(263, '18.jpg', '18.jpg', 'jpg', 'image', '', 1475482477),
(264, '19.jpg', '19.jpg', 'jpg', 'image', '', 1475482479),
(265, '20.jpg', '20.jpg', 'jpg', 'image', '', 1475482481),
(266, '21.jpg', '21.jpg', 'jpg', 'image', '', 1475482482),
(256, 'banner2.jpg', 'banner2.jpg', 'jpg', 'image', '', 1475481623),
(255, 'banner1.jpg', 'banner1.jpg', 'jpg', 'image', '', 1475481623),
(289, 'icon3.png', 'icon3.png', 'png', 'image', '', 1475758048),
(290, 'icon2.png', 'icon2.png', 'png', 'image', '', 1475758048),
(291, 'icon1.png', 'icon1.png', 'png', 'image', '', 1475758048),
(292, '6.jpg', '6.jpg', 'jpg', 'image', '', 1475758506),
(293, 'tester1.jpg', 'tester1.jpg', 'jpg', 'image', '', 1475759109),
(294, 'tester2.jpg', 'tester2.jpg', 'jpg', 'image', '', 1475759109),
(295, '7.jpg', '7.jpg', 'jpg', 'image', '', 1475760849),
(296, '9.jpg', '9.jpg', 'jpg', 'image', '', 1475760849),
(297, '8.jpg', '8.jpg', 'jpg', 'image', '', 1475760850),
(298, '11.jpg', '11.jpg', 'jpg', 'image', '', 1475761619),
(299, '10.jpg', '10.jpg', 'jpg', 'image', '', 1475761619),
(300, '12.jpg', '12.jpg', 'jpg', 'image', '', 1475761619),
(301, '13.jpg', '13.jpg', 'jpg', 'image', '', 1475761619),
(302, '14.jpg', '14.jpg', 'jpg', 'image', '', 1475761619),
(303, '15.jpg', '15.jpg', 'jpg', 'image', '', 1475761619),
(304, '16.jpg', '16.jpg', 'jpg', 'image', '', 1475761619),
(305, '17.jpg', '17.jpg', 'jpg', 'image', '', 1475761619),
(306, 'gallery-img2.jpg', 'gallery-img2.jpg', 'jpg', 'image', '', 1475820765),
(307, 'gallery-img1.jpg', 'gallery-img1.jpg', 'jpg', 'image', '', 1475820765),
(308, 'gallery-img3.jpg', 'gallery-img3.jpg', 'jpg', 'image', '', 1475820765),
(309, 'gallery-img4.jpg', 'gallery-img4.jpg', 'jpg', 'image', '', 1475820766),
(310, 'gallery-img5.jpg', 'gallery-img5.jpg', 'jpg', 'image', '', 1475820766),
(311, 'team2.jpg', 'team2.jpg', 'jpg', 'image', '', 1475823576),
(312, 'team1.jpg', 'team1.jpg', 'jpg', 'image', '', 1475823576),
(313, 'team3.jpg', 'team3.jpg', 'jpg', 'image', '', 1475823577),
(315, 'mcms_profile.jpg', 'mcms_profile.jpg', 'jpg', 'image', '', 1497967530),
(317, 'zagruzheno.jpg', 'zagruzheno.jpg', 'jpg', 'image', '', 1516895823);

-- --------------------------------------------------------

--
-- Table structure for table `s_files_image_sizes`
--

CREATE TABLE `s_files_image_sizes` (
  `imgid` int(11) NOT NULL DEFAULT '0',
  `size` varchar(255) NOT NULL DEFAULT '',
  `x1` int(5) NOT NULL DEFAULT '0',
  `y1` int(5) NOT NULL DEFAULT '0',
  `x2` int(5) NOT NULL DEFAULT '0',
  `y2` int(5) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_files_image_sizes`
--

INSERT INTO `s_files_image_sizes` (`imgid`, `size`, `x1`, `y1`, `x2`, `y2`) VALUES
(140, '400x400', 174, 90, 574, 490),
(140, '430x330', 162, 105, 592, 435),
(140, '227x330', 241, 128, 468, 458),
(140, '325x160', 215, 187, 540, 347),
(140, '325x340', 189, 73, 514, 413),
(140, '325x155', 210, 155, 535, 310),
(140, '328x247', 259, 162, 587, 409),
(251, '328x247', 21, 121, 349, 368),
(251, '325x155', 357, 176, 682, 331),
(251, '325x340', 33, 53, 358, 393),
(251, '325x160', 39, 179, 340, 327),
(251, '227x330', 102, 44, 329, 374),
(251, '430x330', 151, 23, 581, 353),
(251, '400x400', 7, 0, 400, 393),
(316, '325x340', 0, 0, 325, 340),
(316, '325x160', 260, 184, 585, 344),
(316, '227x330', 66, 83, 293, 413),
(316, '430x330', 0, 90, 430, 420),
(316, '400x400', 49, 0, 513, 464),
(316, '325x155', 0, 0, 325, 155),
(316, '328x247', 0, 0, 328, 247);

-- --------------------------------------------------------

--
-- Table structure for table `s_gallery`
--

CREATE TABLE `s_gallery` (
  `gid` int(11) NOT NULL,
  `gdate` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `position` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_gallery`
--

INSERT INTO `s_gallery` (`gid`, `gdate`, `active`, `position`) VALUES
(63, 1475526525, 1, 1),
(65, 1516894753, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `s_gallerylocalizations`
--

CREATE TABLE `s_gallerylocalizations` (
  `gid` int(11) NOT NULL DEFAULT '0',
  `galleryname` varchar(255) NOT NULL DEFAULT '',
  `gallery_desc` text,
  `lang` char(2) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_gallerylocalizations`
--

INSERT INTO `s_gallerylocalizations` (`gid`, `galleryname`, `gallery_desc`, `lang`) VALUES
(63, 'Nuestra Galería', '<p>Lorem ipsum dolor sit amet</p>', 'es'),
(63, 'Our Gallery', '<p>Lorem ipsum dolor sit amet</p>', 'en'),
(65, 'Auto', '', 'en'),
(65, '', '', 'es');

-- --------------------------------------------------------

--
-- Table structure for table `s_gallery_images`
--

CREATE TABLE `s_gallery_images` (
  `imgid` int(11) NOT NULL,
  `gid` int(11) NOT NULL DEFAULT '0',
  `img_name` varchar(255) NOT NULL DEFAULT '',
  `img_date` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_gallery_images`
--

INSERT INTO `s_gallery_images` (`imgid`, `gid`, `img_name`, `img_date`) VALUES
(545, 63, '1.jpg', 1475571289),
(546, 63, '2.jpg', 1475571289),
(547, 63, '3.jpg', 1475571289),
(548, 63, '4.jpg', 1475571289),
(554, 65, '12-2013-ford-mustang-gt-review.jpg', 1516894776),
(555, 65, '13FordMustang_22.jpg', 1516894777),
(556, 65, '2013-ford-mustang-califor_600x0w.jpg', 1516894777),
(557, 65, '2013-Ford-Mustang-Rear-Sequential-LEDs.jpg', 1516894777),
(558, 65, '2013-ford-mustang_100381239_l.jpg', 1516894777),
(559, 65, '2013-Ford-Mustang-GT-Ringbrothers-front-three-quarter-2.jpg', 1516894777),
(560, 65, '2013-Ford-Mustang-Shelby-GT500-Interior.jpg', 1516894777),
(561, 65, '2013-ford-mustang-shelby-gt500-super-snake-widebody.jpg', 1516894777),
(562, 65, '056307-2013-ford-mustang-gt-review-by-john-heilig.1-lg.jpg', 1516894777),
(563, 65, 'ford_mustang_gt_2013_interior.jpg', 1516894777),
(564, 65, 'lamborghini-sesto-elemento-concept-in-detail-49.jpg', 1516894778),
(565, 65, 'New-Ford-Mustang-2013-HD-Wallpaper.jpg', 1516894778);

-- --------------------------------------------------------

--
-- Table structure for table `s_logs`
--

CREATE TABLE `s_logs` (
  `logsid` int(11) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `ip` varchar(100) DEFAULT NULL,
  `comp` text,
  `date_int` int(11) NOT NULL DEFAULT '0',
  `module_name` varchar(255) NOT NULL,
  `moduleid` int(11) NOT NULL DEFAULT '0',
  `operation_mode` varchar(150) NOT NULL,
  `page` varchar(150) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_logs`
--

INSERT INTO `s_logs` (`logsid`, `user_id`, `ip`, `comp`, `date_int`, `module_name`, `moduleid`, `operation_mode`, `page`) VALUES
(838, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.170 Safari/537.36 OPR/53.0.2907.99', 1530070268, 'logout', 0, 'logout', 'module=action&act=logout'),
(839, 0, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.170 Safari/537.36 OPR/53.0.2907.99', 1530070268, 'login', 0, 'login_page', 'module=login'),
(840, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.170 Safari/537.36 OPR/53.0.2907.99', 1530070273, 'login', 0, 'login_ok', 'module=login'),
(841, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.170 Safari/537.36 OPR/53.0.2907.99', 1530070275, 'logout', 0, 'logout', 'module=action&act=logout'),
(842, 0, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.170 Safari/537.36 OPR/53.0.2907.99', 1530070275, 'login', 0, 'login_page', 'module=login'),
(843, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.170 Safari/537.36 OPR/53.0.2907.99', 1530070282, 'login', 0, 'login_ok', 'module=login'),
(844, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.170 Safari/537.36 OPR/53.0.2907.99', 1530070858, 'logout', 0, 'logout', 'module=action&act=logout'),
(845, 0, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.170 Safari/537.36 OPR/53.0.2907.99', 1530070858, 'login', 0, 'login_page', 'module=login'),
(846, 0, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.170 Safari/537.36 OPR/53.0.2907.99', 1530070893, 'login', 0, 'login_page', 'module=login'),
(847, 0, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.170 Safari/537.36 OPR/53.0.2907.99', 1530070926, 'login', 0, 'login_page', 'module=login'),
(848, 0, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.170 Safari/537.36 OPR/53.0.2907.99', 1530071013, 'login', 0, 'login_page', 'module=login'),
(849, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.170 Safari/537.36 OPR/53.0.2907.99', 1530071016, 'login', 0, 'login_ok', 'module=login'),
(850, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.170 Safari/537.36 OPR/53.0.2907.99', 1530071019, 'logout', 0, 'logout', 'module=action&act=logout'),
(851, 0, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.170 Safari/537.36 OPR/53.0.2907.99', 1530071019, 'login', 0, 'login_page', 'module=login'),
(852, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.170 Safari/537.36 OPR/53.0.2907.99', 1530071030, 'login', 0, 'login_ok', 'module=login'),
(853, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.170 Safari/537.36 OPR/53.0.2907.99', 1530071033, 'logout', 0, 'logout', 'module=action&act=logout'),
(854, 0, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.170 Safari/537.36 OPR/53.0.2907.99', 1530071033, 'login', 0, 'login_page', 'module=login'),
(855, 0, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.170 Safari/537.36 OPR/53.0.2907.99', 1530071044, 'login', 0, 'login_page', 'module=login'),
(856, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.170 Safari/537.36 OPR/53.0.2907.99', 1530071056, 'login', 0, 'login_ok', 'module=login'),
(857, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.170 Safari/537.36 OPR/53.0.2907.99', 1530071063, 'logout', 0, 'logout', 'module=action&act=logout'),
(858, 0, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.170 Safari/537.36 OPR/53.0.2907.99', 1530071063, 'login', 0, 'login_page', 'module=login'),
(859, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.170 Safari/537.36 OPR/53.0.2907.99', 1530071091, 'login', 0, 'login_ok', 'module=login'),
(860, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.170 Safari/537.36 OPR/53.0.2907.99', 1530071121, 'manage_users', 10, 'update_user', 'module=manage_users&userid=10'),
(861, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.170 Safari/537.36 OPR/53.0.2907.99', 1530071124, 'logout', 0, 'logout', 'module=action&act=logout'),
(862, 0, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.170 Safari/537.36 OPR/53.0.2907.99', 1530071124, 'login', 0, 'login_page', 'module=login'),
(863, 0, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.170 Safari/537.36 OPR/53.0.2907.99', 1530071134, 'login', 0, 'login_page', 'module=login'),
(864, 10, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.170 Safari/537.36 OPR/53.0.2907.99', 1530071140, 'login', 0, 'login_ok', 'module=login'),
(865, 10, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.170 Safari/537.36 OPR/53.0.2907.99', 1530071143, 'logout', 0, 'logout', 'module=action&act=logout'),
(866, 0, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.170 Safari/537.36 OPR/53.0.2907.99', 1530071143, 'login', 0, 'login_page', 'module=login'),
(867, 0, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.170 Safari/537.36 OPR/53.0.2907.99', 1530071148, 'login', 0, 'login_incorrect', 'module=login'),
(868, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.170 Safari/537.36 OPR/53.0.2907.99', 1530071155, 'login', 0, 'login_ok', 'module=login'),
(869, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.170 Safari/537.36 OPR/53.0.2907.99', 1530071158, 'logout', 0, 'logout', 'module=action&act=logout'),
(870, 0, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.170 Safari/537.36 OPR/53.0.2907.99', 1530071159, 'login', 0, 'login_page', 'module=login'),
(871, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.170 Safari/537.36 OPR/53.0.2907.99', 1530071165, 'login', 0, 'login_ok', 'module=login'),
(872, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.170 Safari/537.36 OPR/53.0.2907.99', 1530071174, 'manage_users', 13, 'update_user', 'module=manage_users&userid=13'),
(873, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.170 Safari/537.36 OPR/53.0.2907.99', 1530071179, 'manage_users', 15, 'delete_user', 'module=manage_users'),
(874, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.170 Safari/537.36 OPR/53.0.2907.99', 1530071183, 'logout', 0, 'logout', 'module=action&act=logout'),
(875, 0, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.170 Safari/537.36 OPR/53.0.2907.99', 1530071183, 'login', 0, 'login_page', 'module=login'),
(876, 12, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.170 Safari/537.36 OPR/53.0.2907.99', 1530071191, 'login', 0, 'login_ok', 'module=login'),
(877, 12, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.170 Safari/537.36 OPR/53.0.2907.99', 1530071193, 'logout', 0, 'logout', 'module=action&act=logout'),
(878, 0, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.170 Safari/537.36 OPR/53.0.2907.99', 1530071193, 'login', 0, 'login_page', 'module=login'),
(879, 12, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.170 Safari/537.36 OPR/53.0.2907.99', 1530071205, 'login', 0, 'login_ok', 'module=login'),
(880, 12, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.170 Safari/537.36 OPR/53.0.2907.99', 1530071208, 'logout', 0, 'logout', 'module=action&act=logout'),
(881, 0, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.170 Safari/537.36 OPR/53.0.2907.99', 1530071208, 'login', 0, 'login_page', 'module=login'),
(882, 0, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.170 Safari/537.36 OPR/53.0.2907.99', 1530352607, 'login', 0, 'login_page', 'module=login'),
(883, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.170 Safari/537.36 OPR/53.0.2907.99', 1530352627, 'login', 0, 'login_ok', 'module=login'),
(884, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.170 Safari/537.36 OPR/53.0.2907.99', 1530352665, 'modules', 1000057, 'add_module', 'module=modules&mode=add'),
(885, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.170 Safari/537.36 OPR/53.0.2907.99', 1530352680, 'modules', 1000057, 'delete_module', 'module=modules'),
(886, 0, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.170 Safari/537.36 OPR/53.0.2907.99', 1530962295, 'login', 0, 'login_page', 'module=login'),
(887, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.170 Safari/537.36 OPR/53.0.2907.99', 1530962304, 'login', 0, 'login_ok', 'module=login'),
(888, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.170 Safari/537.36 OPR/53.0.2907.99', 1531104053, 'section', 560, 'section_update', 'module=section&c_id=560'),
(889, 1, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36 OPR/62.0.3331.72', 1564471821, 'login', 0, 'login_ok', 'module=login'),
(890, 0, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36', 1568788255, 'login', 0, 'login_page', 'module=login'),
(891, 0, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36', 1568788260, 'login', 0, 'login_incorrect', 'module=login'),
(892, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36', 1568788264, 'login', 0, 'login_ok', 'module=login'),
(893, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36', 1568788638, 'modules', 1000058, 'add_module', 'module=modules&mode=add'),
(894, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36', 1568791174, 'members', 2, 'delete_user', 'module=members'),
(895, 0, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36', 1568808489, 'login', 0, 'login_page', 'module=login'),
(896, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36', 1568808492, 'login', 0, 'login_ok', 'module=login'),
(897, 0, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36', 1568810057, 'login', 0, 'login_page', 'module=login'),
(898, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36', 1568810159, 'modules', 1000059, 'add_module', 'module=modules&mode=add'),
(899, 0, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36', 1568872444, 'login', 0, 'login_page', 'module=login'),
(900, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36', 1568872446, 'login', 0, 'login_ok', 'module=login'),
(901, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36', 1568875359, 'logout', 0, 'logout', 'module=action&act=logout'),
(902, 0, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36', 1568875359, 'login', 0, 'login_page', 'module=login'),
(903, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36', 1568875372, 'login', 0, 'login_ok', 'module=login'),
(904, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36', 1568876756, 'members', 3, 'update_member', 'module=members&perid=3&mod=changepassword'),
(905, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36', 1568876788, 'members', 3, 'update_member', 'module=members&perid=3&mod=changepassword'),
(906, 0, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579036862, 'login', 0, 'login_page', 'module=login'),
(907, 0, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579036865, 'login', 0, 'login_incorrect', 'module=login'),
(908, 0, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579036868, 'login', 0, 'login_incorrect', 'module=login'),
(909, 0, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579036911, 'login', 0, 'login_incorrect', 'module=login'),
(910, 0, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579036915, 'login', 0, 'login_incorrect', 'module=login'),
(911, 0, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579036920, 'login', 0, 'login_incorrect', 'module=login'),
(912, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579036984, 'login', 0, 'login_ok', 'module=login'),
(913, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579037112, 'modules', 1000060, 'add_module', 'module=modules&mode=add'),
(914, 0, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579121511, 'login', 0, 'login_page', 'module=login'),
(915, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579121518, 'login', 0, 'login_ok', 'module=login'),
(916, 0, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36', 1579181887, 'login', 0, 'login_page', 'module=login'),
(917, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36', 1579181891, 'login', 0, 'login_ok', 'module=login'),
(918, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36', 1579182844, 'questions', 0, 'cat_add', 'module=questions&cateditid=-1'),
(919, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579204419, 'questions', 1, 'cat_add', 'module=questions&cateditid=-1'),
(920, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579204451, 'questions', 1, 'cat_update', 'module=questions&cateditid=1'),
(921, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579204517, 'questions', 1, 'cat_update', 'module=questions&cateditid=1'),
(922, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579204552, 'questions', 1, 'cat_update', 'module=questions&cateditid=1'),
(923, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579204577, 'questions', 1, 'cat_update', 'module=questions&cateditid=1'),
(924, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579204708, 'questions', 1, 'cat_update', 'module=questions&cateditid=1'),
(925, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579204788, 'questions', 1, 'cat_update', 'module=questions&cateditid=1'),
(926, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579204985, 'questions', 1, 'cat_update', 'module=questions&cateditid=1'),
(927, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579205055, 'questions', 1, 'cat_add', 'module=questions&cateditid=-1'),
(928, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579205271, 'questions', 1, 'cat_update', 'module=questions&cateditid=1'),
(929, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579205350, 'questions', 1, 'cat_update', 'module=questions&cateditid=1'),
(930, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579205366, 'questions', 1, 'cat_update', 'module=questions&cateditid=1'),
(931, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579205371, 'questions', 1, 'cat_update', 'module=questions&cateditid=1'),
(932, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579205499, 'questions', 1, 'cat_delete', 'module=questions'),
(933, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579205507, 'questions', 2, 'cat_add', 'module=questions&cateditid=-1'),
(934, 0, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579466745, 'login', 0, 'login_page', 'module=login'),
(935, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579466753, 'login', 0, 'login_ok', 'module=login'),
(936, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579467258, 'questions', 3, 'cat_add', 'module=questions&cateditid=-1'),
(937, 0, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579509013, 'login', 0, 'login_page', 'module=login'),
(938, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579509016, 'login', 0, 'login_ok', 'module=login'),
(939, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579511835, 'questions', 2, 'cat_update', 'module=questions&cateditid=2'),
(940, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579511856, 'questions', 3, 'cat_update', 'module=questions&cateditid=3'),
(941, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579511877, 'questions', 4, 'cat_add', 'module=questions&cateditid=-1'),
(942, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579511895, 'questions', 5, 'cat_add', 'module=questions&cateditid=-1'),
(943, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579513826, 'questions', 1, 'questions_add', 'module=questions&catid=2&questionid=-1'),
(944, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579515538, 'questions', 1, 'questions_update', 'module=questions&catid=2&questionid=1'),
(945, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579515614, 'questions', 1, 'questions_update', 'module=questions&catid=2&questionid=1'),
(946, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579515814, 'questions', 1, 'questions_update', 'module=questions&catid=2&questionid=1'),
(947, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579515822, 'questions', 1, 'questions_update', 'module=questions&catid=2&questionid=1'),
(948, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579515963, 'questions', 1, 'questions_update', 'module=questions&catid=2&questionid=1'),
(949, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579516366, 'questions', 1, 'questions_update', 'module=questions&catid=2&questionid=1'),
(950, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579516485, 'logout', 0, 'logout', 'module=action&act=logout'),
(951, 0, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579516485, 'login', 0, 'login_page', 'module=login'),
(952, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579516490, 'login', 0, 'login_ok', 'module=login'),
(953, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579526086, 'questions', 1, 'answers_update', 'module=questions&mod=answers&act=edit&questionid=1'),
(954, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579526541, 'questions', 1, 'answers_update', 'module=questions&mod=answers&act=edit&questionid=1'),
(955, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579526637, 'questions', 1, 'answers_update', 'module=questions&mod=answers&act=edit&questionid=1'),
(956, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579526642, 'questions', 1, 'answers_update', 'module=questions&mod=answers&act=edit&questionid=1'),
(957, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579526997, 'questions', 1, 'answers_update', 'module=questions&mod=answers&answerid=-1&questionid=1'),
(958, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579527163, 'questionanswers', 1, 'answers_add', 'module=questions&mod=answers&answerid=-1&questionid=1'),
(959, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579527403, 'questionanswers', 2, 'answers_add', 'module=questions&mod=answers&answerid=-1&questionid=-1'),
(960, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579527528, 'questions', 2, 'answers_update', 'module=questions&catid=-1&mod=answers&answerid=2'),
(961, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579527610, 'questionanswers', 1, 'answers_add', 'module=questions&mod=answers&answerid=-1&questionid=1'),
(962, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579528232, 'questions', 2, 'questions_add', 'module=questions&catid=2&questionid=-1'),
(963, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579528246, 'questionanswers', 2, 'answers_add', 'module=questions&mod=answers&answerid=-1&questionid=2'),
(964, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579528303, 'questions', 2, 'answers_update', 'module=questions&questionid=2&mod=answers&answerid=2'),
(965, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579528328, 'questions', 2, 'answers_update', 'module=questions&questionid=2&mod=answers&answerid=2'),
(966, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579528533, 'questions', 2, 'answers_delete', 'module=questions&mod=answers&questionid=2'),
(967, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579528545, 'questionanswers', 3, 'answers_add', 'module=questions&mod=answers&answerid=-1&questionid=2'),
(968, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579528622, 'questionanswers', 1, 'answers_add', 'module=questions&mod=answers&answerid=-1&questionid=2'),
(969, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579528640, 'questionanswers', 2, 'answers_add', 'module=questions&mod=answers&answerid=-1&questionid=-1'),
(970, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579528794, 'questionanswers', 1, 'answers_add', 'module=questions&mod=answers&answerid=-1&questionid=2'),
(971, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579528879, 'questionanswers', 1, 'answers_add', 'module=questions&mod=answers&answerid=-1&questionid=1'),
(972, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579528899, 'questionanswers', 2, 'answers_add', 'module=questions&mod=answers&answerid=-1&questionid=1'),
(973, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579529207, 'questions', 2, 'answers_update', 'module=questions&questionid=1&mod=answers&answerid=2'),
(974, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579529269, 'questions', 1, 'answers_update', 'module=questions&questionid=1&mod=answers&answerid=1'),
(975, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579529284, 'questions', 2, 'answers_update', 'module=questions&questionid=1&mod=answers&answerid=2'),
(976, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579529310, 'questions', 2, 'answers_update', 'module=questions&questionid=1&mod=answers&answerid=2'),
(977, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579529316, 'questions', 1, 'answers_update', 'module=questions&questionid=1&mod=answers&answerid=1'),
(978, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579530661, 'questions', 2, 'answers_update', 'module=questions&questionid=1&mod=answers&answerid=2'),
(979, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579532274, 'section', 539, 'section_delete', 'module=section'),
(980, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579532274, 'section', 540, 'section_delete', 'module=section'),
(981, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579532274, 'section', 541, 'section_delete', 'module=section'),
(982, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579532274, 'section', 542, 'section_delete', 'module=section'),
(983, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579532274, 'section', 543, 'section_delete', 'module=section'),
(984, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579532274, 'section', 560, 'section_delete', 'module=section'),
(985, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579532274, 'section', 538, 'section_delete', 'module=section'),
(986, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579532346, 'section', 571, 'section_add', 'module=section&c_id=-1&to=119'),
(987, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579532376, 'section', 572, 'section_add', 'module=section&c_id=-1&to=119'),
(988, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579532386, 'section', 573, 'section_add', 'module=section&c_id=-1&to=119'),
(989, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579532406, 'section', 574, 'section_add', 'module=section&c_id=-1&to=119'),
(990, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579532417, 'section', 574, 'section_update', 'module=section&c_id=574'),
(991, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36', 1579615357, 'questions', 2, 'cat_update', 'module=questions&cateditid=2'),
(992, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36', 1579615362, 'questions', 2, 'cat_update', 'module=questions&cateditid=2'),
(993, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36', 1579615395, 'questions', 2, 'cat_update', 'module=questions&cateditid=2'),
(994, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36', 1579615428, 'questions', 2, 'cat_update', 'module=questions&cateditid=2'),
(995, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36', 1579615480, 'questions', 2, 'cat_update', 'module=questions&cateditid=2'),
(996, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36', 1579615550, 'questions', 2, 'cat_update', 'module=questions&cateditid=2'),
(997, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36', 1579615608, 'questions', 2, 'cat_update', 'module=questions&cateditid=2'),
(998, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36', 1579616717, 'questions', 3, 'cat_update', 'module=questions&cateditid=3'),
(999, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36', 1579616720, 'questions', 4, 'cat_update', 'module=questions&cateditid=4'),
(1000, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36', 1579616726, 'questions', 5, 'cat_update', 'module=questions&cateditid=5'),
(1001, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36', 1579616773, 'questions', 2, 'cat_update', 'module=questions&cateditid=2'),
(1002, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36', 1579616923, 'questions', 3, 'cat_update', 'module=questions&cateditid=3'),
(1003, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36', 1579616932, 'questions', 4, 'cat_update', 'module=questions&cateditid=4'),
(1004, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36', 1579616940, 'questions', 5, 'cat_update', 'module=questions&cateditid=5'),
(1005, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36', 1579617171, 'questions', 2, 'cat_update', 'module=questions&cateditid=2'),
(1006, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36', 1579617218, 'questions', 2, 'cat_update', 'module=questions&cateditid=2'),
(1007, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36', 1579617332, 'questions', 2, 'cat_update', 'module=questions&cateditid=2'),
(1008, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36', 1579617338, 'questions', 2, 'cat_update', 'module=questions&cateditid=2'),
(1009, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36', 1579617404, 'questions', 2, 'cat_update', 'module=questions&cateditid=2'),
(1010, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36', 1579617476, 'questions', 2, 'cat_update', 'module=questions&cateditid=2'),
(1011, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36', 1579617678, 'questions', 3, 'cat_update', 'module=questions&cateditid=3'),
(1012, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36', 1579617711, 'questions', 4, 'cat_update', 'module=questions&cateditid=4'),
(1013, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36', 1579617732, 'questions', 5, 'cat_update', 'module=questions&cateditid=5'),
(1014, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36', 1579618384, 'section', 530, 'section_delete', 'module=section'),
(1015, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36', 1579618387, 'section', 531, 'section_delete', 'module=section&exp=294'),
(1016, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36', 1579618389, 'section', 554, 'section_delete', 'module=section&exp=294'),
(1017, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36', 1579618389, 'section', 555, 'section_delete', 'module=section&exp=294'),
(1018, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36', 1579618389, 'section', 556, 'section_delete', 'module=section&exp=294'),
(1019, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36', 1579618389, 'section', 557, 'section_delete', 'module=section&exp=294'),
(1020, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36', 1579618389, 'section', 558, 'section_delete', 'module=section&exp=294'),
(1021, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36', 1579618389, 'section', 553, 'section_delete', 'module=section&exp=294'),
(1022, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36', 1579618391, 'section', 559, 'section_delete', 'module=section&exp=294'),
(1023, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36', 1579618393, 'section', 534, 'section_delete', 'module=section&exp=294'),
(1024, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36', 1579621524, 'questions', 2, 'questions_update', 'module=questions&mod=tests&catid=2&testid=-1'),
(1025, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36', 1579621970, 'tests', 1, 'tests_add', 'module=questions&mod=tests&catid=2&testid=-1'),
(1026, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36', 1579622645, 'tests', 1, 'tests_add', 'module=questions&mod=tests&catid=2&testid=-1'),
(1027, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36', 1579622717, 'tests', 1, 'tests_add', 'module=questions&mod=tests&catid=2&testid=-1'),
(1028, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36', 1579622990, 'questions', 1, 'questions_update', 'module=questions&mod=tests&catid=2&testid=1'),
(1029, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36', 1579623002, 'questions', 1, 'questions_update', 'module=questions&mod=tests&catid=2&testid=1'),
(1030, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36', 1579623007, 'questions', 1, 'questions_update', 'module=questions&mod=tests&catid=2&testid=1'),
(1031, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36', 1579623054, 'questions', 1, 'questions_update', 'module=questions&mod=tests&catid=2&testid=1'),
(1032, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36', 1579623064, 'questions', 1, 'questions_update', 'module=questions&mod=tests&catid=2&testid=1'),
(1033, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36', 1579623118, 'tests', 1, 'tests_update', 'module=questions&mod=tests&catid=2&testid=1'),
(1034, 0, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579631701, 'login', 0, 'login_page', 'module=login'),
(1035, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579631704, 'login', 0, 'login_ok', 'module=login'),
(1036, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579632014, 'questions', 1, 'questions_add', 'module=questions&mod=questions&testid=1&questionid=-1'),
(1037, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579632089, 'questions', 2, 'questions_add', 'module=questions&mod=questions&testid=1&questionid=-1'),
(1038, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579634234, 'questions', 2, 'questions_update', 'module=questions&mod=questions&testid=1&questionid=2'),
(1039, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579634300, 'questions', 2, 'questions_update', 'module=questions&mod=questions&testid=1&questionid=2'),
(1040, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579634384, 'questions', 2, 'questions_update', 'module=questions&mod=questions&testid=1&questionid=2'),
(1041, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579634640, 'questions', 2, 'questions_update', 'module=questions&mod=questions&testid=1&questionid=2'),
(1042, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579634646, 'questions', 1, 'questions_delete', 'module=questions&mod=questions&testid=1'),
(1043, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579635573, 'questionanswers', 3, 'answers_add', 'module=questions&mod=answers&answerid=-1&questionid=2'),
(1044, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579635856, 'questions', 3, 'answers_update', 'module=questions&questionid=2&mod=answers&answerid=3'),
(1045, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579635863, 'questions', 3, 'answers_update', 'module=questions&questionid=2&mod=answers&answerid=3'),
(1046, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579636816, 'tests', 1, 'tests_add', 'module=questions&mod=tests&catid=2&testid=-1'),
(1047, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579636845, 'questions', 1, 'questions_add', 'module=questions&mod=questions&testid=1&questionid=-1'),
(1048, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579636861, 'questions', 2, 'questions_add', 'module=questions&mod=questions&testid=1&questionid=-1'),
(1049, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579636872, 'questions', 3, 'questions_add', 'module=questions&mod=questions&testid=1&questionid=-1'),
(1050, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579636888, 'questions', 4, 'questions_add', 'module=questions&mod=questions&testid=1&questionid=-1'),
(1051, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579637405, 'modules', 1000061, 'add_module', 'module=modules&mode=add'),
(1052, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579638903, 'tests', 2, 'tests_add', 'module=questions&mod=tests&catid=2&testid=-1'),
(1053, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579638919, 'tests', 3, 'tests_add', 'module=questions&mod=tests&catid=2&testid=-1'),
(1054, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579639520, 'questions', 2, 'cat_update', 'module=questions&cateditid=2'),
(1055, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579639820, 'modules', 1000062, 'add_module', 'module=modules&mode=add'),
(1056, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1579640431, 'modules', 1000063, 'add_module', 'module=modules&mode=add'),
(1057, 0, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36', 1579716972, 'login', 0, 'login_page', 'module=login'),
(1058, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36', 1579716976, 'login', 0, 'login_ok', 'module=login'),
(1059, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36', 1579719541, 'questionanswers', 1, 'answers_add', 'module=questions&mod=answers&answerid=-1&questionid=4'),
(1060, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36', 1579719558, 'questionanswers', 2, 'answers_add', 'module=questions&mod=answers&answerid=-1&questionid=4'),
(1061, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36', 1579719574, 'questionanswers', 3, 'answers_add', 'module=questions&mod=answers&answerid=-1&questionid=4'),
(1062, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36', 1579719965, 'questionanswers', 4, 'answers_add', 'module=questions&mod=answers&answerid=-1&questionid=1'),
(1063, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36', 1579720009, 'questionanswers', 5, 'answers_add', 'module=questions&mod=answers&answerid=-1&questionid=1'),
(1064, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36', 1579720019, 'questionanswers', 6, 'answers_add', 'module=questions&mod=answers&answerid=-1&questionid=1'),
(1065, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36', 1579720088, 'questionanswers', 7, 'answers_add', 'module=questions&mod=answers&answerid=-1&questionid=1'),
(1066, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36', 1579720100, 'questionanswers', 8, 'answers_add', 'module=questions&mod=answers&answerid=-1&questionid=1'),
(1067, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36', 1579727554, 'questions', 5, 'questions_add', 'module=questions&mod=questions&testid=1&questionid=-1'),
(1068, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36', 1579727560, 'questions', 5, 'questions_delete', 'module=questions&mod=questions&testid=1'),
(1069, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36', 1579727568, 'questions', 6, 'questions_add', 'module=questions&mod=questions&testid=1&questionid=-1'),
(1070, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36', 1579727831, 'questions', 6, 'questions_delete', 'module=questions&mod=questions&testid=1'),
(1071, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36', 1579727869, 'questions', 7, 'answers_delete', 'module=questions&mod=answers&questionid=1'),
(1072, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36', 1579727871, 'questions', 8, 'answers_delete', 'module=questions&mod=answers&questionid=1'),
(1073, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36', 1580150129, 'questions', 4, 'questions_update', 'module=questions&mod=questions&testid=1&questionid=4');
INSERT INTO `s_logs` (`logsid`, `user_id`, `ip`, `comp`, `date_int`, `module_name`, `moduleid`, `operation_mode`, `page`) VALUES
(1074, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36', 1580150139, 'questions', 4, 'questions_update', 'module=questions&mod=questions&testid=1&questionid=4'),
(1075, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36', 1580156106, 'tests_correct_answers', 1, 'correctanswers_add', 'module=questions&mod=correct_answers&answerid=-1&questionid=3'),
(1076, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36', 1580158224, 'tests_correct_answers', 1, 'correctanswers_add', 'module=questions&mod=correct_answers&answerid=-1&testid=1'),
(1077, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36', 1580158305, 'tests_correct_answers', 1, 'correctanswers_add', 'module=questions&mod=correct_answers&answerid=-1&testid=1'),
(1078, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36', 1580158343, 'tests_correct_answers', 2, 'correctanswers_add', 'module=questions&mod=correct_answers&answerid=-1&testid=1'),
(1079, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36', 1580158533, 'questions', 2, 'answers_update', 'module=questions&testid=1&mod=correct_answers&answerid=2'),
(1080, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36', 1580158566, 'questions', 2, 'answers_update', 'module=questions&testid=1&mod=correct_answers&answerid=2'),
(1081, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36', 1580158584, 'questions', 2, 'answers_update', 'module=questions&testid=1&mod=correct_answers&answerid=2'),
(1082, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36', 1580158608, 'questions', 2, 'correct_answers_update', 'module=questions&testid=1&mod=correct_answers&answerid=2'),
(1083, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36', 1580158658, 'questions', 2, 'correct_answers', 'module=questions&mod=correct_answers&testid=1'),
(1084, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36', 1580158667, 'tests_correct_answers', 3, 'correctanswers_add', 'module=questions&mod=correct_answers&answerid=-1&testid=1');

-- --------------------------------------------------------

--
-- Table structure for table `s_media`
--

CREATE TABLE `s_media` (
  `media_id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `comment` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `add_date` int(11) DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `s_media_likes`
--

CREATE TABLE `s_media_likes` (
  `media_id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `add_date` varchar(128) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `s_members`
--

CREATE TABLE `s_members` (
  `member_id` int(11) NOT NULL,
  `m_user` varchar(255) NOT NULL,
  `m_mail` varchar(255) NOT NULL,
  `m_name` varchar(255) NOT NULL,
  `m_pass` varchar(255) NOT NULL,
  `m_phone` varchar(256) NOT NULL,
  `m_birthdate` varchar(16) NOT NULL,
  `interestid` int(11) NOT NULL DEFAULT '0',
  `private` int(11) NOT NULL DEFAULT '0',
  `comment` text CHARACTER SET utf32 NOT NULL,
  `activate_info` varchar(255) NOT NULL,
  `reg_date` int(11) DEFAULT '0',
  `last_visit_date` varchar(256) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_members`
--

INSERT INTO `s_members` (`member_id`, `m_user`, `m_mail`, `m_name`, `m_pass`, `m_phone`, `m_birthdate`, `interestid`, `private`, `comment`, `activate_info`, `reg_date`, `last_visit_date`, `active`) VALUES
(5, 'AbbasovAzik', 'abbasov.azik@gmail.com', 'Azer', 'e10adc3949ba59abbe56e057f20f883e', '', '', 1, 0, '', '', 1569104423, '1569104423', 1);

-- --------------------------------------------------------

--
-- Table structure for table `s_menu`
--

CREATE TABLE `s_menu` (
  `mid` int(11) NOT NULL,
  `parentid` int(11) NOT NULL DEFAULT '0',
  `htaccessname` varchar(255) NOT NULL DEFAULT '',
  `indexed` tinyint(1) NOT NULL DEFAULT '1',
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  `template` smallint(5) NOT NULL DEFAULT '0',
  `menutype` enum('content','link','module','nolink','welcome','static','contacts','top_menu','middle_menu','foot_menu','bottom_menu') NOT NULL DEFAULT 'content',
  `link` varchar(255) NOT NULL DEFAULT '',
  `newwindow` tinyint(1) NOT NULL DEFAULT '0',
  `position` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_menu`
--

INSERT INTO `s_menu` (`mid`, `parentid`, `htaccessname`, `indexed`, `visible`, `template`, `menutype`, `link`, `newwindow`, `position`) VALUES
(294, 0, 'top_menu', 1, 1, 0, 'top_menu', '', 0, 1),
(119, 0, 'foot_menu', 1, 1, 0, 'foot_menu', '', 0, 3),
(260, 0, 'middle_menu', 1, 1, 0, 'middle_menu', '', 0, 2),
(261, 0, 'bottom_menu', 1, 1, 0, 'bottom_menu', '', 0, 4),
(297, 295, 'contacts', 0, 0, 0, 'content', '', 0, 2),
(295, 0, 'static', 1, 1, 0, 'static', '', 0, 5),
(537, 260, '', 1, 1, 0, 'module', 'news', 0, 2),
(536, 261, '', 1, 1, 0, 'content', '', 0, 2),
(535, 261, '', 1, 1, 0, 'content', '', 0, 1),
(533, 260, '', 1, 1, 5, 'content', '', 0, 3),
(532, 260, '', 1, 1, 3, 'content', '', 0, 1),
(571, 119, '', 1, 1, 0, 'content', '', 0, 1),
(544, 533, '', 1, 1, 0, 'content', '', 0, 1),
(545, 533, '', 1, 1, 0, 'content', '', 0, 2),
(546, 533, '', 1, 1, 0, 'content', '', 0, 3),
(547, 533, '', 1, 1, 0, 'content', '', 0, 4),
(548, 533, '', 1, 1, 0, 'content', '', 0, 5),
(549, 533, '', 1, 1, 0, 'content', '', 0, 6),
(550, 533, '', 1, 1, 0, 'content', '', 0, 7),
(551, 533, '', 1, 1, 0, 'content', '', 0, 8),
(552, 533, '', 1, 1, 0, 'content', '', 0, 9),
(572, 119, '', 1, 1, 0, 'content', '', 0, 2),
(573, 119, '', 1, 1, 0, 'content', '', 0, 3),
(574, 119, '', 1, 1, 0, 'link', 'http://www.nar.az', 1, 4),
(570, 260, '', 1, 1, 0, 'link', '535', 0, 4);

-- --------------------------------------------------------

--
-- Table structure for table `s_menulocalizations`
--

CREATE TABLE `s_menulocalizations` (
  `mid` int(11) NOT NULL,
  `lang` char(2) DEFAULT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `header` varchar(255) NOT NULL DEFAULT '',
  `comment` longtext NOT NULL,
  `text` longtext NOT NULL,
  `seo_header` varchar(250) NOT NULL,
  `seo_description` text NOT NULL,
  `seo_keywords` text NOT NULL,
  `link` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_menulocalizations`
--

INSERT INTO `s_menulocalizations` (`mid`, `lang`, `name`, `header`, `comment`, `text`, `seo_header`, `seo_description`, `seo_keywords`, `link`, `slug`) VALUES
(294, 'en', 'top_menu', '', '', '', 'top_menu', '', 'top_menu', 'http://', 'top-menu'),
(119, 'az', 'foot_menu', '', '', '', 'foot_menu', '', 'foot_menu', 'http://', 'foot-menu'),
(260, 'az', 'middle_menu', '', '', '', 'middle_menu', '', 'middle_menu', 'http://', 'middle-menu'),
(261, 'az', 'bottom_menu', '', '', '', 'bottom_menu', '', 'bottom_menu', 'http://', 'bottom-menu'),
(295, 'en', 'static', '', '', '', 'static', '', 'static', 'http://', 'static'),
(295, 'az', 'static', '', '', '', 'static', '', 'static', 'http://', 'static'),
(294, 'az', 'top_menu', '', '', '', 'top_menu', '', 'top_menu', 'http://', 'top-menu'),
(261, 'ru', 'bottom_menu', '', '', '', 'bottom_menu', '', 'bottom_menu', 'http://', 'bottom-menu'),
(294, 'ru', 'top_menu', '', '', '', 'top_menu', '', 'top_menu', 'http://', 'top-menu'),
(260, 'en', 'middle_menu', '', '', '', 'middle_menu', '', 'middle_menu', 'http://', 'middle-menu'),
(260, 'ru', 'middle_menu', '', '', '', 'middle_menu', '', 'middle_menu', 'http://', 'middle-menu'),
(119, 'en', 'foot_menu', '', '', '', 'foot_menu', '', 'foot_menu', 'http://', 'foot-menu'),
(119, 'ru', 'foot_menu', '', '', '', 'foot_menu', '', 'foot_menu', 'http://', 'foot-menu'),
(295, 'ru', 'static', '', '', '', 'static', '', 'static', 'http://', 'static'),
(261, 'en', 'bottom_menu', '', '', '', 'bottom_menu', '', 'bottom_menu', 'http://', 'bottom-menu'),
(532, 'en', 'Gallery', '', '', '<p>{GALLERY_63}</p>', '', '', '', 'http://', 'galery'),
(532, 'es', 'Galería', '', '', '<p>{GALLERY_63}</p>', '', '', '', 'http://', 'galera'),
(533, 'es', 'Servicios', '', '', '', '', '', '', 'http://', 'servicios'),
(297, 'es', 'Contactos', '', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3164.289259162295!2d-120.7989351!3d37.5246781!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8091042b3386acd7%3A0x3b4a4cedc60363dd!2sMain+St%2C+Denair%2C+CA+95316%2C+Hoa+K%E1%BB%B3!5e0!3m2!1svi!2s!4v1434016649434\" width=\"100%\" height=\"450px\" frameborder=\"0\" style=\"border: 0\"></iframe>', '<p>SED UT PERSPICIATIS UNDE OMNIS ISTE NATUS ERROR SIT VOLUPTATEM ACCUSANTIUM DOLOREMQUE LAUDANTIUM, TOTAM REM APERIAM.</p>\r\n\r\n<p>JL.Kemacetan timur no.23. block.Q3<br />\r\nJakarta-Indonesia</p>\r\n\r\n<p>+994 12 409-0239<br />\r\ninfo@microphp.com</p>', '', '', '', 'http://', 'contact-page'),
(297, 'en', 'Contacts', '', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3164.289259162295!2d-120.7989351!3d37.5246781!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8091042b3386acd7%3A0x3b4a4cedc60363dd!2sMain+St%2C+Denair%2C+CA+95316%2C+Hoa+K%E1%BB%B3!5e0!3m2!1svi!2s!4v1434016649434\" width=\"100%\" height=\"450px\" frameborder=\"0\" style=\"border: 0\"></iframe>', '<p>SED UT PERSPICIATIS UNDE OMNIS ISTE NATUS ERROR SIT VOLUPTATEM ACCUSANTIUM DOLOREMQUE LAUDANTIUM, TOTAM REM APERIAM.</p>\r\n\r\n<p>JL.Kemacetan timur no.23. block.Q3<br />\r\nJakarta-Indonesia</p>\r\n\r\n<p>+994 12 409-0239<br />\r\ninfo@microphp.com</p>', 'Contacts', 'Contact us', 'Contacts', 'http://', 'contact-page'),
(535, 'es', 'Política de privacidad', '', '', '', '', '', '', 'http://', 'política-de-privacidad'),
(535, 'en', 'Privacy Policy', '', '', '', '', '', '', 'http://', 'privacy-policy'),
(536, 'en', 'Terms of Use', '', '', '', '', '', '', 'http://', 'terms-of-use'),
(536, 'es', 'Términos de Uso', '', '', '', '', '', '', 'http://', 'terminos-de-uso'),
(537, 'en', 'News', '', '', '', '', '', '', 'http://', 'name1'),
(537, 'es', 'Noticias', '', '', '', '', '', '', 'http://', 'noticias'),
(571, 'uz', 'Xidmət haqqında', '', '', '<p>Xidmət haqqında test</p>', '', '', '', 'http://', 'xidmet-haqqinda'),
(571, 'ru', 'Xidmət haqqında', '', '', '', '', '', '', 'http://', 'xidmet-haqqinda'),
(574, 'ru', 'www.nar.az', '', '', '', '', '', '', 'http://', 'wwwnaraz'),
(574, 'uz', 'www.nar.az', '', '', '', '', '', '', 'http://', 'wwwnaraz'),
(573, 'uz', 'Dəstək', '', '', '', '', '', '', 'http://', 'destek'),
(573, 'ru', 'Dəstək', '', '', '', '', '', '', 'http://', 'destek'),
(572, 'uz', 'Oferta', '', '', '', '', '', '', 'http://', 'oferta'),
(533, 'en', 'Services', '', '', '', '', '', '', 'http://', 'services'),
(544, 'en', 'Image Format', '298', '', '<p>This format perfectly fits in case you need only a single image for your post display. Use Featured image option to add image to the...</p>', '', '', '', 'http://', 'image-format'),
(544, 'es', 'Formato de imagen', '298', '', '<p>Este formato se adapta perfectamente en caso de que necesite una &uacute;nica imagen para su visualizaci&oacute;n posterior. Utilice la opci&oacute;n Imagen destacada para agregar una imagen a la ...</p>', '', '', '', 'http://', 'formato-de-imagen'),
(545, 'en', 'Image Format', '299', '', '<p>This format perfectly fits in case you need only a single image for your post display. Use Featured image option to add image to the...</p>', '', '', '', 'http://', 'image-format1'),
(545, 'es', 'Formato de imagen', '299', '', '<p>Este formato se adapta perfectamente en caso de que necesite una &uacute;nica imagen para su visualizaci&oacute;n posterior. Utilice la opci&oacute;n Imagen destacada para agregar una imagen a la ...</p>', '', '', '', 'http://', 'formato-de-imagen1'),
(546, 'en', 'Image Format', '300', '', '<p>This format perfectly fits in case you need only a single image for your post display. Use Featured image option to add image to the...</p>', '', '', '', 'http://', 'image-format2'),
(546, 'es', 'Formato de imagen', '300', '', '<p>Este formato se adapta perfectamente en caso de que necesite una &uacute;nica imagen para su visualizaci&oacute;n posterior. Utilice la opci&oacute;n Imagen destacada para agregar una imagen a la ...</p>', '', '', '', 'http://', 'formato-de-imagen2'),
(547, 'en', 'Image Format', '301', '', '<p>This format perfectly fits in case you need only a single image for your post display. Use Featured image option to add image to the...</p>', '', '', '', 'http://', 'image-format3'),
(547, 'es', 'Formato de imagen', '301', '', '<p>Este formato se adapta perfectamente en caso de que necesite una &uacute;nica imagen para su visualizaci&oacute;n posterior. Utilice la opci&oacute;n Imagen destacada para agregar una imagen a la ...</p>', '', '', '', 'http://', 'formato-de-imagen3'),
(548, 'en', 'Image Format', '302', '', '<p>This format perfectly fits in case you need only a single image for your post display. Use Featured image option to add image to the...</p>', '', '', '', 'http://', 'image-format4'),
(548, 'es', 'Formato de imagen', '302', '', '<p>Este formato se adapta perfectamente en caso de que necesite una &uacute;nica imagen para su visualizaci&oacute;n posterior. Utilice la opci&oacute;n Imagen destacada para agregar una imagen a la ...</p>', '', '', '', 'http://', 'formato-de-imagen4'),
(549, 'en', 'Image Format', '303', '', '<p>This format perfectly fits in case you need only a single image for your post display. Use Featured image option to add image to the...</p>', '', '', '', 'http://', 'image-format5'),
(549, 'es', 'Formato de imagen', '303', '', '<p>Este formato se adapta perfectamente en caso de que necesite una &uacute;nica imagen para su visualizaci&oacute;n posterior. Utilice la opci&oacute;n Imagen destacada para agregar una imagen a la ...</p>', '', '', '', 'http://', 'formato-de-imagen5'),
(550, 'en', 'Image Format', '304', '', '<p>This format perfectly fits in case you need only a single image for your post display. Use Featured image option to add image to the...</p>', '', '', '', 'http://', 'image-format6'),
(550, 'es', 'Formato de imagen', '304', '', '<p>Este formato se adapta perfectamente en caso de que necesite una &uacute;nica imagen para su visualizaci&oacute;n posterior. Utilice la opci&oacute;n Imagen destacada para agregar una imagen a la ...</p>', '', '', '', 'http://', 'formato-de-imagen6'),
(551, 'en', 'Image Format', '305', '', '<p>This format perfectly fits in case you need only a single image for your post display. Use Featured image option to add image to the...</p>', '', '', '', 'http://', 'image-format7'),
(551, 'es', 'Formato de imagen', '305', '', '<p>Este formato se adapta perfectamente en caso de que necesite una &uacute;nica imagen para su visualizaci&oacute;n posterior. Utilice la opci&oacute;n Imagen destacada para agregar una imagen a la ...</p>', '', '', '', 'http://', 'formato-de-imagen7'),
(552, 'en', 'Image Format', '302', '', '<p>This format perfectly fits in case you need only a single image for your post display. Use Featured image option to add image to the...</p>', '', '', '', 'http://', 'image-format8'),
(552, 'es', 'Formato de imagen', '302', '', '<p>Este formato se adapta perfectamente en caso de que necesite una &uacute;nica imagen para su visualizaci&oacute;n posterior. Utilice la opci&oacute;n Imagen destacada para agregar una imagen a la ...</p>', '', '', '', 'http://', 'formato-de-imagen8'),
(572, 'ru', 'Oferta', '', '', '', '', '', '', 'http://', 'oferta'),
(570, 'es', 'Inside link', '', '', '', '', '', '', 'http://', 'inside-link'),
(570, 'en', 'Inside link', '', '', '', '', '', '', 'http://', 'inside-link');

-- --------------------------------------------------------

--
-- Table structure for table `s_message_sends`
--

CREATE TABLE `s_message_sends` (
  `msgid` int(11) NOT NULL,
  `create_date` int(11) NOT NULL DEFAULT '0',
  `send_datetime` int(11) NOT NULL DEFAULT '0',
  `mail_to` varchar(255) NOT NULL DEFAULT '',
  `mail_subject` text NOT NULL,
  `mail_txt` longtext NOT NULL,
  `sended` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_message_sends`
--

INSERT INTO `s_message_sends` (`msgid`, `create_date`, `send_datetime`, `mail_to`, `mail_subject`, `mail_txt`, `sended`) VALUES
(66, 1475760110, 1485076195, 'microphprashad@gmail.com', 'mCMS - Contact form', '<b>Name:</b> Rashad<br/>\r\n<b>E-mail:</b> rashad@aliev.info<br/>\r\n<b>Subject:</b> test subject<br/>\r\n<b>Message:</b> test subjecttest subjecttest subjecttest subject<br/>\r\n', 1),
(67, 1475825321, 1499495902, 'microphprashad@gmail.com', 'mCMS - Contact form', '<b>Name:</b> Rashad<br/>\r\n<b>E-mail:</b> rashad@aliev.info<br/>\r\n<b>Subject:</b> Testing system<br/>\r\n<b>Message:</b> Testing system<br/>\r\n', 1),
(69, 1481725578, 0, 'rashad2@aliev.info', 'mCMS - Subscribe', 'Hello Rashad,<br/>\nYou subscribed to system<br/>\n<b>E-poçt:</b> rashad2@aliev.info<br/>\n<b>Phone:</b> 0506472737<br/><br/>\nYou can cancel your subscription at any time.<br/>\n<a href=\"http://mcms.localhost/mcms_5.x.x/\">Unsubscribe</a> http://mcms.localhost/mcms_5.x.x/<br/><br/>\n', 0);

-- --------------------------------------------------------

--
-- Table structure for table `s_modulelocalizations`
--

CREATE TABLE `s_modulelocalizations` (
  `moduleid` int(11) NOT NULL,
  `lang` char(2) DEFAULT NULL,
  `title` varchar(255) NOT NULL DEFAULT '',
  `seo_title` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_modulelocalizations`
--

INSERT INTO `s_modulelocalizations` (`moduleid`, `lang`, `title`, `seo_title`) VALUES
(1000056, 'en', 'Events log', ''),
(9, 'ru', 'Files', ''),
(999799, 'ru', 'Manage Users', ''),
(999899, 'en', 'Modules', ''),
(999999, 'es', 'Profile Options', ''),
(9, 'en', 'Files', ''),
(999799, 'en', 'Manage Users', ''),
(10, 'es', 'Gallery', ''),
(10, 'ru', 'Gallery', ''),
(50, 'ru', 'Slider', ''),
(50, 'en', 'Slider', ''),
(100, 'es', 'Contactos', 'Contact us'),
(101, 'es', 'Suscripción', 'Suscripción'),
(1, 'en', 'Main Page', ''),
(550, 'es', 'Blocks', ''),
(550, 'ru', 'Blocks', ''),
(7, 'es', 'News', ''),
(9, 'az', 'Fayllar', ''),
(10, 'en', 'Gallery', ''),
(50, 'az', 'Slider', ''),
(550, 'en', 'Blocks', ''),
(1, 'az', 'İlk Səhifə', ''),
(999799, 'az', 'Manage Users', ''),
(999899, 'az', 'Modullar', ''),
(1000056, 'es', 'Events log', ''),
(10, 'az', 'Qalereya', ''),
(5, 'ru', 'Section Manager', ''),
(7, 'en', 'News', ''),
(100, 'en', 'Contacts', 'Contact us'),
(50, 'es', '', ''),
(9, 'es', '', ''),
(1, 'ru', 'Main Page', ''),
(1, 'es', '', ''),
(999999, 'en', 'Profile Options', ''),
(999799, 'es', '', ''),
(999899, 'ru', 'Modules', ''),
(999899, 'es', '', ''),
(101, 'en', 'Subscribe', 'Subscription'),
(5, 'uz', 'Section Manager', ''),
(1000017, 'en', 'Inside pages', ''),
(1000017, 'az', 'Daxili dəhifələr', ''),
(1000018, 'az', 'Xəbərlər ətraflı', ''),
(1000018, 'en', 'News more', ''),
(1000018, 'ru', '', ''),
(1000018, 'es', '', ''),
(1000031, 'az', 'Abune - imtina', ''),
(1000031, 'en', 'Unsubscribe', ''),
(1000031, 'ru', 'Unsubscribe', ''),
(1000031, 'es', 'Unsubscribe', ''),
(1000017, 'ru', '', ''),
(1000017, 'es', '', ''),
(550, 'az', 'Blocks', ''),
(1000054, 'es', 'Correos enviados', 'Correos enviados'),
(999998, 'ru', 'Site options', ''),
(999998, 'kz', 'Site options', ''),
(1000054, 'en', 'Sent mails', 'Sent mails'),
(1000058, 'az', 'Members', 'Members'),
(1000058, 'en', 'Members', 'Members'),
(1000058, 'ru', 'Members', 'Members'),
(1000059, 'az', 'Media', 'Media'),
(1000059, 'en', 'Media', 'Media'),
(1000059, 'ru', 'Media', 'Media'),
(1000060, 'az', 'Questions', 'Questions'),
(1000060, 'en', 'Questions', 'Questions'),
(1000060, 'ru', 'Test', 'Questions'),
(1000061, 'az', 'MCategory', 'CAtegory'),
(1000061, 'en', 'MCategory', 'CAtegory'),
(1000061, 'ru', 'MCategory', 'CAtegory'),
(1000062, 'az', 'MQuestion', 'Question'),
(1000062, 'en', 'MQuestion', 'Question'),
(1000062, 'ru', 'MQuestion', 'Question'),
(1000063, 'az', 'MTest', 'Test'),
(1000063, 'en', 'MTest', 'Test'),
(1000063, 'ru', 'MTest', 'Test');

-- --------------------------------------------------------

--
-- Table structure for table `s_modules`
--

CREATE TABLE `s_modules` (
  `moduleid` int(11) NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` text,
  `deleteable` tinyint(1) NOT NULL DEFAULT '1',
  `have_view` tinyint(1) NOT NULL DEFAULT '1',
  `have_admin` tinyint(1) NOT NULL DEFAULT '1',
  `have_cnfg` tinyint(1) NOT NULL DEFAULT '0',
  `admin_files` text NOT NULL,
  `view_files` text NOT NULL,
  `admin_template_files` text NOT NULL,
  `view_template_files` text NOT NULL,
  `tables` varchar(255) NOT NULL DEFAULT '',
  `seo_table` varchar(50) NOT NULL DEFAULT '',
  `table_id` varchar(15) NOT NULL DEFAULT 'mid',
  `title_header` varchar(20) NOT NULL DEFAULT 'name',
  `title_description` varchar(20) NOT NULL DEFAULT 'text',
  `title_keywords` varchar(20) NOT NULL DEFAULT 'name',
  `type` tinyint(1) NOT NULL DEFAULT '3',
  `icon` varchar(100) NOT NULL DEFAULT 'fa fa-check',
  `view_type` enum('stat','stat1x','tile','tile2x','gallery') NOT NULL DEFAULT 'stat',
  `position` int(11) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_modules`
--

INSERT INTO `s_modules` (`moduleid`, `name`, `description`, `deleteable`, `have_view`, `have_admin`, `have_cnfg`, `admin_files`, `view_files`, `admin_template_files`, `view_template_files`, `tables`, `seo_table`, `table_id`, `title_header`, `title_description`, `title_keywords`, `type`, `icon`, `view_type`, `position`, `active`) VALUES
(5, 'section', 'For manage site menus', 0, 0, 1, 0, 'tree_json.php,tree_move.php', '', '', '', 'menu,menulocalizations', '', 'mid', 'seo_header', 'seo_description', 'seo_keywords', 3, 'fa fa-bars', 'stat', 1, 1),
(9, 'file_manager', 'Photo, document - Files manager', 0, 0, 1, 0, 'ajax_file_list.php', '', '', '', 'files', '', 'mid', 'name', 'text', 'name', 3, 'fa fa-upload', 'stat1x', 3, 1),
(999799, 'manage_users', 'Manage admin users and permissions', 0, 0, 1, 0, '', '', '', '', 'users', '', 'mid', 'name', 'text', 'name', 2, 'fa fa-user', 'stat1x', 11, 1),
(999899, 'modules', 'Manage site modules', 0, 0, 1, 0, '', '', '', '', 'modules', '', 'mid', 'name', 'text', 'name', 1, 'fa fa-code', 'stat1x', 12, 1),
(999999, 'options_profile', 'Profile Options', 0, 1, 1, 0, '', '', '', '', 'options', '', '', '', '', '', 3, 'fa fa-user', 'stat', 16, 1),
(10, 'gallery_manager', 'Photo Gallery manager', 0, 0, 1, 0, '', '', '', '', '', '', 'mid', 'name', 'text', 'name', 3, 'fa fa-camera', 'gallery', 4, 1),
(50, 'slider', 'Slider manager', 0, 0, 1, 0, '', '', '', '', 'slider', '', 'mid', 'name', 'text', 'name', 3, 'fa fa-tasks', 'stat', 6, 1),
(7, 'news', 'Site news and events', 0, 1, 1, 0, 'ajax_news_list.php,news_drag.php', '', '', '', '', 'newscategorylocalizations', 'catid', 'name', 'header', 'name', 3, 'fa fa-newspaper', 'tile2x', 2, 1),
(100, 'contacts', 'Contacts manager', 0, 1, 1, 0, '', '', '', '', '', '', 'mid', 'name', 'text', 'name', 3, 'fa fa-user', 'tile', 7, 1),
(101, 'subscribe', 'Manage subscribers, templates, send broadcast', 0, 1, 1, 0, '', '', '', '', '', '', 'mid', 'name', 'text', 'name', 3, 'fa fa-envelope', 'tile', 8, 1),
(1, 'homepage', 'module for click and back to index page', 0, 1, 0, 0, '', '', '', '', '', '', 'mid', 'name', 'text', 'name', 3, 'fa fa-check', 'stat', 18, 1),
(550, 'blocks', 'Manage site banners,&nbsp;blocks, widgets', 0, 1, 1, 0, '', '', '', '', '', '', 'mid', 'name', 'text', 'name', 3, 'fa fa-eye', 'tile2x', 5, 1),
(1000017, 'inside', 'Site menu link more page (inside page)', 0, 1, 0, 0, '', '', '', '', 'menu,menulocalizations', 'menulocalizations', 'mid', 'name', 'text', 'name', 3, 'fa fa-file', 'stat', 13, 1),
(1000018, 'news_more', 'Site news more page', 0, 1, 0, 0, '', '', '', '', 'news,newslocalizations', 'newslocalizations', 'newsid', 'seo_header', 'seo_description', 'seo_keywords', 3, 'fa fa-check', 'stat', 14, 1),
(1000031, 'unsubscribe', 'Unsubscribtion module', 0, 1, 0, 1, '', '', '', '', '', '', 'mid', 'name', 'text', 'name', 3, 'fa fa-envelope', 'stat', 17, 1),
(1000054, 'mail_log', 'Sent mails', 0, 0, 1, 0, '', '', 'mail_log.tpl', '', '', '', '', '', '', '', 3, 'fa fa-envelope-open', 'tile', 9, 1),
(999998, 'options_site', 'Site options, meta tags', 0, 0, 1, 0, '', '', '', '', 'options', '', 'mid', 'name', 'text', 'name', 3, 'fa fa-wrench', 'stat', 15, 1),
(1000056, 'mcms_log', 'Events log', 0, 0, 1, 0, '', '', 'mcms_log.tpl', '', '', '', '', '', '', '', 3, 'fa fa-calendar-alt', 'tile', 10, 1),
(1000058, 'members', 'Members', 0, 0, 1, 0, 'members.admin.php', '', 'members.tpl', '', '', '', '', '', '', '', 3, 'fa fa-list-alt', 'tile', 4, 1),
(1000059, 'media', 'Media', 0, 0, 1, 0, 'media.admin.php', '', 'media.tpl', '', '', '', '', '', '', '', 3, 'fa fa-list-alt', 'tile', 4, 1),
(1000060, 'questions', 'Questions', 0, 0, 1, 0, 'questions.admin.php', '', 'questions.tpl', '', '', '', '', '', '', '', 3, 'fa fa-list-alt', 'tile', 4, 1),
(1000061, 'MCategory', 'Category', 0, 1, 0, 0, '', 'mcategory.view.php', '', 'mcategory.tpl', '', '', '', '', '', '', 3, 'fa fa-list-alt', 'tile', 4, 1),
(1000062, 'MQuestion', 'Questions', 0, 1, 0, 0, '', 'mquestion.view.php', '', 'mquestion.tpl', '', '', '', '', '', '', 3, 'fa fa-list-alt', 'tile', 4, 1),
(1000063, 'MTest', 'Tests', 0, 1, 0, 0, '', 'mtest.view.php', '', 'mtest.tpl', '', '', '', '', '', '', 3, 'fa fa-list-alt', 'tile', 4, 1);

-- --------------------------------------------------------

--
-- Table structure for table `s_news`
--

CREATE TABLE `s_news` (
  `newsid` int(11) NOT NULL,
  `catid` int(11) NOT NULL DEFAULT '0',
  `newsdate` varchar(255) NOT NULL DEFAULT '',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `index` tinyint(1) NOT NULL DEFAULT '1',
  `template` smallint(5) NOT NULL DEFAULT '0',
  `img_show` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_news`
--

INSERT INTO `s_news` (`newsid`, `catid`, `newsdate`, `active`, `index`, `template`, `img_show`) VALUES
(839, 35, '1500058800', 1, 1, 0, 1),
(838, 36, '1521489600', 1, 1, 0, 1),
(837, 35, '1505502000', 1, 0, 0, 1),
(834, 35, '1502737200', 1, 1, 0, 1),
(835, 35, '1549396800', 1, 1, 0, 1),
(836, 35, '1491246000', 1, 1, 0, 1),
(844, 35, '1520366400', 1, 1, 0, 1),
(843, 35, '1518379200', 1, 1, 0, 1),
(842, 35, '1489089600', 1, 1, 0, 1),
(841, 35, '1496602800', 1, 1, 0, 1),
(840, 35, '1479758400', 1, 1, 0, 1),
(846, 35, '1547064000', 1, 1, 0, 1),
(847, 36, '1543780800', 1, 1, 0, 1),
(848, 35, '1553198400', 1, 1, 0, 1),
(849, 35, '1501527600', 1, 1, 0, 1),
(850, 35, '1498849200', 1, 1, 0, 1),
(851, 35, '1512936000', 1, 1, 0, 1),
(852, 35, '1506798000', 1, 1, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `s_newscategories`
--

CREATE TABLE `s_newscategories` (
  `catid` int(11) NOT NULL,
  `position` int(5) NOT NULL DEFAULT '1',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `index` tinyint(1) NOT NULL DEFAULT '1',
  `event` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_newscategories`
--

INSERT INTO `s_newscategories` (`catid`, `position`, `active`, `index`, `event`) VALUES
(36, 1, 1, 1, 1),
(35, 0, 1, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `s_newscategorylocalizations`
--

CREATE TABLE `s_newscategorylocalizations` (
  `catid` int(11) NOT NULL DEFAULT '0',
  `lang` varchar(5) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `header` varchar(255) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_newscategorylocalizations`
--

INSERT INTO `s_newscategorylocalizations` (`catid`, `lang`, `name`, `header`) VALUES
(36, 'en', 'Events', '<p>Events category</p>'),
(35, 'es', 'Noticias', '<p>Categor&iacute;a de noticias</p>'),
(35, 'en', 'News', '<p>News category</p>'),
(36, 'es', 'Eventos', '');

-- --------------------------------------------------------

--
-- Table structure for table `s_newslocalizations`
--

CREATE TABLE `s_newslocalizations` (
  `newsid` int(11) NOT NULL DEFAULT '0',
  `lang` varchar(5) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `header` varchar(255) NOT NULL DEFAULT '',
  `comment` longtext NOT NULL,
  `text` longtext NOT NULL,
  `seo_header` varchar(250) NOT NULL,
  `seo_description` text NOT NULL,
  `seo_keywords` text NOT NULL,
  `slug` varchar(255) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_newslocalizations`
--

INSERT INTO `s_newslocalizations` (`newsid`, `lang`, `name`, `header`, `comment`, `text`, `seo_header`, `seo_description`, `seo_keywords`, `slug`) VALUES
(849, 'es', 'Most visited countries', '284', '', '', 'Most visited countries', '', '', 'most-visited-countries'),
(843, 'es', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras hendrerit.', '315', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras odio arcu, elementum sed aliquet non, consectetur non neque. Curabitur tempor pretium velit id consequat. Nunc felis dolor, eleifend ut pretium at, euismod id nunc.', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc aliquet molestie tortor ut luctus. Proin id hendrerit tortor, et vehicula risus. Aliquam adipiscing magna iaculis nunc malesuada facilisis. Aliquam justo magna, accumsan sed malesuada eu, pretium sit amet arcu. Ut lobortis eu augue sed fringilla. Curabitur vestibulum est sed sollicitudin venenatis. Etiam vitae commodo ligula. Donec lacinia elit non tellus convallis porta. Vivamus eu nunc ac urna vestibulum laoreet. Ut tincidunt imperdiet lobortis. Donec bibendum nunc sed lorem adipiscing vehicula.</p>\r\n\r\n<p>Etiam condimentum elit non interdum tincidunt. Nullam sagittis, sapien sit amet iaculis ullamcorper, odio lorem accumsan leo, ac eleifend mauris libero vel nibh. Aliquam aliquam libero quis nisl mattis ornare. Cras dictum vitae mi eu blandit. Maecenas ac aliquam sem. Morbi dictum felis eu augue blandit molestie. Integer aliquam vitae enim ultricies vehicula. Duis suscipit, augue sed molestie vehicula, turpis mi rutrum arcu, at pulvinar sapien est at massa. Ut interdum purus at turpis pellentesque ultricies. Aenean luctus dapibus elit non pellentesque. Praesent posuere nisi a nibh suscipit, at sagittis mauris faucibus. Vestibulum vitae erat eros.</p>\r\n\r\n<p>Fusce tristique justo vel iaculis condimentum. Sed vitae arcu et risus commodo ornare. Fusce varius sapien laoreet, tincidunt leo non, dignissim nisl. Morbi ut luctus risus. Mauris laoreet in tellus sed luctus. Fusce eu augue eu mi sollicitudin ullamcorper. Cras consequat dui eu leo mattis varius.</p>\r\n\r\n<p>Praesent quam lorem, pharetra ac elementum quis, lobortis vel nunc. Nulla et cursus dolor, sit amet aliquam purus. Donec id sapien pulvinar, varius orci et, venenatis lectus. Nullam posuere vel lacus nec fermentum. Integer et rhoncus turpis. Cras blandit libero nisi, nec volutpat libero eleifend ac. Mauris lacinia lacus in convallis interdum. Donec laoreet magna fermentum, mattis orci sit amet, rutrum mauris.</p>\r\n\r\n<p>Nulla diam massa, pharetra vel vestibulum sed, egestas ut augue. Etiam tempor bibendum sagittis. Aliquam erat volutpat. Maecenas et eros et risus ullamcorper viverra ut eu dui. Mauris semper ut dui non ultrices. Integer a lectus a metus pellentesque congue eget ac orci. Donec fermentum mi quam, at sagittis eros rhoncus sed. Duis non laoreet odio. Nullam vulputate vehicula justo, eget blandit lacus feugiat sit amet.</p>\r\n\r\n<p>Integer ac quam vitae odio fringilla mattis vel sed ante. Etiam eu magna eleifend, mattis leo vel, porttitor nisl. Vestibulum ornare turpis at erat imperdiet, ac convallis felis lacinia. Nullam ultrices diam eu lorem ultrices, dapibus viverra felis ornare. Nam semper adipiscing urna non consequat. Morbi tempor laoreet felis, a pulvinar nunc commodo vel. Donec nec nulla ac turpis scelerisque interdum. Etiam tempor sem nisi. Sed lacinia id tellus sodales commodo. Quisque porttitor lobortis aliquam.</p>\r\n\r\n<p>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Ut id ultricies leo. Sed rhoncus, justo non convallis auctor, est augue condimentum lacus, ut porttitor leo tortor sit amet urna. Praesent ut ultrices nisi. Nunc tincidunt blandit cursus. Suspendisse potenti. Curabitur mollis metus nec odio vestibulum, sit amet iaculis sapien iaculis. Mauris non arcu at quam venenatis gravida in a ipsum. Vestibulum aliquet libero ut diam mattis fermentum. Donec dignissim dictum vulputate. Quisque commodo magna aliquet neque pretium dictum.</p>\r\n\r\n<p>Cras turpis mi, porta sed dignissim nec, posuere in velit. Duis eget magna at nisi auctor mattis. Donec eget pharetra velit, ac gravida neque. Integer fringilla turpis sed ipsum elementum facilisis. Morbi accumsan mi ut ligula vestibulum, sit amet cursus ligula imperdiet. Duis a euismod elit, non facilisis tortor. Mauris id ligula tellus. Sed ligula tellus, pharetra vel nisl a, placerat iaculis libero. Etiam pellentesque mauris libero, non dapibus nibh lobortis nec. Nam fermentum, urna non luctus semper, tellus leo rutrum risus, a tempor purus ligula non justo. Vivamus aliquam arcu a porttitor ultricies. Nunc sodales vel risus non mattis. Fusce id nisl dapibus, ornare dui at, vestibulum arcu.</p>', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras hendrerit.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras odio arcu, elementum sed aliquet non, consectetur non neque. Curabitur tempor pretium velit id consequat. Nunc felis dolor, eleifend ut pretium at, euismod id nunc.', '', 'lorem-ipsum-dolor-sit-amet-consectetur-adipiscing-elit-cras-hendrerit12'),
(835, 'en', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras hendrerit.', '284', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras odio arcu, elementum sed aliquet non, consectetur non neque. Curabitur tempor pretium velit id consequat. Nunc felis dolor, eleifend ut pretium at, euismod id nunc.', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc aliquet molestie tortor ut luctus. Proin id hendrerit tortor, et vehicula risus. Aliquam adipiscing magna iaculis nunc malesuada facilisis. Aliquam justo magna, accumsan sed malesuada eu, pretium sit amet arcu. Ut lobortis eu augue sed fringilla. Curabitur vestibulum est sed sollicitudin venenatis. Etiam vitae commodo ligula. Donec lacinia elit non tellus convallis porta. Vivamus eu nunc ac urna vestibulum laoreet. Ut tincidunt imperdiet lobortis. Donec bibendum nunc sed lorem adipiscing vehicula.</p>\r\n\r\n<p>Etiam condimentum elit non interdum tincidunt. Nullam sagittis, sapien sit amet iaculis ullamcorper, odio lorem accumsan leo, ac eleifend mauris libero vel nibh. Aliquam aliquam libero quis nisl mattis ornare. Cras dictum vitae mi eu blandit. Maecenas ac aliquam sem. Morbi dictum felis eu augue blandit molestie. Integer aliquam vitae enim ultricies vehicula. Duis suscipit, augue sed molestie vehicula, turpis mi rutrum arcu, at pulvinar sapien est at massa. Ut interdum purus at turpis pellentesque ultricies. Aenean luctus dapibus elit non pellentesque. Praesent posuere nisi a nibh suscipit, at sagittis mauris faucibus. Vestibulum vitae erat eros.</p>\r\n\r\n<p>Fusce tristique justo vel iaculis condimentum. Sed vitae arcu et risus commodo ornare. Fusce varius sapien laoreet, tincidunt leo non, dignissim nisl. Morbi ut luctus risus. Mauris laoreet in tellus sed luctus. Fusce eu augue eu mi sollicitudin ullamcorper. Cras consequat dui eu leo mattis varius.</p>\r\n\r\n<p>Praesent quam lorem, pharetra ac elementum quis, lobortis vel nunc. Nulla et cursus dolor, sit amet aliquam purus. Donec id sapien pulvinar, varius orci et, venenatis lectus. Nullam posuere vel lacus nec fermentum. Integer et rhoncus turpis. Cras blandit libero nisi, nec volutpat libero eleifend ac. Mauris lacinia lacus in convallis interdum. Donec laoreet magna fermentum, mattis orci sit amet, rutrum mauris.</p>\r\n\r\n<p>Nulla diam massa, pharetra vel vestibulum sed, egestas ut augue. Etiam tempor bibendum sagittis. Aliquam erat volutpat. Maecenas et eros et risus ullamcorper viverra ut eu dui. Mauris semper ut dui non ultrices. Integer a lectus a metus pellentesque congue eget ac orci. Donec fermentum mi quam, at sagittis eros rhoncus sed. Duis non laoreet odio. Nullam vulputate vehicula justo, eget blandit lacus feugiat sit amet.</p>\r\n\r\n<p>Integer ac quam vitae odio fringilla mattis vel sed ante. Etiam eu magna eleifend, mattis leo vel, porttitor nisl. Vestibulum ornare turpis at erat imperdiet, ac convallis felis lacinia. Nullam ultrices diam eu lorem ultrices, dapibus viverra felis ornare. Nam semper adipiscing urna non consequat. Morbi tempor laoreet felis, a pulvinar nunc commodo vel. Donec nec nulla ac turpis scelerisque interdum. Etiam tempor sem nisi. Sed lacinia id tellus sodales commodo. Quisque porttitor lobortis aliquam.</p>\r\n\r\n<p>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Ut id ultricies leo. Sed rhoncus, justo non convallis auctor, est augue condimentum lacus, ut porttitor leo tortor sit amet urna. Praesent ut ultrices nisi. Nunc tincidunt blandit cursus. Suspendisse potenti. Curabitur mollis metus nec odio vestibulum, sit amet iaculis sapien iaculis. Mauris non arcu at quam venenatis gravida in a ipsum. Vestibulum aliquet libero ut diam mattis fermentum. Donec dignissim dictum vulputate. Quisque commodo magna aliquet neque pretium dictum.</p>\r\n\r\n<p>Cras turpis mi, porta sed dignissim nec, posuere in velit. Duis eget magna at nisi auctor mattis. Donec eget pharetra velit, ac gravida neque. Integer fringilla turpis sed ipsum elementum facilisis. Morbi accumsan mi ut ligula vestibulum, sit amet cursus ligula imperdiet. Duis a euismod elit, non facilisis tortor. Mauris id ligula tellus. Sed ligula tellus, pharetra vel nisl a, placerat iaculis libero. Etiam pellentesque mauris libero, non dapibus nibh lobortis nec. Nam fermentum, urna non luctus semper, tellus leo rutrum risus, a tempor purus ligula non justo. Vivamus aliquam arcu a porttitor ultricies. Nunc sodales vel risus non mattis. Fusce id nisl dapibus, ornare dui at, vestibulum arcu.</p>\r\n\r\n<p>&nbsp;</p>', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras hendrerit.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras odio arcu, elementum sed aliquet non, consectetur non neque. Curabitur tempor pretium velit id consequat. Nunc felis dolor, eleifend ut pretium at, euismod id nunc.', '', 'lorem-ipsum-dolor-sit-amet-consectetur-adipiscing-elit-cras-hendrerit5'),
(840, 'es', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras hendrerit.', '288', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras odio arcu, elementum sed aliquet non, consectetur non neque. Curabitur tempor pretium velit id consequat. Nunc felis dolor, eleifend ut pretium at, euismod id nunc.', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc aliquet molestie tortor ut luctus. Proin id hendrerit tortor, et vehicula risus. Aliquam adipiscing magna iaculis nunc malesuada facilisis. Aliquam justo magna, accumsan sed malesuada eu, pretium sit amet arcu. Ut lobortis eu augue sed fringilla. Curabitur vestibulum est sed sollicitudin venenatis. Etiam vitae commodo ligula. Donec lacinia elit non tellus convallis porta. Vivamus eu nunc ac urna vestibulum laoreet. Ut tincidunt imperdiet lobortis. Donec bibendum nunc sed lorem adipiscing vehicula.</p>\r\n\r\n<p>Etiam condimentum elit non interdum tincidunt. Nullam sagittis, sapien sit amet iaculis ullamcorper, odio lorem accumsan leo, ac eleifend mauris libero vel nibh. Aliquam aliquam libero quis nisl mattis ornare. Cras dictum vitae mi eu blandit. Maecenas ac aliquam sem. Morbi dictum felis eu augue blandit molestie. Integer aliquam vitae enim ultricies vehicula. Duis suscipit, augue sed molestie vehicula, turpis mi rutrum arcu, at pulvinar sapien est at massa. Ut interdum purus at turpis pellentesque ultricies. Aenean luctus dapibus elit non pellentesque. Praesent posuere nisi a nibh suscipit, at sagittis mauris faucibus. Vestibulum vitae erat eros.</p>\r\n\r\n<p>Fusce tristique justo vel iaculis condimentum. Sed vitae arcu et risus commodo ornare. Fusce varius sapien laoreet, tincidunt leo non, dignissim nisl. Morbi ut luctus risus. Mauris laoreet in tellus sed luctus. Fusce eu augue eu mi sollicitudin ullamcorper. Cras consequat dui eu leo mattis varius.</p>\r\n\r\n<p>Praesent quam lorem, pharetra ac elementum quis, lobortis vel nunc. Nulla et cursus dolor, sit amet aliquam purus. Donec id sapien pulvinar, varius orci et, venenatis lectus. Nullam posuere vel lacus nec fermentum. Integer et rhoncus turpis. Cras blandit libero nisi, nec volutpat libero eleifend ac. Mauris lacinia lacus in convallis interdum. Donec laoreet magna fermentum, mattis orci sit amet, rutrum mauris.</p>\r\n\r\n<p>Nulla diam massa, pharetra vel vestibulum sed, egestas ut augue. Etiam tempor bibendum sagittis. Aliquam erat volutpat. Maecenas et eros et risus ullamcorper viverra ut eu dui. Mauris semper ut dui non ultrices. Integer a lectus a metus pellentesque congue eget ac orci. Donec fermentum mi quam, at sagittis eros rhoncus sed. Duis non laoreet odio. Nullam vulputate vehicula justo, eget blandit lacus feugiat sit amet.</p>\r\n\r\n<p>Integer ac quam vitae odio fringilla mattis vel sed ante. Etiam eu magna eleifend, mattis leo vel, porttitor nisl. Vestibulum ornare turpis at erat imperdiet, ac convallis felis lacinia. Nullam ultrices diam eu lorem ultrices, dapibus viverra felis ornare. Nam semper adipiscing urna non consequat. Morbi tempor laoreet felis, a pulvinar nunc commodo vel. Donec nec nulla ac turpis scelerisque interdum. Etiam tempor sem nisi. Sed lacinia id tellus sodales commodo. Quisque porttitor lobortis aliquam.</p>\r\n\r\n<p>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Ut id ultricies leo. Sed rhoncus, justo non convallis auctor, est augue condimentum lacus, ut porttitor leo tortor sit amet urna. Praesent ut ultrices nisi. Nunc tincidunt blandit cursus. Suspendisse potenti. Curabitur mollis metus nec odio vestibulum, sit amet iaculis sapien iaculis. Mauris non arcu at quam venenatis gravida in a ipsum. Vestibulum aliquet libero ut diam mattis fermentum. Donec dignissim dictum vulputate. Quisque commodo magna aliquet neque pretium dictum.</p>\r\n\r\n<p>Cras turpis mi, porta sed dignissim nec, posuere in velit. Duis eget magna at nisi auctor mattis. Donec eget pharetra velit, ac gravida neque. Integer fringilla turpis sed ipsum elementum facilisis. Morbi accumsan mi ut ligula vestibulum, sit amet cursus ligula imperdiet. Duis a euismod elit, non facilisis tortor. Mauris id ligula tellus. Sed ligula tellus, pharetra vel nisl a, placerat iaculis libero. Etiam pellentesque mauris libero, non dapibus nibh lobortis nec. Nam fermentum, urna non luctus semper, tellus leo rutrum risus, a tempor purus ligula non justo. Vivamus aliquam arcu a porttitor ultricies. Nunc sodales vel risus non mattis. Fusce id nisl dapibus, ornare dui at, vestibulum arcu.</p>', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras hendrerit.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras odio arcu, elementum sed aliquet non, consectetur non neque. Curabitur tempor pretium velit id consequat. Nunc felis dolor, eleifend ut pretium at, euismod id nunc.', '', 'lorem-ipsum-dolor-sit-amet-consectetur-adipiscing-elit-cras-hendrerit8'),
(837, 'es', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras hendrerit.', '297', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras odio arcu, elementum sed aliquet non, consectetur non neque. Curabitur tempor pretium velit id consequat. Nunc felis dolor, eleifend ut pretium at, euismod id nunc.', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc aliquet molestie tortor ut luctus. Proin id hendrerit tortor, et vehicula risus. Aliquam adipiscing magna iaculis nunc malesuada facilisis. Aliquam justo magna, accumsan sed malesuada eu, pretium sit amet arcu. Ut lobortis eu augue sed fringilla. Curabitur vestibulum est sed sollicitudin venenatis. Etiam vitae commodo ligula. Donec lacinia elit non tellus convallis porta. Vivamus eu nunc ac urna vestibulum laoreet. Ut tincidunt imperdiet lobortis. Donec bibendum nunc sed lorem adipiscing vehicula.</p>\r\n\r\n<p>Etiam condimentum elit non interdum tincidunt. Nullam sagittis, sapien sit amet iaculis ullamcorper, odio lorem accumsan leo, ac eleifend mauris libero vel nibh. Aliquam aliquam libero quis nisl mattis ornare. Cras dictum vitae mi eu blandit. Maecenas ac aliquam sem. Morbi dictum felis eu augue blandit molestie. Integer aliquam vitae enim ultricies vehicula. Duis suscipit, augue sed molestie vehicula, turpis mi rutrum arcu, at pulvinar sapien est at massa. Ut interdum purus at turpis pellentesque ultricies. Aenean luctus dapibus elit non pellentesque. Praesent posuere nisi a nibh suscipit, at sagittis mauris faucibus. Vestibulum vitae erat eros.</p>\r\n\r\n<p>Fusce tristique justo vel iaculis condimentum. Sed vitae arcu et risus commodo ornare. Fusce varius sapien laoreet, tincidunt leo non, dignissim nisl. Morbi ut luctus risus. Mauris laoreet in tellus sed luctus. Fusce eu augue eu mi sollicitudin ullamcorper. Cras consequat dui eu leo mattis varius.</p>\r\n\r\n<p>Praesent quam lorem, pharetra ac elementum quis, lobortis vel nunc. Nulla et cursus dolor, sit amet aliquam purus. Donec id sapien pulvinar, varius orci et, venenatis lectus. Nullam posuere vel lacus nec fermentum. Integer et rhoncus turpis. Cras blandit libero nisi, nec volutpat libero eleifend ac. Mauris lacinia lacus in convallis interdum. Donec laoreet magna fermentum, mattis orci sit amet, rutrum mauris.</p>\r\n\r\n<p>Nulla diam massa, pharetra vel vestibulum sed, egestas ut augue. Etiam tempor bibendum sagittis. Aliquam erat volutpat. Maecenas et eros et risus ullamcorper viverra ut eu dui. Mauris semper ut dui non ultrices. Integer a lectus a metus pellentesque congue eget ac orci. Donec fermentum mi quam, at sagittis eros rhoncus sed. Duis non laoreet odio. Nullam vulputate vehicula justo, eget blandit lacus feugiat sit amet.</p>\r\n\r\n<p>Integer ac quam vitae odio fringilla mattis vel sed ante. Etiam eu magna eleifend, mattis leo vel, porttitor nisl. Vestibulum ornare turpis at erat imperdiet, ac convallis felis lacinia. Nullam ultrices diam eu lorem ultrices, dapibus viverra felis ornare. Nam semper adipiscing urna non consequat. Morbi tempor laoreet felis, a pulvinar nunc commodo vel. Donec nec nulla ac turpis scelerisque interdum. Etiam tempor sem nisi. Sed lacinia id tellus sodales commodo. Quisque porttitor lobortis aliquam.</p>\r\n\r\n<p>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Ut id ultricies leo. Sed rhoncus, justo non convallis auctor, est augue condimentum lacus, ut porttitor leo tortor sit amet urna. Praesent ut ultrices nisi. Nunc tincidunt blandit cursus. Suspendisse potenti. Curabitur mollis metus nec odio vestibulum, sit amet iaculis sapien iaculis. Mauris non arcu at quam venenatis gravida in a ipsum. Vestibulum aliquet libero ut diam mattis fermentum. Donec dignissim dictum vulputate. Quisque commodo magna aliquet neque pretium dictum.</p>\r\n\r\n<p>Cras turpis mi, porta sed dignissim nec, posuere in velit. Duis eget magna at nisi auctor mattis. Donec eget pharetra velit, ac gravida neque. Integer fringilla turpis sed ipsum elementum facilisis. Morbi accumsan mi ut ligula vestibulum, sit amet cursus ligula imperdiet. Duis a euismod elit, non facilisis tortor. Mauris id ligula tellus. Sed ligula tellus, pharetra vel nisl a, placerat iaculis libero. Etiam pellentesque mauris libero, non dapibus nibh lobortis nec. Nam fermentum, urna non luctus semper, tellus leo rutrum risus, a tempor purus ligula non justo. Vivamus aliquam arcu a porttitor ultricies. Nunc sodales vel risus non mattis. Fusce id nisl dapibus, ornare dui at, vestibulum arcu.</p>', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras hendrerit.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras odio arcu, elementum sed aliquet non, consectetur non neque. Curabitur tempor pretium velit id consequat. Nunc felis dolor, eleifend ut pretium at, euismod id nunc.', '', 'lorem-ipsum-dolor-sit-amet-consectetur-adipiscing-elit-cras-hendrerit1'),
(846, 'es', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras hendrerit.', '281', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras odio arcu, elementum sed aliquet non, consectetur non neque. Curabitur tempor pretium velit id consequat. Nunc felis dolor, eleifend ut pretium at, euismod id nunc.', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc aliquet molestie tortor ut luctus. Proin id hendrerit tortor, et vehicula risus. Aliquam adipiscing magna iaculis nunc malesuada facilisis. Aliquam justo magna, accumsan sed malesuada eu, pretium sit amet arcu. Ut lobortis eu augue sed fringilla. Curabitur vestibulum est sed sollicitudin venenatis. Etiam vitae commodo ligula. Donec lacinia elit non tellus convallis porta. Vivamus eu nunc ac urna vestibulum laoreet. Ut tincidunt imperdiet lobortis. Donec bibendum nunc sed lorem adipiscing vehicula.</p>\r\n\r\n<p>Etiam condimentum elit non interdum tincidunt. Nullam sagittis, sapien sit amet iaculis ullamcorper, odio lorem accumsan leo, ac eleifend mauris libero vel nibh. Aliquam aliquam libero quis nisl mattis ornare. Cras dictum vitae mi eu blandit. Maecenas ac aliquam sem. Morbi dictum felis eu augue blandit molestie. Integer aliquam vitae enim ultricies vehicula. Duis suscipit, augue sed molestie vehicula, turpis mi rutrum arcu, at pulvinar sapien est at massa. Ut interdum purus at turpis pellentesque ultricies. Aenean luctus dapibus elit non pellentesque. Praesent posuere nisi a nibh suscipit, at sagittis mauris faucibus. Vestibulum vitae erat eros.</p>\r\n\r\n<p>Fusce tristique justo vel iaculis condimentum. Sed vitae arcu et risus commodo ornare. Fusce varius sapien laoreet, tincidunt leo non, dignissim nisl. Morbi ut luctus risus. Mauris laoreet in tellus sed luctus. Fusce eu augue eu mi sollicitudin ullamcorper. Cras consequat dui eu leo mattis varius.</p>\r\n\r\n<p>Praesent quam lorem, pharetra ac elementum quis, lobortis vel nunc. Nulla et cursus dolor, sit amet aliquam purus. Donec id sapien pulvinar, varius orci et, venenatis lectus. Nullam posuere vel lacus nec fermentum. Integer et rhoncus turpis. Cras blandit libero nisi, nec volutpat libero eleifend ac. Mauris lacinia lacus in convallis interdum. Donec laoreet magna fermentum, mattis orci sit amet, rutrum mauris.</p>\r\n\r\n<p>Nulla diam massa, pharetra vel vestibulum sed, egestas ut augue. Etiam tempor bibendum sagittis. Aliquam erat volutpat. Maecenas et eros et risus ullamcorper viverra ut eu dui. Mauris semper ut dui non ultrices. Integer a lectus a metus pellentesque congue eget ac orci. Donec fermentum mi quam, at sagittis eros rhoncus sed. Duis non laoreet odio. Nullam vulputate vehicula justo, eget blandit lacus feugiat sit amet.</p>\r\n\r\n<p>Integer ac quam vitae odio fringilla mattis vel sed ante. Etiam eu magna eleifend, mattis leo vel, porttitor nisl. Vestibulum ornare turpis at erat imperdiet, ac convallis felis lacinia. Nullam ultrices diam eu lorem ultrices, dapibus viverra felis ornare. Nam semper adipiscing urna non consequat. Morbi tempor laoreet felis, a pulvinar nunc commodo vel. Donec nec nulla ac turpis scelerisque interdum. Etiam tempor sem nisi. Sed lacinia id tellus sodales commodo. Quisque porttitor lobortis aliquam.</p>\r\n\r\n<p>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Ut id ultricies leo. Sed rhoncus, justo non convallis auctor, est augue condimentum lacus, ut porttitor leo tortor sit amet urna. Praesent ut ultrices nisi. Nunc tincidunt blandit cursus. Suspendisse potenti. Curabitur mollis metus nec odio vestibulum, sit amet iaculis sapien iaculis. Mauris non arcu at quam venenatis gravida in a ipsum. Vestibulum aliquet libero ut diam mattis fermentum. Donec dignissim dictum vulputate. Quisque commodo magna aliquet neque pretium dictum.</p>\r\n\r\n<p>Cras turpis mi, porta sed dignissim nec, posuere in velit. Duis eget magna at nisi auctor mattis. Donec eget pharetra velit, ac gravida neque. Integer fringilla turpis sed ipsum elementum facilisis. Morbi accumsan mi ut ligula vestibulum, sit amet cursus ligula imperdiet. Duis a euismod elit, non facilisis tortor. Mauris id ligula tellus. Sed ligula tellus, pharetra vel nisl a, placerat iaculis libero. Etiam pellentesque mauris libero, non dapibus nibh lobortis nec. Nam fermentum, urna non luctus semper, tellus leo rutrum risus, a tempor purus ligula non justo. Vivamus aliquam arcu a porttitor ultricies. Nunc sodales vel risus non mattis. Fusce id nisl dapibus, ornare dui at, vestibulum arcu.</p>', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras hendrerit.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras odio arcu, elementum sed aliquet non, consectetur non neque. Curabitur tempor pretium velit id consequat. Nunc felis dolor, eleifend ut pretium at, euismod id nunc.', '', 'lorem-ipsum-dolor-sit-amet-consectetur-adipiscing-elit-cras-hendrerit5'),
(840, 'en', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras hendrerit.', '288', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras odio arcu, elementum sed aliquet non, consectetur non neque. Curabitur tempor pretium velit id consequat. Nunc felis dolor, eleifend ut pretium at, euismod id nunc.', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc aliquet molestie tortor ut luctus. Proin id hendrerit tortor, et vehicula risus. Aliquam adipiscing magna iaculis nunc malesuada facilisis. Aliquam justo magna, accumsan sed malesuada eu, pretium sit amet arcu. Ut lobortis eu augue sed fringilla. Curabitur vestibulum est sed sollicitudin venenatis. Etiam vitae commodo ligula. Donec lacinia elit non tellus convallis porta. Vivamus eu nunc ac urna vestibulum laoreet. Ut tincidunt imperdiet lobortis. Donec bibendum nunc sed lorem adipiscing vehicula.</p>\r\n\r\n<p>Etiam condimentum elit non interdum tincidunt. Nullam sagittis, sapien sit amet iaculis ullamcorper, odio lorem accumsan leo, ac eleifend mauris libero vel nibh. Aliquam aliquam libero quis nisl mattis ornare. Cras dictum vitae mi eu blandit. Maecenas ac aliquam sem. Morbi dictum felis eu augue blandit molestie. Integer aliquam vitae enim ultricies vehicula. Duis suscipit, augue sed molestie vehicula, turpis mi rutrum arcu, at pulvinar sapien est at massa. Ut interdum purus at turpis pellentesque ultricies. Aenean luctus dapibus elit non pellentesque. Praesent posuere nisi a nibh suscipit, at sagittis mauris faucibus. Vestibulum vitae erat eros.</p>\r\n\r\n<p>Fusce tristique justo vel iaculis condimentum. Sed vitae arcu et risus commodo ornare. Fusce varius sapien laoreet, tincidunt leo non, dignissim nisl. Morbi ut luctus risus. Mauris laoreet in tellus sed luctus. Fusce eu augue eu mi sollicitudin ullamcorper. Cras consequat dui eu leo mattis varius.</p>\r\n\r\n<p>Praesent quam lorem, pharetra ac elementum quis, lobortis vel nunc. Nulla et cursus dolor, sit amet aliquam purus. Donec id sapien pulvinar, varius orci et, venenatis lectus. Nullam posuere vel lacus nec fermentum. Integer et rhoncus turpis. Cras blandit libero nisi, nec volutpat libero eleifend ac. Mauris lacinia lacus in convallis interdum. Donec laoreet magna fermentum, mattis orci sit amet, rutrum mauris.</p>\r\n\r\n<p>Nulla diam massa, pharetra vel vestibulum sed, egestas ut augue. Etiam tempor bibendum sagittis. Aliquam erat volutpat. Maecenas et eros et risus ullamcorper viverra ut eu dui. Mauris semper ut dui non ultrices. Integer a lectus a metus pellentesque congue eget ac orci. Donec fermentum mi quam, at sagittis eros rhoncus sed. Duis non laoreet odio. Nullam vulputate vehicula justo, eget blandit lacus feugiat sit amet.</p>\r\n\r\n<p>Integer ac quam vitae odio fringilla mattis vel sed ante. Etiam eu magna eleifend, mattis leo vel, porttitor nisl. Vestibulum ornare turpis at erat imperdiet, ac convallis felis lacinia. Nullam ultrices diam eu lorem ultrices, dapibus viverra felis ornare. Nam semper adipiscing urna non consequat. Morbi tempor laoreet felis, a pulvinar nunc commodo vel. Donec nec nulla ac turpis scelerisque interdum. Etiam tempor sem nisi. Sed lacinia id tellus sodales commodo. Quisque porttitor lobortis aliquam.</p>\r\n\r\n<p>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Ut id ultricies leo. Sed rhoncus, justo non convallis auctor, est augue condimentum lacus, ut porttitor leo tortor sit amet urna. Praesent ut ultrices nisi. Nunc tincidunt blandit cursus. Suspendisse potenti. Curabitur mollis metus nec odio vestibulum, sit amet iaculis sapien iaculis. Mauris non arcu at quam venenatis gravida in a ipsum. Vestibulum aliquet libero ut diam mattis fermentum. Donec dignissim dictum vulputate. Quisque commodo magna aliquet neque pretium dictum.</p>\r\n\r\n<p>Cras turpis mi, porta sed dignissim nec, posuere in velit. Duis eget magna at nisi auctor mattis. Donec eget pharetra velit, ac gravida neque. Integer fringilla turpis sed ipsum elementum facilisis. Morbi accumsan mi ut ligula vestibulum, sit amet cursus ligula imperdiet. Duis a euismod elit, non facilisis tortor. Mauris id ligula tellus. Sed ligula tellus, pharetra vel nisl a, placerat iaculis libero. Etiam pellentesque mauris libero, non dapibus nibh lobortis nec. Nam fermentum, urna non luctus semper, tellus leo rutrum risus, a tempor purus ligula non justo. Vivamus aliquam arcu a porttitor ultricies. Nunc sodales vel risus non mattis. Fusce id nisl dapibus, ornare dui at, vestibulum arcu.</p>', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras hendrerit.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras odio arcu, elementum sed aliquet non, consectetur non neque. Curabitur tempor pretium velit id consequat. Nunc felis dolor, eleifend ut pretium at, euismod id nunc.', '', 'lorem-ipsum-dolor-sit-amet-consectetur-adipiscing-elit-cras-hendrerit'),
(851, 'es', 'Pebble time steel is on track to ship in july', '295', '', '', 'Pebble time steel is on track to ship in july', '', '', 'pebble-time-steel-is-on-track-to-ship-in-july'),
(834, 'en', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras hendrerit.', '295', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras odio arcu, elementum sed aliquet non, consectetur non neque. Curabitur tempor pretium velit id consequat. Nunc felis dolor, eleifend ut pretium at, euismod id nunc.', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc aliquet molestie tortor ut luctus. Proin id hendrerit tortor, et vehicula risus. Aliquam adipiscing magna iaculis nunc malesuada facilisis. Aliquam justo magna, accumsan sed malesuada eu, pretium sit amet arcu. Ut lobortis eu augue sed fringilla. Curabitur vestibulum est sed sollicitudin venenatis. Etiam vitae commodo ligula. Donec lacinia elit non tellus convallis porta. Vivamus eu nunc ac urna vestibulum laoreet. Ut tincidunt imperdiet lobortis. Donec bibendum nunc sed lorem adipiscing vehicula.</p>\r\n\r\n<p>Etiam condimentum elit non interdum tincidunt. Nullam sagittis, sapien sit amet iaculis ullamcorper, odio lorem accumsan leo, ac eleifend mauris libero vel nibh. Aliquam aliquam libero quis nisl mattis ornare. Cras dictum vitae mi eu blandit. Maecenas ac aliquam sem. Morbi dictum felis eu augue blandit molestie. Integer aliquam vitae enim ultricies vehicula. Duis suscipit, augue sed molestie vehicula, turpis mi rutrum arcu, at pulvinar sapien est at massa. Ut interdum purus at turpis pellentesque ultricies. Aenean luctus dapibus elit non pellentesque. Praesent posuere nisi a nibh suscipit, at sagittis mauris faucibus. Vestibulum vitae erat eros.</p>\r\n\r\n<p>Fusce tristique justo vel iaculis condimentum. Sed vitae arcu et risus commodo ornare. Fusce varius sapien laoreet, tincidunt leo non, dignissim nisl. Morbi ut luctus risus. Mauris laoreet in tellus sed luctus. Fusce eu augue eu mi sollicitudin ullamcorper. Cras consequat dui eu leo mattis varius.</p>\r\n\r\n<p>Praesent quam lorem, pharetra ac elementum quis, lobortis vel nunc. Nulla et cursus dolor, sit amet aliquam purus. Donec id sapien pulvinar, varius orci et, venenatis lectus. Nullam posuere vel lacus nec fermentum. Integer et rhoncus turpis. Cras blandit libero nisi, nec volutpat libero eleifend ac. Mauris lacinia lacus in convallis interdum. Donec laoreet magna fermentum, mattis orci sit amet, rutrum mauris.</p>\r\n\r\n<p>Nulla diam massa, pharetra vel vestibulum sed, egestas ut augue. Etiam tempor bibendum sagittis. Aliquam erat volutpat. Maecenas et eros et risus ullamcorper viverra ut eu dui. Mauris semper ut dui non ultrices. Integer a lectus a metus pellentesque congue eget ac orci. Donec fermentum mi quam, at sagittis eros rhoncus sed. Duis non laoreet odio. Nullam vulputate vehicula justo, eget blandit lacus feugiat sit amet.</p>\r\n\r\n<p>Integer ac quam vitae odio fringilla mattis vel sed ante. Etiam eu magna eleifend, mattis leo vel, porttitor nisl. Vestibulum ornare turpis at erat imperdiet, ac convallis felis lacinia. Nullam ultrices diam eu lorem ultrices, dapibus viverra felis ornare. Nam semper adipiscing urna non consequat. Morbi tempor laoreet felis, a pulvinar nunc commodo vel. Donec nec nulla ac turpis scelerisque interdum. Etiam tempor sem nisi. Sed lacinia id tellus sodales commodo. Quisque porttitor lobortis aliquam.</p>\r\n\r\n<p>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Ut id ultricies leo. Sed rhoncus, justo non convallis auctor, est augue condimentum lacus, ut porttitor leo tortor sit amet urna. Praesent ut ultrices nisi. Nunc tincidunt blandit cursus. Suspendisse potenti. Curabitur mollis metus nec odio vestibulum, sit amet iaculis sapien iaculis. Mauris non arcu at quam venenatis gravida in a ipsum. Vestibulum aliquet libero ut diam mattis fermentum. Donec dignissim dictum vulputate. Quisque commodo magna aliquet neque pretium dictum.</p>\r\n\r\n<p>Cras turpis mi, porta sed dignissim nec, posuere in velit. Duis eget magna at nisi auctor mattis. Donec eget pharetra velit, ac gravida neque. Integer fringilla turpis sed ipsum elementum facilisis. Morbi accumsan mi ut ligula vestibulum, sit amet cursus ligula imperdiet. Duis a euismod elit, non facilisis tortor. Mauris id ligula tellus. Sed ligula tellus, pharetra vel nisl a, placerat iaculis libero. Etiam pellentesque mauris libero, non dapibus nibh lobortis nec. Nam fermentum, urna non luctus semper, tellus leo rutrum risus, a tempor purus ligula non justo. Vivamus aliquam arcu a porttitor ultricies. Nunc sodales vel risus non mattis. Fusce id nisl dapibus, ornare dui at, vestibulum arcu.</p>', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras hendrerit.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras odio arcu, elementum sed aliquet non, consectetur non neque. Curabitur tempor pretium velit id consequat. Nunc felis dolor, eleifend ut pretium at, euismod id nunc.', '', 'lorem-ipsum-dolor-sit-amet-consectetur-adipiscing-elit-cras-hendrerit3'),
(846, 'en', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras hendrerit.', '281', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras odio arcu, elementum sed aliquet non, consectetur non neque. Curabitur tempor pretium velit id consequat. Nunc felis dolor, eleifend ut pretium at, euismod id nunc.', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc aliquet molestie tortor ut luctus. Proin id hendrerit tortor, et vehicula risus. Aliquam adipiscing magna iaculis nunc malesuada facilisis. Aliquam justo magna, accumsan sed malesuada eu, pretium sit amet arcu. Ut lobortis eu augue sed fringilla. Curabitur vestibulum est sed sollicitudin venenatis. Etiam vitae commodo ligula. Donec lacinia elit non tellus convallis porta. Vivamus eu nunc ac urna vestibulum laoreet. Ut tincidunt imperdiet lobortis. Donec bibendum nunc sed lorem adipiscing vehicula.</p>\r\n\r\n<p>Etiam condimentum elit non interdum tincidunt. Nullam sagittis, sapien sit amet iaculis ullamcorper, odio lorem accumsan leo, ac eleifend mauris libero vel nibh. Aliquam aliquam libero quis nisl mattis ornare. Cras dictum vitae mi eu blandit. Maecenas ac aliquam sem. Morbi dictum felis eu augue blandit molestie. Integer aliquam vitae enim ultricies vehicula. Duis suscipit, augue sed molestie vehicula, turpis mi rutrum arcu, at pulvinar sapien est at massa. Ut interdum purus at turpis pellentesque ultricies. Aenean luctus dapibus elit non pellentesque. Praesent posuere nisi a nibh suscipit, at sagittis mauris faucibus. Vestibulum vitae erat eros.</p>\r\n\r\n<p>Fusce tristique justo vel iaculis condimentum. Sed vitae arcu et risus commodo ornare. Fusce varius sapien laoreet, tincidunt leo non, dignissim nisl. Morbi ut luctus risus. Mauris laoreet in tellus sed luctus. Fusce eu augue eu mi sollicitudin ullamcorper. Cras consequat dui eu leo mattis varius.</p>\r\n\r\n<p>Praesent quam lorem, pharetra ac elementum quis, lobortis vel nunc. Nulla et cursus dolor, sit amet aliquam purus. Donec id sapien pulvinar, varius orci et, venenatis lectus. Nullam posuere vel lacus nec fermentum. Integer et rhoncus turpis. Cras blandit libero nisi, nec volutpat libero eleifend ac. Mauris lacinia lacus in convallis interdum. Donec laoreet magna fermentum, mattis orci sit amet, rutrum mauris.</p>\r\n\r\n<p>Nulla diam massa, pharetra vel vestibulum sed, egestas ut augue. Etiam tempor bibendum sagittis. Aliquam erat volutpat. Maecenas et eros et risus ullamcorper viverra ut eu dui. Mauris semper ut dui non ultrices. Integer a lectus a metus pellentesque congue eget ac orci. Donec fermentum mi quam, at sagittis eros rhoncus sed. Duis non laoreet odio. Nullam vulputate vehicula justo, eget blandit lacus feugiat sit amet.</p>\r\n\r\n<p>Integer ac quam vitae odio fringilla mattis vel sed ante. Etiam eu magna eleifend, mattis leo vel, porttitor nisl. Vestibulum ornare turpis at erat imperdiet, ac convallis felis lacinia. Nullam ultrices diam eu lorem ultrices, dapibus viverra felis ornare. Nam semper adipiscing urna non consequat. Morbi tempor laoreet felis, a pulvinar nunc commodo vel. Donec nec nulla ac turpis scelerisque interdum. Etiam tempor sem nisi. Sed lacinia id tellus sodales commodo. Quisque porttitor lobortis aliquam.</p>\r\n\r\n<p>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Ut id ultricies leo. Sed rhoncus, justo non convallis auctor, est augue condimentum lacus, ut porttitor leo tortor sit amet urna. Praesent ut ultrices nisi. Nunc tincidunt blandit cursus. Suspendisse potenti. Curabitur mollis metus nec odio vestibulum, sit amet iaculis sapien iaculis. Mauris non arcu at quam venenatis gravida in a ipsum. Vestibulum aliquet libero ut diam mattis fermentum. Donec dignissim dictum vulputate. Quisque commodo magna aliquet neque pretium dictum.</p>\r\n\r\n<p>Cras turpis mi, porta sed dignissim nec, posuere in velit. Duis eget magna at nisi auctor mattis. Donec eget pharetra velit, ac gravida neque. Integer fringilla turpis sed ipsum elementum facilisis. Morbi accumsan mi ut ligula vestibulum, sit amet cursus ligula imperdiet. Duis a euismod elit, non facilisis tortor. Mauris id ligula tellus. Sed ligula tellus, pharetra vel nisl a, placerat iaculis libero. Etiam pellentesque mauris libero, non dapibus nibh lobortis nec. Nam fermentum, urna non luctus semper, tellus leo rutrum risus, a tempor purus ligula non justo. Vivamus aliquam arcu a porttitor ultricies. Nunc sodales vel risus non mattis. Fusce id nisl dapibus, ornare dui at, vestibulum arcu.</p>', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras hendrerit.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras odio arcu, elementum sed aliquet non, consectetur non neque. Curabitur tempor pretium velit id consequat. Nunc felis dolor, eleifend ut pretium at, euismod id nunc.', '', 'lorem-ipsum-dolor-sit-amet-consectetur-adipiscing-elit-cras-hendrerit12'),
(847, 'es', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras hendrerit.', '287', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras odio arcu, elementum sed aliquet non, consectetur non neque. Curabitur tempor pretium velit id consequat. Nunc felis dolor, eleifend ut pretium at, euismod id nunc.', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc aliquet molestie tortor ut luctus. Proin id hendrerit tortor, et vehicula risus. Aliquam adipiscing magna iaculis nunc malesuada facilisis. Aliquam justo magna, accumsan sed malesuada eu, pretium sit amet arcu. Ut lobortis eu augue sed fringilla. Curabitur vestibulum est sed sollicitudin venenatis. Etiam vitae commodo ligula. Donec lacinia elit non tellus convallis porta. Vivamus eu nunc ac urna vestibulum laoreet. Ut tincidunt imperdiet lobortis. Donec bibendum nunc sed lorem adipiscing vehicula.</p>\r\n\r\n<p>Etiam condimentum elit non interdum tincidunt. Nullam sagittis, sapien sit amet iaculis ullamcorper, odio lorem accumsan leo, ac eleifend mauris libero vel nibh. Aliquam aliquam libero quis nisl mattis ornare. Cras dictum vitae mi eu blandit. Maecenas ac aliquam sem. Morbi dictum felis eu augue blandit molestie. Integer aliquam vitae enim ultricies vehicula. Duis suscipit, augue sed molestie vehicula, turpis mi rutrum arcu, at pulvinar sapien est at massa. Ut interdum purus at turpis pellentesque ultricies. Aenean luctus dapibus elit non pellentesque. Praesent posuere nisi a nibh suscipit, at sagittis mauris faucibus. Vestibulum vitae erat eros.</p>\r\n\r\n<p>Fusce tristique justo vel iaculis condimentum. Sed vitae arcu et risus commodo ornare. Fusce varius sapien laoreet, tincidunt leo non, dignissim nisl. Morbi ut luctus risus. Mauris laoreet in tellus sed luctus. Fusce eu augue eu mi sollicitudin ullamcorper. Cras consequat dui eu leo mattis varius.</p>\r\n\r\n<p>Praesent quam lorem, pharetra ac elementum quis, lobortis vel nunc. Nulla et cursus dolor, sit amet aliquam purus. Donec id sapien pulvinar, varius orci et, venenatis lectus. Nullam posuere vel lacus nec fermentum. Integer et rhoncus turpis. Cras blandit libero nisi, nec volutpat libero eleifend ac. Mauris lacinia lacus in convallis interdum. Donec laoreet magna fermentum, mattis orci sit amet, rutrum mauris.</p>\r\n\r\n<p>Nulla diam massa, pharetra vel vestibulum sed, egestas ut augue. Etiam tempor bibendum sagittis. Aliquam erat volutpat. Maecenas et eros et risus ullamcorper viverra ut eu dui. Mauris semper ut dui non ultrices. Integer a lectus a metus pellentesque congue eget ac orci. Donec fermentum mi quam, at sagittis eros rhoncus sed. Duis non laoreet odio. Nullam vulputate vehicula justo, eget blandit lacus feugiat sit amet.</p>\r\n\r\n<p>Integer ac quam vitae odio fringilla mattis vel sed ante. Etiam eu magna eleifend, mattis leo vel, porttitor nisl. Vestibulum ornare turpis at erat imperdiet, ac convallis felis lacinia. Nullam ultrices diam eu lorem ultrices, dapibus viverra felis ornare. Nam semper adipiscing urna non consequat. Morbi tempor laoreet felis, a pulvinar nunc commodo vel. Donec nec nulla ac turpis scelerisque interdum. Etiam tempor sem nisi. Sed lacinia id tellus sodales commodo. Quisque porttitor lobortis aliquam.</p>\r\n\r\n<p>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Ut id ultricies leo. Sed rhoncus, justo non convallis auctor, est augue condimentum lacus, ut porttitor leo tortor sit amet urna. Praesent ut ultrices nisi. Nunc tincidunt blandit cursus. Suspendisse potenti. Curabitur mollis metus nec odio vestibulum, sit amet iaculis sapien iaculis. Mauris non arcu at quam venenatis gravida in a ipsum. Vestibulum aliquet libero ut diam mattis fermentum. Donec dignissim dictum vulputate. Quisque commodo magna aliquet neque pretium dictum.</p>\r\n\r\n<p>Cras turpis mi, porta sed dignissim nec, posuere in velit. Duis eget magna at nisi auctor mattis. Donec eget pharetra velit, ac gravida neque. Integer fringilla turpis sed ipsum elementum facilisis. Morbi accumsan mi ut ligula vestibulum, sit amet cursus ligula imperdiet. Duis a euismod elit, non facilisis tortor. Mauris id ligula tellus. Sed ligula tellus, pharetra vel nisl a, placerat iaculis libero. Etiam pellentesque mauris libero, non dapibus nibh lobortis nec. Nam fermentum, urna non luctus semper, tellus leo rutrum risus, a tempor purus ligula non justo. Vivamus aliquam arcu a porttitor ultricies. Nunc sodales vel risus non mattis. Fusce id nisl dapibus, ornare dui at, vestibulum arcu.</p>', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras hendrerit.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras odio arcu, elementum sed aliquet non, consectetur non neque. Curabitur tempor pretium velit id consequat. Nunc felis dolor, eleifend ut pretium at, euismod id nunc.', '', 'lorem-ipsum-dolor-sit-amet-consectetur-adipiscing-elit-cras-hendrerit6'),
(838, 'en', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras hendrerit.', '309', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras odio arcu, elementum sed aliquet non, consectetur non neque. Curabitur tempor pretium velit id consequat. Nunc felis dolor, eleifend ut pretium at, euismod id nunc.', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc aliquet molestie tortor ut luctus. Proin id hendrerit tortor, et vehicula risus. Aliquam adipiscing magna iaculis nunc malesuada facilisis. Aliquam justo magna, accumsan sed malesuada eu, pretium sit amet arcu. Ut lobortis eu augue sed fringilla. Curabitur vestibulum est sed sollicitudin venenatis. Etiam vitae commodo ligula. Donec lacinia elit non tellus convallis porta. Vivamus eu nunc ac urna vestibulum laoreet. Ut tincidunt imperdiet lobortis. Donec bibendum nunc sed lorem adipiscing vehicula.</p>\r\n\r\n<p>Etiam condimentum elit non interdum tincidunt. Nullam sagittis, sapien sit amet iaculis ullamcorper, odio lorem accumsan leo, ac eleifend mauris libero vel nibh. Aliquam aliquam libero quis nisl mattis ornare. Cras dictum vitae mi eu blandit. Maecenas ac aliquam sem. Morbi dictum felis eu augue blandit molestie. Integer aliquam vitae enim ultricies vehicula. Duis suscipit, augue sed molestie vehicula, turpis mi rutrum arcu, at pulvinar sapien est at massa. Ut interdum purus at turpis pellentesque ultricies. Aenean luctus dapibus elit non pellentesque. Praesent posuere nisi a nibh suscipit, at sagittis mauris faucibus. Vestibulum vitae erat eros.</p>\r\n\r\n<p>Fusce tristique justo vel iaculis condimentum. Sed vitae arcu et risus commodo ornare. Fusce varius sapien laoreet, tincidunt leo non, dignissim nisl. Morbi ut luctus risus. Mauris laoreet in tellus sed luctus. Fusce eu augue eu mi sollicitudin ullamcorper. Cras consequat dui eu leo mattis varius.</p>\r\n\r\n<p>Praesent quam lorem, pharetra ac elementum quis, lobortis vel nunc. Nulla et cursus dolor, sit amet aliquam purus. Donec id sapien pulvinar, varius orci et, venenatis lectus. Nullam posuere vel lacus nec fermentum. Integer et rhoncus turpis. Cras blandit libero nisi, nec volutpat libero eleifend ac. Mauris lacinia lacus in convallis interdum. Donec laoreet magna fermentum, mattis orci sit amet, rutrum mauris.</p>\r\n\r\n<p>Nulla diam massa, pharetra vel vestibulum sed, egestas ut augue. Etiam tempor bibendum sagittis. Aliquam erat volutpat. Maecenas et eros et risus ullamcorper viverra ut eu dui. Mauris semper ut dui non ultrices. Integer a lectus a metus pellentesque congue eget ac orci. Donec fermentum mi quam, at sagittis eros rhoncus sed. Duis non laoreet odio. Nullam vulputate vehicula justo, eget blandit lacus feugiat sit amet.</p>\r\n\r\n<p>Integer ac quam vitae odio fringilla mattis vel sed ante. Etiam eu magna eleifend, mattis leo vel, porttitor nisl. Vestibulum ornare turpis at erat imperdiet, ac convallis felis lacinia. Nullam ultrices diam eu lorem ultrices, dapibus viverra felis ornare. Nam semper adipiscing urna non consequat. Morbi tempor laoreet felis, a pulvinar nunc commodo vel. Donec nec nulla ac turpis scelerisque interdum. Etiam tempor sem nisi. Sed lacinia id tellus sodales commodo. Quisque porttitor lobortis aliquam.</p>\r\n\r\n<p>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Ut id ultricies leo. Sed rhoncus, justo non convallis auctor, est augue condimentum lacus, ut porttitor leo tortor sit amet urna. Praesent ut ultrices nisi. Nunc tincidunt blandit cursus. Suspendisse potenti. Curabitur mollis metus nec odio vestibulum, sit amet iaculis sapien iaculis. Mauris non arcu at quam venenatis gravida in a ipsum. Vestibulum aliquet libero ut diam mattis fermentum. Donec dignissim dictum vulputate. Quisque commodo magna aliquet neque pretium dictum.</p>\r\n\r\n<p>Cras turpis mi, porta sed dignissim nec, posuere in velit. Duis eget magna at nisi auctor mattis. Donec eget pharetra velit, ac gravida neque. Integer fringilla turpis sed ipsum elementum facilisis. Morbi accumsan mi ut ligula vestibulum, sit amet cursus ligula imperdiet. Duis a euismod elit, non facilisis tortor. Mauris id ligula tellus. Sed ligula tellus, pharetra vel nisl a, placerat iaculis libero. Etiam pellentesque mauris libero, non dapibus nibh lobortis nec. Nam fermentum, urna non luctus semper, tellus leo rutrum risus, a tempor purus ligula non justo. Vivamus aliquam arcu a porttitor ultricies. Nunc sodales vel risus non mattis. Fusce id nisl dapibus, ornare dui at, vestibulum arcu.</p>\r\n\r\n<p>&nbsp;</p>', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras hendrerit.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras odio arcu, elementum sed aliquet non, consectetur non neque. Curabitur tempor pretium velit id consequat. Nunc felis dolor, eleifend ut pretium at, euismod id nunc.', '', 'lorem-ipsum-dolor-sit-amet-consectetur-adipiscing-elit-cras-hendrerit6');
INSERT INTO `s_newslocalizations` (`newsid`, `lang`, `name`, `header`, `comment`, `text`, `seo_header`, `seo_description`, `seo_keywords`, `slug`) VALUES
(847, 'en', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras hendrerit.', '287', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras odio arcu, elementum sed aliquet non, consectetur non neque. Curabitur tempor pretium velit id consequat. Nunc felis dolor, eleifend ut pretium at, euismod id nunc.', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc aliquet molestie tortor ut luctus. Proin id hendrerit tortor, et vehicula risus. Aliquam adipiscing magna iaculis nunc malesuada facilisis. Aliquam justo magna, accumsan sed malesuada eu, pretium sit amet arcu. Ut lobortis eu augue sed fringilla. Curabitur vestibulum est sed sollicitudin venenatis. Etiam vitae commodo ligula. Donec lacinia elit non tellus convallis porta. Vivamus eu nunc ac urna vestibulum laoreet. Ut tincidunt imperdiet lobortis. Donec bibendum nunc sed lorem adipiscing vehicula.</p>\r\n\r\n<p>Etiam condimentum elit non interdum tincidunt. Nullam sagittis, sapien sit amet iaculis ullamcorper, odio lorem accumsan leo, ac eleifend mauris libero vel nibh. Aliquam aliquam libero quis nisl mattis ornare. Cras dictum vitae mi eu blandit. Maecenas ac aliquam sem. Morbi dictum felis eu augue blandit molestie. Integer aliquam vitae enim ultricies vehicula. Duis suscipit, augue sed molestie vehicula, turpis mi rutrum arcu, at pulvinar sapien est at massa. Ut interdum purus at turpis pellentesque ultricies. Aenean luctus dapibus elit non pellentesque. Praesent posuere nisi a nibh suscipit, at sagittis mauris faucibus. Vestibulum vitae erat eros.</p>\r\n\r\n<p>Fusce tristique justo vel iaculis condimentum. Sed vitae arcu et risus commodo ornare. Fusce varius sapien laoreet, tincidunt leo non, dignissim nisl. Morbi ut luctus risus. Mauris laoreet in tellus sed luctus. Fusce eu augue eu mi sollicitudin ullamcorper. Cras consequat dui eu leo mattis varius.</p>\r\n\r\n<p>Praesent quam lorem, pharetra ac elementum quis, lobortis vel nunc. Nulla et cursus dolor, sit amet aliquam purus. Donec id sapien pulvinar, varius orci et, venenatis lectus. Nullam posuere vel lacus nec fermentum. Integer et rhoncus turpis. Cras blandit libero nisi, nec volutpat libero eleifend ac. Mauris lacinia lacus in convallis interdum. Donec laoreet magna fermentum, mattis orci sit amet, rutrum mauris.</p>\r\n\r\n<p>Nulla diam massa, pharetra vel vestibulum sed, egestas ut augue. Etiam tempor bibendum sagittis. Aliquam erat volutpat. Maecenas et eros et risus ullamcorper viverra ut eu dui. Mauris semper ut dui non ultrices. Integer a lectus a metus pellentesque congue eget ac orci. Donec fermentum mi quam, at sagittis eros rhoncus sed. Duis non laoreet odio. Nullam vulputate vehicula justo, eget blandit lacus feugiat sit amet.</p>\r\n\r\n<p>Integer ac quam vitae odio fringilla mattis vel sed ante. Etiam eu magna eleifend, mattis leo vel, porttitor nisl. Vestibulum ornare turpis at erat imperdiet, ac convallis felis lacinia. Nullam ultrices diam eu lorem ultrices, dapibus viverra felis ornare. Nam semper adipiscing urna non consequat. Morbi tempor laoreet felis, a pulvinar nunc commodo vel. Donec nec nulla ac turpis scelerisque interdum. Etiam tempor sem nisi. Sed lacinia id tellus sodales commodo. Quisque porttitor lobortis aliquam.</p>\r\n\r\n<p>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Ut id ultricies leo. Sed rhoncus, justo non convallis auctor, est augue condimentum lacus, ut porttitor leo tortor sit amet urna. Praesent ut ultrices nisi. Nunc tincidunt blandit cursus. Suspendisse potenti. Curabitur mollis metus nec odio vestibulum, sit amet iaculis sapien iaculis. Mauris non arcu at quam venenatis gravida in a ipsum. Vestibulum aliquet libero ut diam mattis fermentum. Donec dignissim dictum vulputate. Quisque commodo magna aliquet neque pretium dictum.</p>\r\n\r\n<p>Cras turpis mi, porta sed dignissim nec, posuere in velit. Duis eget magna at nisi auctor mattis. Donec eget pharetra velit, ac gravida neque. Integer fringilla turpis sed ipsum elementum facilisis. Morbi accumsan mi ut ligula vestibulum, sit amet cursus ligula imperdiet. Duis a euismod elit, non facilisis tortor. Mauris id ligula tellus. Sed ligula tellus, pharetra vel nisl a, placerat iaculis libero. Etiam pellentesque mauris libero, non dapibus nibh lobortis nec. Nam fermentum, urna non luctus semper, tellus leo rutrum risus, a tempor purus ligula non justo. Vivamus aliquam arcu a porttitor ultricies. Nunc sodales vel risus non mattis. Fusce id nisl dapibus, ornare dui at, vestibulum arcu.</p>', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras hendrerit.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras odio arcu, elementum sed aliquet non, consectetur non neque. Curabitur tempor pretium velit id consequat. Nunc felis dolor, eleifend ut pretium at, euismod id nunc.', '', 'lorem-ipsum-dolor-sit-amet-consectetur-adipiscing-elit-cras-hendrerit13'),
(835, 'es', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras hendrerit.', '284', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras odio arcu, elementum sed aliquet non, consectetur non neque. Curabitur tempor pretium velit id consequat. Nunc felis dolor, eleifend ut pretium at, euismod id nunc.', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc aliquet molestie tortor ut luctus. Proin id hendrerit tortor, et vehicula risus. Aliquam adipiscing magna iaculis nunc malesuada facilisis. Aliquam justo magna, accumsan sed malesuada eu, pretium sit amet arcu. Ut lobortis eu augue sed fringilla. Curabitur vestibulum est sed sollicitudin venenatis. Etiam vitae commodo ligula. Donec lacinia elit non tellus convallis porta. Vivamus eu nunc ac urna vestibulum laoreet. Ut tincidunt imperdiet lobortis. Donec bibendum nunc sed lorem adipiscing vehicula.</p>\r\n\r\n<p>Etiam condimentum elit non interdum tincidunt. Nullam sagittis, sapien sit amet iaculis ullamcorper, odio lorem accumsan leo, ac eleifend mauris libero vel nibh. Aliquam aliquam libero quis nisl mattis ornare. Cras dictum vitae mi eu blandit. Maecenas ac aliquam sem. Morbi dictum felis eu augue blandit molestie. Integer aliquam vitae enim ultricies vehicula. Duis suscipit, augue sed molestie vehicula, turpis mi rutrum arcu, at pulvinar sapien est at massa. Ut interdum purus at turpis pellentesque ultricies. Aenean luctus dapibus elit non pellentesque. Praesent posuere nisi a nibh suscipit, at sagittis mauris faucibus. Vestibulum vitae erat eros.</p>\r\n\r\n<p>Fusce tristique justo vel iaculis condimentum. Sed vitae arcu et risus commodo ornare. Fusce varius sapien laoreet, tincidunt leo non, dignissim nisl. Morbi ut luctus risus. Mauris laoreet in tellus sed luctus. Fusce eu augue eu mi sollicitudin ullamcorper. Cras consequat dui eu leo mattis varius.</p>\r\n\r\n<p>Praesent quam lorem, pharetra ac elementum quis, lobortis vel nunc. Nulla et cursus dolor, sit amet aliquam purus. Donec id sapien pulvinar, varius orci et, venenatis lectus. Nullam posuere vel lacus nec fermentum. Integer et rhoncus turpis. Cras blandit libero nisi, nec volutpat libero eleifend ac. Mauris lacinia lacus in convallis interdum. Donec laoreet magna fermentum, mattis orci sit amet, rutrum mauris.</p>\r\n\r\n<p>Nulla diam massa, pharetra vel vestibulum sed, egestas ut augue. Etiam tempor bibendum sagittis. Aliquam erat volutpat. Maecenas et eros et risus ullamcorper viverra ut eu dui. Mauris semper ut dui non ultrices. Integer a lectus a metus pellentesque congue eget ac orci. Donec fermentum mi quam, at sagittis eros rhoncus sed. Duis non laoreet odio. Nullam vulputate vehicula justo, eget blandit lacus feugiat sit amet.</p>\r\n\r\n<p>Integer ac quam vitae odio fringilla mattis vel sed ante. Etiam eu magna eleifend, mattis leo vel, porttitor nisl. Vestibulum ornare turpis at erat imperdiet, ac convallis felis lacinia. Nullam ultrices diam eu lorem ultrices, dapibus viverra felis ornare. Nam semper adipiscing urna non consequat. Morbi tempor laoreet felis, a pulvinar nunc commodo vel. Donec nec nulla ac turpis scelerisque interdum. Etiam tempor sem nisi. Sed lacinia id tellus sodales commodo. Quisque porttitor lobortis aliquam.</p>\r\n\r\n<p>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Ut id ultricies leo. Sed rhoncus, justo non convallis auctor, est augue condimentum lacus, ut porttitor leo tortor sit amet urna. Praesent ut ultrices nisi. Nunc tincidunt blandit cursus. Suspendisse potenti. Curabitur mollis metus nec odio vestibulum, sit amet iaculis sapien iaculis. Mauris non arcu at quam venenatis gravida in a ipsum. Vestibulum aliquet libero ut diam mattis fermentum. Donec dignissim dictum vulputate. Quisque commodo magna aliquet neque pretium dictum.</p>\r\n\r\n<p>Cras turpis mi, porta sed dignissim nec, posuere in velit. Duis eget magna at nisi auctor mattis. Donec eget pharetra velit, ac gravida neque. Integer fringilla turpis sed ipsum elementum facilisis. Morbi accumsan mi ut ligula vestibulum, sit amet cursus ligula imperdiet. Duis a euismod elit, non facilisis tortor. Mauris id ligula tellus. Sed ligula tellus, pharetra vel nisl a, placerat iaculis libero. Etiam pellentesque mauris libero, non dapibus nibh lobortis nec. Nam fermentum, urna non luctus semper, tellus leo rutrum risus, a tempor purus ligula non justo. Vivamus aliquam arcu a porttitor ultricies. Nunc sodales vel risus non mattis. Fusce id nisl dapibus, ornare dui at, vestibulum arcu.</p>', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras hendrerit.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras odio arcu, elementum sed aliquet non, consectetur non neque. Curabitur tempor pretium velit id consequat. Nunc felis dolor, eleifend ut pretium at, euismod id nunc.', '', 'lorem-ipsum-dolor-sit-amet-consectetur-adipiscing-elit-cras-hendrerit4'),
(850, 'es', '5 places that make a great holiday', '283', '', '', '5 places that make a great holiday', '', '', '5-places-that-make-a-great-holiday'),
(839, 'es', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras hendrerit.', '287', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras odio arcu, elementum sed aliquet non, consectetur non neque. Curabitur tempor pretium velit id consequat. Nunc felis dolor, eleifend ut pretium at, euismod id nunc.', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc aliquet molestie tortor ut luctus. Proin id hendrerit tortor, et vehicula risus. Aliquam adipiscing magna iaculis nunc malesuada facilisis. Aliquam justo magna, accumsan sed malesuada eu, pretium sit amet arcu. Ut lobortis eu augue sed fringilla. Curabitur vestibulum est sed sollicitudin venenatis. Etiam vitae commodo ligula. Donec lacinia elit non tellus convallis porta. Vivamus eu nunc ac urna vestibulum laoreet. Ut tincidunt imperdiet lobortis. Donec bibendum nunc sed lorem adipiscing vehicula.</p>\r\n\r\n<p>Etiam condimentum elit non interdum tincidunt. Nullam sagittis, sapien sit amet iaculis ullamcorper, odio lorem accumsan leo, ac eleifend mauris libero vel nibh. Aliquam aliquam libero quis nisl mattis ornare. Cras dictum vitae mi eu blandit. Maecenas ac aliquam sem. Morbi dictum felis eu augue blandit molestie. Integer aliquam vitae enim ultricies vehicula. Duis suscipit, augue sed molestie vehicula, turpis mi rutrum arcu, at pulvinar sapien est at massa. Ut interdum purus at turpis pellentesque ultricies. Aenean luctus dapibus elit non pellentesque. Praesent posuere nisi a nibh suscipit, at sagittis mauris faucibus. Vestibulum vitae erat eros.</p>\r\n\r\n<p>Fusce tristique justo vel iaculis condimentum. Sed vitae arcu et risus commodo ornare. Fusce varius sapien laoreet, tincidunt leo non, dignissim nisl. Morbi ut luctus risus. Mauris laoreet in tellus sed luctus. Fusce eu augue eu mi sollicitudin ullamcorper. Cras consequat dui eu leo mattis varius.</p>\r\n\r\n<p>Praesent quam lorem, pharetra ac elementum quis, lobortis vel nunc. Nulla et cursus dolor, sit amet aliquam purus. Donec id sapien pulvinar, varius orci et, venenatis lectus. Nullam posuere vel lacus nec fermentum. Integer et rhoncus turpis. Cras blandit libero nisi, nec volutpat libero eleifend ac. Mauris lacinia lacus in convallis interdum. Donec laoreet magna fermentum, mattis orci sit amet, rutrum mauris.</p>\r\n\r\n<p>Nulla diam massa, pharetra vel vestibulum sed, egestas ut augue. Etiam tempor bibendum sagittis. Aliquam erat volutpat. Maecenas et eros et risus ullamcorper viverra ut eu dui. Mauris semper ut dui non ultrices. Integer a lectus a metus pellentesque congue eget ac orci. Donec fermentum mi quam, at sagittis eros rhoncus sed. Duis non laoreet odio. Nullam vulputate vehicula justo, eget blandit lacus feugiat sit amet.</p>\r\n\r\n<p>Integer ac quam vitae odio fringilla mattis vel sed ante. Etiam eu magna eleifend, mattis leo vel, porttitor nisl. Vestibulum ornare turpis at erat imperdiet, ac convallis felis lacinia. Nullam ultrices diam eu lorem ultrices, dapibus viverra felis ornare. Nam semper adipiscing urna non consequat. Morbi tempor laoreet felis, a pulvinar nunc commodo vel. Donec nec nulla ac turpis scelerisque interdum. Etiam tempor sem nisi. Sed lacinia id tellus sodales commodo. Quisque porttitor lobortis aliquam.</p>\r\n\r\n<p>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Ut id ultricies leo. Sed rhoncus, justo non convallis auctor, est augue condimentum lacus, ut porttitor leo tortor sit amet urna. Praesent ut ultrices nisi. Nunc tincidunt blandit cursus. Suspendisse potenti. Curabitur mollis metus nec odio vestibulum, sit amet iaculis sapien iaculis. Mauris non arcu at quam venenatis gravida in a ipsum. Vestibulum aliquet libero ut diam mattis fermentum. Donec dignissim dictum vulputate. Quisque commodo magna aliquet neque pretium dictum.</p>\r\n\r\n<p>Cras turpis mi, porta sed dignissim nec, posuere in velit. Duis eget magna at nisi auctor mattis. Donec eget pharetra velit, ac gravida neque. Integer fringilla turpis sed ipsum elementum facilisis. Morbi accumsan mi ut ligula vestibulum, sit amet cursus ligula imperdiet. Duis a euismod elit, non facilisis tortor. Mauris id ligula tellus. Sed ligula tellus, pharetra vel nisl a, placerat iaculis libero. Etiam pellentesque mauris libero, non dapibus nibh lobortis nec. Nam fermentum, urna non luctus semper, tellus leo rutrum risus, a tempor purus ligula non justo. Vivamus aliquam arcu a porttitor ultricies. Nunc sodales vel risus non mattis. Fusce id nisl dapibus, ornare dui at, vestibulum arcu.</p>', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras hendrerit.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras odio arcu, elementum sed aliquet non, consectetur non neque. Curabitur tempor pretium velit id consequat. Nunc felis dolor, eleifend ut pretium at, euismod id nunc.', '', 'lorem-ipsum-dolor-sit-amet-consectetur-adipiscing-elit-cras-hendrerit11'),
(843, 'en', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras hendrerit.', '315', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras odio arcu, elementum sed aliquet non, consectetur non neque. Curabitur tempor pretium velit id consequat. Nunc felis dolor, eleifend ut pretium at, euismod id nunc.', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc aliquet molestie tortor ut luctus. Proin id hendrerit tortor, et vehicula risus. Aliquam adipiscing magna iaculis nunc malesuada facilisis. Aliquam justo magna, accumsan sed malesuada eu, pretium sit amet arcu. Ut lobortis eu augue sed fringilla. Curabitur vestibulum est sed sollicitudin venenatis. Etiam vitae commodo ligula. Donec lacinia elit non tellus convallis porta. Vivamus eu nunc ac urna vestibulum laoreet. Ut tincidunt imperdiet lobortis. Donec bibendum nunc sed lorem adipiscing vehicula.</p>\r\n\r\n<p>Etiam condimentum elit non interdum tincidunt. Nullam sagittis, sapien sit amet iaculis ullamcorper, odio lorem accumsan leo, ac eleifend mauris libero vel nibh. Aliquam aliquam libero quis nisl mattis ornare. Cras dictum vitae mi eu blandit. Maecenas ac aliquam sem. Morbi dictum felis eu augue blandit molestie. Integer aliquam vitae enim ultricies vehicula. Duis suscipit, augue sed molestie vehicula, turpis mi rutrum arcu, at pulvinar sapien est at massa. Ut interdum purus at turpis pellentesque ultricies. Aenean luctus dapibus elit non pellentesque. Praesent posuere nisi a nibh suscipit, at sagittis mauris faucibus. Vestibulum vitae erat eros.</p>\r\n\r\n<p>Fusce tristique justo vel iaculis condimentum. Sed vitae arcu et risus commodo ornare. Fusce varius sapien laoreet, tincidunt leo non, dignissim nisl. Morbi ut luctus risus. Mauris laoreet in tellus sed luctus. Fusce eu augue eu mi sollicitudin ullamcorper. Cras consequat dui eu leo mattis varius.</p>\r\n\r\n<p>Praesent quam lorem, pharetra ac elementum quis, lobortis vel nunc. Nulla et cursus dolor, sit amet aliquam purus. Donec id sapien pulvinar, varius orci et, venenatis lectus. Nullam posuere vel lacus nec fermentum. Integer et rhoncus turpis. Cras blandit libero nisi, nec volutpat libero eleifend ac. Mauris lacinia lacus in convallis interdum. Donec laoreet magna fermentum, mattis orci sit amet, rutrum mauris.</p>\r\n\r\n<p>Nulla diam massa, pharetra vel vestibulum sed, egestas ut augue. Etiam tempor bibendum sagittis. Aliquam erat volutpat. Maecenas et eros et risus ullamcorper viverra ut eu dui. Mauris semper ut dui non ultrices. Integer a lectus a metus pellentesque congue eget ac orci. Donec fermentum mi quam, at sagittis eros rhoncus sed. Duis non laoreet odio. Nullam vulputate vehicula justo, eget blandit lacus feugiat sit amet.</p>\r\n\r\n<p>Integer ac quam vitae odio fringilla mattis vel sed ante. Etiam eu magna eleifend, mattis leo vel, porttitor nisl. Vestibulum ornare turpis at erat imperdiet, ac convallis felis lacinia. Nullam ultrices diam eu lorem ultrices, dapibus viverra felis ornare. Nam semper adipiscing urna non consequat. Morbi tempor laoreet felis, a pulvinar nunc commodo vel. Donec nec nulla ac turpis scelerisque interdum. Etiam tempor sem nisi. Sed lacinia id tellus sodales commodo. Quisque porttitor lobortis aliquam.</p>\r\n\r\n<p>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Ut id ultricies leo. Sed rhoncus, justo non convallis auctor, est augue condimentum lacus, ut porttitor leo tortor sit amet urna. Praesent ut ultrices nisi. Nunc tincidunt blandit cursus. Suspendisse potenti. Curabitur mollis metus nec odio vestibulum, sit amet iaculis sapien iaculis. Mauris non arcu at quam venenatis gravida in a ipsum. Vestibulum aliquet libero ut diam mattis fermentum. Donec dignissim dictum vulputate. Quisque commodo magna aliquet neque pretium dictum.</p>\r\n\r\n<p>Cras turpis mi, porta sed dignissim nec, posuere in velit. Duis eget magna at nisi auctor mattis. Donec eget pharetra velit, ac gravida neque. Integer fringilla turpis sed ipsum elementum facilisis. Morbi accumsan mi ut ligula vestibulum, sit amet cursus ligula imperdiet. Duis a euismod elit, non facilisis tortor. Mauris id ligula tellus. Sed ligula tellus, pharetra vel nisl a, placerat iaculis libero. Etiam pellentesque mauris libero, non dapibus nibh lobortis nec. Nam fermentum, urna non luctus semper, tellus leo rutrum risus, a tempor purus ligula non justo. Vivamus aliquam arcu a porttitor ultricies. Nunc sodales vel risus non mattis. Fusce id nisl dapibus, ornare dui at, vestibulum arcu.</p>\r\n\r\n<p>&nbsp;</p>', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras hendrerit.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras odio arcu, elementum sed aliquet non, consectetur non neque. Curabitur tempor pretium velit id consequat. Nunc felis dolor, eleifend ut pretium at, euismod id nunc.', '', 'lorem-ipsum-dolor-sit-amet-consectetur-adipiscing-elit-cras-hendrerit9'),
(841, 'es', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras hendrerit.', '284', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras odio arcu, elementum sed aliquet non, consectetur non neque. Curabitur tempor pretium velit id consequat. Nunc felis dolor, eleifend ut pretium at, euismod id nunc.', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc aliquet molestie tortor ut luctus. Proin id hendrerit tortor, et vehicula risus. Aliquam adipiscing magna iaculis nunc malesuada facilisis. Aliquam justo magna, accumsan sed malesuada eu, pretium sit amet arcu. Ut lobortis eu augue sed fringilla. Curabitur vestibulum est sed sollicitudin venenatis. Etiam vitae commodo ligula. Donec lacinia elit non tellus convallis porta. Vivamus eu nunc ac urna vestibulum laoreet. Ut tincidunt imperdiet lobortis. Donec bibendum nunc sed lorem adipiscing vehicula.</p>\r\n\r\n<p>Etiam condimentum elit non interdum tincidunt. Nullam sagittis, sapien sit amet iaculis ullamcorper, odio lorem accumsan leo, ac eleifend mauris libero vel nibh. Aliquam aliquam libero quis nisl mattis ornare. Cras dictum vitae mi eu blandit. Maecenas ac aliquam sem. Morbi dictum felis eu augue blandit molestie. Integer aliquam vitae enim ultricies vehicula. Duis suscipit, augue sed molestie vehicula, turpis mi rutrum arcu, at pulvinar sapien est at massa. Ut interdum purus at turpis pellentesque ultricies. Aenean luctus dapibus elit non pellentesque. Praesent posuere nisi a nibh suscipit, at sagittis mauris faucibus. Vestibulum vitae erat eros.</p>\r\n\r\n<p>Fusce tristique justo vel iaculis condimentum. Sed vitae arcu et risus commodo ornare. Fusce varius sapien laoreet, tincidunt leo non, dignissim nisl. Morbi ut luctus risus. Mauris laoreet in tellus sed luctus. Fusce eu augue eu mi sollicitudin ullamcorper. Cras consequat dui eu leo mattis varius.</p>\r\n\r\n<p>Praesent quam lorem, pharetra ac elementum quis, lobortis vel nunc. Nulla et cursus dolor, sit amet aliquam purus. Donec id sapien pulvinar, varius orci et, venenatis lectus. Nullam posuere vel lacus nec fermentum. Integer et rhoncus turpis. Cras blandit libero nisi, nec volutpat libero eleifend ac. Mauris lacinia lacus in convallis interdum. Donec laoreet magna fermentum, mattis orci sit amet, rutrum mauris.</p>\r\n\r\n<p>Nulla diam massa, pharetra vel vestibulum sed, egestas ut augue. Etiam tempor bibendum sagittis. Aliquam erat volutpat. Maecenas et eros et risus ullamcorper viverra ut eu dui. Mauris semper ut dui non ultrices. Integer a lectus a metus pellentesque congue eget ac orci. Donec fermentum mi quam, at sagittis eros rhoncus sed. Duis non laoreet odio. Nullam vulputate vehicula justo, eget blandit lacus feugiat sit amet.</p>\r\n\r\n<p>Integer ac quam vitae odio fringilla mattis vel sed ante. Etiam eu magna eleifend, mattis leo vel, porttitor nisl. Vestibulum ornare turpis at erat imperdiet, ac convallis felis lacinia. Nullam ultrices diam eu lorem ultrices, dapibus viverra felis ornare. Nam semper adipiscing urna non consequat. Morbi tempor laoreet felis, a pulvinar nunc commodo vel. Donec nec nulla ac turpis scelerisque interdum. Etiam tempor sem nisi. Sed lacinia id tellus sodales commodo. Quisque porttitor lobortis aliquam.</p>\r\n\r\n<p>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Ut id ultricies leo. Sed rhoncus, justo non convallis auctor, est augue condimentum lacus, ut porttitor leo tortor sit amet urna. Praesent ut ultrices nisi. Nunc tincidunt blandit cursus. Suspendisse potenti. Curabitur mollis metus nec odio vestibulum, sit amet iaculis sapien iaculis. Mauris non arcu at quam venenatis gravida in a ipsum. Vestibulum aliquet libero ut diam mattis fermentum. Donec dignissim dictum vulputate. Quisque commodo magna aliquet neque pretium dictum.</p>\r\n\r\n<p>Cras turpis mi, porta sed dignissim nec, posuere in velit. Duis eget magna at nisi auctor mattis. Donec eget pharetra velit, ac gravida neque. Integer fringilla turpis sed ipsum elementum facilisis. Morbi accumsan mi ut ligula vestibulum, sit amet cursus ligula imperdiet. Duis a euismod elit, non facilisis tortor. Mauris id ligula tellus. Sed ligula tellus, pharetra vel nisl a, placerat iaculis libero. Etiam pellentesque mauris libero, non dapibus nibh lobortis nec. Nam fermentum, urna non luctus semper, tellus leo rutrum risus, a tempor purus ligula non justo. Vivamus aliquam arcu a porttitor ultricies. Nunc sodales vel risus non mattis. Fusce id nisl dapibus, ornare dui at, vestibulum arcu.</p>', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras hendrerit.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras odio arcu, elementum sed aliquet non, consectetur non neque. Curabitur tempor pretium velit id consequat. Nunc felis dolor, eleifend ut pretium at, euismod id nunc.', '', 'lorem-ipsum-dolor-sit-amet-consectetur-adipiscing-elit-cras-hendrerit2'),
(842, 'es', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras hendrerit.', '315', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras odio arcu, elementum sed aliquet non, consectetur non neque. Curabitur tempor pretium velit id consequat. Nunc felis dolor, eleifend ut pretium at, euismod id nunc.', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc aliquet molestie tortor ut luctus. Proin id hendrerit tortor, et vehicula risus. Aliquam adipiscing magna iaculis nunc malesuada facilisis. Aliquam justo magna, accumsan sed malesuada eu, pretium sit amet arcu. Ut lobortis eu augue sed fringilla. Curabitur vestibulum est sed sollicitudin venenatis. Etiam vitae commodo ligula. Donec lacinia elit non tellus convallis porta. Vivamus eu nunc ac urna vestibulum laoreet. Ut tincidunt imperdiet lobortis. Donec bibendum nunc sed lorem adipiscing vehicula.</p>\r\n\r\n<p>Etiam condimentum elit non interdum tincidunt. Nullam sagittis, sapien sit amet iaculis ullamcorper, odio lorem accumsan leo, ac eleifend mauris libero vel nibh. Aliquam aliquam libero quis nisl mattis ornare. Cras dictum vitae mi eu blandit. Maecenas ac aliquam sem. Morbi dictum felis eu augue blandit molestie. Integer aliquam vitae enim ultricies vehicula. Duis suscipit, augue sed molestie vehicula, turpis mi rutrum arcu, at pulvinar sapien est at massa. Ut interdum purus at turpis pellentesque ultricies. Aenean luctus dapibus elit non pellentesque. Praesent posuere nisi a nibh suscipit, at sagittis mauris faucibus. Vestibulum vitae erat eros.</p>\r\n\r\n<p>Fusce tristique justo vel iaculis condimentum. Sed vitae arcu et risus commodo ornare. Fusce varius sapien laoreet, tincidunt leo non, dignissim nisl. Morbi ut luctus risus. Mauris laoreet in tellus sed luctus. Fusce eu augue eu mi sollicitudin ullamcorper. Cras consequat dui eu leo mattis varius.</p>\r\n\r\n<p>Praesent quam lorem, pharetra ac elementum quis, lobortis vel nunc. Nulla et cursus dolor, sit amet aliquam purus. Donec id sapien pulvinar, varius orci et, venenatis lectus. Nullam posuere vel lacus nec fermentum. Integer et rhoncus turpis. Cras blandit libero nisi, nec volutpat libero eleifend ac. Mauris lacinia lacus in convallis interdum. Donec laoreet magna fermentum, mattis orci sit amet, rutrum mauris.</p>\r\n\r\n<p>Nulla diam massa, pharetra vel vestibulum sed, egestas ut augue. Etiam tempor bibendum sagittis. Aliquam erat volutpat. Maecenas et eros et risus ullamcorper viverra ut eu dui. Mauris semper ut dui non ultrices. Integer a lectus a metus pellentesque congue eget ac orci. Donec fermentum mi quam, at sagittis eros rhoncus sed. Duis non laoreet odio. Nullam vulputate vehicula justo, eget blandit lacus feugiat sit amet.</p>\r\n\r\n<p>Integer ac quam vitae odio fringilla mattis vel sed ante. Etiam eu magna eleifend, mattis leo vel, porttitor nisl. Vestibulum ornare turpis at erat imperdiet, ac convallis felis lacinia. Nullam ultrices diam eu lorem ultrices, dapibus viverra felis ornare. Nam semper adipiscing urna non consequat. Morbi tempor laoreet felis, a pulvinar nunc commodo vel. Donec nec nulla ac turpis scelerisque interdum. Etiam tempor sem nisi. Sed lacinia id tellus sodales commodo. Quisque porttitor lobortis aliquam.</p>\r\n\r\n<p>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Ut id ultricies leo. Sed rhoncus, justo non convallis auctor, est augue condimentum lacus, ut porttitor leo tortor sit amet urna. Praesent ut ultrices nisi. Nunc tincidunt blandit cursus. Suspendisse potenti. Curabitur mollis metus nec odio vestibulum, sit amet iaculis sapien iaculis. Mauris non arcu at quam venenatis gravida in a ipsum. Vestibulum aliquet libero ut diam mattis fermentum. Donec dignissim dictum vulputate. Quisque commodo magna aliquet neque pretium dictum.</p>\r\n\r\n<p>Cras turpis mi, porta sed dignissim nec, posuere in velit. Duis eget magna at nisi auctor mattis. Donec eget pharetra velit, ac gravida neque. Integer fringilla turpis sed ipsum elementum facilisis. Morbi accumsan mi ut ligula vestibulum, sit amet cursus ligula imperdiet. Duis a euismod elit, non facilisis tortor. Mauris id ligula tellus. Sed ligula tellus, pharetra vel nisl a, placerat iaculis libero. Etiam pellentesque mauris libero, non dapibus nibh lobortis nec. Nam fermentum, urna non luctus semper, tellus leo rutrum risus, a tempor purus ligula non justo. Vivamus aliquam arcu a porttitor ultricies. Nunc sodales vel risus non mattis. Fusce id nisl dapibus, ornare dui at, vestibulum arcu.</p>', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras hendrerit.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras odio arcu, elementum sed aliquet non, consectetur non neque. Curabitur tempor pretium velit id consequat. Nunc felis dolor, eleifend ut pretium at, euismod id nunc.', '', 'lorem-ipsum-dolor-sit-amet-consectetur-adipiscing-elit-cras-hendrerit9'),
(842, 'en', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras hendrerit.', '315', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras odio arcu, elementum sed aliquet non, consectetur non neque. Curabitur tempor pretium velit id consequat. Nunc felis dolor, eleifend ut pretium at, euismod id nunc.', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc aliquet molestie tortor ut luctus. Proin id hendrerit tortor, et vehicula risus. Aliquam adipiscing magna iaculis nunc malesuada facilisis. Aliquam justo magna, accumsan sed malesuada eu, pretium sit amet arcu. Ut lobortis eu augue sed fringilla. Curabitur vestibulum est sed sollicitudin venenatis. Etiam vitae commodo ligula. Donec lacinia elit non tellus convallis porta. Vivamus eu nunc ac urna vestibulum laoreet. Ut tincidunt imperdiet lobortis. Donec bibendum nunc sed lorem adipiscing vehicula.</p>\r\n\r\n<p>Etiam condimentum elit non interdum tincidunt. Nullam sagittis, sapien sit amet iaculis ullamcorper, odio lorem accumsan leo, ac eleifend mauris libero vel nibh. Aliquam aliquam libero quis nisl mattis ornare. Cras dictum vitae mi eu blandit. Maecenas ac aliquam sem. Morbi dictum felis eu augue blandit molestie. Integer aliquam vitae enim ultricies vehicula. Duis suscipit, augue sed molestie vehicula, turpis mi rutrum arcu, at pulvinar sapien est at massa. Ut interdum purus at turpis pellentesque ultricies. Aenean luctus dapibus elit non pellentesque. Praesent posuere nisi a nibh suscipit, at sagittis mauris faucibus. Vestibulum vitae erat eros.</p>\r\n\r\n<p>Fusce tristique justo vel iaculis condimentum. Sed vitae arcu et risus commodo ornare. Fusce varius sapien laoreet, tincidunt leo non, dignissim nisl. Morbi ut luctus risus. Mauris laoreet in tellus sed luctus. Fusce eu augue eu mi sollicitudin ullamcorper. Cras consequat dui eu leo mattis varius.</p>\r\n\r\n<p>Praesent quam lorem, pharetra ac elementum quis, lobortis vel nunc. Nulla et cursus dolor, sit amet aliquam purus. Donec id sapien pulvinar, varius orci et, venenatis lectus. Nullam posuere vel lacus nec fermentum. Integer et rhoncus turpis. Cras blandit libero nisi, nec volutpat libero eleifend ac. Mauris lacinia lacus in convallis interdum. Donec laoreet magna fermentum, mattis orci sit amet, rutrum mauris.</p>\r\n\r\n<p>Nulla diam massa, pharetra vel vestibulum sed, egestas ut augue. Etiam tempor bibendum sagittis. Aliquam erat volutpat. Maecenas et eros et risus ullamcorper viverra ut eu dui. Mauris semper ut dui non ultrices. Integer a lectus a metus pellentesque congue eget ac orci. Donec fermentum mi quam, at sagittis eros rhoncus sed. Duis non laoreet odio. Nullam vulputate vehicula justo, eget blandit lacus feugiat sit amet.</p>\r\n\r\n<p>Integer ac quam vitae odio fringilla mattis vel sed ante. Etiam eu magna eleifend, mattis leo vel, porttitor nisl. Vestibulum ornare turpis at erat imperdiet, ac convallis felis lacinia. Nullam ultrices diam eu lorem ultrices, dapibus viverra felis ornare. Nam semper adipiscing urna non consequat. Morbi tempor laoreet felis, a pulvinar nunc commodo vel. Donec nec nulla ac turpis scelerisque interdum. Etiam tempor sem nisi. Sed lacinia id tellus sodales commodo. Quisque porttitor lobortis aliquam.</p>\r\n\r\n<p>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Ut id ultricies leo. Sed rhoncus, justo non convallis auctor, est augue condimentum lacus, ut porttitor leo tortor sit amet urna. Praesent ut ultrices nisi. Nunc tincidunt blandit cursus. Suspendisse potenti. Curabitur mollis metus nec odio vestibulum, sit amet iaculis sapien iaculis. Mauris non arcu at quam venenatis gravida in a ipsum. Vestibulum aliquet libero ut diam mattis fermentum. Donec dignissim dictum vulputate. Quisque commodo magna aliquet neque pretium dictum.</p>\r\n\r\n<p>Cras turpis mi, porta sed dignissim nec, posuere in velit. Duis eget magna at nisi auctor mattis. Donec eget pharetra velit, ac gravida neque. Integer fringilla turpis sed ipsum elementum facilisis. Morbi accumsan mi ut ligula vestibulum, sit amet cursus ligula imperdiet. Duis a euismod elit, non facilisis tortor. Mauris id ligula tellus. Sed ligula tellus, pharetra vel nisl a, placerat iaculis libero. Etiam pellentesque mauris libero, non dapibus nibh lobortis nec. Nam fermentum, urna non luctus semper, tellus leo rutrum risus, a tempor purus ligula non justo. Vivamus aliquam arcu a porttitor ultricies. Nunc sodales vel risus non mattis. Fusce id nisl dapibus, ornare dui at, vestibulum arcu.</p>\r\n\r\n<p>&nbsp;</p>', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras hendrerit.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras odio arcu, elementum sed aliquet non, consectetur non neque. Curabitur tempor pretium velit id consequat. Nunc felis dolor, eleifend ut pretium at, euismod id nunc.', '', 'lorem-ipsum-dolor-sit-amet-consectetur-adipiscing-elit-cras-hendrerit8'),
(841, 'en', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras hendrerit.', '284', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras odio arcu, elementum sed aliquet non, consectetur non neque. Curabitur tempor pretium velit id consequat. Nunc felis dolor, eleifend ut pretium at, euismod id nunc.', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc aliquet molestie tortor ut luctus. Proin id hendrerit tortor, et vehicula risus. Aliquam adipiscing magna iaculis nunc malesuada facilisis. Aliquam justo magna, accumsan sed malesuada eu, pretium sit amet arcu. Ut lobortis eu augue sed fringilla. Curabitur vestibulum est sed sollicitudin venenatis. Etiam vitae commodo ligula. Donec lacinia elit non tellus convallis porta. Vivamus eu nunc ac urna vestibulum laoreet. Ut tincidunt imperdiet lobortis. Donec bibendum nunc sed lorem adipiscing vehicula.</p>\r\n\r\n<p>Etiam condimentum elit non interdum tincidunt. Nullam sagittis, sapien sit amet iaculis ullamcorper, odio lorem accumsan leo, ac eleifend mauris libero vel nibh. Aliquam aliquam libero quis nisl mattis ornare. Cras dictum vitae mi eu blandit. Maecenas ac aliquam sem. Morbi dictum felis eu augue blandit molestie. Integer aliquam vitae enim ultricies vehicula. Duis suscipit, augue sed molestie vehicula, turpis mi rutrum arcu, at pulvinar sapien est at massa. Ut interdum purus at turpis pellentesque ultricies. Aenean luctus dapibus elit non pellentesque. Praesent posuere nisi a nibh suscipit, at sagittis mauris faucibus. Vestibulum vitae erat eros.</p>\r\n\r\n<p>Fusce tristique justo vel iaculis condimentum. Sed vitae arcu et risus commodo ornare. Fusce varius sapien laoreet, tincidunt leo non, dignissim nisl. Morbi ut luctus risus. Mauris laoreet in tellus sed luctus. Fusce eu augue eu mi sollicitudin ullamcorper. Cras consequat dui eu leo mattis varius.</p>\r\n\r\n<p>Praesent quam lorem, pharetra ac elementum quis, lobortis vel nunc. Nulla et cursus dolor, sit amet aliquam purus. Donec id sapien pulvinar, varius orci et, venenatis lectus. Nullam posuere vel lacus nec fermentum. Integer et rhoncus turpis. Cras blandit libero nisi, nec volutpat libero eleifend ac. Mauris lacinia lacus in convallis interdum. Donec laoreet magna fermentum, mattis orci sit amet, rutrum mauris.</p>\r\n\r\n<p>Nulla diam massa, pharetra vel vestibulum sed, egestas ut augue. Etiam tempor bibendum sagittis. Aliquam erat volutpat. Maecenas et eros et risus ullamcorper viverra ut eu dui. Mauris semper ut dui non ultrices. Integer a lectus a metus pellentesque congue eget ac orci. Donec fermentum mi quam, at sagittis eros rhoncus sed. Duis non laoreet odio. Nullam vulputate vehicula justo, eget blandit lacus feugiat sit amet.</p>\r\n\r\n<p>Integer ac quam vitae odio fringilla mattis vel sed ante. Etiam eu magna eleifend, mattis leo vel, porttitor nisl. Vestibulum ornare turpis at erat imperdiet, ac convallis felis lacinia. Nullam ultrices diam eu lorem ultrices, dapibus viverra felis ornare. Nam semper adipiscing urna non consequat. Morbi tempor laoreet felis, a pulvinar nunc commodo vel. Donec nec nulla ac turpis scelerisque interdum. Etiam tempor sem nisi. Sed lacinia id tellus sodales commodo. Quisque porttitor lobortis aliquam.</p>\r\n\r\n<p>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Ut id ultricies leo. Sed rhoncus, justo non convallis auctor, est augue condimentum lacus, ut porttitor leo tortor sit amet urna. Praesent ut ultrices nisi. Nunc tincidunt blandit cursus. Suspendisse potenti. Curabitur mollis metus nec odio vestibulum, sit amet iaculis sapien iaculis. Mauris non arcu at quam venenatis gravida in a ipsum. Vestibulum aliquet libero ut diam mattis fermentum. Donec dignissim dictum vulputate. Quisque commodo magna aliquet neque pretium dictum.</p>\r\n\r\n<p>Cras turpis mi, porta sed dignissim nec, posuere in velit. Duis eget magna at nisi auctor mattis. Donec eget pharetra velit, ac gravida neque. Integer fringilla turpis sed ipsum elementum facilisis. Morbi accumsan mi ut ligula vestibulum, sit amet cursus ligula imperdiet. Duis a euismod elit, non facilisis tortor. Mauris id ligula tellus. Sed ligula tellus, pharetra vel nisl a, placerat iaculis libero. Etiam pellentesque mauris libero, non dapibus nibh lobortis nec. Nam fermentum, urna non luctus semper, tellus leo rutrum risus, a tempor purus ligula non justo. Vivamus aliquam arcu a porttitor ultricies. Nunc sodales vel risus non mattis. Fusce id nisl dapibus, ornare dui at, vestibulum arcu.</p>\r\n\r\n<p>&nbsp;</p>', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras hendrerit.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras odio arcu, elementum sed aliquet non, consectetur non neque. Curabitur tempor pretium velit id consequat. Nunc felis dolor, eleifend ut pretium at, euismod id nunc.', '', 'lorem-ipsum-dolor-sit-amet-consectetur-adipiscing-elit-cras-hendrerit7'),
(850, 'en', '5 places that make a great holiday', '283', '', '', '5 places that make a great holiday', '', '', '5-places-that-make-a-great-holiday'),
(839, 'en', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras hendrerit.', '287', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras odio arcu, elementum sed aliquet non, consectetur non neque. Curabitur tempor pretium velit id consequat. Nunc felis dolor, eleifend ut pretium at, euismod id nunc.', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc aliquet molestie tortor ut luctus. Proin id hendrerit tortor, et vehicula risus. Aliquam adipiscing magna iaculis nunc malesuada facilisis. Aliquam justo magna, accumsan sed malesuada eu, pretium sit amet arcu. Ut lobortis eu augue sed fringilla. Curabitur vestibulum est sed sollicitudin venenatis. Etiam vitae commodo ligula. Donec lacinia elit non tellus convallis porta. Vivamus eu nunc ac urna vestibulum laoreet. Ut tincidunt imperdiet lobortis. Donec bibendum nunc sed lorem adipiscing vehicula.</p>\r\n\r\n<p>Etiam condimentum elit non interdum tincidunt. Nullam sagittis, sapien sit amet iaculis ullamcorper, odio lorem accumsan leo, ac eleifend mauris libero vel nibh. Aliquam aliquam libero quis nisl mattis ornare. Cras dictum vitae mi eu blandit. Maecenas ac aliquam sem. Morbi dictum felis eu augue blandit molestie. Integer aliquam vitae enim ultricies vehicula. Duis suscipit, augue sed molestie vehicula, turpis mi rutrum arcu, at pulvinar sapien est at massa. Ut interdum purus at turpis pellentesque ultricies. Aenean luctus dapibus elit non pellentesque. Praesent posuere nisi a nibh suscipit, at sagittis mauris faucibus. Vestibulum vitae erat eros.</p>\r\n\r\n<p>Fusce tristique justo vel iaculis condimentum. Sed vitae arcu et risus commodo ornare. Fusce varius sapien laoreet, tincidunt leo non, dignissim nisl. Morbi ut luctus risus. Mauris laoreet in tellus sed luctus. Fusce eu augue eu mi sollicitudin ullamcorper. Cras consequat dui eu leo mattis varius.</p>\r\n\r\n<p>Praesent quam lorem, pharetra ac elementum quis, lobortis vel nunc. Nulla et cursus dolor, sit amet aliquam purus. Donec id sapien pulvinar, varius orci et, venenatis lectus. Nullam posuere vel lacus nec fermentum. Integer et rhoncus turpis. Cras blandit libero nisi, nec volutpat libero eleifend ac. Mauris lacinia lacus in convallis interdum. Donec laoreet magna fermentum, mattis orci sit amet, rutrum mauris.</p>\r\n\r\n<p>Nulla diam massa, pharetra vel vestibulum sed, egestas ut augue. Etiam tempor bibendum sagittis. Aliquam erat volutpat. Maecenas et eros et risus ullamcorper viverra ut eu dui. Mauris semper ut dui non ultrices. Integer a lectus a metus pellentesque congue eget ac orci. Donec fermentum mi quam, at sagittis eros rhoncus sed. Duis non laoreet odio. Nullam vulputate vehicula justo, eget blandit lacus feugiat sit amet.</p>\r\n\r\n<p>Integer ac quam vitae odio fringilla mattis vel sed ante. Etiam eu magna eleifend, mattis leo vel, porttitor nisl. Vestibulum ornare turpis at erat imperdiet, ac convallis felis lacinia. Nullam ultrices diam eu lorem ultrices, dapibus viverra felis ornare. Nam semper adipiscing urna non consequat. Morbi tempor laoreet felis, a pulvinar nunc commodo vel. Donec nec nulla ac turpis scelerisque interdum. Etiam tempor sem nisi. Sed lacinia id tellus sodales commodo. Quisque porttitor lobortis aliquam.</p>\r\n\r\n<p>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Ut id ultricies leo. Sed rhoncus, justo non convallis auctor, est augue condimentum lacus, ut porttitor leo tortor sit amet urna. Praesent ut ultrices nisi. Nunc tincidunt blandit cursus. Suspendisse potenti. Curabitur mollis metus nec odio vestibulum, sit amet iaculis sapien iaculis. Mauris non arcu at quam venenatis gravida in a ipsum. Vestibulum aliquet libero ut diam mattis fermentum. Donec dignissim dictum vulputate. Quisque commodo magna aliquet neque pretium dictum.</p>\r\n\r\n<p>Cras turpis mi, porta sed dignissim nec, posuere in velit. Duis eget magna at nisi auctor mattis. Donec eget pharetra velit, ac gravida neque. Integer fringilla turpis sed ipsum elementum facilisis. Morbi accumsan mi ut ligula vestibulum, sit amet cursus ligula imperdiet. Duis a euismod elit, non facilisis tortor. Mauris id ligula tellus. Sed ligula tellus, pharetra vel nisl a, placerat iaculis libero. Etiam pellentesque mauris libero, non dapibus nibh lobortis nec. Nam fermentum, urna non luctus semper, tellus leo rutrum risus, a tempor purus ligula non justo. Vivamus aliquam arcu a porttitor ultricies. Nunc sodales vel risus non mattis. Fusce id nisl dapibus, ornare dui at, vestibulum arcu.</p>', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras hendrerit.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras odio arcu, elementum sed aliquet non, consectetur non neque. Curabitur tempor pretium velit id consequat. Nunc felis dolor, eleifend ut pretium at, euismod id nunc.', '', 'lorem-ipsum-dolor-sit-amet-consectetur-adipiscing-elit-cras-hendrerit4'),
(849, 'en', 'Most visited countries', '284', '', '', 'Most visited countries', '', '', 'most-visited-countries');
INSERT INTO `s_newslocalizations` (`newsid`, `lang`, `name`, `header`, `comment`, `text`, `seo_header`, `seo_description`, `seo_keywords`, `slug`) VALUES
(836, 'es', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras hendrerit.', '281', 'Lorum ipsum is the traditional Latin placeholder text, used when a designer needs a chunk of text for dummying up a layout. Journo Ipsum is like that, only using some of the most common catchphrases, buzzwords, and bon mots of the future-of-news crowd. Hit reload for a new batch. For entertainment purposes only.', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc aliquet molestie tortor ut luctus. Proin id hendrerit tortor, et vehicula risus. Aliquam adipiscing magna iaculis nunc malesuada facilisis. Aliquam justo magna, accumsan sed malesuada eu, pretium sit amet arcu. Ut lobortis eu augue sed fringilla. Curabitur vestibulum est sed sollicitudin venenatis. Etiam vitae commodo ligula. Donec lacinia elit non tellus convallis porta. Vivamus eu nunc ac urna vestibulum laoreet. Ut tincidunt imperdiet lobortis. Donec bibendum nunc sed lorem adipiscing vehicula.</p>\r\n\r\n<p>Etiam condimentum elit non interdum tincidunt. Nullam sagittis, sapien sit amet iaculis ullamcorper, odio lorem accumsan leo, ac eleifend mauris libero vel nibh. Aliquam aliquam libero quis nisl mattis ornare. Cras dictum vitae mi eu blandit. Maecenas ac aliquam sem. Morbi dictum felis eu augue blandit molestie. Integer aliquam vitae enim ultricies vehicula. Duis suscipit, augue sed molestie vehicula, turpis mi rutrum arcu, at pulvinar sapien est at massa. Ut interdum purus at turpis pellentesque ultricies. Aenean luctus dapibus elit non pellentesque. Praesent posuere nisi a nibh suscipit, at sagittis mauris faucibus. Vestibulum vitae erat eros.</p>\r\n\r\n<p>Fusce tristique justo vel iaculis condimentum. Sed vitae arcu et risus commodo ornare. Fusce varius sapien laoreet, tincidunt leo non, dignissim nisl. Morbi ut luctus risus. Mauris laoreet in tellus sed luctus. Fusce eu augue eu mi sollicitudin ullamcorper. Cras consequat dui eu leo mattis varius.</p>\r\n\r\n<p>Praesent quam lorem, pharetra ac elementum quis, lobortis vel nunc. Nulla et cursus dolor, sit amet aliquam purus. Donec id sapien pulvinar, varius orci et, venenatis lectus. Nullam posuere vel lacus nec fermentum. Integer et rhoncus turpis. Cras blandit libero nisi, nec volutpat libero eleifend ac. Mauris lacinia lacus in convallis interdum. Donec laoreet magna fermentum, mattis orci sit amet, rutrum mauris.</p>\r\n\r\n<p>Nulla diam massa, pharetra vel vestibulum sed, egestas ut augue. Etiam tempor bibendum sagittis. Aliquam erat volutpat. Maecenas et eros et risus ullamcorper viverra ut eu dui. Mauris semper ut dui non ultrices. Integer a lectus a metus pellentesque congue eget ac orci. Donec fermentum mi quam, at sagittis eros rhoncus sed. Duis non laoreet odio. Nullam vulputate vehicula justo, eget blandit lacus feugiat sit amet.</p>\r\n\r\n<p>Integer ac quam vitae odio fringilla mattis vel sed ante. Etiam eu magna eleifend, mattis leo vel, porttitor nisl. Vestibulum ornare turpis at erat imperdiet, ac convallis felis lacinia. Nullam ultrices diam eu lorem ultrices, dapibus viverra felis ornare. Nam semper adipiscing urna non consequat. Morbi tempor laoreet felis, a pulvinar nunc commodo vel. Donec nec nulla ac turpis scelerisque interdum. Etiam tempor sem nisi. Sed lacinia id tellus sodales commodo. Quisque porttitor lobortis aliquam.</p>\r\n\r\n<p>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Ut id ultricies leo. Sed rhoncus, justo non convallis auctor, est augue condimentum lacus, ut porttitor leo tortor sit amet urna. Praesent ut ultrices nisi. Nunc tincidunt blandit cursus. Suspendisse potenti. Curabitur mollis metus nec odio vestibulum, sit amet iaculis sapien iaculis. Mauris non arcu at quam venenatis gravida in a ipsum. Vestibulum aliquet libero ut diam mattis fermentum. Donec dignissim dictum vulputate. Quisque commodo magna aliquet neque pretium dictum.</p>\r\n\r\n<p>Cras turpis mi, porta sed dignissim nec, posuere in velit. Duis eget magna at nisi auctor mattis. Donec eget pharetra velit, ac gravida neque. Integer fringilla turpis sed ipsum elementum facilisis. Morbi accumsan mi ut ligula vestibulum, sit amet cursus ligula imperdiet. Duis a euismod elit, non facilisis tortor. Mauris id ligula tellus. Sed ligula tellus, pharetra vel nisl a, placerat iaculis libero. Etiam pellentesque mauris libero, non dapibus nibh lobortis nec. Nam fermentum, urna non luctus semper, tellus leo rutrum risus, a tempor purus ligula non justo. Vivamus aliquam arcu a porttitor ultricies. Nunc sodales vel risus non mattis. Fusce id nisl dapibus, ornare dui at, vestibulum arcu.</p>', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras hendrerit.', 'Lorum ipsum is the traditional Latin placeholder text, used when a designer needs a chunk of text for dummying up a layout. Journo Ipsum is like that, only using some of the most common catchphrases, buzzwords, and bon mots of the future-of-news crowd. Hit reload for a new batch. For entertainment purposes only.', '', 'lorem-ipsum-dolor-sit-amet-consectetur-adipiscing-elit-cras-hendrerit10'),
(836, 'en', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras hendrerit.', '281', 'Lorum ipsum is the traditional Latin placeholder text, used when a designer needs a chunk of text for dummying up a layout. Journo Ipsum is like that, only using some of the most common catchphrases, buzzwords, and bon mots of the future-of-news crowd. Hit reload for a new batch. For entertainment purposes only.', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc aliquet molestie tortor ut luctus. Proin id hendrerit tortor, et vehicula risus. Aliquam adipiscing magna iaculis nunc malesuada facilisis. Aliquam justo magna, accumsan sed malesuada eu, pretium sit amet arcu. Ut lobortis eu augue sed fringilla. Curabitur vestibulum est sed sollicitudin venenatis. Etiam vitae commodo ligula. Donec lacinia elit non tellus convallis porta. Vivamus eu nunc ac urna vestibulum laoreet. Ut tincidunt imperdiet lobortis. Donec bibendum nunc sed lorem adipiscing vehicula.</p>\r\n\r\n<p>Etiam condimentum elit non interdum tincidunt. Nullam sagittis, sapien sit amet iaculis ullamcorper, odio lorem accumsan leo, ac eleifend mauris libero vel nibh. Aliquam aliquam libero quis nisl mattis ornare. Cras dictum vitae mi eu blandit. Maecenas ac aliquam sem. Morbi dictum felis eu augue blandit molestie. Integer aliquam vitae enim ultricies vehicula. Duis suscipit, augue sed molestie vehicula, turpis mi rutrum arcu, at pulvinar sapien est at massa. Ut interdum purus at turpis pellentesque ultricies. Aenean luctus dapibus elit non pellentesque. Praesent posuere nisi a nibh suscipit, at sagittis mauris faucibus. Vestibulum vitae erat eros.</p>\r\n\r\n<p>Fusce tristique justo vel iaculis condimentum. Sed vitae arcu et risus commodo ornare. Fusce varius sapien laoreet, tincidunt leo non, dignissim nisl. Morbi ut luctus risus. Mauris laoreet in tellus sed luctus. Fusce eu augue eu mi sollicitudin ullamcorper. Cras consequat dui eu leo mattis varius.</p>\r\n\r\n<p>Praesent quam lorem, pharetra ac elementum quis, lobortis vel nunc. Nulla et cursus dolor, sit amet aliquam purus. Donec id sapien pulvinar, varius orci et, venenatis lectus. Nullam posuere vel lacus nec fermentum. Integer et rhoncus turpis. Cras blandit libero nisi, nec volutpat libero eleifend ac. Mauris lacinia lacus in convallis interdum. Donec laoreet magna fermentum, mattis orci sit amet, rutrum mauris.</p>\r\n\r\n<p>Nulla diam massa, pharetra vel vestibulum sed, egestas ut augue. Etiam tempor bibendum sagittis. Aliquam erat volutpat. Maecenas et eros et risus ullamcorper viverra ut eu dui. Mauris semper ut dui non ultrices. Integer a lectus a metus pellentesque congue eget ac orci. Donec fermentum mi quam, at sagittis eros rhoncus sed. Duis non laoreet odio. Nullam vulputate vehicula justo, eget blandit lacus feugiat sit amet.</p>\r\n\r\n<p>Integer ac quam vitae odio fringilla mattis vel sed ante. Etiam eu magna eleifend, mattis leo vel, porttitor nisl. Vestibulum ornare turpis at erat imperdiet, ac convallis felis lacinia. Nullam ultrices diam eu lorem ultrices, dapibus viverra felis ornare. Nam semper adipiscing urna non consequat. Morbi tempor laoreet felis, a pulvinar nunc commodo vel. Donec nec nulla ac turpis scelerisque interdum. Etiam tempor sem nisi. Sed lacinia id tellus sodales commodo. Quisque porttitor lobortis aliquam.</p>\r\n\r\n<p>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Ut id ultricies leo. Sed rhoncus, justo non convallis auctor, est augue condimentum lacus, ut porttitor leo tortor sit amet urna. Praesent ut ultrices nisi. Nunc tincidunt blandit cursus. Suspendisse potenti. Curabitur mollis metus nec odio vestibulum, sit amet iaculis sapien iaculis. Mauris non arcu at quam venenatis gravida in a ipsum. Vestibulum aliquet libero ut diam mattis fermentum. Donec dignissim dictum vulputate. Quisque commodo magna aliquet neque pretium dictum.</p>\r\n\r\n<p>Cras turpis mi, porta sed dignissim nec, posuere in velit. Duis eget magna at nisi auctor mattis. Donec eget pharetra velit, ac gravida neque. Integer fringilla turpis sed ipsum elementum facilisis. Morbi accumsan mi ut ligula vestibulum, sit amet cursus ligula imperdiet. Duis a euismod elit, non facilisis tortor. Mauris id ligula tellus. Sed ligula tellus, pharetra vel nisl a, placerat iaculis libero. Etiam pellentesque mauris libero, non dapibus nibh lobortis nec. Nam fermentum, urna non luctus semper, tellus leo rutrum risus, a tempor purus ligula non justo. Vivamus aliquam arcu a porttitor ultricies. Nunc sodales vel risus non mattis. Fusce id nisl dapibus, ornare dui at, vestibulum arcu.</p>', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras hendrerit.', 'Lorum ipsum is the traditional Latin placeholder text, used when a designer needs a chunk of text for dummying up a layout. Journo Ipsum is like that, only using some of the most common catchphrases, buzzwords, and bon mots of the future-of-news crowd. Hit reload for a new batch. For entertainment purposes only.', '', 'lorem-ipsum-dolor-sit-amet-consectetur-adipiscing-elit-cras-hendrerit2'),
(838, 'es', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras hendrerit.', '309', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras odio arcu, elementum sed aliquet non, consectetur non neque. Curabitur tempor pretium velit id consequat. Nunc felis dolor, eleifend ut pretium at, euismod id nunc.', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc aliquet molestie tortor ut luctus. Proin id hendrerit tortor, et vehicula risus. Aliquam adipiscing magna iaculis nunc malesuada facilisis. Aliquam justo magna, accumsan sed malesuada eu, pretium sit amet arcu. Ut lobortis eu augue sed fringilla. Curabitur vestibulum est sed sollicitudin venenatis. Etiam vitae commodo ligula. Donec lacinia elit non tellus convallis porta. Vivamus eu nunc ac urna vestibulum laoreet. Ut tincidunt imperdiet lobortis. Donec bibendum nunc sed lorem adipiscing vehicula.</p>\r\n\r\n<p>Etiam condimentum elit non interdum tincidunt. Nullam sagittis, sapien sit amet iaculis ullamcorper, odio lorem accumsan leo, ac eleifend mauris libero vel nibh. Aliquam aliquam libero quis nisl mattis ornare. Cras dictum vitae mi eu blandit. Maecenas ac aliquam sem. Morbi dictum felis eu augue blandit molestie. Integer aliquam vitae enim ultricies vehicula. Duis suscipit, augue sed molestie vehicula, turpis mi rutrum arcu, at pulvinar sapien est at massa. Ut interdum purus at turpis pellentesque ultricies. Aenean luctus dapibus elit non pellentesque. Praesent posuere nisi a nibh suscipit, at sagittis mauris faucibus. Vestibulum vitae erat eros.</p>\r\n\r\n<p>Fusce tristique justo vel iaculis condimentum. Sed vitae arcu et risus commodo ornare. Fusce varius sapien laoreet, tincidunt leo non, dignissim nisl. Morbi ut luctus risus. Mauris laoreet in tellus sed luctus. Fusce eu augue eu mi sollicitudin ullamcorper. Cras consequat dui eu leo mattis varius.</p>\r\n\r\n<p>Praesent quam lorem, pharetra ac elementum quis, lobortis vel nunc. Nulla et cursus dolor, sit amet aliquam purus. Donec id sapien pulvinar, varius orci et, venenatis lectus. Nullam posuere vel lacus nec fermentum. Integer et rhoncus turpis. Cras blandit libero nisi, nec volutpat libero eleifend ac. Mauris lacinia lacus in convallis interdum. Donec laoreet magna fermentum, mattis orci sit amet, rutrum mauris.</p>\r\n\r\n<p>Nulla diam massa, pharetra vel vestibulum sed, egestas ut augue. Etiam tempor bibendum sagittis. Aliquam erat volutpat. Maecenas et eros et risus ullamcorper viverra ut eu dui. Mauris semper ut dui non ultrices. Integer a lectus a metus pellentesque congue eget ac orci. Donec fermentum mi quam, at sagittis eros rhoncus sed. Duis non laoreet odio. Nullam vulputate vehicula justo, eget blandit lacus feugiat sit amet.</p>\r\n\r\n<p>Integer ac quam vitae odio fringilla mattis vel sed ante. Etiam eu magna eleifend, mattis leo vel, porttitor nisl. Vestibulum ornare turpis at erat imperdiet, ac convallis felis lacinia. Nullam ultrices diam eu lorem ultrices, dapibus viverra felis ornare. Nam semper adipiscing urna non consequat. Morbi tempor laoreet felis, a pulvinar nunc commodo vel. Donec nec nulla ac turpis scelerisque interdum. Etiam tempor sem nisi. Sed lacinia id tellus sodales commodo. Quisque porttitor lobortis aliquam.</p>\r\n\r\n<p>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Ut id ultricies leo. Sed rhoncus, justo non convallis auctor, est augue condimentum lacus, ut porttitor leo tortor sit amet urna. Praesent ut ultrices nisi. Nunc tincidunt blandit cursus. Suspendisse potenti. Curabitur mollis metus nec odio vestibulum, sit amet iaculis sapien iaculis. Mauris non arcu at quam venenatis gravida in a ipsum. Vestibulum aliquet libero ut diam mattis fermentum. Donec dignissim dictum vulputate. Quisque commodo magna aliquet neque pretium dictum.</p>\r\n\r\n<p>Cras turpis mi, porta sed dignissim nec, posuere in velit. Duis eget magna at nisi auctor mattis. Donec eget pharetra velit, ac gravida neque. Integer fringilla turpis sed ipsum elementum facilisis. Morbi accumsan mi ut ligula vestibulum, sit amet cursus ligula imperdiet. Duis a euismod elit, non facilisis tortor. Mauris id ligula tellus. Sed ligula tellus, pharetra vel nisl a, placerat iaculis libero. Etiam pellentesque mauris libero, non dapibus nibh lobortis nec. Nam fermentum, urna non luctus semper, tellus leo rutrum risus, a tempor purus ligula non justo. Vivamus aliquam arcu a porttitor ultricies. Nunc sodales vel risus non mattis. Fusce id nisl dapibus, ornare dui at, vestibulum arcu.</p>', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras hendrerit.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras odio arcu, elementum sed aliquet non, consectetur non neque. Curabitur tempor pretium velit id consequat. Nunc felis dolor, eleifend ut pretium at, euismod id nunc.', '', 'lorem-ipsum-dolor-sit-amet-consectetur-adipiscing-elit-cras-hendrerit7'),
(848, 'en', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras hendrerit.', '315', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras odio arcu, elementum sed aliquet non, consectetur non neque. Curabitur tempor pretium velit id consequat. Nunc felis dolor, eleifend ut pretium at, euismod id nunc.', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc aliquet molestie tortor ut luctus. Proin id hendrerit tortor, et vehicula risus. Aliquam adipiscing magna iaculis nunc malesuada facilisis. Aliquam justo magna, accumsan sed malesuada eu, pretium sit amet arcu. Ut lobortis eu augue sed fringilla. Curabitur vestibulum est sed sollicitudin venenatis. Etiam vitae commodo ligula. Donec lacinia elit non tellus convallis porta. Vivamus eu nunc ac urna vestibulum laoreet. Ut tincidunt imperdiet lobortis. Donec bibendum nunc sed lorem adipiscing vehicula.</p>\r\n\r\n<p>Etiam condimentum elit non interdum tincidunt. Nullam sagittis, sapien sit amet iaculis ullamcorper, odio lorem accumsan leo, ac eleifend mauris libero vel nibh. Aliquam aliquam libero quis nisl mattis ornare. Cras dictum vitae mi eu blandit. Maecenas ac aliquam sem. Morbi dictum felis eu augue blandit molestie. Integer aliquam vitae enim ultricies vehicula. Duis suscipit, augue sed molestie vehicula, turpis mi rutrum arcu, at pulvinar sapien est at massa. Ut interdum purus at turpis pellentesque ultricies. Aenean luctus dapibus elit non pellentesque. Praesent posuere nisi a nibh suscipit, at sagittis mauris faucibus. Vestibulum vitae erat eros.</p>\r\n\r\n<p>Fusce tristique justo vel iaculis condimentum. Sed vitae arcu et risus commodo ornare. Fusce varius sapien laoreet, tincidunt leo non, dignissim nisl. Morbi ut luctus risus. Mauris laoreet in tellus sed luctus. Fusce eu augue eu mi sollicitudin ullamcorper. Cras consequat dui eu leo mattis varius.</p>\r\n\r\n<p>Praesent quam lorem, pharetra ac elementum quis, lobortis vel nunc. Nulla et cursus dolor, sit amet aliquam purus. Donec id sapien pulvinar, varius orci et, venenatis lectus. Nullam posuere vel lacus nec fermentum. Integer et rhoncus turpis. Cras blandit libero nisi, nec volutpat libero eleifend ac. Mauris lacinia lacus in convallis interdum. Donec laoreet magna fermentum, mattis orci sit amet, rutrum mauris.</p>\r\n\r\n<p>Nulla diam massa, pharetra vel vestibulum sed, egestas ut augue. Etiam tempor bibendum sagittis. Aliquam erat volutpat. Maecenas et eros et risus ullamcorper viverra ut eu dui. Mauris semper ut dui non ultrices. Integer a lectus a metus pellentesque congue eget ac orci. Donec fermentum mi quam, at sagittis eros rhoncus sed. Duis non laoreet odio. Nullam vulputate vehicula justo, eget blandit lacus feugiat sit amet.</p>\r\n\r\n<p>Integer ac quam vitae odio fringilla mattis vel sed ante. Etiam eu magna eleifend, mattis leo vel, porttitor nisl. Vestibulum ornare turpis at erat imperdiet, ac convallis felis lacinia. Nullam ultrices diam eu lorem ultrices, dapibus viverra felis ornare. Nam semper adipiscing urna non consequat. Morbi tempor laoreet felis, a pulvinar nunc commodo vel. Donec nec nulla ac turpis scelerisque interdum. Etiam tempor sem nisi. Sed lacinia id tellus sodales commodo. Quisque porttitor lobortis aliquam.</p>\r\n\r\n<p>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Ut id ultricies leo. Sed rhoncus, justo non convallis auctor, est augue condimentum lacus, ut porttitor leo tortor sit amet urna. Praesent ut ultrices nisi. Nunc tincidunt blandit cursus. Suspendisse potenti. Curabitur mollis metus nec odio vestibulum, sit amet iaculis sapien iaculis. Mauris non arcu at quam venenatis gravida in a ipsum. Vestibulum aliquet libero ut diam mattis fermentum. Donec dignissim dictum vulputate. Quisque commodo magna aliquet neque pretium dictum.</p>\r\n\r\n<p>Cras turpis mi, porta sed dignissim nec, posuere in velit. Duis eget magna at nisi auctor mattis. Donec eget pharetra velit, ac gravida neque. Integer fringilla turpis sed ipsum elementum facilisis. Morbi accumsan mi ut ligula vestibulum, sit amet cursus ligula imperdiet. Duis a euismod elit, non facilisis tortor. Mauris id ligula tellus. Sed ligula tellus, pharetra vel nisl a, placerat iaculis libero. Etiam pellentesque mauris libero, non dapibus nibh lobortis nec. Nam fermentum, urna non luctus semper, tellus leo rutrum risus, a tempor purus ligula non justo. Vivamus aliquam arcu a porttitor ultricies. Nunc sodales vel risus non mattis. Fusce id nisl dapibus, ornare dui at, vestibulum arcu.</p>', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras hendrerit.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras odio arcu, elementum sed aliquet non, consectetur non neque. Curabitur tempor pretium velit id consequat. Nunc felis dolor, eleifend ut pretium at, euismod id nunc.', '', 'lorem-ipsum-dolor-sit-amet-consectetur-adipiscing-elit-cras-hendrerit14'),
(848, 'es', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras hendrerit.', '315', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras odio arcu, elementum sed aliquet non, consectetur non neque. Curabitur tempor pretium velit id consequat. Nunc felis dolor, eleifend ut pretium at, euismod id nunc.', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc aliquet molestie tortor ut luctus. Proin id hendrerit tortor, et vehicula risus. Aliquam adipiscing magna iaculis nunc malesuada facilisis. Aliquam justo magna, accumsan sed malesuada eu, pretium sit amet arcu. Ut lobortis eu augue sed fringilla. Curabitur vestibulum est sed sollicitudin venenatis. Etiam vitae commodo ligula. Donec lacinia elit non tellus convallis porta. Vivamus eu nunc ac urna vestibulum laoreet. Ut tincidunt imperdiet lobortis. Donec bibendum nunc sed lorem adipiscing vehicula.</p>\r\n\r\n<p>Etiam condimentum elit non interdum tincidunt. Nullam sagittis, sapien sit amet iaculis ullamcorper, odio lorem accumsan leo, ac eleifend mauris libero vel nibh. Aliquam aliquam libero quis nisl mattis ornare. Cras dictum vitae mi eu blandit. Maecenas ac aliquam sem. Morbi dictum felis eu augue blandit molestie. Integer aliquam vitae enim ultricies vehicula. Duis suscipit, augue sed molestie vehicula, turpis mi rutrum arcu, at pulvinar sapien est at massa. Ut interdum purus at turpis pellentesque ultricies. Aenean luctus dapibus elit non pellentesque. Praesent posuere nisi a nibh suscipit, at sagittis mauris faucibus. Vestibulum vitae erat eros.</p>\r\n\r\n<p>Fusce tristique justo vel iaculis condimentum. Sed vitae arcu et risus commodo ornare. Fusce varius sapien laoreet, tincidunt leo non, dignissim nisl. Morbi ut luctus risus. Mauris laoreet in tellus sed luctus. Fusce eu augue eu mi sollicitudin ullamcorper. Cras consequat dui eu leo mattis varius.</p>\r\n\r\n<p>Praesent quam lorem, pharetra ac elementum quis, lobortis vel nunc. Nulla et cursus dolor, sit amet aliquam purus. Donec id sapien pulvinar, varius orci et, venenatis lectus. Nullam posuere vel lacus nec fermentum. Integer et rhoncus turpis. Cras blandit libero nisi, nec volutpat libero eleifend ac. Mauris lacinia lacus in convallis interdum. Donec laoreet magna fermentum, mattis orci sit amet, rutrum mauris.</p>\r\n\r\n<p>Nulla diam massa, pharetra vel vestibulum sed, egestas ut augue. Etiam tempor bibendum sagittis. Aliquam erat volutpat. Maecenas et eros et risus ullamcorper viverra ut eu dui. Mauris semper ut dui non ultrices. Integer a lectus a metus pellentesque congue eget ac orci. Donec fermentum mi quam, at sagittis eros rhoncus sed. Duis non laoreet odio. Nullam vulputate vehicula justo, eget blandit lacus feugiat sit amet.</p>\r\n\r\n<p>Integer ac quam vitae odio fringilla mattis vel sed ante. Etiam eu magna eleifend, mattis leo vel, porttitor nisl. Vestibulum ornare turpis at erat imperdiet, ac convallis felis lacinia. Nullam ultrices diam eu lorem ultrices, dapibus viverra felis ornare. Nam semper adipiscing urna non consequat. Morbi tempor laoreet felis, a pulvinar nunc commodo vel. Donec nec nulla ac turpis scelerisque interdum. Etiam tempor sem nisi. Sed lacinia id tellus sodales commodo. Quisque porttitor lobortis aliquam.</p>\r\n\r\n<p>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Ut id ultricies leo. Sed rhoncus, justo non convallis auctor, est augue condimentum lacus, ut porttitor leo tortor sit amet urna. Praesent ut ultrices nisi. Nunc tincidunt blandit cursus. Suspendisse potenti. Curabitur mollis metus nec odio vestibulum, sit amet iaculis sapien iaculis. Mauris non arcu at quam venenatis gravida in a ipsum. Vestibulum aliquet libero ut diam mattis fermentum. Donec dignissim dictum vulputate. Quisque commodo magna aliquet neque pretium dictum.</p>\r\n\r\n<p>Cras turpis mi, porta sed dignissim nec, posuere in velit. Duis eget magna at nisi auctor mattis. Donec eget pharetra velit, ac gravida neque. Integer fringilla turpis sed ipsum elementum facilisis. Morbi accumsan mi ut ligula vestibulum, sit amet cursus ligula imperdiet. Duis a euismod elit, non facilisis tortor. Mauris id ligula tellus. Sed ligula tellus, pharetra vel nisl a, placerat iaculis libero. Etiam pellentesque mauris libero, non dapibus nibh lobortis nec. Nam fermentum, urna non luctus semper, tellus leo rutrum risus, a tempor purus ligula non justo. Vivamus aliquam arcu a porttitor ultricies. Nunc sodales vel risus non mattis. Fusce id nisl dapibus, ornare dui at, vestibulum arcu.</p>', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras hendrerit.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras odio arcu, elementum sed aliquet non, consectetur non neque. Curabitur tempor pretium velit id consequat. Nunc felis dolor, eleifend ut pretium at, euismod id nunc.', '', 'lorem-ipsum-dolor-sit-amet-consectetur-adipiscing-elit-cras-hendrerit3'),
(834, 'es', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras hendrerit.', '295', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras odio arcu, elementum sed aliquet non, consectetur non neque. Curabitur tempor pretium velit id consequat. Nunc felis dolor, eleifend ut pretium at, euismod id nunc.', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc aliquet molestie tortor ut luctus. Proin id hendrerit tortor, et vehicula risus. Aliquam adipiscing magna iaculis nunc malesuada facilisis. Aliquam justo magna, accumsan sed malesuada eu, pretium sit amet arcu. Ut lobortis eu augue sed fringilla. Curabitur vestibulum est sed sollicitudin venenatis. Etiam vitae commodo ligula. Donec lacinia elit non tellus convallis porta. Vivamus eu nunc ac urna vestibulum laoreet. Ut tincidunt imperdiet lobortis. Donec bibendum nunc sed lorem adipiscing vehicula.</p>\r\n\r\n<p>Etiam condimentum elit non interdum tincidunt. Nullam sagittis, sapien sit amet iaculis ullamcorper, odio lorem accumsan leo, ac eleifend mauris libero vel nibh. Aliquam aliquam libero quis nisl mattis ornare. Cras dictum vitae mi eu blandit. Maecenas ac aliquam sem. Morbi dictum felis eu augue blandit molestie. Integer aliquam vitae enim ultricies vehicula. Duis suscipit, augue sed molestie vehicula, turpis mi rutrum arcu, at pulvinar sapien est at massa. Ut interdum purus at turpis pellentesque ultricies. Aenean luctus dapibus elit non pellentesque. Praesent posuere nisi a nibh suscipit, at sagittis mauris faucibus. Vestibulum vitae erat eros.</p>\r\n\r\n<p>Fusce tristique justo vel iaculis condimentum. Sed vitae arcu et risus commodo ornare. Fusce varius sapien laoreet, tincidunt leo non, dignissim nisl. Morbi ut luctus risus. Mauris laoreet in tellus sed luctus. Fusce eu augue eu mi sollicitudin ullamcorper. Cras consequat dui eu leo mattis varius.</p>\r\n\r\n<p>Praesent quam lorem, pharetra ac elementum quis, lobortis vel nunc. Nulla et cursus dolor, sit amet aliquam purus. Donec id sapien pulvinar, varius orci et, venenatis lectus. Nullam posuere vel lacus nec fermentum. Integer et rhoncus turpis. Cras blandit libero nisi, nec volutpat libero eleifend ac. Mauris lacinia lacus in convallis interdum. Donec laoreet magna fermentum, mattis orci sit amet, rutrum mauris.</p>\r\n\r\n<p>Nulla diam massa, pharetra vel vestibulum sed, egestas ut augue. Etiam tempor bibendum sagittis. Aliquam erat volutpat. Maecenas et eros et risus ullamcorper viverra ut eu dui. Mauris semper ut dui non ultrices. Integer a lectus a metus pellentesque congue eget ac orci. Donec fermentum mi quam, at sagittis eros rhoncus sed. Duis non laoreet odio. Nullam vulputate vehicula justo, eget blandit lacus feugiat sit amet.</p>\r\n\r\n<p>Integer ac quam vitae odio fringilla mattis vel sed ante. Etiam eu magna eleifend, mattis leo vel, porttitor nisl. Vestibulum ornare turpis at erat imperdiet, ac convallis felis lacinia. Nullam ultrices diam eu lorem ultrices, dapibus viverra felis ornare. Nam semper adipiscing urna non consequat. Morbi tempor laoreet felis, a pulvinar nunc commodo vel. Donec nec nulla ac turpis scelerisque interdum. Etiam tempor sem nisi. Sed lacinia id tellus sodales commodo. Quisque porttitor lobortis aliquam.</p>\r\n\r\n<p>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Ut id ultricies leo. Sed rhoncus, justo non convallis auctor, est augue condimentum lacus, ut porttitor leo tortor sit amet urna. Praesent ut ultrices nisi. Nunc tincidunt blandit cursus. Suspendisse potenti. Curabitur mollis metus nec odio vestibulum, sit amet iaculis sapien iaculis. Mauris non arcu at quam venenatis gravida in a ipsum. Vestibulum aliquet libero ut diam mattis fermentum. Donec dignissim dictum vulputate. Quisque commodo magna aliquet neque pretium dictum.</p>\r\n\r\n<p>Cras turpis mi, porta sed dignissim nec, posuere in velit. Duis eget magna at nisi auctor mattis. Donec eget pharetra velit, ac gravida neque. Integer fringilla turpis sed ipsum elementum facilisis. Morbi accumsan mi ut ligula vestibulum, sit amet cursus ligula imperdiet. Duis a euismod elit, non facilisis tortor. Mauris id ligula tellus. Sed ligula tellus, pharetra vel nisl a, placerat iaculis libero. Etiam pellentesque mauris libero, non dapibus nibh lobortis nec. Nam fermentum, urna non luctus semper, tellus leo rutrum risus, a tempor purus ligula non justo. Vivamus aliquam arcu a porttitor ultricies. Nunc sodales vel risus non mattis. Fusce id nisl dapibus, ornare dui at, vestibulum arcu.</p>', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras hendrerit.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras odio arcu, elementum sed aliquet non, consectetur non neque. Curabitur tempor pretium velit id consequat. Nunc felis dolor, eleifend ut pretium at, euismod id nunc.', '', 'lorem-ipsum-dolor-sit-amet-consectetur-adipiscing-elit-cras-hendrerit'),
(852, 'es', 'Startup company’s co-founder talks on his new product', '296', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Consetetur sadipscing elitr, sed diam nonumy eirmod tempor inviduntut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.justo duo dolores et ea rebum. Consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt orem ipsum dolor sit amet', '', 'Startup company’s co-founder talks on his new product', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Consetetur sadipscing elitr, sed diam nonumy eirmod tempor inviduntut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.justo duo dolores et ea rebum. Consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt orem ipsum dolor sit amet', '', 'startup-companys-co-founder-talks-on-his-new-product'),
(837, 'en', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras hendrerit.', '297', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras odio arcu, elementum sed aliquet non, consectetur non neque. Curabitur tempor pretium velit id consequat. Nunc felis dolor, eleifend ut pretium at, euismod id nunc.', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc aliquet molestie tortor ut luctus. Proin id hendrerit tortor, et vehicula risus. Aliquam adipiscing magna iaculis nunc malesuada facilisis. Aliquam justo magna, accumsan sed malesuada eu, pretium sit amet arcu. Ut lobortis eu augue sed fringilla. Curabitur vestibulum est sed sollicitudin venenatis. Etiam vitae commodo ligula. Donec lacinia elit non tellus convallis porta. Vivamus eu nunc ac urna vestibulum laoreet. Ut tincidunt imperdiet lobortis. Donec bibendum nunc sed lorem adipiscing vehicula.</p>\r\n\r\n<p>Etiam condimentum elit non interdum tincidunt. Nullam sagittis, sapien sit amet iaculis ullamcorper, odio lorem accumsan leo, ac eleifend mauris libero vel nibh. Aliquam aliquam libero quis nisl mattis ornare. Cras dictum vitae mi eu blandit. Maecenas ac aliquam sem. Morbi dictum felis eu augue blandit molestie. Integer aliquam vitae enim ultricies vehicula. Duis suscipit, augue sed molestie vehicula, turpis mi rutrum arcu, at pulvinar sapien est at massa. Ut interdum purus at turpis pellentesque ultricies. Aenean luctus dapibus elit non pellentesque. Praesent posuere nisi a nibh suscipit, at sagittis mauris faucibus. Vestibulum vitae erat eros.</p>\r\n\r\n<p>Fusce tristique justo vel iaculis condimentum. Sed vitae arcu et risus commodo ornare. Fusce varius sapien laoreet, tincidunt leo non, dignissim nisl. Morbi ut luctus risus. Mauris laoreet in tellus sed luctus. Fusce eu augue eu mi sollicitudin ullamcorper. Cras consequat dui eu leo mattis varius.</p>\r\n\r\n<p>Praesent quam lorem, pharetra ac elementum quis, lobortis vel nunc. Nulla et cursus dolor, sit amet aliquam purus. Donec id sapien pulvinar, varius orci et, venenatis lectus. Nullam posuere vel lacus nec fermentum. Integer et rhoncus turpis. Cras blandit libero nisi, nec volutpat libero eleifend ac. Mauris lacinia lacus in convallis interdum. Donec laoreet magna fermentum, mattis orci sit amet, rutrum mauris.</p>\r\n\r\n<p>Nulla diam massa, pharetra vel vestibulum sed, egestas ut augue. Etiam tempor bibendum sagittis. Aliquam erat volutpat. Maecenas et eros et risus ullamcorper viverra ut eu dui. Mauris semper ut dui non ultrices. Integer a lectus a metus pellentesque congue eget ac orci. Donec fermentum mi quam, at sagittis eros rhoncus sed. Duis non laoreet odio. Nullam vulputate vehicula justo, eget blandit lacus feugiat sit amet.</p>\r\n\r\n<p>Integer ac quam vitae odio fringilla mattis vel sed ante. Etiam eu magna eleifend, mattis leo vel, porttitor nisl. Vestibulum ornare turpis at erat imperdiet, ac convallis felis lacinia. Nullam ultrices diam eu lorem ultrices, dapibus viverra felis ornare. Nam semper adipiscing urna non consequat. Morbi tempor laoreet felis, a pulvinar nunc commodo vel. Donec nec nulla ac turpis scelerisque interdum. Etiam tempor sem nisi. Sed lacinia id tellus sodales commodo. Quisque porttitor lobortis aliquam.</p>\r\n\r\n<p>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Ut id ultricies leo. Sed rhoncus, justo non convallis auctor, est augue condimentum lacus, ut porttitor leo tortor sit amet urna. Praesent ut ultrices nisi. Nunc tincidunt blandit cursus. Suspendisse potenti. Curabitur mollis metus nec odio vestibulum, sit amet iaculis sapien iaculis. Mauris non arcu at quam venenatis gravida in a ipsum. Vestibulum aliquet libero ut diam mattis fermentum. Donec dignissim dictum vulputate. Quisque commodo magna aliquet neque pretium dictum.</p>\r\n\r\n<p>Cras turpis mi, porta sed dignissim nec, posuere in velit. Duis eget magna at nisi auctor mattis. Donec eget pharetra velit, ac gravida neque. Integer fringilla turpis sed ipsum elementum facilisis. Morbi accumsan mi ut ligula vestibulum, sit amet cursus ligula imperdiet. Duis a euismod elit, non facilisis tortor. Mauris id ligula tellus. Sed ligula tellus, pharetra vel nisl a, placerat iaculis libero. Etiam pellentesque mauris libero, non dapibus nibh lobortis nec. Nam fermentum, urna non luctus semper, tellus leo rutrum risus, a tempor purus ligula non justo. Vivamus aliquam arcu a porttitor ultricies. Nunc sodales vel risus non mattis. Fusce id nisl dapibus, ornare dui at, vestibulum arcu.</p>', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras hendrerit.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras odio arcu, elementum sed aliquet non, consectetur non neque. Curabitur tempor pretium velit id consequat. Nunc felis dolor, eleifend ut pretium at, euismod id nunc.', '', 'lorem-ipsum-dolor-sit-amet-consectetur-adipiscing-elit-cras-hendrerit1'),
(851, 'en', 'Pebble time steel is on track to ship in july', '295', '', '', 'Pebble time steel is on track to ship in july', '', '', 'pebble-time-steel-is-on-track-to-ship-in-july'),
(852, 'en', 'Startup company’s co-founder talks on his new product', '296', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Consetetur sadipscing elitr, sed diam nonumy eirmod tempor inviduntut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.justo duo dolores et ea rebum. Consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt orem ipsum dolor sit amet', '', 'Startup company’s co-founder talks on his new product', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Consetetur sadipscing elitr, sed diam nonumy eirmod tempor inviduntut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.justo duo dolores et ea rebum. Consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt orem ipsum dolor sit amet', '', 'startup-companys-co-founder-talks-on-his-new-product'),
(844, 'en', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras hendrerit.', '283', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras odio arcu, elementum sed aliquet non, consectetur non neque. Curabitur tempor pretium velit id consequat. Nunc felis dolor, eleifend ut pretium at, euismod id nunc.', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc aliquet molestie tortor ut luctus. Proin id hendrerit tortor, et vehicula risus. Aliquam adipiscing magna iaculis nunc malesuada facilisis. Aliquam justo magna, accumsan sed malesuada eu, pretium sit amet arcu. Ut lobortis eu augue sed fringilla. Curabitur vestibulum est sed sollicitudin venenatis. Etiam vitae commodo ligula. Donec lacinia elit non tellus convallis porta. Vivamus eu nunc ac urna vestibulum laoreet. Ut tincidunt imperdiet lobortis. Donec bibendum nunc sed lorem adipiscing vehicula.</p>\r\n\r\n<p>Etiam condimentum elit non interdum tincidunt. Nullam sagittis, sapien sit amet iaculis ullamcorper, odio lorem accumsan leo, ac eleifend mauris libero vel nibh. Aliquam aliquam libero quis nisl mattis ornare. Cras dictum vitae mi eu blandit. Maecenas ac aliquam sem. Morbi dictum felis eu augue blandit molestie. Integer aliquam vitae enim ultricies vehicula. Duis suscipit, augue sed molestie vehicula, turpis mi rutrum arcu, at pulvinar sapien est at massa. Ut interdum purus at turpis pellentesque ultricies. Aenean luctus dapibus elit non pellentesque. Praesent posuere nisi a nibh suscipit, at sagittis mauris faucibus. Vestibulum vitae erat eros.</p>\r\n\r\n<p>Fusce tristique justo vel iaculis condimentum. Sed vitae arcu et risus commodo ornare. Fusce varius sapien laoreet, tincidunt leo non, dignissim nisl. Morbi ut luctus risus. Mauris laoreet in tellus sed luctus. Fusce eu augue eu mi sollicitudin ullamcorper. Cras consequat dui eu leo mattis varius.</p>\r\n\r\n<p>Praesent quam lorem, pharetra ac elementum quis, lobortis vel nunc. Nulla et cursus dolor, sit amet aliquam purus. Donec id sapien pulvinar, varius orci et, venenatis lectus. Nullam posuere vel lacus nec fermentum. Integer et rhoncus turpis. Cras blandit libero nisi, nec volutpat libero eleifend ac. Mauris lacinia lacus in convallis interdum. Donec laoreet magna fermentum, mattis orci sit amet, rutrum mauris.</p>\r\n\r\n<p>Nulla diam massa, pharetra vel vestibulum sed, egestas ut augue. Etiam tempor bibendum sagittis. Aliquam erat volutpat. Maecenas et eros et risus ullamcorper viverra ut eu dui. Mauris semper ut dui non ultrices. Integer a lectus a metus pellentesque congue eget ac orci. Donec fermentum mi quam, at sagittis eros rhoncus sed. Duis non laoreet odio. Nullam vulputate vehicula justo, eget blandit lacus feugiat sit amet.</p>\r\n\r\n<p>Integer ac quam vitae odio fringilla mattis vel sed ante. Etiam eu magna eleifend, mattis leo vel, porttitor nisl. Vestibulum ornare turpis at erat imperdiet, ac convallis felis lacinia. Nullam ultrices diam eu lorem ultrices, dapibus viverra felis ornare. Nam semper adipiscing urna non consequat. Morbi tempor laoreet felis, a pulvinar nunc commodo vel. Donec nec nulla ac turpis scelerisque interdum. Etiam tempor sem nisi. Sed lacinia id tellus sodales commodo. Quisque porttitor lobortis aliquam.</p>\r\n\r\n<p>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Ut id ultricies leo. Sed rhoncus, justo non convallis auctor, est augue condimentum lacus, ut porttitor leo tortor sit amet urna. Praesent ut ultrices nisi. Nunc tincidunt blandit cursus. Suspendisse potenti. Curabitur mollis metus nec odio vestibulum, sit amet iaculis sapien iaculis. Mauris non arcu at quam venenatis gravida in a ipsum. Vestibulum aliquet libero ut diam mattis fermentum. Donec dignissim dictum vulputate. Quisque commodo magna aliquet neque pretium dictum.</p>\r\n\r\n<p>Cras turpis mi, porta sed dignissim nec, posuere in velit. Duis eget magna at nisi auctor mattis. Donec eget pharetra velit, ac gravida neque. Integer fringilla turpis sed ipsum elementum facilisis. Morbi accumsan mi ut ligula vestibulum, sit amet cursus ligula imperdiet. Duis a euismod elit, non facilisis tortor. Mauris id ligula tellus. Sed ligula tellus, pharetra vel nisl a, placerat iaculis libero. Etiam pellentesque mauris libero, non dapibus nibh lobortis nec. Nam fermentum, urna non luctus semper, tellus leo rutrum risus, a tempor purus ligula non justo. Vivamus aliquam arcu a porttitor ultricies. Nunc sodales vel risus non mattis. Fusce id nisl dapibus, ornare dui at, vestibulum arcu.</p>', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras hendrerit.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras odio arcu, elementum sed aliquet non, consectetur non neque. Curabitur tempor pretium velit id consequat. Nunc felis dolor, eleifend ut pretium at, euismod id nunc.', '', 'lorem-ipsum-dolor-sit-amet-consectetur-adipiscing-elit-cras-hendrerit10'),
(844, 'es', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras hendrerit.', '283', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras odio arcu, elementum sed aliquet non, consectetur non neque. Curabitur tempor pretium velit id consequat. Nunc felis dolor, eleifend ut pretium at, euismod id nunc.', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc aliquet molestie tortor ut luctus. Proin id hendrerit tortor, et vehicula risus. Aliquam adipiscing magna iaculis nunc malesuada facilisis. Aliquam justo magna, accumsan sed malesuada eu, pretium sit amet arcu. Ut lobortis eu augue sed fringilla. Curabitur vestibulum est sed sollicitudin venenatis. Etiam vitae commodo ligula. Donec lacinia elit non tellus convallis porta. Vivamus eu nunc ac urna vestibulum laoreet. Ut tincidunt imperdiet lobortis. Donec bibendum nunc sed lorem adipiscing vehicula.</p>\r\n\r\n<p>Etiam condimentum elit non interdum tincidunt. Nullam sagittis, sapien sit amet iaculis ullamcorper, odio lorem accumsan leo, ac eleifend mauris libero vel nibh. Aliquam aliquam libero quis nisl mattis ornare. Cras dictum vitae mi eu blandit. Maecenas ac aliquam sem. Morbi dictum felis eu augue blandit molestie. Integer aliquam vitae enim ultricies vehicula. Duis suscipit, augue sed molestie vehicula, turpis mi rutrum arcu, at pulvinar sapien est at massa. Ut interdum purus at turpis pellentesque ultricies. Aenean luctus dapibus elit non pellentesque. Praesent posuere nisi a nibh suscipit, at sagittis mauris faucibus. Vestibulum vitae erat eros.</p>\r\n\r\n<p>Fusce tristique justo vel iaculis condimentum. Sed vitae arcu et risus commodo ornare. Fusce varius sapien laoreet, tincidunt leo non, dignissim nisl. Morbi ut luctus risus. Mauris laoreet in tellus sed luctus. Fusce eu augue eu mi sollicitudin ullamcorper. Cras consequat dui eu leo mattis varius.</p>\r\n\r\n<p>Praesent quam lorem, pharetra ac elementum quis, lobortis vel nunc. Nulla et cursus dolor, sit amet aliquam purus. Donec id sapien pulvinar, varius orci et, venenatis lectus. Nullam posuere vel lacus nec fermentum. Integer et rhoncus turpis. Cras blandit libero nisi, nec volutpat libero eleifend ac. Mauris lacinia lacus in convallis interdum. Donec laoreet magna fermentum, mattis orci sit amet, rutrum mauris.</p>\r\n\r\n<p>Nulla diam massa, pharetra vel vestibulum sed, egestas ut augue. Etiam tempor bibendum sagittis. Aliquam erat volutpat. Maecenas et eros et risus ullamcorper viverra ut eu dui. Mauris semper ut dui non ultrices. Integer a lectus a metus pellentesque congue eget ac orci. Donec fermentum mi quam, at sagittis eros rhoncus sed. Duis non laoreet odio. Nullam vulputate vehicula justo, eget blandit lacus feugiat sit amet.</p>\r\n\r\n<p>Integer ac quam vitae odio fringilla mattis vel sed ante. Etiam eu magna eleifend, mattis leo vel, porttitor nisl. Vestibulum ornare turpis at erat imperdiet, ac convallis felis lacinia. Nullam ultrices diam eu lorem ultrices, dapibus viverra felis ornare. Nam semper adipiscing urna non consequat. Morbi tempor laoreet felis, a pulvinar nunc commodo vel. Donec nec nulla ac turpis scelerisque interdum. Etiam tempor sem nisi. Sed lacinia id tellus sodales commodo. Quisque porttitor lobortis aliquam.</p>\r\n\r\n<p>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Ut id ultricies leo. Sed rhoncus, justo non convallis auctor, est augue condimentum lacus, ut porttitor leo tortor sit amet urna. Praesent ut ultrices nisi. Nunc tincidunt blandit cursus. Suspendisse potenti. Curabitur mollis metus nec odio vestibulum, sit amet iaculis sapien iaculis. Mauris non arcu at quam venenatis gravida in a ipsum. Vestibulum aliquet libero ut diam mattis fermentum. Donec dignissim dictum vulputate. Quisque commodo magna aliquet neque pretium dictum.</p>\r\n\r\n<p>Cras turpis mi, porta sed dignissim nec, posuere in velit. Duis eget magna at nisi auctor mattis. Donec eget pharetra velit, ac gravida neque. Integer fringilla turpis sed ipsum elementum facilisis. Morbi accumsan mi ut ligula vestibulum, sit amet cursus ligula imperdiet. Duis a euismod elit, non facilisis tortor. Mauris id ligula tellus. Sed ligula tellus, pharetra vel nisl a, placerat iaculis libero. Etiam pellentesque mauris libero, non dapibus nibh lobortis nec. Nam fermentum, urna non luctus semper, tellus leo rutrum risus, a tempor purus ligula non justo. Vivamus aliquam arcu a porttitor ultricies. Nunc sodales vel risus non mattis. Fusce id nisl dapibus, ornare dui at, vestibulum arcu.</p>', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras hendrerit.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras odio arcu, elementum sed aliquet non, consectetur non neque. Curabitur tempor pretium velit id consequat. Nunc felis dolor, eleifend ut pretium at, euismod id nunc.', '', 'lorem-ipsum-dolor-sit-amet-consectetur-adipiscing-elit-cras-hendrerit13');

-- --------------------------------------------------------

--
-- Table structure for table `s_news_read`
--

CREATE TABLE `s_news_read` (
  `newsid` int(11) NOT NULL DEFAULT '0',
  `read_date` int(11) NOT NULL DEFAULT '0',
  `total_views` int(11) NOT NULL DEFAULT '1',
  `month_views` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_news_read`
--

INSERT INTO `s_news_read` (`newsid`, `read_date`, `total_views`, `month_views`) VALUES
(844, 1527495940, 5, 5),
(839, 1527496089, 2, 2),
(850, 1527496238, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `s_notifications`
--

CREATE TABLE `s_notifications` (
  `nid` int(11) NOT NULL,
  `to_member_id` int(11) NOT NULL DEFAULT '0',
  `from_member_id` int(11) NOT NULL DEFAULT '0',
  `n_subject` text NOT NULL,
  `n_text` longtext NOT NULL,
  `n_date` int(11) NOT NULL DEFAULT '0',
  `read_date` int(11) NOT NULL DEFAULT '0',
  `send_mail` tinyint(1) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `s_options`
--

CREATE TABLE `s_options` (
  `id` int(11) NOT NULL,
  `opt_key` varchar(255) NOT NULL DEFAULT '',
  `value` longtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_options`
--

INSERT INTO `s_options` (`id`, `opt_key`, `value`) VALUES
(1, 'charset', 'utf-8'),
(22, 'contacts_mail_user', 'ZGVtb3NlbmRAbWljcm9waHAubmV0'),
(23, 'contacts_mail_pass', 'dGg0cUQ5MWtoOHhI'),
(6, 'lang_langs', 'ru,uz'),
(7, 'lang_showlangs', 'ru,uz'),
(8, 'lang_mainlang', 'ru'),
(24, 'contacts_mail_pop3', 'bWljcm9waHAubmV0'),
(11, 'lang_adminmainlang', 'ru'),
(21, 'contacts_mail_from', 'ZGVtb3NlbmRAbWljcm9waHAubmV0'),
(20, 'contacts_mail_to', 'bWljcm9waHByYXNoYWRAZ21haWwuY29t'),
(19, 'slider', '1'),
(25, 'contacts_mail_smtp', 'bWljcm9waHAubmV0'),
(26, 'contacts_mail_host', 'bWljcm9waHAubmV0'),
(27, 'watermark', '1'),
(28, 'watermark_name', 'MicroPHP'),
(29, 'watermark_color', '#bf3030'),
(30, 'news_foot_limit', '3'),
(31, 'news_index_limit', '4'),
(32, 'news_page_limit', '10'),
(33, 'news_block_seperator', '3'),
(34, 'news_date_format', 'd.m.Y'),
(35, 'parallax', '0');

-- --------------------------------------------------------

--
-- Table structure for table `s_optionslocalizations`
--

CREATE TABLE `s_optionslocalizations` (
  `lang` char(2) NOT NULL,
  `opt_key` varchar(255) NOT NULL DEFAULT '',
  `value` longtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_optionslocalizations`
--

INSERT INTO `s_optionslocalizations` (`lang`, `opt_key`, `value`) VALUES
('en', 'site_name', 'Sharebm'),
('es', 'site_name', 'Sharebm'),
('en', 'meta_description', ''),
('es', 'meta_description', ''),
('en', 'site_title', 'Sharebm'),
('es', 'site_title', 'Sharebm'),
('es', 'meta_keywords', ''),
('en', 'meta_keywords', ''),
('en', 'meta_email', ''),
('es', 'meta_email', ''),
('en', 'meta_author', ''),
('es', 'meta_author', ''),
('en', 'site_copyright', '[YEAR] Sharebm'),
('es', 'site_copyright', '[YEAR] Sharebm'),
('ru', 'site_name', ''),
('ru', 'site_title', ''),
('ru', 'meta_keywords', ''),
('ru', 'meta_description', ''),
('ru', 'meta_email', ''),
('ru', 'meta_author', ''),
('ru', 'site_copyright', ''),
('uz', 'site_name', ''),
('uz', 'site_title', ''),
('uz', 'meta_keywords', ''),
('uz', 'meta_description', ''),
('uz', 'meta_email', ''),
('uz', 'meta_author', ''),
('uz', 'site_copyright', '');

-- --------------------------------------------------------

--
-- Table structure for table `s_questionanswers`
--

CREATE TABLE `s_questionanswers` (
  `answerid` int(11) NOT NULL,
  `questionid` int(11) NOT NULL DEFAULT '0',
  `variant` varchar(64) NOT NULL,
  `position` int(11) NOT NULL DEFAULT '0',
  `answersdate` varchar(255) NOT NULL DEFAULT '',
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_questionanswers`
--

INSERT INTO `s_questionanswers` (`answerid`, `questionid`, `variant`, `position`, `answersdate`, `active`) VALUES
(1, 4, 'A', 0, '1579719541', 1),
(2, 4, 'B', 0, '1579719558', 1),
(3, 4, 'C', 0, '1579719574', 1),
(4, 1, 'A', 0, '1579719965', 1),
(5, 1, 'B', 0, '1579720009', 1),
(6, 1, 'C', 0, '1579720019', 1);

-- --------------------------------------------------------

--
-- Table structure for table `s_questionanswerslocalizations`
--

CREATE TABLE `s_questionanswerslocalizations` (
  `answerid` int(11) NOT NULL DEFAULT '0',
  `lang` varchar(5) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_questionanswerslocalizations`
--

INSERT INTO `s_questionanswerslocalizations` (`answerid`, `lang`, `name`) VALUES
(1, 'ru', 'Люди не созданы друг для друга'),
(1, 'uz', 'Люди не созданы друг для друга'),
(2, 'ru', 'aaaaaaaaa'),
(3, 'ru', 'Люди раздражают друг друга'),
(3, 'uz', 'Люди раздражают друг друга'),
(4, 'ru', 'Слезы'),
(4, 'uz', 'Слезы'),
(5, 'ru', 'Смех'),
(5, 'uz', 'Смех'),
(6, 'ru', 'Объятия'),
(6, 'uz', 'Объятия');

-- --------------------------------------------------------

--
-- Table structure for table `s_questions`
--

CREATE TABLE `s_questions` (
  `questionid` int(11) NOT NULL,
  `testid` int(11) NOT NULL DEFAULT '0',
  `answers_limit` int(11) NOT NULL DEFAULT '5',
  `header` int(11) NOT NULL,
  `questionsdate` varchar(255) NOT NULL DEFAULT '',
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_questions`
--

INSERT INTO `s_questions` (`questionid`, `testid`, `answers_limit`, `header`, `questionsdate`, `active`) VALUES
(1, 1, 5, 0, '1579636845', 1),
(2, 1, 5, 0, '1579636861', 0),
(3, 1, 5, 0, '1579636872', 0),
(4, 1, 5, 0, '1579636888', 1);

-- --------------------------------------------------------

--
-- Table structure for table `s_questionscategories`
--

CREATE TABLE `s_questionscategories` (
  `catid` int(11) NOT NULL,
  `position` int(5) NOT NULL DEFAULT '1',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `index` int(11) NOT NULL DEFAULT '0',
  `image` int(11) NOT NULL DEFAULT '0',
  `level` varchar(64) CHARACTER SET utf32 NOT NULL DEFAULT '',
  `icon` varchar(256) CHARACTER SET utf32 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_questionscategories`
--

INSERT INTO `s_questionscategories` (`catid`, `position`, `active`, `index`, `image`, `level`, `icon`) VALUES
(2, 1, 1, 1, 317, '1', 'icon-heart-red'),
(3, 2, 1, 1, 312, '2', 'icon-activity-red'),
(4, 3, 1, 1, 310, '2', 'icon-makeup-red'),
(5, 4, 1, 1, 308, '3', 'icon-brain-box-red');

-- --------------------------------------------------------

--
-- Table structure for table `s_questionscategorylocalizations`
--

CREATE TABLE `s_questionscategorylocalizations` (
  `catid` int(11) NOT NULL DEFAULT '0',
  `lang` varchar(5) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `header` varchar(255) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_questionscategorylocalizations`
--

INSERT INTO `s_questionscategorylocalizations` (`catid`, `lang`, `name`, `header`) VALUES
(2, 'ru', 'Sevgi', '<p>Sevgi iki qəlbi bir edir</p>'),
(3, 'ru', 'Sağlamlıq', '<p>Sağlamlıq sərvətdir - bu xəzinəni etibarlı saxlayın</p>'),
(4, 'ru', 'Gözəllik', '<p>G&ouml;zəllik - g&uuml;cd&uuml;r</p>'),
(5, 'ru', 'Psixoloqiya', '<p>&Ouml;z&uuml;n&uuml;z&uuml;n daha m&uuml;kəmməl versiyası olun</p>'),
(3, 'uz', 'Sağlamlıq', ''),
(4, 'uz', 'Gözəllik', ''),
(5, 'uz', 'Psixoloqiya', '');

-- --------------------------------------------------------

--
-- Table structure for table `s_questionslocalizations`
--

CREATE TABLE `s_questionslocalizations` (
  `questionid` int(11) NOT NULL DEFAULT '0',
  `lang` varchar(5) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_questionslocalizations`
--

INSERT INTO `s_questionslocalizations` (`questionid`, `lang`, `name`) VALUES
(1, 'ru', 'Самая романтическая концовка фильма для вас:'),
(1, 'uz', 'Самая романтическая концовка фильма для вас:'),
(2, 'ru', 'Настоящая любовь - это, когда'),
(2, 'uz', 'Настоящая любовь - это, когда'),
(3, 'ru', 'Любовь на одну ночь - это то, что'),
(3, 'uz', 'Любовь на одну ночь - это то, что'),
(4, 'ru', 'Большинство разрывов случаются потому, что'),
(4, 'uz', 'Большинство разрывов случаются потому, что');

-- --------------------------------------------------------

--
-- Table structure for table `s_search`
--

CREATE TABLE `s_search` (
  `name` varchar(255) DEFAULT NULL,
  `text` longtext,
  `slug` varchar(255) DEFAULT NULL,
  `lang` varchar(5) DEFAULT NULL,
  `date` varchar(255) DEFAULT NULL,
  `id` bigint(20) DEFAULT NULL,
  `catid` bigint(20) DEFAULT NULL,
  `pg` varchar(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `s_slider`
--

CREATE TABLE `s_slider` (
  `sid` int(11) UNSIGNED NOT NULL,
  `slide_img` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `position` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_slider`
--

INSERT INTO `s_slider` (`sid`, `slide_img`, `position`, `active`) VALUES
(24, 256, 2, 1),
(23, 255, 3, 1),
(25, 315, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `s_sliderlocalizations`
--

CREATE TABLE `s_sliderlocalizations` (
  `sid` int(11) NOT NULL DEFAULT '0',
  `lang` varchar(5) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `more_txt` text,
  `text` text NOT NULL,
  `embed_code` text NOT NULL,
  `url` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_sliderlocalizations`
--

INSERT INTO `s_sliderlocalizations` (`sid`, `lang`, `name`, `more_txt`, `text`, `embed_code`, `url`) VALUES
(24, 'es', 'PASAR SUS VACACIONES SUEÑO!', 'Aprende más', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima quo. Aenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non.</p>', '', 'http://mcms.ws'),
(24, 'en', 'SPEND YOUR DREAM HOLIDAY!', 'Learn More', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima quo. Aenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non.</p>', '', 'http://mcms.ws'),
(23, 'es', '¡LA BIENVENIDA A NOSOTROS!', 'Aprende más', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima quo. Aenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non.</p>', '', 'http://www.microphp.com/es/'),
(23, 'en', 'WELCOME TO US!', 'Learn More', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima quo. Aenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non.</p>', '', 'http://www.microphp.com'),
(25, 'en', 'mCMS', 'more', '<p>Multilingual Content Management System</p>', '', 'https://codecanyon.net/item/mcms-multilingual-content-management-system/18293619'),
(25, 'es', 'mCMS', 'more', '<p>Multilingual Content Management System</p>', '', 'https://codecanyon.net/item/mcms-multilingual-content-management-system/18293619');

-- --------------------------------------------------------

--
-- Table structure for table `s_subscribe`
--

CREATE TABLE `s_subscribe` (
  `sid` int(11) NOT NULL,
  `add_date` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(45) NOT NULL DEFAULT '',
  `lang` varchar(5) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_subscribe`
--

INSERT INTO `s_subscribe` (`sid`, `add_date`, `name`, `email`, `phone`, `lang`, `active`) VALUES
(9, 1475587175, 'Rashad', 'rashad@aliev.info', '6482737', 'en', 1);

-- --------------------------------------------------------

--
-- Table structure for table `s_subscribe_broadcast`
--

CREATE TABLE `s_subscribe_broadcast` (
  `bid` int(11) NOT NULL,
  `tid` int(11) NOT NULL DEFAULT '0',
  `startdate` int(11) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `finished` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_subscribe_broadcast`
--

INSERT INTO `s_subscribe_broadcast` (`bid`, `tid`, `startdate`, `active`, `finished`) VALUES
(11, 5, 1477162800, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `s_subscribe_broadcastlocalizations`
--

CREATE TABLE `s_subscribe_broadcastlocalizations` (
  `bid` int(11) NOT NULL DEFAULT '0',
  `lang` varchar(5) NOT NULL DEFAULT '',
  `text` longtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_subscribe_broadcastlocalizations`
--

INSERT INTO `s_subscribe_broadcastlocalizations` (`bid`, `lang`, `text`) VALUES
(11, 'en', 'blabla'),
(11, 'es', '');

-- --------------------------------------------------------

--
-- Table structure for table `s_subscribe_broadcast_mails`
--

CREATE TABLE `s_subscribe_broadcast_mails` (
  `bid` int(11) NOT NULL DEFAULT '0',
  `sid` int(11) NOT NULL DEFAULT '0',
  `sendtime` int(11) NOT NULL DEFAULT '0',
  `sended` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_subscribe_broadcast_mails`
--

INSERT INTO `s_subscribe_broadcast_mails` (`bid`, `sid`, `sendtime`, `sended`) VALUES
(11, 9, 1499495914, 1);

-- --------------------------------------------------------

--
-- Table structure for table `s_subscribe_templates`
--

CREATE TABLE `s_subscribe_templates` (
  `tid` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_subscribe_templates`
--

INSERT INTO `s_subscribe_templates` (`tid`, `active`) VALUES
(5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `s_subscribe_templateslocalizations`
--

CREATE TABLE `s_subscribe_templateslocalizations` (
  `tid` int(11) NOT NULL DEFAULT '0',
  `lang` varchar(5) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `template` longtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_subscribe_templateslocalizations`
--

INSERT INTO `s_subscribe_templateslocalizations` (`tid`, `lang`, `name`, `template`) VALUES
(5, 'ru', 'test mail - ru', 'Test mail template in Russian\r\n[TEXT]'),
(5, 'en', 'test mail - en', 'Test mail template in English\r\n[TEXT]'),
(5, 'az', 'test mail - az', 'Test mail template in Azeri\r\n[TEXT]'),
(5, 'es', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `s_tests`
--

CREATE TABLE `s_tests` (
  `testid` int(11) NOT NULL,
  `catid` int(11) NOT NULL DEFAULT '0',
  `position` int(5) NOT NULL DEFAULT '1',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `index` int(11) NOT NULL DEFAULT '0',
  `image` int(11) NOT NULL DEFAULT '0',
  `testdate` varchar(64) CHARACTER SET utf32 NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_tests`
--

INSERT INTO `s_tests` (`testid`, `catid`, `position`, `active`, `index`, `image`, `testdate`) VALUES
(1, 2, 1, 1, 1, 310, '1579636816'),
(2, 2, 1, 1, 1, 302, '1579638903'),
(3, 2, 1, 1, 1, 309, '1579638919');

-- --------------------------------------------------------

--
-- Table structure for table `s_testslocalizations`
--

CREATE TABLE `s_testslocalizations` (
  `testid` int(11) NOT NULL DEFAULT '0',
  `lang` varchar(5) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `header` varchar(255) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_testslocalizations`
--

INSERT INTO `s_testslocalizations` (`testid`, `lang`, `name`, `header`) VALUES
(1, 'ru', 'Где Вы найдете свою любовь?', ''),
(1, 'uz', 'Где Вы найдете свою любовь?', ''),
(2, 'ru', 'Влюблены ли вы?', ''),
(2, 'uz', 'Влюблены ли вы?', ''),
(3, 'ru', 'Готовы ли вы к строению отношений на расстоянии?', ''),
(3, 'uz', 'Готовы ли вы к строению отношений на расстоянии?', '');

-- --------------------------------------------------------

--
-- Table structure for table `s_tests_correct_answers`
--

CREATE TABLE `s_tests_correct_answers` (
  `answerid` int(11) NOT NULL,
  `testid` int(11) NOT NULL DEFAULT '0',
  `variant` varchar(32) NOT NULL,
  `add_date` varchar(255) NOT NULL DEFAULT '',
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_tests_correct_answers`
--

INSERT INTO `s_tests_correct_answers` (`answerid`, `testid`, `variant`, `add_date`, `active`) VALUES
(1, 1, 'A', '1580158305', 1),
(3, 1, 'asd', '1580158667', 1);

-- --------------------------------------------------------

--
-- Table structure for table `s_tests_correct_answerslocalizations`
--

CREATE TABLE `s_tests_correct_answerslocalizations` (
  `answerid` int(11) NOT NULL DEFAULT '0',
  `lang` varchar(5) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_tests_correct_answerslocalizations`
--

INSERT INTO `s_tests_correct_answerslocalizations` (`answerid`, `lang`, `name`) VALUES
(1, 'ru', 'dsadad'),
(1, 'uz', 'dsadad'),
(3, 'ru', 'dasd');

-- --------------------------------------------------------

--
-- Table structure for table `s_users`
--

CREATE TABLE `s_users` (
  `userid` int(11) NOT NULL,
  `username` varchar(255) NOT NULL DEFAULT '',
  `usertype` int(2) NOT NULL DEFAULT '3',
  `password` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) DEFAULT NULL,
  `surname` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(30) NOT NULL,
  `mobile` varchar(30) NOT NULL,
  `updated` int(42) NOT NULL DEFAULT '0',
  `permissions` text NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_users`
--

INSERT INTO `s_users` (`userid`, `username`, `usertype`, `password`, `name`, `surname`, `email`, `phone`, `mobile`, `updated`, `permissions`, `active`) VALUES
(1, 'admin', 1, '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', 'MicroPHP', '', 'info@microphp.com', '', '', 1529310435, '0', 1),
(9, 'test', 3, '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08', 'test', '', 'test@test.az', '', '', 1501233815, ';5::01001;9::00101;999999::11111;10::00000;50::00011;7::00001;100::00001;101::01011;550::00111;', 1),
(10, 'user1', 2, '0a041b9462caa4a31bac3567e0b6e6fd9100787db2ab433d96f6d178cabfce90', 'user1', '', 'user1@user1.az', '', '', 1530071121, ';5::00000;9::00111;999999::00111;10::00111;50::00111;7::00111;100::00111;101::00111;550::00111;', 1),
(11, 'user2', 3, '6025d18fe48abd45168528f18a82e265dd98d421a7084aa09f61b341703901a3', 'user2', '', 'user2@user2.az', '', '', 1463604738, ';5::00101;9::00101;999999::00101;10::00101;50::00101;7::00101;100::00101;101::00101;550::00101;1000030::00101;', 1),
(12, 'user3', 3, '5860faf02b6bc6222ba5aca523560f0e364ccd8b67bee486fe8bf7c01d492ccb', 'user3', '', 'user3@user3.az', '', '', 1463604756, ';5::00011;9::00011;999999::11111;10::00011;50::00011;7::00011;100::00011;101::00011;550::00011;1000054::00000;999998::00000;', 1),
(13, 'user4', 3, '5269ef980de47819ba3d14340f4665262c41e933dc92c1a27dd5d01b047ac80e', 'user4', '', 'user4@user4.com', '', '', 1530071174, ';5::00001;9::00001;999999::00001;10::00001;50::00001;7::00001;100::00001;101::00001;550::00001;1000030::00001;', 1);

-- --------------------------------------------------------

--
-- Table structure for table `s_user_follows`
--

CREATE TABLE `s_user_follows` (
  `from_member_id` int(11) NOT NULL,
  `to_member_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `add_date` varchar(128) NOT NULL,
  `modify_date` varchar(128) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `s_verify_codes`
--

CREATE TABLE `s_verify_codes` (
  `type` enum('email','mobile') NOT NULL,
  `obj` varchar(512) NOT NULL,
  `code` varchar(64) NOT NULL,
  `add_date` varchar(128) NOT NULL,
  `modify_date` varchar(128) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_verify_codes`
--

INSERT INTO `s_verify_codes` (`type`, `obj`, `code`, `add_date`, `modify_date`) VALUES
('email', 'abbasov.azik@gmail.com', '128378', '1569104396', ''),
('email', 'dasd', '372483', '1569106968', ''),
('email', 'dasd', '423477', '1569106974', ''),
('email', 'ds', '180737', '1569106985', ''),
('email', 'da', '227710', '1569355597', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `s_blocks`
--
ALTER TABLE `s_blocks`
  ADD PRIMARY KEY (`blocksid`),
  ADD KEY `catid` (`catid`),
  ADD KEY `active` (`active`);

--
-- Indexes for table `s_blockscategories`
--
ALTER TABLE `s_blockscategories`
  ADD PRIMARY KEY (`catid`),
  ADD KEY `active` (`active`);

--
-- Indexes for table `s_blockscategorylocalizations`
--
ALTER TABLE `s_blockscategorylocalizations`
  ADD UNIQUE KEY `catid_lang` (`catid`,`lang`) USING BTREE,
  ADD KEY `catid` (`catid`),
  ADD KEY `lang` (`lang`);

--
-- Indexes for table `s_blockslocalizations`
--
ALTER TABLE `s_blockslocalizations`
  ADD UNIQUE KEY `blocksid_lang` (`blocksid`,`lang`) USING BTREE,
  ADD KEY `blocksid` (`blocksid`),
  ADD KEY `lang` (`lang`);

--
-- Indexes for table `s_files`
--
ALTER TABLE `s_files`
  ADD PRIMARY KEY (`fileid`),
  ADD KEY `category` (`category`),
  ADD KEY `extension` (`extension`);

--
-- Indexes for table `s_files_image_sizes`
--
ALTER TABLE `s_files_image_sizes`
  ADD KEY `imgid` (`imgid`),
  ADD KEY `size` (`size`);

--
-- Indexes for table `s_gallery`
--
ALTER TABLE `s_gallery`
  ADD PRIMARY KEY (`gid`),
  ADD KEY `active` (`active`);

--
-- Indexes for table `s_gallerylocalizations`
--
ALTER TABLE `s_gallerylocalizations`
  ADD UNIQUE KEY `gid_lang` (`gid`,`lang`) USING BTREE,
  ADD KEY `gid` (`gid`),
  ADD KEY `lang` (`lang`);

--
-- Indexes for table `s_gallery_images`
--
ALTER TABLE `s_gallery_images`
  ADD PRIMARY KEY (`imgid`),
  ADD KEY `gid` (`gid`);

--
-- Indexes for table `s_logs`
--
ALTER TABLE `s_logs`
  ADD PRIMARY KEY (`logsid`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `s_media_likes`
--
ALTER TABLE `s_media_likes`
  ADD PRIMARY KEY (`media_id`),
  ADD KEY `member_id` (`member_id`);

--
-- Indexes for table `s_members`
--
ALTER TABLE `s_members`
  ADD PRIMARY KEY (`member_id`),
  ADD UNIQUE KEY `m_mail` (`m_mail`);

--
-- Indexes for table `s_menu`
--
ALTER TABLE `s_menu`
  ADD PRIMARY KEY (`mid`),
  ADD KEY `parentid` (`parentid`),
  ADD KEY `indexed` (`indexed`),
  ADD KEY `visible` (`visible`),
  ADD KEY `newwindow` (`newwindow`),
  ADD KEY `template` (`template`),
  ADD KEY `menutype` (`menutype`);

--
-- Indexes for table `s_menulocalizations`
--
ALTER TABLE `s_menulocalizations`
  ADD UNIQUE KEY `mid_lang` (`mid`,`lang`) USING BTREE,
  ADD KEY `lang` (`lang`),
  ADD KEY `mid` (`mid`);

--
-- Indexes for table `s_message_sends`
--
ALTER TABLE `s_message_sends`
  ADD PRIMARY KEY (`msgid`),
  ADD KEY `sended` (`sended`);

--
-- Indexes for table `s_modulelocalizations`
--
ALTER TABLE `s_modulelocalizations`
  ADD UNIQUE KEY `moduleid_lang` (`moduleid`,`lang`) USING BTREE,
  ADD KEY `moduleid` (`moduleid`),
  ADD KEY `lang` (`lang`);

--
-- Indexes for table `s_modules`
--
ALTER TABLE `s_modules`
  ADD PRIMARY KEY (`moduleid`),
  ADD KEY `have_view` (`have_view`),
  ADD KEY `have_cnfg` (`have_cnfg`),
  ADD KEY `active` (`active`);

--
-- Indexes for table `s_news`
--
ALTER TABLE `s_news`
  ADD PRIMARY KEY (`newsid`),
  ADD KEY `catid` (`catid`),
  ADD KEY `active` (`active`),
  ADD KEY `index` (`index`),
  ADD KEY `template` (`template`),
  ADD KEY `img_show` (`img_show`);

--
-- Indexes for table `s_newscategories`
--
ALTER TABLE `s_newscategories`
  ADD PRIMARY KEY (`catid`),
  ADD KEY `active` (`active`),
  ADD KEY `index` (`index`),
  ADD KEY `event` (`event`);

--
-- Indexes for table `s_newscategorylocalizations`
--
ALTER TABLE `s_newscategorylocalizations`
  ADD UNIQUE KEY `catid_lang` (`catid`,`lang`) USING BTREE,
  ADD KEY `catid` (`catid`),
  ADD KEY `lang` (`lang`);

--
-- Indexes for table `s_newslocalizations`
--
ALTER TABLE `s_newslocalizations`
  ADD UNIQUE KEY `newsid_lang` (`newsid`,`lang`) USING BTREE,
  ADD KEY `newsid` (`newsid`),
  ADD KEY `lang` (`lang`);

--
-- Indexes for table `s_news_read`
--
ALTER TABLE `s_news_read`
  ADD KEY `newsid` (`newsid`);

--
-- Indexes for table `s_notifications`
--
ALTER TABLE `s_notifications`
  ADD PRIMARY KEY (`nid`);

--
-- Indexes for table `s_options`
--
ALTER TABLE `s_options`
  ADD PRIMARY KEY (`id`),
  ADD KEY `opt_key` (`opt_key`);

--
-- Indexes for table `s_optionslocalizations`
--
ALTER TABLE `s_optionslocalizations`
  ADD KEY `lang` (`lang`),
  ADD KEY `opt_key` (`opt_key`);

--
-- Indexes for table `s_questionanswers`
--
ALTER TABLE `s_questionanswers`
  ADD PRIMARY KEY (`answerid`);

--
-- Indexes for table `s_questionanswerslocalizations`
--
ALTER TABLE `s_questionanswerslocalizations`
  ADD UNIQUE KEY `answerid_lang` (`answerid`,`lang`) USING BTREE,
  ADD KEY `answerid` (`answerid`),
  ADD KEY `lang` (`lang`);

--
-- Indexes for table `s_questions`
--
ALTER TABLE `s_questions`
  ADD PRIMARY KEY (`questionid`);

--
-- Indexes for table `s_questionscategories`
--
ALTER TABLE `s_questionscategories`
  ADD PRIMARY KEY (`catid`);

--
-- Indexes for table `s_questionslocalizations`
--
ALTER TABLE `s_questionslocalizations`
  ADD UNIQUE KEY `questionid_lang` (`questionid`,`lang`) USING BTREE,
  ADD KEY `questionid` (`questionid`),
  ADD KEY `lang` (`lang`);

--
-- Indexes for table `s_slider`
--
ALTER TABLE `s_slider`
  ADD PRIMARY KEY (`sid`),
  ADD KEY `active` (`active`);

--
-- Indexes for table `s_sliderlocalizations`
--
ALTER TABLE `s_sliderlocalizations`
  ADD UNIQUE KEY `sid_lang` (`sid`,`lang`) USING BTREE,
  ADD KEY `sid` (`sid`),
  ADD KEY `lang` (`lang`);

--
-- Indexes for table `s_subscribe`
--
ALTER TABLE `s_subscribe`
  ADD PRIMARY KEY (`sid`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `s_subscribe_broadcast`
--
ALTER TABLE `s_subscribe_broadcast`
  ADD PRIMARY KEY (`bid`);

--
-- Indexes for table `s_subscribe_broadcastlocalizations`
--
ALTER TABLE `s_subscribe_broadcastlocalizations`
  ADD UNIQUE KEY `bid_lang` (`bid`,`lang`) USING BTREE,
  ADD KEY `bid` (`bid`);

--
-- Indexes for table `s_subscribe_templates`
--
ALTER TABLE `s_subscribe_templates`
  ADD PRIMARY KEY (`tid`);

--
-- Indexes for table `s_subscribe_templateslocalizations`
--
ALTER TABLE `s_subscribe_templateslocalizations`
  ADD UNIQUE KEY `tid_lang` (`tid`,`lang`) USING BTREE,
  ADD KEY `tid` (`tid`) USING BTREE;

--
-- Indexes for table `s_tests`
--
ALTER TABLE `s_tests`
  ADD PRIMARY KEY (`testid`);

--
-- Indexes for table `s_tests_correct_answers`
--
ALTER TABLE `s_tests_correct_answers`
  ADD PRIMARY KEY (`answerid`);

--
-- Indexes for table `s_users`
--
ALTER TABLE `s_users`
  ADD PRIMARY KEY (`userid`),
  ADD UNIQUE KEY `username` (`username`),
  ADD KEY `usertype` (`usertype`),
  ADD KEY `active` (`active`);

--
-- Indexes for table `s_user_follows`
--
ALTER TABLE `s_user_follows`
  ADD UNIQUE KEY `from_member_id` (`from_member_id`,`to_member_id`),
  ADD UNIQUE KEY `from_member_id_2` (`from_member_id`,`to_member_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `s_blocks`
--
ALTER TABLE `s_blocks`
  MODIFY `blocksid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=103;

--
-- AUTO_INCREMENT for table `s_blockscategories`
--
ALTER TABLE `s_blockscategories`
  MODIFY `catid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `s_files`
--
ALTER TABLE `s_files`
  MODIFY `fileid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=343;

--
-- AUTO_INCREMENT for table `s_gallery`
--
ALTER TABLE `s_gallery`
  MODIFY `gid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT for table `s_gallery_images`
--
ALTER TABLE `s_gallery_images`
  MODIFY `imgid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=566;

--
-- AUTO_INCREMENT for table `s_logs`
--
ALTER TABLE `s_logs`
  MODIFY `logsid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1085;

--
-- AUTO_INCREMENT for table `s_members`
--
ALTER TABLE `s_members`
  MODIFY `member_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `s_menu`
--
ALTER TABLE `s_menu`
  MODIFY `mid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=575;

--
-- AUTO_INCREMENT for table `s_message_sends`
--
ALTER TABLE `s_message_sends`
  MODIFY `msgid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;

--
-- AUTO_INCREMENT for table `s_modules`
--
ALTER TABLE `s_modules`
  MODIFY `moduleid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1000064;

--
-- AUTO_INCREMENT for table `s_news`
--
ALTER TABLE `s_news`
  MODIFY `newsid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=857;

--
-- AUTO_INCREMENT for table `s_newscategories`
--
ALTER TABLE `s_newscategories`
  MODIFY `catid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `s_notifications`
--
ALTER TABLE `s_notifications`
  MODIFY `nid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `s_options`
--
ALTER TABLE `s_options`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `s_questionanswers`
--
ALTER TABLE `s_questionanswers`
  MODIFY `answerid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `s_questions`
--
ALTER TABLE `s_questions`
  MODIFY `questionid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `s_questionscategories`
--
ALTER TABLE `s_questionscategories`
  MODIFY `catid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `s_slider`
--
ALTER TABLE `s_slider`
  MODIFY `sid` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `s_subscribe`
--
ALTER TABLE `s_subscribe`
  MODIFY `sid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `s_subscribe_broadcast`
--
ALTER TABLE `s_subscribe_broadcast`
  MODIFY `bid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `s_subscribe_templates`
--
ALTER TABLE `s_subscribe_templates`
  MODIFY `tid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `s_tests`
--
ALTER TABLE `s_tests`
  MODIFY `testid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `s_tests_correct_answers`
--
ALTER TABLE `s_tests_correct_answers`
  MODIFY `answerid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `s_users`
--
ALTER TABLE `s_users`
  MODIFY `userid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
